#include <iomanip>
#include <iostream>
#include <vector>

/* #include "analytic/4q1g-analytic.h" */
#include "chsums/4q1g.h"
#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

const int Nc { 1000000 };
const int W { 5 };

/* template <typename T> */
/* std::vector<MOM<T>> reorder(const std::vector<MOM<T>>& input, const std::vector<int>& order) */
/* { */
/*     std::vector<MOM<T>> output(5); */
/*     for (int i { 0 }; i < 5; ++i) { */
/*         output[i] = input[order[i]]; */
/*     } */
/*     return output; */
/* } */

template <typename T>
void run(const int num)
{
    const std::vector<double> scales2 { { 0 } };

    std::cout << '\n'
              /* << "4q1g (qq->QQg)" << '\n' */
              << "4q1g (qq->qqg)" << '\n'
              << "Nc=" << Nc << '\n'
              << '\n';

    Amp4q1g2<T> amp;
    amp.setNc(Nc);

    for (int rseed { 1 }; rseed < num + 1; ++rseed) {
        std::cout << "Phase-space point " << rseed << '\n';

        PhaseSpace<T> ps5(5, rseed);
        std::vector<MOM<T>> momenta { ps5.getPSpoint() };

        refineM(momenta, momenta, scales2);

        for (const MOM<T>& momentum : momenta) {
            std::cout << momentum << '\n';
        }

        // std::cout << "0_q 1_qb -> 2_Q 3_Qb 4_g" << '\n';
        amp.setMomenta(momenta);

        std::cout << "Helicity sum" << '\n';
        std::cout << std::setw(W) << "A2:" << std::setw(std::cout.precision() + 8) << amp.born() << '\n';

        for (auto h : Amp4q1g2Static::HSarr) {
            for (int i { 0 }; i < 5; ++i) {
                std::cout << (h[i] == 1 ? '+' : '-');
            }
            std::cout << '\n';

            std::cout << std::setw(W) << "hA2:" << std::setw(std::cout.precision() + 8) << amp.born(h) << '\n';
        }

        // std::cout << "0_q 2_g -> 1_q 3_g 4_g" << '\n';
        // std::vector<MOM<T>> mom2 { reorder(momenta, { 0, 2, 1, 3, 4 }) };
        // amp.setMomenta(mom2);
        // std::cout << std::setw(W) << "A2:" << std::setw(std::cout.precision() + 8) << amp.born() << '\n';

        // std::cout << "3_g 4_g -> 0_q 1_qb 2_g" << '\n';
        // std::vector<MOM<T>> mom3 { reorder(momenta, { 3, 4, 0, 2, 1 }) };
        // amp.setMomenta(mom3);
        // std::cout << std::setw(W) << "A2:" << std::setw(std::cout.precision() + 8) << amp.born() << '\n';

        std::cout << '\n';
    }
}

int main()
{
    std::cout << std::scientific << std::setprecision(16);
    run<double>(2);
}
