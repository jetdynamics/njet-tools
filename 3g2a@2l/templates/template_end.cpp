
#ifdef USE_SD
template class AmpPPPPP_a2v_CCCCC<double>;
#endif
#ifdef USE_DD
template class AmpPPPPP_a2v_CCCCC<dd_real>;
#endif
#ifdef USE_QD
template class AmpPPPPP_a2v_CCCCC<qd_real>;
#endif
#ifdef USE_VC
template class AmpPPPPP_a2v_CCCCC<Vc::double_v>;
#endif
