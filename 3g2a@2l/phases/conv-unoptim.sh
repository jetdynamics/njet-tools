#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
OUT=${BASE}.c

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|(s[AB])\[(\d, \d)\]|a.\1(\2)|g;
        s|pow\[(.*?)\]|njet_pow(\1)|g;
        s|1 \+|T(1.) +|g;
        s|\{(.*)\}|\1|gs;
        " \
    ${IN} >${OUT}
