(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/3g2a@2l/phases"];
<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_5g.m"


njetify[expr_]:=expr//.{spA[a_,b_]->sA[a,b],spB[a_,b_]->sB[a,b],Power[a_,b_]/;b>1->pow[a,b],Power[a_,b_]/;b<-1->1/pow[a,-b],ex[1]->x1,ex[2]->x2,ex[3]->x3,ex[4]->x4,ex[5]->x5}/.{p[i_]->i-1};
treeX[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalytic]//FullSimplify;


(* ::Section:: *)
(*MHVs*)


mhv[m1_,m2_]:=Module[{o,n},
	o=DeleteCases[Range[5],m1|m2];
	(spA[p[m1],p[m2]]^2/(spA[p[o[[1]]],p[o[[2]]]]*spA[p[o[[2]]],p[o[[3]]]]*spA[p[o[[3]]],p[o[[1]]]]))
];


poss=Table[{i,j},{i,5},{j,i+1,5}]//Flatten[#,1]&;
names=m@@#&/@poss;
strees=mhv@@#&/@poss;
njetstrees=strees//njetify


xtrees=treeX/@strees;
njetxtrees=xtrees//njetify


Export["phases.m",{#[[1]]->#[[2]]&/@Transpose[{names,njetstrees}],#[[1]]->#[[2]]&/@Transpose[{names,njetxtrees}]}];


(* ::Section:: *)
(*UHVs*)


allPlus=1/Times@@(spA[p[#],p[Mod[#+1,5,1]]]&/@Range[5])


allPlus//njetify


singleMinus[m_]:=Module[{n},
	n[i_]:=Mod[m+i,5,1];
	spA[p[n[0]],p[n[1]]]*spB[p[n[1]],p[n[2]]]*spA[p[n[2]],p[n[0]]]/(spA[p[n[4]],p[n[1]]]*spA[p[n[1]],p[n[2]]]*spA[p[n[2]],p[n[3]]]*spA[p[n[3]],p[n[4]]])
];


sinMins=singleMinus/@Range[5]


sinMinS=sinMins//njetify


Export["zeroesS.m",sinMinS];


zeroes=Join[{allPlus},sinMins]


zeroesX=(GetMomentumTwistorExpression[#,PSanalytic]//Simplify)&/@zeroes


zeroExp=zeroesX//njetify


Export["zeroes.m",zeroExp];
