(* Created with the Wolfram Language : www.wolfram.com *)
NSM[{40, 11}, {1, 2, 2, 2, 3, 3, 20, 9, 8, 8, 8}, 
 {w[0] -> 2, w[1] -> 1, w[2] -> 1/3}, {m[0, 0] -> -w[0], m[6, 1] -> -w[0], 
  m[10, 1] -> w[0], m[1, 2] -> -w[0], m[19, 2] -> w[0], m[10, 3] -> -w[0], 
  m[19, 3] -> w[0], m[3, 4] -> w[0], m[10, 4] -> -w[0], m[31, 4] -> -w[0], 
  m[14, 5] -> w[0], m[19, 5] -> -w[0], m[31, 5] -> -w[0], m[2, 6] -> -w[1], 
  m[4, 6] -> w[1], m[7, 6] -> -w[1], m[8, 6] -> w[1], m[9, 6] -> -w[1], 
  m[11, 6] -> -w[1], m[12, 6] -> w[1], m[15, 6] -> w[1], m[16, 6] -> w[1], 
  m[17, 6] -> -w[1], m[20, 6] -> w[1], m[21, 6] -> -w[1], m[22, 6] -> w[1], 
  m[23, 6] -> -w[1], m[26, 6] -> -w[0], m[28, 6] -> -w[0], m[32, 6] -> -w[0], 
  m[33, 6] -> -w[0], m[34, 6] -> w[0], m[35, 6] -> w[0], m[5, 7] -> -w[1], 
  m[9, 7] -> w[1], m[11, 7] -> w[0], m[12, 7] -> -w[0], m[25, 7] -> -w[0], 
  m[26, 7] -> w[0], m[30, 7] -> -w[2], m[34, 7] -> -w[0], m[36, 7] -> w[0], 
  m[2, 8] -> w[0], m[4, 8] -> -w[0], m[20, 8] -> -w[0], m[21, 8] -> w[0], 
  m[24, 8] -> w[0], m[28, 8] -> w[0], m[35, 8] -> -w[0], m[37, 8] -> w[0], 
  m[9, 9] -> w[1], m[13, 9] -> -w[1], m[16, 9] -> -w[0], m[17, 9] -> w[0], 
  m[26, 9] -> w[0], m[27, 9] -> -w[0], m[33, 9] -> w[0], m[38, 9] -> -w[0], 
  m[2, 10] -> w[1], m[15, 10] -> -w[0], m[18, 10] -> w[1], m[28, 10] -> w[0], 
  m[29, 10] -> w[0], m[30, 10] -> w[2], m[32, 10] -> w[0], 
  m[39, 10] -> -w[0]}]
