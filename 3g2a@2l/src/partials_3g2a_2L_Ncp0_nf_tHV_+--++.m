(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1]}}, {{1}}, {{MySparseMatrix[{1, 1}, {m[1, 1] -> -2/3}], 
   MySparseMatrix[{1, 1}, {}]}}, {1, pflip}, 
 {f[1] -> (y[2]*y[3])/y[1] - (y[2]^2*y[4]^2*y[6])/(y[1]*y[5]^2) - 
    (y[2]*y[7])/(y[1]*y[5])}, {y[1] -> ex[1], y[2] -> ex[3], 
  y[3] -> 1 + ex[3], y[4] -> ex[2], y[5] -> ex[4], y[6] -> -1 + ex[5], 
  y[7] -> ex[2] + 2*ex[2]*ex[3] + ex[5] + ex[3]*ex[5]}}
