(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1]}}, {{1}}, {{MySparseMatrix[{1, 1}, {m[1, 1] -> 1/64}], 
   MySparseMatrix[{1, 1}, {}]}}, {1, pflip}, 
 {f[1] -> (-128*y[1]^3*y[2]^2*y[3]^2*y[4]^2)/3 + 
    (128*y[1]^3*y[2]^4*y[3]^3*y[6])/(3*y[5]) + 
    (128*y[1]^3*y[2]^2*y[3]^2*y[4]*y[7])/(3*y[5])}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> ex[3], y[4] -> ex[4], 
  y[5] -> 1 + ex[3], y[6] -> -1 + ex[5], 
  y[7] -> ex[2] + 2*ex[2]*ex[3] + ex[5] + ex[3]*ex[5]}}
