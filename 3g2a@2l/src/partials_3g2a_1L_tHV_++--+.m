(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1], f[2], f[3], f[4], f[5], f[6], f[7], f[8], f[9], f[10], f[11]}}, 
 {{1, 19, 4, 24, 26, 5, 34, 7, 8, 37, 38, 42, 44, 105, 45, 47, 51, 52, 53, 
   54, 56, 58, 61, 62, 10, 11, 78, 106, 80, 81, 12, 93, 14, 15, 95, 96, 16, 
   17, 109, 99}}, {{MySparseMatrix[{11, 40}, {m[1, 1] -> -2, m[2, 7] -> -2, 
     m[2, 11] -> 2, m[3, 2] -> -2, m[3, 20] -> 2, m[4, 11] -> -2, 
     m[4, 20] -> 2, m[5, 4] -> 2, m[5, 11] -> -2, m[5, 32] -> -2, 
     m[6, 15] -> 2, m[6, 20] -> -2, m[6, 32] -> -2, m[7, 3] -> -1, 
     m[7, 5] -> 1, m[7, 8] -> -1, m[7, 9] -> 1, m[7, 10] -> -1, 
     m[7, 12] -> -1, m[7, 13] -> 1, m[7, 16] -> 1, m[7, 17] -> 1, 
     m[7, 18] -> -1, m[7, 21] -> 1, m[7, 22] -> -1, m[7, 23] -> 1, 
     m[7, 24] -> -1, m[7, 27] -> -2, m[7, 29] -> -2, m[7, 33] -> -2, 
     m[7, 34] -> -2, m[7, 35] -> 2, m[7, 36] -> 2, m[8, 6] -> -1, 
     m[8, 10] -> 1, m[8, 12] -> 2, m[8, 13] -> -2, m[8, 26] -> -2, 
     m[8, 27] -> 2, m[8, 31] -> -1/3, m[8, 35] -> -2, m[8, 37] -> 2, 
     m[9, 3] -> 2, m[9, 5] -> -2, m[9, 21] -> -2, m[9, 22] -> 2, 
     m[9, 25] -> 2, m[9, 29] -> 2, m[9, 36] -> -2, m[9, 38] -> 2, 
     m[10, 10] -> 1, m[10, 14] -> -1, m[10, 17] -> -2, m[10, 18] -> 2, 
     m[10, 27] -> 2, m[10, 28] -> -2, m[10, 34] -> 2, m[10, 39] -> -2, 
     m[11, 3] -> 1, m[11, 16] -> -2, m[11, 19] -> 1, m[11, 29] -> 2, 
     m[11, 30] -> 2, m[11, 31] -> 1/3, m[11, 33] -> 2, m[11, 40] -> -2}], 
   MySparseMatrix[{11, 40}, {}]}}, {1, pflip}, 
 {f[11] -> (y[2]*y[3]^2*y[5])/(y[1]*y[4]^3), 
  f[6] -> y[7]/(y[1]*y[4]^2*y[6]) - (y[2]*y[3]^2*y[6]^2)/(y[1]*y[4]^2*y[8]) + 
    y[2]/(y[1]*y[6]*y[9]), f[1] -> y[10]/(y[1]*y[3]*y[4]^3*y[6]^2) + 
    (y[2]^2*y[3]^2)/(y[1]*y[4]^2*y[11]*y[12]) - 1/(y[1]*y[12]*y[13]) + 
    (y[2]*y[3]^2*y[14])/(y[1]*y[4]*y[12]*y[15]*y[16]*y[17]) + 
    (y[2]*y[3]*y[12]^2*y[18]^3)/(y[1]*y[4]^3*y[17]*y[19]) - 
    (y[3]*y[21])/(y[1]*y[12]*y[16]*y[20]*y[22]) - 
    (y[12]^2*y[18]^3)/(y[1]*y[3]*y[4]*y[6]^2*y[22]*y[23]), 
  f[10] -> y[24]/(y[1]*y[3]*y[6]), 
  f[2] -> (y[2]^2*y[3]^2)/(y[1]*y[4]^2*y[11]^2) - 
    (y[2]^2*y[3]*y[6]*y[12]^2*y[18]^3)/(y[1]*y[4]^4*y[19]^2) - 
    (y[18]*y[25])/(y[1]*y[4]^4) + (y[2]*y[3]^2*y[26])/(y[1]*y[4]^3*y[11]) - 
    (y[2]*y[12]*y[18]^2*y[27])/(y[1]*y[4]^4*y[19]), 
  f[4] -> y[2]/(y[1]*y[6]*y[9]) - 1/(y[1]*y[13]^2) + 
    y[28]/(y[1]*y[3]*y[6]^2) + y[29]/(y[1]*y[3]*y[6]*y[13]), 
  f[9] -> y[30]/(y[1]*y[3]^2*y[6]^2), 
  f[5] -> (y[2]^2*y[3]^2)/(y[1]*y[4]^2*y[11]^2) + 1/(y[1]*y[13]^2) + 
    (y[2]*y[3]^2*y[26])/(y[1]*y[4]^3*y[11]) - y[29]/(y[1]*y[3]*y[6]*y[13]) - 
    y[31]/(y[1]*y[3]*y[4]^3*y[6]^2), f[8] -> (y[3]^2*y[32])/(y[1]*y[4]^4), 
  f[7] -> (y[3]*y[33])/(y[1]*y[4]*y[6]), 
  f[3] -> 1/(y[1]*y[13]^2) + (y[12]^2*y[18]^3)/(y[1]*y[3]^2*y[4]*y[6]^2*
      y[23]^2) - y[29]/(y[1]*y[3]*y[6]*y[13]) + 
    (y[18]*y[34])/(y[1]*y[3]^2*y[4]^2*y[6]^2) - (y[12]*y[18]^2*y[35])/
     (y[1]*y[3]^2*y[4]^2*y[6]^2*y[23])}, {y[1] -> ex[1], y[2] -> 1 + ex[2], 
  y[3] -> ex[3], y[4] -> 1 + ex[3] + ex[2]*ex[3], 
  y[5] -> 1 + 2*ex[2] + ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3], y[6] -> ex[2], 
  y[7] -> 1 + 2*ex[3] + 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[8] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4], 
  y[9] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[10] -> 1 + 4*ex[3] + 2*ex[2]*ex[3] + 6*ex[3]^2 + 6*ex[2]*ex[3]^2 + 
    4*ex[3]^3 + 6*ex[2]*ex[3]^3 + 2*ex[2]^2*ex[3]^3 + ex[3]^4 + 
    2*ex[2]*ex[3]^4 + 2*ex[2]^2*ex[3]^4 + ex[2]^3*ex[3]^4, 
  y[11] -> 1 + ex[4], y[12] -> ex[5], y[13] -> ex[4] - ex[5], 
  y[14] -> 1 + ex[2] - ex[5], y[15] -> 1 + ex[4] - ex[5], 
  y[16] -> -1 + ex[5], y[17] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - 
    ex[3]*ex[5], y[18] -> 1 + ex[3], 
  y[19] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] - ex[2]*ex[5], y[20] -> ex[4], y[21] -> ex[2] + ex[5], 
  y[22] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[23] -> ex[2]*ex[3] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[24] -> -1 + ex[2]*ex[3], y[25] -> 1 + 3*ex[3] + 4*ex[2]*ex[3] + 
    2*ex[3]^2 + 2*ex[2]*ex[3]^2, y[26] -> 1 + 3*ex[2] + ex[3] + 
    2*ex[2]*ex[3] + ex[2]^2*ex[3], y[27] -> 1 + 3*ex[3] + 5*ex[2]*ex[3] + 
    2*ex[3]^2 + 2*ex[2]*ex[3]^2, y[28] -> 2 + ex[3], 
  y[29] -> -2 + ex[2]*ex[3], y[30] -> 2 + 2*ex[3] - ex[2]*ex[3] + ex[3]^2 + 
    ex[2]^2*ex[3]^2, y[31] -> 2 + 7*ex[3] + 5*ex[2]*ex[3] + 9*ex[3]^2 + 
    12*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 5*ex[3]^3 + 9*ex[2]*ex[3]^3 + 
    5*ex[2]^2*ex[3]^3 + 2*ex[2]^3*ex[3]^3 + ex[3]^4 + 2*ex[2]*ex[3]^4 + 
    2*ex[2]^2*ex[3]^4 + ex[2]^3*ex[3]^4, 
  y[32] -> 2 + 5*ex[2] + 4*ex[2]^2 + 4*ex[3] + 11*ex[2]*ex[3] + 
    10*ex[2]^2*ex[3] + 3*ex[2]^3*ex[3] + 2*ex[3]^2 + 6*ex[2]*ex[3]^2 + 
    7*ex[2]^2*ex[3]^2 + 4*ex[2]^3*ex[3]^2 + ex[2]^4*ex[3]^2, 
  y[33] -> 1 + 2*ex[2] + 2*ex[2]^2, y[34] -> 3 + 5*ex[3] + 4*ex[2]*ex[3] + 
    2*ex[3]^2 + 2*ex[2]*ex[3]^2, y[35] -> 4 + 6*ex[3] + 5*ex[2]*ex[3] + 
    2*ex[3]^2 + 2*ex[2]*ex[3]^2}}
