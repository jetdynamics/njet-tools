#!/usr/bin/env python3

import math
import numpy
import pickle
import pathlib
import sys
import os
from functools import reduce

dirname = os.path.dirname(__file__)
path1 = os.path.join(dirname, "../test-stability/")
sys.path.insert(0, path1)

import reader


def parse_moms(filename):
    if not pathlib.Path(filename).is_file():
        print(f"Error: file {filename} not found!")
        exit()

    moms = []
    pt = []

    with open(filename, "r") as filecont:
        for line in filecont:
            if not (line := line.strip()):
                moms.append(pt)
                pt = []
            else:
                if not line[0].isalpha():
                    pt.append(line.split())
        if pt:
            moms.append(pt)

    return numpy.array(moms, dtype=numpy.float64)


def read_moms(filename):
    data = reader.read_pickle(filename)
    if data is None:
        data = parse_moms(filename)
        reader.write(filename, data)
    return data


def dot(m0, m1):
    return reduce(lambda t, i: t - m0[i] * m1[i], range(1, 4), m0[0] * m1[0])


class Alphabet:
    def __init__(self, moms):
        self.moms = moms

        self.sijs = dict()

        self.w = numpy.empty(31, dtype=numpy.float64)

        for i in range(5):
            self.w[i] = self.v(i)

        for i in range(5, 10):
            self.w[i] = self.v(i + 2) + self.v(i + 3)

        for i in range(10, 15):
            self.w[i] = self.w[i - 10] - self.v(i + 3)

        for i in range(15, 20):
            self.w[i] = self.w[i - 5] + self.v(i + 1)

        for i in range(20, 25):
            self.w[i] = self.w[i - 15] - self.w[i - 20] - self.v(i + 1)

        self.w[30] = math.sqrt(math.fabs(self.delta()))  # just consider magnitude

        for i in range(25, 30):
            aa = self.a(i, i + 1, i + 2, i + 3)
            self.w[i] = (aa - self.w[30]) / (aa + self.w[30])

    def sij(self, i, j):
        if (i, j) not in self.sijs:
            self.sijs[(i, j)] = 2 * dot(self.moms[i], self.moms[j])
        return self.sijs[(i, j)]

    def v(self, i):
        return self.sij(i % 5, (i + 1) % 5)

    def a(self, i1, i2, i3, i4):
        i1 %= 5
        i2 %= 5
        i3 %= 5
        i4 %= 5
        i5 = {1, 2, 3, 4, 5}.difference({i1, i2, i3, i4}).pop()
        return (
            self.v(i1) * self.v(i2)
            - self.v(i2) * self.v(i3)
            + self.v(i3) * self.v(i4)
            - self.v(i1) * self.v(i5)
            - self.v(i4) * self.v(i5)
        )

    def delta(self):
        return (
            -2 * self.sij(0, 1) ** 2 * self.sij(2, 3) * self.sij(2, 4) * self.sij(3, 4)
            - 2 * self.sij(0, 1) * self.sij(0, 2) * self.sij(1, 2) * self.sij(3, 4) ** 2
            + 2
            * self.sij(0, 1)
            * self.sij(0, 2)
            * self.sij(1, 3)
            * self.sij(2, 4)
            * self.sij(3, 4)
            + 2
            * self.sij(0, 1)
            * self.sij(0, 2)
            * self.sij(1, 4)
            * self.sij(2, 3)
            * self.sij(3, 4)
            + 2
            * self.sij(0, 1)
            * self.sij(0, 3)
            * self.sij(1, 2)
            * self.sij(2, 4)
            * self.sij(3, 4)
            - 2 * self.sij(0, 1) * self.sij(0, 3) * self.sij(1, 3) * self.sij(2, 4) ** 2
            + 2
            * self.sij(0, 1)
            * self.sij(0, 3)
            * self.sij(1, 4)
            * self.sij(2, 3)
            * self.sij(2, 4)
            + 2
            * self.sij(0, 1)
            * self.sij(0, 4)
            * self.sij(1, 2)
            * self.sij(2, 3)
            * self.sij(3, 4)
            + 2
            * self.sij(0, 1)
            * self.sij(0, 4)
            * self.sij(1, 3)
            * self.sij(2, 3)
            * self.sij(2, 4)
            - 2 * self.sij(0, 1) * self.sij(0, 4) * self.sij(1, 4) * self.sij(2, 3) ** 2
            - 2 * self.sij(0, 2) ** 2 * self.sij(1, 3) * self.sij(1, 4) * self.sij(3, 4)
            + 2
            * self.sij(0, 2)
            * self.sij(0, 3)
            * self.sij(1, 2)
            * self.sij(1, 4)
            * self.sij(3, 4)
            + 2
            * self.sij(0, 2)
            * self.sij(0, 3)
            * self.sij(1, 3)
            * self.sij(1, 4)
            * self.sij(2, 4)
            - 2 * self.sij(0, 2) * self.sij(0, 3) * self.sij(1, 4) ** 2 * self.sij(2, 3)
            + 2
            * self.sij(0, 2)
            * self.sij(0, 4)
            * self.sij(1, 2)
            * self.sij(1, 3)
            * self.sij(3, 4)
            - 2 * self.sij(0, 2) * self.sij(0, 4) * self.sij(1, 3) ** 2 * self.sij(2, 4)
            + 2
            * self.sij(0, 2)
            * self.sij(0, 4)
            * self.sij(1, 3)
            * self.sij(1, 4)
            * self.sij(2, 3)
            - 2 * self.sij(0, 3) ** 2 * self.sij(1, 2) * self.sij(1, 4) * self.sij(2, 4)
            - 2 * self.sij(0, 3) * self.sij(0, 4) * self.sij(1, 2) ** 2 * self.sij(3, 4)
            + 2
            * self.sij(0, 3)
            * self.sij(0, 4)
            * self.sij(1, 2)
            * self.sij(1, 3)
            * self.sij(2, 4)
            + 2
            * self.sij(0, 3)
            * self.sij(0, 4)
            * self.sij(1, 2)
            * self.sij(1, 4)
            * self.sij(2, 3)
            - 2 * self.sij(0, 4) ** 2 * self.sij(1, 2) * self.sij(1, 3) * self.sij(2, 3)
        )


if __name__ == "__main__":
    moms_file = "../collab/points_new.txt"
    moms = read_moms(moms_file)
    alphas = numpy.array([Alphabet(mom).w for mom in moms])
    reader.write("alphabets", alphas)
