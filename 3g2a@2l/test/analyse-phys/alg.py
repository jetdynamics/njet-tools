#!/usr/bin/env python3

import sympy

(
    s00,
    s01,
    s02,
    s03,
    s04,
    s10,
    s11,
    s12,
    s13,
    s14,
    s20,
    s21,
    s22,
    s23,
    s24,
    s30,
    s31,
    s32,
    s33,
    s34,
    s40,
    s41,
    s42,
    s43,
    s44,
) = sympy.symbols(
    "s00,s01,s02,s03,s04,s10,s11,s12,s13,s14,s20,s21,s22,s23,s24,s30,s31,s32,s33,s34,s40,s41,s42,s43,s44"
)

mat = sympy.Matrix(
    [
        [s00, s01, s02, s03, s04],
        [s10, s11, s12, s13, s14],
        [s20, s21, s22, s23, s24],
        [s30, s31, s32, s33, s34],
        [s40, s41, s42, s43, s44],
    ]
)

mat = mat.subs(
    [
        (s00, 0),
        (s11, 0),
        (s22, 0),
        (s33, 0),
        (s44, 0),
        (s10, s01),
        (s20, s02),
        (s30, s03),
        (s40, s04),
        (s21, s12),
        (s31, s13),
        (s41, s14),
        (s32, s23),
        (s42, s24),
        (s43, s34),
    ]
)

print(mat)
print()

det = mat.det()

print(det)
print()
