#!/usr/bin/env python3

import argparse, sys, os, math

import numpy, matplotlib.pyplot

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "../test-stability/")
sys.path.insert(0, filename)

import reader, main, plotter


# the truest number should be second (=b)
def d(a, b):
    return abs(a - b) / b


def consistency(
    data,
    alphabets,
    loops=2,
    cut=0.1,
    print_all=False,
    dp=1,
    base="QQ",
    print_fail=False,
):
    print()

    c = 0
    if loops == 2:
        j = 3
    elif loops == 1:
        j = 1

    b = 2 if base == "QQ" else 1
    N = len(data[b])
    for i in range(N):
        n = int(data[b][i, 0])
        dd_val, dd_err = data[0][data[0][:, 0] == n][0, [j, j + 1]]
        if print_all and base == "QD":
            qd_val, qd_err = data[1][data[1][:, 0] == n][0, [j, j + 1]]
        qq_val, qq_err = data[b][i, [j, j + 1]]
        rel_diff = d(dd_val, qq_val)
        if print_all:
            print("DD", dd_val, dd_err, rel_diff)
            if base == "QD":
                print("QD", qd_val, qd_err, d(qd_val, qq_val))
            print(base, qq_val, qq_err, 1)
        if rel_diff > max(dd_err, cut):
            c += 1
            if print_all:
                print("Fail")
            if print_fail and not print_all:
                print("DD", dd_val, dd_err)
                print("QQ", qq_val, qq_err)
                print("d ", rel_diff)
                a = alphabets[n - 1][:25]
                print(a)
                a = numpy.abs(a)
                print("k ", a.min() / a.max())  # kappa
                # print()
                input()  # step through
        else:
            if print_all:
                print("Pass")
        if print_all:
            print()

    if print_all:
        print("Columns: precision 2L_value 2L_error rel_diff_to_QQ")
        print()

    cut_perc = round(100 * cut, dp)
    fail_perc = round(100 * c / N, dp)
    print(
        f"{loops}L: {c} failed out of {N} ({fail_perc}%) to meet rel_diff(DD, {base}) < max(DD err, {cut_perc}%)"
    )


def histo_alt(
    data,
    names,
    cutoff=1e-3,
    label="",
    x_min_pow=None,
    x_max_pow=None,
    num_bins=100,
    leg_loc="best",
    xlog=True,
    ylog=True,
    w=6.4,
    r=3 / 4,
    legend=True,
    xlabel="",
    normalisation=1.0,
    vector=False,
    xexps=False,
    title=True,
    cumulative=False,
    weight=1.0,
):
    if cumulative:
        rescue = numpy.array([])
        for error in data:
            error = error[error < cutoff]
            rescue = numpy.concatenate((rescue, error))

    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w * r))

    if xlog:
        matplotlib.pyplot.xscale("log")
        mini = (
            numpy.log10(numpy.min([numpy.min(x) for x in data]))
            if x_min_pow is None
            else x_min_pow
        )
        maxi = (
            numpy.log10(numpy.max([numpy.max(x) for x in data]))
            if x_max_pow is None
            else x_max_pow
        )
        bins = numpy.logspace(mini, maxi, num=num_bins, base=10)
    else:
        bins = num_bins

    for datum, name in zip(data, names):
        ax.hist(
            datum / normalisation,
            bins=bins,
            histtype="step",
            log=ylog,
            label=name,
            weights=numpy.full_like(datum, weight),
        )

    if cumulative:
        _, _, patches = ax.hist(
            rescue,
            cumulative=-1,
            bins=bins,
            histtype="step",
            log=ylog,
            label="Cumulative",
            linestyle="dashed",
            color="black",
            weights=numpy.full_like(rescue, weight),
        )
        patches[0].set_xy(patches[0].get_xy()[1:])

    if legend:
        ax.legend(loc=leg_loc, frameon=False)

    ax.set_xlabel(xlabel)
    ax.set_ylabel("Frequency" if weight == 1.0 else "Proportion")

    if xexps:
        ax.xaxis.set_major_formatter(lambda x, _: -int(math.log10(x)))

    if xlog:
        fm = math.floor(mini)
        cm = math.ceil(maxi)
        ax.set_xticks(
            numpy.logspace(fm, cm, num=-fm + cm + 1, base=10),
            minor=True,
        )
        ax.tick_params(axis="x", which="minor", labelbottom=False)

    ax.axvline(cutoff, color="black", linewidth=1)

    ax.invert_xaxis()

    plotter.write(fig, f"stability_ggyyg{plotter.slug(label)}", vector)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse stability and timing of 3g2A 2L run over NNLOJET 1L phase space"
    )
    parser.add_argument(
        "-p",
        "--plot",
        action="store_true",
        help="produce plots",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="verbose logging to terminal",
    )
    parser.add_argument(
        "-c",
        "--consistency",
        action="store_true",
        help="consistency tests",
    )
    args = parser.parse_args()

    dr = "../collab/3g2a_phys_res"
    # dr = "../test-stability/3g2a_res"
    # dr = "../test-stability/5g_res"
    print("Using", dr)

    # all_3g2a_dd, all_3g2a_qd, all_3g2a_qq = reader.read_proc(dr, "result")
    all_3g2a = reader.read_proc(dr, "result")

    data = main.stability(
        *all_3g2a,
        4,
    )

    if args.consistency:

        alphabets = reader.read_pickle("alphabets")

        consistency(all_3g2a, alphabets, cut=0.1, loops=1, print_all=args.verbose)
        consistency(
            all_3g2a, alphabets, cut=0.1, loops=2, base="QD", print_all=args.verbose
        )
        consistency(
            all_3g2a,
            alphabets,
            cut=0.1,
            loops=2,
            print_all=args.verbose,
            print_fail=True,
        )

    if args.plot:
        print("Plotting...")

        plotter.init_plots()

        # plotter.histo_alt(
        #     all_3g2a,
        #     names=(
        #         r"$\texttt{f64}$/$\texttt{f64}$",
        #         r"$\texttt{f128}$/$\texttt{f64}$",
        #         r"$\texttt{f128}$/$\texttt{f128}$",
        #     ),
        #     xlabel="Correct digits",
        #     x_min_pow=-21,
        #     x_max_pow=3,
        #     xexps=True,
        # )

        # plotter.histo_alt(
        #     all_3g2a,
        #     names=(
        #         r"$\texttt{f64}$/$\texttt{f64}$",
        #         r"$\texttt{f128}$/$\texttt{f64}$",
        #         r"$\texttt{f128}$/$\texttt{f128}$",
        #     ),
        #     xlabel="Correct digits",
        #     x_min_pow=-15,
        #     x_max_pow=2,
        #     xexps=True,
        #     label="slides",
        #     title=False,
        #     vector=True,
        #     cumulative=True,
        #     leg_loc="upper left",
        # )

        histo_alt(
            [x[:, 4] for x in all_3g2a],
            names=(
                r"$\texttt{f64}$/$\texttt{f64}$",
                r"$\texttt{f128}$/$\texttt{f64}$",
                r"$\texttt{f128}$/$\texttt{f128}$",
            ),
            xlabel="Correct digits",
            xexps=True,
            label="proc",
            title=False,
            vector=True,
            cumulative=True,
            leg_loc="upper left",
            r=1 / plotter.PHI,
            weight=1 / len(all_3g2a[0]),
            num_bins=50,
        )

        # (
        #     cumulative3g2a,
        #     specfs3g2a,
        #     coeffs3g2a,
        #     t_specf3g2a,
        #     t_coeff3g2a,
        #     t_specf_D_pass_3g2a,
        #     t_specf_Q_pass_3g2a,
        #     t_coeff_D_pass_3g2a,
        #     t_coeff_Q_pass_3g2a,
        #     t_specf_D_fail_3g2a,
        #     t_specf_Q_fail_3g2a,
        #     t_coeff_D_fail_3g2a,
        #     t_coeff_Q_fail_3g2a,
        # ) = main.timing_cutoff(
        #     *all_3g2a,
        #     4,
        #     5,
        #     6,
        # )

        # plotter.histo_single(
        #     data,
        #     names=(
        #         r"$\texttt{f64}$/$\texttt{f64}$ pass",
        #         r"$\texttt{f128}$/$\texttt{f64}$ pass",
        #         r"$\texttt{f128}$/$\texttt{f128}$ pass",
        #         r"$\texttt{f64}$/$\texttt{f64}$ fail",
        #         r"$\texttt{f128}$/$\texttt{f64}$ fail",
        #         r"$\texttt{f128}$/$\texttt{f128}$ fail",
        #     ),
        #     xlabel="Correct digits",
        #     x_min_pow=-15,
        #     x_max_pow=3,
        #     label="prelim_stability",
        #     xexps=True,
        # )

        # plotter.histo_single(
        #     data,
        #     names=(
        #         r"$\texttt{f64}$/$\texttt{f64}$ pass",
        #         r"$\texttt{f128}$/$\texttt{f64}$ pass",
        #         r"$\texttt{f128}$/$\texttt{f128}$ pass",
        #         r"$\texttt{f64}$/$\texttt{f64}$ fail",
        #         r"$\texttt{f128}$/$\texttt{f64}$ fail",
        #     ),
        #     xlabel="Correct digits",
        #     x_min_pow=-15,
        #     x_max_pow=3,
        #     label="stability",
        #     vector=True,
        #     xexps=True,
        # )

        # plotter.histo_single(
        #     (
        #         t_specf_D_pass_3g2a,
        #         t_specf_Q_pass_3g2a,
        #         t_coeff_D_pass_3g2a,
        #         t_coeff_Q_pass_3g2a,
        #     ),
        #     names=(
        #         "specf SD pass",
        #         "specf DD pass",
        #         "coeff SD pass",
        #         "coeff DD pass",
        #     ),
        #     xlabel="Time (s)",
        #     label="timing",
        #     normalisation=1e6,
        #     x_min_pow=-1.3,
        #     x_max_pow=3.5,
        # )
