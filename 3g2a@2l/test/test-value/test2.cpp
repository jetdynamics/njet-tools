#include <array>
#include <chrono>
#include <cstddef>
#include <iostream>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "chsums/0q3gA.h"
#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

template <typename T>
void run()
{
    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;
    TP t0;
    TP t1;

    const int Nc { 3 };
    const int Nf { 5 };
    const double mur { 91.188 };
    const std::vector<double> scales2 { { 0 } };

     std::vector<std::vector<MOM<T>>> Momenta { {
         { MOM<T>(
               -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717,
               0,
               0,
               -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717),
             MOM<T>(
                 -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717,
                 0,
                 0,
                 2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717),
             MOM<T>(
                 1.682974525366494782246854479945472254502023578629832118570207806012814704916308966565368132377714031,
                 -1.079747351318587941358594060381223399354431958293849060382947477320032048578042314995431578294157304,
                 0,
                 -1.290948841106057237503789725354001618279428184943145859825838243099600142313062082169686348029016787),
             MOM<T>(
                 1.242688824881897295162159277135318646818080525238122726300520128144964618834875928114614437727490379,
                 1.16031897874675827065945495640637647502875585398925755233836279500229403565165670926743339229,
                 0.3590852419648624529549368948138776181518711561008379998087151300037262257928613866001568451,
                 0.26266551362801115875997181715610795652612130008648577173008495962255999697621927713030222440),
             MOM<T>(
                 1.092154109873103145662680502223351565631812104932106825716255272148722450665922492832275006697117023,
                 -0.08057162742817032930086089602515307567432389569540849195541531768226198707361439427200181400,
                 -0.3590852419648624529549368948138776181518711561008379998087151300037262257928613866001568451,
                 1.02828332747804607874381790819789366175330688485666008809575328347704014533684280503938412363) },
     } };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<T>* const amp { NJetAccuracy<T>::template create<Amp0q3g2A_a2l<T, T>>(Ax) };
    amp->setNf(Nf);

    amp->setNc(Nc);
    amp->setMuR2(mur * mur);

     for (std::size_t p { 0 }; p < Momenta.size(); ++p) {
    //for (std::size_t p { 0 }; p < 10; ++p) {
        std::cout << "==================== Test point " << p + 1
                  << " ====================" << '\n';

         std::vector<MOM<T>> momenta { Momenta[p] };
        //std::vector<MOM<T>> momenta { njet_random_point<T>(p + 1) };

        refineM(momenta, momenta, scales2);

        //std::cout<<"s12="<<2*dot(momenta[0],momenta[1])<<'\n';
        //std::cout<<"s23="<<2*dot(momenta[1],momenta[2])<<'\n';

        t0 = std::chrono::high_resolution_clock::now();
        amp->setMomenta(momenta);
        amp->initFinRem();
        amp->setSpecFuncs();
        amp->c1lx1l_fin();
        amp->c2lx1l_fin();
        t1 = std::chrono::high_resolution_clock::now();
        const long int d1 { std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() };

        //std::vector<std::vector<int>> hels{{
        //    { +1, +1, +1, +1, +1 },
        //    { +1, +1, +1, +1, -1 },
        //    { +1, +1, +1, -1, +1 },
        //    { +1, +1, +1, -1, -1 },
        //    { +1, +1, -1, +1, +1 },
        //    { +1, +1, -1, +1, -1 },
        //    { +1, +1, -1, -1, +1 },
        //    { +1, +1, -1, -1, -1 },
        //    { -1, +1, +1, +1, +1 },
        //    { -1, +1, +1, +1, -1 },
        //    { -1, +1, +1, -1, +1 },
        //    { -1, +1, +1, -1, -1 },
        //    { -1, +1, -1, +1, +1 },
        //    { -1, +1, -1, +1, -1 },
        //    { -1, +1, -1, -1, +1 },
        //    { -1, +1, -1, -1, -1 },
        //}};
        //
        //for (std::vector<int> h : hels) {
        //for (int i : h){
        //std::cout << ((i==1)?'+':'-');
        //}
        //std::cout << '\n';

        std::cout
            << "time: " << d1 << "ms" << '\n'
            << "1lx1l val:   " << amp->c1lx1l_fin_value() << '\n'
            << "1lx1l err:   " << abs(amp->c1lx1l_fin_error() / amp->c1lx1l_fin_value()) << '\n'
            << "2lx1l val:   " << amp->c2lx1l_fin_value() << '\n'
            << "2lx1l err:   " << abs(amp->c2lx1l_fin_error() / amp->c2lx1l_fin_value()) << '\n'
            << "2lx1l/1lx1l: " << amp->c2lx1l_fin_value() / amp->c1lx1l_fin_value() << '\n'
            << '\n';
        //  }
    }
}

int main()
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    run<double>();
}
