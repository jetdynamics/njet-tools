#include <array>
#include <algorithm>
#include <chrono>
#include <cstddef>
#include <iostream>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "chsums/0q3gA.h"
#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

template <typename T, typename P>
void run()
{
    const int Nc { 3 };
    const int Nf { 5 };
    const double mur { 91.188 };
    const std::vector<double> scales2 { { 0 } };

     std::vector<std::vector<MOM<T>>> Momenta { {
         { MOM<T>(
               -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717,
               0,
               0,
               -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717),
             MOM<T>(
                 -2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717,
                 0,
                 0,
                 2.008908730060747611535847129652071233475958104400030835293491603153250887208553693756128788401160717),
             MOM<T>(
                 1.682974525366494782246854479945472254502023578629832118570207806012814704916308966565368132377714031,
                 -1.079747351318587941358594060381223399354431958293849060382947477320032048578042314995431578294157304,
                 0,
                 -1.290948841106057237503789725354001618279428184943145859825838243099600142313062082169686348029016787),
             MOM<T>(
                 1.242688824881897295162159277135318646818080525238122726300520128144964618834875928114614437727490379,
                 1.16031897874675827065945495640637647502875585398925755233836279500229403565165670926743339229,
                 0.3590852419648624529549368948138776181518711561008379998087151300037262257928613866001568451,
                 0.26266551362801115875997181715610795652612130008648577173008495962255999697621927713030222440),
             MOM<T>(
                 1.092154109873103145662680502223351565631812104932106825716255272148722450665922492832275006697117023,
                 -0.08057162742817032930086089602515307567432389569540849195541531768226198707361439427200181400,
                 -0.3590852419648624529549368948138776181518711561008379998087151300037262257928613866001568451,
                 1.02828332747804607874381790819789366175330688485666008809575328347704014533684280503938412363) },
     } };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    Amp0q3g2A_a<T> old(Ax, 1.);
    old.setNf(Nf);
    old.setNc(Nc);
    old.setMuR2(mur * mur);

    Amp0q3g2A_a2l<T, P> amp(Ax, 1.);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);

     for (std::size_t p { 0 }; p < Momenta.size(); ++p) {
    //for (std::size_t p { 0 }; p < 10; ++p) {
        std::cout << "==================== Test point " << p + 1
                  << " ====================" << '\n';

         std::vector<MOM<T>> momenta { Momenta[p] };
        //std::vector<MOM<T>> momenta { njet_random_point<T>(p + 1) };

        refineM(momenta, momenta, scales2);

        //std::cout<<"s12="<<2*dot(momenta[0],momenta[1])<<'\n';
        //std::cout<<"s23="<<2*dot(momenta[1],momenta[2])<<'\n';

        old.setMomenta(momenta);

        amp.setMomenta(momenta);
        amp.initFinRem();
        //amp.setSpecFuncs();
        amp.setSpecFuncs1L();

        std::vector<std::vector<int>> hels{{
            { +1, +1, +1, +1, +1 },
            { +1, +1, +1, +1, -1 },
            { +1, +1, +1, -1, +1 },
            { +1, +1, -1, +1, +1 },
            { +1, -1, +1, +1, +1 },
            { -1, +1, +1, +1, +1 },
            { +1, +1, +1, -1, -1 },
            { +1, +1, -1, -1, +1 },
            { +1, -1, -1, +1, +1 },
            { -1, -1, +1, +1, +1 },
            { +1, +1, -1, +1, -1 },
            { +1, -1, +1, -1, +1 },
            { -1, +1, -1, +1, +1 },
            { +1, -1, +1, +1, -1 },
            { -1, +1, +1, -1, +1 },
            { -1, +1, +1, +1, -1 },
        }};

        std::vector<std::vector<int>> chels(hels.size(), std::vector<int>(hels.front().size()));
	for (std::size_t i { 0 }; i < hels.size(); ++i) {
		std::transform(hels[i].cbegin(), hels[i].cend(), chels[i].begin(), [](int h) -> int { return -h; });
	}

	std::cout << '\n' << "primaries:" << '\n';

        for (std::vector<int> h : hels) {
		for (int i : h){
			std::cout << ((i==1)?'+':'-');
		}
		std::cout << ' ';
		//std::cout << '\n';

		T cf { old.virtsq(h.data()).get0().real() };

		T c1lx1l { amp.c1lx1l_fin(h.data()) };
		//T c2lx1l { amp.c2lx1l_fin(h.data()) };

		std::cout
		    //<< cf/c1lx1l
		    //<< '\n'
		    //<< "new:   " << c1lx1l << '\n'
		    //<< "      old:   " << cf << '\n'
		    << "      ratio: " << cf/c1lx1l << '\n'
		    //<< "1lx1l:       " << c1lx1l << '\n'
		    //<< "2lx1l:       " << c2lx1l << '\n'
		    //<< "2lx1l/1lx1l: " << c2lx1l / c1lx1l << '\n'
		    //<< ' ' << c2lx1l / c1lx1l
		    //<< '\n'
			;
          }

	std::cout << '\n' << "conjugates:" << '\n';
 
        for (std::vector<int> h : chels) {
		for (int i : h){
			std::cout << ((i==1)?'+':'-');
		}
		std::cout << ' ';
		//std::cout << '\n';

		T cf { old.virtsq(h.data()).get0().real() };

		T c1lx1l { amp.c1lx1l_fin(h.data()) };
		//T c2lx1l { amp.c2lx1l_fin(h.data()) };

		std::cout
		    //<< cf/c1lx1l
		    //<< '\n'
		    //<< "new:   " << c1lx1l << '\n'
		    //<< "      old:   " << cf << '\n'
		    << "      ratio: " << cf/c1lx1l << '\n'
		    //<< "1lx1l:       " << c1lx1l << '\n'
		    //<< "2lx1l:       " << c2lx1l << '\n'
		    //<< "2lx1l/1lx1l: " << c2lx1l / c1lx1l << '\n'
		    //<< ' ' << c2lx1l / c1lx1l
		    //<< '\n';
			;
          }
	    std::cout << '\n';
    }
}

int main()
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    //std::cout << "DD" << '\n';
    //run<double, double>();

    std::cout << "QD" << '\n';
    run<dd_real, double>();
}
