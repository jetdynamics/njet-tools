#include <complex>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

#include "chsums/0q3gA.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

using std::abs;

template <typename T>
std::complex<T> minimum(std::complex<T> a, std::complex<T> b, std::complex<T> c)
{
    T ma { std::abs(a) };
    T mb { std::abs(b) };
    T mc { std::abs(c) };

    std::complex<T> m { b };
    T mm { mb };

    if (ma < mb) {
        m = a;
        mm = ma;
    }

    return mc < mm ? c : m;
}

template <typename T, typename P>
void run(const int start, const int end)
{
    const int legs { 5 };
    const int Nc { 3 };
    const int Nf { 5 };
    const std::vector<double> scales2 { { 0 } };
    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    Amp0q3g2A_a2l<T, P> amp(Ax);
    amp.setNf(Nf);
    amp.setNc(Nc);

    std::complex<T> SLC {};
    std::complex<T> SSLC {};
    std::complex<T> SF {};

    const PhysMom::Data<double> data("../collab/points_new.txt");

    for (int p { start }; p <= end; ++p) {
        std::cout << "==================== Test point " << p
                  << " ====================" << '\n';

        std::vector<MOM<T>> momenta(legs);
        for (int i { 0 }; i < legs; ++i) {
            momenta[i] = MOM<T>(data.points[p].momenta[i][0], data.points[p].momenta[i][1],
                data.points[p].momenta[i][2], data.points[p].momenta[i][3]);
        }
        refineM(momenta, momenta, scales2);

        amp.setMuR2(std::pow(data.points[p].mu, 2));

        amp.setMomenta(momenta);
        amp.initFinRem();
        amp.setSpecFuncs();

        std::complex<T> HLC {};
        std::complex<T> HSLC {};
        std::complex<T> HF {};

        for (auto h : Amp0q3gAAStatic::HSarr) {
            for (int i { 0 }; i < legs; ++i) {
                std::cout << (h[i] == 1 ? '+' : '-');
            }
            std::cout << '\n';

            amp.setHelicity(h);

            std::complex<T> AF { amp.AFp()[0].loop };
            std::complex<T> A2LC { amp.A2LCp()[0].loop };
            std::complex<T> A2SLC { amp.A2SLCp()[0].loop };
            std::complex<T> A2F { amp.A2Fp()[0].loop };

            std::complex<T> MLC { T(Nc) * A2LC / AF };
            std::complex<T> MSLC { A2SLC / T(Nc) / AF };
            std::complex<T> MF { T(Nf) * A2F / AF };

            HLC += MLC;
            HSLC += MSLC;
            HF += MF;

            std::cout
                << "AF:         " << std::setw(49) << AF << '\n'
                << "A2LC:  Nc*  " << std::setw(49) << A2LC << std::setw(49) << MLC << '\n'
                << "A2SLC: 1/Nc*" << std::setw(49) << A2SLC << std::setw(49) << MSLC << '\n'
                << "A2F:   Nf*  " << std::setw(49) << A2F << std::setw(49) << MF << '\n';
        }

        HLC /= T(32.);
        HSLC /= T(32.);
        HF /= T(32.);
        std::complex<T> Hmin { minimum(HLC, HSLC, HF) };

        std::cout
            << '\n'
            << "Average over helicities" << '\n'
            << "A2LC:  " << std::setw(49) << HLC << '\n'
            << "A2SLC: " << std::setw(49) << HSLC << '\n'
            << "A2F:   " << std::setw(49) << HF << '\n'
            << '\n'
            << "Ratios" << '\n'
            << "A2LC:  " << std::setw(35) << std::abs(HLC / Hmin) << '\n'
            << "A2SLC: " << std::setw(35) << std::abs(HSLC / Hmin) << '\n'
            << "A2F:   " << std::setw(35) << std::abs(HF / Hmin) << '\n'
            << '\n';

        SLC += HLC;
        SSLC += HSLC;
        SF += HF;
    }

    std::complex<T> Smin { minimum(SLC, SSLC, SF) };

    const int num { end - start + 1 };
    std::cout
        << '\n'
        << "Average over everything (" << num << " points)" << '\n'
        << "A2LC:  " << std::setw(49) << SLC / T(num) << '\n'
        << "A2SLC: " << std::setw(49) << SSLC / T(num) << '\n'
        << "A2F:   " << std::setw(49) << SF / T(num) << '\n'
        << '\n'
        << "Ratios" << '\n'
        << "A2LC:  " << std::setw(35) << std::abs(SLC / Smin) << '\n'
        << "A2SLC: " << std::setw(35) << std::abs(SSLC / Smin) << '\n'
        << "A2F:   " << std::setw(35) << std::abs(SF / Smin) << '\n'
        << '\n';
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 3) {
        std::cerr
            << '\n'
            << "Error: incorrect arguments!"
            << '\n'
            << "       use as ./partials <init rseed (>=0)> <final rseed (inclusive)>"
            << '\n'
            << '\n';
        std::exit(EXIT_FAILURE);
    }

    run<double, double>(std::atoi(argv[1]), std::atoi(argv[2]));
}
