#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

#include "chsums/0q3gA.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

template<typename T>
T minimum(T a, T b, T c) {
	T min { a < b ? a : b };
	if (c < min) min = c;
	return min;
}

template <typename T, typename P>
void run(const int start, const int end)
{
	const int legs { 5 };
	const int Nc { 3 };
	const int Nf { 5 };
	const double mur { 91.188 };
	const std::vector<double> scales2 { { 0 } };
	const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

	Amp0q3g2A_a2l<T, P> amp(Ax);
	amp.setNf(Nf);
	amp.setNc(Nc);
	amp.setMuR2(mur * mur);

	T SLC { };
	T SSLC { };
	T SF { };

	for (int p { start }; p <= end; ++p) {
		std::cout << "==================== Test point " << p
			<< " ====================" << '\n';

		std::vector<MOM<T>> momenta { njet_random_point<T>(p) };
		refineM(momenta, momenta, scales2);

		amp.setMomenta(momenta);
		amp.initFinRem();
		amp.setSpecFuncs();

		T HLC { };
		T HSLC { };
		T HF { };

		for (auto h : Amp0q3gAAStatic::HSarr) {
			for (int i{0}; i < legs; ++i) {
				std::cout << (h[i] == 1 ? '+' : '-');
			}
			std::cout << '\n';

			amp.setHelicity(h);

			std::complex<T> AF { amp.AFp()[0].loop };
			std::complex<T> A2LC { amp.A2LCp()[0].loop };
			std::complex<T> A2SLC { amp.A2SLCp()[0].loop };
			std::complex<T> A2F { amp.A2Fp()[0].loop };

			T MLC { std::abs(3.*A2LC) };
			T MSLC { std::abs(A2SLC/3.) };
			T MF { std::abs(5.*A2F) };

			HLC += MLC;
			HSLC += MSLC;
			HF += MF;

			std::cout
				<< "AF:         " << std::setw(49) << AF << '\n'
				<< "A2LC:  Nc*  " << std::setw(49) << A2LC  << std::setw(35) << MLC << '\n'
				<< "A2SLC: 1/Nc*" << std::setw(49) << A2SLC << std::setw(35) << MSLC << '\n'
				<< "A2F:   Nf*  " << std::setw(49) << A2F   << std::setw(35) << MF << '\n'
				;
		}

		HLC/=32.;
		HSLC/=32.;
		HF/=32.;
		T Hmin{minimum(HLC,HSLC,HF)};

		std::cout
			<< '\n'
			<< "Average over helicities" << '\n'
			<< "A2LC:  " << std::setw(35) << HLC << '\n'
			<< "A2SLC: " << std::setw(35) << HSLC << '\n'
			<< "A2F:   " << std::setw(35) << HF << '\n'
			<< '\n'
			<< "Ratios" << '\n'
			<< "A2LC:  " << std::setw(35) << HLC/Hmin << '\n'
			<< "A2SLC: " << std::setw(35) << HSLC/Hmin << '\n'
			<< "A2F:   " << std::setw(35) << HF/Hmin << '\n'
			<< '\n'
			;

		SLC += HLC;
		SSLC += HSLC;
		SF += HF;
	}

	T Smin{minimum(SLC,SSLC,SF)};

	const int num{end-start+1};
	std::cout
		<< '\n'
		<< "Average over everything" << '\n'
		<< "A2LC:  " << std::setw(35) << SLC/num << '\n'
		<< "A2SLC: " << std::setw(35) << SSLC/num << '\n'
		<< "A2F:   " << std::setw(35) << SF/num << '\n'
		<< '\n'
		<< "Ratios" << '\n'
		<< "A2LC:  " << std::setw(35) << SLC/Smin << '\n'
		<< "A2SLC: " << std::setw(35) << SSLC/Smin << '\n'
		<< "A2F:   " << std::setw(35) << SF/Smin << '\n'
		<< '\n'
		;
}

int main(int argc, char *argv[]) 
{
	std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
	std::cout.precision(16);
	std::cout << '\n';

	if (argc != 3) {
		std::cerr 
			<< '\n' 
			<< "Error: incorrect arguments!" 
			<< '\n'
			<< "       use as ./partials <init rseed (>0)> <final rseed (inclusive)>"
			<< '\n'
			<< '\n'
			;
		std::exit(EXIT_FAILURE);
	}

	run<double, double>(std::atoi(argv[1]), std::atoi(argv[2]));
}
