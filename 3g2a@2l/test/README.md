# test

## analyse-phys

Latest stability plot for 3g2a from `main.py`

## test-stability

Points using NJet uniform random PS from `run.sh`.

## collab

Points using NNLOJET 1L random PS from `run.sh`. 

## consistency

Produce the Euler plot.
