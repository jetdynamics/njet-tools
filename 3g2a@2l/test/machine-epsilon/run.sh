#!/usr/bin/env bash

strt=$1 # 1+
points=$2
cores=$3
quotient=$((points / cores))
remainder=$((points % cores))

first_cores=$((cores - remainder))
first_each=$quotient
first_total=$((first_cores * first_each))
first_start=$strt
first_end=$((strt + first_total - 1))

second_cores=$remainder
second_each=$((quotient + 1))
second_total=$((second_cores * second_each))
second_start=$((first_end + 1))
second_end=$((second_start + second_total - 1))

last_point=$(($strt + $points - 1))

binary=test

log=run.$binary.main.log
echo Requested $points points over $cores cores, starting from random number seed $strt >${log}
echo So get rseeds $strt to $last_point >>${log}
echo With $first_cores cores doing $first_each points each \($first_total points: rseed $first_start to $first_end\) >>${log}
echo And $second_cores cores doing $second_each points each \($second_total points: rseed $second_start to $second_end\) >>${log}
echo Running binary $binary >>${log}
cat ${log}

n=0
for ((i = $first_start; i <= $first_end; i += $first_each)); do
    ./$binary ${i} $((${i} + ${first_each})) >run.$binary.${n}.log 2>run.$binary.${n}.err &
    n=$((n += 1))
done
for ((i = $second_start; i <= $second_end; i += $second_each)); do
    ./$binary ${i} $((${i} + ${second_each})) >run.$binary.${n}.log 2>run.$binary.${n}.err &
    n=$((n += 1))
done
