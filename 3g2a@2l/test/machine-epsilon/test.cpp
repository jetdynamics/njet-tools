#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"
#include "test.h"

void run(const int start, const int end)
{
    const std::vector<double> scales2 { { 0 } };
    const double sqrtS { 10. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 5 };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    Amp0q3g2A_a2l<double, double> amp(Ax);
    amp.setMuR2(mur * mur);
    amp.setNf(Nf);
    amp.setNc(Nc);

    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::vector<MOM<double>> momenta1 { njet_random_point<double>(p, sqrtS) };
        refineM(momenta1, momenta1, scales2);

        // for (const MOM<double>& mom : momenta1) {
        //     std::cout << mom.x0 << ' ' << mom.x1 << ' ' << mom.x2 << ' ' << mom.x3 << '\n';
        // }

        amp.setMomenta(momenta1);
        amp.setSpecFuncs();
        amp.initFinRem();

        // const double born_val1 { amp.c1lx1l_fin() };
        const double virt_val1 { amp.c2lx1l_fin() };

        // {
        //     // std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
        //     // o.setf(std::ios_base::scientific);
        //     // o.precision(16);
        //     // o
        //     std::cout
        //         << p << ' '
        //         << born_val1 << ' '
        //         << virt_val1 << ' '
        //         << '\n';
        // }

        const std::vector<MOM<double>> momenta2 { infinitesimal_perturbation(momenta1) };

        // for (const MOM<double>& mom : momenta2) {
        //     std::cout << mom.x0 << ' ' << mom.x1 << ' ' << mom.x2 << ' ' << mom.x3 << '\n';
        // }

        amp.setMomenta(momenta2);
        amp.setSpecFuncs();
        amp.initFinRem();

        // const double born_val2 { amp.c1lx1l_fin() };
        const double virt_val2 { amp.c2lx1l_fin() };

        // {
        //     // std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
        //     // o.setf(std::ios_base::scientific);
        //     // o.precision(16);
        //     // o
        //     std::cout
        //         << p << ' '
        //         << born_val2 << ' '
        //         << virt_val2 << ' '
        //         << '\n';
        // }

        const double rel_diff { std::abs(virt_val1 - virt_val2) / virt_val1 };

        std::cout << p << ' ' << rel_diff << '\n';
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);

    if (argc != 2 && argc != 3) {
        std::cerr << "Error: run as `./test <initial rseed> <final rseed (exclusive, optional)>`, where rseed is the random number seed for the phase space point generator." << '\n';
        std::exit(EXIT_FAILURE);
    }

    const int start { std::atoi(argv[1]) };

    if (start < 1) {
        std::cerr << "Error: <initial rseed> < 1" << '\n';
        std::exit(EXIT_FAILURE);
    }

    const int end { argc == 3 ? std::atoi(argv[2]) : start + 1 };

    if (end < start) {
        std::cerr << "Error: <final rseed> < <initial rseed>" << '\n';
        std::exit(EXIT_FAILURE);
    }

    run(start, end);
}
