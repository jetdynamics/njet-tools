# test-stability

* To generate data, build with `make test` then run `./run.sh <initial rseed> <total # points> <# parallel jobs to run>`.
* This uses the random uniform PS generator from NJet.
* `post.sh` will `cp` results to public site on IPPP system.
* `grab.sh` will download the 3g2a results from the IPPP public directory to the local machine
* Data for 100k points included.
    * 3g2a data uses PentagonFunctions++ v1.0.
    * 5g data is taken from `../../5g@2l/results/refine.*.csv`, but has had unfinished points removed. It uses PentagonFunctions++ v2.0.
* To analyse data, `./main`.
* Produce summary document with `make notes.pdf`.
