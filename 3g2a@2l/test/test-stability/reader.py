# Python library

import numpy
import pickle
import pathlib


def read(filename):
    if not pathlib.Path(filename).is_file():
        print(f"File {filename} not found")
        exit()
    with open(filename, "r") as filecont:
        data = numpy.genfromtxt(filecont, delimiter=" ")
    return data[data[:, 0].argsort()]


def write(filename, data):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        print(f"File {picklefile} already exists.")
        while True:
            decision = input("Overwrite? [y/n]")
            if decision == "n":
                return None
            elif decision == "y":
                break
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)


def read_pickle(filename):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        with open(picklefile, "rb") as f:
            return pickle.load(f)


# this is slow - how to keep vectorised?
def truncate(data1, data2, data3, number):
    new1 = data1[:number]
    new2 = []
    new3 = []
    for i in new1[:, 0]:
        match2 = data2[data2[:, 0] == i]
        if match2.size > 0:
            new2.append(match2[0])
        match3 = data3[data3[:, 0] == i]
        if match3.size > 0:
            new3.append(match3[0])
    return new1, numpy.array(new2), numpy.array(new3)


def read_proc_trunc(folder, name, number=100000):
    file1 = f"{folder}/{name}.DD.csv"
    file2 = f"{folder}/{name}.QD.csv"
    file3 = f"{folder}/{name}.QQ.csv"
    data1 = read_pickle(file1)
    data2 = read_pickle(file2)
    data3 = read_pickle(file3)
    if any([a is None for a in (data1, data2, data3)]):
        data1 = read(file1)
        data2 = read(file2)
        data3 = read(file3)
        data1, data2, data3 = truncate(data1, data2, data3, number)
        write(file1, data1)
        write(file2, data2)
        write(file3, data3)
    print(f"Length of dataset: {len(data1)}")
    return data1, data2, data3


def read_proc(folder, name="result"):
    data1 = read_memo(f"{folder}/{name}.DD.csv")
    data2 = read_memo(f"{folder}/{name}.QD.csv")
    data3 = read_memo(f"{folder}/{name}.QQ.csv")
    print(f"Length of dataset: {len(data1)}")
    return data1, data2, data3


def read_memo(filename):
    data = read_pickle(filename)
    if data is None:
        data = read(filename)
        write(filename, data)
    return data
