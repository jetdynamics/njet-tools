# Python library

import math, pathlib

import numpy, matplotlib.pyplot

PHI = (1 + math.sqrt(5)) / 2


def init_plots():
    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")


def slug(word):
    return ("_" if word else "") + word.lower().replace(" ", "_")


def ll(boolean):
    return "log" if boolean else "lin"


def write(fig, name, vector=False):
    suffix = "pdf" if vector else "png"
    filename = name + "." + suffix

    if pathlib.Path(filename).is_file():
        print(f"Overwriting existing file {filename}")
    else:
        print(f"Writing new file {filename}")

    if vector:
        fig.savefig(
            filename,
            bbox_inches="tight",
            format=suffix,
        )
    else:
        fig.savefig(
            filename,
            bbox_inches="tight",
            format=suffix,
            dpi=300,
        )


def histo_single(
    data,
    names,
    cutoff=1e-3,
    label="",
    x_min_pow=-16,
    x_max_pow=2,
    num_bins=100,
    leg_loc="best",
    xlog=True,
    ylog=True,
    w=6.4,
    r=3 / 4,
    legend=True,
    xlabel="",
    normalisation=1.0,
    vector=False,
    xexps=False,
):
    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w * r))

    if xlog:
        matplotlib.pyplot.xscale("log")
        bins = numpy.logspace(x_min_pow, x_max_pow, num=num_bins, base=10)
    else:
        bins = num_bins

    for datum, name in zip(data, names):
        ax.hist(
            datum / normalisation,
            bins=bins,
            histtype="step",
            log=ylog,
            label=name,
        )

    if legend:
        ax.legend(loc=leg_loc)

    ax.set_xlabel(xlabel)
    ax.set_ylabel("Frequency")

    if xexps:
        ax.xaxis.set_major_formatter(lambda x, _: -int(math.log10(x)))

    write(fig, f"histo{slug(label)}_{ll(xlog)}{ll(ylog)}_cut_{cutoff}", vector)


def histo_alt(
    data,
    names,
    cutoff=1e-3,
    label="",
    x_min_pow=-16,
    x_max_pow=2,
    num_bins=100,
    leg_loc="best",
    xlog=True,
    ylog=True,
    w=6.4,
    r=3 / 4,
    legend=True,
    xlabel="",
    normalisation=1.0,
    vector=False,
    xexps=False,
    title=True,
    cumulative=False,
):
    if cumulative:
        rescue = numpy.array([])
        for prec in data:
            error = prec[:, 4]
            error = error[error < cutoff]
            rescue = numpy.concatenate((rescue, error))

    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w * r))

    if xlog:
        matplotlib.pyplot.xscale("log")
        bins = numpy.logspace(x_min_pow, x_max_pow, num=num_bins, base=10)
    else:
        bins = num_bins

    for datum, name in zip(data, names):
        ax.hist(
            datum[:, 4] / normalisation,
            bins=bins,
            histtype="step",
            log=ylog,
            label=name,
        )

    if cumulative:
        _, _, patches = ax.hist(
            rescue,
            cumulative=-1,
            bins=bins,
            histtype="step",
            log=ylog,
            label="Cumulative",
            linestyle="dashed",
            color="black",
        )
        patches[0].set_xy(patches[0].get_xy()[1:])

    if legend:
        ax.legend(loc=leg_loc, frameon=False)

    ax.set_xlabel(xlabel)
    ax.set_ylabel("Frequency")

    if xexps:
        ax.xaxis.set_major_formatter(lambda x, _: -int(math.log10(x)))

    ax.set_xticks(
        numpy.logspace(x_min_pow, x_max_pow, num=x_max_pow - x_min_pow + 1, base=10),
        minor=True,
    )
    ax.tick_params(axis="x", which="minor", labelbottom=False)

    ax.axvline(cutoff, color="black", linewidth=1)

    m = 0.05
    chan = r"$gg\to\gamma\gamma g$"
    if title:
        ax.set_title(chan)
    else:
        ax.text(
            x=1 - m,
            y=1 - 2 * m,
            s=chan,
            transform=ax.transAxes,
            ha="right",
            va="top",
        )

    ax.invert_xaxis()

    write(fig, f"stability_ggyyg{slug(label)}", vector)


def histo_pair(
    data_pair,
    names,
    cutoff=1e-3,
    x_min_pow=-16,
    x_max_pow=2,
    num_bins=100,
    leg_loc="upper left",
    xlog=True,
    ylog=True,
    procs=(),
    xlabel="",
    normalisation=1,
    label="",
    text_reloc=False,
    w=6.4,
    r=4 / 3,
    m=0.98,
    vector=False,
):
    fig, axs = matplotlib.pyplot.subplots(
        len(data_pair),
        1,
        sharex=True,
        tight_layout=True,
        figsize=(w, w * r),
    )

    if xlog:
        matplotlib.pyplot.xscale("log")
        bins = numpy.logspace(x_min_pow, x_max_pow, num=num_bins, base=10)
    else:
        bins = num_bins

    ylim = []
    for ax, data, proc in zip(axs, data_pair, procs):
        for datum, name in zip(data, names):
            ax.hist(
                datum / normalisation,
                bins=bins,
                histtype="step",
                log=ylog,
                label=name,
            )
        ax.text(
            (1 - m) if text_reloc else m,
            m,
            proc,
            transform=ax.transAxes,
            ha="left" if text_reloc else "right",
            va="top",
        )
        ax.set_ylabel("Frequency")
        ylim.append(ax.get_ylim()[1])

    for ax in axs:
        ax.set_ylim(top=max(ylim))

    axs[0].legend(loc=leg_loc)
    axs[1].set_xlabel(xlabel)

    write(fig, f"histo{slug(label)}_{ll(xlog)}{ll(ylog)}_cut_{cutoff}", vector)


def bar_timing(
    ax,
    names,
    specf1,
    coeff1,
    specf2,
    coeff2,
    tu="s",
    width=0.4,
    margin=0.01,
    proc1="5g",
    proc2="3g2a",
    logy=False,
    vector=False,
):
    x = numpy.arange(len(names))

    ax.bar(
        x - width / 2 - margin,
        specf1,
        width=width,
        label="Special functions " + proc1,
        log=logy,
    )
    ax.bar(
        x - width / 2 - margin,
        coeff1,
        width=width,
        label="Coefficients " + proc1,
        bottom=specf1,
        log=logy,
    )

    ax.bar(
        x + width / 2 + margin,
        specf2,
        width=width,
        label="Special functions " + proc2,
        log=logy,
    )
    ax.bar(
        x + width / 2 + margin,
        coeff2,
        width=width,
        label="Coefficients " + proc2,
        bottom=specf2,
        log=logy,
    )

    ax.set_xticks(x)
    ax.set_xticklabels(names)
    ax.set_ylabel(f"Time ({tu})")


def bar_timing_inset(
    specfs1_,
    coeffs1_,
    specfs2_,
    coeffs2_,
    tf=1e6,
    tu="s",
    bar_width=0.4,
    margin=0.01,
    proc1="5g",
    proc2="3g2a",
    logy=False,
    n=3,
    name="",
    legend=True,
    title=False,
    vector=False,
):
    names = ("All", "SD/SD", "DD/SD", "DD/DD")
    specf1 = [x / tf for x in specfs1_]
    coeff1 = [x / tf for x in coeffs1_]
    specf2 = [x / tf for x in specfs2_]
    coeff2 = [x / tf for x in coeffs2_]

    fig, ax1 = matplotlib.pyplot.subplots()

    left, bottom, width, height = [0.22, 0.22, 0.45, 0.45]
    ax2 = fig.add_axes([left, bottom, width, height])

    bar_timing(
        ax1,
        names,
        specf1,
        coeff1,
        specf2,
        coeff2,
        tu=tu,
        width=bar_width,
        margin=margin,
        proc1=proc1,
        proc2=proc2,
        logy=logy,
    )
    bar_timing(
        ax2,
        names[:n],
        specf1[:n],
        coeff1[:n],
        specf2[:n],
        coeff2[:n],
        tu=tu,
        width=bar_width,
        margin=margin,
        proc1=proc1,
        proc2=proc2,
        logy=logy,
    )

    ax1.legend(loc="upper left")

    write(fig, f"bar_timing{slug(name)}", vector)
