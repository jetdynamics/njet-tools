#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

#include <qd/qd_real.h>

#include "chsums/NJetAnalytic.h"
#include "ngluon2/Mom.h"

#include "PhaseSpace.hpp"

template <typename T>
void run(const std::vector<std::vector<double>>& input_momenta)
{
    const std::vector<double> scales2 { { 0 } };

    NJetAnalytic<T> an(1., 5);

    std::vector<MOM<T>> momenta { to_njet_momenta<T>(input_momenta) };

    an.setMomenta(momenta.data());

    std::cout << "{" << '\n';
    for (std::size_t i { 0 }; i < momenta.size(); ++i) {
        std::cout << momenta[i] << (i == 4 ? "" : ",") << '\n';
    }
    std::cout << "}" << '\n';

    for (std::size_t i { 0 }; i < momenta.size(); ++i) {
        for (std::size_t j { i + 1 }; j < momenta.size(); ++j) {
            std::cout << "s" << i << j << ": " << an.lS(i, j) << '\n';
        }
    }

    std::cout << "eps5: " << an.eps5() << '\n';
}

int main(int argc, char* argv[])
{
    std::cout << std::scientific << std::setprecision(64);
    std::cout << '\n';

    if (argc != 1) {
        std::cerr << "Error: run as `./test_phys`; accepts no command line arguments." << '\n';
        std::exit(EXIT_FAILURE);
    }

    const std::vector<std::vector<double>> momenta { {
        {
            -51.208668230165337,
            -0.0000000000000000,
            -0.0000000000000000,
            -51.208668230165337,
        },
        {
            -68.915340172015348,
            0.0000000000000000,
            0.0000000000000000,
            68.915340172015348,
        },
        {
            40.335434890866708,
            -26.800795984501626,
            -0.0000000000000000,
            -30.144064796116488,
        },
        {
            50.868262907098710,
            36.648968454328177,
            -23.259992766600654,
            26.521802708659475,
        },
        {
            28.920310604215288,
            -9.8481724698265491,
            23.259992766600654,
            -14.084409854393002,
        },
    } };

    run<qd_real>(momenta);

    std::cout << '\n';
}
