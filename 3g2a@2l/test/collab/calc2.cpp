#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "PhaseSpace.hpp"

using PhysMom::Point;
using PhysMom::Data;
using PhysMom::printMom;

void run(const Data<double>& data, const int start, const int end)
{
    std::cout << "Initialising amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;
    TP t0;
    TP t1;

    const int legs { 5 };
    const std::vector<double> scales2 { { 0 } };
    const int Nc { 3 };
    const int Nf { 5 };

    const double cutoff_errD { 1e-3 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };

    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q3g2A_a2l<double, double>>(Ax) };
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQD { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, double>>(Ax) };
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q3g2A_a2l<dd_real, dd_real>>(Ax) };
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    for (int i { start }; i < end; ++i) {
        std::cout
            << "Evaluating point " << i << " ..." << '\n'
            << "  try coeff(double)+specialFn(double) [f64/f64]..." << '\n';

        std::vector<MOM<double>> psD(legs);
        for (int j { 0 }; j < legs; ++j) {
            psD[j] = MOM<double>(
                data.points[i].momenta[j][0], data.points[i].momenta[j][1],
                data.points[i].momenta[j][2], data.points[i].momenta[j][3]);
        }
        refineM(psD, psD, scales2);

        ampDD->setMuR2(std::pow(data.points[i].mu, 2));
        ampDD->setMomenta(psD);
        ampDD->initFinRem();

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setSpecFuncs();
        t1 = std::chrono::high_resolution_clock::now();
        long int tsfDD { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->c1lx1l_fin();
        ampDD->c2lx1l_fin();
        t1 = std::chrono::high_resolution_clock::now();
        long int tcDD { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

        const double errD { std::abs(ampDD->c2lx1l_fin_error() / ampDD->c2lx1l_fin_value()) };

        {
            std::ofstream o("result.DD." + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific, std::ios_base::floatfield);
            o.precision(16);
            o
                << i << ' '
                << ampDD->c1lx1l_fin_value() << ' '
                << std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) << ' '
                << ampDD->c2lx1l_fin_value() << ' '
                << errD << ' '
                << tsfDD << ' '
                << tcDD
                << '\n';
        }

        if (errD < cutoff_errD) {
            std::cout
                << "    stability test passed (" << tsfDD + tcDD << " us)" << '\n';
        } else {
            std::cout
                << "    stability test failed (" << tsfDD + tcDD << " us)" << '\n'
                << "  try coeff(quad)+specialFn(double) [f128/f64]..." << '\n';

            dd_real errQ;

            std::vector<MOM<dd_real>> psQ(legs);
            for (int j { 0 }; j < legs; ++j) {
                psQ[j] = MOM<dd_real>(
                    data.points[i].momenta[j][0], data.points[i].momenta[j][1],
                    data.points[i].momenta[j][2], data.points[i].momenta[j][3]);
            }
            refineM(psQ, psQ, scales2);

            ampQD->setMuR2(std::pow(data.points[i].mu, 2));
            ampQD->setMomenta(psQ);
            ampQD->initFinRem();

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->copySpecFuncs(ampDD);
            t1 = std::chrono::high_resolution_clock::now();
            long int tsfQD { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->c1lx1l_fin();
            ampQD->c2lx1l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            long int tcQD { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

            errQ = abs(ampQD->c2lx1l_fin_error() / ampQD->c2lx1l_fin_value());

            {
                std::ofstream o("result.QD." + std::to_string(start), std::ios::app);
                o.setf(std::ios_base::scientific, std::ios_base::floatfield);
                o.precision(16);
                o
                    << i << ' '
                    << ampQD->c1lx1l_fin_value() << ' '
                    << abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value()) << ' '
                    << ampQD->c2lx1l_fin_value() << ' '
                    << errQ << ' '
                    << tsfQD << ' '
                    << tcQD
                    << '\n';
            }

            if (errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed (" << tsfQD + tcQD << " us)" << '\n';

            } else {
                std::cout
                    << "    stability test failed (" << tsfQD + tcQD << " us)" << '\n'
                    << "  try coeff(quad)+specialFn(quad) [f128/128]..." << '\n';

                ampQQ->setMuR2(std::pow(data.points[i].mu, 2));
                ampQQ->setMomenta(psQ);
                ampQQ->initFinRem();

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->setSpecFuncs();
                t1 = std::chrono::high_resolution_clock::now();
                long int tsfQQ { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

                t0 = std::chrono::high_resolution_clock::now();
                ampQQ->copyCoeffs(ampQD);
                ampQQ->c1lx1l_fin();
                ampQQ->c2lx1l_fin();
                t1 = std::chrono::high_resolution_clock::now();
                long int tcQQ { std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() };

                errQ = abs(ampQQ->c2lx1l_fin_error() / ampQQ->c2lx1l_fin_value());

                {
                    std::ofstream o("result.QQ." + std::to_string(start), std::ios::app);
                    o.setf(std::ios_base::scientific, std::ios_base::floatfield);
                    o.precision(16);
                    o
                        << i << ' '
                        << ampQQ->c1lx1l_fin_value() << ' '
                        << abs(ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value()) << ' '
                        << ampQQ->c2lx1l_fin_value() << ' '
                        << errQ << ' '
                        << tsfQQ << ' '
                        << tcQQ
                        << '\n';
                }

                if (errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed (" << tsfQQ + tcQQ << " us)" << '\n';
                } else {
                    std::cout
                        << "    stability test failed (" << tsfQQ + tcQQ << " us) [END]" << '\n';
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 3 && argc != 4) {
        std::cerr << "Incorrect arguments!\n";
        std::exit(EXIT_FAILURE);
    }

    const std::string filename { argv[1] };
    const int start { std::atoi(argv[2]) }; // zero indexed
    const int end { argc == 4 ? std::atoi(argv[3]) : start + 1 }; // exclusive
    const Data<double> data(filename);

    std::cout << "Running points " << start << " to " << end - 1 << " inclusive" << '\n';

    if (end > static_cast<int>(data.points.size())) {
        std::cerr << "end is beyond length of momenta!\n";
        std::exit(EXIT_FAILURE);
    }

    run(data, start, end);
}
