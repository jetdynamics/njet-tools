#!/usr/bin/env bash

DIR=~/www/files/research/3g2A2l/phys-fix
mkdir -p $DIR
cat result.DD.* > $DIR/result.DD.csv
cat result.QD.* > $DIR/result.QD.csv
cat result.QQ.* > $DIR/result.QQ.csv
