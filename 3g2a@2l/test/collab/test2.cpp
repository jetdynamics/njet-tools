#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

//#include "chsums/NJetAccuracy.h"
#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
//#include "ngluon2/refine.h"

template <typename T>
struct Point {
    std::vector<std::vector<T>> momenta;
    T prefactor;
    T alpha_s;
    T alpha;
    T mu;
    T bare_me;
    T full_me;
};

template <typename T>
class Data {
public:
    Data(const std::string& filename, const int m_ = 5, const int d_ = 4, const std::string& delim = " ")
        : points()
        , d(d_)
        , m(m_)
        , delimiter(delim)
    {
	std::cout << "Reading data from file `" << filename << "`..." << '\n';

        std::ifstream data(filename);
        std::string line;
        std::vector<std::vector<T>> moms(m, std::vector<T>(d));
        Point<T> point;
        int j { 0 };

        while (std::getline(data, line)) {
            if (j == 0) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.prefactor = std::stod(line);
                ++j;
            } else if (j == 1) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.alpha_s = std::stod(line);
                ++j;
            } else if (j == 2) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.alpha = std::stod(line);
                ++j;
            } else if (j > 2 && j < 3 + m) {
		std::vector<T> mom(d);
                int i { 0 };
                while (i < d) {
                    std::size_t pos { line.find(delimiter) };
                    std::string comp { line.substr(0, pos) };
                    line.erase(0, pos + delimiter.length());
                    if (comp != "") {
                        mom[i++] = (j < 5 ? -1 : +1) * std::stod(comp);
                    }
                }
                moms[j++ - 3] = mom;
            } else if (j == 3 + m) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.mu = std::stod(line);
                ++j;
            } else if (j == m + 4) {
		for (int i { 0 }; i < 2; ++i) {
                    std::size_t pos { line.find(delimiter) };
                    line.erase(0, pos + delimiter.length());
		}
                point.bare_me = std::stod(line);
                ++j;
            } else if (j == m + 5) {
		for (int i { 0 }; i < 2; ++i) {
                    std::size_t pos { line.find(delimiter) };
                    line.erase(0, pos + delimiter.length());
		}
                point.full_me = std::stod(line);
                ++j;
            } else {
                point.momenta = moms;
                points.push_back(point);
                j = 0;
            }
        }

	std::cout << "  Done" << '\n'; 
    };

    std::vector<Point<T>> points;

private:
    const int d;
    const int m;
    const std::string delimiter;
};

template <typename T>
void printMom(const std::vector<std::vector<T>>& pt)
{
    for (const std::vector<T>& mom : pt) {
        for (const T& comp : mom) {
            std::cout << comp << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

template <typename T>
void test(const Data<T>& data, const int start, const int end)
{
    std::cout
        << "Notation:" << '\n'
        << "  ratio    = |a/b|" << '\n'
        // << "  abs diff = |a-b|" << '\n'
        // << "  rel diff = 2*|a-b|/(a+b)" << '\n'
        << '\n';

    const int legs { 5 };
    const int Nc { 3 };
    const int Nf { 5 };
    //const int pow_alpha { 2 };
    //const int pow_alpha_s { 3 };
    //const T alpha { 7.2973525693e-3 };
    //const T mz2 { std::pow(91.1876, 2) };
    //const T alpha_s_mz2 { 0.1179 };
    //const T b0 { (11. * Nc - 2. * Nf) / 3. };
    //const std::vector<int> order{{0,1,3,4,2}}; // NNLOJET(gg->gyy) -> NJet(gg->yyg) WRONG
    //const std::vector<int> order{{0,1,2,4,3}}; // bose symmetry NJET INVARIANT
    //const std::vector<int> order { { 0, 1, 2, 3, 4 } }; // no change: both gg->gyy RIGHT
    const std::vector<T> scales2 { { 0 } }; // no scales
    const Flavour<T> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
    Amp0q3g2A_a2l<T, T> amp(Ax, 1.);
    amp.setNf(Nf);
    amp.setNc(Nc);

    for (int i { start }; i < end; ++i) {
        std::vector<MOM<T>> psp(legs);
        for (int j { 0 }; j < legs; ++j) {
            //psp[order[j]] = MOM<T>(
            psp[j] = MOM<T>(
                data.points[i].momenta[j][0], data.points[i].momenta[j][1],
                data.points[i].momenta[j][2], data.points[i].momenta[j][3]);
        }
        //refineM(psp, psp, scales2);

        //for (const MOM<T>& m : psp) {
        //    std::cout << m << '\n';
        //}

        //const T s12 { 2. * dot(psp[0], psp[1]) };
        //const T alpha_s { 1. / ((1. / alpha_s_mz2) + b0 * std::log(s12 / mz2)) };

        amp.setMomenta(psp);

        //const MOM<T> p2y { psp[3] + psp[4] };
        //const T m2y2 { dot(p2y, p2y) };
        //amp.setMuR2(m2y2);
        amp.setMuR2(std::pow(data.points[i].mu, 2));

        amp.setSpecFuncs1L();
        amp.initFinRem();

        const T bare { amp.c1lx1l_fin() };

	const T other { data.points[i].bare_me * std::pow(11./9., 2) * Nc*(std::pow(Nc, 2)-1) };

        //const T coupling { std::pow(alpha, pow_alpha) * std::pow(alpha_s, pow_alpha_s) };

        //const T res { coupling * bare };

        //const T abs_diff { std::abs(res - data.points[i].me) };
        //const T av { 0.5 * (res + data.points[i].me) };
        //const T rel_diff { abs_diff / av };
        //const T ratio { std::abs(res / data.points[i].me) };

        const T ratio { std::abs(bare / other) };

        std::cout
            //<< "NNLOJET:  " << data.points[i].me << '\n'
            //<< "NJet:     " << bare << '\n'
            << "ratio:    " << ratio << '\n'
            //<< "abs diff: " << abs_diff << '\n'
            //<< "rel diff: " << rel_diff << '\n'
            //<< '\n'
            ;
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 3 && argc != 4) {
        std::cerr << "Incorrect arguments!\n";
        std::exit(EXIT_FAILURE);
    }

    const std::string filename { argv[1] };
    const int start { std::atoi(argv[2]) };
    const int end { argc == 4 ? std::atoi(argv[3]) : start + 1 };
    Data<double> data(filename);

    if (end > static_cast<int>(data.points.size())) {
        std::cerr << "end is beyond length of momenta!\n";
        std::exit(EXIT_FAILURE);
    }

    //for (Point<double> point : data.points) {
    //    printMom(point.momenta);
    //    std::cout
    //        << point.mu << '\n'
    //        << point.me << '\n'
    //        << '\n';
    //}

    test<double>(data, start, end);
}
