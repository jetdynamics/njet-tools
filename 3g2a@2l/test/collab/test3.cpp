#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

int main()
{
    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    const int d { 4 };
    const int m { 5 };
    const std::vector<double> scales2 { { 0. } };

    std::ifstream data("points_timings.txt");
    std::string line;
    std::vector<std::vector<MOM<double>>> points;
    std::vector<MOM<double>> point(m);
    std::vector<double> musqs;
    std::vector<double> times;
    int j { 0 };

    while (std::getline(data, line)) {
        if (line == "") {
            j = 0;
        } else if (j == 0) {
            musqs.push_back(std::stod(line));
            ++j;
        } else if (j > 0 && j < 1 + m) {
            MOM<double> mom;
            int i { 0 };
            while (i < d) {
                std::size_t pos { line.find(',') };
                std::string comp { line.substr(0, pos) };
                line.erase(0, pos + 1);
                if (comp != "") {
		    //double num { (j < 3 ? -1 : +1) * std::stod(comp) };
		    double num { std::stod(comp) };
		    switch (i) {
			case 0:
			    mom.x0 = num;
			    break;
			case 1:
			    mom.x1 = num;
			    break;
			case 2:
			    mom.x2 = num;
			    break;
			case 3:
			    mom.x3 = num;
			    break;
		    }
		    ++i;
                }
            }
            point[j++ - 1] = mom;
        } else if (j == 1 + m) {
            points.push_back(point);
            line.erase(0, 7);
            std::size_t pos { line.find(' ') };
            times.push_back(std::stod(line.substr(0, pos)));
            ++j;
        }
    }

    Amp0q3g2A_a2l<double, double> amp;

    for (std::size_t p { 0 }; p < points.size(); ++p) {
        std::cout << "Point " << p << '\n';
	for (int i{0}; i<m;++i) {
	    std::cout << "p" << i << ": " << points[p][i] << '\n';
	}
	std::cout << "MuR^2: " << musqs[p] << '\n'
	          << "Matteo's time: " << times[p] << "s" << std::endl;

        TP t0 { std::chrono::high_resolution_clock::now() };
        refineM(points[p], points[p], scales2);
        amp.setMomenta(points[p]);
        amp.setMuR2(musqs[p]);
        amp.initFinRem();
        amp.setSpecFuncs();
        double virt { amp.c2lx1l_fin() };
        TP t1 { std::chrono::high_resolution_clock::now() };
        long dur { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

        std::cout 
                  << "My time:       " << static_cast<double>(dur)/1e9 << "s" << '\n'
	          << "Virt: " << virt << '\n'
                  << std::endl;
    }
}
