#!/usr/bin/env bash

# conv-sm.sh
# Use on `*.m` file

IN=$1
L=$2
BASE=${IN%.m}
NAME=$(basename $IN)
OUT=${BASE}.cpp

if $3; then
    P=${NAME:0:1}
else
    P=
fi

if [ "${NAME:2:1}" == "." ]; then
    O=${NAME:1:1}
else
    O=${NAME:1:2}
fi

perl \
    -0777p \
    -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)$||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|NSM\[\{\d+, (\d+)\},\s*(\{.+?\}),\s*\{(.*?)\}\]\s*|m${L}${O}${P}.reserve(std::array<int, \1>(\2));\n\3,\nm${L}${O}${P}.makeCompressed();\n\n|gs;
        s|w\[(\d+)\] ->\s*([\-\d/\s]+),\s*|const T w\1 { \2 };\n|gs;
        s|w\[(\d+)\] ->\s*([\-\d/\s]+)\}\s*,\s*\{\s*|const T w\1 { \2 };\n|gs;
        s|m\[(\d+), (\d+)\] ->\s*(-?)w\[(\d+)\],\s*|m${L}${O}${P}.insert(\1, \2) = \3w\4;\n|gs;
        s|^(const T w\d+ \{.*[ \/])(\d+)([ \/].*\};)$|\1T(\2.)\3|gm;
        s|^(const T w\d+ \{.*[ \/])(\d+)([ \/].*\};)$|\1T(\2.)\3|gm;
        " \
    ${IN} >${OUT}

perl -0777p -e \
    "s|.*NSM\[\{(\d+), (\d+)\},\s*\{.*|, \1, \2|gms" \
    ${IN} >>${BASE}.size
