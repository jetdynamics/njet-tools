#!/usr/bin/env python3

# import sys
import os
from pathlib import Path

dirname = os.path.dirname(__file__)
# filename = os.path.join(dirname, "../../libs/")
# sys.path.insert(0, filename)

from static import uhvs, mhvs, start


def gen_list(partial_name, num, par=""):
    txt = ""
    for i in range(1, num + 1):
        txt += partial_name + str(i) + par
        if i != num:
            txt += ", "
    return txt


def hdr(process, hstr, n, years, b2l, loop, mhv):
    bapp = "2L" if b2l else ""
    txt = start(f"{process}/{process}-2v-{hstr}.h", years)

    txt += f"""#ifndef FINREM_{process.upper()}_2V_{h.upper()}_H
#define FINREM_{process.upper()}_2V_{h.upper()}_H

#include <array>
#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#include \"../../ngluon2/EpsQuintuplet.h\"
#include \"../../ngluon2/utility.h\"

#include \"{process}-2v-hel.h\"

template <typename T>
class Amp{process}_a2v_{hstr} : public HelAmp{process}{bapp}<T> {{
  using Base = HelAmp{process}{bapp}<T>;
public:
  Amp{process}_a2v_{hstr}(const std::vector<std::complex<T>> &sf_);

  using Base::c_2f;
  using Base::c_2lce;
  using Base::c_2lco;
  using Base::c_2slce;
  using Base::c_2slco;
  using Base::c_{loop};
  using Base::coeffVec;
  using Base::f_2f;
  using Base::f_2lc;
  using Base::f_2slc;
  using Base::f_{loop};

"""
    if b2l:
        if mhv:
            for i in range(1, n + 1):
                txt += f"  void fill_m2LC{i}();\n"
            txt += "\n"

            for i in range(1, n + 1):
                txt += f"  void fill_m2SLC{i}();\n"
            txt += "\n"

        txt += "  // }{ 1l\n\n"

    for i in range(1, n + 1):
        txt += f"  using Base::fi_{loop}{i};\n"
    txt += "\n"

    for i in range(1, n + 1):
        txt += f"  using Base::m{loop}{i};\n"

    txt += f"""
  virtual void hA{loop}_coeffs(std::complex<T> x1, std::complex<T> x2,
                               std::complex<T> x3, std::complex<T> x4,
                               std::complex<T> x5) override;

"""

    if b2l:
        txt += "  // }{ 2l\n\n"

        for i in range(1, n + 1):
            txt += f"  using Base::fi_2LC{i};\n"
        txt += "\n"

        for i in range(1, n + 1):
            txt += f"  using Base::m2LC{i}e;\n  using Base::m2LC{i}o;\n"

        txt += """
  virtual void hA2LC_coeffs(std::complex<T> x1, std::complex<T> x2,
                            std::complex<T> x3, std::complex<T> x4,
                            std::complex<T> x5) override;

"""

        for i in range(1, n + 1):
            txt += f"  using Base::fi_2SLC{i};\n"
        txt += "\n"

        for i in range(1, n + 1):
            txt += f"  using Base::m2SLC{i}e;\n  using Base::m2SLC{i}o;\n"

        txt += """
  virtual void hA2SLC_coeffs(std::complex<T> x1, std::complex<T> x2,
                             std::complex<T> x3, std::complex<T> x4,
                             std::complex<T> x5) override;

"""

        for i in range(1, n + 1):
            txt += f"  using Base::fi_2F{i};\n"
        txt += "\n"

        for i in range(1, n + 1):
            txt += f"  using Base::m2F{i};\n"

        txt += """
  virtual void hA2F_coeffs(std::complex<T> x1, std::complex<T> x2,
                             std::complex<T> x3, std::complex<T> x4,
                             std::complex<T> x5) override;

"""

        txt += """
  // }
"""

        if mhv:
            txt += """
protected:
"""

            for i in range(1, n + 1):
                j = 0
                while Path(f"export/{p}-2v-{h}-2lc-o{i}-{j}.cpp").is_file():
                    txt += f"  void fill_m2LC{i}e{j}();\n"
                    j += 1

    txt += f"""
}};

#endif // FINREM_{p.upper()}_2V_{h.upper()}_H"""
    return txt


if __name__ == "__main__":
    p = "0q3g2A"
    PF = 1
    for h in uhvs:
        print(h)
        with open(os.path.join(dirname, f"../export/{p}-2v-{h}.h"), "w") as f:
            f.write(hdr(p, h, PF, 2021, True, "f", False))
    for h in mhvs:
        print(h)
        with open(os.path.join(dirname, f"../export/{p}-2v-{h}.h"), "w") as f:
            f.write(hdr(p, h, PF, 2021, True, "f", True))
