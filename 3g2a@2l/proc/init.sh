#!/usr/bin/env bash

ROOT=/home/ryan/git/njet-tools/3g2a@2l
cd $ROOT
PF=1

SRC1="build//one/"
SRC2LC="build//twoLC/"
SRC2SLC="build//twoSLC/"
SRC2F="build//nf/"
DES="concat/"

NLO=false

mkdir -p ${DES}

order=(
    01234
)

for s in ${SRC1}/h*; do
    h="${s#*//*//h}"

    s1=$s
    s2lc=${SRC2LC}/h$h
    s2slc=${SRC2SLC}/h$h
    s2f=${SRC2F}/h$h

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j == 31 || $j == 30 || $j == 29 || $j == 27 || $j == 23 || $j == 15)); then
        UHV=true
    else
        UHV=false
    fi

    if $UHV; then
        echo $h UHV
    else
        echo $h MHV
    fi

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"
    mkdir -p $d

    SRC_HEL_I="${d}/src_hel.cpp"

    F=$(cat build/**/h${h}/fm.count | sort -n | tail -1)
    F1=$(cat ${SRC1}/h${h}/fm.count)
    F2LC=$(cat ${SRC2LC}/h${h}/fm.count)
    F2SLC=$(cat ${SRC2SLC}/h${h}/fm.count)
    F2F=$(cat ${SRC2F}/h${h}/fm.count)
    Y=$(cat build/**/h${h}/ym.count | sort -n | tail -1)

    Y1=$(cat ${SRC1}/h${h}/ym.count | sort -n | tail -1)
    Y2LC=$(cat ${SRC2LC}/h${h}/ym.count | sort -n | tail -1)
    Y2SLC=$(cat ${SRC2SLC}/h${h}/ym.count | sort -n | tail -1)
    Y2F=$(cat ${SRC2F}/h${h}/ym.count | sort -n | tail -1)

    FZ1=$(cat ${SRC1}/h${h}/fm.tmp)
    FZ2LC=$(cat ${SRC2LC}/h${h}/fm.tmp)
    FZ2SLC=$(cat ${SRC2SLC}/h${h}/fm.tmp)
    FZ2F=$(cat ${SRC2F}/h${h}/fm.tmp)
    YZ1=$(cat ${SRC1}/h${h}/ym.tmp)
    YZ2LC=$(cat ${SRC2LC}/h${h}/ym.tmp)
    YZ2SLC=$(cat ${SRC2SLC}/h${h}/ym.tmp)
    YZ2F=$(cat ${SRC2F}/h${h}/ym.tmp)

    FZ1=$((FZ1 + 1))
    FZ2LC=$((FZ2LC + 1))
    FZ2SLC=$((FZ2SLC + 1))
    FZ2F=$((FZ2F + 1))
    YZ1=$((YZ1 + 1))
    YZ2LC=$((YZ2LC + 1))
    YZ2SLC=$((YZ2SLC + 1))
    YZ2F=$((YZ2F + 1))

    Z=$(cat build/**/h${h}/*.tmp | sort -n | tail -1)
    Z=$((Z + 1))

    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g; s|YYYY|2021|g;" templates/template_start.cpp >${SRC_HEL_I}
    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g;" templates/template_src_hel.cpp >>${SRC_HEL_I}

    # base class initialiser list 1L
    echo ", $F1" >>${SRC_HEL_I}

    # F sparse matrices init
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s1}/${p}${o}.m >>${SRC_HEL_I}
        done
    done

    # F special function indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s1}/Fv.m >>${SRC_HEL_I}

    # F coefficient indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s1}/fv.m >>${SRC_HEL_I}

    # base class initialiser list 2L
    echo ", $F2LC, $F2SLC, $F2F" >>${SRC_HEL_I}

    # 2LC sparse matrices init
    # nb odd is same size so don't need to read
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s2lc}/${p}${o}.m >>${SRC_HEL_I}
        done
    done

    # 2LC special function indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2lc}/Fv.m >>${SRC_HEL_I}

    # 2LC coefficient indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2lc}/fv.m >>${SRC_HEL_I}

    # 2SLC sparse matrices init
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s2slc}/${p}${o}.m >>${SRC_HEL_I}
        done
    done

    # 2SLC special function indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2slc}/Fv.m >>${SRC_HEL_I}

    # 2SLC coefficient indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2slc}/fv.m >>${SRC_HEL_I}

    # 2F sparse matrices init
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|, \1, \2|gms" ${s2f}/${p}${o}.m >>${SRC_HEL_I}
        done
    done

    # 2F special function indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2f}/Fv.m >>${SRC_HEL_I}

    # 2F coefficient indices vector init
    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|, \2|gs;
        " \
        ${s2f}/fv.m >>${SRC_HEL_I}

    # finish init list
    echo -e ") {\n" >>${SRC_HEL_I}

    # resize coeff vectors F
    for ((o = 1; o <= $PF; o++)); do
        perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_f[$((o - 1))].resize(\1);\n|gms" ${s1}/e${o}.m >>${SRC_HEL_I}
    done
    echo >>${SRC_HEL_I}

    # resize coeff vectors 2LC
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_2lc${p}[$((o - 1))].resize(\1);\n|gms" ${s2lc}/${p}${o}.m >>${SRC_HEL_I}
        done
    done
    echo >>${SRC_HEL_I}

    # resize coeff vectors 2SLC
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_2slc${p}[$((o - 1))].resize(\1);\n|gms" ${s2slc}/${p}${o}.m >>${SRC_HEL_I}
        done
    done
    echo >>${SRC_HEL_I}

    # resize coeff vectors 2F
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  c_2f${p}[$((o - 1))].resize(\1);\n|gms" ${s2f}/${p}${o}.m >>${SRC_HEL_I}
        done
    done
    echo >>${SRC_HEL_I}

    # fill F sparse matrix
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            cat ${s1}/${p}${o}.cpp >>${SRC_HEL_I}
        done
    done

    # fill 2LC sparse matrix
    if $UHV; then
        ps=('e' 'o')
        for p in ${ps[@]}; do
            for ((o = 1; o <= $PF; o++)); do
                cat ${s2lc}/${p}${o}.cpp >>${SRC_HEL_I}
            done
        done
    else
        for ((o = 1; o <= $PF; o++)); do
            echo "fill_m2LC${o}();" >>${SRC_HEL_I}
            SRC_HEL_SM="${d}/src_hel_lc_${o}.cpp"
            perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g; s|YYYY|2021|g;" templates/template_start.cpp >${SRC_HEL_SM}
            echo -e "template<typename T>\nvoid Amp0q3g2A_a2v_${h_char}<T>::fill_m2LC${o}() {" >>${SRC_HEL_SM}
            for alt in ${s2lc}/e${o}.cpp_*; do
                n=${alt#${s2lc}/e${o}.cpp_}
                echo "fill_m2LC${o}e${n}();" >>${SRC_HEL_SM}
            done
        done
        for ((o = 1; o <= $PF; o++)); do
            SRC_HEL_SM="${d}/src_hel_lc_${o}.cpp"
            cat ${s2lc}/o${o}.cpp >>${SRC_HEL_SM}
            echo -e "}" >>${SRC_HEL_SM}
            perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g;" templates/template_end.cpp >>${SRC_HEL_SM}
        done
    fi

    # fill 2SLC sparse matrix
    if $UHV; then
        ps=('e' 'o')
        for p in ${ps[@]}; do
            for ((o = 1; o <= $PF; o++)); do
                cat ${s2slc}/${p}${o}.cpp >>${SRC_HEL_I}
            done
        done
    else
        for ((o = 1; o <= $PF; o++)); do
            echo "fill_m2SLC${o}();" >>${SRC_HEL_I}
            SRC_HEL_SM="${d}/src_hel_slc_${o}.cpp"
            perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g; s|YYYY|2021|g;" templates/template_start.cpp >${SRC_HEL_SM}
            echo -e "template<typename T>\nvoid Amp0q3g2A_a2v_${h_char}<T>::fill_m2SLC${o}() {" >>${SRC_HEL_SM}
            cat ${s2slc}/e${o}.cpp >>${SRC_HEL_SM}
        done
        for ((o = 1; o <= $PF; o++)); do
            SRC_HEL_SM="${d}/src_hel_slc_${o}.cpp"
            cat ${s2slc}/o${o}.cpp >>${SRC_HEL_SM}
            echo -e "}" >>${SRC_HEL_SM}
            perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g;" templates/template_end.cpp >>${SRC_HEL_SM}
        done
    fi

    # fill 2F sparse matrix
    ps=('e')
    for p in ${ps[@]}; do
        for ((o = 1; o <= $PF; o++)); do
            cat ${s2f}/${p}${o}.cpp >>${SRC_HEL_I}
        done
    done

    # finish constructor
    echo -e "}\n" >>${SRC_HEL_I}

    # F coefficient monomials
    perl -p -e "s|CCCCC|$h_char|g; s|GGG|f|g; s|PPPPP|0q3g2A|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $Y1> w;" >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $YZ1> u;" >>${SRC_HEL_I}
    perl -p -e "s|Z|u|g; s|y|w|g;" ${s1}/ym.cpp >>${SRC_HEL_I}
    echo -e "\nstd::array<std::complex<T>, $FZ1> v;" >>${SRC_HEL_I}
    perl -p -e "s|f\[|f_f[|g; s|Z|v|g; s|y|w|g;" ${s1}/fm.cpp >>${SRC_HEL_I}
    echo -e "}\n" >>${SRC_HEL_I}

    # 2LC coefficient monomials
    if $UHV; then
        perl -p -e "s|CCCCC|$h_char|g; s|GGG|2LC|g; s|PPPPP|0q3g2A|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
        echo -e "std::array<std::complex<T>, $Y2LC> w;" >>${SRC_HEL_I}
        echo -e "std::array<std::complex<T>, $YZ2LC> u;" >>${SRC_HEL_I}
        perl -p -e "s|Z|u|g; s|y|w|g;" ${s2lc}/ym.cpp >>${SRC_HEL_I}
        echo -e "\nstd::array<std::complex<T>, $FZ2LC> v;" >>${SRC_HEL_I}
        perl -p -e "s|f\[|f_2lc[|g; s|Z|v|g; s|y|w|g;" ${s2lc}/fm.cpp >>${SRC_HEL_I}
        echo -e "  }\n" >>${SRC_HEL_I}
    else
        SRC_HEL_C2LC="${d}/src_hel_2lc_coeffs.cpp"
        perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g; s|YYYY|2021|g;" templates/template_start.cpp >${SRC_HEL_C2LC}
        perl -p -e "s|CCCCC|$h_char|g; s|GGG|2LC|g; s|PPPPP|0q3g2A|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_C2LC}
        echo -e "std::array<std::complex<T>, $Y2LC> w;" >>${SRC_HEL_C2LC}
        echo -e "std::array<std::complex<T>, $YZ2LC> u;" >>${SRC_HEL_C2LC}
        perl -p -e "s|Z|u|g; s|y|w|g;" ${s2lc}/ym.cpp >>${SRC_HEL_C2LC}
        echo -e "\nstd::array<std::complex<T>, $FZ2LC> v;" >>${SRC_HEL_C2LC}
        perl -p -e "s|f\[|f_2lc[|g; s|Z|v|g; s|y|w|g;" ${s2lc}/fm.cpp >>${SRC_HEL_C2LC}
        echo -e "  }\n" >>${SRC_HEL_C2LC}
        perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g;" templates/template_end.cpp >>${SRC_HEL_C2LC}
    fi

    # 2SLC coefficient monomials
    perl -p -e "s|CCCCC|$h_char|g; s|GGG|2SLC|g; s|PPPPP|0q3g2A|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $Y2SLC> w;" >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $YZ2SLC> u;" >>${SRC_HEL_I}
    perl -p -e "s|Z|u|g; s|y|w|g;" ${s2slc}/ym.cpp >>${SRC_HEL_I}
    echo -e "\nstd::array<std::complex<T>, $FZ2SLC> v;" >>${SRC_HEL_I}
    perl -p -e "s|f\[|f_2slc[|g; s|Z|v|g; s|y|w|g;" ${s2slc}/fm.cpp >>${SRC_HEL_I}
    echo -e "  }\n" >>${SRC_HEL_I}

    # 2F coefficient monomials
    perl -p -e "s|CCCCC|$h_char|g; s|GGG|2F|g; s|PPPPP|0q3g2A|g;" templates/template_coeffs_start.cpp >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $Y2F> w;" >>${SRC_HEL_I}
    echo -e "std::array<std::complex<T>, $YZ2F> u;" >>${SRC_HEL_I}
    perl -p -e "s|Z|u|g; s|y|w|g;" ${s2f}/ym.cpp >>${SRC_HEL_I}
    echo -e "\nstd::array<std::complex<T>, $FZ2F> v;" >>${SRC_HEL_I}
    perl -p -e "s|f\[|f_2f[|g; s|Z|v|g; s|y|w|g;" ${s2f}/fm.cpp >>${SRC_HEL_I}
    echo -e "  }\n" >>${SRC_HEL_I}

    perl -p -e "s|CCCCC|$h_char|g; s|PPPPP|0q3g2A|g;" templates/template_end.cpp >>${SRC_HEL_I}
done
