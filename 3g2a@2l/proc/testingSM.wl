(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/3g2a@2l"];
in="src/";
outpre="build/";
allHels={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};


(* ::Section:: *)
(*Test*)


src=Get[in<>"partials_3g2a_2L_Ncp1_tHV_+-+-+.m"];


entries=src[[3,1,1,2]];
%//Short
el=entries//Length


values=entries/.Rule[a_,b_]->b;
%//Short
unique=DeleteDuplicates[values];
%//Short
ul=unique//Length


ul/el//N


(* ::Section:: *)
(*Proc*)


src1=Get[in<>"partials_3g2a_1L_tHV_-++-+.m"];


sm[src_,par_]:=Module[
	{a=src[[3,1,par]],entries1,entries0,ncols,nrows,nnz},
	entries1=a[[2]]/.{m[i_,j_]:>m[j,i]};
	entries0=entries1/.{m[i_,j_]:>m[i-1,j-1]};
	ncols=a[[1,1]];
	nrows=a[[1,2]];
	nnz=Table[Length[Select[entries1,#[[1,2]]==i&]],{i,ncols}];
	SM[{nrows, ncols},nnz,entries0]
];


newSparseMatrix[src_,par_]:=Module[{sm1,e1,e0,v1,u1,r1,p1,n1,ncols,nrows,nnz},
sm1=src[[3,1,par]];
e1=sm1[[2]]/.{m[i_,j_]:>m[j,i]};
e0=e1/.{m[i_,j_]:>m[i-1,j-1]};
v1=e0/.Rule[a_,b_]->b;
u1=DeleteDuplicates[Abs/@v1];
r1=w[#-1]->u1[[#]]&/@Range[Length[u1]];
p1=r1/.Rule[a_,b_]->Rule[b,a];
p1=Join[p1,p1/.Rule[a_,b_]->Rule[-a,-b]];
n1=e0/.Rule[a_,b_]:>Rule[a,(b/.p1)];
ncols=sm1[[1,1]];
nrows=sm1[[1,2]];
nnz=Table[Length[Select[e1,#[[1,2]]==i&]],{i,ncols}];
NSM[{nrows, ncols},nnz,r1,n1]
]


sm[src1,1]


newSparseMatrix[src1,1]
