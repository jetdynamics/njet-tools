#!/usr/bin/env python3

from pathlib import Path

from static import mhvs

if __name__ == "__main__":
    proc = "0q3g2A"

    txt = "## }{ MHVs\n"

    for hel in mhvs:

        txt += f"""
lib_LTLIBRARIES += libnjet2an{proc}2v{hel}.la
libnjet2an{proc}2v{hel}_la_SOURCES = ../blank-staticfix.cpp
libnjet2an{proc}2v{hel}SD_la_SOURCES = \\
    {proc}-2v-{hel}.cpp \\
    {proc}-2v-{hel}-2slc-o1.cpp \\
    {proc}-2v-{hel}-2lc-coeffs.cpp \\
    {proc}-2v-{hel}-2lc-o1.cpp"""

        i = 0
        while Path(f"export/{proc}-2v-{hel}-2lc-o1-{i}.cpp").is_file():
            txt += f" \\\n    {proc}-2v-{hel}-2lc-o1-{i}.cpp"
            i += 1

        txt += f"""
EXTRA_libnjet2an{proc}2v{hel}SD_la_SOURCES = {proc}-2v-{hel}.h {proc}-2v-hel.h
libnjet2an{proc}2v{hel}SD_la_CPPFLAGS = -DUSE_SD $(EIGEN_CFLAGS)
libnjet2an{proc}2v{hel}SD_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet2an{proc}2v{hel}VC_la_SOURCES = $(libnjet2an{proc}2v{hel}SD_la_SOURCES)
libnjet2an{proc}2v{hel}VC_la_CPPFLAGS = -DUSE_VC $(EIGEN_CFLAGS)
libnjet2an{proc}2v{hel}VC_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet2an{proc}2v{hel}DD_la_SOURCES = $(libnjet2an{proc}2v{hel}SD_la_SOURCES)
libnjet2an{proc}2v{hel}DD_la_CPPFLAGS = -DUSE_DD $(EIGEN_CFLAGS)
libnjet2an{proc}2v{hel}DD_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet2an{proc}2v{hel}QD_la_SOURCES = $(libnjet2an{proc}2v{hel}SD_la_SOURCES)
libnjet2an{proc}2v{hel}QD_la_CPPFLAGS = -DUSE_QD $(EIGEN_CFLAGS)
libnjet2an{proc}2v{hel}QD_la_CXXFLAGS = $(AM_CXXFLAGS) -O0
libnjet2an{proc}2v{hel}_la_LIBADD = libnjet2an{proc}2v{hel}SD.la ../common/libpentagons.la ../common/libfinremhelamp.la
libnjet2an{proc}2v{hel}_la_DEPENDENCIES = libnjet2an{proc}2v{hel}SD.la ../common/libpentagons.la ../common/libfinremhelamp.la
if ENABLE_VC
libnjet2an{proc}2v{hel}_la_LIBADD += libnjet2an{proc}2v{hel}VC.la
libnjet2an{proc}2v{hel}_la_DEPENDENCIES += libnjet2an{proc}2v{hel}VC.la
endif
if ENABLE_DD
libnjet2an{proc}2v{hel}_la_LIBADD += libnjet2an{proc}2v{hel}DD.la
libnjet2an{proc}2v{hel}_la_DEPENDENCIES += libnjet2an{proc}2v{hel}DD.la
endif
if ENABLE_QD
libnjet2an{proc}2v{hel}_la_LIBADD += libnjet2an{proc}2v{hel}QD.la
libnjet2an{proc}2v{hel}_la_DEPENDENCIES += libnjet2an{proc}2v{hel}QD.la
endif
"""

    txt += "\n## }\n"

    with open("Makefile.am.part", "w") as fle:
        fle.write(txt)
