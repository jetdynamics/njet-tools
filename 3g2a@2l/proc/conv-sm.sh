#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
L=$2
BASE=${IN%.m}
NAME=`basename $IN`
C=${BASE}.cpp

if $3; then
    P=${NAME:0:1}
else
    P=
fi

if [ "${NAME:2:1}" == "." ]; then
    O=${NAME:1:1}
else
    O=${NAME:1:2}
fi

perl \
    -0777p \
    -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)$||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|SM\[\{\d+, \d+\},\s*\{.+?\},\s*\{\}\]\s*||gs;
        s|SM\[\{\d+, (\d+)\},\s*(\{.+?\}),\s*\{(.*?)\}\]\s*|m${L}${O}${P}.reserve(std::array<int, \1>(\2));\n\3,\nm${L}${O}${P}.makeCompressed();\n\n|gs;
        s|m\[(\d+), (\d+)\] ->\s*?([\-\d/\s]+),\s*|m${L}${O}${P}.insert(\1, \2) = \3;\n|gs;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        " \
    ${IN} >${C}
