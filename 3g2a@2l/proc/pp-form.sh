#!/usr/bin/env bash

# use on FORM processed file

OUT=$1pp

perl \
    -0777p \
    -e  "
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|pow|njet_pow|g;
        s|fvu(\d+)u(\d+)u(\d+)|p.fv_\1_\2_\3|g;
        s|fvu(\d+)u(\d+)|p.fv_\1_\2|g;
        s|(tc[ir])|p.\1|g;
        s|([fy])(\d+)|\1\[\2\]|g;
        " \
    $1 > $OUT
