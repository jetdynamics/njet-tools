# ROOT="out-one"
# SRC1="${ROOT}/one"
# SRC2="expr-one/two"
# DES="export"

ROOT=/home/ryan/git/njet-tools/3g2a@2l
cd $ROOT
PF=1

SRC="concat/"
DES="export/"

mkdir -p ${DES}

for f in ${SRC}/h*; do
    h_symb=${f#*h}

    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h_symb:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_char+="p"
        else
            h_char+="m"
        fi
    done

    if (( $j == 31 || $j == 30 || $j == 29 || $j == 27 || $j == 23 || $j == 15 )); then
        UHV=true
        MHV=false
    else
        UHV=false
        MHV=true
    fi

    # cp "${f}/hdr_hel.cpp" "${DES}/0q3g2A-2v-${h_char}.h"
    cp "${f}/src_hel.cpp" "${DES}/0q3g2A-2v-${h_char}.cpp"

    if $MHV; then
        cp "${f}/src_hel_2lc_coeffs.cpp" "${DES}/0q3g2A-2v-${h_char}-2lc-coeffs.cpp"

        for ((o = 1; o <= $PF; o++)); do
            cp "${f}/src_hel_lc_${o}.cpp" "${DES}/0q3g2A-2v-${h_char}-2lc-o${o}.cpp"
            cp "${f}/src_hel_slc_${o}.cpp" "${DES}/0q3g2A-2v-${h_char}-2slc-o${o}.cpp"
        done
    fi
done

# cp ${ROOT}/hdr_main.cpp ${DES}/0q3gA-2l-analytic.h.part
# cp ${ROOT}/src_main.cpp ${DES}/0q3gA-2l-analytic.cpp.part
# clang-format -i ${DES}/0q3gA-2l-analytic.cpp.part

# echo "Ignore missing UHV copies"

# echo "For Makefile.am"
# for f in ${SRC2}/h*; do
#     h_symb=${f#*h}

    # h_char=""
    # for i in {0..4}; do
    #     if [ "${h_symb:$i:1}" = "+" ]; then
    #         h_char+="p"
    #     else
    #         h_char+="m"
    #     fi
    # done

    # for o in {1..12}; do
    #     echo "0q3gA-2v-${h_char}-2l-o${o}-analytic.cpp \\"
    # done
# done
