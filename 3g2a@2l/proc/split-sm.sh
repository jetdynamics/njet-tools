#!/usr/bin/env bash

ROOT=/home/ryan/git/njet-tools/3g2a@2l
cd $ROOT

# for f in build/twoLC/h*/e1.cpp; do
#     split $f ${f}_ -a 1 -d -C 3MB
# done

hels() {
    h="${1#build/twoLC/h}"

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    J=$j
    if (($j < 10)); then
        j="0$j"
    fi

    if (($j == 31 || $j == 30 || $j == 29 || $j == 27 || $j == 23 || $j == 15)); then
        UHV=true
        MHV=false
    else
        UHV=false
        MHV=true
    fi

    if $UHV; then
        echo $1 $h_symb $h_char $j UHV $J
    else
        echo $1 $h_symb $h_char $j MHV $J
    fi
}

for d in build/twoLC/h*; do
    hels $d
    if $MHV; then
        for f in $d/e1.cpp_*; do
            n=${f#$d/e1.cpp_}
            # SRC_HEL_SM="concat/h${h_symb}/src_hel_lc_1_${n}.cpp"
            SRC_HEL_SM="export/0q3g2A-2v-${h_char}-2lc-o1-${n}.cpp"
            echo "  " $f $n "->" $SRC_HEL_SM
            # echo "fill_m2LC${o}();" >>${SRC_HEL_I}
            perl -p -e "s|CCCCC|$h_char|g;" templates/template_start.cpp >${SRC_HEL_SM}
            echo -e "template<typename T>\nvoid Amp0q3g2A_a2v_${h_char}<T>::fill_m2LC1e${n}() {" >>${SRC_HEL_SM}
            cat ${f} >>${SRC_HEL_SM}
            echo -e "}" >>${SRC_HEL_SM}
            perl -p -e "s|CCCCC|$h_char|g;" templates/template_end.cpp >>${SRC_HEL_SM}
        done
    fi
done

