#include <complex>
#include <iostream>
#include <vector>

#include "ngluon2/Mom.h"
#include "splitting/Splitting.h"

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n";
    }

    const std::vector<MOM<double>> mom {
        {
            { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, 0.5000000000000000E+03 },
            { 0.5000000000000000E+03, 0.0000000000000000E+00, 0.0000000000000000E+00, -0.5000000000000000E+03 },
            { 0.4585787878854402E+03, 0.1694532203096798E+03, 0.3796536620781987E+03, -0.1935024746502525E+03 },
            { 0.3640666207368177E+03, -0.1832986929319185E+02, -0.3477043013193671E+03, 0.1063496077587081E+03 },
            { 0.1773545913777421E+03, -0.1511233510164880E+03, -0.3194936075883156E+02, 0.8715286689154436E+02 },
        },
    };

#ifdef DEBUG
    for (MOM<double> m : mom) {
        std::cout << m << '\n';
    }
#endif

    const MOM<double> p1 { mom[2] };
    const MOM<double> p2 { mom[3] };
    const MOM<double> p3 { mom[4] };
    // const MOM<double> p12 { p1 + p2 };
    // const double x12_3 { dot(p1, p2) / dot(p12, p3) };
    // const MOM<double> p12t { p12 - x12_3 * p3 };
    // const MOM<double> p3t { (1 + x12_3) * p3 };

    CollinearLimit<double> collim(p1, p2, p3);
    std::complex<double> mpp { collim.get_tree(-1, +1, +1) };

    std::cout << mpp << '\n';

    return 0;
}
