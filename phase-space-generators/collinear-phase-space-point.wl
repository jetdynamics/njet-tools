(* ::Package:: *)

(* ::Section::Closed:: *)
(*Tools*)


Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
dot[x_,y_]:=x.minkowskiMetric.y;
z ={0,0,1};
X=Sqrt[s]/2;
expMom=p[i_,j__]:>p[i]+p[j];
Unprotect[Power];
Format[Power[a_,n_Integer?Positive], CForm] := Distribute[
    ConstantArray[Hold[a],n],
    Hold, List, HoldForm, Times
];
Protect[Power];
rep={
	x[1]->x1,
	x[2]->x2,
	x[3]->x3,
	Cos[\[Alpha]]->ca,
	Cos[\[Beta]]->cb,
	Cos[\[Gamma]]->cg,
	Cos[\[Theta]]->ct,
	Cos[\[Phi]]->cp,
	Cos[\[Delta]]->cd,
	Sin[\[Alpha]]->sa,
	Sin[\[Beta]]->sb,
	Sin[\[Gamma]]->sg,
	Sin[\[Theta]]->st,
	Sin[\[Phi]]->sp,
	Sin[\[Delta]]->sd
	};
sinToCos=Sin[\[Theta]_]->Sqrt[1-Cos[\[Theta]]^2];
njetExport[expr_]:=expr/.rep//CForm;
s[i_,j_]:=dot[p[i],p[j]];


(* ::Section::Closed:: *)
(*4 - point*)


(* ::Subsection::Closed:: *)
(*Canonical*)


(*This doesn't work for a collinear PS point*)
n4=Ry[\[Theta]].z;
q[1]=Prepend[-z,-1];
q[2]=Prepend[z,-1];
q[3]=Prepend[n4,1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X q[3];
p[4]=-Plus@@p/@Range[3]//Factor;
{MatrixForm[p[#]]&/@Range[4]}//TableForm


(* ::Subsection:: *)
(*Recursive*)


(*BROKEN - doesn't make healthy || limit*)
(*0||1 for \[Theta]\[Rule]0*)
Clear[n4];
n4[1]=Rx[\[Theta]].z;
n4[2]=Rx[\[Theta]].Ry[\[Phi]].z;
q[1]=Prepend[z,1];
q[2]=Prepend[n4[1],1];
q[3]=Prepend[n4[2],-1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X q[3];
p[4]=-Plus@@p/@Range[3]//Factor;
{MatrixForm[p[#]]&/@Range[4]}//TableForm


Plus@@p/@Range[4]//Simplify
dot[p[#],p[#]]&/@Range[3]//Simplify


m4=dot[p[4],p[4]]/.sinToCos;
sol=Solve[m4==0,{Cos[\[Phi]]}]
sol=sol[[1]];
Simplify[Cos[\[Phi]]/.sol]
N[%/.\[Theta]->0.0001]


Print[njetExport/@q[#]]&/@Range[3]


(* ::Section:: *)
(*5 - point, fix last energy fraction (all momenta outgoing) - untested*)


q[1]=Prepend[-z,-1];
q[2]=Prepend[z,-1];
n5[1]=Ry[\[Theta]].z;
n5[2]=Rx[\[Alpha]].n5[1];
q[3]=Prepend[n5[1],1];
q[4]=Prepend[n5[2],1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X x[1] q[3];
p[4]=X x[2] q[4];
p[5]=-Plus@@p/@Range[4]//Factor;
{MatrixForm[p[#]]&/@Range[5]}//TableForm


(njetExport/@q[#])&/@Range[4]


Plus@@p/@Range[5]//Simplify
s[#,#]&/@Range[4]//Simplify


sol5=Solve[dot[p[5],p[5]]==0,{x[2]}][[1]]//TrigExpand


njetExport[x[2]/.sol5]


(* ::Section::Closed:: *)
(*5 - point, fix extra angle (3||4 for x1->1) (all momenta outgoing)*)


n5[1]=Ry[\[Theta]].z;
n5[2]=Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].z;
q[1]=Prepend[-z,-1];
q[2]=Prepend[z,-1];
q[3]=Prepend[n5[1],1];
q[4]=Prepend[n5[2],1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X x[1] q[3];
p[4]=X x[2] q[4];
p[5]=-Plus@@p/@Range[4]//Factor;
{MatrixForm[p[#]]&/@Range[5]}//TableForm


(njetExport/@q[#])&/@Range[4]


Plus@@p/@Range[5]//Simplify
s[#,#]&/@Range[4]//Simplify


m5=dot[p[5],p[5]]/.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2];
sol=Solve[m5==0,{Cos[\[Beta]]}][[1]]


njetExport[Cos[\[Beta]]/.sol]


Flatten[Table[S[i,j],{j,1,5},{i,1,j-1}]]
Flatten[Table[Simplify[s[i,j]/.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2]/.sol],{j,1,5},{i,1,j-1}]];
%/.{x[1]->0.6,x[2]->0.7,\[Theta]->\[Pi]/3,\[Alpha]->\[Pi]/5,s->1}


(* ::Section::Closed:: *)
(*5 - point (3||4 for x1->1) (2->3)*)


n5[1]=Ry[\[Theta]].z;
n5[2]=Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].z;
q[1]=Prepend[z,1];
q[2]=Prepend[-z,1];
q[3]=Prepend[n5[1],1];
q[4]=Prepend[n5[2],1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X x[1] q[3];
p[4]=X x[2] q[4];
p[5]=Plus@@p/@Range[2]-Plus@@p/@Range[3, 4]//Factor;
{MatrixForm[p[#]]&/@Range[5]}//TableForm


(njetExport/@q[#])&/@Range[4]


Plus@@(If[#<3,-1,1]*p[#]&/@Range[5])//Simplify
dot[p[#],p[#]]&/@Range[4]//Simplify


m5=dot[p[5],p[5]]/.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2];
sol=Solve[m5==0,{Cos[\[Beta]]}][[1]]


njetExport[Cos[\[Beta]]/.sol]


Flatten[Table[S[i,j],{j,1,5},{i,1,j-1}]]
Flatten[Table[Simplify[s[i,j]/.Sin[\[Beta]]->Sqrt[1-Cos[\[Beta]]^2]/.sol],{j,1,5},{i,1,j-1}]];
%/.{x[1]->0.6,x[2]->0.7,\[Theta]->\[Pi]/3,\[Alpha]->\[Pi]/5,s->1}


(* ::Section::Closed:: *)
(*6 - point, fix last energy fraction (2||3 for \[Beta]->0)*)


n6[1]=Rx[\[Alpha]].z;
n6[2]=Ry[\[Beta]].n6[1];
n6[3]=Rz[\[Gamma]].n6[2];
q[1]=-Prepend[z,1];
q[2]=-Prepend[-z,1];
q[3]=Prepend[n6[1],1];
q[4]=Prepend[n6[2],1];
q[5]=Prepend[n6[3],1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X x[1] q[3];
p[4]=X x[2] q[4];
p[5]=X x[3] q[5];
p[6]=-Plus@@p/@Range[5]//Factor;
{MatrixForm[p[#]]&/@Range[5]}//TableForm
{MatrixForm[p[6]]}//TableForm


(njetExport/@q[#])&/@Range[5]


Plus@@(p[#]&/@Range[6])//Simplify


FullSimplify[dot[p[#],p[#]]]&/@Range[5]


sol6=Solve[dot[p[6],p[6]]==0,{x[3]}][[1]]


njetExport[x[3]/.sol6]


point6tmp1={x[1]->0.1,x[2]->0.2,\[Alpha]->\[Pi]/3,\[Beta]->\[Pi]/5,\[Gamma]->\[Pi]/7,s->1};
finish[tmp_]:=Module[
	{sol},
	sol=Join[sol6/.tmp,tmp];
	Print[x[4]->-(-2+x[1]+x[2]+x[3])/.sol];
	Return[sol];
	];
point61=finish[point6tmp1]


point6tmp2={x[1]->0.1,x[2]->0.3,\[Alpha]->3,\[Beta]->0.5,\[Gamma]->0.9,s->1};
point62=finish[point6tmp2]


num[point_]:=If[#<3,-1,1]*p[#]&/@Range[6]/.point//N;
arrToStr[arr_]:=StringTrim@StringJoin[ToString[#, InputForm] <> "  " & /@ arr];
arrToNewlines[arr_]:=StringJoin[# <> "\n" & /@ arr];
cout[pt_]:=Print[arrToNewlines[arrToStr[num[pt][[#]]]&/@Range[6]]];


cout[point61]


cout[point62]


(* ::Section::Closed:: *)
(*6 - point, fix extra angle (long eval time)*)


n6[1]=Ry[\[Beta]].Rx[\[Alpha]].z;
n6[2]=Rz[\[Gamma]].n6[1];
n6[3]=Rx[\[Delta]].n6[2];
p[1]=-X Prepend[z,1];
p[2]=-X Prepend[-z,1];
p[3]=X x[1] Prepend[n6[1],1];
p[4]=X x[2] Prepend[n6[2],1];
p[5]=X x[3] Prepend[n6[3],1];
p[6]=-Plus@@p/@Range[5]//Factor;
{MatrixForm[p[#]]&/@Range[5]}//TableForm
{MatrixForm[p[6]]}//TableForm


Plus@@(p[#]&/@Range[6])//Simplify
Simplify[dot[p[#],p[#]]]&/@Range[6]
m6=%[[6]]


m6/.Sin[\[Beta]]->Sqrt[1-c\[Beta]];
%/.Cos[\[Beta]]->c\[Beta];
Solve[%==0,{c\[Beta]}]


(* ::Section::Closed:: *)
(*7 - point, fix last energy fraction*)


n6[1]=Rx[\[Alpha]].z;
n6[2]=Ry[\[Beta]].n6[1];
n6[3]=Rz[\[Gamma]].n6[2];
n6[4]=Rx[\[Delta]].n6[3];
q[1]=-Prepend[z,1];
q[2]=-Prepend[-z,1];
q[3]=Prepend[n6[1],1];
q[4]=Prepend[n6[2],1];
q[5]=Prepend[n6[3],1];
q[6]=Prepend[n6[4],1];
p[1]=X q[1];
p[2]=X q[2];
p[3]=X x[1] q[3];
p[4]=X x[2] q[4];
p[5]=X x[3] q[5];
p[6]=X x[4] q[6];
p[7]=-Plus@@p/@Range[6]//Factor;
{MatrixForm[p[#]]&/@Range[6]}//TableForm
{MatrixForm[p[7]]}//TableForm


(njetExport/@q[#])&/@Range[6]


njetExport[q[6][[4]]]


Plus@@(p[#]&/@Range[7])//Simplify


FullSimplify[dot[p[#],p[#]]]&/@Range[6]


sol7=Solve[dot[p[7],p[7]]==0,{x[4]}][[1]]


njetExport[x[4]/.sol7]
