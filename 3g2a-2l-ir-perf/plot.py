#!/usr/bin/env python3

import csv, sqlite3, pathlib, math
import numpy, matplotlib.pyplot, matplotlib.ticker

if __name__ == "__main__":
    folder = pathlib.Path("data")
    folder.mkdir(exist_ok=True)
    file = folder / "data.db"

    if file.is_file():
        print(f"Opening database {file}")
        con = sqlite3.connect(file)
        cur = con.cursor()

    else:
        print(f"Creating database {file}")
        con = sqlite3.connect(file)
        cur = con.cursor()

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS point (
                id INTEGER PRIMARY KEY NOT NULL,
                yij REAL NOT NULL
            ) STRICT
            """
        )

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS prec (
                id INTEGER PRIMARY KEY NOT NULL,
                plain TEXT NOT NULL,
                label TEXT NOT NULL
            ) STRICT
            """
        )

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS eval (
                id INTEGER PRIMARY KEY NOT NULL,
                rseed INTEGER NOT NULL,
                prec_id INTEGER NOT NULL REFERENCES prec (id),
                point_id INTEGER NOT NULL REFERENCES point (id),
                value_1l REAL NOT NULL,
                error_1l REAL NOT NULL,
                value_2l REAL NOT NULL,
                error_2l REAL NOT NULL
            ) STRICT
            """
        )

        for p, l in zip(
            ("f64f64", "f128f64", "f128f128"),
            (
                r"\texttt{f64}/\texttt{f64}",
                r"\texttt{f128}/\texttt{f64}",
                r"\texttt{f128}/\texttt{f128}",
            ),
        ):
            cur.execute(
                """
                INSERT INTO prec (plain, label)
                VALUES (?, ?)
                """,
                (p, l),
            )

        with open(folder / "sijs.1.csv", "r") as f:
            reader = csv.reader(f, delimiter=" ")

            for i, (r,) in enumerate(reader):
                cur.execute(
                    """
                    INSERT INTO point (id, yij)
                    VALUES (?, ?)
                    """,
                    (i, r),
                )

        res = cur.execute(
            """
            SELECT
                id,
                plain
            FROM prec
            ORDER BY id
            """
        )
        precs = res.fetchall()

        for s in range(1, 11):
            for p, pp in precs:
                with open(folder / f"res.{pp}.{s}.csv", "r") as f:
                    reader = csv.reader(f, delimiter=" ")

                    for r in reader:
                        cur.execute(
                            """
                            INSERT INTO eval (rseed, prec_id, point_id, value_1l, error_1l, value_2l, error_2l)
                            VALUES (?, ?, ?, ?, ?, ?, ?)
                            """,
                            (s, p, *r[1:6]),
                        )

        con.commit()

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    w = 6.4
    r = (1 + math.sqrt(5)) / 2

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(w, w / r),
        tight_layout=True,
    )

    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    ax.set_xlabel(r"$s_{34}/s_{12}$")
    ax.set_ylabel(r"$\langle |\mathcal{H}^{(2)}| / \mathcal{H}^{(1)} \rangle$")

    res = cur.execute(
        """
        SELECT
            id,
            label
        FROM prec
        ORDER BY id
        """
    )

    for i, l in res.fetchall():
        res = cur.execute(
            """
            SELECT
                yij,
                val,
                val * (1 - rerr),
                val * (1 + rerr)
            FROM (
                SELECT
                    p.yij,
                    AVG(ABS(e.value_2l) / e.value_1l) AS val,
                    AVG(SQRT(POW(e.error_2l, 2) + POW(e.error_1l, 2))) AS rerr
                FROM eval AS e
                INNER JOIN point AS p ON p.id = e.point_id
                WHERE e.prec_id = ?
                GROUP BY p.id
                HAVING COUNT(rseed) = 10 AND val < 4e4
                ORDER BY p.id
            )
            """,
            (i,),
        )
        raw = res.fetchall()
        x, y, e0, e1 = numpy.array(raw).T

        ax.fill_between(
            x,
            e0,
            e1,
            alpha=0.2,
        )

        ax.plot(x, y, label=l)

    ax.legend(frameon=False)

    res = cur.execute(
        """
        SELECT
            id,
            label
        FROM prec
        ORDER BY id
        """
    )
    y = res.fetchall()

    fig.savefig("3g2a-ir.pdf")

    t = 1e-3
    res = cur.execute(
        """
        SELECT
            p.yij,
            AVG(SQRT(POW(e.error_2l, 2) + POW(e.error_1l, 2))) AS err
        FROM eval AS e
        INNER JOIN point AS p ON p.id = e.point_id
        WHERE e.prec_id = 3
        GROUP BY p.id
        HAVING err >= ?
        ORDER BY p.yij DESC LIMIT 1
        """,
        (t,),
    )
    y, e = res.fetchone()
    print(f"f128/f128 drops below error={t:.1e} at y34={y:.1e} with error={e:.1e}")
