(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[2], f[3], f[4], f[1], f[5], f[6]}, {f[10], f[8], f[9], f[11], f[14], 
   f[12], f[13], f[15], f[17], f[1], f[5], f[6], f[16], f[7]}}, 
 {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 
   23, 24, 25, 26, 27, 28, 29, 30, 31}, {1, 32, 33, 34, 35, 4, 37, 38, 6, 10, 
   39, 40, 12, 14, 41, 42, 43, 44, 45, 46, 22, 48, 24, 25, 49, 28, 50, 30}}, 
 {{MySparseMatrix[{6, 30}, {m[1, 1] -> 2/3, m[2, 4] -> -1/2, m[2, 10] -> 1/2, 
     m[2, 24] -> 1/2, m[3, 4] -> -2/3, m[3, 10] -> 2/3, m[3, 24] -> 2/3, 
     m[4, 1] -> -2/3, m[4, 2] -> -5/3, m[4, 4] -> -2/3, m[4, 5] -> 1, 
     m[4, 7] -> -3/2, m[4, 8] -> 1, m[4, 9] -> -1, m[4, 11] -> -1, 
     m[4, 12] -> 1, m[4, 13] -> -1, m[4, 15] -> -5/3, m[4, 16] -> -1, 
     m[4, 17] -> -1, m[4, 18] -> 1, m[4, 19] -> 1, m[4, 23] -> -5/6, 
     m[4, 24] -> 2/3, m[4, 25] -> -1, m[4, 26] -> 1, m[4, 27] -> -1, 
     m[4, 28] -> 1, m[5, 6] -> 1/2, m[5, 14] -> -1/2, m[5, 17] -> -1, 
     m[5, 19] -> 1, m[5, 21] -> 1, m[5, 22] -> -1, m[5, 23] -> 1/6, 
     m[5, 28] -> 1, m[5, 29] -> -1, m[6, 3] -> -1/2, m[6, 6] -> 1/2, 
     m[6, 11] -> 1, m[6, 12] -> -1, m[6, 20] -> -1, m[6, 21] -> 1, 
     m[6, 23] -> 2/3, m[6, 25] -> 1, m[6, 27] -> 1, m[6, 29] -> -1, 
     m[6, 30] -> -1}], MySparseMatrix[{6, 30}, {}]}, 
  {MySparseMatrix[{14, 28}, {m[1, 1] -> -2/3, m[2, 10] -> 1/2, 
     m[2, 15] -> -1/2, m[3, 10] -> -1/2, m[3, 15] -> 1/2, m[4, 6] -> 2/3, 
     m[4, 10] -> -2/3, m[4, 24] -> -2/3, m[5, 6] -> 1/2, m[5, 10] -> -1/2, 
     m[5, 24] -> -1/2, m[6, 2] -> 1/2, m[6, 10] -> -1/2, m[6, 24] -> -1/2, 
     m[7, 2] -> -1/2, m[7, 10] -> 1/2, m[7, 24] -> 1/2, m[8, 9] -> 1/4, 
     m[8, 14] -> -1/4, m[8, 18] -> -1/2, m[8, 19] -> 1/2, m[8, 21] -> 1/2, 
     m[8, 22] -> -1/2, m[8, 23] -> 1/12, m[8, 27] -> 1/2, m[8, 28] -> -1/2, 
     m[9, 3] -> -1/4, m[9, 9] -> 1/4, m[9, 11] -> 1/2, m[9, 13] -> -1/2, 
     m[9, 20] -> -1/2, m[9, 21] -> 1/2, m[9, 25] -> 1/2, m[9, 28] -> -1/2, 
     m[10, 1] -> 2/3, m[10, 2] -> 5/3, m[10, 4] -> 3/2, m[10, 5] -> -1, 
     m[10, 7] -> -1, m[10, 8] -> 1, m[10, 9] -> 1/2, m[10, 10] -> 2/3, 
     m[10, 11] -> 1, m[10, 12] -> 1, m[10, 13] -> -1, m[10, 14] -> -1/2, 
     m[10, 15] -> 5/3, m[10, 16] -> 1, m[10, 17] -> -1, m[10, 21] -> 1, 
     m[10, 22] -> -1, m[10, 24] -> -19/6, m[10, 25] -> 2, m[10, 26] -> -1, 
     m[10, 28] -> -1, m[11, 1] -> 2/3, m[11, 2] -> 5/3, m[11, 4] -> 3/2, 
     m[11, 5] -> -1, m[11, 7] -> -1, m[11, 8] -> 1, m[11, 9] -> 1/2, 
     m[11, 10] -> 2/3, m[11, 11] -> 1, m[11, 12] -> 1, m[11, 13] -> -1, 
     m[11, 14] -> -1/2, m[11, 15] -> 5/3, m[11, 16] -> 1, m[11, 17] -> -1, 
     m[11, 21] -> 1, m[11, 22] -> -1, m[11, 24] -> -19/6, m[11, 25] -> 2, 
     m[11, 26] -> -1, m[11, 28] -> -1, m[12, 1] -> -2/3, m[12, 2] -> -5/3, 
     m[12, 4] -> -3/2, m[12, 5] -> 1, m[12, 7] -> 1, m[12, 8] -> -1, 
     m[12, 9] -> -1/2, m[12, 10] -> -2/3, m[12, 11] -> -1, m[12, 12] -> -1, 
     m[12, 13] -> 1, m[12, 14] -> 1/2, m[12, 15] -> -5/3, m[12, 16] -> -1, 
     m[12, 17] -> 1, m[12, 21] -> -1, m[12, 22] -> 1, m[12, 24] -> 19/6, 
     m[12, 25] -> -2, m[12, 26] -> 1, m[12, 28] -> 1, m[13, 9] -> -1/4, 
     m[13, 14] -> 1/4, m[13, 18] -> 1/2, m[13, 19] -> -1/2, 
     m[13, 21] -> -1/2, m[13, 22] -> 1/2, m[13, 23] -> -1/12, 
     m[13, 27] -> -1/2, m[13, 28] -> 1/2, m[14, 3] -> -1/4, m[14, 9] -> 1/4, 
     m[14, 11] -> 1/2, m[14, 13] -> -1/2, m[14, 20] -> -1/2, 
     m[14, 21] -> 1/2, m[14, 25] -> 1/2, m[14, 28] -> -1/2}], 
   MySparseMatrix[{14, 28}, {}]}}, {1, pflip}, 
 {f[5] -> (y[1]^2*y[2]^2*y[4])/(y[3]*y[5]), 
  f[3] -> (y[1]^2*y[6])/(y[3]*y[4]) + (y[1]^2*y[7])/(y[3]*y[4]*y[8]), 
  f[6] -> y[1]^2*y[2]*y[4], f[1] -> (y[1]^2*y[2]*y[4])/y[3], 
  f[4] -> (y[1]^2*y[5]^2)/(y[2]*y[4]*y[8]^3) - 
    (y[1]^2*y[9])/(4*y[2]*y[3]*y[4]) - (3*y[1]^2*y[5]*y[10])/
     (4*y[2]*y[3]*y[4]*y[8]^2) + (3*y[1]^2*y[5]*y[11])/
     (4*y[2]*y[3]*y[4]*y[8]), 
  f[2] -> (y[1]^2*y[5]^2)/(y[2]*y[4]*y[8]^2*y[14]) + 
    (y[1]^2*y[12]^2*y[13])/(4*y[2]*y[4]*y[14]) - (y[1]^2*y[5]*y[16]^2*y[17])/
     (4*y[2]*y[3]*y[4]*y[14]^2*y[15]) - (y[1]^2*y[5]*y[18])/
     (4*y[2]*y[3]*y[4]*y[8]*y[14]^2) + (y[1]^2*y[19])/
     (12*y[2]*y[3]*y[4]*y[14]), f[14] -> (y[1]^2*y[7])/(y[3]*y[4]*y[8]) + 
    (y[1]^2*y[20])/(y[3]*y[4]) + (y[1]^2*y[12]*y[13]*y[21])/(y[4]*y[16]), 
  f[15] -> (y[1]^2*y[2]^2*y[22])/(y[3]^2*y[4]), 
  f[13] -> (y[1]^2*y[12]*y[13]*y[21])/(y[4]*y[16]) + 
    (y[1]^2*y[2]^2)/(y[5]*y[23]) - (y[1]^2*y[2]^3*y[4])/(y[5]*y[24]), 
  f[8] -> (y[1]^2*y[2]^2*y[21])/(y[3]^2*y[4]) + (y[1]^2*y[2]*y[14])/y[23] - 
    (y[1]^2*y[2]^3*y[14]*y[21])/(y[3]^2*y[4]*y[25]) - 
    (y[1]^2*y[2]*y[26])/y[27], f[7] -> (y[1]^2*y[28])/y[4], 
  f[11] -> (y[1]^2*y[5]^2)/(y[2]*y[4]*y[8]^3) - (3*y[1]^2*y[12]^2*y[13]^2)/
     (4*y[2]*y[4]*y[16]^2) - (3*y[1]^2*y[11]*y[12]*y[13])/
     (4*y[2]*y[4]*y[16]) - (y[1]^2*y[29])/(4*y[2]*y[3]^2*y[4]) - 
    (3*y[1]^2*y[5]*y[30])/(4*y[2]*y[3]*y[4]*y[8]^2) + 
    (3*y[1]^2*y[31])/(4*y[2]*y[3]^2*y[4]*y[8]), 
  f[16] -> (y[1]^2*y[2]^2*y[32])/(y[3]^3*y[4]), 
  f[10] -> (y[1]^2*y[5]^2)/(y[2]*y[4]*y[8]^2*y[14]) - 
    (3*y[1]^2*y[12]^2*y[13]^2)/(4*y[2]*y[4]*y[14]*y[16]) - 
    (y[1]^2*y[12]*y[13]*y[33])/(4*y[2]*y[4]*y[14]) - 
    (3*y[1]^2*y[2]^3*y[12]*y[14]^2)/(4*y[3]^2*y[4]*y[25]*y[34]) - 
    (y[1]^2*y[16]^2*y[35]*y[36])/(4*y[2]*y[4]*y[14]^2*y[15]*y[34]) - 
    (y[1]^2*y[5]*y[37])/(4*y[2]*y[3]*y[4]*y[8]*y[14]^2) - 
    (y[1]^2*y[38])/(12*y[2]*y[3]^2*y[4]*y[5]*y[14]), 
  f[12] -> (y[1]^2*y[12]^2*y[13]^2)/(y[2]*y[4]*y[16]^2) + 
    (y[1]^2*y[11]*y[12]*y[13])/(y[2]*y[4]*y[16]) + 
    (y[1]^2*y[2]^2)/(y[5]*y[23]) - (y[1]^2*y[2]^3*y[4])/(y[5]*y[24]), 
  f[9] -> (y[1]^2*y[2]*y[14])/y[23] - (y[1]^2*y[2]^4*y[5]*y[14]^2)/
     (y[3]^3*y[4]*y[25]^2) - (y[1]^2*y[2]*y[26])/y[27] - 
    (y[1]^2*y[2]^2*y[39])/(y[3]^3*y[4]) + (y[1]^2*y[2]^3*y[14]*y[40])/
     (y[3]^3*y[4]*y[25]), f[17] -> (y[1]^2*y[11]*y[12])/(y[2]*y[4])}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> 1 + ex[2], y[4] -> ex[3], 
  y[5] -> 1 + ex[3] + ex[2]*ex[3], y[6] -> 1 + 5*ex[3] + ex[2]*ex[3] + 
    4*ex[3]^2, y[7] -> -1 - 5*ex[3] - ex[2]*ex[3] - 4*ex[3]^2 + 
    4*ex[2]^2*ex[3]^2, y[8] -> 1 + ex[4], 
  y[9] -> 1 + ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 2*ex[2]^2*ex[3] + ex[3]^2 + 
    3*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2, 
  y[10] -> 3 + 2*ex[2] + 3*ex[3] + 4*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[11] -> 2 + ex[2] + 2*ex[3] + 2*ex[2]*ex[3], y[12] -> 1 + ex[3], 
  y[13] -> ex[4], y[14] -> ex[5], y[15] -> 1 + ex[4] - ex[5], 
  y[16] -> -1 + ex[5], y[17] -> 2 + 2*ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] - 2*ex[5] + ex[2]*ex[5] - 2*ex[3]*ex[5] - 
    2*ex[2]*ex[3]*ex[5], y[18] -> -2 - 2*ex[2] - 2*ex[3] - 4*ex[2]*ex[3] - 
    2*ex[2]^2*ex[3] + 9*ex[5] + 6*ex[2]*ex[5] + 9*ex[3]*ex[5] + 
    12*ex[2]*ex[3]*ex[5] + 3*ex[2]^2*ex[3]*ex[5], 
  y[19] -> 3 + 3*ex[2] + 6*ex[3] - 6*ex[2]^2*ex[3] + 3*ex[3]^2 - 
    3*ex[2]*ex[3]^2 - 6*ex[2]^2*ex[3]^2 + 6*ex[5] - 3*ex[2]*ex[5] + 
    12*ex[3]*ex[5] + 9*ex[2]*ex[3]*ex[5] - 3*ex[2]^2*ex[3]*ex[5] + 
    6*ex[3]^2*ex[5] + 12*ex[2]*ex[3]^2*ex[5] + 44*ex[2]^2*ex[3]^2*ex[5], 
  y[20] -> 1 + 5*ex[3] + ex[2]*ex[3] + ex[2]^2*ex[3] + 4*ex[3]^2, 
  y[21] -> 1 + 4*ex[3], y[22] -> 1 + 4*ex[3] + 2*ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[23] -> ex[2] - ex[4] + ex[5], y[24] -> -ex[2] - ex[2]*ex[3] + ex[4] + 
    ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[25] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[26] -> 1 + ex[3]*ex[5], 
  y[27] -> 1 + ex[2]*ex[3] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[28] -> -1 - 5*ex[3] - 4*ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[29] -> 13 + 32*ex[2] + 25*ex[2]^2 + 26*ex[3] + 72*ex[2]*ex[3] + 
    66*ex[2]^2*ex[3] + 17*ex[2]^3*ex[3] + 3*ex[2]^4*ex[3] + 13*ex[3]^2 + 
    40*ex[2]*ex[3]^2 + 42*ex[2]^2*ex[3]^2 + 19*ex[2]^3*ex[3]^2 + 
    4*ex[2]^4*ex[3]^2, y[30] -> 5 + 6*ex[2] + 5*ex[3] + 8*ex[2]*ex[3] + 
    3*ex[2]^2*ex[3], y[31] -> 8 + 19*ex[2] + 13*ex[2]^2 + 16*ex[3] + 
    45*ex[2]*ex[3] + 42*ex[2]^2*ex[3] + 13*ex[2]^3*ex[3] + 8*ex[3]^2 + 
    26*ex[2]*ex[3]^2 + 30*ex[2]^2*ex[3]^2 + 14*ex[2]^3*ex[3]^2 + 
    2*ex[2]^4*ex[3]^2, y[32] -> -1 + ex[2], 
  y[33] -> 5 + 5*ex[3] + 6*ex[2]*ex[3], y[34] -> 1 + ex[2] - ex[5], 
  y[35] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[36] -> 2 + 2*ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 2*ex[2]^2*ex[3] - 
    2*ex[5] - 3*ex[2]*ex[5] - 2*ex[3]*ex[5] - 2*ex[2]*ex[3]*ex[5], 
  y[37] -> -2 - 2*ex[2] - 2*ex[3] - 4*ex[2]*ex[3] - 2*ex[2]^2*ex[3] + 
    15*ex[5] + 18*ex[2]*ex[5] + 15*ex[3]*ex[5] + 24*ex[2]*ex[3]*ex[5] + 
    9*ex[2]^2*ex[3]*ex[5], y[38] -> -21 - 42*ex[2] - 21*ex[2]^2 - 63*ex[3] - 
    177*ex[2]*ex[3] - 165*ex[2]^2*ex[3] - 51*ex[2]^3*ex[3] - 63*ex[3]^2 - 
    228*ex[2]*ex[3]^2 - 306*ex[2]^2*ex[3]^2 - 180*ex[2]^3*ex[3]^2 - 
    39*ex[2]^4*ex[3]^2 - 21*ex[3]^3 - 93*ex[2]*ex[3]^3 - 
    162*ex[2]^2*ex[3]^3 - 138*ex[2]^3*ex[3]^3 - 57*ex[2]^4*ex[3]^3 - 
    9*ex[2]^5*ex[3]^3 - 6*ex[5] - 21*ex[2]*ex[5] - 24*ex[2]^2*ex[5] - 
    18*ex[3]*ex[5] - 66*ex[2]*ex[3]*ex[5] - 87*ex[2]^2*ex[3]*ex[5] - 
    30*ex[2]^3*ex[3]*ex[5] - 18*ex[3]^2*ex[5] - 69*ex[2]*ex[3]^2*ex[5] - 
    99*ex[2]^2*ex[3]^2*ex[5] - 54*ex[2]^3*ex[3]^2*ex[5] - 
    6*ex[2]^4*ex[3]^2*ex[5] - 6*ex[3]^3*ex[5] - 24*ex[2]*ex[3]^3*ex[5] - 
    36*ex[2]^2*ex[3]^3*ex[5] + 14*ex[2]^3*ex[3]^3*ex[5] + 
    70*ex[2]^4*ex[3]^3*ex[5] + 38*ex[2]^5*ex[3]^3*ex[5], 
  y[39] -> 2 - ex[2] + ex[3] + ex[2]*ex[3], 
  y[40] -> 3 - ex[2] + 2*ex[3] + 2*ex[2]*ex[3]}}
