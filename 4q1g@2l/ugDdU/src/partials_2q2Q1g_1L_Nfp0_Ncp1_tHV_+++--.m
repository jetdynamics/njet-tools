(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[8], f[5], f[6], f[7], f[4], f[9], f[2], f[11], f[10], f[12], 
   f[1]}, {f[14], f[16], f[15], f[13], f[1], f[2], f[17]}}, 
 {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 
   22, 23, 24, 25, 26, 27, 28, 29}, {1, 30, 31, 32, 33, 34, 2, 35, 36, 3, 37, 
   38, 39, 40, 4, 41, 42, 5, 43, 44, 45, 19, 22, 23, 46, 47, 24, 29}}, 
 {{MySparseMatrix[{12, 29}, {m[1, 1] -> 1/18, m[2, 2] -> 1/2, 
     m[2, 14] -> -1/2, m[2, 23] -> -1/2, m[3, 2] -> 1/2, m[3, 10] -> -1/2, 
     m[3, 23] -> -1/2, m[4, 2] -> -1/2, m[4, 4] -> 1/2, m[4, 23] -> 1/2, 
     m[5, 2] -> -2/3, m[5, 4] -> 2/3, m[5, 23] -> 2/3, m[6, 2] -> -1/2, 
     m[6, 10] -> 1/2, m[6, 23] -> 1/2, m[7, 2] -> -1/2, m[7, 14] -> 1/2, 
     m[7, 23] -> 1/2, m[8, 1] -> -1/3, m[8, 2] -> -1/3, m[8, 5] -> 1/2, 
     m[8, 7] -> -3/4, m[8, 8] -> -1/2, m[8, 9] -> -1/2, m[8, 10] -> -5/6, 
     m[8, 11] -> -1/2, m[8, 12] -> 1/2, m[8, 13] -> 1/2, m[8, 14] -> -5/6, 
     m[8, 15] -> 1/2, m[8, 16] -> -1/2, m[8, 17] -> 1/2, m[8, 18] -> -1/2, 
     m[8, 22] -> -5/12, m[8, 23] -> 1/3, m[8, 24] -> -1/2, m[8, 25] -> 1/2, 
     m[8, 26] -> 1/2, m[8, 27] -> -1/2, m[9, 3] -> 1/4, m[9, 6] -> -1/4, 
     m[9, 11] -> -1/2, m[9, 12] -> 1/2, m[9, 19] -> 1/2, m[9, 20] -> -1/2, 
     m[9, 22] -> 1/12, m[9, 26] -> 1/2, m[9, 29] -> -1/2, m[10, 3] -> 1/4, 
     m[10, 6] -> -1/4, m[10, 11] -> -1/2, m[10, 12] -> 1/2, m[10, 19] -> 1/2, 
     m[10, 20] -> -1/2, m[10, 22] -> 1/12, m[10, 26] -> 1/2, 
     m[10, 29] -> -1/2, m[11, 3] -> 1/2, m[11, 5] -> -1/2, m[11, 15] -> -1/2, 
     m[11, 16] -> 1/2, m[11, 19] -> 1/2, m[11, 21] -> 1/2, m[11, 22] -> 5/12, 
     m[11, 24] -> 1/2, m[11, 27] -> 1/2, m[11, 28] -> -1/2, 
     m[11, 29] -> -1/2, m[12, 1] -> -1/3, m[12, 2] -> -1/3, m[12, 3] -> 1/2, 
     m[12, 7] -> -3/4, m[12, 8] -> -1/2, m[12, 9] -> -1/2, m[12, 10] -> -5/6, 
     m[12, 11] -> -1/2, m[12, 12] -> 1/2, m[12, 13] -> 1/2, 
     m[12, 14] -> -5/6, m[12, 17] -> 1/2, m[12, 18] -> -1/2, 
     m[12, 19] -> 1/2, m[12, 21] -> 1/2, m[12, 23] -> 1/3, m[12, 25] -> 1/2, 
     m[12, 26] -> 1/2, m[12, 28] -> -1/2, m[12, 29] -> -1/2}], 
   MySparseMatrix[{12, 29}, {}]}, 
  {MySparseMatrix[{7, 28}, {m[1, 1] -> 2/3, m[2, 7] -> 2/3, m[2, 15] -> -2/3, 
     m[2, 24] -> -2/3, m[3, 7] -> 1/2, m[3, 15] -> -1/2, m[3, 24] -> -1/2, 
     m[4, 1] -> 2/3, m[4, 2] -> 5/3, m[4, 4] -> 5/3, m[4, 5] -> 1, 
     m[4, 6] -> 1/2, m[4, 8] -> -1, m[4, 10] -> 1/2, m[4, 11] -> 3/2, 
     m[4, 12] -> -1, m[4, 13] -> -1, m[4, 14] -> 1, m[4, 15] -> 2/3, 
     m[4, 16] -> 1, m[4, 17] -> -1, m[4, 18] -> -1, m[4, 19] -> 1, 
     m[4, 21] -> 1, m[4, 22] -> 1, m[4, 24] -> -19/6, m[4, 25] -> 2, 
     m[4, 27] -> -1, m[4, 28] -> -1, m[5, 3] -> -1/4, m[5, 10] -> 1/4, 
     m[5, 16] -> 1/2, m[5, 18] -> -1/2, m[5, 20] -> -1/2, m[5, 22] -> 1/2, 
     m[5, 25] -> 1/2, m[5, 28] -> -1/2, m[6, 3] -> -1/4, m[6, 10] -> 1/4, 
     m[6, 16] -> 1/2, m[6, 18] -> -1/2, m[6, 20] -> -1/2, m[6, 22] -> 1/2, 
     m[6, 25] -> 1/2, m[6, 28] -> -1/2, m[7, 3] -> 1/2, m[7, 6] -> -1/2, 
     m[7, 9] -> 1, m[7, 10] -> -1, m[7, 16] -> -1, m[7, 18] -> 1, 
     m[7, 20] -> 1, m[7, 21] -> -1, m[7, 22] -> -2, m[7, 23] -> -1/6, 
     m[7, 25] -> -1, m[7, 26] -> -1, m[7, 28] -> 2}], 
   MySparseMatrix[{7, 28}, {}]}}, {1, pflip}, 
 {f[10] -> y[4]/(y[1]^2*y[2]*y[3]^2), 
  f[6] -> y[7]/(y[2]*y[3]^2*y[5]*y[6]) + y[8]/(y[3]*y[6]*y[9]) - 
    (y[10]*y[11]*y[12])/(y[3]*y[5]*y[13]), 
  f[4] -> (y[10]*y[11]*y[12])/(y[3]*y[5]*y[13]) + 
    (y[10]*y[14])/(y[1]*y[3]*y[6]*y[15]) + (y[1]*y[17])/(y[3]*y[6]^2*y[16]) - 
    y[18]/(y[1]*y[3]^2*y[5]*y[6]^2*y[15]), 
  f[8] -> y[14]/(y[1]*y[2]*y[3]*y[15]) - y[19]/(y[2]^2*y[3]*y[10]*y[15]) + 
    (y[1]*y[20])/(y[2]^2*y[3]*y[10]*y[21]) + y[11]/(y[3]*y[22]), 
  f[1] -> -(y[23]/(y[3]*y[6])), f[2] -> y[24]/(y[2]*y[3]^2), 
  f[11] -> (y[10]*y[25])/(y[1]^3*y[3]^2), 
  f[7] -> (y[1]^3*y[2]^2*y[3])/(y[6]*y[9]^3) + (3*y[1]*y[2]*y[10]^2*y[12]^2)/
     (4*y[5]^2*y[13]^2) + y[26]/(4*y[1]*y[2]*y[3]^2*y[5]^2*y[6]) - 
    (3*y[1]*y[2]*y[27])/(4*y[6]*y[9]^2) + (3*y[28])/(4*y[1]*y[3]*y[9]) - 
    (3*y[10]*y[12]*y[29])/(4*y[1]*y[3]*y[5]^2*y[13]), 
  f[3] -> y[30]/(y[1]*y[2]*y[3]^2*y[5]*y[6]) - (9*y[1])/(y[22]*y[31]) - 
    (9*y[10]^2*y[12]^2)/(y[3]*y[5]*y[13]*y[32]) - 
    (3*y[1]*y[33]*y[35])/(y[3]*y[32]*y[34]*y[36]^2) - 
    (12*y[1]^2*y[2]^2)/(y[9]^2*y[36]) + (3*y[1]*y[12]^2*y[38])/
     (y[3]*y[31]*y[36]^2*y[37]) + (3*y[2]*y[39])/(y[3]*y[9]*y[36]^2), 
  f[5] -> (y[1]*y[2]*y[10]^2*y[12]^2)/(y[5]^2*y[13]^2) + 
    (y[10]*y[14])/(y[1]*y[3]*y[6]*y[15]) + (y[1]*y[17])/(y[3]*y[6]^2*y[16]) - 
    (y[10]*y[12]*y[29])/(y[1]*y[3]*y[5]^2*y[13]) - 
    y[40]/(y[1]^2*y[3]^2*y[5]^2*y[6]^2*y[15]), 
  f[9] -> y[14]/(y[1]*y[2]*y[3]*y[15]) - y[19]/(y[2]^2*y[3]*y[10]*y[15]) + 
    (y[1]*y[20])/(y[2]^2*y[3]*y[10]*y[21]) - y[6]/(y[3]*y[22]^2) + 
    y[41]/y[22], f[12] -> -1 - 2*y[1], f[13] -> 1/(y[1]*y[3]^2), 
  f[15] -> y[8]/(y[3]*y[6]*y[9]) - y[42]/(y[1]*y[3]^2*y[6]), 
  f[17] -> 1/(y[2]*y[3]^2), f[16] -> (y[1]^3*y[2]^2*y[3])/(y[6]*y[9]^3) + 
    y[43]/(4*y[1]*y[3]^2*y[6]) + (3*y[1]*y[2]*y[44])/(4*y[6]*y[9]^2) - 
    (3*y[2]*y[45])/(4*y[6]*y[9]), f[14] -> (y[1]^2*y[2]^2)/(y[9]^2*y[36]) - 
    y[46]/(12*y[1]*y[3]^2*y[10]) + (y[2]*y[47])/(4*y[3]*y[34]*y[36]^2) + 
    (y[1]*y[6]*y[12]^2*y[48])/(4*y[3]*y[10]*y[36]^2*y[37]) + 
    (y[2]*y[49])/(4*y[3]*y[9]*y[36]^2)}, {y[1] -> ex[2], y[2] -> 1 + ex[2], 
  y[3] -> ex[3], y[4] -> 4 + 6*ex[2] + 5*ex[3] + 5*ex[2]*ex[3] + ex[3]^2 + 
    ex[2]*ex[3]^2, y[5] -> 1 + ex[2] + ex[3], 
  y[6] -> 1 + ex[3] + ex[2]*ex[3], y[7] -> -4 - 4*ex[2] + 5*ex[3]^2 + 
    4*ex[2]*ex[3]^2 + ex[3]^3 + ex[2]*ex[3]^3, 
  y[8] -> 4 + 8*ex[2] + 5*ex[3] + 9*ex[2]*ex[3] + 4*ex[2]^2*ex[3] + ex[3]^2 + 
    2*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[9] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4], y[10] -> 1 + ex[3], y[11] -> 4 + ex[3], 
  y[12] -> -1 + ex[5], y[13] -> -(ex[2]*ex[3]) + ex[4] + ex[2]*ex[4] + 
    ex[3]*ex[4] + ex[2]*ex[3]*ex[5], y[14] -> ex[4], y[15] -> ex[5], 
  y[16] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] - ex[2]*ex[5], 
  y[17] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[18] -> ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2 + 2*ex[2]*ex[3]^3 + 
    2*ex[2]^2*ex[3]^3 + ex[2]^3*ex[3]^3 + ex[2]*ex[3]^4 + ex[2]^2*ex[3]^4 + 
    4*ex[5] + 13*ex[3]*ex[5] + 9*ex[2]*ex[3]*ex[5] + ex[2]^2*ex[3]*ex[5] + 
    15*ex[3]^2*ex[5] + 20*ex[2]*ex[3]^2*ex[5] + 5*ex[2]^2*ex[3]^2*ex[5] + 
    7*ex[3]^3*ex[5] + 13*ex[2]*ex[3]^3*ex[5] + 5*ex[2]^2*ex[3]^3*ex[5] + 
    ex[3]^4*ex[5] + 2*ex[2]*ex[3]^4*ex[5] + ex[2]^2*ex[3]^4*ex[5], 
  y[19] -> 1 + ex[2] + ex[3] + ex[2]*ex[3] - ex[5], 
  y[20] -> 1 + ex[2] - ex[5], y[21] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[2]*ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[5], 
  y[22] -> 1 + ex[2]*ex[3] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[23] -> 6 + 5*ex[3] + 4*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[24] -> 2 + 4*ex[3] + 4*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[25] -> 2 + 2*ex[3] + ex[2]*ex[3], 
  y[26] -> -ex[2] - 2*ex[2]^2 - ex[2]^3 + 6*ex[3] + 10*ex[2]*ex[3] + 
    4*ex[2]^2*ex[3] + 18*ex[3]^2 + 32*ex[2]*ex[3]^2 + 24*ex[2]^2*ex[3]^2 + 
    6*ex[2]^3*ex[3]^2 + 18*ex[3]^3 + 30*ex[2]*ex[3]^3 + 21*ex[2]^2*ex[3]^3 + 
    6*ex[2]^3*ex[3]^3 + 6*ex[3]^4 + 9*ex[2]*ex[3]^4 + 3*ex[2]^2*ex[3]^4, 
  y[27] -> -1 + 2*ex[2] - ex[3] + ex[2]^2*ex[3], 
  y[28] -> 2 + 2*ex[3] + ex[2]*ex[3] + ex[2]^2*ex[3] + 2*ex[2]^3*ex[3], 
  y[29] -> 2 + 4*ex[2] + 2*ex[2]^2 + 4*ex[3] + 5*ex[2]*ex[3] + 
    3*ex[2]^2*ex[3] + 2*ex[3]^2 + ex[2]*ex[3]^2, 
  y[30] -> 9 + 47*ex[2] + 38*ex[2]^2 + 18*ex[3] + 65*ex[2]*ex[3] + 
    9*ex[2]^2*ex[3] + 9*ex[3]^2 + 18*ex[2]*ex[3]^2 + 9*ex[2]^2*ex[3]^2, 
  y[31] -> 1 + ex[3]*ex[5], y[32] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[33] -> ex[2] + ex[5], y[34] -> ex[2] - ex[4] + ex[5], 
  y[35] -> -ex[2] + 2*ex[2]^2 - ex[5] + 2*ex[2]*ex[5] - ex[3]*ex[5] - 
    ex[2]*ex[3]*ex[5], y[36] -> ex[2] + ex[5] + ex[3]*ex[5] + 
    ex[2]*ex[3]*ex[5], y[37] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[3]*ex[5], y[38] -> -2 - 2*ex[3] + ex[2]*ex[3] + ex[3]*ex[5] + 
    ex[3]^2*ex[5] + ex[2]*ex[3]^2*ex[5], 
  y[39] -> -5*ex[2] + 4*ex[2]^2 - 5*ex[2]*ex[3] + 5*ex[2]^3*ex[3] - 3*ex[5] + 
    6*ex[2]*ex[5] - 6*ex[3]*ex[5] + 3*ex[2]*ex[3]*ex[5] + 
    9*ex[2]^2*ex[3]*ex[5] - 3*ex[3]^2*ex[5] - 3*ex[2]*ex[3]^2*ex[5] + 
    3*ex[2]^2*ex[3]^2*ex[5] + 3*ex[2]^3*ex[3]^2*ex[5], 
  y[40] -> ex[2]^2*ex[3]^2 + 2*ex[2]^3*ex[3]^2 + ex[2]^4*ex[3]^2 + 
    3*ex[2]^2*ex[3]^3 + 5*ex[2]^3*ex[3]^3 + 3*ex[2]^4*ex[3]^3 + 
    ex[2]^5*ex[3]^3 + 3*ex[2]^2*ex[3]^4 + 4*ex[2]^3*ex[3]^4 + 
    2*ex[2]^4*ex[3]^4 + ex[2]^2*ex[3]^5 + ex[2]^3*ex[3]^5 - 2*ex[5] - 
    3*ex[2]*ex[5] - ex[2]^2*ex[5] - 10*ex[3]*ex[5] - 17*ex[2]*ex[3]*ex[5] - 
    10*ex[2]^2*ex[3]*ex[5] + ex[2]^4*ex[3]*ex[5] - 20*ex[3]^2*ex[5] - 
    38*ex[2]*ex[3]^2*ex[5] - 28*ex[2]^2*ex[3]^2*ex[5] - 
    7*ex[2]^3*ex[3]^2*ex[5] - 20*ex[3]^3*ex[5] - 42*ex[2]*ex[3]^3*ex[5] - 
    34*ex[2]^2*ex[3]^3*ex[5] - 15*ex[2]^3*ex[3]^3*ex[5] - 
    3*ex[2]^4*ex[3]^3*ex[5] - 10*ex[3]^4*ex[5] - 23*ex[2]*ex[3]^4*ex[5] - 
    19*ex[2]^2*ex[3]^4*ex[5] - 9*ex[2]^3*ex[3]^4*ex[5] - 
    2*ex[2]^4*ex[3]^4*ex[5] - 2*ex[3]^5*ex[5] - 5*ex[2]*ex[3]^5*ex[5] - 
    4*ex[2]^2*ex[3]^5*ex[5] - ex[2]^3*ex[3]^5*ex[5], y[41] -> -1 - 2*ex[2], 
  y[42] -> 4 + 5*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[43] -> 3 + 6*ex[3] + 4*ex[2]*ex[3] + 3*ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[44] -> -1 - 2*ex[2] - ex[3] + ex[2]^2*ex[3], 
  y[45] -> 1 + 2*ex[2] + ex[3] + ex[2]*ex[3], 
  y[46] -> 38 + 38*ex[3] + 9*ex[2]*ex[3], 
  y[47] -> 3*ex[2] + 2*ex[2]^2 + 3*ex[5] + 2*ex[2]*ex[5] + 3*ex[3]*ex[5] + 
    3*ex[2]*ex[3]*ex[5], y[48] -> 2 + 2*ex[3] + 3*ex[2]*ex[3] + 
    3*ex[3]*ex[5] + 3*ex[3]^2*ex[5] + 3*ex[2]*ex[3]^2*ex[5], 
  y[49] -> -ex[2] - 4*ex[2]^2 - ex[2]*ex[3] + ex[2]^3*ex[3] - 3*ex[5] - 
    6*ex[2]*ex[5] - 6*ex[3]*ex[5] - 9*ex[2]*ex[3]*ex[5] - 
    3*ex[2]^2*ex[3]*ex[5] - 3*ex[3]^2*ex[5] - 3*ex[2]*ex[3]^2*ex[5] + 
    3*ex[2]^2*ex[3]^2*ex[5] + 3*ex[2]^3*ex[3]^2*ex[5]}}
