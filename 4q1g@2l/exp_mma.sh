#! /usr/bin/env bash

for f in *; do
	if [[ $f =~ ^[udUDg]{5}$ ]]; then
		cd $f
		math -script proc/proc.wl &
		cd ..
	fi
done
