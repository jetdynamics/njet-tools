# 4q1g@2l

Derived from `../2q3g@2l`.

## Usage

```shell
./proc/init.py
./exp_mma.sh
./loop_loop.sh
./proc/gen_src.py -a
cd export
find -name "*.cpp" -o -name "*.h" | parallel clang-format -i -style=file
```
