(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[1]}, {f[2]}}, {{1}, {1}}, {{MySparseMatrix[{1, 1}, {m[1, 1] -> 1}], 
   MySparseMatrix[{1, 1}, {}]}, {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}], 
   MySparseMatrix[{1, 1}, {}]}}, {1, pflip}, 
 {f[1] -> (y[1]^2*y[2]^3*y[3]^2)/(y[4]*y[5]), f[2] -> y[1]^2*y[2]^2*y[3]}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> ex[3], y[4] -> 1 + ex[3], 
  y[5] -> 1 + ex[3] + ex[2]*ex[3]}}
