(* ::Package:: *)

(* A[nf,Nc] *)
(* Nf=nf/Nc *)


names={A0->tree,A1->loop_nfp0,nfA1->loop_nfp1};


fixconstants = {\[Beta]0->11/3-(2 Nf)/3,\[Beta]1->34/3-(13 Nf)/3};


expr1=3/2*\[Beta]0*A0*Log[MU2]/.fixconstants//Collect[#,Nf]&


expr2=(15*A0*\[Beta]0^2*Log[MU2]^2)/8+(
            Log[MU2]*(
                5*(A1+Nf nfA1)*\[Beta]0+(
                    A0*(
                        998+80*Nf^2-165*Pi^2+Nf*(-556+30*Pi^2)+
                        324*\[Beta]1+3240*Zeta[3]
                    )
                )/108
            )
        )/2/.fixconstants//Collect[#,Nf]&


(* A[0,1] *)
Coefficient[expr1, Nf,0]


(* A[1,0] *)
Coefficient[expr1, Nf,1]


(* A[0,2] *)
Coefficient[expr2, Nf, 0]//Collect[#,A[0,0]]&;
%/.names


(* A[1,1] *)
Coefficient[expr2, Nf, 1]//Collect[#,A[0,0]]&;
%/.names


(* A[2,0] *)
Coefficient[expr2, Nf, 2]//Collect[#,A[0,0]]&;
%/.names
