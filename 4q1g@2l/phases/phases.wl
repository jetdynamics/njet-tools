(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/4q1g@2l/phases"];
<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_2q2Q1g.m"


njetify[expr_]:=ToString[InputForm[expr//.{
	spA[a_,b_]->Sort[sA[a-1,b-1]],
	spB[a_,b_]->Sort[sB[a-1,b-1]],
	Power[a_,b_]/;b>1->pow[a,b],
	Power[a_,b_]/;b<-1->1/pow[a,-b],
	ex[1]->x1,
	ex[2]->x2,
	ex[3]->x3,
	ex[4]->x4,
	ex[5]->x5
	}/.{p[i_]->i-1}]];
treeX[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalytic]//FullSimplify;


Helicity[expr_]:= Module[{tt,l},
  tt = Simplify[(expr/.{spA[i_,j_]:>spA[i,j]/l[i]/l[j],spAA[i_,aa__,j_]:>spAA[i,aa,j]/l[i]/l[j],
    spB[i_,j_]:>spB[i,j]*l[i]*l[j],spBB[i_,aa__,j_]:>spBB[i,aa,j]*l[i]*l[j]})/expr];
  Table[Exponent[tt,l[kk]]/2,{kk,1,5}]]
  
Dimension[expr_]:=Module[{tt,l},
  tt = Simplify[(expr /. {spA[ii__]:>l spA[ii],spB[ii__]:>l spB[ii],s[ii__]:>l^2 s[ii],
          spAA[ii__]:>l^3 spAA[ii],spBB[ii__]:>l^3 spBB[ii]})/expr];
  If[SubsetQ[{l},Variables[tt]], Exponent[tt,l], $Failed]
  ]


export[channel_]:=Module[{res,allhelicities},

Which[
  channel==="uUdDg",
  allhelicities = {"+-+-+","+--++","-++-+","-+-++"};,
  
  channel==="udUDg",
  allhelicities = {"++--+","+--++","-++-+","--+++"};,
  
  channel==="uDdUg",
  allhelicities = {"++--+","+-+-+","-+-++","--+++"};,
  
  channel === "ugUdD",
  allhelicities = {"++-+-","++--+","-+++-","-++-+"};,
    
  channel==="ugDdU",
  allhelicities = {"+++--","++-+-","-++-+","-+-++"};,
  
  channel === "udDUg",
  allhelicities = {"++--+","+-+-+","-+-++","--+++"};
];

gpos = Flatten[Position[DeleteCases[StringSplit[channel,""],"b"],"g"]][[1]]; (* position of the gluon *)

Do[
  Module[{posplus,posmin},
  posplus=Flatten[Position[StringSplit[hh,""],"+"]];
  If[Not[MemberQ[posplus,gpos]], Print["error: ", hh, " \[Rule] g should have helicity +"]; Abort[]];
  posmin=Flatten[Position[StringSplit[hh,""],"-"]];
  posplus=DeleteCases[posplus,gpos];
  If[Length[posplus]=!=Length[posmin], Print["error: ", hh, " -> non-MHV configuration"]; Abort[]];
  phase[hh]=(spA@@posplus)*(spA@@posmin)^3/(spA[1,2]spA[2,3]spA[3,4]spA[4,5]spA[5,1]);
    
  If[Union[Helicity[phase[hh]]-(StringSplit[hh,""]/.{"-"->-1,"+"->+1})*Normal[SparseArray[gpos->1,5,1/2]]]=!={0},
    Print["error"]; Abort[]];

  ];
,{hh, allhelicities}];

res = Table[
	Module[{s,x},
		s=phase[h];
		x=treeX[s];
		{"hchars"->h,"expr"->njetify[s/x]}
	],
	{h, allhelicities}
];

Export["phases_"<>channel<>".json", res];

];


export["udUDg"];
export["udDUg"];
export["uUdDg"];
export["uDdUg"];
export["ugUdD"];
export["ugDdU"];


(*EOF*)
