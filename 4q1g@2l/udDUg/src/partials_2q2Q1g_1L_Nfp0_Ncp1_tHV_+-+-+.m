(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[8], f[5], f[7], f[3], f[4], f[6], f[12], f[11], f[9], f[10], f[2], 
   f[13], f[1]}, {f[16], f[17], f[15], f[1], f[2], f[14], f[18]}}, 
 {{1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 
   23, 24, 25, 26, 27, 28, 29, 31}, {1, 2, 3, 32, 33, 34, 35, 36, 38, 39, 40, 
   41, 42, 10, 11, 43, 44, 45, 46, 22, 48, 24, 25, 26, 49, 50, 28, 52}}, 
 {{MySparseMatrix[{13, 28}, {m[1, 1] -> -1/2, m[2, 2] -> -2/3, 
     m[2, 9] -> 2/3, m[3, 9] -> 1/2, m[3, 14] -> -1/2, m[4, 2] -> -1/2, 
     m[4, 14] -> 1/2, m[5, 9] -> -1/2, m[5, 14] -> 1/2, m[6, 9] -> -1/2, 
     m[6, 14] -> 1/2, m[7, 3] -> 1/4, m[7, 13] -> -1/4, m[7, 15] -> -1/2, 
     m[7, 18] -> 1/2, m[7, 20] -> 1/2, m[7, 21] -> -1/2, m[7, 22] -> -1/12, 
     m[8, 3] -> -1/4, m[8, 13] -> 1/4, m[8, 15] -> 1/2, m[8, 18] -> -1/2, 
     m[8, 20] -> -1/2, m[8, 21] -> 1/2, m[8, 22] -> 1/12, m[9, 4] -> -1/2, 
     m[9, 14] -> 1/2, m[9, 23] -> 1/2, m[10, 4] -> -1/2, m[10, 9] -> 1/2, 
     m[10, 23] -> 1/2, m[11, 1] -> 1/3, m[11, 3] -> 1/4, m[11, 4] -> 5/6, 
     m[11, 5] -> -1/2, m[11, 6] -> 3/4, m[11, 7] -> 1/2, m[11, 8] -> -1/2, 
     m[11, 9] -> 1/3, m[11, 10] -> -1/2, m[11, 11] -> 1/2, m[11, 12] -> 1/2, 
     m[11, 13] -> -1/4, m[11, 14] -> 5/6, m[11, 16] -> 1/2, 
     m[11, 17] -> -1/2, m[11, 20] -> 1/2, m[11, 21] -> -1/2, 
     m[11, 22] -> 1/3, m[11, 23] -> -5/6, m[11, 24] -> 1/2, m[11, 25] -> 1/2, 
     m[11, 26] -> -1/2, m[11, 27] -> -1/2, m[12, 3] -> -1/2, m[12, 5] -> 1/2, 
     m[12, 10] -> 1/2, m[12, 11] -> -1/2, m[12, 19] -> -1/2, 
     m[12, 20] -> -1/2, m[12, 26] -> 1/2, m[12, 28] -> -1/2, m[13, 1] -> 1/3, 
     m[13, 3] -> -1/4, m[13, 4] -> 5/6, m[13, 6] -> 3/4, m[13, 7] -> 1/2, 
     m[13, 8] -> -1/2, m[13, 9] -> 1/3, m[13, 12] -> 1/2, m[13, 13] -> -1/4, 
     m[13, 14] -> 5/6, m[13, 16] -> 1/2, m[13, 17] -> -1/2, 
     m[13, 19] -> -1/2, m[13, 21] -> -1/2, m[13, 22] -> 1/3, 
     m[13, 23] -> -5/6, m[13, 24] -> 1/2, m[13, 25] -> 1/2, 
     m[13, 27] -> -1/2, m[13, 28] -> -1/2}], MySparseMatrix[{13, 28}, {}]}, 
  {MySparseMatrix[{7, 28}, {m[1, 1] -> -2/3, m[2, 2] -> 2/3, 
     m[2, 14] -> -2/3, m[3, 2] -> 1/2, m[3, 14] -> -1/2, m[4, 3] -> 1/4, 
     m[4, 10] -> -1/2, m[4, 13] -> 1/4, m[4, 20] -> 1/2, m[4, 21] -> 1/2, 
     m[4, 22] -> 1/12, m[4, 24] -> 1/2, m[4, 28] -> -1/2, m[5, 3] -> 1/4, 
     m[5, 10] -> -1/2, m[5, 13] -> 1/4, m[5, 20] -> 1/2, m[5, 21] -> 1/2, 
     m[5, 22] -> 1/12, m[5, 24] -> 1/2, m[5, 28] -> -1/2, m[6, 1] -> 2/3, 
     m[6, 3] -> 1/2, m[6, 4] -> 5/3, m[6, 5] -> -1, m[6, 6] -> 3/2, 
     m[6, 7] -> 1, m[6, 8] -> -1, m[6, 9] -> 5/3, m[6, 11] -> 1, 
     m[6, 12] -> -1, m[6, 13] -> 1/2, m[6, 14] -> 2/3, m[6, 15] -> -1, 
     m[6, 16] -> 1, m[6, 17] -> 1, m[6, 18] -> -1, m[6, 20] -> 1, 
     m[6, 21] -> 1, m[6, 22] -> 1, m[6, 23] -> -5/3, m[6, 25] -> -1, 
     m[6, 26] -> 1, m[6, 27] -> 1, m[6, 28] -> -1, m[7, 3] -> -3/2, 
     m[7, 5] -> 1, m[7, 10] -> 1, m[7, 13] -> -1/2, m[7, 15] -> 1, 
     m[7, 16] -> -1, m[7, 19] -> -1, m[7, 20] -> -2, m[7, 21] -> -1, 
     m[7, 22] -> -1/3, m[7, 24] -> -1, m[7, 28] -> 1}], 
   MySparseMatrix[{7, 28}, {}]}}, {1, pflip}, 
 {f[11] -> (y[2]^2*y[4])/(y[1]*y[3]^2), 
  f[4] -> y[7]/(y[1]*y[5]*y[6]) - (y[8]*y[9])/(y[1]*y[3]*y[10]) - 
    (y[1]^2*y[2]^2*y[11]*y[12])/(y[3]*y[5]*y[13]), 
  f[3] -> (y[8]*y[9])/(y[1]*y[3]*y[10]) - y[14]/(y[1]*y[3]) + 
    (y[2]*y[11])/y[15], f[6] -> (y[2]^2*y[16]^2)/y[6] - 
    (y[2]^2*y[9]*y[16]^2)/(y[6]*y[17]) - (y[1]*y[2]^2*y[16]*y[18])/
     (y[3]*y[19]) + (y[1]*y[2]^2*y[16]*y[21])/(y[3]*y[6]*y[20]), 
  f[9] -> (y[2]*y[11])/y[1] - (y[2]*y[11])/y[15] + 
    (y[2]^2*y[9]*y[16]^2)/(y[6]*y[17]) + (y[1]*y[2]^2*y[9]*y[16])/
     (y[6]*y[20]), f[1] -> y[22]/y[1], f[2] -> y[23]/(y[1]*y[6]), 
  f[12] -> (y[1]*y[2]^2*y[24])/y[3]^3, 
  f[8] -> (y[1]*y[2])/(y[9]*y[15]) + y[25]/(9*y[1]*y[3]*y[5]*y[6]) - 
    (4*y[6]^2*y[9]^3)/(3*y[1]*y[10]^2*y[26]) - 
    (y[1]*y[2]^2*y[12]^2*y[28]*y[29])/(3*y[9]*y[26]^2*y[27]) + 
    (y[1]^3*y[2]^2*y[12]^2)/(y[3]*y[5]*y[13]*y[30]) - 
    (y[1]*y[2]^2*y[32]*y[33])/(3*y[3]*y[26]^2*y[30]*y[31]) + 
    (y[6]*y[9]^2*y[34])/(3*y[1]*y[3]*y[10]*y[26]^2), 
  f[5] -> (y[6]^2*y[9]^3)/(y[1]*y[2]*y[10]^3) - (3*y[1]*y[2])/(4*y[15]^2) - 
    y[35]/(4*y[1]*y[2]*y[3]^2) - (3*y[36])/(4*y[15]) - 
    (3*y[6]*y[9]^2*y[37])/(4*y[1]*y[2]*y[3]*y[10]^2) + 
    (3*y[9]*y[38])/(4*y[1]*y[2]*y[3]^2*y[10]), 
  f[7] -> (y[1]^4*y[2]^2*y[6]*y[12]^2)/(y[3]*y[5]^2*y[13]^2) - 
    (y[1]*y[2]^2*y[16]*y[18])/(y[3]*y[19]) + (y[1]*y[2]^2*y[16])/
     (y[3]*y[20]) + (y[2]^2*y[40])/(y[3]^2*y[39]^2) + 
    (y[1]^2*y[2]^2*y[12]*y[41])/(y[3]^2*y[5]^2*y[13]), 
  f[10] -> (y[1]*y[2])/y[15]^2 - (y[2]^2*y[9]*y[16]^2)/(y[6]*y[17]) - 
    (y[1]*y[2]^2*y[9]*y[16])/(y[6]*y[20]) + y[36]/y[15] - y[42]/y[1], 
  f[13] -> y[43]/(y[1]*y[2]), f[14] -> (y[2]*y[16]^2)/(y[1]*y[3]), 
  f[15] -> (y[8]*y[9])/(y[1]*y[3]*y[10]) + y[11]/(y[1]*y[3]), 
  f[18] -> (y[2]*y[16]^2)/y[1], 
  f[17] -> (y[6]^2*y[9]^3)/(y[1]*y[2]*y[10]^3) + (3*y[6]*y[9]*y[43])/
     (4*y[1]*y[2]*y[3]*y[10]) + y[44]/(4*y[1]*y[2]*y[3]) - 
    (3*y[6]*y[9]^2*y[45])/(4*y[1]*y[2]*y[3]*y[10]^2), 
  f[16] -> (y[6]^2*y[9]^3)/(y[1]*y[10]^2*y[26]) + y[46]/(12*y[1]*y[3]^2) + 
    (y[1]*y[2]*y[12]^2*y[47])/(4*y[26]^2*y[27]) + (y[1]*y[2]^2*y[6]*y[48])/
     (4*y[3]^2*y[26]^2*y[31]) - (y[6]*y[9]^2*y[49])/
     (4*y[1]*y[3]*y[10]*y[26]^2)}, {y[1] -> ex[2], y[2] -> ex[3], 
  y[3] -> 1 + ex[3], y[4] -> 2 - ex[2]^2 + 2*ex[3] + 4*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3], y[5] -> -1 + ex[2] - ex[3], 
  y[6] -> 1 + ex[3] + ex[2]*ex[3], y[7] -> 4 - ex[2] - 3*ex[2]^2 + 4*ex[3] + 
    6*ex[2]*ex[3] - ex[2]^2*ex[3] - 2*ex[2]^3*ex[3] + 3*ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 3*ex[2]^3*ex[3]^2, 
  y[8] -> -4 - 3*ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] + 4*ex[3]^2 + 
    8*ex[2]*ex[3]^2 + 4*ex[2]^2*ex[3]^2, y[9] -> ex[5], 
  y[10] -> ex[2]*ex[3] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[11] -> 4 + 3*ex[2], y[12] -> -1 + ex[5], 
  y[13] -> -ex[2] - ex[2]*ex[3] + ex[4] - ex[2]*ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[14] -> -4 - 3*ex[2] + 4*ex[3] + 4*ex[2]*ex[3] + ex[2]^2*ex[3] + 
    4*ex[3]^2 + 4*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, y[15] -> ex[4] - ex[5], 
  y[16] -> 1 + ex[2], y[17] -> 1 + ex[4], y[18] -> 1 + ex[2] - ex[5], 
  y[19] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], 
  y[20] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[21] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[22] -> -4 - 3*ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 2*ex[2]^2*ex[3], 
  y[23] -> 4 + 3*ex[2] + 2*ex[3] + 3*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[24] -> -1 + ex[3], y[25] -> 21 - 21*ex[2] + 72*ex[3] - 15*ex[2]*ex[3] - 
    36*ex[2]^2*ex[3] + 52*ex[3]^2 - 7*ex[2]*ex[3]^2 - 48*ex[2]^2*ex[3]^2 + 
    61*ex[2]^3*ex[3]^2 + 38*ex[2]^4*ex[3]^2 - 28*ex[3]^3 - 
    100*ex[2]*ex[3]^3 - 99*ex[2]^2*ex[3]^3 + 20*ex[2]^3*ex[3]^3 + 
    38*ex[2]^4*ex[3]^3 - 29*ex[3]^4 - 87*ex[2]*ex[3]^4 - 96*ex[2]^2*ex[3]^4 - 
    38*ex[2]^3*ex[3]^4, y[26] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[27] -> ex[4], y[28] -> ex[2] + ex[5], 
  y[29] -> 3*ex[2]*ex[3] + ex[5] + 3*ex[3]*ex[5], 
  y[30] -> ex[2] - ex[5] - ex[3]*ex[5], 
  y[31] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[32] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[33] -> -2*ex[2] - 2*ex[2]*ex[3] + ex[2]^2*ex[3] + 2*ex[5] + 
    3*ex[2]*ex[5] + 4*ex[3]*ex[5] + 5*ex[2]*ex[3]*ex[5] + 2*ex[3]^2*ex[5] + 
    2*ex[2]*ex[3]^2*ex[5], y[34] -> 15*ex[2]*ex[3] + 24*ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 9*ex[2]*ex[3]^3 + 9*ex[2]^2*ex[3]^3 + 11*ex[5] + 
    29*ex[3]*ex[5] + 2*ex[2]*ex[3]*ex[5] + 25*ex[3]^2*ex[5] + 
    9*ex[2]*ex[3]^2*ex[5] + 7*ex[3]^3*ex[5] + 7*ex[2]*ex[3]^3*ex[5], 
  y[35] -> 13 + 34*ex[3] + 8*ex[2]*ex[3] + 27*ex[3]^2 + 15*ex[2]*ex[3]^2 + 
    ex[2]^2*ex[3]^2 + 7*ex[3]^3 + 15*ex[2]*ex[3]^3 + 2*ex[2]^2*ex[3]^3 + 
    ex[3]^4 + 2*ex[2]*ex[3]^4 + ex[2]^2*ex[3]^4, y[36] -> 2 + ex[2]*ex[3], 
  y[37] -> 5 + 8*ex[3] + 2*ex[2]*ex[3] + 3*ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[38] -> 8 + 26*ex[3] + 7*ex[2]*ex[3] + 30*ex[3]^2 + 18*ex[2]*ex[3]^2 + 
    ex[2]^2*ex[3]^2 + 14*ex[3]^3 + 15*ex[2]*ex[3]^3 + ex[2]^2*ex[3]^3 + 
    2*ex[3]^4 + 4*ex[2]*ex[3]^4 + 2*ex[2]^2*ex[3]^4, 
  y[39] -> 1 - ex[2] + ex[3], y[40] -> 1 - ex[2] + ex[2]^2 + 2*ex[3] + 
    ex[2]*ex[3] - ex[2]^2*ex[3] + ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[41] -> -2 + ex[2] - ex[2]^2 - 4*ex[3] - 2*ex[2]*ex[3] + ex[2]^2*ex[3] - 
    2*ex[3]^2 - 3*ex[2]*ex[3]^2, y[42] -> 2 + ex[3] + ex[2]*ex[3], 
  y[43] -> 2 + 2*ex[3] + ex[2]*ex[3], 
  y[44] -> -1 - 3*ex[3] - 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[45] -> 3 + 4*ex[3] + 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[46] -> 3 + 44*ex[3] + 91*ex[2]*ex[3] + 38*ex[2]^2*ex[3] + 41*ex[3]^2 + 
    91*ex[2]*ex[3]^2 + 47*ex[2]^2*ex[3]^2, 
  y[47] -> ex[2]*ex[3] + 3*ex[5] + ex[3]*ex[5], 
  y[48] -> 2*ex[2] + 2*ex[2]*ex[3] + 3*ex[2]^2*ex[3] - 2*ex[5] + 
    ex[2]*ex[5] - 4*ex[3]*ex[5] - ex[2]*ex[3]*ex[5] - 2*ex[3]^2*ex[5] - 
    2*ex[2]*ex[3]^2*ex[5], y[49] -> 9*ex[2]*ex[3] + 12*ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 3*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + 5*ex[5] + 
    11*ex[3]*ex[5] + 2*ex[2]*ex[3]*ex[5] + 7*ex[3]^2*ex[5] + 
    3*ex[2]*ex[3]^2*ex[5] + ex[3]^3*ex[5] + ex[2]*ex[3]^3*ex[5]}}
