(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[6], f[4], f[5], f[3], f[8], f[7], f[2], f[1], f[12], f[11], f[9], 
   f[10]}, {f[17], f[16], f[14], f[15], f[18], f[19], f[20], f[1], f[22], 
   f[13], f[23], f[21]}}, {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 
   15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31}, 
  {1, 2, 3, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 10, 11, 43, 44, 45, 
   46, 47, 22, 48, 24, 25, 26, 49, 50, 28, 51, 52}}, 
 {{MySparseMatrix[{12, 31}, {m[1, 1] -> 1/2, m[2, 2] -> 1, m[2, 7] -> -1, 
     m[3, 2] -> 2/3, m[3, 10] -> -2/3, m[4, 2] -> 1/2, m[4, 10] -> -1/2, 
     m[5, 2] -> -1/2, m[5, 4] -> 1/2, m[5, 25] -> -1/2, m[6, 2] -> -1/2, 
     m[6, 4] -> 1/2, m[6, 25] -> -1/2, m[7, 1] -> -2/3, m[7, 2] -> -2/3, 
     m[7, 4] -> -5/3, m[7, 6] -> 1/2, m[7, 7] -> -3/2, m[7, 13] -> -1, 
     m[7, 14] -> 1/2, m[7, 15] -> -5/3, m[7, 17] -> -1, m[7, 18] -> 1, 
     m[7, 21] -> 1, m[7, 23] -> 1, m[7, 25] -> 5/3, m[7, 29] -> 1, 
     m[7, 30] -> -1, m[8, 1] -> -2/3, m[8, 2] -> -2/3, m[8, 3] -> 1/2, 
     m[8, 4] -> -5/3, m[8, 6] -> 1/2, m[8, 7] -> -3/2, m[8, 13] -> -1, 
     m[8, 15] -> -5/3, m[8, 16] -> -1, m[8, 17] -> -1, m[8, 18] -> 1, 
     m[8, 19] -> 1, m[8, 21] -> 1, m[8, 22] -> 1, m[8, 24] -> -1/6, 
     m[8, 25] -> 5/3, m[8, 29] -> 1, m[8, 30] -> -1, m[9, 3] -> -1/4, 
     m[9, 5] -> 1/2, m[9, 6] -> -1/4, m[9, 8] -> -1/2, m[9, 9] -> 1/2, 
     m[9, 11] -> 1/2, m[9, 12] -> -1/2, m[9, 21] -> -1/2, m[9, 22] -> -1/2, 
     m[9, 24] -> -1/3, m[9, 26] -> -1/2, m[9, 27] -> -1/2, m[9, 28] -> 1/2, 
     m[9, 30] -> 1/2, m[10, 3] -> 1/4, m[10, 6] -> -1/4, m[10, 8] -> -1/2, 
     m[10, 9] -> 1/2, m[10, 20] -> 1/2, m[10, 21] -> -1/2, m[10, 24] -> -1/3, 
     m[10, 26] -> -1/2, m[10, 27] -> -1/2, m[10, 30] -> 1/2, 
     m[10, 31] -> 1/2, m[11, 3] -> 1/2, m[11, 5] -> -1/2, m[11, 11] -> -1/2, 
     m[11, 12] -> 1/2, m[11, 20] -> 1/2, m[11, 22] -> 1/2, m[11, 28] -> -1/2, 
     m[11, 31] -> 1/2, m[12, 3] -> 1/4, m[12, 6] -> -1/4, m[12, 8] -> -1/2, 
     m[12, 9] -> 1/2, m[12, 20] -> 1/2, m[12, 21] -> -1/2, m[12, 24] -> -1/3, 
     m[12, 26] -> -1/2, m[12, 27] -> -1/2, m[12, 30] -> 1/2, 
     m[12, 31] -> 1/2}], MySparseMatrix[{12, 31}, {}]}, 
  {MySparseMatrix[{12, 31}, {m[1, 1] -> 2/3, m[2, 6] -> 1, m[2, 15] -> -1, 
     m[3, 2] -> -1/2, m[3, 15] -> 1/2, m[4, 2] -> -2/3, m[4, 15] -> 2/3, 
     m[5, 10] -> -1/2, m[5, 15] -> 1/2, m[5, 25] -> 1/2, m[6, 10] -> -1/2, 
     m[6, 15] -> 1/2, m[6, 25] -> 1/2, m[7, 3] -> 1/4, m[7, 11] -> -1/2, 
     m[7, 14] -> 1/4, m[7, 22] -> 1/2, m[7, 23] -> 1/2, m[7, 24] -> 1/12, 
     m[7, 26] -> 1/2, m[7, 31] -> -1/2, m[8, 1] -> 2/3, m[8, 3] -> 1/2, 
     m[8, 4] -> 5/3, m[8, 5] -> -1, m[8, 6] -> 3/2, m[8, 7] -> 1, 
     m[8, 8] -> -1, m[8, 10] -> 5/3, m[8, 12] -> 1, m[8, 13] -> -1, 
     m[8, 14] -> 1/2, m[8, 15] -> 2/3, m[8, 16] -> -1, m[8, 17] -> 1, 
     m[8, 18] -> 1, m[8, 19] -> -1, m[8, 22] -> 1, m[8, 23] -> 1, 
     m[8, 24] -> 1, m[8, 25] -> -5/3, m[8, 27] -> -1, m[8, 28] -> 1, 
     m[8, 29] -> 1, m[8, 31] -> -1, m[9, 9] -> 1/4, m[9, 14] -> -1/4, 
     m[9, 18] -> -1/2, m[9, 19] -> 1/2, m[9, 21] -> 1/2, m[9, 23] -> -1/2, 
     m[9, 24] -> -1/3, m[9, 28] -> -1/2, m[9, 29] -> -1/2, m[9, 30] -> 1/2, 
     m[9, 31] -> 1/2, m[10, 1] -> -2/3, m[10, 3] -> 1/2, m[10, 4] -> -5/3, 
     m[10, 6] -> -3/2, m[10, 7] -> -1, m[10, 8] -> 1, m[10, 10] -> -5/3, 
     m[10, 12] -> -1, m[10, 13] -> 1, m[10, 14] -> -1/2, m[10, 15] -> -2/3, 
     m[10, 18] -> -1, m[10, 19] -> 1, m[10, 20] -> 1, m[10, 23] -> -1, 
     m[10, 24] -> -5/6, m[10, 25] -> 5/3, m[10, 27] -> 1, m[10, 28] -> -1, 
     m[10, 29] -> -1, m[10, 31] -> 1, m[11, 9] -> 1/4, m[11, 14] -> -1/4, 
     m[11, 18] -> -1/2, m[11, 19] -> 1/2, m[11, 21] -> 1/2, 
     m[11, 23] -> -1/2, m[11, 24] -> -1/3, m[11, 28] -> -1/2, 
     m[11, 29] -> -1/2, m[11, 30] -> 1/2, m[11, 31] -> 1/2, m[12, 3] -> -1/4, 
     m[12, 11] -> 1/2, m[12, 14] -> -1/4, m[12, 22] -> -1/2, 
     m[12, 23] -> -1/2, m[12, 24] -> -1/12, m[12, 26] -> -1/2, 
     m[12, 31] -> 1/2}], MySparseMatrix[{12, 31}, {}]}}, {1, pflip}, 
 {f[1] -> y[2]^2/(y[1]*y[3]), f[2] -> y[2]^2/(y[3]*y[4]), 
  f[3] -> y[5]/(y[1]*y[4]) + (y[6]*y[7])/(y[1]*y[4]*y[8]), 
  f[4] -> (y[2]^2*y[9]^2)/(y[4]*y[10]), 
  f[7] -> (y[2]^2*y[9]^2)/(y[4]*y[10]) + (y[2]*y[11])/(y[1]*y[4]) - 
    (y[2]*y[12])/y[13], f[11] -> y[14]/y[1], f[12] -> y[15]/(y[1]*y[4]), 
  f[10] -> (y[1]*y[2]^2*y[9])/y[4], 
  f[5] -> (y[3]^2*y[7]^3)/(y[1]*y[2]*y[8]^3) - y[16]/(4*y[1]*y[2]*y[4]) - 
    (3*y[3]*y[7]^2*y[17])/(4*y[1]*y[2]*y[4]*y[8]^2) + 
    (3*y[3]*y[7]*y[18])/(4*y[1]*y[2]*y[4]*y[8]), 
  f[6] -> (y[1]*y[2])/(y[7]*y[13]) + y[19]/(9*y[1]*y[4]) - 
    (4*y[3]^2*y[7]^3)/(3*y[1]*y[8]^2*y[20]) - (y[1]*y[2]^2*y[22]*y[23])/
     (3*y[7]*y[20]^2*y[21]) + (y[1]*y[2]^2*y[25])/(3*y[4]*y[20]^2*y[24]) + 
    (y[3]*y[7]^2*y[26])/(3*y[1]*y[4]*y[8]*y[20]^2), 
  f[8] -> (y[2]^2*y[9]^2)/(y[4]*y[10]) + (y[1]*y[2])/y[13]^2 - 
    (y[3]*y[27])/(y[1]*y[4]) + y[28]/y[13], 
  f[9] -> (y[3]*y[29])/(y[1]*y[2]*y[4]), 
  f[14] -> (y[6]*y[7])/(y[1]*y[4]*y[8]) + y[12]/(y[1]*y[4]), 
  f[23] -> (y[2]*y[12])/y[3], f[16] -> (y[2]*y[9])/(y[3]*y[30]), 
  f[18] -> (y[2]*y[9])/(y[3]*y[30]) - (y[2]*y[31])/(y[3]*y[4]) - 
    (y[1]^2*y[2]^2*y[12])/(y[3]*y[4]*y[32]), f[13] -> y[2]/y[1], 
  f[20] -> (y[2]*y[33])/(y[1]*y[4]^2), f[22] -> (y[1]*y[2])/y[3], 
  f[21] -> (y[1]*y[2]*y[34])/y[4]^3, 
  f[15] -> (y[3]^2*y[7]^3)/(y[1]*y[2]*y[8]^3) + y[35]/(4*y[1]*y[2]*y[4]^2) - 
    (3*y[3]*y[7]^2*y[36])/(4*y[1]*y[2]*y[4]*y[8]^2) + 
    (3*y[7]*y[37])/(4*y[1]*y[2]*y[4]^2*y[8]), 
  f[17] -> (y[3]^2*y[7]^3)/(y[1]*y[8]^2*y[20]) + y[38]/(12*y[1]*y[3]*y[4]) + 
    (y[1]*y[2]*y[39])/(4*y[20]^2*y[21]) + (3*y[1]^3*y[2]^2*y[9])/
     (4*y[3]*y[4]*y[32]*y[40]) - (y[1]*y[2]^2*y[41]*y[42])/
     (4*y[3]*y[20]^2*y[24]*y[40]) - (y[3]*y[7]^2*y[43])/
     (4*y[1]*y[4]*y[8]*y[20]^2), f[19] -> (y[2]*y[9])/(y[3]*y[30]) - 
    (y[1]^4*y[2]^2)/(y[3]*y[4]*y[32]^2) - (y[2]*y[44])/(y[3]*y[4]^2) - 
    (y[1]^2*y[2]^2*y[45])/(y[3]*y[4]^2*y[32])}, 
 {y[1] -> ex[2], y[2] -> ex[3], y[3] -> 1 + ex[3], 
  y[4] -> 1 + ex[3] + ex[2]*ex[3], y[5] -> 4 + ex[2] + ex[2]*ex[3], 
  y[6] -> -4 - ex[2] - ex[2]*ex[3] + 4*ex[3]^2 + 4*ex[2]*ex[3]^2, 
  y[7] -> ex[5], y[8] -> ex[2]*ex[3] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[9] -> 1 + ex[2], y[10] -> 1 + ex[4], 
  y[11] -> 4 + ex[2] + 4*ex[3] + 4*ex[2]*ex[3], y[12] -> 4 + ex[2], 
  y[13] -> ex[4] - ex[5], y[14] -> -4 - ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 
    ex[2]^2*ex[3], y[15] -> 4 + ex[2] + 2*ex[3] + ex[2]*ex[3], 
  y[16] -> 13 + 27*ex[3] + 7*ex[2]*ex[3] + 15*ex[3]^2 + 8*ex[2]*ex[3]^2, 
  y[17] -> 5 + 8*ex[3] + 4*ex[2]*ex[3] + 3*ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[18] -> 8 + 10*ex[3] + 5*ex[2]*ex[3] + 2*ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[19] -> -21 - 30*ex[3] - 12*ex[2]*ex[3] + 29*ex[3]^2 + 29*ex[2]*ex[3]^2, 
  y[20] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], y[21] -> ex[4], 
  y[22] -> ex[2] + ex[5], y[23] -> 3*ex[2]*ex[3] + ex[5] + 3*ex[3]*ex[5], 
  y[24] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[25] -> -2*ex[2] - 2*ex[2]*ex[3] + ex[2]^2*ex[3] + 2*ex[5] + 
    3*ex[2]*ex[5] + 4*ex[3]*ex[5] + 5*ex[2]*ex[3]*ex[5] + 2*ex[3]^2*ex[5] + 
    2*ex[2]*ex[3]^2*ex[5], y[26] -> 15*ex[2]*ex[3] + 24*ex[2]*ex[3]^2 + 
    12*ex[2]^2*ex[3]^2 + 9*ex[2]*ex[3]^3 + 9*ex[2]^2*ex[3]^3 + 11*ex[5] + 
    29*ex[3]*ex[5] + 8*ex[2]*ex[3]*ex[5] + 25*ex[3]^2*ex[5] + 
    15*ex[2]*ex[3]^2*ex[5] + 7*ex[3]^3*ex[5] + 7*ex[2]*ex[3]^3*ex[5], 
  y[27] -> 2 + ex[3] + ex[2]*ex[3], y[28] -> 2 - ex[2]*ex[3], 
  y[29] -> 2 + 2*ex[3] + ex[2]*ex[3], 
  y[30] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[31] -> -1 + 3*ex[3], y[32] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - 
    ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4], 
  y[33] -> 2 + 4*ex[2] + ex[2]^2 + 2*ex[3] + 2*ex[2]*ex[3], 
  y[34] -> -1 + ex[3] + ex[2]*ex[3], 
  y[35] -> -1 - 4*ex[3] - 2*ex[2]*ex[3] - 2*ex[3]^2 - 9*ex[2]*ex[3]^2 - 
    7*ex[2]^2*ex[3]^2 + ex[3]^3 + 2*ex[2]*ex[3]^3 + ex[2]^2*ex[3]^3, 
  y[36] -> 3 + 4*ex[3] + 4*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[37] -> 2 + 6*ex[3] + 5*ex[2]*ex[3] + 6*ex[3]^2 + 10*ex[2]*ex[3]^2 + 
    5*ex[2]^2*ex[3]^2 + 2*ex[3]^3 + 5*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3, 
  y[38] -> 3 + 44*ex[3] + 12*ex[2]*ex[3] + 41*ex[3]^2 + 41*ex[2]*ex[3]^2, 
  y[39] -> ex[2]*ex[3] + 3*ex[5] + ex[3]*ex[5], 
  y[40] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[41] -> ex[2] - ex[5] - ex[3]*ex[5], 
  y[42] -> 2*ex[2] + 2*ex[2]*ex[3] + 3*ex[2]^2*ex[3] - 2*ex[5] + 
    ex[2]*ex[5] - 4*ex[3]*ex[5] - ex[2]*ex[3]*ex[5] - 2*ex[3]^2*ex[5] - 
    2*ex[2]*ex[3]^2*ex[5], y[43] -> 9*ex[2]*ex[3] + 12*ex[2]*ex[3]^2 + 
    12*ex[2]^2*ex[3]^2 + 3*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + 5*ex[5] + 
    11*ex[3]*ex[5] + 8*ex[2]*ex[3]*ex[5] + 7*ex[3]^2*ex[5] + 
    9*ex[2]*ex[3]^2*ex[5] + ex[3]^3*ex[5] + ex[2]*ex[3]^3*ex[5], 
  y[44] -> -1 - ex[3] + ex[2]*ex[3], 
  y[45] -> 2 + 3*ex[2] + 2*ex[3] + 3*ex[2]*ex[3] + ex[2]^2*ex[3]}}
