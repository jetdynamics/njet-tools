(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[5], f[4], f[7], f[6], f[11], f[10], f[8], f[9], f[2], f[12], 
   f[1]}, {f[15], f[14], f[16], f[1], f[2], f[13], f[17]}}, 
 {{1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 
   24, 25, 26, 27, 28, 29, 30}, {1, 32, 34, 35, 36, 37, 38, 39, 40, 2, 41, 
   42, 43, 3, 4, 44, 45, 46, 5, 48, 49, 20, 24, 25, 50, 51, 26, 27, 52}}, 
 {{MySparseMatrix[{12, 28}, {m[1, 1] -> -1/18, m[2, 2] -> -1/2, 
     m[2, 4] -> 1/2, m[3, 2] -> -2/3, m[3, 4] -> 2/3, m[4, 2] -> 1/2, 
     m[4, 14] -> -1/2, m[5, 2] -> -1/2, m[5, 14] -> 1/2, m[6, 3] -> -1/2, 
     m[6, 5] -> 1/2, m[6, 15] -> 1/2, m[6, 16] -> -1/2, m[6, 19] -> -1/2, 
     m[6, 21] -> -1/2, m[6, 22] -> -1/12, m[7, 3] -> 1/2, m[7, 5] -> -1/2, 
     m[7, 15] -> -1/2, m[7, 16] -> 1/2, m[7, 19] -> 1/2, m[7, 21] -> 1/2, 
     m[7, 22] -> 1/12, m[8, 2] -> -1/2, m[8, 7] -> 1/2, m[8, 23] -> -1/2, 
     m[9, 2] -> 1/2, m[9, 7] -> -1/2, m[9, 23] -> 1/2, m[10, 1] -> 1/3, 
     m[10, 2] -> 1/3, m[10, 3] -> -1/2, m[10, 7] -> 5/6, m[10, 8] -> 1/2, 
     m[10, 9] -> -1/2, m[10, 10] -> 3/4, m[10, 11] -> 1/2, m[10, 12] -> 1/2, 
     m[10, 13] -> -1/2, m[10, 14] -> 5/6, m[10, 17] -> 1/2, 
     m[10, 18] -> -1/2, m[10, 19] -> -1/2, m[10, 21] -> -1/2, 
     m[10, 22] -> 1/3, m[10, 23] -> -5/6, m[10, 24] -> -1/2, 
     m[10, 25] -> 1/2, m[10, 26] -> 1/2, m[10, 27] -> -1/2, m[11, 3] -> 1/4, 
     m[11, 6] -> -1/4, m[11, 8] -> -1/2, m[11, 9] -> 1/2, m[11, 19] -> 1/2, 
     m[11, 20] -> -1/2, m[11, 24] -> 1/2, m[11, 28] -> -1/2, m[12, 1] -> 1/3, 
     m[12, 2] -> 1/3, m[12, 3] -> -1/4, m[12, 6] -> -1/4, m[12, 7] -> 5/6, 
     m[12, 10] -> 3/4, m[12, 11] -> 1/2, m[12, 12] -> 1/2, m[12, 13] -> -1/2, 
     m[12, 14] -> 5/6, m[12, 17] -> 1/2, m[12, 18] -> -1/2, 
     m[12, 20] -> -1/2, m[12, 21] -> -1/2, m[12, 22] -> 1/3, 
     m[12, 23] -> -5/6, m[12, 25] -> 1/2, m[12, 26] -> 1/2, 
     m[12, 27] -> -1/2, m[12, 28] -> -1/2}], MySparseMatrix[{12, 28}, {}]}, 
  {MySparseMatrix[{7, 29}, {m[1, 1] -> -2/3, m[2, 10] -> -2, m[2, 15] -> 2, 
     m[3, 10] -> -1/6, m[3, 15] -> 1/6, m[4, 5] -> 1/4, m[4, 14] -> -1/4, 
     m[4, 17] -> -1/2, m[4, 19] -> 1/2, m[4, 20] -> 1/2, m[4, 22] -> -1/2, 
     m[4, 23] -> 1/12, m[4, 28] -> 1/2, m[4, 29] -> -1/2, m[5, 5] -> 1/4, 
     m[5, 14] -> -1/4, m[5, 17] -> -1/2, m[5, 19] -> 1/2, m[5, 20] -> 1/2, 
     m[5, 22] -> -1/2, m[5, 23] -> 1/12, m[5, 28] -> 1/2, m[5, 29] -> -1/2, 
     m[6, 1] -> 2/3, m[6, 2] -> 3/2, m[6, 3] -> 5/3, m[6, 4] -> -1, 
     m[6, 5] -> 1/2, m[6, 6] -> 5/3, m[6, 7] -> -1, m[6, 8] -> 1, 
     m[6, 10] -> 2/3, m[6, 11] -> 1, m[6, 12] -> -1, m[6, 13] -> 1, 
     m[6, 14] -> -1/2, m[6, 16] -> 1, m[6, 18] -> -1, m[6, 20] -> 1, 
     m[6, 22] -> -1, m[6, 23] -> 1, m[6, 24] -> -5/3, m[6, 25] -> 1, 
     m[6, 26] -> -1, m[6, 27] -> 1, m[6, 29] -> -1, m[7, 5] -> -1/2, 
     m[7, 9] -> 1/2, m[7, 13] -> -1, m[7, 14] -> 1, m[7, 17] -> 1, 
     m[7, 19] -> -1, m[7, 20] -> -1, m[7, 21] -> 1, m[7, 22] -> 2, 
     m[7, 23] -> -1/3, m[7, 28] -> -1, m[7, 29] -> 1}], 
   MySparseMatrix[{7, 29}, {}]}}, {1, pflip}, 
 {f[10] -> (y[2]^2*y[4])/(y[1]*y[3]), f[1] -> y[5]/(y[1]*y[3]), 
  f[5] -> (y[2]*y[7])/(y[1]*y[3]*y[6]) - (y[2]*y[8]*y[9]*y[10])/
     (y[6]*y[11]) - (y[2]*y[12])/(y[3]*y[13]), 
  f[6] -> (y[2]^2*y[8])/y[14] + (y[2]^2*y[8]^2)/(y[3]*y[15]) + 
    (y[2]^2*y[8]*y[10]*y[12])/y[16] + (y[2]^2*y[8]^3*y[17])/
     (y[3]*y[14]*y[18]), f[8] -> (y[2]*y[12])/(y[3]*y[13]) + 
    (y[2]*y[19])/(y[1]*y[3]) + (y[2]^2*y[8]^2*y[20])/(y[6]*y[15]) + 
    (y[2]^2*y[8]^3*y[22])/(y[1]*y[3]*y[6]*y[21]), f[2] -> y[23]/(y[1]*y[6]), 
  f[11] -> y[1]*y[2]^2*y[24], f[3] -> (y[2]*y[8]*y[25])/(y[1]*y[3]*y[6]) - 
    (9*y[1]*y[2]^2*y[3]*y[8]*y[10]^2)/(y[16]*y[26]) - 
    (12*y[1]*y[2]^2*y[3]^2*y[8]^2*y[10]^3)/(y[11]^2*y[27]) - 
    (9*y[2]*y[8])/(y[3]*y[13]*y[28]) + (3*y[2]^2*y[8]^2*y[10]*y[30])/
     (y[26]*y[27]^2*y[29]) + (3*y[2]^2*y[3]*y[8]*y[10]^2*y[32]*y[33])/
     (y[27]^2*y[28]*y[31]) + (3*y[2]*y[3]*y[8]*y[10]^2*y[34])/
     (y[11]*y[27]^2), f[12] -> y[35]/(y[1]*y[2]), 
  f[4] -> (y[1]^2*y[2]^2*y[3]^2*y[8]^3*y[10]^3)/(y[6]*y[11]^3) + 
    (3*y[1]*y[2])/(4*y[3]*y[13]^2) + y[36]/(4*y[1]*y[3]*y[6]) - 
    (3*y[1]*y[2]*y[3]*y[8]^2*y[10]^2*y[37])/(4*y[6]*y[11]^2) + 
    (3*y[8]*y[10]*y[38])/(4*y[11]) + (3*y[39])/(4*y[3]*y[13]), 
  f[7] -> (y[2]^2*y[8])/y[14] + (y[2]^2*y[8]^2)/(y[3]*y[15]) + 
    (y[2]^2*y[6]*y[8]^2*y[10]^2)/y[16]^2 + (y[2]^2*y[8]^3*y[17])/
     (y[3]*y[14]*y[18]) + (y[1]*y[2]^2*y[8]*y[10]*y[24])/y[16], 
  f[9] -> (y[1]*y[2])/(y[3]*y[13]^2) + (y[2]^2*y[8]^2*y[20])/(y[6]*y[15]) + 
    (y[2]^2*y[8]^3*y[22])/(y[1]*y[3]*y[6]*y[21]) + y[39]/(y[3]*y[13]) + 
    y[40]/y[1], f[13] -> (y[2]*y[8]^2)/y[1], 
  f[14] -> (y[2]*y[8]^2)/(y[1]*y[6]) + (y[2]*y[8]*y[9]*y[10])/(4*y[6]*y[11]), 
  f[17] -> (y[2]*y[8]^2)/(y[1]*y[3]), 
  f[16] -> (y[2]*y[8]^2)/(y[1]*y[6]) - (4*y[1]^2*y[2]^2*y[3]^2*y[8]^3*
      y[10]^3)/(y[6]*y[11]^3) - (3*y[1]*y[2]*y[3]*y[8]^2*y[10]^2*y[41])/
     (y[6]*y[11]^2) - (3*y[1]*y[2]*y[3]*y[8]*y[10]*y[42])/(y[6]*y[11]), 
  f[15] -> (y[1]*y[2]^2*y[3]^2*y[8]^2*y[10]^3)/(y[11]^2*y[27]) - 
    (y[2]*y[8]*y[43])/(12*y[1]) + (y[2]^2*y[6]*y[8]*y[44])/
     (4*y[27]^2*y[29]) + (y[2]*y[3]^2*y[8]*y[10]^2*y[45])/(4*y[27]^2*y[31]) + 
    (y[2]*y[3]*y[8]*y[10]^2*y[46])/(4*y[11]*y[27]^2)}, 
 {y[1] -> 1 + ex[2], y[2] -> ex[3], y[3] -> 1 + ex[3], 
  y[4] -> -1 + 2*ex[2] + ex[2]^2 - ex[3] + 2*ex[2]*ex[3] + 3*ex[2]^2*ex[3], 
  y[5] -> 1 - 3*ex[2] + ex[3] - 3*ex[2]*ex[3] + 2*ex[2]^2*ex[3], 
  y[6] -> 1 + ex[3] + ex[2]*ex[3], y[7] -> 1 - 2*ex[2] - 3*ex[2]^2 + ex[3] - 
    ex[2]*ex[3] - 5*ex[2]^2*ex[3] + ex[2]^3*ex[3], y[8] -> ex[2], 
  y[9] -> -1 + 3*ex[2] - 2*ex[3] + 5*ex[2]*ex[3] - ex[2]^2*ex[3] - ex[3]^2 + 
    2*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2, y[10] -> ex[5], 
  y[11] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] - ex[2]*ex[5], y[12] -> -1 + 3*ex[2], 
  y[13] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[14] -> -1 + ex[2] - ex[3], y[15] -> ex[4], 
  y[16] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[17] -> ex[2] - ex[5] - ex[3]*ex[5], 
  y[18] -> -ex[2] - ex[2]*ex[3] + ex[4] - ex[2]*ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[5] + ex[2]*ex[3]*ex[5], y[19] -> -1 + 3*ex[2] + ex[2]*ex[3], 
  y[20] -> -1 + ex[5], y[21] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[2]*ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4], 
  y[22] -> -ex[2] + ex[5] + ex[2]*ex[5] + ex[3]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[23] -> -1 + 3*ex[2] - ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[24] -> 1 + 2*ex[3], y[25] -> 9 + 9*ex[3] + 9*ex[2]*ex[3] + 
    38*ex[2]^2*ex[3], y[26] -> 1 + ex[2] - ex[5], 
  y[27] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[28] -> -1 + ex[5] + ex[3]*ex[5], y[29] -> 1 + ex[4] - ex[5], 
  y[30] -> 1 + ex[2] + ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3] - ex[5] - 
    3*ex[2]*ex[5] - ex[3]*ex[5] - ex[2]*ex[3]*ex[5], 
  y[31] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[32] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[33] -> 1 + ex[3] + 3*ex[2]*ex[3] - ex[5] + ex[3]*ex[5] + 2*ex[3]^2*ex[5], 
  y[34] -> -3 - 3*ex[3] - 9*ex[2]*ex[3] + 3*ex[3]^2 - 3*ex[2]*ex[3]^2 - 
    6*ex[2]^2*ex[3]^2 + 3*ex[3]^3 + 6*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + 
    3*ex[5] + ex[3]*ex[5] + 2*ex[2]*ex[3]*ex[5] - 7*ex[3]^2*ex[5] - 
    3*ex[2]*ex[3]^2*ex[5] - 5*ex[3]^3*ex[5] - 5*ex[2]*ex[3]^3*ex[5], 
  y[35] -> 2 + ex[3] + ex[2]*ex[3], 
  y[36] -> 6 + 6*ex[2] + 12*ex[3] + 18*ex[2]*ex[3] + 6*ex[2]^2*ex[3] + 
    6*ex[3]^2 + 12*ex[2]*ex[3]^2 + 6*ex[2]^2*ex[3]^2 + ex[2]^3*ex[3]^2, 
  y[37] -> -1 - 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[38] -> 2 + ex[3] + ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2 + 2*ex[3]^3 + 
    2*ex[2]*ex[3]^3, y[39] -> 2 + 3*ex[3] + ex[2]*ex[3], 
  y[40] -> 2 + ex[2]*ex[3], y[41] -> -1 + 2*ex[2]*ex[3] + ex[3]^2 + 
    ex[2]*ex[3]^2, y[42] -> -1 - ex[3] + ex[2]*ex[3], 
  y[43] -> -38*ex[2] + 9*ex[3] + 9*ex[2]*ex[3], 
  y[44] -> 3 + 3*ex[2] + 3*ex[3] + 6*ex[2]*ex[3] + 3*ex[2]^2*ex[3] - 
    3*ex[5] - ex[2]*ex[5] - 3*ex[3]*ex[5] - 3*ex[2]*ex[3]*ex[5], 
  y[45] -> 3 + 3*ex[3] + ex[2]*ex[3] - 3*ex[5] - 5*ex[3]*ex[5] - 
    2*ex[3]^2*ex[5], y[46] -> -3 - 3*ex[3] + 3*ex[2]*ex[3] + 3*ex[3]^2 + 
    9*ex[2]*ex[3]^2 + 6*ex[2]^2*ex[3]^2 + 3*ex[3]^3 + 6*ex[2]*ex[3]^3 + 
    3*ex[2]^2*ex[3]^3 + 3*ex[5] + 5*ex[3]*ex[5] - 2*ex[2]*ex[3]*ex[5] + 
    ex[3]^2*ex[5] - 3*ex[2]*ex[3]^2*ex[5] - ex[3]^3*ex[5] - 
    ex[2]*ex[3]^3*ex[5]}}
