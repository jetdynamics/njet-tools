(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[4], f[6], f[3], f[1]}, {f[6], f[5], f[4]}}, 
 {{1, 2, 4, 7, 15, 25}, {1, 34, 37, 2, 4, 25}}, 
 {{MySparseMatrix[{4, 6}, {m[1, 1] -> 10/9, m[2, 1] -> -10/9, m[3, 2] -> 2/3, 
     m[3, 3] -> -2/3, m[4, 1] -> 10/9, m[4, 3] -> -2/3, m[4, 4] -> -1/6, 
     m[4, 5] -> -1/6, m[4, 6] -> 1/6}], MySparseMatrix[{4, 6}, {}]}, 
  {MySparseMatrix[{3, 6}, {m[1, 1] -> 10/9, m[2, 4] -> 2/3, m[2, 5] -> -2/3, 
     m[3, 2] -> -1/6, m[3, 3] -> -1/6, m[3, 4] -> -2/3, m[3, 6] -> 1/6}], 
   MySparseMatrix[{3, 6}, {}]}}, {1, pflip}, 
 {f[3] -> (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/(y[5]*y[7]^3) + 
    (3*y[1]^3*y[2]*y[3]^2*y[4]*y[6]^2)/(2*y[5]*y[7]^2) + 
    (3*y[1]^3*y[3]^2*y[6])/(2*y[5]*y[7]), 
  f[2] -> (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/(y[5]*y[7]^3) - 
    (3*y[1]*y[3]^2*y[4]^2*y[6]^2*y[10])/(22*y[8]*y[9]^2) + 
    (3*y[1]^2*y[2]*y[3]^2*y[4]*y[6]^2*y[11])/(22*y[5]*y[7]^2*y[9]) + 
    (3*y[1]*y[3]^2*y[6]*y[12])/(22*y[5]*y[7]*y[9]^2), 
  f[1] -> (y[1]^3*y[3]^2)/(y[2]*y[4]*y[5]), 
  f[5] -> (y[1]^2*y[3])/(y[2]*y[5]) - (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[6]^3)/
     (y[5]*y[7]^3) - (3*y[1]^3*y[2]*y[3]^2*y[4]*y[6]^2)/(2*y[5]*y[7]^2) - 
    (3*y[1]^3*y[3]^2*y[6])/(2*y[5]*y[7]), 
  f[6] -> (y[1]^2*y[3])/y[2] + (3*y[1]^2*y[2]*y[3]^2*y[4]^2*y[6]^3)/
     (5*y[7]^2*y[9]) - (3*y[1]*y[3]^2*y[4]^2*y[6]^2*y[10])/(10*y[8]*y[9]^2) + 
    (3*y[1]^2*y[3]^2*y[5]*y[6])/(10*y[9]^2*y[13]) + 
    (3*y[1]*y[3]^2*y[4]*y[6]^2*y[14])/(10*y[7]*y[9]^2), 
  f[4] -> (y[1]^2*y[3])/y[2]}, {y[1] -> ex[2], y[2] -> 1 + ex[2], 
  y[3] -> ex[3], y[4] -> 1 + ex[3], y[5] -> 1 + ex[3] + ex[2]*ex[3], 
  y[6] -> ex[5], y[7] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4] - ex[2]*ex[5], 
  y[8] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[9] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[10] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[11] -> 11*ex[2] + 11*ex[2]*ex[3] + 11*ex[2]^2*ex[3] + ex[5] - 
    11*ex[2]*ex[5] + 2*ex[3]*ex[5] - 10*ex[2]*ex[3]*ex[5] + ex[3]^2*ex[5] + 
    ex[2]*ex[3]^2*ex[5], y[12] -> 7*ex[2]^2 + 14*ex[2]^2*ex[3] + 
    14*ex[2]^3*ex[3] + 7*ex[2]^2*ex[3]^2 + 14*ex[2]^3*ex[3]^2 + 
    7*ex[2]^4*ex[3]^2 + ex[2]*ex[5] - 14*ex[2]^2*ex[5] + 
    3*ex[2]*ex[3]*ex[5] - 26*ex[2]^2*ex[3]*ex[5] - 14*ex[2]^3*ex[3]*ex[5] + 
    3*ex[2]*ex[3]^2*ex[5] - 10*ex[2]^2*ex[3]^2*ex[5] - 
    13*ex[2]^3*ex[3]^2*ex[5] + ex[2]*ex[3]^3*ex[5] + 
    2*ex[2]^2*ex[3]^3*ex[5] + ex[2]^3*ex[3]^3*ex[5] + ex[5]^2 + 
    7*ex[2]^2*ex[5]^2 + 4*ex[3]*ex[5]^2 + 2*ex[2]*ex[3]*ex[5]^2 + 
    14*ex[2]^2*ex[3]*ex[5]^2 + 6*ex[3]^2*ex[5]^2 + 6*ex[2]*ex[3]^2*ex[5]^2 + 
    8*ex[2]^2*ex[3]^2*ex[5]^2 + 4*ex[3]^3*ex[5]^2 + 6*ex[2]*ex[3]^3*ex[5]^2 + 
    2*ex[2]^2*ex[3]^3*ex[5]^2 + ex[3]^4*ex[5]^2 + 2*ex[2]*ex[3]^4*ex[5]^2 + 
    ex[2]^2*ex[3]^4*ex[5]^2, y[13] -> 1 + ex[4] - ex[5], 
  y[14] -> 3*ex[2] + 3*ex[2]*ex[3] + 3*ex[2]^2*ex[3] + ex[5] - ex[2]*ex[5] + 
    2*ex[3]*ex[5] + ex[3]^2*ex[5] + ex[2]*ex[3]^2*ex[5]}}
