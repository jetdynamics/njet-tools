(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[5], f[3], f[4], f[6], f[2], f[1]}, {f[11], f[9], f[10], f[8], f[7], 
   f[14], f[15], f[12], f[13], f[16], f[6], f[17]}}, 
 {{1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 
   24, 25, 26, 27, 28, 29, 30}, {1, 32, 34, 35, 36, 37, 38, 39, 40, 2, 41, 
   42, 43, 3, 4, 44, 45, 46, 5, 48, 49, 20, 24, 25, 50, 51, 26, 27, 52}}, 
 {{MySparseMatrix[{6, 28}, {m[1, 1] -> -1/18, m[2, 2] -> -1/2, 
     m[2, 4] -> 1/2, m[3, 2] -> -2/3, m[3, 4] -> 2/3, m[4, 3] -> -1, 
     m[4, 5] -> 1, m[4, 15] -> 1, m[4, 16] -> -1, m[4, 19] -> -1, 
     m[4, 21] -> -1, m[4, 22] -> -1/6, m[5, 1] -> -2/3, m[5, 3] -> 1, 
     m[5, 4] -> -2/3, m[5, 7] -> -5/3, m[5, 8] -> -1, m[5, 9] -> 1, 
     m[5, 10] -> -3/2, m[5, 11] -> -1, m[5, 12] -> -1, m[5, 13] -> 1, 
     m[5, 14] -> -5/3, m[5, 17] -> -1, m[5, 18] -> 1, m[5, 19] -> 1, 
     m[5, 21] -> 1, m[5, 22] -> -2/3, m[5, 23] -> 5/3, m[5, 24] -> 1, 
     m[5, 25] -> -1, m[5, 26] -> -1, m[5, 27] -> 1, m[6, 1] -> 2/3, 
     m[6, 3] -> 1/2, m[6, 4] -> 2/3, m[6, 5] -> -1, m[6, 6] -> -1/2, 
     m[6, 7] -> 5/3, m[6, 10] -> 3/2, m[6, 11] -> 1, m[6, 12] -> 1, 
     m[6, 13] -> -1, m[6, 14] -> 5/3, m[6, 15] -> -1, m[6, 16] -> 1, 
     m[6, 17] -> 1, m[6, 18] -> -1, m[6, 19] -> 1, m[6, 20] -> -1, 
     m[6, 22] -> 5/6, m[6, 23] -> -5/3, m[6, 25] -> 1, m[6, 26] -> 1, 
     m[6, 27] -> -1, m[6, 28] -> -1}], MySparseMatrix[{6, 28}, {}]}, 
  {MySparseMatrix[{12, 29}, {m[1, 1] -> -2/3, m[2, 6] -> 1/2, 
     m[2, 15] -> -1/2, m[3, 10] -> 2/3, m[3, 15] -> -2/3, m[4, 10] -> 1/2, 
     m[4, 15] -> -1/2, m[5, 6] -> -1/2, m[5, 15] -> 1/2, m[6, 9] -> 1/4, 
     m[6, 13] -> -1/2, m[6, 14] -> 1/4, m[6, 21] -> 1/2, m[6, 22] -> 1/2, 
     m[6, 23] -> -1/12, m[7, 9] -> 1/4, m[7, 13] -> -1/2, m[7, 14] -> 1/4, 
     m[7, 21] -> 1/2, m[7, 22] -> 1/2, m[7, 23] -> -1/12, m[8, 3] -> 1/2, 
     m[8, 15] -> -1/2, m[8, 24] -> -1/2, m[9, 3] -> -1/2, m[9, 15] -> 1/2, 
     m[9, 24] -> 1/2, m[10, 5] -> 1/4, m[10, 14] -> -1/4, m[10, 17] -> -1/2, 
     m[10, 19] -> 1/2, m[10, 20] -> 1/2, m[10, 22] -> -1/2, 
     m[10, 23] -> 1/12, m[10, 28] -> 1/2, m[10, 29] -> -1/2, m[11, 1] -> 2/3, 
     m[11, 2] -> 3/2, m[11, 3] -> 5/3, m[11, 4] -> -1, m[11, 5] -> 1/2, 
     m[11, 6] -> 5/3, m[11, 7] -> -1, m[11, 8] -> 1, m[11, 11] -> 1, 
     m[11, 12] -> -1, m[11, 13] -> 1, m[11, 14] -> -1/2, m[11, 15] -> 2/3, 
     m[11, 16] -> 1, m[11, 18] -> -1, m[11, 20] -> 1, m[11, 22] -> -1, 
     m[11, 23] -> 1, m[11, 24] -> -5/3, m[11, 25] -> 1, m[11, 26] -> -1, 
     m[11, 27] -> 1, m[11, 29] -> -1, m[12, 5] -> -1/4, m[12, 14] -> 1/4, 
     m[12, 17] -> 1/2, m[12, 19] -> -1/2, m[12, 20] -> -1/2, 
     m[12, 22] -> 1/2, m[12, 23] -> -1/12, m[12, 28] -> -1/2, 
     m[12, 29] -> 1/2}], MySparseMatrix[{12, 29}, {}]}}, {1, pflip}, 
 {f[3] -> (y[1]^2*y[2]^2*y[4]*y[5]^2)/(y[3]*y[6]*y[7]^2) + 
    (y[1]^2*y[2]^3*y[5]^2*y[8]*y[9])/(y[6]*y[7]^2*y[10]), 
  f[1] -> (y[1]^2*y[2]^2*y[5])/(y[3]*y[6]), 
  f[2] -> (y[1]^2*y[2]^2*y[5])/(y[3]*y[7]), 
  f[4] -> (y[1]^2*y[2]^5*y[3]^2*y[5]^4*y[9]^3)/(y[7]^3*y[10]^3) + 
    (y[1]^2*y[2]^2*y[5]^2*y[11])/(4*y[3]*y[6]*y[7]^3) + 
    (3*y[1]^2*y[2]^4*y[3]*y[5]^3*y[9]^2*y[12])/(4*y[6]*y[7]^3*y[10]^2) + 
    (3*y[1]^2*y[2]^3*y[3]*y[5]^3*y[9]*y[13])/(4*y[6]*y[7]^3*y[10]), 
  f[5] -> (y[1]^2*y[2]*y[5]^2*y[14])/(y[3]*y[6]^2*y[7]^2) - 
    (12*y[1]^2*y[2]^4*y[3]*y[5]^4*y[9]^3)/(y[7]^2*y[10]^2*y[15]) + 
    (3*y[1]^2*y[2]*y[5]^2*y[17]^2*y[18])/(y[7]*y[15]^2*y[16]) + 
    (3*y[1]^2*y[2]^3*y[5]^3*y[20])/(y[6]^2*y[15]^2*y[19]) - 
    (3*y[1]^2*y[2]^3*y[5]^3*y[9]^2*y[21])/(y[6]*y[7]^2*y[10]*y[15]^2), 
  f[6] -> (y[1]^2*y[2]^2*y[5])/y[3], 
  f[8] -> (y[1]^2*y[2]^3*y[5]^2*y[8]*y[9])/(y[6]*y[7]^2*y[10]) + 
    (y[1]^2*y[5]^2*y[22])/(y[3]*y[6]*y[7]^2) + (y[1]^2*y[3]*y[4]*y[5]^2)/
     (y[7]*y[23]), f[15] -> (y[1]^2*y[2]*y[5]*y[24])/(y[3]*y[6]^2), 
  f[7] -> (y[1]^2*y[2]*y[4]*y[5]^2)/(y[3]*y[6]*y[25]) + 
    (y[1]^2*y[2]*y[5]^2*y[26])/(y[6]*y[27]) - (y[1]^2*y[2]^2*y[5]^2)/
     (y[6]*y[28]) + (y[1]^2*y[2]^2*y[4]*y[5]^2*y[17])/(y[6]*y[25]*y[29]), 
  f[12] -> (y[1]^2*y[4]*y[5]^2)/y[7] - (y[1]^2*y[3]*y[4]*y[5]^2)/
     (y[7]*y[23]) + (y[1]^2*y[2]*y[5]*y[9])/y[28] + 
    (y[1]^2*y[2]*y[5]*y[9])/y[30], f[16] -> (y[1]^2*y[2]*y[5]*y[31])/
    (y[3]*y[7]^2), f[11] -> (y[1]^2*y[2]^4*y[3]*y[5]^4*y[9]^3)/
     (y[7]^2*y[10]^2*y[15]) - (3*y[1]^2*y[3]^2*y[5]^2)/(4*y[7]*y[9]*y[23]) + 
    (y[1]^2*y[5]*y[32])/(12*y[3]*y[6]*y[7]^2*y[25]) - 
    (3*y[1]^2*y[2]^3*y[5]^2*y[17]^2)/(4*y[6]*y[25]*y[29]*y[33]) + 
    (y[1]^2*y[5]^2*y[17]^2*y[34]*y[36])/(4*y[9]*y[16]*y[35]^2) + 
    (y[1]^2*y[2]^3*y[5]^2*y[37]*y[38])/(4*y[6]*y[15]^2*y[19]*y[33]) + 
    (y[1]^2*y[2]^3*y[5]^3*y[9]^2*y[39])/(4*y[6]*y[7]^2*y[10]*y[15]^2), 
  f[10] -> (y[1]^2*y[2]^5*y[3]^2*y[5]^4*y[9]^3)/(y[7]^3*y[10]^3) + 
    (3*y[1]^2*y[3]^2*y[5]^2)/(4*y[7]*y[23]^2) + (y[1]^2*y[2]*y[5]^2*y[40])/
     (4*y[3]*y[6]^2*y[7]^3) - (3*y[1]^2*y[3]*y[5]^2*y[41])/(4*y[7]^2*y[23]) + 
    (3*y[1]^2*y[2]^4*y[3]*y[5]^3*y[9]^2*y[42])/(4*y[6]*y[7]^3*y[10]^2) + 
    (3*y[1]^2*y[2]^3*y[5]^2*y[9]*y[43])/(4*y[6]^2*y[7]^3*y[10]), 
  f[14] -> (y[1]^2*y[2]*y[5]^2*y[44])/(y[3]*y[6]^3), 
  f[9] -> (y[1]^2*y[2]*y[5]^2*y[26])/(y[6]*y[27]) - 
    (y[1]^2*y[2]^2*y[5]^2)/(y[6]*y[28]) - (y[1]^2*y[2]^4*y[3]*y[5]^3*y[17]^2)/
     (y[6]*y[25]^2*y[29]^2) - (y[1]^2*y[2]*y[5]^2*y[45])/
     (y[3]*y[6]^2*y[25]^2) - (y[1]^2*y[2]^2*y[5]^2*y[17]*y[46])/
     (y[6]^2*y[25]^2*y[29]), f[13] -> (y[1]^2*y[3]^2*y[5]^2)/(y[7]*y[23]^2) + 
    (y[1]^2*y[2]*y[5]*y[9])/y[28] + (y[1]^2*y[2]*y[5]*y[9])/y[30] - 
    (y[1]^2*y[3]*y[5]^2*y[41])/(y[7]^2*y[23]) + (y[1]^2*y[2]*y[5]^2*y[47])/
     y[7]^2, f[17] -> (y[1]^2*y[2]*y[3]*y[5]^2*y[13])/y[7]^3}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> 1 + ex[2], y[4] -> -1 + 3*ex[2], 
  y[5] -> ex[3], y[6] -> 1 + ex[3], y[7] -> 1 + ex[3] + ex[2]*ex[3], 
  y[8] -> 4 + 9*ex[3] + ex[2]*ex[3] + 4*ex[3]^2 + 4*ex[2]*ex[3]^2, 
  y[9] -> ex[5], y[10] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4] - ex[2]*ex[5], 
  y[11] -> 2 - ex[2] + 3*ex[3] + ex[2]*ex[3] - 2*ex[2]^2*ex[3] + ex[3]^2 + 
    2*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[12] -> -1 + 2*ex[2]*ex[3] + ex[3]^2 + ex[2]*ex[3]^2, 
  y[13] -> -1 - ex[3] + ex[2]*ex[3], 
  y[14] -> -3 - 3*ex[2] + 38*ex[2]^2 - 6*ex[3] - 3*ex[2]*ex[3] + 
    79*ex[2]^2*ex[3] + 38*ex[2]^3*ex[3] - 3*ex[3]^2 + 50*ex[2]^2*ex[3]^2 + 
    47*ex[2]^3*ex[3]^2, y[15] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - 
    ex[3]*ex[5], y[16] -> 1 + ex[4] - ex[5], y[17] -> -1 + ex[5], 
  y[18] -> 1 + ex[2] + ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3] - ex[5] - 
    3*ex[2]*ex[5] - ex[3]*ex[5] - ex[2]*ex[3]*ex[5], 
  y[19] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[20] -> 1 + ex[3] + 3*ex[2]*ex[3] - ex[5] + ex[3]*ex[5] + 2*ex[3]^2*ex[5], 
  y[21] -> -3 - 3*ex[3] + 3*ex[2]*ex[3] + 3*ex[3]^2 + 9*ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 3*ex[3]^3 + 6*ex[2]*ex[3]^3 + 3*ex[2]^2*ex[3]^3 + 
    3*ex[5] + 5*ex[3]*ex[5] - 2*ex[2]*ex[3]*ex[5] + ex[3]^2*ex[5] - 
    3*ex[2]*ex[3]^2*ex[5] - ex[3]^3*ex[5] - ex[2]*ex[3]^3*ex[5], 
  y[22] -> 1 - ex[2] - 2*ex[2]^2 + 4*ex[2]^3 + 2*ex[3] - ex[2]*ex[3] - 
    3*ex[2]^2*ex[3] + ex[2]^3*ex[3] + ex[2]^4*ex[3] + ex[3]^2 - 
    2*ex[2]^2*ex[3]^2 + ex[2]^4*ex[3]^2, y[23] -> 1 + ex[4], 
  y[24] -> 2*ex[2] - ex[3] + 5*ex[2]*ex[3], y[25] -> 1 + ex[2] + ex[3], 
  y[26] -> ex[2] + ex[5], y[27] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + 
    ex[3]*ex[5], y[28] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + 
    ex[3]*ex[5], y[29] -> -(ex[2]*ex[3]) + ex[4] + ex[2]*ex[4] + 
    ex[3]*ex[4] + ex[2]*ex[3]*ex[5], y[30] -> ex[4] - ex[5], 
  y[31] -> -2*ex[2] - ex[3] + ex[2]^2*ex[3], 
  y[32] -> 38*ex[2]^2 + 38*ex[2]^3 + 9*ex[3] + 21*ex[2]*ex[3] + 
    149*ex[2]^2*ex[3] + 184*ex[2]^3*ex[3] + 76*ex[2]^4*ex[3] + 27*ex[3]^2 + 
    78*ex[2]*ex[3]^2 + 315*ex[2]^2*ex[3]^2 + 393*ex[2]^3*ex[3]^2 + 
    205*ex[2]^4*ex[3]^2 + 38*ex[2]^5*ex[3]^2 + 27*ex[3]^3 + 
    93*ex[2]*ex[3]^3 + 287*ex[2]^2*ex[3]^3 + 353*ex[2]^3*ex[3]^3 + 
    170*ex[2]^4*ex[3]^3 + 38*ex[2]^5*ex[3]^3 + 9*ex[3]^4 + 36*ex[2]*ex[3]^4 + 
    83*ex[2]^2*ex[3]^4 + 94*ex[2]^3*ex[3]^4 + 38*ex[2]^4*ex[3]^4, 
  y[33] -> ex[2] + ex[5] + ex[3]*ex[5], y[34] -> -1 - ex[2] + ex[5], 
  y[35] -> -1 - ex[3] - ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[36] -> -3 - 3*ex[2] - 3*ex[3] - 6*ex[2]*ex[3] - 3*ex[2]^2*ex[3] + 
    3*ex[5] + ex[2]*ex[5] + 3*ex[3]*ex[5] + 3*ex[2]*ex[3]*ex[5], 
  y[37] -> -1 + ex[5] + ex[3]*ex[5], 
  y[38] -> 3 + 3*ex[3] + ex[2]*ex[3] - 3*ex[5] - 5*ex[3]*ex[5] - 
    2*ex[3]^2*ex[5], y[39] -> -9 - 9*ex[3] - 3*ex[2]*ex[3] + 9*ex[3]^2 + 
    15*ex[2]*ex[3]^2 + 6*ex[2]^2*ex[3]^2 + 9*ex[3]^3 + 18*ex[2]*ex[3]^3 + 
    9*ex[2]^2*ex[3]^3 + 9*ex[5] + 11*ex[3]*ex[5] - 2*ex[2]*ex[3]*ex[5] - 
    5*ex[3]^2*ex[5] - 9*ex[2]*ex[3]^2*ex[5] - 7*ex[3]^3*ex[5] - 
    7*ex[2]*ex[3]^3*ex[5], y[40] -> -6 + 9*ex[2] + 6*ex[2]^2 - 18*ex[3] - 
    3*ex[2]*ex[3] + 6*ex[2]^2*ex[3] - 6*ex[2]^3*ex[3] - 18*ex[3]^2 - 
    32*ex[2]*ex[3]^2 - 21*ex[2]^2*ex[3]^2 - 6*ex[2]^3*ex[3]^2 + 
    ex[2]^4*ex[3]^2 - 6*ex[3]^3 - 19*ex[2]*ex[3]^3 - 18*ex[2]^2*ex[3]^3 - 
    3*ex[2]^3*ex[3]^3 + 2*ex[2]^4*ex[3]^3 + ex[2]*ex[3]^4 + 
    3*ex[2]^2*ex[3]^4 + 3*ex[2]^3*ex[3]^4 + ex[2]^4*ex[3]^4, 
  y[41] -> 1 - ex[2] + ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[42] -> -3 + 2*ex[2]*ex[3] + 3*ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[43] -> 2 - 5*ex[3] - 5*ex[2]*ex[3] - 14*ex[3]^2 - 13*ex[2]*ex[3]^2 + 
    ex[2]^2*ex[3]^2 - 5*ex[3]^3 - 4*ex[2]*ex[3]^3 + ex[2]^2*ex[3]^3 + 
    2*ex[3]^4 + 4*ex[2]*ex[3]^4 + 2*ex[2]^2*ex[3]^4, 
  y[44] -> 1 - ex[2] + ex[3] + ex[2]*ex[3], 
  y[45] -> 1 - ex[2]^2 + 2*ex[3] + 2*ex[2]*ex[3] + ex[2]^2*ex[3] + ex[3]^2 + 
    2*ex[2]*ex[3]^2, y[46] -> 1 - ex[2]^2 + 2*ex[3] + 3*ex[2]*ex[3] + 
    ex[2]^2*ex[3] + ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[47] -> -1 + ex[3] + ex[2]*ex[3]}}
