#!/usr/bin/env python3

import time, multiprocessing


def f(i):
    time.sleep(1)
    print(i)


def pfor_handler(n, p):
    handlers = []
    with multiprocessing.Pool(processes=p) as pool:
        for i in range(n):
            handler = pool.apply_async(f, (i,))
            handlers.append(handler)

        for handler in handlers:
            handler.wait()


def pfor_join(n, p):
    with multiprocessing.Pool(processes=p) as pool:
        for i in range(n):
            pool.apply_async(f, (i,))

        pool.close()
        pool.join()


def pmap_handler(n, p):
    with multiprocessing.Pool(processes=p) as pool:
        handler = pool.map_async(f, range(n))

        handler.wait()


def pmap_join(n, p):
    with multiprocessing.Pool(processes=p) as pool:
        pool.map_async(f, range(n))

        pool.close()
        pool.join()


class ProcessesHandler:
    def __init__(self, processes):
        self.pool = multiprocessing.Pool(processes=processes)
        self.handlers = []

    def add(self, func, *args):
        handler = self.pool.apply_async(func, args)
        self.handlers.append(handler)

    def join(self):
        for handler in self.handlers:
            handler.wait()


def pclass_handler(n, p):
    procs = ProcessesHandler(p)
    for i in range(n):
        procs.add(f, i)
    procs.join()


class ProcessesJoin:
    def __init__(self, processes):
        self.pool = multiprocessing.Pool(processes=processes)

    def add(self, func, *args):
        self.pool.apply_async(func, args)

    def join(self):
        self.pool.close()
        self.pool.join()


def pclass_join(n, p):
    procs = ProcessesHandler(p)
    for i in range(n):
        procs.add(f, i)
    procs.join()


if __name__ == "__main__":
    n = 4
    p = 2

    # pfor_handler(n, p)
    pfor_join(n, p)
    # pmap_handler(n, p)
    # pmap_join(n, p)
    # pclass_handler(n, p)
    # pclass_join(n, p)
