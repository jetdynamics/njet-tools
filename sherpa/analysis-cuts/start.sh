#!/usr/bin/env bash

l=(0 50 100 200)

cores=$(nproc)
len=${#l[@]}
num=$(($len ** $len))
each=$(($cores / $num))
if [ $each -lt 1 ]; then
    each=1
fi

for a in ${l[@]}; do
    for b in ${l[@]}; do
        for c in ${l[@]}; do
            dir="myy${a}-ptyy${b}-ptj${c}"

            if [ ! -d "${dir}" ]; then
                name="diphoton-${dir}"
                src="${name}.cc"
                nfo="${name}.info"
                pln="diphoton_myy${a}_ptyy${b}_ptj${c}"
                plt="${name}.plot"

                mkdir "${dir}"
                cd "${dir}" || exit 1

                cp ../template/diphoton.cc "${src}"
                cp ../template/diphoton.info "${nfo}"
                cp ../template/diphoton.plot "${plt}"

                perl -pi -e "s|/diphoton/|/diphoton_${pln}|g" "${plt}"

                perl -pi -e "s|^(Name:).*$|\1 ${name}|g" "${nfo}"

                perl -pi -e "s|(if \(Myy <) \d+(\. \* GeV\))|\1 ${a}\2|g" "${src}"
                perl -pi -e "s|(if \(pTyy <) \d+(\. \* GeV\))|\1 ${b}\2|g" "${src}"
                perl -pi -e "s|(if \(j1.pt\(\) <) \d+(\. \* GeV\))|\1 ${c}\2|g" "${src}"
                perl -pi -e "s|\(diphoton\)|(${pln})|g" "${src}"
                perl -pi -e "s|(class )diphoton|\1${pln}|g" "${src}"

                rivet-buildplugin -j ${each} "Rivet${name}.so" "${src}" >"${name}.log" 2>&1 &

                cd .. || exit 1
            fi
        done
    done
done

wait
