---
title: gg $\rightarrow$ g$\gamma \gamma$ using quark-initiated tree to optimise integration --- integrator library
author: Ryan Moodie
date: 18 Feb 2020
---

## Usage

### Running

* Run on IPPP system
* Generates channels (integrator library) using quark initiated tree
    * `make`
    * ie. detect resonances
* Then can continue with [3g2A-opt](https://gitlab.com/jetdynamics/njet-tools/-/tree/master/sherpa/3g2A-opt)

### Cleaning

* Delete temporary files and get rid of output with `make clean`
* Also get rid of pdfs with `make wipe`
