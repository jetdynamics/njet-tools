#!/usr/bin/env python3
# coding=UTF-8

import pickle

import matplotlib.pyplot


class Plots:
    def __init__(self):
        with open("sijs.pickle", "rb") as f:
            self.sijs = pickle.load(f)

        with open("res.pickle", "rb") as f:
            (
                self.nn,
                self.njet,
                self.logratio,
                self.err_abs,
                self.err_rel,
                self.cut,
            ) = pickle.load(f)

        self.ss = len(self.cut) - sum(self.cut)
        self.used_sijs = [a for a, c in zip(self.sijs, self.cut) if not c]

    def all_sij(self):

        fig, ax = matplotlib.pyplot.subplots()

        ax.hist(self.sijs, bins=100)
        ax.set_xlabel("sij")
        ax.set_ylabel("Frequency")
        ax.set_title(f"All sijs. # PS points: {len(self.cut)}")

        # fig.savefig("diff.pdf", bbox_inches="tight", format="pdf")
        fig.savefig("sij.png", bbox_inches="tight", format="png", dpi=300)

    def cut_sij(self):
        fig, ax = matplotlib.pyplot.subplots()

        cut_sijs = [a for a, c in zip(self.sijs, self.cut) if c]

        ax.hist(cut_sijs, bins=100)
        ax.set_xlabel("sij")
        ax.set_ylabel("Frequency")
        ax.set_title(f"Cut sijs. # PS points: {self.ss}")

        # fig.savefig("diff.pdf", bbox_inches="tight", format="pdf")
        fig.savefig("cut_sij.png", bbox_inches="tight", format="png", dpi=300)

    def sij_2d(self, x, y):
        fig, ax = matplotlib.pyplot.subplots()

        ax.hist2d(self.sijs[x::10], self.sijs[y::10], bins=100)

        fig.savefig("sij_2d.png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":
    p = Plots()
    # p.all_sij()
    # p.cut_sij()
    # p.sij_2d(0,1)
