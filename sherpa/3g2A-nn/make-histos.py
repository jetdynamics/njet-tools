#!/usr/bin/env python
# coding=UTF-8

import math

import matplotlib.pyplot
import matplotlib.ticker


class MakeHistos:
    def __init__(self, datafilename, errorfilename):

        with open(datafilename, "r") as datafile:
            raw = []
            tmp = []
            for line in datafile.readlines():
                if line == "\n":
                    raw.append(tmp)
                    tmp = []
                else:
                    tmp.append(line.rstrip("\n").split(" "))

        with open(errorfilename, "r") as errfile:
            errs = [line.rstrip("\n").split(" ")[2] for line in errfile.readlines()]

        titles = []
        xs = []
        ys = []
        for h in raw:
            titles.append(h[0][0].split("/")[-1])
            xs.append([float(l[0]) for l in h[1:]])
            ys.append([float(l[1]) for l in h[1:]])

        for i in range(len(titles)):
            print(f"{i} plotting {titles[i]}")
            fig, ax = matplotlib.pyplot.subplots()

            ax.hist(xs[i], weights=ys[i], bins=len(xs[i]), histtype="step", log=True, label="NN")
            # ax.set_xlabel("Fractional standard error")
            # ax.set_ylabel("Percentage of sample")
            ax.set_title(titles[i])
            # ax.set_major_formatter(matplotlib.ticker.PercentFormatter(1))
            ax.legend(loc="best")

            # # fig.savefig("err.pdf", bbox_inches="tight", format="pdf")
            fig.savefig(titles[i] + ".png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":

    dp = MakeHistos("histos", "errs")
