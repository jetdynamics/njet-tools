#!/usr/bin/env python3
# coding=UTF-8

import math


class Signs:
    def __init__(self, filename):

        with open(filename, "r") as datafile:
            data = [
                float(line.rstrip("\n")) for line in datafile.readlines()
            ]

        print(sum(data))
        print(sum(data)/len(data))

        pos = [datum for datum in data if datum > 0]
        print(sum(pos))
        print(sum(pos)/len(pos))
        print(max(pos))

        neg = [datum for datum in data if datum < 0]
        print(sum(neg))
        print(sum(neg)/len(neg))
        print(min(neg))

        print(len(pos)/len(data))
        print(len(neg)/len(data))


if __name__ == "__main__":
    s = Signs("diffs")
