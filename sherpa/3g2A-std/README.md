---
title: gg $\rightarrow$ g$\gamma \gamma$ with standard analysis
author: Ryan Moodie
date: 21 Jan 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2A/ATLAS_2017_I1591327/index.html>

## Usage

### Running

* Uses interface from NJET0q3gAA
* Files are symlinked here

#### All in one

* `make` will do all calculations
Use this to set up a run on the IPPP system.
Use your desktop or empty workstation as you'll use all cores during first stage (ME & integration).

#### Step by step

* First generate process with `make Process/`
* Run matrix element generation and integration (grid optimisation) with `make Results.db`.

> The above first two steps are then used by [3g2A-ctm](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/3g2A-ctm)

* Run event generation and analysis with:
    * `Sherpa -f Run.dat` for serial execution on laptop.
    * `make analysis`, or `./analyses.sh <number of jobs to run>`, to submit parallel batch jobs on the IPPP system. 
        * This uses different `-R <rseed>` flags, `-A <output filename>` flags where `<output filename>` omits the `.yoda` suffix, and `-e <events>` flags where `<events>`=total number of events/number of jobs. 
        * The separate files can then be stitched together with `make Analysis.yoda` or `yodamerge -o Analysis.yoda part.*.yoda` on the IPPP system.
* Show plots with `make show` on laptop or `make copy` on IPPP system after merging analyses.

### Cleaning

* Delete temporary files created by batch execution with `make flush`.
* Get rid of compilations with `make clean`
* Get rid also of matrix element generation and integration (grid optimisation) with `make reset`
* Also get rid of pdfs with `make wipe`

## Details

* Uses PDFs
* Uses jets (FastJet, antikt)
* Does nothing else fancy with final states (no showering, hadronisation)

## Problems

* Currently `yodamerge` doesn't seem to work with Python 3 on my laptop, hence the serial execution.
