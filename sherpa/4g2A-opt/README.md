---
title: gg $\rightarrow$ gg$\gamma \gamma$ using quark-initiated tree to optimise integration - calculation
author: Ryan Moodie
date: 18 Feb 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/4g2A-qq/>

## Usage

### Running

* Run on IPPP system
* Uses ME library and order file from [`../NJET0q4gAA`](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q4gAA)
    * Run `make libSherpaNJET0q4gAA.so` and `make OLE_contract_4g2A.lh` there first
    * Files are symlinked here
* Uses optimised integrator library from [`../4g2A-qq`](https://gitlab.com/jetdynamics/njet-tools/-/tree/master/sherpa/4g2A-qq) 
    * Run `make` there first
    * Files are symlinked here
* Then do grid optimisation using actual ME but with the optimised integrator library
    * `make Results.db CORES=<number>`
    * Results are included in this repo
* Uses [custom Rivet analysis](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/analysis-diphoton) from `../analysis-diphoton`
    * Run `make` there first
* Then do analysis here: 
    * On IPPP system,
        * `make analysis` to use batch system, or `./local-analyses <init seed> <num runs> <total num events>` to run on a single machine
            * optionally `make analysis JOBS=200 EVENTS=100000 START=1` for example
        * `make Analysis.yoda` to compile results of parallel runs into a single file
        * `make rivet-plots/` to produce plots
        * `make copy` to copy plots to personal IPPP webpage

### Cleaning

* Delete temporary files with `make flush`.
* Get rid also of output with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`
