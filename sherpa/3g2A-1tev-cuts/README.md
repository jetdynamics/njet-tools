---
title: gg $\rightarrow$ g$\gamma \gamma$ at 1 TeV---cuts
author: Ryan Moodie
date: 8 Sep 2020
---

* Like <https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/3g2A-1tev> but trying different cuts
