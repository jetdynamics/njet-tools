#!/usr/bin/env bash

# To run jobs (on a single machine), do
# ./analysis.sh <max number of events to use>

for f in ../hepmc/Events.*.hepmc2g; do
    g=${f#../hepmc/Events.}
    h=${g%.hepmc2g}
    rivet --analysis-path ../../analysis-cuts -a diphoton -o Analysis.${h}.yoda --nevts $1 --ignore-beams $f >${h}.log 2>&1 &
done

wait
