# ppAA minimal example

Uses Sherpa matrix element generator.
Has bells and whistles turned on:
* Showering
* PDFs
* Fragmentation/hadronisation?
Analysis/event generation takes a long time.

Run calculation with `make`.
Show plots with `make show`.
