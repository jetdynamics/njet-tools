LOCAL?=$(HOME)/local
NJET_LOCAL?=$(LOCAL)/njet

CXX?=g++

CXXFLAGS=-g -O2 -pedantic -Wall -Wextra -fPIC -DPIC -std=c++17
CPPFLAGS=-I$(shell Sherpa-config --incdir)
CPPFLAGS+=-I$(NJET_LOCAL)/include
# DEFS=-DJADE_CUT # add a JADE cut
# DEFS+=-DCUT_ZERO # add a cut on negative |A|^2 results
# DEFS+=-DDEBUG # turn on verbose debugging output to terminal
# DEFS+=-DTEST # test NN against a single known point
# DEFS+=-DMOMS # record phase space point momenta
# DEFS+=-DSIJS # calculate momentum invariants and record
# DEFS+=-DERRS # calculate NN error and record
# DEFS+=-DDIFF # also calculate NJet results and record all results
# DEFS+=-DTIMING # recording runtime information
# DEFS+=-DNJET # do NJet calculation and ignore everything else (except PS & res recording)
# DEFS+=-DRES # record NJet results
LDFLAGS=$(shell Sherpa-config --ldflags)
LDFLAGS+=-L$(NJET_LOCAL)/lib
LDFLAGS+=-lnjet2 -lqd

.PHONY: clean wipe all

all: libSherpaNN3g2A.so libSherpaNJet3g2A.so ex

NN3g2A_Interface.o: NN3g2A_Interface.cpp NN3g2A_Interface.hpp Makefile
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS) $(DEFS)

NJet3g2A_Interface.o: NN3g2A_Interface.cpp NN3g2A_Interface.hpp Makefile
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS) -DNJET

model_fns.o: model_fns.cpp model_fns.h Makefile
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS) $(DEFS)

ex.o: ex.cpp Makefile
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS) $(DEFS)

# Generate the library file for the Sherpa/NN3g2A interface
libSherpaNN3g2A.so: NN3g2A_Interface.o model_fns.o
	$(CXX) -shared -o $@ $^ $(LDFLAGS) $(LIBS)

# Generate the library file for the Sherpa/NJet3g2A interface
libSherpaNJet3g2A.so: NJet3g2A_Interface.o model_fns.o
	$(CXX) -shared -o $@ $^ $(LDFLAGS) $(LIBS)

ex: ex.o model_fns.o
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS)

# Remove compiled files
clean:
	rm -rf *.so *.o OLE_contract_3g2A.lh ex

# Remove everything
wipe: clean
	rm *.pdf

include ../../Makefile.inc
