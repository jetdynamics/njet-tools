#ifndef NN3G2A_INTERFACE_HPP
#define NN3G2A_INTERFACE_HPP

#include <array>
#include <chrono>
#include <complex>
#include <string>
#include <vector>

#include "ATOOLS/Org/Run_Parameter.H"
#include "MODEL/Main/Model_Base.H"
#include "MODEL/Main/Running_AlphaS.H"
#include "PHASIC++/Process/ME_Generator_Base.H"
#include "PHASIC++/Process/Tree_ME2_Base.H"

#include "model_fns.h"

namespace NN3g2A {

constexpr static int legs { 5 };
constexpr static int d { 4 };

class SquaredMatrixElement {
public:
    SquaredMatrixElement();
#ifdef TIMING
    ~SquaredMatrixElement();
#endif
    void PrintSummary() const;
    double Calculate(const ATOOLS::Vec4D_Vector& point);

private:
    const double zero; // for BLHA interface complex parameter entry
    static constexpr int n { 5 };        // momenta fifth entry is mass

    const double m_alpha;
    const double m_alphas;
    const double m_mur;

    const double delta;
    const double s_com;

    // n.b. there is an additional FKS pair for the cut network (for non-divergent regions)
    static constexpr int pairs { 9 };
    static constexpr int training_reruns { 20 };

    const std::string cut_dirs;
    const std::string model_base;

    std::array<std::string, training_reruns> model_dirs;
    const std::array<std::string, pairs> pair_dirs;

    std::vector<std::vector<std::vector<double>>> metadatas;
    std::array<std::array<std::string, pairs + 1>, training_reruns> model_dir_models;
    std::vector<std::vector<nn::KerasModel>> kerasModels;

    const double x;

    const std::string momfile;
    const std::string resfile;
    const std::string kinfile;
    const std::string timefile;

#ifdef TIMING
    int counter;
    std::vector<std::chrono::duration<double>> nndur;
    std::vector<std::chrono::duration<double>> njdur;
#endif
};

class Interface : public PHASIC::ME_Generator_Base {
public:
    // constructor
    Interface();

    // member functions
    bool Initialize(
        const std::string& path,
        const std::string& file,
        MODEL::Model_Base* const model,
        BEAM::Beam_Spectra_Handler* const beam,
        PDF::ISR_Handler* const isr);

    PHASIC::Process_Base*
    InitializeProcess(const PHASIC::Process_Info& pi, bool add);

    int PerformTests();

    bool NewLibraries();

    void SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs);

    ATOOLS::Cluster_Amplitude*
    ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode);
};

class Process : public PHASIC::Tree_ME2_Base {
protected:
    SquaredMatrixElement m_me;

public:
    Process(
        const PHASIC::Process_Info& pi,
        const ATOOLS::Flavour_Vector& flavs,
        const bool swap,
        const bool anti);

    double Calc(const ATOOLS::Vec4D_Vector& p);

    int OrderQCD(const int& id);

    int OrderEW(const int& id);
};

} // End namespace NN3g2A

#endif // NN3G2A_INTERFACE_HPP
