#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <complex>
#include <ctime>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#if defined(DIFF) || defined(NJET)
#include "njet.h"
#endif

#include "NN3g2A_Interface.hpp"
#include "model_fns.h"

#define NN_MODEL "1M_new_sherpa_1e-16_float64"

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

// SquaredMatrixElement class constructor and member implementations

NN3g2A::SquaredMatrixElement::SquaredMatrixElement()
    : zero(0.)
    , m_alpha(1. / 137.035999084)
    , m_alphas(0.118)
    , m_mur(91.188)
    , delta(2e-2)
    , s_com(5e5)
    , cut_dirs("cut_0.02/")
    , model_base("./models/" + std::string(NN_MODEL) + "/")
    , model_dirs()
    , pair_dirs({
          "pair_0.02_0/",
          "pair_0.02_1/",
          "pair_0.02_2/",
          "pair_0.02_3/",
          "pair_0.02_4/",
          "pair_0.02_5/",
          "pair_0.02_6/",
          "pair_0.02_7/",
          "pair_0.02_8/",
      })
    , metadatas(training_reruns, std::vector<std::vector<double>>(pairs + 1, std::vector<double>(10)))
    , kerasModels(training_reruns, std::vector<nn::KerasModel>(pairs + 1))
    , x(1e-2)
    , momfile("moms")
    , resfile("res")
    , kinfile("sijs")
    , timefile("time")
#ifdef TIMING
    , counter()
    , nndur()
    , njdur()
#endif
{
    std::generate(model_dirs.begin(), model_dirs.end(), [n = 0]() mutable { return std::to_string(n++) + "/"; });
#ifndef NJET
#ifdef TIMING
    const std::chrono::high_resolution_clock::time_point t1 { std::chrono::high_resolution_clock::now() };
#endif
    for (int i { 0 }; i < training_reruns; ++i) {
        // Near networks
        for (int j { 0 }; j < pairs; ++j) {
            std::string metadata_file { model_base + model_dirs[i] + pair_dirs[j] + "dataset_metadata.dat" };
            std::vector<double> metadata { nn::read_metadata_from_file(metadata_file) };
            for (int k { 0 }; k < 10; ++k) {
                metadatas[i][j][k] = metadata[k];
            };
            model_dir_models[i][j] = model_base + model_dirs[i] + pair_dirs[j] + "model.nnet";
#ifdef DEBUG
            std::cout << "Loading from: " << model_dir_models[i][j] << '\n';
#endif
            kerasModels[i][j].load_weights(model_dir_models[i][j]);
        };

        // Cut networks
        std::string metadata_file { model_base + model_dirs[i] + cut_dirs + "dataset_metadata.dat" };
        std::vector<double> metadata { nn::read_metadata_from_file(metadata_file) };
        for (int k { 0 }; k < 10; ++k) {
            metadatas[i][pairs][k] = metadata[k];
        };
        model_dir_models[i][pairs] = model_base + model_dirs[i] + cut_dirs + "model.nnet";
#ifdef DEBUG
        std::cout << "Loading from: " << model_dir_models[i][pairs] << '\n';
#endif
        kerasModels[i][pairs].load_weights(model_dir_models[i][pairs]);
    }
#ifdef DEBUG
    std::cout << "# Initialised neural net ensemble for 3g2A SqME\n";
#endif
#ifdef TIMING
    {
        const std::chrono::high_resolution_clock::time_point t2 { std::chrono::high_resolution_clock::now() };
        const std::chrono::milliseconds dur { std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1) };
        std::ofstream o(timefile, std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
        o << "NN initialisation: " << dur.count() << " ms\n";
    }
#endif
#endif
#if defined(DIFF) || defined(NJET)
    {
#ifdef TIMING
        const std::chrono::high_resolution_clock::time_point t1 { std::chrono::high_resolution_clock::now() };
#endif
        const std::string f { "OLE_contract_" + std::to_string(NN3g2A::legs - 2) + "g2A.lh" };
        const char* contract { f.c_str() };
        int status;
        OLP_Start(contract, &status);
        assertm(status, "There seems to be a problem with the contract file.");
#ifdef DEBUG
        std::cout << "# Initialised NJet BLHA interface for 3g2A SqME\n";
#endif
#ifdef TIMING
        {
            const std::chrono::high_resolution_clock::time_point t2 { std::chrono::high_resolution_clock::now() };
            const std::chrono::milliseconds dur { std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1) };
            std::ofstream o(timefile, std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            o << "NJet initialisation: " << dur.count() << " ms\n";
            // micro=\u03BC
        }
#endif
    }
#endif
#ifdef DEBUG
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);
#endif
}

#ifdef TIMING
NN3g2A::SquaredMatrixElement::~SquaredMatrixElement()
{
    std::cout << "# Recording timing information\n";
    std::ofstream o(timefile, std::ios::app);
    o.setf(std::ios_base::scientific);
    o.precision(16);

    const std::chrono::high_resolution_clock::time_point t { std::chrono::system_clock::now() };
    const std::time_t tt { std::chrono::system_clock::to_time_t(t) };
    o << "Current time: " << std::ctime(&tt);

    o << "Number of evaluations: " << counter << '\n';

    const std::chrono::duration<double> nnTot { std::accumulate(nndur.cbegin(), nndur.cend(), std::chrono::duration<double>()) };
    const std::chrono::seconds nnTotUnits { std::chrono::duration_cast<std::chrono::seconds>(nnTot) };
    o << "Total NN eval time: " << nnTotUnits.count() << " s\n";
    const std::chrono::milliseconds nnAv { std::chrono::duration_cast<std::chrono::milliseconds>(nnTot / counter) };
    o << "Average NN eval time: " << nnAv.count() << " ms\n";
    const std::chrono::microseconds nnMin { std::chrono::duration_cast<std::chrono::microseconds>(*std::min_element(nndur.cbegin(), nndur.cend())) };
    o << "Min NN eval time: " << nnMin.count() << " \u03BCs\n";
    const std::chrono::milliseconds nnMax { std::chrono::duration_cast<std::chrono::milliseconds>(*std::max_element(nndur.cbegin(), nndur.cend())) };
    o << "Max NN eval time: " << nnMax.count() << " ms\n";

    const std::chrono::duration<double> njTot { std::accumulate(njdur.cbegin(), njdur.cend(), std::chrono::duration<double>()) };
    const std::chrono::seconds njTotUnits { std::chrono::duration_cast<std::chrono::seconds>(njTot) };
    o << "Total NJet eval time: " << njTotUnits.count() << " s\n";
    const std::chrono::milliseconds njAv { std::chrono::duration_cast<std::chrono::milliseconds>(njTot / counter) };
    o << "Average NJet eval time: " << njAv.count() << " ms\n";
    const std::chrono::microseconds njMin { std::chrono::duration_cast<std::chrono::microseconds>(*std::min_element(njdur.cbegin(), njdur.cend())) };
    o << "Min NJet eval time: " << njMin.count() << " \u03BCs\n";
    const std::chrono::milliseconds njMax { std::chrono::duration_cast<std::chrono::milliseconds>(*std::max_element(njdur.cbegin(), njdur.cend())) };
    o << "Max NJet eval time: " << njMax.count() << " ms\n";

    o << '\n';
}
#endif

void NN3g2A::SquaredMatrixElement::PrintSummary() const
{
    msg_Info() << "Using NN3g2A interface with the following parameters:" << '\n'
               << "  mur = " << m_mur << '\n'
               << "  alpha = " << m_alpha << '\n'
               << "  alpha_s = " << m_alphas << '\n'
               << "  ----------------------------------------" << '\n';
}

// code to compute amplitude here
#ifdef TEST
// double NN3g2A::SquaredMatrixElement::Calculate(const double pointold[NN3g2A::legs][NN3g2A::d])
double NN3g2A::SquaredMatrixElement::Calculate(const ATOOLS::Vec4D_Vector& pointold)
#else
// double NN3g2A::SquaredMatrixElement::Calculate(const double point[NN3g2A::legs][NN3g2A::d])
double NN3g2A::SquaredMatrixElement::Calculate(const ATOOLS::Vec4D_Vector& point)
#endif
{
#ifdef TEST
    const int pspoints { 2 };
    const double Momenta[pspoints][NN3g2A::legs][NN3g2A::d] {
        { { 500., 0, 0., 500. },
            { 500., 0., 0., -500. },
            { 478.90769254, 179.71954662, -115.72193182, 428.55792728 },
            { 89.06015462, -76.15029226, 43.54647889, -15.38012701 },
            { 432.03215284, -103.56925437, 72.17545292, -413.17780026 } },
        { { 500., 0., 0., 500. },
            { 500., 0., 0., -500. },
            { 126.69820582, 120.01853098, -31.03838326, 26.16498318 },
            { 397.08701945, 157.11405855, -345.48658337, -116.75741663 },
            { 476.21477472, -277.13258953, 376.52496662, 90.59243344 } }
    };
    const int python_cut_near[pspoints] { 1, 0 };
    const double python_check[pspoints] { 0.01885435257459624, 0.04757045055113991 };
    const double python_outputs[pspoints] { 8.79497694672e-07, 1.62700327877e-08 };

    //const int a { 0 };
    //const double point[5][4]
    //    { { 500., 0, 0., 500. },
    //        { 500., 0., 0., -500. },
    //        { 478.90769254, 179.71954662, -115.72193182, 428.55792728 },
    //        { 89.06015462, -76.15029226, 43.54647889, -15.38012701 },
    //        { 432.03215284, -103.56925437, 72.17545292, -413.17780026 } };

    const int a { 1 };
    const double point[NN3g2A::legs][NN3g2A::d] { { 500., 0., 0., 500. },
        { 500., 0., 0., -500. },
        { 126.69820582, 120.01853098, -31.03838326, 26.16498318 },
        { 397.08701945, 157.11405855, -345.48658337, -116.75741663 },
        { 476.21477472, -277.13258953, 376.52496662, 90.59243344 } };
#endif

#ifdef MOMS
    {
        std::ofstream o(momfile, std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
        for (int l { 0 }; l < NN3g2A::legs; ++l) {
            for (int mu { 0 }; mu < NN3g2A::d; ++mu) {
                // Convert from all-outgoing to 2->(mul-2) convention
                // mom *= (++j < 3) ? -1 : 1;
                o << point[l][mu] << " ";
            }
            o << '\n';
        }
        o << '\n';
    }
#endif

#if defined(ERRS) || defined(DIFF)
    bool cut { false };
#endif

#if defined(JADE_CUT) || defined(SIJS)
    {
#ifdef SIJS
        std::ofstream o(kinfile, std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
#endif
        for (int j { 0 }; j < NN3g2A::legs; ++j) {
            for (int k { j + 1 }; k < NN3g2A::legs; ++k) {
                const double hsij { (point[j][0] * point[k][0] - (point[j][1] * point[k][1] + point[j][2] * point[k][2] + point[j][3] * point[k][3])) };
#ifdef SIJS
                o << 2. * hsij << " ";
#endif
#ifdef JADE_CUT
                if (hsij / s_com < x) {
#if defined(ERRS) || defined(DIFF)
                    cut = true;
#else
                    return 1e-64;
#endif
                }
#endif
            }
        }
#ifdef SIJS
        o << '\n';
#endif
    }
#endif

#ifdef TIMING
    ++counter;
    const std::chrono::high_resolution_clock::time_point nnt1 { std::chrono::high_resolution_clock::now() };
#endif

#ifndef NJET
    std::array<double, training_reruns> results;

    // moms is an vector of training_reruns results, each of which is an vector of FKS pairs results, each of which is an vector of flattened momenta
    std::vector<std::vector<std::vector<double>>> moms(training_reruns, std::vector<std::vector<double>>(pairs + 1, std::vector<double>(NN3g2A::legs * NN3g2A::d)));

    // flatten momenta
    for (int p { 0 }; p < NN3g2A::legs; ++p) {
        for (int mu { 0 }; mu < NN3g2A::d; ++mu) {
            // standardise input
#ifdef DEBUG
            std::cout << point[p][mu] << " ";
#endif
            for (int k { 0 }; k < training_reruns; ++k) {
                for (int j { 0 }; j <= pairs; ++j) {
                    moms[k][j][p * NN3g2A::d + mu] = nn::standardise(point[p][mu], metadatas[k][j][mu], metadatas[k][j][NN3g2A::d + mu]);
                }
                moms[k][pairs][p * NN3g2A::d + mu] = nn::standardise(point[p][mu], metadatas[k][pairs][mu], metadatas[k][pairs][NN3g2A::d + mu]);
            }
        }
#ifdef DEBUG
        std::cout << '\n';
#endif
    }

#ifdef DEBUG
    std::cout << "\nChecking how near we are\n";
#endif

    // cut/near check
    int cut_near { 0 };
    for (int j { 0 }; j < legs - 1; ++j) {
        for (int k { j + 1 }; k < legs; ++k) {
            const double prod { point[j][0] * point[k][0] - (point[j][1] * point[k][1] + point[j][2] * point[k][2] + point[j][3] * point[k][3]) };
            const double dist { prod / s_com };
#ifdef DEBUG
            std::cout << "Distance is: " << dist << '\n';
#endif
            if (dist < delta) {
                cut_near += 1;
            }
        }
    }

#ifdef DEBUG
    std::cout << "C++    cut/near check is: " << (cut_near >= 1) << '\n';
#endif

    // inference
    for (int j { 0 }; j < training_reruns; ++j) {
        if (cut_near >= 1) {
            // the point is near an IR singularity
            // infer over all FKS pairs
            results[j] = 0;
            for (int k { 0 }; k < pairs; ++k) {
                const double result { kerasModels[j][k].compute_output(moms[j][k])[0] };
                const double result_pair { nn::destandardise(result, metadatas[j][k][8], metadatas[j][k][9]) };
                results[j] += result_pair;
            }
        } else {
            // the point is in a non-divergent region
            // use the 'cut' network which is the final entry in the pair network
            const double result { kerasModels[j][pairs].compute_output(moms[j][pairs])[0] };
            results[j] = nn::destandardise(result, metadatas[j][pairs][8], metadatas[j][pairs][9]);
        }
    }

    const double mean { std::accumulate(results.cbegin(), results.cend(), 0.) / training_reruns };
#ifdef TIMING
    const std::chrono::high_resolution_clock::time_point nnt2 { std::chrono::high_resolution_clock::now() };
    nndur.push_back(nnt2 - nnt1);
#endif

#ifdef CUT_ZERO
    if (mean < 0.) {
#if defined(ERRS) || defined(DIFF)
        cut = true;
#else
        return 0.;
#endif
    }
#endif

#ifdef DEBUG
    std::cout << "Result = " << mean << '\n';
#endif
#endif
#if defined(DIFF) || defined(NJET)
#ifdef TIMING
    const std::chrono::high_resolution_clock::time_point njt1 { std::chrono::high_resolution_clock::now() };
#endif
    double out[11];
    {
        double LHMomenta[NN3g2A::legs * n];
        for (int p { 0 }; p < NN3g2A::legs; ++p) {
            for (int mu { 0 }; mu < NN3g2A::d; ++mu) {
                LHMomenta[mu + p * n] = point[p][mu];
            }
            // Set masses
            LHMomenta[d + p * n] = 0.;
        }

        int alphasReturnStatus;
        OLP_SetParameter("alphas", &m_alphas, &zero, &alphasReturnStatus);
        assert(alphasReturnStatus == 1);

        // set alpha QED (answer changes if this changed, so does something)
        int alphaReturnStatus;
        OLP_SetParameter("alpha", &m_alpha, &zero, &alphaReturnStatus);
        assert(alphaReturnStatus == 1);

        double acc { 0. };
        const int channel { 1 };
        OLP_EvalSubProcess2(&channel, LHMomenta, &m_mur, out, &acc);
    }
    const double njet_ans { out[4] };
    // const double njet_err { out[10] }; // relative
#ifdef TIMING
    const std::chrono::high_resolution_clock::time_point njt2 { std::chrono::high_resolution_clock::now() };
    njdur.push_back(njt2 - njt1);
#endif
#endif

#if defined(ERRS) || defined(DIFF) || (defined(RES) && defined(NJET))
    {
        std::ofstream o(resfile, std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
#ifndef NJET
        o << mean;
#endif

        o << " " << njet_ans;

#ifdef DIFF
        // ln(|nn/njet|) nn njet nn_err_abs nn_err_fractional bool(cut)
        const double diff { std::log(std::abs(mean / njet_ans)) };
        o << " " << diff;
#endif
#ifdef ERRS
        std::for_each(results.begin(), results.end(), [mean](double& r) { r = std::pow(r - mean, 2); });
        // standard error = sample standard deviation / sqrt(N)
        const double err { std::sqrt(std::accumulate(results.cbegin(), results.cend(), 0.) / ((training_reruns - 1) * training_reruns)) };
        o << " " << err << " " << std::abs(err / mean);
#endif
#ifndef NJET
        o << " " << cut;
#endif
        o << '\n';
    }

#endif

#ifdef TEST
    std::cout << "Python Loop( 0) = " << python_outputs[a] << std::endl;
    std::cout << "C++    Loop( 0) = " << mean << std::endl;
#endif

#ifdef NJET
    return njet_ans;
#else

#if defined(ERRS) || defined(DIFF)
    return cut ? 1e-64 : mean;
#else
    return mean;
#endif

#endif
}

// Interface class constructor and member implementations

NN3g2A::Interface::Interface()
    : ME_Generator_Base("NN3g2A")
{
}

bool NN3g2A::Interface::Initialize(
    const std::string& path, const std::string& file,
    MODEL::Model_Base* const model,
    BEAM::Beam_Spectra_Handler* const beam,
    PDF::ISR_Handler* const isr)
{
    return true;
}

PHASIC::Process_Base*
NN3g2A::Interface::InitializeProcess(const PHASIC::Process_Info& pi, bool add)
{
    return NULL;
}

int NN3g2A::Interface::PerformTests()
{
    return 1;
}

bool NN3g2A::Interface::NewLibraries()
{
    return false;
}

void NN3g2A::Interface::SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs) { }

ATOOLS::Cluster_Amplitude*
NN3g2A::Interface::ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode)
{
    return NULL;
}

// Process class constructor and member implementations

NN3g2A::Process::Process(
    const PHASIC::Process_Info& pi,
    const ATOOLS::Flavour_Vector& flavs,
    const bool swap,
    const bool anti)
    : Tree_ME2_Base(pi, flavs)
    , m_me()
{
    // m_me.SetParameter("alpha", AlphaQED());
    // m_me.RecalcDependentParameters();

    // Data_Reader reader(" ", ";", "#", "=");
    // const double defaultAccuracy { 1e-5 };
    // const double accu { reader.GetValue<double>("NJET_ACCURACY", defaultAccuracy) };

    // m_me.SetParameter("Accuracy", accu);
    m_me.PrintSummary();

    // rpa->gen.AddCitation(1, std::string("<Description of calculation> from \\cite{xxx:2019yy}"));
}

double NN3g2A::Process::Calc(const ATOOLS::Vec4D_Vector& p)
{
    if (p.size() != NN3g2A::legs)
        THROW(fatal_error, "Wrong process.");
    // double moms[NN3g2A::legs][NN3g2A::d];
    // for (size_t i { 0 }; i < NN3g2A::legs; ++i)
    //     for (size_t j { 0 }; j < NN3g2A::d; ++j)
    //         moms[i][j] = p[i][j];
    return m_me.Calculate(p);
}

int NN3g2A::Process::OrderQCD(const int& id)
{
    return NN3g2A::legs - 2;
}

int NN3g2A::Process::OrderEW(const int& id)
{
    return 2;
}

// End class member implementations

DECLARE_GETTER(NN3g2A::Interface, "NN3g2A", PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key);

PHASIC::ME_Generator_Base*
ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NN3g2A::Interface>::
operator()(const PHASIC::ME_Generator_Key& key) const
{
    return new NN3g2A::Interface();
}

void ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NN3g2A::Interface>::
    PrintInfo(std::ostream& str, const size_t width) const
{
    str << "Interface to the NN3g2A calculation";
}

using namespace PHASIC;

DECLARE_TREEME2_GETTER(NN3g2A::Process, "NN3g2A::Process")

PHASIC::Tree_ME2_Base*
ATOOLS::Getter<PHASIC::Tree_ME2_Base, PHASIC::Process_Info, NN3g2A::Process>::
operator()(const PHASIC::Process_Info& pi) const
{
    assert(pi.m_loopgenerator == "NN3g2A");
    assert(MODEL::s_model->Name() == std::string("SM"));
    assert(pi.m_fi.m_nloewtype == nlo_type::lo);
    assert(pi.m_fi.m_nloqcdtype == nlo_type::lo);
    Flavour_Vector fl(pi.ExtractFlavours());
    // check for g g  -> g a a
    assert(fl[0].Kfcode() == kf_gluon && fl[1].Kfcode() == kf_gluon && fl[4].Kfcode() == kf_gluon
        && fl[2].Kfcode() == kf_photon && fl[3].Kfcode() == kf_photon);
    return new NN3g2A::Process(pi, fl, 0, 0);
}
