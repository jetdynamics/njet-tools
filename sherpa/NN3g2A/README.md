---
title: gg $\rightarrow$ g$\gamma \gamma$ --- NN/Sherpa interface
author: Ryan Moodie
date: 11 Jun 2020
---

Using [Joe's work on neural nets](https://github.com/JosephPB/n3jet/tree/master/c%2B%2B_calls).

## Usage

### Dependencies

* First run `make OLE_contract_3g2A.lh` in [NJET0q3gAA](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q3g3AA)

### Running

* `make` will do all compiles: contract file and ME library

### Cleaning

* Get rid of compilations with `make clean`
* Also get rid of pdfs with `make wipe`
