---
title: gg $\rightarrow$ g$\gamma \gamma$ from a neural net---cuts
author: Ryan Moodie
date: 8 Sep 2020
---

* cf <https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/3g2A-nn/>
* Using the 100k NN model with the 1.6 cuts
