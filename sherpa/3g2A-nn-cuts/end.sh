#!/usr/bin/env bash

l=(0 50 100 200)

for a in ${l[@]}; do
    for b in ${l[@]}; do
        for c in ${l[@]}; do
            dir="myy${a}-ptyy${b}-ptj${c}"

            if [ -d "${dir}" ]; then
                cd "${dir}" || exit 1

                make Analysis.yoda

                cd .. || exit 1
            fi
        done
    done
done
