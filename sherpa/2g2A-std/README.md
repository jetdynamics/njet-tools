---
title: gg $\rightarrow \gamma \gamma$
author: Ryan Moodie
date: 9 Dec 2019
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/2g2A/ATLAS_2017_I1591327/index.html>

## Usage

* Uses contract file and interface library from [NJET0q2gAA](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q2gAA)
    * Run `make` there first
* `make` will do all calculations (analysis performed in single run; for testing only).
* Run matrix element generation and integration (grid optimisation) with `make Results.db`.
* Run event generation and analysis with:
    * `make Analysis.yoda` for serial execution on laptop.
    * `./analyses.sbatch <number of jobs to run>` to submit parallel batch jobs on the IPPP system. 
        * This uses different `-R <rseed>` flags, `-A <output filename>` flags where `<output filename>` omits the `.yoda` suffix, and `-e <events>` flags where `<events>`=total number of events/number of jobs. 
        * The separate files can then be stitched together with `yodamerge -o Analysis.yoda part.*.yoda` on the IPPP system.
* Show plots with `make show`.

* `make test` runs some internal Sherpa gg_yy ME provider instead of NJET.

* Delete temporary files created by batch execution with `make flush`.
* Get rid of compilations with `make clean`
* Get rid also of matrix element generation and integration (grid optimisation) with `make reset`
* Also get rid of pdfs with `make wipe`

## Details

* Uses PDFs
* Does nothing fancy with final states

## Problems

* Currently `yodamerge` doesn't seem to work with Python 3 on my laptop, hence the serial execution.
