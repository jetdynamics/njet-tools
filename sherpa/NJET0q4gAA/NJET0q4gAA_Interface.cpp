#include "NJET0q4gAA_Interface.hpp"

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

// cmath
using std::pow;
using std::sqrt;
// complex
using std::complex;
// iostream
using std::cout;
using std::endl;
using std::ostream;
// string
using std::string;
using std::to_string;

// SquaredMatrixElement class constructor and member implementations

NJET0q4gAA::SquaredMatrixElement::SquaredMatrixElement()
    : m_alpha(1. / 128.3479449790089)
    , m_alphacubed(pow(m_alpha, 3))
    , m_ge(sqrt(4. * M_PI * m_alpha))
    , m_prec(1e-5)
{
}

void NJET0q4gAA::SquaredMatrixElement::SetParameter(const string& str, const double& val)
{
    if (str == "alpha") {
        m_alpha = val;
        m_alphacubed = pow(val, 3);
    } else if (str == "Accuracy") {
        // pass val to NJET
        m_prec = val;
    } else
        THROW(fatal_error, "Unknown parameter.");
}

void NJET0q4gAA::SquaredMatrixElement::RecalcDependentParameters() {}

void NJET0q4gAA::SquaredMatrixElement::PrintSummary() const
{
    msg_Info() << "Using NJET0q4gAA Interface with the following parameters:"
               << endl
               << "  1/alpha = " << 1. / m_alpha << endl
               << "  ----------------------------------------"
               << endl;
}

// code to compute amplitude here
double NJET0q4gAA::SquaredMatrixElement::Calculate(const double point[NJET0q4gAA::legs][NJET0q4gAA::d]) const
{
#ifdef TEST
    return 1.;
#endif

#ifdef DEBUG
    cout << "###### Calculating matrix element... ######" << endl;
#endif

    const double zero { 0. };

/* #ifdef DEBUG */
    /* cout << "Momenta from Sherpa" << endl; */
    /* for (size_t p { 0 }; p < NJET0q4gAA::legs; ++p) { */
    /*     cout << "p" << p + 1 << "=("; */
    /*     for (size_t mu { 0 }; mu < NJET0q3gAA::d; ++mu) { */
    /*         cout << point[p][mu] << (mu == NJET0q3gAA::d - 1 ? "" : ","); */
    /*     } */
    /*     cout << ")" << endl; */
    /* } */
    /* cout << endl; */
/* #endif */

    /* const size_t order[NJET0q4gAA::legs] { 0, 1, 4, 5, 2, 3 }; */
    /* const size_t n { 5 }; */
    /* double LHMomenta[NJET0q4gAA::legs * n]; */
    /* for (size_t p { 0 }; p < NJET0q4gAA::legs; ++p) { */
    /*     for (size_t mu { 0 }; mu < NJET0q4gAA::d; ++mu) { */
    /*         LHMomenta[mu + p * n] = point[order[p]][mu]; */
    /*     } */
    /*     // Set masses */
    /*     LHMomenta[d + p * n] = 0.; */
    /* } */

    const size_t n { 5 };
    double LHMomenta[NJET0q4gAA::legs * n];
    for (size_t p { 0 }; p < NJET0q4gAA::legs; ++p) {
        for (size_t mu { 0 }; mu < NJET0q4gAA::d; ++mu) {
            LHMomenta[mu + p * n] = point[p][mu];
        }
        // Set masses
        LHMomenta[d + p * n] = 0.;
    }

#ifdef DEBUG
    cout << "Momenta" << endl;
    for (size_t p { 0 }; p < NJET0q3gAA::legs; ++p) {
        cout << "p" << p + 1 << "=(";
        for (size_t mu { 0 }; mu < NJET0q3gAA::d; ++mu) {
            cout << LHMomenta[mu + p * n] << (mu == NJET0q3gAA::d - 1 ? "" : ",");
        }
        cout << ")" << endl;
    }
    cout << endl;

    cout << "Momentum invariants" << endl;
    for (size_t p { 0 }; p < NJET0q3gAA::legs; ++p) {
        for (size_t q { 0 }; q < p; ++q) {
            double s { LHMomenta[n * p] * LHMomenta[n * q] };
            for (size_t mu { 1 }; mu < NJET0q3gAA::d; ++mu) {
                s -= LHMomenta[n * p + mu] * LHMomenta[n * q + mu];
            }
            s *= 2;
            cout << "s" << p << q << "=" << s << endl;
        }
    }
    cout << endl;
#endif

    const double alphas { 0.118 };
    int alphasReturnStatus;
    OLP_SetParameter("alphas", &alphas, &zero, &alphasReturnStatus);
    assert(alphasReturnStatus == 1);

    // set alpha QED (answer changes if this changed, so does something)
    const double alpha { 1. / 137.035999084 };
    int alphaReturnStatus;
    OLP_SetParameter("alpha", &alpha, &zero, &alphaReturnStatus);
    assert(alphaReturnStatus == 1);

    const double mur { 91.188 };
    double out[7];
    double acc { 0. };
    const int channel { 1 };
    OLP_EvalSubProcess2(&channel, LHMomenta, &mur, out, &acc);

#ifdef DEBUG
    cout << "Notation: Tree     = A0.cA0" << endl;
    cout << "          Loop(-2) = 2*Re(A1.cA1)/eps^2" << endl;
    cout << "          Loop(-1) = 2*Re(A1.cA1)/eps^1" << endl;
    cout << "          Loop( 0) = 2*Re(A1.cA1)/eps^0" << endl;
    cout << endl;
    cout << "Tree            = " << out[3] << endl;
    cout << "Loop(-2)        = " << out[0] << endl;
    cout << "Loop(-1)        = " << out[1] << endl;
    cout << "Loop( 0)        = " << out[2] << endl;
    cout << "  only if NJetReturnAccuracy is used: (fractional error)" << endl;
    cout << "Loop(-2) error  = " << out[4] << endl;
    cout << "Loop(-1) error  = " << out[5] << endl;
    cout << "Loop( 0) error  = " << out[6] << endl;
    cout << endl;
#endif

    return out[2];
}

// Interface class constructor and member implementations

NJET0q4gAA::Interface::Interface()
    : ME_Generator_Base("NJET0q4gAA")
{
}

bool NJET0q4gAA::Interface::Initialize(const string& path, const string& file,
    MODEL::Model_Base* const model,
    BEAM::Beam_Spectra_Handler* const beam,
    PDF::ISR_Handler* const isr)
{
    const string f { "OLE_contract_" + to_string(NJET0q4gAA::legs - 2) + "g2A.lh" };
    const char* contract { f.c_str() };
    int status;
    OLP_Start(contract, &status);
    assertm(status, "There seems to be a problem with the contract file.");

#ifdef DEBUG
    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    cout << endl << "# Running " << olpname
         << " version " << olpversion
         << " note " << olpmessage << endl << endl;
#endif

    return true;
}

PHASIC::Process_Base*
NJET0q4gAA::Interface::InitializeProcess(const PHASIC::Process_Info& pi, bool add)
{
    return NULL;
}

int NJET0q4gAA::Interface::PerformTests()
{
    return 1;
}

bool NJET0q4gAA::Interface::NewLibraries()
{
    return false;
}

void NJET0q4gAA::Interface::SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs) {}

ATOOLS::Cluster_Amplitude*
NJET0q4gAA::Interface::ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode)
{
    return NULL;
}

// Process class constructor and member implementations

NJET0q4gAA::Process::Process(
    const PHASIC::Process_Info& pi,
    const ATOOLS::Flavour_Vector& flavs,
    const bool swap, const bool anti)
    : Tree_ME2_Base(pi, flavs)
    , m_me()
{
    m_me.SetParameter("alpha", AlphaQED());
    m_me.RecalcDependentParameters();

    Data_Reader reader(" ", ";", "#", "=");

    m_me.PrintSummary();

    rpa->gen.AddCitation(1, string("<Description of calculation> from \\cite{xxx:2019yy}"));
}

double NJET0q4gAA::Process::Calc(const ATOOLS::Vec4D_Vector& p)
{
    if (p.size() != NJET0q4gAA::legs)
        THROW(fatal_error, "Wrong process.");
    double moms[NJET0q4gAA::legs][NJET0q4gAA::d];
    for (size_t i { 0 }; i < NJET0q4gAA::legs; ++i)
        for (size_t j { 0 }; j < NJET0q4gAA::d; ++j)
            moms[i][j] = p[i][j];
    return m_me.Calculate(moms);
}

int NJET0q4gAA::Process::OrderQCD(const int& id)
{
    return NJET0q4gAA::legs - 2;
}

int NJET0q4gAA::Process::OrderEW(const int& id)
{
    return 2;
}

// End class member implementations

DECLARE_GETTER(NJET0q4gAA::Interface, "NJET0q4gAA", PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key);

PHASIC::ME_Generator_Base*
ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NJET0q4gAA::Interface>::
operator()(const PHASIC::ME_Generator_Key& key) const
{
    return new NJET0q4gAA::Interface();
}

void ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NJET0q4gAA::Interface>::
    PrintInfo(ostream& str, const size_t width) const
{
    str << "Interface to the NJET0q4gAA calculation";
}

using namespace PHASIC;

DECLARE_TREEME2_GETTER(NJET0q4gAA::Process, "NJET0q4gAA::Process")

PHASIC::Tree_ME2_Base*
ATOOLS::Getter<PHASIC::Tree_ME2_Base, PHASIC::Process_Info, NJET0q4gAA::Process>::
operator()(const PHASIC::Process_Info& pi) const
{
    assert(pi.m_loopgenerator == "NJET0q4gAA");
    assert(MODEL::s_model->Name() == string("SM"));
    assert(pi.m_fi.m_nloewtype == nlo_type::lo);
    assert(pi.m_fi.m_nloqcdtype == nlo_type::lo);
    Flavour_Vector fl(pi.ExtractFlavours());
    // check for g g  -> g g a a
    assert(fl[0].Kfcode() == kf_gluon && fl[1].Kfcode() == kf_gluon && fl[4].Kfcode() == kf_gluon && fl[5].Kfcode() == kf_gluon
        && fl[2].Kfcode() == kf_photon && fl[3].Kfcode() == kf_photon);
    return new NJET0q4gAA::Process(pi, fl, 0, 0);
}
