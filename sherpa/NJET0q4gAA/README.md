---
title: gg $\rightarrow$ gg$\gamma \gamma$ --- NJet/Sherpa interface
author: Ryan Moodie
date: 25 Feb 2020
---

## Usage

### Running

* Run `make`
* See `Makefile` for more info

### Cleaning

* Delete temporary files created by batch execution with `make flush`.
* Get rid of compilations with `make clean`
* Get rid also of matrix element generation and integration (grid optimisation) with `make reset`
* Also get rid of pdfs with `make wipe`

## Notes

Doing grid optimisation on real ME takes too long.
This is inefficient in any case as this stage exists only to inform the integrator of the event generation stage where to adaptively sample phase space, so full loop information (which has no (?) external phase space information) is not required anyway.

* Quick thing to do: starting with Rambo integrator, optimise integrator on qqbar -> ggyy
    * Use now for LO
* Best thing to do: use Vegas integrator and train on trees with effective vertices (or grid optimisation on this?)
    * Would need to add this effective vertex stuff to NJet
    * Consider for NLO
    * Vegas needs matrix element information to optimise final states, eg. propagator resonances. Particularly important for diagrams where final states are not directly radiated from the loop, since get these propagators. 

## Approximate evaluation time for 4g2A

Precision|Float type|Time
---|---|---
1e-1|SD|2m
1e-2|SD|2m
1e-5|DD|3m20s
1e-20|DD|20m
