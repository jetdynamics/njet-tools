RUN_GRID=Run_grid.dat
RUN_EVENTS=Run_events.dat

.PHONY: clean reset wipe all show test flush analysis test copy

all: analysis

include ../../Makefile.inc

# Generate BLHA contract file from order file
OLE_contract_%.lh: OLE_order_%.lh
	njet.py -o $@ $<

# Generate matrix element mapping/process information
Process/: $(RUN_GRID)
	Sherpa -f $< INIT_ONLY=1 -l process.log || exit 0
	./makelibs

# Do integration (grid optimisation)
Results.db: $(RUN_GRID) Process/
	mpirun --use-hwthread-cpus Sherpa -f $< -e 0 -a 0 -l integration.log

# Event generation and analysis on IPPP system - on laptop, just do `Sherpa -f $(RUNCARD)`
analysis: $(RUN_EVENTS) Results.db libSherpaNJET0q2gAA.so OLE_contract_2g2A.lh
	./analyses.sh 1 200 500000

# After above, on IPPP system, you must manually do:
Analysis.yoda: part.*.yoda
	yodamerge -o $@ $^

# Generate the plots with Rivet, using custom analysis
rivet-plots/: Analysis.yoda 
	RIVET_ANALYSIS_PATH=../analysis-diphoton rivet-mkhtml -n $(shell nproc) $<

copy: rivet-plots/
	rm -rf ~/www/files/research/2g2A-gg
	cp -r $< ~/www/files/research/2g2A-gg

# Remove temporary files
flush:
	rm -rf part.*.yoda *.out *.err *.log Status__* Sherpa_References.tex 

# Remove compiled files
clean: flush
	rm -rf rivet-plots Process/ OLE_contract_2g2A.lh makelibs SConstruct

# Remove results of calculation
reset: clean
	rm -rf Results.db* Analysis.yoda Events.hepmc2g

# Remove everything
wipe: reset
	rm *.pdf
