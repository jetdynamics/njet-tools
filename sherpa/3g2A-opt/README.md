---
title: gg $\rightarrow$ g$\gamma \gamma$ using quark-initiated tree to optimise integration - calculation
author: Ryan Moodie
date: 18 Feb 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2A-qq/>

## Usage

### Running

* Run on IPPP system
* Uses ME library and order file from [previous 2g3A](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/NJET0q3gAA)
    * Run `make libSherpaNJET0q3gAA.so` and `make OLE_contract_3g2A.lh` there first
    * Files are symlinked here
* Uses optimised integrator library from [here](https://gitlab.com/jetdynamics/njet-tools/-/tree/master/sherpa/3g2A-qq) 
    * Run `make` there first
* Then do grid optimisation using actual ME but with the optimised integrator library
    * `make Results.db CORES=<number>`
* Uses [custom Rivet analysis](https://gitlab.com/jetdynamics/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make` there first
* Then do analysis here. 
    * On IPPP system,
        * `make analysis` or `./local-analyses <init seed> <num runs> <total num events>`
        * `make copy`

### Cleaning

* Delete temporary files with `make flush`.
* Get rid also of output with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`
