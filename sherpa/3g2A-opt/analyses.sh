#!/usr/bin/env bash
# To run jobs on the batch, do:
# ./analyses.sh

init_seed=$1
num_runs=$2
end_seed=$(($init_seed + $num_runs - 1 ))
total_events=$3
run_events=$(($total_events / $num_runs))
output=part.

for rseed in $(seq -w ${init_seed} ${end_seed}); do
    name=${rseed}-3g2A-A
    sed "s/%j/${name}/g" analysis.sbatch >$name
    run_output=$output$rseed
    sbatch $name $rseed $run_events $run_output
    rm -f $name
done
