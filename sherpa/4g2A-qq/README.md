---
title: gg $\rightarrow$ gg$\gamma \gamma$ - integrator library
author: Ryan Moodie
date: 20 Feb 2020
---

## Usage

### Running

* Run on IPPP system
* Generates channels (integrator library) using quark initiated tree
    * `make`
    * ie. detect resonances
* Then can continue with [4g2A-opt](https://gitlab.com/jetdynamics/njet-tools/-/tree/master/sherpa/4g2A-opt)

### Cleaning

* Clean with `make clean`
* Also get rid of pdfs with `make wipe`
