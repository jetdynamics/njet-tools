---
title: gg $\rightarrow \gamma \gamma$ --- NJet/Sherpa interface
subtitle: test
author: Ryan Moodie
date: 9 Dec 2019
---

## Usage

* `make` will generate interface library and contract file
* `make Process/` runs some internal Sherpa gg_yy ME provider instead of NJET for testing.

* Delete temporary files with `make flush`.
* Get rid of compilations with `make clean`
* Also get rid of pdfs with `make wipe`

## References

* <https://twiki.cern.ch/twiki/pub/LHCPhysics/ProposalTtbb/readme_sherpa21.txt>
* <https://bitbucket.org/njet/njet/wiki/NJetSherpa/NJetSherpa>
* <https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html>

## Notes

### Enable_MHV 10;

Marek:

> Amegic abuses its MHV amplitude settings to interface external matrix elements and has multiple modes for this
> 
> 10 - Amegic loads both the phase space libraries and the process libraries from an external source
>
> 11 - Amegic only loads the process libraries from an external source, but builds it's own phase space

### Loop_Generator gg_yy; in test

Marek:

> Btw. for the LO gg->aa process you can test your implementation against our internal ME for this process, eg. to make sure all conventions and parameters are set correctly. Simply set "Loop_Generator gg_yy;" for that.

and on LibraryLoader::LoadLibrary(), ME_Generators::LoadGenerator() failures:

> no, nothing to worry about. In fact, when stating you want Loop_Generator xyz Sherpa tries to load the library libSherpaxyz.so by default. Which, given that this is just a pseudo-generator used to interface one specific internal matrix element, does not exist. Instead, it exists in another internal library. The same spurious output is printed when using the Dummy as the virtual correction. Probably laziness on our part.
