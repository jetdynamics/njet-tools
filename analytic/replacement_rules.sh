#! /usr/bin/env bash

sed -i "s/\ \([0-9]\+\)/ static_cast<T>(\1.)/g" *.c

sed -i "s/=\([0-9]\+\)\ /=static_cast<T>(\1.) /g" *.c
sed -i "s/(\([0-9]\+\)\ /(static_cast<T>(\1.) /g" *.c
sed -i "s/{\([0-9]\+\)\ /{static_cast<T>(\1.) /g" *.c

sed -i "s/=\([0-9]\+\)\*/=static_cast<T>(\1.)*/g" *.c
sed -i "s/(\([0-9]\+\)\*/(static_cast<T>(\1.)*/g" *.c
sed -i "s/{\([0-9]\+\)\*/{static_cast<T>(\1.)*/g" *.c

sed -i "s/,\([0-9]\+\))/,static_cast<T>(\1.))/g" *.c

sed -i "s/1\//static_cast<T>(1.)\//g" *.c

sed -i "s/pow(\(\w\+\),-1)/static_cast<T>(1.)\/\1/g" *.c

for n in $(seq 1 99); do
    m=$((n - 1))
    sed -i -r "s/\[$n\]/[$m]/g" *.c
done

clang-format -i *.c
