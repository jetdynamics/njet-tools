#-

S x1,x2,x3,x4,x5;


L cAN1425=(-64*x1^3*x2^5*x3^2*(x4 + x3*(x4 + x2*(-1 + x5))));

L cAD1425=((1 + x3)^2*(1 + x3 + x2*x3)*(x2 - x4));

L cAN2415=(-64*x1^3*x2^3*(1 + x2)*x3*(x4 + x3*(x4 + x2*(-1 + x5))));

L cAD2415= ((1 + x3)^2*(x2 - x4 + x5));

L cAN2314= (-64*x1^3*x2^2*x3^3*(1 + x3 + x2*x3)^2*(2 + 3*x2 + 2*(1 + x2)*x3)*x5);

L cAD2314= ((1 + x3)^2*(-x4 + x5 + x3*(x2 - x4 + x5)));


L cBN1324= (-64*x1^3*x2^4*x3^4*x5*((3 + 2*x3)*(1 + x3 + x2*x3)^2*(x2 - x4) - x2*(3 + x3*(6 + 4*x2 + 3*x3 + 3*x2*x3))*x5));

L cBD1324= ((1 + x2)*(1 + x3)^2*(-(x2*x3) + x4 + x3*x4)^2);

L cBN1325= (64*x1^3*x2^4*x3^2*((1 + x2)^2*(-(x2*x3) + x4 + x3*x4)^2 + (1 + x2)*(-1 + x2 - (1 + x2)^2*x3 + 3*(1 + x2)*x3^2 + (1 + x2)*(5 + 3*x2)*x3^3 + 2*(1 + x2)^2*x3^4)*(x2*x3 - (1 + x3)*x4)*x5 +
            x2*x3*(3*(1 + x3)^3 + x2^2*(1 + x3)^2*(-1 + 3*x3) + x2*x3*(9 + 2*x3*(7 + 3*x3)))*x5^2));

L cBD1325= ((1 + x3)^2*(1 + x3 + x2*x3)^2*(x2 - x4)^2);

L cBN1523= (-64*x1^3*x2^2*x3^2*(x2^2*(-(x2*x3) + x4 + x3*x4)^2 + x3*(2 - (-3 + x2)*x2*(1 + x2) + 6*x3 + 6*x2*(2 + x2)*x3 + 3*(1 + x2)^2*(2 + x2)*x3^2 + 2*(1 + x2)^3*x3^3)*(x2*x3 - (1 + x3)*x4)*x5 +
            x3^2*(1 - 2*x2^2*(1 + x2) + 4*x3 - 2*x2*(-3 + x2^2)*x3 + (1 + x2)^2*(5 + 2*x2)*x3^2 + 2*(1 + x2)^3*x3^3)*x5^2));

L cBD1523= ((1 + x3)^2*x4^2);


L FBOX1M32415= (-16*x1^5*x2^2*(1 + x2)*(1 + 2*x2*(1 + x2))*x3^2*x4*(x2*x3 - (1 + x3)*x4))/(1 + x3);

L FBOX1M32514= (-16*x1^5*x2^2*x3^2*(1 + x3 + x2*x3)*(1 + x3*(2 + x3 + 2*x2*(1 + x3 + x2*x3)))*(x2 - x4)*x4)/(1 + x3)^3;

L FBOX1M41325= (16*x1^5*x2^3*(1 + 2*x2*(1 + x2))*x3^2*(1 + x4 - x5)*(-x4 + x5 + x3*(x2 - x4 + x5)))/(1 + x3);

L FBOX1M51324= (16*x1^5*x2^3*x3^3*(1 + x3*(2 + x3 + 2*x2*(1 + x3 + x2*x3)))*(1 + x4 - x5)*(x2 - x4 + x5))/(1 + x3)^3;

L FBOX1M51423= (-16*x1^5*x2^2*x3^3*(2 + x2 + 2*(1 + x2)*x3)*(1 + 2*(1 + x2)*x3*(1 + x3 + x2*x3))*(x2 - x4 + x5)*(-x4 + x5 + x3*(x2 - x4 + x5)))/(1 + x3);

L FBOX1M52413= (16*x1^5*x2*(1 + x2)*x3^2*(1 + 2*x3)*(1 + x3 + x2*x3)*(1 + 2*(1 + x2)*x3*(1 + x3 + x2*x3))*(x2 - x4)*(x2*x3 - (1 + x3)*x4))/(1 + x3);


L rat1=-64*x1^3*x2^2*x3^2*(x2^7*x3^4*(1 + x4 - x5)*(1 + (1 + x3)^2*(-1 + 2*x3)*x5) - x2^6*x3^3*(1 + x4 - x5)*(-1 - x3 + 2*x4 + 4*x3*x4 - x3*(1 + x3*(1 + x3)*(9 + 8*x3))*x5 +
            (-1 + x3*(-3 + x3*(3 + 2*x3)*(2 + 3*x3)))*x4*x5 - (1 + x3)^2*(-1 + 2*x3^2)*x5^2) -
        x2^4*x3*(1 + x3)*(x3*(1 + x3)*(-1 + x5)*x5*(x3*(4 + x3*(11 + 8*x3)) + x5 + 3*x3*(1 + x3)*(1 + 4*x3)*x5 - (1 + x3)*x5^2) -
            x4^3*(5 + 2*x3*(2 + x3) - 5*x5 + x3*(5 + x3*(20 + x3*(43 + 54*x3 + 22*x3^2)))*x5 + (2 + (-1 + x3)*x3*(1 + x3*(12 + x3*(13 + 4*x3))))*x5^2) +
            x4*x5*(2 + x3*(1 + x3)*(7 + 4*x3*(4 + x3*(10 + 7*x3))) - 3*x5 - x3*(10 + x3*(28 + x3*(67 + 16*x3*(5 + 2*x3))))*x5 - (1 + x3)*(-1 + x3*(-1 + x3*(1 + 2*x3)*(5 + 8*x3)))*x5^2 +
                x3*(1 + x3)*(1 + 2*x3)*x5^3) + x4^2*(-3 - 6*x3*(1 + x3) + 7*x5 + 2*x3*(1 + x3)*(5 + x3*(5 + 8*x3 + 6*x3^2))*x5 + (-4 + x3*(2 + x3*(24 + x3*(70 + x3*(89 + 38*x3)))))*x5^2 +
                (1 + x3*(2 + x3)*(-1 - 5*x3 + 2*x3^3))*x5^3) + x4^4*(-2 + x5 + x3*(2 + 4*x3 + x3*(-4 + x3 + 5*x3^2 + 2*x3^3)*x5))) +
        x2^5*x3^2*(1 + x4 - x5)*(x4^2*(6*x3*(1 + x3) + x5 + x3*(-2 + x3*(-3 + x3*(3 + 2*x3)*(4 + 3*x3)))*x5) +
            (1 + x3)*x5*(1 - x5 + x3*(2 + 3*x3*(1 + x3)*(3 + 4*x3) - x5 + x3*(4 + x3*(13 + 8*x3))*x5 - (1 + x3)*x5^2)) +
            x4*(-3 + x5 + x3*(-6 - 4*x3 - (3 + x3*(24 + x3*(63 + x3*(67 + 24*x3))))*x5 - (1 + x3)*(-4 + x3*(-1 + 6*x3 + 4*x3^2))*x5^2))) -
        x2^2*(1 + x3)^3*(x4^2*(x4 - x5)*(1 + x4 - x5)*x5 + 2*x3^4*(1 + x4 - x5)*x5*(-x5 + x4*(3 + 6*x4*(-2 + x4 - x5) + 8*x5)) +
            x3^3*(1 + x4 - x5)*x5*(-3*x5 + x4*(10 + x4*(-41 + 21*x4 - 15*x5) + 22*x5)) - x3*x4*(x4^4 + 2*(-1 + x5)*x5 + 5*x4^2*x5^2 + x4*x5*(1 + x5)*(2 + (-4 + x5)*x5) - x4^3*(-1 + x5*(2 + x5))) -
            x3^2*x5*(x5 - x5^2 - 2*x4^4*(3 + x5) + x4*(-7 + x5 + 7*x5^2) + x4^2*(-8 + x5)*(-2 + x5*(3 + x5)) + x4^3*(11 + x5*(14 + x5)))) -
        (1 + x3)^5*x4^2*x5*(x4^3 + x4^2*(1 + 2*x3*(1 + x3) - x5) + x3*(1 + 2*x3)*(-1 + x5)*x5 - x4*(-1 + x5 + x3*(-2 + 3*x5 + x3*(-2 + 4*x5)))) +
        x2^3*(1 + x3)^2*((-1 + x3^2)*x4^5 + x4^3*(-1 - 2*x3*(1 + 2*x3) + 3*x5 + 2*x3*(3 + x3*(11 + x3*(20 + x3*(29 + 14*x3))))*x5 + (-1 + x3 + x3^2*(-5 + 2*x3*(6 + x3*(15 + 8*x3))))*x5^2 -
                x3^2*(1 + x3)*x5^3) + x3*x4*x5*(-(x3*(1 + 2*x3)*(7 + x3*(17 + 11*x3))) + 2*(-1 + x3^2*(9 + x3*(11 + 4*x3)))*x5 + (1 + 2*x3)*(3 + x3*(5 + 3*x3*(5 + 4*x3)))*x5^2 -
                (1 + x3)*(1 + 2*x3)*x5^3) - x4^4*(2 - 2*x5 + x3*(2 + 3*x3 + (1 + x3)*(1 + x3*(-2 + x3*(9 + 8*x3)))*x5)) - x3^2*(1 + x3)*(-1 + x5)*x5*(1 + x3*(2 + 7*x5 + x3*(2 + 8*x5))) +
            x4^2*x5*(1 - x5 + x3*(5 + (-7 + x5)*x5 + x3^2*(17 - 73*x5 + x5^3) - 13*x3^3*(-2 + x5*(8 + x5)) + x3*(11 + (-3 + x5)*x5*(11 + x5)) - 4*x3^4*(-3 + x5*(13 + 2*x5))))) -
        x2*(1 + x3)^4*x4*x5*(x3*x4^4 - x3*(1 + 2*x3)^2*(-1 + x5)*x5 + x4^3*(-1 + x5 + x3*(1 + x3*(11 + 8*x3) + x5)) -
            x4^2*(1 + x3 + (-1 + x5)*x5 + 2*x3*x5*(1 + x5) + 3*x3^2*(-1 + 6*x5) + 2*x3^3*(-1 + 8*x5)) + x4*(-1 + x5 + x3*(-5 + 6*x5 + x3*(-8 + x5*(5 + 7*x5) + 2*x3*(-3 + x5 + 4*x5^2))))));

L rat2= ((1 + x3)^2*(1 + x3 + x2*x3)*(x2 - x4)*x4*(x2*x3 - (1 + x3)*x4)*(1 + x4 - x5)*(-x4 + x5 + x3*(x2 - x4 + x5)));


Format O4;
.sort

Format C;

ExtraSymbols,array,Z;

#optimize cAN1425
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAN1425{%E};\n", cAN1425

ExtraSymbols,array,Z;
#optimize cAD1425
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAD1425{%E};\n", cAD1425

ExtraSymbols,array,Z;
#optimize cBN1324
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBN1324{%E};\n", cBN1324

ExtraSymbols,array,Z;
#optimize cBD1324
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBD1324{%E};\n", cBD1324

ExtraSymbols,array,Z;
#optimize cBN1325
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBN1325{%E};\n", cBN1325

ExtraSymbols,array,Z;
#optimize cBD1325
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBD1325{%E};\n", cBD1325

ExtraSymbols,array,Z;
#optimize cBN1523
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBN1523{%E};\n", cBN1523

ExtraSymbols,array,Z;
#optimize cBD1523
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cBD1523{%E};\n", cBD1523

ExtraSymbols,array,Z;
#optimize FBOX1M32415
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M32415{%E};\n", FBOX1M32415

ExtraSymbols,array,Z;
#optimize FBOX1M32514
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M32514{%E};\n", FBOX1M32514

ExtraSymbols,array,Z;
#optimize FBOX1M41325
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M41325{%E};\n", FBOX1M41325

ExtraSymbols,array,Z;
#optimize FBOX1M51324
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M51324{%E};\n", FBOX1M51324

ExtraSymbols,array,Z;
#optimize FBOX1M51423
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M51423{%E};\n", FBOX1M51423

ExtraSymbols,array,Z;
#optimize FBOX1M52413
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue FBOX1M52413{%E};\n", FBOX1M52413

ExtraSymbols,array,Z;
#optimize rat1
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue rat1{%E};\n", rat1

ExtraSymbols,array,Z;
#optimize rat2
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue rat2{%E};\n", rat2

ExtraSymbols,array,Z;
#optimize cAN2415
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAN2415{%E};\n", cAN2415

ExtraSymbols,array,Z;
#optimize cAD2415
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAD2415{%E};\n", cAD2415

ExtraSymbols,array,Z;
#optimize cAN2314
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAN2314{%E};\n", cAN2314

ExtraSymbols,array,Z;
#optimize cAD2314
#write <optimize_mmppp.c> "%O"
#write <optimize_mmppp.c> "const TreeValue cAD2314{%E};\n", cAD2314

#write <optimize_mmppp.c> "const TreeValue cA1425{cAN1425/cAD1425};"
#write <optimize_mmppp.c> "const TreeValue cA2415{cAN2415/cAD2415};"
#write <optimize_mmppp.c> "const TreeValue cA2314{cAN2314/cAD2314};"
#write <optimize_mmppp.c> "const TreeValue cB1324{cBN1324/cBD1324};"
#write <optimize_mmppp.c> "const TreeValue cB1325{cBN1325/cBD1325};"
#write <optimize_mmppp.c> "const TreeValue cB1523{cBN1523/cBD1523};"

.end
