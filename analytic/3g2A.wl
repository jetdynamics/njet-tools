(* ::Package:: *)

(* ::Section::Closed:: *)
(*mpppp*)


ratMTmpppp=(1/((1 + x2) (1 + x3) (1 + (1 + x2) x3) (1 + x4 - 
   x5)))64 x1^6 x2^2 x3^2 (-(1 + x3)^2 x4^3 - 
   x2 (1 + 3 x3 + 2 x3^2) x4^3 + 
   x2^3 x3 (1 + 2 x3) (x4^2 - (-1 + x5)^2) + 
   x2^4 x3^2 (1 + x4 - x5) (-1 + x5) + 
   x2^2 ((1 + 3 x3 + 3 x3^2) x4^2 - x3 (1 + x3) x4^3 - 
      x3 (1 + x3) (-1 + x5) x5 - 
      x4 (-1 + x5 + x3 (-3 + 2 x5) + x3^2 (-3 + 2 x5))))

phaseMTmpppp=(x1^6 x2^2 x3^2 (x2 - x4)^2)/(x2 - x4 + x5)



ratMTmpppp/phaseMTmpppp//Simplify//InputForm


(* ::Section:: *)
(*pppmp*)


Npppmp=(64 * x1^2 * x3 * (x2 - x4 + x5 + x3 * x5 + x2 * x3 * x5)) / ((1 + x3) * (1 + (1 + x2) * x3))
Dpppmp=(x1 * x3 * (x4 + x2 * (-1 + x5))^2) / (x2 * ((1 + x3) * x4 + x2 * x3 * (-1 + x5)) * x5)
pppmp=Npppmp/Dpppmp//Simplify//InputForm



