HEAD=$(shell git rev-parse --show-toplevel)/header.tex
TEX=xelatex

# pdfs customised to:
# show a table of contents
# code blocks have line wrapping (defined in $HEAD)
# links to be coloured differently from main text
%.pdf: %.md $(HEAD)
	pandoc --toc -f markdown -t latex --pdf-engine=$(TEX) -H $(HEAD) -V colorlinks -o $@ $<

.PHONY: view

DOC_SRCS=$(wildcard *.md)
# USing <https://www.gnu.org/software/make/manual/make.html#Substitution-Refs>
DOCS=$(DOC_SRCS:.md=.pdf)

view: $(DOCS)
	evince $^ &
