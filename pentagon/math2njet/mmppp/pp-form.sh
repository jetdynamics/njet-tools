#!/usr/bin/env bash

# use on FORM processed file

OUT=$1pp

perl \
    -0777p \
    -e  "
        s|([ (\-={\/] ?)(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/] ?)(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|pow|njet_pow|g;
        s|gs(\d+)s(\d+)s(\d+)|g_\1_\2_\3|g;
        s|gs(\d+)s(\d+)|g_\1_\2|g;
        " \
    $1 > $OUT
