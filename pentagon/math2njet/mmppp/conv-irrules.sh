#!/usr/bin/env bash

# use on Mathematica processed (by `proc.wl`) file

OUT=irrules-unoptim.cpp

perl \
    -0777p \
    -e  "
        s|f\[(\d+), (\d+)\]|const TreeValue g_\1_\2|g;
        s|f\[(\d+), (\d+), (\d+)\]|const TreeValue g_\1_\2_\3|g;
        s|ex\[(\d)\]|x\1|g;
        s|->|{|g;
        s|,[ \n]*(const TreeValue g_)| };\n\1|g;
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|InputForm\[{||g;
        s|}\]|};|g;
        s|\[|(|g;
        s|\]|)|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        s|pow|njet_pow|g;
        " \
    $1 > $OUT

# clang-format -i $1
