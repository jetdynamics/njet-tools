(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/pentagon/math2njet/mmppp/dsm2p2"];

irpoles = Get["IRpoles_I2+I1A1_pfuncs_2L_5g_trT12345_1_Ncp2_dsm2p2_mm+++_PSanalytic.m"];

Print[irpoles//Length];

irpolesout = Collect[#,eps]&/@irpoles//.{Power[a_,b_]->pow[a,b]}//InputForm;
Export["irpoles.m", irpolesout];



