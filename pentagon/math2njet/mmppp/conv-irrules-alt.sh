#!/usr/bin/env bash

# Use directly on `*.m` file from Simon
# Cannot handle Eps's!

OUT=irrules-unoptim.cpp

perl \
    -0777p \
    -e  "
        s|f\[(\d+), (\d+)\]|const TreeValue g_\1_\2|g;
        s|f\[(\d+), (\d+), (\d+)\]|const TreeValue g_\1_\2_\3|g;
        s|ex\[(\d)\]|x\1|g;
        s|->|{|g;
        s|,[ \n]*(const TreeValue g_)| };\n\1|g;
        s|([^ *+-/()]+)\^(\d+)|pow(\1, \2)|g;
        s|(\(.*?)\)\^(\d+)|pow\1, \2)|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        " \
    $1 > $OUT
