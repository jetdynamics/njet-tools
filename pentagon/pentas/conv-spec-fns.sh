#!/usr/bin/env bash

IN=$1
BASE=${IN%.m}
OUT=${BASE}.c

perl \
    -0777p \
    -e  "
    s|\s*\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
    s|\{?F\[(\d+), (\d+)\][,\s}]*|PentagonFunctions::FunctionObjectType<P> F_\1_\2 { PentagonFunctions::FunctionID(\1, \2).get_evaluator<P>() };\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|PentagonFunctions::FunctionObjectType<P> F_\1_\2_\3 { PentagonFunctions::FunctionID(\1, \2, \3).get_evaluator<P>() };\n|g;
        " \
    ${IN} > ${OUT}

echo >> ${OUT}

echo "std::complex<T> " >> ${OUT}
perl \
    -0777p \
    -e  "
    s|\s*\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
    s|\{?F\[(\d+), (\d+)\][,\s}]*|fv_\1_\2, |g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|fv_\1_\2_\3, |g;
        " \
    ${IN} >> ${OUT}

echo -e "\n" >> ${OUT}

perl \
    -0777p \
    -e  "
    s|\s*\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
    s|\{?F\[(\d+), (\d+)\][,\s}]*|fv_\1_\2 = F_\1_\2(k);\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|fv_\1_\2_\3 = F_\1_\2_\3(k);\n|g;
        " \
    ${IN} >> ${OUT}

echo >> ${OUT}

perl \
    -0777p \
    -e  "
    s|\s*\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
    s|\{?F\[(\d+), (\d+)\][,\s}]*|void eval_F_\1_\2() { fv_\1_\2 = F_\1_\2(k); }\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|void eval_F_\1_\2_\3() { fv_\1_\2_\3 = F_\1_\2_\3(k); }\n|g;
        " \
    ${IN} >> ${OUT}

echo >> ${OUT}
