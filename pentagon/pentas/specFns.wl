(* ::Package:: *)

root="/home/ryan/git/njet-tools/"
SetDirectory[root<>"pentagon/pentas"];


Fs3g2A=Get[root<>"3g2a@2l/src/"<>"AllPfuncMonomials.m"]; 
Fs5g=Get[root<>"5g@2l/sparse-matrix/expr-new/exprs/"<>"AllPfuncMonomials.m"]; 


unique=Select[Variables[Join[Fs3g2A,Fs5g]],MatchQ[#,_F]&];


Export["specFns.m",unique];
