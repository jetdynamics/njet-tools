#include "FunctionID.h"

#include <complex>
#include <iostream>
#include <vector>

int main()
{
    std::cout << '\n';

    using T = double;
    using namespace PentagonFunctions;

    const std::array<T, 5> sij {
        100.00000000000000,
        -44.627966432493800,
        29.602455883989265,
        17.852178035369874,
        -22.592147032542179
    };
    const Kin<T> k(sij);

    FunctionObjectType<T> f111 { FunctionID(1, 1, 1).get_evaluator<T>() };
    std::cout << f111(k) << '\n';
}
