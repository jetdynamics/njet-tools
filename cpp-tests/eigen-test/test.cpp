#include "common.hpp"
#include <Eigen/SparseCore>
#include <algorithm>
#include <chrono>
#include <iostream>
#include <qd/qd_real.h>
#include <random>
#include <vector>

// template <typename T>
// void run1()
// {
//     const int rows { 20 };
//     const int cols { 10 };

//     std::vector<Eigen::Triplet<T>> tripletList;
//     tripletList.reserve(5);
//     for (int x { 0 }; x < 5; ++x) {
//         tripletList.push_back(Eigen::Triplet<T>(x, x, x));
//     }
//     Eigen::SparseMatrix<T, Eigen::RowMajor> mat(rows, cols);
//     mat.setFromTriplets(tripletList.begin(), tripletList.end());
//     std::cout
//         << mat.nonZeros() << '\n'
//         << '\n';
// }

template <typename T>
void reader(typename Eigen::SparseMatrix<T, Eigen::ColMajor> mat) {
  for (int k{0}; k < mat.outerSize(); ++k) {
    for (typename Eigen::SparseMatrix<T, Eigen::ColMajor>::InnerIterator it(mat,
                                                                            k);
         it; ++it) {
      std::cout << it.value() << " @(i,j)=(" << it.row() << ',' << it.col()
                << ")\n";
    }
  }
}

template <typename T> void run2() {
  const int iLen{20}; // # rows = length of a column = i_max+1
  const int jLen{10}; // # cols = length of a row = j_max+1

  Eigen::SparseMatrix<T, Eigen::ColMajor> mat(iLen, jLen);

  mat.reserve(std::vector<int>({1, 1, 0, 0, 0, 0, 1, 2, 1, 2}));

  // mat.insert(i, j) = v_ij;
  mat.insert(0, 0) = T(5.);
  mat.insert(1, 1) = 5.;
  mat.insert(9, 6) = 4. / 3.;
  mat.insert(9, 7) = T(4. / 3.);
  mat.insert(8, 7) = 4. / T(3.);
  mat.insert(9, 8) = T(4.) / 3.;
  mat.insert(9, 9) = T(4.) / T(3.);
  mat.insert(19, 9) = T(4.);

  mat.makeCompressed();

  std::cout << mat.rows() << '\n'      // iLen
            << mat.cols() << '\n'      // jLen
            << mat.outerSize() << '\n' // cols since ColMajor
            << mat.innerSize() << '\n' // rows since ColMajor
            << mat.nonZeros() << '\n'
            << mat.coeff(19, 1) << '\n'
            << '\n';

  reader(mat);
}

// matrix times vector
void run3() {
  const int rows{5};
  const int cols{10};
  Eigen::SparseMatrix<double> mat(rows, cols);
  mat.reserve(std::vector<int>({1, 0, 0, 0, 0, 1, 0, 0, 0, 1}));
  mat.insert(4, 0) = 2.;
  mat.insert(3, 5) = 3.;
  mat.insert(0, 9) = 5.;

  std::vector<double> specFuncMons({0., 1., 2., 3., 4., 5., 6., 7., 8., 9.});
  std::vector<double> specFuncVec(rows, 0.);

  for (int k{0}; k < mat.outerSize(); ++k) {
    for (typename Eigen::SparseMatrix<double>::InnerIterator it(mat, k); it;
         ++it) {
      specFuncVec[it.row()] += it.value() * specFuncMons[it.col()];
    }
  }

  for (double e : specFuncVec) {
    std::cout << e << ' ';
  }
  std::cout << '\n';
}

template <typename T> struct Test {
  Test();

  const int cl;
  std::vector<Eigen::SparseMatrix<T>> sms;
};

template <typename T> Test<T>::Test() : cl(2), sms(cl) {
  for (int i{0}; i < cl; ++i) {
    const int j{i + 1};
    sms[i].resize(2 * j, 3 * j);
  }
}

int main() {
  std::cout.precision(16);
  std::cout.setf(std::ios_base::scientific);

  // run1<dd_real>();
  // run2<dd_real>();
  // run3();

  Eigen::VectorX<double> vec(1000);

  std::mt19937_64 rng(std::random_device{}());
  const double r{1.};
  std::uniform_real_distribution<double> dist(-r, r);

  std::generate(vec.begin(), vec.end(),
                [&dist, &rng]() -> double { return dist(rng); });

  Eigen::VectorX<double> vec1 = vec;
  Eigen::VectorX<double> vec2 = vec;
  Eigen::VectorX<double> vec3 = vec;

  Timer timer;

  timer.start();
  // std::generate(vec2.begin(), vec2.end(), [](double a) -> double { return
  // std::tanh(a); });
  std::transform(vec2.cbegin(), vec2.cend(), vec2.begin(),
                 [](double a) -> double { return std::tanh(a); });
  std::cout << "std: " << timer.split_us() << " ms" << ' ' << vec2[0] << '\n';

  timer.start();
  vec = Eigen::tanh(vec.array());
  std::cout << "Eigen: " << timer.split_us() << " ms" << ' ' << vec[0] << '\n';

  timer.start();
  for (double &a : vec3) {
    a = std::tanh(a);
  }
  std::cout << "std alt: " << timer.split_us() << " ms" << ' ' << vec3[0]
            << '\n';

  timer.start();
  vec1 = vec1.array().tanh();
  std::cout << "Eigen alt: " << timer.split_us() << " ms" << ' ' << vec1[0]
            << '\n';

  std::cout << vec[0] << '\n';
  vec = vec.unaryExpr([](double a) { return a * a; });
  std::cout << vec[0] << '\n';

  Test<double> test;
  std::cout << test.sms[0].coeff(0, 0) << '\n';
}
