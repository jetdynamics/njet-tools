#include <array>
#include <iostream>
// #include <tuple>
#include <vector>

template <typename T>
class Parent {
protected:
    T val;
    T* ptr;

public:
    Parent()
        : val()
        , ptr()
    {
    }

    Parent(const T val_, T* ptr_)
        : val(val_)
        , ptr(ptr_)
    {
    }

    T get_val() const
    {
        return this->val;
    }

    void set_val(const T val_)
    {
        this->val = val_;
    }
};

template <typename T>
class Parent2 {
protected:
    T val;
    T& refc;

public:
    Parent2()
        : val()
    {
    }

    Parent2(const T val_, T& refc_)
        : val(val_)
        , refc(refc_)
    {
    }

    T get_val2() const
    {
        return this->val;
    }

    void set_val2(const T val_)
    {
        this->val = val_;
    }

    void fn() { std::cout << "fn0\n"; }
};

template <typename T>
class Child0 : public Parent2<T> {
    using Base = Parent2<T>;

public:
    Child0(T& refc_)
        : Base(1., refc_)
        , truth()
    {
    }

    void fn() { Base::fn(); }

    std::array<bool, 2> truth;
};

template <typename T>
class Child : public Parent<T> {
    using Base = Parent<T>;

public:
    Child()
        : Base(1., new T(1.))
        , truth()
    {
    }

    std::array<bool, 2> truth;
};

template <typename T>
class Child2 : public Parent<T>, public Parent2<T> {
    using Base = Parent<T>;
    using Base2 = Parent2<T>;

public:
    Child2()
        : Base(1.)
        , Base2(2.)
    {
    }

    // using Base::val;
};

template <typename T>
void run()
{
    T* ptr = new T(1.);
    Child0<T> child(*ptr);
    std::cout
        << child.get_val2() << '\n'
        // << child.truth[0] << '\n'
        // << child.truth[1] << '\n'
        << '\n';
    child.fn();

    // child.set_val2(3.);
    // std::cout
    //     << child.val << '\n'
    //     << '\n';
}

int add(int a, int b)
{
    return a + b;
}

// int main(int argc, char* argv[])
int main()
{
    std::cout << '\n';
    // std::cout.precision(16);
    // std::cout.setf(std::ios_base::scientific);

    run<double>();
    std::cout << '\n';

    std::vector<int> a({ 1, 2, 3 });
    std::cout << a[0] << '\n';

    // std::tuple<int, int> arg(1, 2);
    // std::cout << add(arg) << '\n';
}
