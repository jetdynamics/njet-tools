#include <iostream>
#include <vector>

#include "common.hpp"

int main()
{
    std::cout << '\n';

    Timer timer;
    timer.start();
    std::vector vec { { 1, 2, 3 } };
    long dur { timer.split_us() };

    std::cout << dur << "us\n"
              << vec << '\n';

    std::cout << '\n';
}
