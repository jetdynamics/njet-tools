#include <chrono>

#include "common.hpp"

Timer::Timer()
    : t0()
    , t1()
{
}

void Timer::start()
{
    t0 = std::chrono::high_resolution_clock::now();
}

void Timer::end()
{
    t1 = std::chrono::high_resolution_clock::now();
}

template <typename U>
long Timer::dur()
{
    return std::chrono::duration_cast<U>(t1 - t0).count();
}

long Timer::dur_ms()
{
    return dur<std::chrono::milliseconds>();
}

long Timer::dur_us()
{
    return dur<std::chrono::microseconds>();
}

long Timer::dur_ns()
{
    return dur<std::chrono::nanoseconds>();
}

template <typename U>
long Timer::split()
{
    end();
    return dur<U>();
}

long Timer::split_ms()
{
    return split<std::chrono::milliseconds>();
}

long Timer::split_us()
{
    return split<std::chrono::microseconds>();
}

long Timer::split_ns()
{
    return split<std::chrono::nanoseconds>();
}
