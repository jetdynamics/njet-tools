#include <array>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <valarray>
#include <vector>

// declarations
// ~~~~~~~~~~~~

template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream& strm, const std::array<T, N>& arr);

template <typename T>
std::ostream& operator<<(std::ostream& strm, const std::vector<T>& vec);

template <typename T>
std::ostream& operator<<(std::ostream& strm, const std::valarray<T>& vec);

class Timer {
private:
    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;

public:
    Timer();

    void start();

    void end();

    template <typename U>
    long dur();

    long dur_ms();

    long dur_us();

    long dur_ns();

    template <typename U>
    long split();

    long split_ms();

    long split_us();

    long split_ns();
};

// implementations
// ~~~~~~~~~~~~~~~

template <typename T, std::size_t N>
std::ostream& operator<<(std::ostream& strm, const std::array<T, N>& arr)
{
    for (T e : arr) {
        strm << e << ' ';
    }
    return strm;
}

template <typename T>
std::ostream& operator<<(std::ostream& strm, const std::vector<T>& vec)
{
    for (T e : vec) {
        strm << e << ' ';
    }
    return strm;
}

template <typename T>
std::ostream& operator<<(std::ostream& strm, const std::valarray<T>& vec)
{
    for (T e : vec) {
        strm << e << ' ';
    }
    return strm;
}
