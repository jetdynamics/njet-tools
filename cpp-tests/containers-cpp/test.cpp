#include <algorithm>
#include <array>
#include <chrono>
#include <complex>
#include <iostream>
#include <vector>

class Test {
public:
    Test();
    std::array<int, 2> arr;
    std::vector<int> vec;
    std::array<std::array<int, 2>, 3> mat;
};

Test::Test()
    : arr({ 1, 2 })
    , vec({ 1, 2 })
    , mat({ { { 1, 2 }, { 3, 4 }, { 5, 6 } } })
{
    std::cout << "constructor" << '\n';
}

template <typename T>
std::ostream& operator<<(std::ostream& strm, const std::vector<T>& vec)
{
    strm << "{";
    typename std::vector<T>::const_iterator el { vec.cbegin() };
    while (true) {
        strm << *el;
        if (++el == vec.cend()) {
            break;
        }
        strm << ", ";
    }
    strm << "}";
    return strm;
}

int main()
{
    std::cout << "main" << '\n';

    Test test;

    std::cout
        << test.arr[0] << '\n'
        << test.vec[0] << '\n'
        << test.mat[0][0] << '\n'
        << '\n';

    const int len { 2 };
    std::array<int, len> arr { 1, 2 };
    std::array<int, len> arr2;
    std::array<int, len>* ptr { &arr2 };
    std::copy(arr.cbegin(), arr.cend(), ptr->begin());
    int* ptr2 { arr2.data() };
    std::copy(arr.cbegin(), arr.cend(), ptr2);
    for (int i { 0 }; i < len; ++i) {
        std::cout << ptr2[i] << '\n';
    }
    const int* cptr { arr2.data() };
    std::cout << cptr[1] << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;
    TP t0;
    TP t1;

    t0 = std::chrono::high_resolution_clock::now();
    const std::vector<int> z { 1, 2 }; // warmup
    t1 = std::chrono::high_resolution_clock::now();
    const long dz { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    t0 = std::chrono::high_resolution_clock::now();
    const std::vector<int> a { 1, 2 };
    t1 = std::chrono::high_resolution_clock::now();
    const long da { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    t0 = std::chrono::high_resolution_clock::now();
    const std::vector<int> b({ 1, 2 });
    t1 = std::chrono::high_resolution_clock::now();
    const long db { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    t0 = std::chrono::high_resolution_clock::now();
    const std::vector<int> c { { 1, 2 } };
    t1 = std::chrono::high_resolution_clock::now();
    const long dc { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    t0 = std::chrono::high_resolution_clock::now();
    const std::vector<int> d = { 1, 2 };
    t1 = std::chrono::high_resolution_clock::now();
    const long dd { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    t0 = std::chrono::high_resolution_clock::now();
    std::vector<int> e(2);
    e[0] = 1;
    e[1] = 2;
    t1 = std::chrono::high_resolution_clock::now();
    const long de { std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() };

    std::cout
        << z << " " << dz << '\n'
        << a << " " << da << '\n'
        << b << " " << db << '\n'
        << c << " " << dc << '\n'
        << d << " " << dd << '\n'
        << e << " " << de << '\n'
        << '\n';

    std::vector<std::vector<std::complex<double>>> nest({std::vector<std::complex<double>>(1),std::vector<std::complex<double>>(2)});
    for (std::vector<std::complex<double>> vec : nest) {
        std::cout << vec.size() << '\n';
    }
}
