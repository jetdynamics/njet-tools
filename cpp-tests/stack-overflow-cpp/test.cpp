#include <array>
#include <complex>
#include <iostream>
#include <vector>

#include <qd/qd_real.h>

class A {
    std::array<double, 1 << 20> data;
};

int main()
{

    // // ONE
    // // fine (4MB) this is the (granular) limit, array on stack
    // std::array<double, 1 << 19> arr1;
    // // fine (8MB) no limit, vector on heap
    // std::vector<double> arr2(1 << 20);

    // // TWO
    // // stack overflow (8192KiB)
    // cf `ulimit -s`
    // std::array<double, 1 << 20> arr2;

    // // THREE
    // // fine (2MB + 2MB)
    // std::array<double, 1 << 18> arr1;
    // std::array<double, 1 << 18> arr2;

    // // FOUR
    // // stack overflow (4MB + 4MB) stack limit is on sum of everything in stack, not each thing individually!
    // std::array<double, 1 << 19> arr1;
    // std::array<double, 1 << 19> arr2;

    // // FIVE
    // // fine (8GB) no limit, vector on heap
    // // have a look on htop to see 50% memory usage on initialisation of this vector
    // std::vector<double> arr2(1 << 30);

    // // SIX
    // // class member arrays still cause seg fault in same way (stack overflow)
    // A a;

    std::cout << "\nEND\n";
}
