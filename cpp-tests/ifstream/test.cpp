#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

int main() {
  std::ifstream infile("test.csv");
  std::string line;

  std::getline(infile, line);
  std::istringstream iss(line);
  // skip two
  int tmp_char;
  iss >> tmp_char;
  iss >> tmp_char;
  // default is end-of-stream iterator
  std::istream_iterator<int> it_begin(iss), it_end;
  std::vector<int> v(it_begin, it_end);

  for (int a : v) {
    std::cout << a << ' ';
  }
  std::cout << '\n';

  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    int n;
    while (iss >> n) {
      std::cout << n << ' ';
    }
    std::cout << '\n';
  }
}
