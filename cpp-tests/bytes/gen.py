#!/usr/bin/env python3

# https://docs.python.org/3/library/struct.html
import struct


def send_int(f, i):
    # little-endian long (integer, 4 bytes = 32 bits)
    bi = struct.pack("<l", i)
    f.write(bi)


def send_double(f, fp):
    # little-endian double (floating point, 8 bytes = 64 bits)
    bfp = struct.pack("<d", fp)
    f.write(bfp)


if __name__ == "__main__":
    nrows = 2
    ncols = 3
    nnz = [1, 0, 2]
    values = [[0, 0, -1, 2], [1, 2, 1, 2], [0, 2, 2, 1]]
    with open("data.bin", "wb") as f:
        send_int(f, nrows)
        send_int(f, ncols)
        send_int(f, len(nnz))
        for n in nnz:
            send_int(f, n)
        for v in values:
            send_int(f, v[0])
            send_int(f, v[1])
            send_double(f, v[2])
            send_double(f, v[3])
