#include <complex>
#include <iostream>

#include <qd/qd_real.h>

int main() {
  dd_real real_dd(-1.);
  std::complex<dd_real> complex_dd(1., 2.);
  std::cout << sqrt(real_dd) << ' ' << std::sqrt(std::complex<dd_real>(real_dd))
            << ' ' << std::sqrt(complex_dd) << '\n';
}
