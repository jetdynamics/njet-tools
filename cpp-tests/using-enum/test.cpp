#include <complex>
#include <iostream>

template <typename T> struct Super {
  enum Enum {
    Zero,
    One,
  };
};

template <typename T> struct Sub : public Super<T> {
  using Base = Super<T>;
  using Base::Zero;
  void fn();
};

template <typename T> void Sub<T>::fn() { std::cout << Zero << '\n'; }

int main() {
  Sub<double> test;
  test.fn();
}
