#include <atomic>
#include <chrono>
#include <cstdlib>
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <numeric>
#include <random>
#include <thread>

#include <taskflow/taskflow.hpp>

#include "common.hpp"

double serial(const long total)
{
    std::mt19937_64 rng(std::random_device {}());
    const double r { 1. };
    std::uniform_real_distribution<double> dist(-r, r);
    const double radius2 { r * r };

    long inside {};

    for (long j { 0 }; j < total; ++j) {
        const double x { dist(rng) };
        const double y { dist(rng) };

        if (x * x + y * y < radius2) {
            ++inside;
        }
    }

    return 4. * static_cast<double>(inside) / static_cast<double>(total);
}

// thread-safe random doubles
class Random {
private:
    std::mt19937_64 rng;
    std::uniform_real_distribution<double> dist;
    std::mutex mtx;

public:
    Random(const double r)
        : rng(std::random_device {}())
        , dist(-r, r)
    {
    }

    double gen()
    {
        std::scoped_lock<std::mutex> locked(mtx);
        return dist(rng);
    }

    void seed(const int i)
    {
        std::scoped_lock<std::mutex> locked(mtx);
        rng.seed(i);
    }
};

double omp(const long total)
{
    const double r { 1. };
    const double radius2 { r * r };
    Random random(r);

    std::atomic<long> inside {};

#pragma omp parallel for
    for (long j = 0; j < total; ++j) {
        const double x { random.gen() };
        const double y { random.gen() };

        if (x * x + y * y < radius2) {
            ++inside;
        }
    }

    return 4. * static_cast<double>(inside) / static_cast<double>(total);
}

double parallel_taskflow_for_each_index(const long total)
{
    const double r { 1. };
    const double radius2 { r * r };
    Random random(r);

    tf::Executor executor;
    tf::Taskflow taskflow;

    std::atomic<long> inside {};

    taskflow.for_each_index(0l, total, 1l, [radius2, &random, &inside](int i) {
        const double x { random.gen() };
        const double y { random.gen() };

        if (x * x + y * y < radius2) {
            ++inside;
        }
    });

    executor.run(taskflow).get();

    return 4. * static_cast<double>(inside) / static_cast<double>(total);
}

long once(const long num, const int rseed)
{
    std::mt19937_64 rng(std::random_device {}());
    rng.seed(rseed);

    const double r { 1. };
    const double radius2 { r * r };
    std::uniform_real_distribution<double> dist(-r, r);

    long inside {};

    for (long j { 0 }; j < num; ++j) {
        const double x { dist(rng) };
        const double y { dist(rng) };

        if (x * x + y * y < radius2) {
            ++inside;
        }
    }

    return inside;
}

double parallel_thread(const long total, const int n)
{
    const long each { total / n };
    long extra { total % n };

    std::vector<std::thread> threads;

    std::atomic<long> inside {};

    for (int i { 0 }; i < n; ++i) {
        long num { each };

        if (extra != 0) {
            ++num;
            --extra;
        }

        threads.emplace_back(std::thread([&inside, num, i]() {
            const long sub { once(num, i) };
            inside += sub;
        }));
    }

    for (std::thread& t : threads) {
        t.join();
    }

    return 4. * static_cast<double>(inside) / static_cast<double>(total);
}

double parallel_future(const long total, const int n)
{
    const long each { total / n };
    long extra { total % n };

    std::vector<std::thread> threads;

    std::vector<std::future<long>> insides(n);

    for (int i { 0 }; i < n; ++i) {
        long num { each };

        if (extra != 0) {
            ++num;
            --extra;
        }

        insides[i] = std::async([num, i]() { return once(num, i); });
    }

    long inside { std::transform_reduce(insides.begin(), insides.end(), 0l, std::plus<>(), [](std::future<long>& a) {
        return a.get();
    }) };

    return 4. * static_cast<double>(inside) / static_cast<double>(total);
}

int main()
{
    Timer timer;
    const long total { 1'000'000'000 };
    const int n = std::thread::hardware_concurrency();

    std::cout << std::fixed << std::setprecision(16) << '\n'
              << "n = " << total << '\n'
              << '\n';

    // no load balancing!
    // how to do thread pool?
    std::cout << "manually threaded (nproc=" << n << ")" << '\n';
    timer.start();
    const double pi2 { parallel_thread(total, n) };
    std::cout
        << pi2 << '\n'
        << timer.split_ms() << " ms" << '\n'
        << '\n';

    // as previous
    std::cout << "async/futures (nproc=" << n << ")" << '\n';
    timer.start();
    const double pi5 { parallel_future(total, n) };
    std::cout
        << pi5 << '\n'
        << timer.split_ms() << " ms" << '\n'
        << '\n';

    std::exit(EXIT_SUCCESS);

    std::cout << "serial" << '\n';
    timer.start();
    const double pi0 { serial(total) };
    std::cout
        << pi0 << '\n'
        << timer.split_ms() << " ms" << '\n'
        << '\n';

    // This is slow because of the mutexes
    std::cout << "OpenMP" << '\n';
    timer.start();
    const double pi3 { omp(total) };
    std::cout
        << pi3 << '\n'
        << timer.split_ms() << " ms" << '\n'
        << '\n';

    // This is slow because of the mutexes
    std::cout << "taskflow" << '\n';
    timer.start();
    const double pi1 { parallel_taskflow_for_each_index(total) };
    std::cout
        << pi1 << '\n'
        << timer.split_ms() << " ms" << '\n'
        << '\n';
}
