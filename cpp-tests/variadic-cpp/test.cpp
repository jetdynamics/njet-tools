#include <cstdarg>
#include <functional>
#include <iostream>

// variadic
void fn(int* a...)
{
    va_list args;
    va_start(args, a);

    // termination condition wrong
    while (*a != '\0') {
        int i = va_arg(args, int);
        std::cout << i << '\n';
        ++a;
    }

    va_end(args);
}

void f1(int a)
{
    std::cout << "1 " << a << '\n';
}

void f2(int a, int b)
{
    std::cout << "2 " << a << b << '\n';
}

// parameter pack
template <typename FUN, typename... Ts>
void f(FUN func, Ts... args)
{
    func(args...);
}

int main()
{
    // int one { 1 };
    // int two { 2 };
    // fn(&one, &two);

    f(f2, 0, 0);
}
