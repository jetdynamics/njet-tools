# simd-cpp
## Copmiler auto-vectorisation
* <https://gcc.gnu.org/projects/tree-ssa/vectorization.html>
* No GCC auto-vectorisation at `-O2`
* `-ftree-vectorize` turned on at `-O3`
* Denote `-O2 -ftree-vectorize` as `-Ov`
* `-O3`/`-Ov` speeds up `std` but slows `eigen`
## C++ execution policies (not SIMD)
* <https://en.cppreference.com/w/cpp/algorithm/execution_policy_tag_t>
* `seq`: serial in-order (normal/old/default algos)
* `unseq`: serial out-of-order
* `par`: parallel in-order
* `par_unseq`: parallel out-of-order
## Eigen
* <https://eigen.tuxfamily.org/index.php?title=FAQ#Vectorization>
* Uses vectorisation as made avaiable by setting `-march`
