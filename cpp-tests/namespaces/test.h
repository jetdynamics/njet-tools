template <typename T> struct DataStruct {
  DataStruct();

  T datum;
};

template <typename T> DataStruct<T>::DataStruct() : datum(1.) {}
