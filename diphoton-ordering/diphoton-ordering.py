from argparse import ArgumentParser

parser = ArgumentParser(description='Generates orderings for diphoton amplitudes in njet.')
parser.add_argument('amplitude', type=str,
                    help='String representing the particles of the external legs of the amplitude, with g for gluon and'
                         ' A for photon. eg. "gggAA"')
args = parser.parse_args()
amp = args.amplitude

mul = len(amp)


def find_particles(particle):
    return [i for i, s in enumerate(amp) if s is particle]


gs = find_particles("g")
perm_len = len(gs)
perms = []

for n in range(perm_len):
    perms += [[(i + n) % perm_len for i in range(perm_len)]]

g_perms = [[gs[i] for i in perm] for perm in perms]

As = find_particles("A")
bases = [[As[0], *amp_perm] for amp_perm in g_perms]

amp_perms = []

for i in range(1, mul):
    amp_perms += [[*base[:i], As[1], *base[i:]] for base in bases]

for order in amp_perms:
    print(order)
