#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include "finrem/0q3g2A/0q3g2A-2l.h"
#include "ngluon2/Mom.h"
#include "ngluon2/Model.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  std::cout << std::scientific << std::setprecision(16);

  PhaseSpace<T> ps(5, 1, 1.);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  const std::vector<double> scales2{{0.}};
  refineM(momenta, momenta, scales2);

  const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
  Amp0q3g2A_a2l<T, T> amp(Ax);

  amp.setMuR2(std::pow(91.188, 2));
  amp.setNf(5);
  amp.setNc(3);

  amp.setMomenta(momenta);
  amp.initFinRem();
  amp.setSpecFuncs1L();

  std::cout << "born: " << amp.c1lx1l_fin() << '\n'
            << '\n';
}

int main() { run<double>(); }
