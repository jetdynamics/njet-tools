#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include <qd/dd_real.h>

#include "finrem/0q5g/ggggg/0q5g-ggggg.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  std::cout << std::scientific << std::setprecision(16);

  PhaseSpace<T> ps(5, 1, 1.);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  const std::vector<double> scales2{{0.}};
  refineM(momenta, momenta, scales2);

  Amp0q5g_ggggg_a2l<T, T> amp;

  amp.setMuR2(std::pow(91.188, 2));
  amp.setNf(5);
  amp.setNc(3);

  amp.setMomenta(momenta);
  amp.initFinRem();
  amp.setSpecFuncs();

  std::cout << "born:    " << amp.c0lx0l_fin() << '\n'
            << "virt:    " << amp.c1lx0l_fin() << '\n'
            << "virtsq:  " << amp.c1lx1l_fin() << '\n'
            << "dblvirt: " << amp.c2lx0l_fin() << '\n'
            << '\n';
}

int main() { run<dd_real>(); }
