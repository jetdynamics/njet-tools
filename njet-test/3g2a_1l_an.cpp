#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

#include "analytic/0q3g2A-analytic.h"
#include "ngluon2/Mom.h"
#include "ngluon2/Model.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  std::cout << std::scientific << std::setprecision(16);

  PhaseSpace<T> ps(5, 1, 1.);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  const std::vector<double> scales2{{0.}};
  refineM(momenta, momenta, scales2);

  const Flavour<double> Ax { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
  Amp0q3g2A_a<T> amp(Ax);

  amp.setMuR2(std::pow(91.188, 2));
  amp.setNf(5);
  amp.setNc(3);

  amp.setMomenta(momenta);

  std::cout << "born: " << amp.virtsq() << '\n' << '\n';
}

int main() { run<double>(); }
