#include <array>
#include <iostream>
#include <vector>

#include "interface.h"

int main()
{
    NJetSherpaInterface::initNJet();

    const double mur { 91.118 };
    const double alphas { 0.118 };
    const double alpha { 7.29735257e-3 };

    const std::vector<std::array<double, 4>> mom2 {
        { 5.000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e+00 },
        { 5.0000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e+00 },
        { 5.0000000000000000e+00, 4.3328718243523374e+00, 1.2317414119409191e+00, 2.1700310707092285e+00 },
        { 5.0000000000000000e+00, -4.3328718243523374e+00, -1.2317414119409191e+00, -2.1700310707092285e+00 }
    };
    const std::vector<std::array<double, 4>> mom3 {
        { 5.0000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e+00 },
        { 5.0000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e+00 },
        { 4.1073910982315063e+00, 2.0393336282060184e+00, -3.5476001585959542e+00, -3.5540554501787402e-01 },
        { 2.3727316959679570e+00, 6.2348782873773423e-01, 1.6215556401520543e+00, 1.6160680475641929e+00 },
        { 3.5198772058005368e+00, -2.6628214569437527e+00, 1.9260445184439003e+00, -1.2606625025463190e+00 },
    };

    // 2g2A

    // const int id2 { NJetSherpaInterface::getProcessLO("21 21 -> 22 22", 2, 2) };
    // const std::complex<double> res2 { NJetSherpaInterface::evalProcess(id2, mom2, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res2.real() << "\n";
// #endif

    // const int id4 { NJetSherpaInterface::getProcessCC("21 21 -> 22 22", 2, 2, 0, 1) };
    // const std::complex<double> res4 { NJetSherpaInterface::evalProcess(id4, mom2, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res4.real() << "\n";
// #endif

    // const int id8 { NJetSherpaInterface::getProcessSC("21 21 -> 22 22", 2, 2, 0, 1) };
    // const std::complex<double> res8 { NJetSherpaInterface::evalProcess(id8, mom2, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res8 << "\n";
// #endif

    const int id12 { NJetSherpaInterface::getProcessCS("21 21 -> 22 22", 2, 2, 0) };
    const std::complex<double> res12 { NJetSherpaInterface::evalProcess(id12, mom2, mur, alphas, alpha) };
#ifndef DEBUG
    std::cout << res12 << "\n";
#endif

    // const int id13 { NJetSherpaInterface::getProcessCS("21 21 -> 22 22", 2, 2, 1) };
    // const std::complex<double> res13 { NJetSherpaInterface::evalProcess(id13, mom2, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res13 << "\n";
// #endif

    // 3g2A

    // const int id3 { NJetSherpaInterface::getProcessLO("21 21 -> 21 22 22", 3, 2) };
    // const std::complex<double> res3 { NJetSherpaInterface::evalProcess(id3, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res3.real() << "\n";
// #endif

    // const int id5 { NJetSherpaInterface::getProcessCC("21 21 -> 21 22 22", 3, 2, 0, 1) };
    // const std::complex<double> res5 { NJetSherpaInterface::evalProcess(id5, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res5.real() << "\n";
// #endif

    // const int id6 { NJetSherpaInterface::getProcessCC("21 21 -> 21 22 22", 3, 2, 0, 2) };
    // const std::complex<double> res6 { NJetSherpaInterface::evalProcess(id6, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res6.real() << "\n";
// #endif

    // const int id7 { NJetSherpaInterface::getProcessCC("21 21 -> 21 22 22", 3, 2, 2, 1) };
    // const std::complex<double> res7 { NJetSherpaInterface::evalProcess(id7, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res7.real() << "\n";
// #endif

    // const int id9 { NJetSherpaInterface::getProcessSC("21 21 -> 21 22 22", 3, 2, 0, 1) };
    // const std::complex<double> res9 { NJetSherpaInterface::evalProcess(id9, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res9 << "\n";
// #endif

    // const int id10 { NJetSherpaInterface::getProcessSC("21 21 -> 21 22 22", 3, 2, 0, 2) };
    // const std::complex<double> res10 { NJetSherpaInterface::evalProcess(id10, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res10 << "\n";
// #endif

    // const int id11 { NJetSherpaInterface::getProcessSC("21 21 -> 21 22 22", 3, 2, 2, 1) };
    // const std::complex<double> res11 { NJetSherpaInterface::evalProcess(id11, mom3, mur, alphas, alpha) };
// #ifndef DEBUG
    // std::cout << res11 << "\n";
// #endif

    return 0;
}
