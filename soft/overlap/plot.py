#!/usr/bin/env python3
# coding=UTF-8

import pathlib

import numpy
import matplotlib.pyplot
import matplotlib.colors
import scipy.interpolate

# import seaborn


# def plot_a2s_sb(data):
#     fig, ax = matplotlib.pyplot.subplots()

#     seaborn.distplot(
#         data,
#         ax=ax,
#         bins=200,
#         kde=False,
#         hist_kws={"log": True, "histtype": "stepfilled"},
#     )

#     ax.set_xlabel("$|A|^2$")
#     ax.set_ylabel("Frequency")
#     ax.set_title(f"$|A|^2$ distribution. Sample size: {len(data)}.")

#     # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
#     fig.savefig("hist_sb.png", bbox_inches="tight", format="png", dpi=300)


def plot_a2s(data):
    print("Plotting histogram.")
    fig, ax = matplotlib.pyplot.subplots()

    ax.hist(
        data, bins=200, log=True, label=r"$gg \rightarrow ggg$", histtype="stepfilled"
    )
    ax.set_xlabel("$|A|^2$")
    ax.set_ylabel("Frequency")
    ax.set_title(f"$|A|^2$ distribution in size. Sample size: {len(data)}.")
    ax.legend()

    # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
    fig.savefig("hist.png", bbox_inches="tight", format="png", dpi=300)


def plot_3d(s34_, s45_, a2_):
    print("Generating 3d surface plot.")
    fig, ax = matplotlib.pyplot.subplots(subplot_kw={"projection": "3d"})

    ax.plot_trisurf(s34_, s45_, a2_)
    ax.set_xlabel("$s_{34}$")
    ax.set_ylabel("$s_{45}$")
    ax.set_zlabel("$|A|^2$")
    ax.set_title(
        r"$|A|^2$ distribution in $s_{34},s_{45}$-plane. " + f"Sample size: {len(a2_)}."
    )

    matplotlib.pyplot.show()

    # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
    # fig.savefig("3d.png", bbox_inches="tight", format="png", dpi=300)


# This method fails if the PS is not isotropic!
# Plot kin suggests it is not sufficiently random in this plane for this technique!
def plot_2d_hist(s34_, s45_, a2_):
    print(
        "Plotting 2d histogram of |A|^2. Results only valid if phase space is isotopic!"
    )
    fig, ax = matplotlib.pyplot.subplots()

    h = ax.hist2d(s34_, s45_, weights=a2_, bins=200, norm=matplotlib.colors.LogNorm())
    ax.set_xlabel("$s_{34}$")
    ax.set_ylabel("$s_{45}$")

    fig.suptitle(
        r"$|A|^2$ distribution in $s_{34},s_{45}$-plane. " + f"Sample size: {len(a2_)}."
    )
    fig.colorbar(h[3], ax=ax)

    # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
    fig.savefig("2d_hist.png", bbox_inches="tight", format="png", dpi=300)


def data_to_grid(s34_, s45_, a2_, memo=True):
    print("Generating data grid from dataset by interpolation.")
    data_file_name = "grid.npy"

    if memo and pathlib.Path(data_file_name).is_file():
        with open(data_file_name, "rb") as f:
            return numpy.load(f)

    bins = 200
    x = numpy.linspace(min(s34_), max(s34_), bins)
    y = numpy.linspace(min(s45_), max(s45_), bins)

    X, Y = numpy.meshgrid(x, y)

    Z = scipy.interpolate.griddata(
        points=(s34_, s45_), values=a2_, xi=(X, Y), method="linear"
    )

    data = numpy.array([X, Y, Z])

    if memo:
        with open(data_file_name, "wb") as f:
            numpy.save(f, data)

    return data


def plot_2d_mesh(s34_, s45_, a2_):
    X, Y, Z = data_to_grid(s34_, s45_, a2_)

    print("Plotting 2d colourmap.")
    fig, ax = matplotlib.pyplot.subplots()

    im = ax.pcolormesh(
        X, Y, Z, norm=matplotlib.colors.LogNorm(), shading="nearest", cmap="viridis"
    )
    ax.set_xlabel("$s_{34}$")
    # ax.set_xlim(0, 5e7)
    ax.set_ylabel("$s_{45}$")
    # ax.set_ylim(0, 5e7)

    fig.suptitle(
        r"$|A|^2$ distribution in $s_{34},s_{45}$-plane. " + f"Sample size: {len(a2_)}."
    )
    fig.colorbar(im, ax=ax)

    # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
    fig.savefig("2d.png", bbox_inches="tight", format="png", dpi=300)


def plot_kin(s34_, s45_):
    print("Plotting 2d histogram of kinematics.")
    fig, ax = matplotlib.pyplot.subplots()

    h = ax.hist2d(s34_, s45_, bins=200, norm=matplotlib.colors.LogNorm())
    ax.set_xlabel("$s_{34}$")
    ax.set_ylabel("$s_{45}$")

    fig.suptitle(
        r"Kinematic distribution in $s_{34},s_{45}$-plane. "
        + f"Sample size: {len(s34_)}."
    )
    fig.colorbar(h[3], ax=ax)

    # fig.savefig("test.pdf", bbox_inches="tight", format="pdf")
    fig.savefig("kin.png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":
    in_name = "data.npy"

    with open(in_name, "rb") as f:
        s34, s45, a2 = numpy.load(f)

    print(min(numpy.concatenate((s34, s45))))
    print(max(numpy.concatenate((s34, s45))))

    # plot_a2s(a2)
    # plot_a2s_sb(a2)

    # plot_kin(s34, s45)
    # plot_2d_hist(s34, s45, a2)

    # plot_2d_mesh(s34, s45, a2)

    # plot_3d(s34, s45, a2)
