#include <algorithm>
#include <array>
#include <cmath>
#include <complex>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "chsums/0q6g.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

const int Nc { 3 };
const int Nc2 { Nc * Nc };
const int Nc4 { Nc2 * Nc2 };
const int V { Nc2 - 1 };
const int CA { Nc };

// index symmetric matrix (including diagonal) in compact form
int is(int i, int j)
{
    return i <= j ? i + j * (j + 1) / 2 : j + i * (i + 1) / 2;
}

int cc2(int i, int j, int k, int l, int a, int b)
{
    const std::array<std::array<int, 6>, 136> cc2arr { {
        { 0, 1, 0, 1, 1, 0 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 4, 2, 4, 3, 2 },
        { 6, 7, 8, 9, 7, 6 },
        { 0, 1, 0, 1, 1, 0 },
        { 2, 4, 5, 3, 4, 2 },
        { 8, 7, 6, 7, 9, 6 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 6, 9, 6, 7, 7, 8 },
        { 5, 5, 5, 3, 3, 10 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 5, 5, 3, 3, 10 },
        { 6, 9, 6, 7, 7, 8 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 15, 16, 15, 17, 17, 16 },
        { 8, 7, 6, 7, 9, 6 },
        { 2, 4, 5, 3, 4, 2 },
        { 0, 1, 0, 1, 1, 0 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 15, 16, 15, 17, 17, 16 },
        { 6, 9, 6, 7, 7, 8 },
        { 6, 7, 8, 9, 7, 6 },
        { 5, 4, 2, 4, 3, 2 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 15, 17, 16, 16, 17, 15 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 16, 17, 15, 17, 16, 15 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 5, 3, 10, 5, 3, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 5, 3, 10, 5, 3, 5 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 5, 4, 2, 4, 3, 2 },
        { 2, 4, 5, 3, 4, 2 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 14, 11, 12, 12, 13 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 15, 17, 16, 16, 17, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 6, 7, 8, 9, 7, 6 },
        { 8, 7, 6, 7, 9, 6 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 14, 11, 12, 12, 13 },
        { 5, 3, 10, 5, 3, 5 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 6, 9, 6, 7, 7, 8 },
        { 15, 16, 15, 17, 17, 16 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 0, 1, 0, 1, 1, 0 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 15, 17, 16, 16, 17, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 10, 3, 5, 3, 5, 5 },
        { 15, 16, 15, 17, 17, 16 },
        { 6, 9, 6, 7, 7, 8 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 0, 1, 0, 1, 1, 0 },
        { 11, 14, 11, 12, 12, 13 },
        { 15, 17, 16, 16, 17, 15 },
        { 6, 7, 8, 9, 7, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 16, 17, 15, 17, 16, 15 },
        { 11, 14, 11, 12, 12, 13 },
        { 8, 7, 6, 7, 9, 6 },
        { 11, 12, 13, 14, 12, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 9, 6, 7, 7, 8 },
        { 5, 5, 5, 3, 3, 10 },
        { 8, 7, 6, 7, 9, 6 },
        { 6, 7, 8, 9, 7, 6 },
        { 0, 1, 0, 1, 1, 0 },
        { 5, 3, 10, 5, 3, 5 },
        { 11, 14, 11, 12, 12, 13 },
        { 13, 12, 11, 12, 14, 11 },
        { 6, 7, 8, 9, 7, 6 },
        { 11, 14, 11, 12, 12, 13 },
        { 10, 3, 5, 3, 5, 5 },
        { 11, 12, 13, 14, 12, 11 },
        { 8, 7, 6, 7, 9, 6 },
        { 13, 12, 11, 12, 14, 11 },
        { 11, 12, 13, 14, 12, 11 },
        { 5, 5, 5, 3, 3, 10 },
        { 6, 9, 6, 7, 7, 8 },
        { 2, 4, 5, 3, 4, 2 },
        { 5, 4, 2, 4, 3, 2 },
        { 2, 3, 2, 4, 4, 5 },
        { 0, 1, 0, 1, 1, 0 },
    } };

    const int norma { V };

    const std::array<int, 18> vals {
        4 * (6 - 2 * Nc2 + Nc4),
        -8 * (-3 + Nc2),
        -4 - Nc4,
        -4,
        2 * (-2 + Nc2),
        4 * V,
        -2 * (4 + Nc4),
        4 * (-2 + Nc2),
        8 * V,
        -8,
        -4 + 8 * Nc2,
        4 - 6 * Nc2,
        4 - 2 * Nc2,
        4 + 2 * Nc2 + Nc4,
        4,
        Nc2 * (4 + Nc2),
        4 * Nc2,
        -2 * Nc2,
    };

    int ii { 4 * i + j };
    int jj { 4 * k + l };

    return norma * vals[cc2arr[is(ii, jj)][is(a, b)]];
}

double s1(const MOM<double> pi, const MOM<double> pj, const MOM<double> q)
{
    return dot(pi, pj) / (dot(pi, q) * dot(pj, q));
}

double s2(const MOM<double> pi, const MOM<double> pj, const MOM<double> q1, const MOM<double> q2)
{
    return (2 * dot(pi, q1) * dot(pi, q2) * dot(pj, q1) * dot(pj, q2) * (dot(pi, q2) * dot(pj, q1) + dot(pi, q1) * dot(pj, q2)) + dot(pi, pj) * (std::pow(dot(pi, q1), 2) * dot(pj, q2) * (2 * dot(pj, q1) + dot(pj, q2)) + std::pow(dot(pi, q2), 2) * dot(pj, q1) * (dot(pj, q1) + 2 * dot(pj, q2)) + 2 * dot(pi, q1) * dot(pi, q2) * (std::pow(dot(pj, q1), 2) - dot(pj, q1) * dot(pj, q2) + std::pow(dot(pj, q2), 2))) * dot(q1, q2) - std::pow(dot(pi, pj), 2) * (dot(pi, q1) * (2 * dot(pj, q1) + dot(pj, q2)) + dot(pi, q2) * (dot(pj, q1) + 2 * dot(pj, q2))) * std::pow(dot(q1, q2), 2)) / (2. * dot(pi, q1) * dot(pi, q2) * (dot(pi, q1) + dot(pi, q2)) * dot(pj, q1) * dot(pj, q2) * (dot(pj, q1) + dot(pj, q2)) * std::pow(dot(q1, q2), 2));
}

double s2so(const MOM<double> pi, const MOM<double> pj, const MOM<double> q1, const MOM<double> q2)
{
    return (dot(pi, pj) * (dot(pi, q2) * dot(pj, q1) + dot(pi, q1) * dot(pj, q2) - dot(pi, pj) * dot(q1, q2))) / (dot(pi, q1) * dot(pi, q2) * dot(pj, q1) * dot(pj, q2) * dot(q1, q2));
}

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    const int N { 6 };     // full amplitude multiplicity
    const int o { 2 };     // how many soft gluons
    const int n { N - o }; // reduced amplitude multiplicity

    // double soft: p[2], p[3] -> 0
    const std::array<MOM<double>, N> full_mom { {
        { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999999999999998e-07, 0.0000000000000000e+00, -4.3301270189221928e-07, 2.5000000000000004e-07 },
        { 4.9999999999999998e-07, 1.4694631307311831e-07, -4.3301270189221928e-07, 2.0225424859373691e-07 },
        { 4.9999907881923744e-01, 3.2027063345893469e-01, -3.2637274674779743e-01, 2.0225387596829103e-01 },
        { 4.9999992118076264e-01, -3.2027078040524776e-01, 3.2637361277320120e-01, -2.0225432822253964e-01 },
    } };

    // strong-ordering soft limit q2<<q1<<1
    // const std::array<MOM<double>, 6> full_mom { {
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e-01 },
    //     { -5.0000000000000000e-01, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e-01 },
    //     { 4.9999999999999998e-07, 0.0000000000000000e+00, -4.3301270189221928e-07, 2.5000000000000004e-07 },
    //     { 4.9999999999999999e-13, 1.4694631307311831e-13, -4.3301270189221931e-13, 2.0225424859373692e-13 },
    //     { 4.9999955811210195e-01, 3.2027094046635896e-01, -3.2637305960463120e-01, 2.0225406984632735e-01 },
    //     { 4.9999994188739805e-01, -3.2027094046650589e-01, 3.2637349261776610e-01, -2.0225431984652961e-01 },
    // } };

    std::array<MOM<double>, n> reduced_mom;
    std::copy_n(full_mom.cbegin(), 2, reduced_mom.begin());
    std::copy_n(full_mom.cbegin() + 4, 2, reduced_mom.begin() + 2);

    std::array<MOM<double>, o> soft_mom;
    std::copy_n(full_mom.cbegin() + 2, 2, soft_mom.begin());

    const double scale { 1. };
    Amp0q6g<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    Helicity<N> full_hels { +1, -1, +1, -1, +1, -1 };
    Helicity<n> reduced_hels;
    std::copy_n(full_hels.cbegin(), 2, reduced_hels.begin());
    std::copy_n(full_hels.cbegin() + 4, 2, reduced_hels.begin() + 2);
    // Helicity<3> soft_hels; // doesn't matter
    reduced_amp.setHelicity(reduced_hels.data());

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[2]^0                   = " << soft_mom[0].x0 << '\n'
        << "p[3]^0                   = " << soft_mom[1].x0 << '\n'
        << full_hels << " > " << reduced_hels << " * "
        << "S" << '\n'
        << '\n';

    // double lim { 0. };
    // int ref_index { 0 };
    // for (int i { 0 }; i < 4; ++i) {
    //     for (int j { i + 1 }; j < 4; ++j) {
    //         while ((ref_index == i) || (ref_index == j)) {
    //             ++ref_index;
    //         }
    //         // the second argument is a reference momentum
    //         soft_amp.setMomenta({ reduced_mom[i], full_mom[3], reduced_mom[j] }, reduced_mom[ref_index]);
    //         const double soft_val { soft_amp.born(soft_hels.data()) };
    //         const double cc_val { reduced_amp.born_ccij(reduced_hels.data(), i, j) };
    //         lim += cc_val * soft_val;
    //     }
    // }
    // lim *= 2.;

    // helicity amplitude modulus squared - includes colour
    const double amp_val { full_amp.born(full_hels.data()) };

    const int cbl { 3 }; // length of colour basis

    // for (int i { 0 }; i < 4; ++i) {
    //     std::cout
    //         << i << "312"
    //         << '\n';
    //     for (int a { 0 }; a < cbl; ++a) {
    //         for (int b { 0 }; b < cbl; ++b) {
    //             std::cout
    //                 << cc2(i, 3, 1, 2, a, b)
    //                 << " ";
    //         }
    //         std::cout
    //             << '\n';
    //     }
    //     std::cout
    //         << '\n';
    // }

    // for (int a { 0 }; a < cbl; ++a) {
    //     for (int b { 0 }; b < cbl; ++b) {
    //         std::cout
    //             << cc2(2, 3, 1, 2, a, b)
    //             << " ";
    //     }
    //     std::cout
    //         << '\n';
    // }
    // std::cout
    //     << '\n';

    std::array<std::complex<double>, cbl> vec {
        // A0(...) tmp moved to public for this
        reduced_amp.A0(0, 1, 2, 3),
        reduced_amp.A0(0, 1, 3, 2),
        reduced_amp.A0(0, 3, 1, 2),
    };

    // for (std::complex<double> v : vec) {
    //     std::cout << v << '\n';
    // }

    // std::cout << -reduced_amp.A0(0, 1, 2, 3)-reduced_amp.A0(0, 1, 3, 2) << '\n';

    double term_one { 0. };

    for (int i { 0 }; i < n; ++i) {
        for (int j { 0 }; j < n; ++j) {
            for (int k { 0 }; k < n; ++k) {
                for (int l { 0 }; l < n; ++l) {

                    std::complex<double> col_sum_2 { std::complex<double>() };

                    for (int a { 0 }; a < cbl; ++a) {

                        std::complex<double> col_i10e { std::complex<double>() };

                        for (int b { 0 }; b < cbl; ++b) {
                            col_i10e += static_cast<double>(cc2(i, j, k, l, a, b)) * vec[b];
                        }

                        col_sum_2 += std::conj(vec[a]) * col_i10e;
                    }

                    // std::cout
                    //     << col_sum_2
                    //     << '\n';

                    term_one += s1(reduced_mom[i], reduced_mom[j], soft_mom[0])
                        * s1(reduced_mom[k], reduced_mom[l], soft_mom[1])
                        * col_sum_2.real();
                }
            }
        }
    }

    term_one *= 0.5;

    std::cout << "colour-connected term    = " << term_one << '\n';

    double term_two { 0. };

    for (int i { 0 }; i < n; ++i) {
        for (int j { 0 }; j < n; ++j) {
            term_two += s2(reduced_mom[i], reduced_mom[j], soft_mom[0], soft_mom[1])
                // term_two += s2so(reduced_mom[i], reduced_mom[j], soft_mom[0], soft_mom[1])
                * reduced_amp.born_ccij(reduced_hels.data(), i, j);
        }
    }

    term_two *= -static_cast<double>(CA);

    std::cout << "un-colour-connected term = " << term_two << '\n';

    double lim { term_one + term_two };

    std::cout
        << '\n'
        << "full amplitude 2         = " << amp_val << '\n'
        << "limit 2                  = " << lim << '\n'
        << "lim2/amp2                = " << lim / amp_val << '\n';

    std::cout
        << '\n';
}
