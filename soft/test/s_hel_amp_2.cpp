#include <algorithm>
#include <array>
#include <complex>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    // p[3] -> 0
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 4.9999999999772449e-06, -2.2505887718767004e-06, 1.7633487034433740e-06, -4.1018839000804920e-06 },
        { 4.9999550000000009e-01, -4.3301001829074548e-01, -1.7633487034433740e-06, -2.4999564811609998e-01 },
    } };

    std::array<MOM<double>, 4> reduced_mom;
    std::copy_n(full_mom.cbegin(), 3, reduced_mom.begin());
    reduced_mom[3] = full_mom[4];

    const double scale { 1. };
    Amp0q5g_a<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);
    Softgg2g_a<double> soft_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    Helicity<5> full_hels { +1, +1, -1, -1, -1 };
    Helicity<4> reduced_hels { +1, +1, -1, -1 };
    Helicity<3> soft_hels { +1, -1, +1 };

    std::cout
        << '\n'
        << "zero indexed" << '\n'
        << "p[3]^0              = " << full_mom[3].x0 << '\n'
        << full_hels << " > " << reduced_hels << " * " << soft_hels << '\n';

    double lim { 0. };
    int ref_index { 0 };
    for (int i { 0 }; i < 4; ++i) {
        for (int j { i + 1 }; j < 4; ++j) {
            while ((ref_index == i) || (ref_index == j)) {
                ++ref_index;
            }
            // the second argument is a reference momentum
            soft_amp.setMomenta({ reduced_mom[i], full_mom[3], reduced_mom[j] }, reduced_mom[ref_index]);
            const double soft_val { soft_amp.born(soft_hels.data()) };
            const double cc_val { reduced_amp.born_ccij(reduced_hels.data(), i, j) };
            lim += cc_val * soft_val;
        }
    }
    lim *= 2.;

    // helicity amplitude modulus squared - includes colour
    const double amp_val { full_amp.born(full_hels.data()) };

    std::cout
        // << "sum" << '\n'
        // << "full amplitude 2    = " << amp_val << '\n'
        // << "limit 2             = " << lim << '\n'
        << "lim2/amp2           = " << lim / amp_val << '\n';

    std::cout << '\n';
}
