#!/usr/bin/env bash

# Generate helicity configurations of a QCD amplitudes with $m legs, including 2 quarks.
# Quarks at legs $q1 and $q2.
# Rejects configurations which violate quark helicity conservation

m=4
q1=1
q2=2

u=$((2 ** $m))
for ((n = 0; n < $u; n++)); do

    z=$n
    arr=()
    for ((i = $m - 1; i >= 0; i--)); do
        y=$(($z - (2 ** $i)))
        if (($y >= 0)); then
            z=$y
            arr[$i]="+1"
        else
            arr[$i]="-1"
        fi
    done

    if (( "${arr[$q1]}" != "${arr[$q2]}" )); then
        printf "{ "
        for ((j = 0; j < $m; j++)); do
            printf -- "${arr[$j]}"
            if (($j != ($m - 1))); then
                printf ", "
            fi
        done
        printf " },\n"
    fi

done
