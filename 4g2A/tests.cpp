#include <iomanip>
#include <iostream>
#include <vector>

#include "chsums/0q4gA.h"
#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

const int W { 7 };

const Flavour<double> Ax {
    StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())
};

const Flavour<double> Au {
    StandardModel::Au(StandardModel::u(), StandardModel::ubar())
};

const int Nc { 3 };
const int Nf { 5 };
const double mur2 { 1e3 };

// six-point
// ~~~~~~~~~~

template <typename T>
void hel4g2a(const std::vector<MOM<T>>& momenta)
{
    Amp0q4gAA<T> amp(Ax);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    amp.setMomenta(momenta);

    T sum_auto { amp.virtsq().get0().real() };

    T sum_man {};

    for (auto h : Amp0q4gAAStatic::HSarr) {
        for (int i { 0 }; i < 6; ++i) {
            std::cout << (h[i] == 1 ? '+' : '-');
        }
        std::cout << ": ";

        T born { amp.virtsq(h).get0().real() };
        std::cout << std::setw(W) << "hA11: " << born << '\n';
        sum_man += born;
    }

    std::cout << std::setw(2*W) << "sum hA11: " << sum_man << '\n';
    std::cout << std::setw(2*W) << "A11: " << sum_auto << '\n';
}

template <typename T>
void run()
{
    std::cout << std::scientific << std::setprecision(16)
              << '\n'
              << "Notation:" << '\n'
              << "  hA10: helicity amplitude squared: one-loop x tree (colour sum)" << '\n'
              << "  hA11: helicity amplitude squared: one-loop x one-loop (colour sum)" << '\n'
              << "  A10:  amplitude squared: one-loop x tree (helicity sum)" << '\n'
              << "  A11:  amplitude squared: one-loop x one-loop (helicity sum)" << '\n';

    const std::vector<double> scales2 { { 0 } };
    const int rseed { 1 };

    PhaseSpace<T> ps6(6, rseed);
    std::vector<MOM<T>> momenta6 { ps6.getPSpoint() };
    refineM(momenta6, momenta6, scales2);

    //std::cout << '\n'
    //          << "Six-point" << '\n';

    //for (int i { 0 }; i < 6; ++i) {
    //    std::cout << "p" << i << "=" << momenta6[i] << '\n';
    //}

    std::cout << '\n'
              << "4g2a" << '\n';

    hel4g2a<T>(momenta6);

    std::cout << '\n';
}

int main()
{
    run<double>();
}
