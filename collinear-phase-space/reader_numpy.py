# Python library

import pathlib

import numpy


def write(filename, data):
    binaryfile = filename + ".npy"
    if pathlib.Path(binaryfile).is_file():
        print(f"File {binaryfile} already exists.")
        while True:
            decision = input("Overwrite? [y/n] ")
            if decision == "n":
                return None
            elif decision == "y":
                break
    numpy.save(binaryfile, data)


def read_proc(folder, name, use_cache=True):
    data = numpy.empty((11, 10, 3, 100, 12))

    for channel in range(11):
        for rseed in range(1, 11):
            for i, prec in enumerate(("f64f64", "f128f64", "f128f128")):
                data[channel][rseed - 1][i] = read_memo(
                    f"{folder}/{name}.{prec}.{channel}.{rseed}.csv", use_cache
                )

    print(f"Length of dataset: {len(data[0][0][0])}")

    return data


def read_memo(filename, use_cache):
    binaryfile = filename + ".npy"

    if use_cache:
        try:
            return numpy.load(binaryfile)
        except OSError:
            pass

    data = numpy.loadtxt(filename)

    numpy.save(binaryfile, data)

    return data
