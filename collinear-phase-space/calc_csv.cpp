#include <cstddef>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "finrem/0q5g/ggggg.h"
// #include "finrem/2q3g/ggqbqg.h"
// #include "finrem/2q3g/qbgqgg.h"
// #include "finrem/2q3g/qbqggg.h"
// #include "finrem/4q1g/uDdUg.h"
// #include "finrem/4q1g/uUdDg.h"
// #include "finrem/4q1g/uUuUg.h"
// #include "finrem/4q1g/udUDg.h"
// #include "finrem/4q1g/ugUdD.h"
// #include "finrem/4q1g/ugUuU.h"
// #include "finrem/4q1g/uuUUg.h"
#include "finrem/common/rescue.h"

std::vector<std::vector<std::vector<double>>>
read_moms(const std::string &filename) {
  const int d{4};

  std::cout << "Reading momenta..." << '\n';

  std::ifstream infile(filename);
  std::string line, num;
  std::vector<std::vector<std::vector<double>>> points;

  while (std::getline(infile, line)) {
    std::vector<std::vector<double>> point;
    std::vector<double> mom(d);

    std::istringstream iss(line);

    int i{0};
    while (std::getline(iss, num, ' ')) {
      mom[i++] = std::stod(num);

      if (i == d) {
        point.push_back(mom);
        i = 0;
      }
    }

    points.push_back(point);
  }

  return points;
}

template <template <typename, typename> class AMP>
void run(const std::vector<std::vector<std::vector<double>>> &momenta,
         const double cutoff, const std::string &name) {
  const double mur{91.188};
  const int Nc{3}, Nf{5};

  Rescue2LNNLO<AMP> amp(cutoff);
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(std::pow(mur, 2));

  for (std::size_t p{0}; p < momenta.size(); ++p) {
    amp.setMomenta(momenta[p]);

    amp.eval_timed(p, name, "ir");
  }
}

int main(int argc, char *argv[]) {
  assert(argc == 2);
  const std::string name{argv[1]};

  std::vector<std::vector<std::vector<double>>> moms{read_moms(name)};

  // for (auto pt : moms) {
  //   for (auto mom : pt) {
  //     for (auto comp : mom) {
  //       std::cout << comp << ' ';
  //     }
  //     std::cout << '\n';
  //   }
  //   std::cout << '\n';
  // }

  run<Amp0q5g_ggggg_a2l>(moms, 1e16, "ggggg");
}
