#!/usr/bin/env python3

import numpy

from phase_space_phys import point, collinear
from phase_space_numpy import sij


if __name__ == "__main__":
    rseed = 0
    N = 128
    n = 5
    z = 0.5
    c = 2

    pts = []
    ds = 10 ** -numpy.linspace(1, 4, N)

    ps4 = point(n - 1, rseed)

    for i, d in enumerate(ds):
        pt = collinear(ps4, z, d, c)

        print(i, sij(pt, c, (c + 1) % (n - 1)) / sij(pt, 0, 1))

        pts.append(pt)

    name = f"n={n}_z={z}_d=var_c={c}_N={N}_r={rseed}"

    numpy.save(f"vars_{name}", ds)

    numpy.savetxt(f"moms_{name}.csv", [mom.flatten() for mom in pts])
