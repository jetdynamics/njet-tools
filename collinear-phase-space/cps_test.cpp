#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "cps.hpp"

int main() {
  std::cout << std::scientific;

  const int rseed{1}, i{2}, start{49}, num{100};
  const double sqrtS{1.}, z{0.5}, base(1.1);

  PhaseSpace<double> ps4(4, rseed, sqrtS);
  const std::vector<MOM<double>> moms4{ps4.getPSpoint()};

  for (int a{0}; a < num; ++a) {
    const int p{start + a};
    const double d{std::pow(base, -p)};

    const std::vector<MOM<double>> moms5{cps::collinear(moms4, z, d, i)};

    // std::cout << '\n';
    // for (const MOM<double> &mom : moms5) {
    //   std::cout << mom << '\n';
    // }
    // std::cout << '\n';

    std::vector<double> v{cps::invariants(moms5)};

    const double delta{std::pow(v[0] * v[1] + v[1] * v[2] - v[2] * v[3] +
                                    v[3] * v[4] - v[4] * v[0],
                                2) -
                       4. * v[0] * v[1] * v[2] * (v[1] - v[3] - v[4])};

    std::cout << "y = " << v[i] / v[0] << " | LI ? " << cps::test(moms5) << " | D<0 ? " << (delta < 0) << '\n';
  }
}
