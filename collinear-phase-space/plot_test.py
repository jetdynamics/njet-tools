#!/usr/bin/env python3

import numpy

import reader, matplotlib.pyplot

if __name__ == "__main__":
    ds = reader.read_memo("data/test.f64f64.csv", True)
    ys = numpy.loadtxt("data/test.sijs.csv")

    val = ds["ggggg"][:, 7] / ds["ggggg"][:, 1]
    err = val * ds["ggggg"][:, 8]  # assume 2L errors dominate

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    fig, ax = matplotlib.pyplot.subplots()

    # ax.plot(ys, )
    ax.fill_between(
        ys,
        val - err,
        val + err,
        alpha=0.2,
    )
    ax.plot(ys, val)

    ax.set_xscale("log", base=10)
    ax.set_yscale("log", base=10)

    ax.set_title(r"$i||j$")
    ax.set_xlabel(r"$s_{ij}/s_{12}$")
    ax.set_ylabel(r"$\mathcal{A}^{(2)} \cdot \mathcal{A}^{(0)}/\left|\mathcal{A}^{(0)}\right|^2$")

    fig.savefig("fig.pdf")
