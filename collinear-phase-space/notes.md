# Means
## 2L
![](mean_ggggg_2l.pdf){width=50%}
![](mean_ggqbqg_2l.pdf){width=50%}
![](mean_qbgqgg_2l.pdf){width=50%}
![](mean_qbqggg_2l.pdf){width=50%}
![](mean_uDdUg_2l.pdf){width=50%}
![](mean_udUDg_2l.pdf){width=50%}
![](mean_ugUdD_2l.pdf){width=50%}
![](mean_uUdDg_2l.pdf){width=50%}
![](mean_ugUuU_2l.pdf){width=50%}
![](mean_uuUUg_2l.pdf){width=50%}
![](mean_uUuUg_2l.pdf){width=50%}

## L2
![](mean_ggggg_l2.pdf){width=50%}
![](mean_ggqbqg_l2.pdf){width=50%}
![](mean_qbgqgg_l2.pdf){width=50%}
![](mean_qbqggg_l2.pdf){width=50%}
![](mean_uDdUg_l2.pdf){width=50%}
![](mean_udUDg_l2.pdf){width=50%}
![](mean_ugUdD_l2.pdf){width=50%}
![](mean_uUdDg_l2.pdf){width=50%}
![](mean_ugUuU_l2.pdf){width=50%}
![](mean_uuUUg_l2.pdf){width=50%}
![](mean_uUuUg_l2.pdf){width=50%}

## 1L
![](mean_ggggg_1l.pdf){width=50%}
![](mean_ggqbqg_1l.pdf){width=50%}
![](mean_qbgqgg_1l.pdf){width=50%}
![](mean_qbqggg_1l.pdf){width=50%}
![](mean_uDdUg_1l.pdf){width=50%}
![](mean_udUDg_1l.pdf){width=50%}
![](mean_ugUdD_1l.pdf){width=50%}
![](mean_uUdDg_1l.pdf){width=50%}
![](mean_ugUuU_1l.pdf){width=50%}
![](mean_uuUUg_1l.pdf){width=50%}
![](mean_uUuUg_1l.pdf){width=50%}

# All 2L
## 5g
### ggggg
![](collinear_ggggg_0.pdf){width=50%}
![](collinear_ggggg_1.pdf){width=50%}
![](collinear_ggggg_2.pdf){width=50%}
![](collinear_ggggg_3.pdf){width=50%}
![](collinear_ggggg_4.pdf){width=50%}
![](collinear_ggggg_5.pdf){width=50%}
![](collinear_ggggg_6.pdf){width=50%}
![](collinear_ggggg_7.pdf){width=50%}
![](collinear_ggggg_8.pdf){width=50%}
![](collinear_ggggg_9.pdf){width=50%}

## 2q3g
### ggqbqg
![](collinear_ggqbqg_0.pdf){width=50%}
![](collinear_ggqbqg_1.pdf){width=50%}
![](collinear_ggqbqg_2.pdf){width=50%}
![](collinear_ggqbqg_3.pdf){width=50%}
![](collinear_ggqbqg_4.pdf){width=50%}
![](collinear_ggqbqg_5.pdf){width=50%}
![](collinear_ggqbqg_6.pdf){width=50%}
![](collinear_ggqbqg_7.pdf){width=50%}
![](collinear_ggqbqg_8.pdf){width=50%}
![](collinear_ggqbqg_9.pdf){width=50%}

### qbgqgg
![](collinear_qbgqgg_0.pdf){width=50%}
![](collinear_qbgqgg_1.pdf){width=50%}
![](collinear_qbgqgg_2.pdf){width=50%}
![](collinear_qbgqgg_3.pdf){width=50%}
![](collinear_qbgqgg_4.pdf){width=50%}
![](collinear_qbgqgg_5.pdf){width=50%}
![](collinear_qbgqgg_6.pdf){width=50%}
![](collinear_qbgqgg_7.pdf){width=50%}
![](collinear_qbgqgg_8.pdf){width=50%}
![](collinear_qbgqgg_9.pdf){width=50%}

### qbqggg
![](collinear_qbqggg_0.pdf){width=50%}
![](collinear_qbqggg_1.pdf){width=50%}
![](collinear_qbqggg_2.pdf){width=50%}
![](collinear_qbqggg_3.pdf){width=50%}
![](collinear_qbqggg_4.pdf){width=50%}
![](collinear_qbqggg_5.pdf){width=50%}
![](collinear_qbqggg_6.pdf){width=50%}
![](collinear_qbqggg_7.pdf){width=50%}
![](collinear_qbqggg_8.pdf){width=50%}
![](collinear_qbqggg_9.pdf){width=50%}

## 2q2Q1g
### uDdUg
![](collinear_uDdUg_0.pdf){width=50%}
![](collinear_uDdUg_1.pdf){width=50%}
![](collinear_uDdUg_2.pdf){width=50%}
![](collinear_uDdUg_3.pdf){width=50%}
![](collinear_uDdUg_4.pdf){width=50%}
![](collinear_uDdUg_5.pdf){width=50%}
![](collinear_uDdUg_6.pdf){width=50%}
![](collinear_uDdUg_7.pdf){width=50%}
![](collinear_uDdUg_8.pdf){width=50%}
![](collinear_uDdUg_9.pdf){width=50%}

### udUDg
![](collinear_udUDg_0.pdf){width=50%}
![](collinear_udUDg_1.pdf){width=50%}
![](collinear_udUDg_2.pdf){width=50%}
![](collinear_udUDg_3.pdf){width=50%}
![](collinear_udUDg_4.pdf){width=50%}
![](collinear_udUDg_5.pdf){width=50%}
![](collinear_udUDg_6.pdf){width=50%}
![](collinear_udUDg_7.pdf){width=50%}
![](collinear_udUDg_8.pdf){width=50%}
![](collinear_udUDg_9.pdf){width=50%}

### ugUdD
![](collinear_ugUdD_0.pdf){width=50%}
![](collinear_ugUdD_1.pdf){width=50%}
![](collinear_ugUdD_2.pdf){width=50%}
![](collinear_ugUdD_3.pdf){width=50%}
![](collinear_ugUdD_4.pdf){width=50%}
![](collinear_ugUdD_5.pdf){width=50%}
![](collinear_ugUdD_6.pdf){width=50%}
![](collinear_ugUdD_7.pdf){width=50%}
![](collinear_ugUdD_8.pdf){width=50%}
![](collinear_ugUdD_9.pdf){width=50%}

### uUdDg
![](collinear_uUdDg_0.pdf){width=50%}
![](collinear_uUdDg_1.pdf){width=50%}
![](collinear_uUdDg_2.pdf){width=50%}
![](collinear_uUdDg_3.pdf){width=50%}
![](collinear_uUdDg_4.pdf){width=50%}
![](collinear_uUdDg_5.pdf){width=50%}
![](collinear_uUdDg_6.pdf){width=50%}
![](collinear_uUdDg_7.pdf){width=50%}
![](collinear_uUdDg_8.pdf){width=50%}
![](collinear_uUdDg_9.pdf){width=50%}

## 4q1g
### ugUuU
![](collinear_ugUuU_0.pdf){width=50%}
![](collinear_ugUuU_1.pdf){width=50%}
![](collinear_ugUuU_2.pdf){width=50%}
![](collinear_ugUuU_3.pdf){width=50%}
![](collinear_ugUuU_4.pdf){width=50%}
![](collinear_ugUuU_5.pdf){width=50%}
![](collinear_ugUuU_6.pdf){width=50%}
![](collinear_ugUuU_7.pdf){width=50%}
![](collinear_ugUuU_8.pdf){width=50%}
![](collinear_ugUuU_9.pdf){width=50%}

### uUuUg
![](collinear_uUuUg_0.pdf){width=50%}
![](collinear_uUuUg_1.pdf){width=50%}
![](collinear_uUuUg_2.pdf){width=50%}
![](collinear_uUuUg_3.pdf){width=50%}
![](collinear_uUuUg_4.pdf){width=50%}
![](collinear_uUuUg_5.pdf){width=50%}
![](collinear_uUuUg_6.pdf){width=50%}
![](collinear_uUuUg_7.pdf){width=50%}
![](collinear_uUuUg_8.pdf){width=50%}
![](collinear_uUuUg_9.pdf){width=50%}

### uuUUg
![](collinear_uuUUg_0.pdf){width=50%}
![](collinear_uuUUg_1.pdf){width=50%}
![](collinear_uuUUg_2.pdf){width=50%}
![](collinear_uuUUg_3.pdf){width=50%}
![](collinear_uuUUg_4.pdf){width=50%}
![](collinear_uuUUg_5.pdf){width=50%}
![](collinear_uuUUg_6.pdf){width=50%}
![](collinear_uuUUg_7.pdf){width=50%}
![](collinear_uuUUg_8.pdf){width=50%}
![](collinear_uuUUg_9.pdf){width=50%}
