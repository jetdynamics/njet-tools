# collinear-phase-space

| script                 | description                                                                                        |
| ---------------------- | -------------------------------------------------------------------------------------------------- |
| `phase_space.py`       | generates random n-point Lorentz-invariant phase space (LIPS)                                      |
| `phase_space_numpy.py` | generates n-point IR-parameterised phase space (IRPS) from random LIPS for two collinear particles |
| `phase_space_phys.py`  | generates IRPS from a collider-frame LIPS                                                          |
| `gen_points.py`        | generates a slice of IRPS driving into IR limit                                                    |
| `calc_csv.cpp`         | calculate amplitude over the points in the csv (given as CLI argument)                             |
| `calc_dir.cpp`         | calculate amplitude (f64) generating points on the fly (using cps.hpp)                             |
| `calc_otf.cpp`         | calculate amplitude (rescue system) generating points on the fly (using cps.hpp)                   |
| `calc_otf`             | `<channel #> <rseed>`                                                                              |
| `cps.hpp`              | generates IRPS from NJet LIPS                                                                      |
| `cps_test.cpp`         | test Lorentz invariance of a point generated using cps.hpp                                         |
| `plot_test.py`         | plots test results                                                                                 |
| `plot.py`              | plots 5g results (test script)                                                                     |
| `plot_paper.py`        | plots results that will be collected by `build_notes.sh`                                           |
| `build_notes.py`       | collects plots from `plot_paper.py` into `notes.pdf`                                               |
| `run.sh`               | `<rseed start> <rseed number>` run `calc_otf` in parallel over channels and rseeds                 |
| `collinear.py`         | plots results formatted for the 3j paper                                                           |
