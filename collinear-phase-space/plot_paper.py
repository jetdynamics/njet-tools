#!/usr/bin/env python3

import numpy, matplotlib.pyplot, matplotlib.cm

import reader_numpy

F64 = r"$\texttt{f64}$"
F128 = r"$\texttt{f128}$"


PREC = {
    "f64f64": f"{F64}/{F64}",
    "f128f64": f"{F128}/{F64}",
    "f128f128": f"{F128}/{F128}",
}

# key is 0->5, value is 2->3
CHAN = {
    "ggggg": r"$gg \to ggg$",
    "ggqbqg": r"$gg \to \bar{q}qg$",
    "qbgqgg": r"$qg \to qgg$",
    "qbqggg": r"$q\bar{q} \to ggg$",
    "uDdUg": r"$\bar{q}Q \to Q\bar{q}g$",
    "udUDg": r"$\bar{q}\bar{Q} \to \bar{q}\bar{Q}g$",
    "ugUdD": r"$\bar{q}g \to \bar{q}Q\bar{Q}$",
    "uUdDg": r"$\bar{q}q \to Q\bar{Q}g$",
    "ugUuU": r"$\bar{q}g \to \bar{q}q\bar{q}$",
    "uUuUg": r"$\bar{q}q \to q\bar{q}g$",
    "uuUUg": r"$\bar{q}\bar{q} \to \bar{q}\bar{q}g$",
}

C0LX0L_V = 2
C0LX0L_E = C0LX0L_V + 1
C1LX0L_V = C0LX0L_V + 2
C1LX0L_E = C1LX0L_V + 1
C1LX1L_V = C0LX0L_V + 4
C1LX1L_E = C1LX1L_V + 1
C2LX0L_V = C0LX0L_V + 6
C2LX0L_E = C2LX0L_V + 1


def plot(x, ys, es, labels, ylabel, title, name, logy=True):
    print(f"Plotting {name}")

    fig, ax = matplotlib.pyplot.subplots()

    for y, e, label in zip(ys, es, labels):
        ax.fill_between(
            x,
            y - e,
            y + e,
            alpha=0.2,
        )
        ax.plot(x, y, label=label)

    ax.set_xscale("log", base=10)
    if logy:
        ax.set_yscale("log", base=10)

    ax.set_title(title)
    ax.set_xlabel(r"$s_{ij}/s_{12}$")
    ax.set_ylabel(ylabel)
    if any(labels):
        ax.legend(frameon=False)

    fig.savefig(f"{name}.pdf", bbox_inches="tight")

    matplotlib.pyplot.close(fig)


def plot_multiple(x, yss, ess, labelss, ylabel, title, name, logy=True):
    print(f"Plotting {name}")

    fig, ax = matplotlib.pyplot.subplots()

    colours = matplotlib.cm.tab10(range(len(labelss[1])))
    styles = ("solid", "dashed", "dotted")
    lines = []

    for j, (ys, es, s) in enumerate(zip(yss, ess, styles)):
        for i, (y, e, c) in enumerate(zip(ys, es, colours)):
            ax.fill_between(
                x,
                y - e,
                y + e,
                alpha=0.2,
                color=c,
            )
            (l,) = ax.plot(
                x,
                y,
                label=labelss[1][i] if j == 0 else None,
                color=c,
                linestyle=s,
            )
            if i == 0:
                lines.append(l)

    ax.set_xscale("log", base=10)
    if logy:
        ax.set_yscale("log", base=10)

    ax.set_title(title)
    ax.set_xlabel(r"$s_{34}/s_{12}$")
    ax.set_ylabel(ylabel)
    if any(labels):
        l0 = ax.legend(frameon=False)
        ax.legend(
            handles=lines,
            labels=["1L", "L2", "2L"],
            loc="center right",
            frameon=False,
        )
        ax.add_artist(l0)

    fig.savefig(f"{name}.pdf", bbox_inches="tight")

    matplotlib.pyplot.close(fig)


def proc(ds, index=C2LX0L_V):
    # ds[channel, rseed, precision, phase space point, results]
    val = ds[:, :, :, :, index] / ds[:, :, :, :, C0LX0L_V]
    err = val * ds[:, :, :, :, index + 1]  # assume L errors dominate
    return val, err


if __name__ == "__main__":
    use_cache = True

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    ylabel = (
        r"$\mathcal{A}^{(i)} \cdot \mathcal{A}^{(j)}/\left|\mathcal{A}^{(0)}\right|^2$"
    )
    labels = numpy.array([*PREC.values()])
    chs = numpy.array([*CHAN.keys()])
    ch_ns = numpy.array([*CHAN.values()])

    ds = reader_numpy.read_proc("data_paper", "res", use_cache)
    ys = numpy.loadtxt("data_paper/sijs.csv")

    v1ls, e1ls = proc(ds, C1LX0L_V)
    vl2s, el2s = proc(ds, C1LX1L_V)
    v2ls, e2ls = proc(ds, C2LX0L_V)

    for channel in range(11):
        for rseed in range(10):
            plot(
                ys,
                v2ls[channel, rseed],
                e2ls[channel, rseed],
                labels,
                ylabel,
                r"$i||j$" + f" {channel}:{ch_ns[channel]} {rseed}",
                f"collinear_{chs[channel]}_{rseed}",
            )

    for channel in range(11):
        v2l = numpy.mean(v2ls[channel], axis=0)
        e2l = numpy.mean(e2ls[channel], axis=0)
        vl2 = numpy.mean(vl2s[channel], axis=0)
        el2 = numpy.mean(el2s[channel], axis=0)
        v1l = numpy.mean(v1ls[channel], axis=0)
        e1l = numpy.mean(e1ls[channel], axis=0)

        plot(
            ys,
            v2l,
            e2l,
            labels,
            ylabel,
            r"$i||j$" + f" {channel}:{ch_ns[channel]} mean 2L",
            f"mean_{chs[channel]}_2l",
        )

        plot(
            ys,
            vl2,
            el2,
            labels,
            ylabel,
            r"$i||j$" + f" {channel}:{ch_ns[channel]} mean L2",
            f"mean_{chs[channel]}_l2",
        )

        plot(
            ys,
            v1l,
            e1l,
            labels,
            ylabel,
            r"$i||j$" + f" {channel}:{ch_ns[channel]} mean 1L",
            f"mean_{chs[channel]}_1l",
        )

        plot_multiple(
            ys,
            [v1l, vl2, v2l],
            [e1l, el2, e2l],
            [["1L", "L2", "2L"], labels],
            ylabel,
            r"$i||j$" + f" {channel}:{ch_ns[channel]} mean",
            f"mean_{chs[channel]}",
        )

    # channel = 8
    # rseed = 0
    # plot(
    #     ys,
    #     vs[channel, rseed],
    #     es[channel, rseed],
    #     labels,
    #     ylabel,
    #     r"$i||j$" + f" {channel}:{ch_ns[channel]} mean",
    #     f"mean_{chs[channel]}",
    # )
