#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
LOOP=$2
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=$IN.tmp

echo -e "#-\n#: WorkSpace 400M\n#: Threads 60\nS u,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{?(f\d+ )->( .*?)\}?,\s*|L \1=\2;\n|gs;
        s|\{?(f\d+ )->( .*?)\}|L \1=\2;\n|gs;
        " \
    ${IN} >${TMP}

ysr=$(grep -oP "y\d+" $TMP)
declare -A ys
for y in $ysr; do
    ys[$y]=1
done
for y in ${!ys[@]}; do
    echo -e "S $y;" >>$FRM
done

echo "ExtraSymbols,array,v;" >>$FRM
cat ${TMP} >>${FRM}
rm -f ${TMP}
echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

fsr=$(grep -oP "f\d+ =" $FRM)
declare -a fs
for f in ${fsr[@]}; do
    fs+=(${f%*=})
done
N=${#fs[@]}
echo $N >${BASE}.count

h="L H="
i=1
for f in ${fs[@]}; do
    h+="+u^$((i++))*$f"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${fs[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

i=0
for f in ${fs[@]}; do
    echo "#write <$C> \"f${LOOP}[${i}] = %E;\", ${f}a" >>$FRM
    i=$((i + 1))
done

echo ".end" >>$FRM
