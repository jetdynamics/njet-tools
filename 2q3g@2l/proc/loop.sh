#!/usr/bin/env bash

MAKE=build/Makefile

echo -e "# Automatically generated Makefile from $0 #\n\n.PHONY: all one two yms1 fms1 yms2 fms2 Fms yms fms prepFORM\nall: Fms one two\none: yms1 fms1\ntwo: yms2 fms2\nyms: yms1 yms2\nfms: fms1 fms2\n" >$MAKE

declare -a prepFORM
declare -a yms1
declare -a fms1
declare -a yms2
declare -a fms2

for contrib in p1LNfp0Ncp1 p1LNfp1Ncp0; do
    for dir in build/$contrib/h*; do
        name="${contrib:6:1}${contrib:10:1}"
        dir=${dir#build/}

        echo -e "
$dir/ym.frm: $dir/ym.m
\t../../proc/conv-y.sh $<" \
            >>$MAKE

        echo -e "
$dir/fm.frm: $dir/fm.m
\t../../proc/conv-f.sh $< ${name}" \
            >>$MAKE

        echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
            >>$MAKE

        echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
            >>$MAKE

        echo -e "
$dir/ym.cpp: $dir/ym.c
\t../../proc/pp-form.sh $<" \
            >>$MAKE

        echo -e "
$dir/fm.cpp: $dir/fm.c
\t../../proc/pp-form.sh $<" \
            >>$MAKE

        yms1+=("$dir/ym.cpp")
        fms1+=("$dir/fm.cpp")
        prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
    done
done

for contrib in p2LNfp0Ncp2 p2LNfp1Ncp1 p2LNfp2Ncp0; do
    for dir in build/$contrib/h*; do
        name="${contrib:6:1}${contrib:10:1}"
        dir=${dir#build/}

        echo -e "
$dir/ym.frm: $dir/ym.m
\t../../proc/conv-y.sh $<" \
            >>$MAKE

        echo -e "
$dir/fm.frm: $dir/fm.m
\t../../proc/conv-f.sh $< ${name}" \
            >>$MAKE

        echo -e "
$dir/ym.c: $dir/ym.frm
\ttform $< >${dir}/ym.log 2>&1" \
            >>$MAKE

        echo -e "
$dir/fm.c: $dir/fm.frm
\ttform $< >${dir}/fm.log 2>&1" \
            >>$MAKE

        echo -e "
$dir/ym.cpp: $dir/ym.c
\t../../proc/pp-form.sh $<" \
            >>$MAKE

        echo -e "
$dir/fm.cpp: $dir/fm.c
\t../../proc/pp-form.sh $<" \
            >>$MAKE

        yms2+=("$dir/ym.cpp")
        fms2+=("$dir/fm.cpp")
        prepFORM+=("$dir/ym.frm" "$dir/fm.frm")
    done
done

echo -e "
Fm.frm: Fm.m
\t../../proc/conv-F.sh $<" \
    >>$MAKE

echo -e "
Fm.c: Fm.frm
\ttform $< >Fm.log 2>&1" \
    >>$MAKE

echo -e "
Fm.cpp: Fm.c
\t../../proc/pp-form.sh $<" \
    >>$MAKE

echo -e "
Fm1L.frm: Fm1L.m
\t../../proc/conv-F.sh $<" \
    >>$MAKE

echo -e "
Fm1L.c: Fm1L.frm
\ttform $< >Fm1L.log 2>&1" \
    >>$MAKE

echo -e "
Fm1L.cpp: Fm1L.c
\t../../proc/pp-form.sh $<" \
    >>$MAKE

echo -e "\nyms1: ${yms1[@]}" >>$MAKE
echo -e "\nfms1: ${fms1[@]}" >>$MAKE

echo -e "\nyms2: ${yms2[@]}" >>$MAKE
echo -e "\nfms2: ${fms2[@]}" >>$MAKE

echo -e "\nFms: Fm.cpp Fm1L.cpp" >>$MAKE

echo -e "\nprepFORM: Fm1L.frm Fm.frm ${prepFORM[@]}" >>$MAKE
