#!/usr/bin/env python3

import json
from dataclasses import dataclass, asdict
from copy import deepcopy
from pathlib import Path
from string import Template

import jinja2


@dataclass
class Loop:
    id: str
    parity_odd: bool
    order: int
    Nf: int
    Nc: int

    def __init__(self, n):
        self.name = "".join(["p", n[14:16], n[17:21], n[22:26]])
        self.order = int(n[14])
        self.Nf = int(n[20])
        self.Nc = int(n[25])
        self.id = f"{self.Nf}{self.Nc}"
        loops_double = ("11", "02")
        self.parity_odd = self.id in loops_double


def hel_symb2char(hel_symb):
    return hel_symb.translate(str.maketrans("+-", "pm"))


@dataclass
class Hel:
    symbs: str
    chars: str
    dec: int
    uhv: bool

    def __init__(self, symbs):
        self.symbs = symbs
        self.chars = hel_symb2char(self.symbs)
        m = len(self.symbs)
        t = 2 ** m - 1
        self.dec = sum([2 ** i for i, s in enumerate(self.symbs) if s == "+"])
        self.uhv = self.dec in [t] + [t - 2 ** i for i in range(m)]


def mkdir_hels(build, loops, hels):
    for loop in loops:
        print(len(hels), "helicities for", loop)
        for hel in hels:
            q = build / loop / f"h{hel}"
            print(f"mkdir {q}")
            q.mkdir(parents=True, exist_ok=True)


def mma_fmt(hels):
    return '"' + '","'.join(hels) + '"'


def mkdir_v(dir_path):
    print(f"mkdir {dir_path}")
    dir_path.mkdir(parents=True, exist_ok=True)
    return dir_path


class Paths:
    def __init__(self, start="."):
        self.root = Path(start)
        self.proc = self.root / "proc"
        self.export = mkdir_v(self.root / "export")
        self.templates = self.root / "templates"
        self.phases = self.root / "phases"
        self.top = (
            self.proc,
            self.export,
            self.templates,
            self.phases,
        )


def _sort_loops(num, nc):
    # (3 since NNLO) * (# loops) + (Nc power)
    return 3 * num + nc


def sort_loops(loop):
    # (3 since NNLO) * (# loops) + (Nc power)
    return _sort_loops(loop.order, loop.Nc)


def fill(subs, template, output_path):
    print(f"Generating {output_path}")
    output = template.render(subs)
    print(f"Writing {output_path}")
    output_path.write_text(output)


def read_and_fill(env, subs, template_file, output_path):
    template = env.get_template(template_file)
    fill(subs, template, output_path)


if __name__ == "__main__":
    p = Paths()

    mma_template = Template((p.proc / "proc.wl").read_text())

    # TODO need to permute these for channels
    colour_orderings = (
        (1, 2, 3, 4, 0),
        (1, 3, 4, 2, 0),
        (1, 4, 2, 3, 0),
        (1, 2, 4, 3, 0),
        (1, 4, 3, 2, 0),
        (1, 3, 2, 4, 0),
    )

    subs = {
        "amp": "2q3g",
        "year": 2021,
        "partials": 6,
        "legs": 5,
        "ords": colour_orderings,
        "channels": [],
    }

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(p.templates))

    template_hel_hdr = env.get_template("hel-each.h")
    template_chan_hdr = env.get_template("chan-each.h")

    ch = {}
    loops = []

    for pchannel in filter(
        lambda x: all([x.is_dir(), x not in p.top, x.name[0] != "."]), p.root.iterdir()
    ):
        channel = pchannel.name
        subs["channels"].append(channel)

        exp_chan = mkdir_v(p.export / channel)

        lcl_prc = mkdir_v(pchannel / "proc")
        lcl_bld = mkdir_v(pchannel / "build")

        hels = []
        for file in (pchannel / "src").glob("partials_*.m"):
            name = file.name

            hel = Hel(name[31:36])
            if hel.dec not in [h.dec for h in hels]:
                hels.append(hel)

            if "loops" not in subs:
                loop = Loop(name)
                if loop.id not in [l.id for l in loops]:
                    loops.append(loop)

        # global
        if "loops" not in subs:
            subs["loops"] = sorted(loops, key=sort_loops)

        mma_file = lcl_prc / "proc.wl"
        print(f"write {mma_file}")
        mma_body = mma_template.substitute(
            {
                "channel": channel,
                "mhvs": mma_fmt([hel.symbs for hel in hels if not hel.uhv]),
                "uhvs": mma_fmt([hel.symbs for hel in hels if hel.uhv]),
            }
        )
        mma_file.write_text(mma_body)

        mkdir_hels(
            lcl_bld,
            [loop.name for loop in loops if loop.order == 1],
            [hel.symbs for hel in hels],
        )
        mkdir_hels(
            lcl_bld,
            [loop.name for loop in loops if loop.order == 2],
            [hel.symbs for hel in hels if not hel.uhv],
        )

        subs_chan = deepcopy(subs)
        subs_chan["chan"] = channel
        subs_chan["hels"] = sorted(hels, key=lambda h: h.dec)

        read_and_fill(env, subs_chan, "Makefile.channel.am", exp_chan / "Makefile.am")

        fill(subs_chan, template_chan_hdr, exp_chan / f"{subs['amp']}-{channel}.h")

        for hel in hels:
            subs_chan["h_char"] = hel.chars
            subs_chan["mhv"] = not hel.uhv
            output_hel_hdr = template_hel_hdr.render(subs_chan)
            (exp_chan / f"{hel.chars}.h").write_text(output_hel_hdr)

        ch[channel] = hels

    read_and_fill(env, subs, "Makefile.root.am", p.export / "Makefile.am")

    com = mkdir_v(p.export / "common")
    read_and_fill(env, subs, "Makefile.common.am", com / "Makefile.am")
    read_and_fill(env, subs, "hel-base.h", com / "hel.h")
    read_and_fill(env, subs, "hel-base.cpp", com / "hel.cpp")
    read_and_fill(env, subs, "chan-base.h", com / f"chan.h")
    read_and_fill(env, subs, "chan-base.cpp", com / f"chan.cpp")

    subs["loops"] = [asdict(l) for l in subs["loops"]]
    (p.root / "subs.json").write_text(json.dumps(subs))

    for channel, hels in ch.items():
        print(
            f"\n    $(top_builddir)/finrem/{subs['amp']}/{channel}/libnjet3an{subs['amp']}{channel}.la \\"
        )
        l = len(hels) - 1
        for i, hel in enumerate(hels):
            delimiter = "" if i == l else "\\"
            print(
                f"    $(top_builddir)/finrem/{subs['amp']}/{channel}/libnjet3an{subs['amp']}{channel}{hel.chars}.la {delimiter}"
            )

    for channel, hels in ch.items():
        print(f"\n{subs['amp']}{channel} \\")
        l = len(hels) - 1
        for i, hel in enumerate(hels):
            delimiter = "" if i == l else "\\"
            print(f"{subs['amp']}{channel}{hel.chars} {delimiter}")
        print()
