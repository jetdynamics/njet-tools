(* ::Package:: *)

SetDirectory["/scratch/rmoodie/njet-tools/2q3g@2l/${channel}"];
in="src/";
outpre="build/";
mhvs={
${mhvs}
};
uhvs={
${uhvs}
};
allHels=Join[mhvs,uhvs];
PF=6;


nsm[src_,ord_,par_]:=Module[{sm1,e1,v1,u1,r1,p1,n1,ncols,nrows,nnz,s1},
sm1=src[[3,ord,par]];
e1=sm1[[2]];
u1=DeleteDuplicates[e1/.Rule[a_,b_]:>Abs[b]];
p1=u1[[#]]->#-1&/@Range[Length[u1]];
p1=Join[p1,p1/.Rule[a_,b_]:>Rule[-a,b]];
n1=e1/.Rule[m[i_,j_],b_]:>{"i"->j-1, "j"->i-1, "v"->Abs[b/.p1], "s"->Positive[b]};
s1=#/.{
	a_:>{"n"->Numerator[a], "d"->Denominator[a]}
	}&/@u1;
ncols=sm1[[1,1]];
nrows=sm1[[1,2]];
nnz=Table[Length[Select[e1,#[[1,1]]==i&]],{i,ncols}];
{"nrows"->nrows, "ncols"->ncols,"nnz"->nnz,"vals"->s1,"entries"->n1}
];
fv[fin_]:=#[[1]]-1&/@#&/@fin[[1]];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=#-1&/@fin[[2]];
export[fins_,hels_,out_,odd_] := ParallelDo[
    fin=fins[[i]];
    hel="h"<>hels[[i]];
    Fvi=Fv[fin];
    fvi=fv[fin];
    fmsi=fms[fin];
    fmsl=Length[fmsi];
    ymsi=yms[fin];
    ymsl=Length[ymsi];
    o=outpre<>out<>"/";
    Export[o<>hel<>"/Fv.json",Fvi];
    Export[o<>hel<>"/fv.json",fvi];
    Export[o<>hel<>"/fm.m",fmsi];
    Export[o<>hel<>"/f_size.txt", fmsl];
    Export[o<>hel<>"/ym.m",ymsi];
    Export[o<>hel<>"/y_size.txt", ymsl];
    Export[o<>hel<>"/e"<>ToString[#]<>".json",nsm[fin,#,1]]&/@Range[PF];
    If[odd,Export[o<>hel<>"/o"<>ToString[#]<>".json",nsm[fin,#,2]]&/@Range[PF]];
    ,{i,Length[hels]}
];
exportF[Fmons_]:=Module[{Fml,Fms},
	Fml=Length[Fmons];
	Fms=SF[#-1]->Fmons[[#]]&/@Range[Fml];
	Export[outpre<>"Fm.m",Fms];
	Export[outpre<>"F_size.txt", Fml];
];
dedup[a_]:=Select[Variables[a],MatchQ[#,_F]&];


Fmons=Get[in<>"AllPfuncMonomials.m"];


exportF[Fmons];
Ffns=dedup[Fmons];
Export[outpre<>"specFns.json",List@@@Ffns];


p1LNfp0Ncp1=Get[in<>"partials_2q3g_1L_Nfp0_Ncp1_tHV_"<>#<>".m"]&/@allHels;
p1LNfp1Ncp0=Get[in<>"partials_2q3g_1L_Nfp1_Ncp0_tHV_"<>#<>".m"]&/@allHels;


p2LNfp0Ncp2m=Get[in<>"partials_2q3g_2L_Nfp0_Ncp2_tHV_"<>#<>".m"]&/@mhvs;
p2LNfp1Ncp1m=Get[in<>"partials_2q3g_2L_Nfp1_Ncp1_tHV_"<>#<>".m"]&/@mhvs;
p2LNfp2Ncp0m=Get[in<>"partials_2q3g_2L_Nfp2_Ncp0_tHV_"<>#<>".m"]&/@mhvs;


Fmoni1L=Sort[DeleteDuplicates[Flatten[Join[p1LNfp0Ncp1,p1LNfp1Ncp0][[;;,2]]]]];
Ffns1l=dedup[Fmons[[#]]&/@Fmoni1L];
Export[outpre<>"specFns1L.json",List@@@Ffns1l];
Fmons1L=Table[SF[i-1]->If[MemberQ[Fmoni1L,i],Fmons[[i]],0],{i,Max[Fmoni1L]}];
Export[outpre<>"Fm1L.m",Fmons1L];
Export[outpre<>"F_size1L.txt", Fmons1L//Length];


export[p1LNfp0Ncp1, allHels, "p1LNfp0Ncp1", False];
export[p1LNfp1Ncp0, allHels, "p1LNfp1Ncp0", False];
export[p2LNfp2Ncp0m, mhvs, "p2LNfp2Ncp0", False];
export[p2LNfp1Ncp1m, mhvs, "p2LNfp1Ncp1", True];
export[p2LNfp0Ncp2m, mhvs, "p2LNfp0Ncp2", True];



