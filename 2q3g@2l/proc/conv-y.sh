#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp

echo -e "#-\n#: WorkSpace 400M\n#: Threads 60\nS a,x1,x2,x3,x4,x5;" >$FRM
echo "ExtraSymbols,array,u;" >>$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{?(y\d+ )->( .*?)\}?,\s*|L \1=\2;\n|gs;
        s|\{?(y\d+ )->( .*?)\}|L \1=\2;\n|gs;
        " \
    ${IN} >>${FRM}

echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

ysr=`grep -oP "y\d+ =" $FRM`
declare -a ys
for y in ${ysr[@]}; do
    ys+=(${y%*=})
done
N=${#ys[@]}
echo $N >${BASE}.count

h="L H="
i=1
for y in ${ys[@]}; do
    h+="+a^$((i++))*$y"
done

echo "$h;" >>$FRM

echo -e "B a;\n.sort\n#optimize H\nB a;\n.sort" >>$FRM

i=1
for y in ${ys[@]}; do
    echo "L ${y}a = H[a^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

i=0
for y in ${ys[@]}; do
    echo "#write <$C> \"y[${i}] = %E;\", ${y}a" >>$FRM
    i=$((i+1))
done

echo ".end" >>$FRM
