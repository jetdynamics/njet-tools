#!/usr/bin/env python3


if __name__ == "__main__":
    ords = (
        (0, 1, 2),
        (1, 2, 0),
        (2, 0, 1),
        (0, 2, 1),
        (2, 1, 0),
        (1, 0, 2),
    )

    chans = (
        "ggqbqg",
        "qbgqgg",
        "qbqggg",
    )

    base = "qbqggg"

    base = base.replace("qb", "b")
    for chan in chans:
        print(chan)
        chan = chan.replace("qb", "b")
        start = [None] * 5
        j = 2
        for i, f in enumerate(chan):
            if f == "b":
                start[0] = i
            elif f == "q":
                start[1] = i
            else:
                start[j] = i
                j += 1
        gluons = [i for i, f in enumerate(chan) if f == "g"]
        for ord in ords:
            new = []
            o = 0
            for i, f in zip(start, base):
                if f in ("q", "b"):
                    new.append(i)
                else:
                    j = gluons[ord[o]]
                    o += 1
                    new.append(j)
            print("{" + ", ".join([str(n) for n in new]) + "},")
