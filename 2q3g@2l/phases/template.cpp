// ${hchars}
template <typename T, typename P>
std::complex<T> Amp${amp}_${chan}_a2l<T, P>::phase${hdec}() {
  const std::complex<T> expr{
      ${expr}
  };
  return -i_ * expr;
}
