(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/2q3g@2l/phases"];
<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_2q3g.m"


njetify[expr_]:=ToString[InputForm[expr//.{
	spA[a_,b_]->Sort[sA[a-1,b-1]],
	spB[a_,b_]->Sort[sB[a-1,b-1]],
	Power[a_,b_]/;b>1->pow[a,b],
	Power[a_,b_]/;b<-1->1/pow[a,-b],
	ex[1]->x1,
	ex[2]->x2,
	ex[3]->x3,
	ex[4]->x4,
	ex[5]->x5
	}/.{p[i_]->i-1}]];
treeX[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalytic]//FullSimplify;


MHV[qbqpos_List,heli_String]:=Module[{tmpheli,negpos,qnegpos,qpluspos,gnegpos},
  tmpheli = StringSplit[heli,""];
  negpos = Flatten[Position[tmpheli,"-"]];
  If[Length[negpos]=!=2, Return[$Failed]];
  qnegpos = Intersection[qbqpos,negpos];
  If[Length[qnegpos]=!=1, Return[$Failed], qnegpos = qnegpos[[1]]];
  qpluspos = DeleteCases[qbqpos,qnegpos][[1]];
  gnegpos = DeleteCases[negpos,qnegpos][[1]];
  Return[ spA[qnegpos,gnegpos]^3*spA[qpluspos,gnegpos]/(spA[1,2]*spA[2,3]*spA[3,4]*spA[4,5]*spA[5,1]) ]
];


UHV[qbqpos_List, heli_String] := Module[{tmpheli,qnegpos,qpluspos,ii,jj,kk},
  tmpheli = StringSplit[heli,""];
  qnegpos = Flatten[Position[tmpheli,"-"]];
  If[Length[qnegpos]=!=1, Return[$Failed],qnegpos=qnegpos[[1]]];
  If[!MemberQ[qbqpos,qnegpos], Return[$Failed]];
  qpluspos = DeleteCases[qbqpos,qnegpos][[1]];
  {ii,jj,kk} = DeleteCases[{1,2,3,4,5},qnegpos|qpluspos];
  Return[ (spA[qnegpos,qpluspos])/(spA[qpluspos, ii]*spA[ii, jj]*spA[jj, kk]*spA[kk, qpluspos]) ]  
];


phase[qbqpos_List, hel_String]:=If[Count[StringSplit[hel,""],"-"]===2,MHV[qbqpos,hel],UHV[qbqpos,hel]];


export[chan_,hels_,qbqpos_]:=Module[{res},
res = Table[
	Module[{s,x},
		s=phase[qbqpos,h];
		Print[s];
		x=treeX[s];
		{"hchars"->h,"expr"->njetify[s/x]}
	],
	{h,hels}
];
(*Print[res];*)
Export["phases_"<>chan<>".json",res];
];


hels = {"-++++","+-+++","-+-++","-++-+","-+++-","+--++","+-+-+","+-++-"};
export["qbqggg",hels,{1,2}];


hels = {"-++++","-+++-","-++-+","--+++","++-++","++-+-","++--+","+--++"};
export["qbgqgg",hels,{1,3}];


hels = {"+++-+","++-++","+++--","+-+-+","-++-+","++-+-","+--++","-+-++"};
export["ggqbqg",hels,{3,4}];


(*EOF*)
