#!/usr/bin/env bash

for f in Mcompare_*_NNLO_nf*_*.m; do
	perl -0777p -e " 
			s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
			s|\`\d+\.\d+||g^;
			s|\*\^|e|g;
			s|\\\\\n||g;
		" "$f" > "NJ${f#M}"
done
