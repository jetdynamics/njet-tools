(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


Table[
dataSplit[channel][nfp]=Get["Mcompare_"<>channel<>"_NNLO_nf"<>ToString[nfp]<>"_split.m"]//Chop[#,10^-8]&;
data[channel][nfp]["1L^2"]=dataSplit[channel][nfp]/.{flag["1L^2"]->1,flag["0Lx2L"]->0};
data[channel][nfp]["0Lx2L"]=dataSplit[channel][nfp]/.{flag["1L^2"]->0,flag["0Lx2L"]->1};
,
{channel,{"ggqbqg","qbgqgg","qbqggg"}},
{nfp,{0,1,2}}
];


Table[
Export["Mcompare_"<>channel<>"_NNLO_nf"<>ToString[nfp]<>"_1L^2.m",data[channel][nfp]["1L^2"]];
Export["Mcompare_"<>channel<>"_NNLO_nf"<>ToString[nfp]<>"_0Lx2L.m",data[channel][nfp]["0Lx2L"]];
,
{channel,{"ggqbqg","qbgqgg","qbqggg"}},
{nfp,{0,1,2}}
];
