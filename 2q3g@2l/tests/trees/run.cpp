#include <complex>
#include <iomanip>
#include <iostream>
#include <vector>

#include "finrem/2q3g/qbgqgg/2q3g-qbgqgg.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

template <typename T> void run() {
  std::cout << std::scientific << std::setprecision(16);

  const int legs{5};
  const int Nc{3};
  const int Nf{5};
  const std::vector<double> scales2{{0.}}; // no associated scales

  const std::vector<int> helicity{+1, +1, -1, -1, -1};

  std::cout << "\nchannel: qbgqgg\n\nhelicity: ";
  for (int h : helicity) {
    std::cout << (h == 1 ? '+' : '-');
  }
  std::cout << "\nNf = 0\n\n";

  Amp2q3g_qbgqgg_a2l<T, T> amp;

  amp.setNc(Nc);
  amp.setNf(Nf);
  amp.setMuR2(1.);

  PhaseSpace<T> ps(legs, 1, 1.);
  std::vector<MOM<T>> momenta{ps.getPSpoint()};
  refineM(momenta, momenta, scales2);

  amp.setMomenta(momenta);
  amp.initFinRem();
  amp.setSpecFuncs1L();

  amp.printMomenta();
  std::cout << '\n';

  amp.printSpinorBrackets();
  std::cout << '\n';

  amp.setHelicity(helicity);

  std::cout << "(* partials *):\n";
  for (int k{0}; k < 6; ++k) {
    std::cout << "A" << k << " = " << amp.A01p()[k].loop << '\n';
  }
}

int main() { run<double>(); }
