(* ::Package:: *)

xxx[nf_]:=AA+BB*nf
Solve[{xxx[0]==x0,xxx[1]==x1},{AA,BB}]


xxx[nf_]:=AA+BB*nf+CC*nf^2
Solve[{xxx[0]==x0,xxx[1]==x1,xxx[2]==x2},{AA,BB,CC}]
