#! /usr/bin/env bash

for f in *; do
	if [[ $f =~ ^[qbg]{6}$ ]]; then
		cd $f
		math -script proc/proc.wl &
		cd ..
	fi
done
