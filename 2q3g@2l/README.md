# 2q3g@2l

## TODO
* still need to fill in TODO marked parts by hand after generation
    * scale dependence: scale-dependence.wl
* the orderings need to be set manually (the placeholder values are not correct)
* new phases in `phases`, add manually
* Helicity reordering!

## Potential improvements: generation
* Instead of exporting two sparse matrix jsons from mma, export one "sparse matrix pair", since they share dimensions
* Instead of `[mma] .wl (export)-> .m (conv-[fy].sh)-> .frm`, do `.wl (export)-> .json (preform.py)-> .frm`
* I use `json` for intermediate data, ie passing between `mma`, `frm`, and `py`, since it is human readable, so easy to debug. It is not efficient though and if this becomes a problem, consider using HDF5 instead.

## Ideas for final format
* Encode sparse matrices in CSV-like files that are read at runtime
    * Format specification DEPRECATED
        * Metadata
            * Header: [nrows] [ncols] [num non-zero per col list]
            * Values: row: [numerator] [denominator]
        * Data (separate file for with "---" break)
            * Row: [i] [j] [sign] [index in values list]
    * Format specification NEW
        * Header: [nrows] [ncols] [num non-zero per col list]
        * Data rows: [i] [j] [numerator] [denominator]
    * Considerations
        * Advantages:
            * Easier to generate source
            * Would make source smaller
            * Would improve compile time
            * Separation of "content" from "code"
        * Disadvantages:
            * Likely to detriment runtime initialisation time, maybe worth testing
    * Review
        * I tested this in `sm_csv` branch. It was very slow to init SMs at runtime, with `-O0` at least. Could read concurrently.
    * Revisit
        * A binary file format would be faster to read than the above proposed text format
        * Note that # nnz = ncols
        * Perhaps:
            * HDF5
            * boost::serialization (serialise to bytes, like `python`'s `pickle`)
            * Write binary with cpp
            * Write binary with Python (see `cpp-tests/bytes`)
* Write `regex` to replace `A = A * B` with `A =* B` in FORM optimised output
    * Would decrease file size

## Usage
```shell
./proc/init.py
./exp_mma.sh
./loop_loop.sh
./proc/gen_src.py -a
cd export
find -name "*.cpp" -o -name "*.h" | parallel clang-format -i -style=file
```

## Checking
* `mypy` ~~is~~ was used to perform static typing analysis of `python` code.
* Links
    * <http://mypy-lang.org/>
    * <https://github.com/python/mypy>
    * <https://mypy.readthedocs.io/en/latest/index.html>
* Install
    ```shell
    pip install --user mypy
    ```

## Templates
* The `jinja2` template engine is used to generate source code
* Links
    * <https://jinja.palletsprojects.com/en/3.0.x/>
    * <https://github.com/pallets/jinja>
    * <https://jinja2docs.readthedocs.io/en/stable/index.html>
* Install
    ```shell
    pip install --user jinja2
    ```
