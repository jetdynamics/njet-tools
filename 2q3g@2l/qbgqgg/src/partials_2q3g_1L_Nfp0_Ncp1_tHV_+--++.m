(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[4], f[2], f[6], f[5], f[1]}, {f[8], f[10], f[11], f[17], f[18], 
   f[29], f[9], f[12], f[42], f[15], f[1], f[38], f[14], f[6], f[37], f[7], 
   f[13]}, {f[19], f[2], f[7], f[13], f[20], f[21], f[18], f[24], f[44], 
   f[29], f[38], f[6], f[5], f[37], f[1], f[22], f[23], f[17], f[25], f[16]}, 
  {f[26], f[18], f[27], f[38], f[5], f[16], f[1], f[37], f[7], f[13], f[29], 
   f[6], f[17]}, {f[30], f[31], f[2], f[33], f[32], f[34], f[7], f[13], 
   f[16], f[22], f[37], f[36], f[35], f[5], f[29], f[6], f[1], f[28]}, 
  {f[41], f[2], f[39], f[7], f[13], f[18], f[29], f[6], f[17], f[40], f[5], 
   f[45], f[44], f[1], f[43], f[46], f[42], f[38], f[37]}}, 
 {{1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 
   24, 25, 26, 27, 28, 29, 30, 31}, {1, 34, 35, 36, 37, 38, 4, 39, 40, 11, 
   41, 42, 13, 15, 43, 44, 45, 46, 47, 48, 50, 51, 26, 27, 52, 30, 53, 54}, 
  {1, 11, 15, 16, 20, 55, 56, 57, 58, 60, 61, 62, 63, 64, 43, 47, 65, 66, 67, 
   25, 68, 51, 70, 26, 27, 30, 31, 71, 53, 72}, 
  {1, 2, 3, 4, 5, 11, 12, 13, 15, 60, 74, 75, 61, 64, 43, 76, 46, 47, 67, 77, 
   51, 70, 26, 27, 28, 30, 71, 53}, {1, 34, 35, 4, 39, 11, 41, 13, 15, 16, 
   78, 18, 20, 55, 56, 79, 80, 57, 58, 25, 81, 68, 26, 27, 52, 30, 31, 72}, 
  {1, 36, 38, 7, 82, 10, 11, 42, 14, 15, 16, 83, 19, 20, 43, 45, 84, 47, 65, 
   24, 25, 50, 51, 26, 27, 29, 30, 31, 53, 54}}, 
 {{MySparseMatrix[{6, 28}, {m[1, 1] -> 1/6, m[2, 10] -> 2/3, 
     m[2, 15] -> -2/3, m[3, 10] -> 2, m[3, 15] -> -2, m[4, 3] -> 1/2, 
     m[4, 13] -> -1, m[4, 14] -> 1/2, m[4, 16] -> -1, m[4, 18] -> 1, 
     m[4, 20] -> 1, m[4, 22] -> 1, m[4, 23] -> -1/3, m[5, 9] -> -1/36, 
     m[5, 13] -> 1/18, m[5, 14] -> -1/36, m[5, 21] -> -1/18, 
     m[5, 22] -> -1/18, m[5, 23] -> 1/108, m[6, 1] -> 9/2, m[6, 2] -> 5/3, 
     m[6, 4] -> 11/6, m[6, 5] -> -1, m[6, 6] -> 5/3, m[6, 7] -> -1, 
     m[6, 8] -> 1, m[6, 9] -> 455/36, m[6, 10] -> -3/2, m[6, 11] -> 1, 
     m[6, 12] -> -1, m[6, 13] -> -437/18, m[6, 14] -> 455/36, 
     m[6, 15] -> 11/6, m[6, 16] -> 1, m[6, 17] -> 1, m[6, 18] -> -1, 
     m[6, 19] -> -1, m[6, 21] -> 455/18, m[6, 22] -> 455/18, 
     m[6, 23] -> -365/108, m[6, 24] -> -11/6, m[6, 25] -> 1, m[6, 26] -> -1, 
     m[6, 27] -> 1, m[6, 28] -> -1}], MySparseMatrix[{6, 28}, {}]}, 
  {MySparseMatrix[{17, 28}, {m[1, 1] -> 1/3, m[2, 2] -> -2, m[2, 4] -> 2, 
     m[3, 2] -> -1/2, m[3, 4] -> 1/2, m[4, 10] -> 2, m[4, 15] -> -2, 
     m[5, 10] -> -2, m[5, 15] -> 2, m[6, 10] -> -2, m[6, 15] -> 2, 
     m[7, 10] -> -2/3, m[7, 15] -> 2/3, m[8, 4] -> 1/2, m[8, 15] -> -1/2, 
     m[8, 24] -> -1/2, m[9, 4] -> -2, m[9, 15] -> 2, m[9, 24] -> 2, 
     m[10, 3] -> 1/2, m[10, 12] -> -1, m[10, 14] -> 1/2, m[10, 16] -> -1, 
     m[10, 17] -> 1, m[10, 20] -> 1, m[10, 22] -> 1, m[10, 23] -> 1/6, 
     m[10, 25] -> -1, m[10, 26] -> 1, m[11, 1] -> -9/2, m[11, 2] -> -5/3, 
     m[11, 4] -> -5/3, m[11, 5] -> 1, m[11, 7] -> -11/6, m[11, 8] -> 1, 
     m[11, 9] -> -1, m[11, 11] -> -1, m[11, 12] -> -1, m[11, 13] -> 1, 
     m[11, 15] -> -1/3, m[11, 16] -> -1, m[11, 17] -> 1, m[11, 18] -> -1, 
     m[11, 19] -> 1, m[11, 23] -> 1/6, m[11, 24] -> 31/6, m[11, 25] -> -2, 
     m[11, 26] -> 1, m[11, 27] -> 1, m[12, 1] -> -9/2, m[12, 2] -> -5/3, 
     m[12, 4] -> -5/3, m[12, 5] -> 1, m[12, 7] -> -11/6, m[12, 8] -> 1, 
     m[12, 9] -> -1, m[12, 11] -> -1, m[12, 12] -> -1, m[12, 13] -> 1, 
     m[12, 15] -> -1/3, m[12, 16] -> -1, m[12, 17] -> 1, m[12, 18] -> -1, 
     m[12, 19] -> 1, m[12, 23] -> 1/6, m[12, 24] -> 31/6, m[12, 25] -> -2, 
     m[12, 26] -> 1, m[12, 27] -> 1, m[13, 6] -> -1/2, m[13, 12] -> 1, 
     m[13, 14] -> -1/2, m[13, 21] -> -1, m[13, 22] -> -1, m[13, 23] -> -1/6, 
     m[13, 26] -> -1, m[13, 28] -> 1, m[14, 1] -> 9/2, m[14, 2] -> 5/3, 
     m[14, 4] -> -1/3, m[14, 5] -> -1, m[14, 7] -> 11/6, m[14, 8] -> -1, 
     m[14, 9] -> 1, m[14, 10] -> 2, m[14, 11] -> 1, m[14, 12] -> 1, 
     m[14, 13] -> -1, m[14, 15] -> 1/3, m[14, 16] -> 1, m[14, 17] -> -1, 
     m[14, 18] -> 1, m[14, 19] -> -1, m[14, 23] -> -1/6, m[14, 24] -> -19/6, 
     m[14, 25] -> 2, m[14, 26] -> -1, m[14, 27] -> -1, m[15, 1] -> -9/2, 
     m[15, 2] -> -5/3, m[15, 4] -> -5/3, m[15, 5] -> 1, m[15, 7] -> -11/6, 
     m[15, 8] -> 1, m[15, 9] -> -1, m[15, 10] -> -2, m[15, 11] -> -1, 
     m[15, 12] -> -1, m[15, 13] -> 1, m[15, 15] -> 5/3, m[15, 16] -> -1, 
     m[15, 17] -> 1, m[15, 18] -> -1, m[15, 19] -> 1, m[15, 23] -> 1/6, 
     m[15, 24] -> 31/6, m[15, 25] -> -2, m[15, 26] -> 1, m[15, 27] -> 1, 
     m[16, 3] -> 1/2, m[16, 10] -> -2, m[16, 12] -> -1, m[16, 14] -> 1/2, 
     m[16, 15] -> 2, m[16, 16] -> -1, m[16, 17] -> 1, m[16, 20] -> 1, 
     m[16, 22] -> 1, m[16, 23] -> 1/6, m[16, 25] -> -1, m[16, 26] -> 1, 
     m[17, 6] -> 1/2, m[17, 10] -> -2, m[17, 12] -> -1, m[17, 14] -> 1/2, 
     m[17, 15] -> 2, m[17, 21] -> 1, m[17, 22] -> 1, m[17, 23] -> 1/6, 
     m[17, 26] -> 1, m[17, 28] -> -1}], MySparseMatrix[{17, 28}, {}]}, 
  {MySparseMatrix[{20, 30}, {m[1, 1] -> 1/3, m[2, 2] -> -2, m[2, 4] -> 2, 
     m[3, 2] -> 2, m[3, 4] -> -2, m[4, 2] -> 2, m[4, 4] -> -2, 
     m[5, 2] -> -1/2, m[5, 4] -> 1/2, m[6, 2] -> 2/3, m[6, 15] -> -2/3, 
     m[7, 2] -> 2, m[7, 15] -> -2, m[8, 3] -> -1, m[8, 5] -> 1, 
     m[8, 16] -> 1, m[8, 17] -> -1, m[8, 20] -> -1, m[8, 22] -> -1, 
     m[8, 24] -> -1/6, m[9, 3] -> -1, m[9, 5] -> 1, m[9, 16] -> 1, 
     m[9, 17] -> -1, m[9, 20] -> -1, m[9, 22] -> -1, m[9, 24] -> -1/6, 
     m[10, 2] -> 2, m[10, 3] -> -1, m[10, 4] -> -2, m[10, 5] -> 1, 
     m[10, 16] -> 1, m[10, 17] -> -1, m[10, 20] -> -1, m[10, 22] -> -1, 
     m[10, 24] -> -1/6, m[11, 3] -> -1/2, m[11, 11] -> 1, m[11, 14] -> -1/2, 
     m[11, 22] -> -1, m[11, 23] -> -1, m[11, 24] -> 1/6, m[12, 2] -> -2, 
     m[12, 3] -> 2, m[12, 4] -> 2, m[12, 5] -> -2, m[12, 16] -> -2, 
     m[12, 17] -> 2, m[12, 20] -> 2, m[12, 22] -> 2, m[12, 24] -> 1/3, 
     m[13, 3] -> -1/36, m[13, 5] -> 1/18, m[13, 11] -> -1/18, 
     m[13, 14] -> 1/36, m[13, 16] -> 1/18, m[13, 17] -> -1/18, 
     m[13, 20] -> -1/18, m[13, 23] -> 1/18, m[13, 24] -> -1/54, 
     m[14, 2] -> 2, m[14, 3] -> -1/2, m[14, 4] -> -2, m[14, 11] -> 1, 
     m[14, 14] -> -1/2, m[14, 22] -> -1, m[14, 23] -> -1, m[14, 24] -> 1/6, 
     m[15, 2] -> 2, m[15, 3] -> 419/36, m[15, 4] -> -2, m[15, 5] -> -437/18, 
     m[15, 11] -> 455/18, m[15, 14] -> -455/36, m[15, 16] -> -437/18, 
     m[15, 17] -> 437/18, m[15, 20] -> 437/18, m[15, 22] -> -1, 
     m[15, 23] -> -455/18, m[15, 24] -> 223/27, m[16, 4] -> -2, 
     m[16, 7] -> 2, m[16, 25] -> -2, m[17, 2] -> 1/2, m[17, 7] -> -1/2, 
     m[17, 25] -> 1/2, m[18, 1] -> 9/2, m[18, 2] -> -3/2, m[18, 3] -> 1/2, 
     m[18, 4] -> 11/6, m[18, 5] -> -1, m[18, 7] -> 5/3, m[18, 8] -> 1, 
     m[18, 9] -> -1, m[18, 10] -> 5/3, m[18, 12] -> 1, m[18, 13] -> -1, 
     m[18, 14] -> 1/2, m[18, 15] -> 11/6, m[18, 16] -> -1, m[18, 17] -> 1, 
     m[18, 18] -> 1, m[18, 19] -> -1, m[18, 22] -> 1, m[18, 23] -> 1, 
     m[18, 24] -> 2/3, m[18, 25] -> -5/3, m[18, 26] -> -1, m[18, 27] -> 1, 
     m[18, 28] -> 1, m[18, 29] -> -1, m[19, 3] -> 1/2, m[19, 6] -> -1/2, 
     m[19, 8] -> -1, m[19, 9] -> 1, m[19, 20] -> 1, m[19, 21] -> -1, 
     m[19, 26] -> 1, m[19, 30] -> -1, m[20, 1] -> 9/2, m[20, 2] -> 1/2, 
     m[20, 4] -> -1/6, m[20, 6] -> -1/2, m[20, 7] -> 5/3, m[20, 10] -> 5/3, 
     m[20, 12] -> 1, m[20, 13] -> -1, m[20, 14] -> 1/2, m[20, 15] -> 11/6, 
     m[20, 18] -> 1, m[20, 19] -> -1, m[20, 21] -> -1, m[20, 23] -> 1, 
     m[20, 24] -> 1/2, m[20, 25] -> -5/3, m[20, 27] -> 1, m[20, 28] -> 1, 
     m[20, 29] -> -1, m[20, 30] -> -1}], MySparseMatrix[{20, 30}, {}]}, 
  {MySparseMatrix[{13, 28}, {m[1, 1] -> 1/3, m[2, 6] -> -2, m[2, 15] -> 2, 
     m[3, 6] -> -2/3, m[3, 15] -> 2/3, m[4, 9] -> 1/2, m[4, 13] -> -1, 
     m[4, 14] -> 1/2, m[4, 21] -> 1, m[4, 22] -> 1, m[4, 23] -> -1/6, 
     m[5, 3] -> 1/36, m[5, 14] -> -1/36, m[5, 16] -> -1/18, m[5, 19] -> 1/18, 
     m[5, 20] -> 1/18, m[5, 22] -> -1/18, m[5, 23] -> -1/108, m[6, 3] -> 1/2, 
     m[6, 14] -> -1/2, m[6, 16] -> -1, m[6, 19] -> 1, m[6, 20] -> 1, 
     m[6, 22] -> -1, m[6, 23] -> -1/6, m[7, 3] -> -437/36, m[7, 9] -> 1/2, 
     m[7, 13] -> -1, m[7, 14] -> 455/36, m[7, 16] -> 437/18, 
     m[7, 19] -> -437/18, m[7, 20] -> -437/18, m[7, 21] -> 1, 
     m[7, 22] -> 455/18, m[7, 23] -> 419/108, m[8, 1] -> 9/2, m[8, 2] -> 5/3, 
     m[8, 3] -> 1/2, m[8, 4] -> 11/6, m[8, 5] -> -1, m[8, 6] -> -3/2, 
     m[8, 7] -> 1, m[8, 8] -> -1, m[8, 9] -> 1, m[8, 10] -> 5/3, 
     m[8, 11] -> -1, m[8, 12] -> 1, m[8, 13] -> -1, m[8, 14] -> 1/2, 
     m[8, 15] -> 11/6, m[8, 17] -> 1, m[8, 18] -> -1, m[8, 20] -> 1, 
     m[8, 21] -> 2, m[8, 22] -> 1, m[8, 23] -> 1/3, m[8, 24] -> -11/6, 
     m[8, 25] -> 1, m[8, 26] -> 1, m[8, 27] -> -1, m[8, 28] -> -1, 
     m[9, 1] -> 9/2, m[9, 2] -> 5/3, m[9, 3] -> 1/2, m[9, 4] -> 11/6, 
     m[9, 5] -> -1, m[9, 6] -> -3/2, m[9, 7] -> 1, m[9, 8] -> -1, 
     m[9, 9] -> 1/2, m[9, 10] -> 5/3, m[9, 11] -> -1, m[9, 12] -> 1, 
     m[9, 15] -> 11/6, m[9, 17] -> 1, m[9, 18] -> -1, m[9, 20] -> 1, 
     m[9, 21] -> 1, m[9, 23] -> 1/2, m[9, 24] -> -11/6, m[9, 25] -> 1, 
     m[9, 26] -> 1, m[9, 27] -> -1, m[9, 28] -> -1, m[10, 1] -> 9/2, 
     m[10, 2] -> 5/3, m[10, 3] -> 1/2, m[10, 4] -> 11/6, m[10, 5] -> -1, 
     m[10, 6] -> -3/2, m[10, 7] -> 1, m[10, 8] -> -1, m[10, 9] -> 1/2, 
     m[10, 10] -> 5/3, m[10, 11] -> -1, m[10, 12] -> 1, m[10, 15] -> 11/6, 
     m[10, 17] -> 1, m[10, 18] -> -1, m[10, 20] -> 1, m[10, 21] -> 1, 
     m[10, 23] -> 1/2, m[10, 24] -> -11/6, m[10, 25] -> 1, m[10, 26] -> 1, 
     m[10, 27] -> -1, m[10, 28] -> -1, m[11, 1] -> 9/2, m[11, 2] -> 5/3, 
     m[11, 3] -> 1/2, m[11, 4] -> 11/6, m[11, 5] -> -1, m[11, 6] -> -3/2, 
     m[11, 7] -> 1, m[11, 8] -> -1, m[11, 9] -> 1/2, m[11, 10] -> 5/3, 
     m[11, 11] -> -1, m[11, 12] -> 1, m[11, 15] -> 11/6, m[11, 17] -> 1, 
     m[11, 18] -> -1, m[11, 20] -> 1, m[11, 21] -> 1, m[11, 23] -> 1/2, 
     m[11, 24] -> -11/6, m[11, 25] -> 1, m[11, 26] -> 1, m[11, 27] -> -1, 
     m[11, 28] -> -1, m[12, 1] -> -9/2, m[12, 2] -> -5/3, m[12, 3] -> -1, 
     m[12, 4] -> -11/6, m[12, 5] -> 1, m[12, 6] -> 3/2, m[12, 7] -> -1, 
     m[12, 8] -> 1, m[12, 9] -> -1, m[12, 10] -> -5/3, m[12, 11] -> 1, 
     m[12, 12] -> -1, m[12, 13] -> 1, m[12, 15] -> -11/6, m[12, 16] -> 1, 
     m[12, 17] -> -1, m[12, 18] -> 1, m[12, 19] -> -1, m[12, 20] -> -2, 
     m[12, 21] -> -2, m[12, 23] -> -1/6, m[12, 24] -> 11/6, m[12, 25] -> -1, 
     m[12, 26] -> -1, m[12, 27] -> 1, m[12, 28] -> 1, m[13, 1] -> -9/2, 
     m[13, 2] -> -5/3, m[13, 4] -> -11/6, m[13, 5] -> 1, m[13, 6] -> 3/2, 
     m[13, 7] -> -1, m[13, 8] -> 1, m[13, 9] -> -1/2, m[13, 10] -> -5/3, 
     m[13, 11] -> 1, m[13, 12] -> -1, m[13, 14] -> -1/2, m[13, 15] -> -11/6, 
     m[13, 16] -> -1, m[13, 17] -> -1, m[13, 18] -> 1, m[13, 19] -> 1, 
     m[13, 21] -> -1, m[13, 22] -> -1, m[13, 23] -> -2/3, m[13, 24] -> 11/6, 
     m[13, 25] -> -1, m[13, 26] -> -1, m[13, 27] -> 1, m[13, 28] -> 1}], 
   MySparseMatrix[{13, 28}, {}]}, 
  {MySparseMatrix[{18, 28}, {m[1, 1] -> -1/3, m[2, 6] -> 2/3, 
     m[2, 10] -> -2/3, m[3, 6] -> 2, m[3, 10] -> -2, m[4, 2] -> 1/2, 
     m[4, 15] -> -1/2, m[5, 2] -> 2, m[5, 15] -> -2, m[6, 10] -> -1/2, 
     m[6, 15] -> 1/2, m[6, 24] -> -1/2, m[7, 10] -> 2, m[7, 15] -> -2, 
     m[7, 24] -> 2, m[8, 10] -> 2, m[8, 15] -> -2, m[8, 24] -> 2, 
     m[9, 10] -> 2, m[9, 15] -> -2, m[9, 24] -> 2, m[10, 10] -> 2, 
     m[10, 15] -> -2, m[10, 24] -> 2, m[11, 10] -> 2, m[11, 15] -> -2, 
     m[11, 24] -> 2, m[12, 3] -> -1/2, m[12, 11] -> 1, m[12, 14] -> -1/2, 
     m[12, 21] -> -1, m[12, 22] -> -1, m[12, 23] -> -1/6, m[12, 25] -> 1, 
     m[12, 28] -> -1, m[13, 9] -> -1/2, m[13, 14] -> 1/2, m[13, 18] -> 1, 
     m[13, 19] -> -1, m[13, 20] -> -1, m[13, 22] -> 1, m[13, 26] -> -1, 
     m[13, 28] -> 1, m[14, 1] -> 1/4, m[14, 2] -> 5/54, m[14, 3] -> 1/36, 
     m[14, 4] -> 11/108, m[14, 5] -> -1/18, m[14, 7] -> 1/18, 
     m[14, 8] -> -1/18, m[14, 9] -> 1/36, m[14, 10] -> 7/54, 
     m[14, 12] -> 1/18, m[14, 13] -> -1/18, m[14, 15] -> -1/54, 
     m[14, 16] -> -1/18, m[14, 17] -> 1/18, m[14, 20] -> 1/18, 
     m[14, 21] -> 1/18, m[14, 24] -> -19/108, m[14, 25] -> 1/18, 
     m[14, 27] -> -1/18, m[15, 3] -> 1/2, m[15, 9] -> 1/2, m[15, 10] -> 2, 
     m[15, 11] -> -1, m[15, 15] -> -2, m[15, 18] -> -1, m[15, 19] -> 1, 
     m[15, 20] -> 1, m[15, 21] -> 1, m[15, 23] -> 1/6, m[15, 24] -> 2, 
     m[15, 25] -> -1, m[15, 26] -> 1, m[16, 1] -> -9/2, m[16, 2] -> -5/3, 
     m[16, 3] -> -1/2, m[16, 4] -> -11/6, m[16, 5] -> 1, m[16, 7] -> -1, 
     m[16, 8] -> 1, m[16, 9] -> -1/2, m[16, 10] -> -13/3, m[16, 12] -> -1, 
     m[16, 13] -> 1, m[16, 15] -> 7/3, m[16, 16] -> 1, m[16, 17] -> -1, 
     m[16, 20] -> -1, m[16, 21] -> -1, m[16, 24] -> 7/6, m[16, 25] -> -1, 
     m[16, 27] -> 1, m[17, 1] -> -437/4, m[17, 2] -> -2185/54, 
     m[17, 3] -> -437/36, m[17, 4] -> -4807/108, m[17, 5] -> 437/18, 
     m[17, 6] -> -2, m[17, 7] -> -437/18, m[17, 8] -> 437/18, 
     m[17, 9] -> -437/36, m[17, 10] -> -2951/54, m[17, 12] -> -437/18, 
     m[17, 13] -> 437/18, m[17, 15] -> 437/54, m[17, 16] -> 437/18, 
     m[17, 17] -> -437/18, m[17, 20] -> -437/18, m[17, 21] -> -437/18, 
     m[17, 24] -> 8303/108, m[17, 25] -> -437/18, m[17, 27] -> 437/18, 
     m[18, 3] -> -1/2, m[18, 11] -> 1, m[18, 14] -> -1/2, m[18, 21] -> -1, 
     m[18, 22] -> -1, m[18, 23] -> -1/6, m[18, 25] -> 1, m[18, 28] -> -1}], 
   MySparseMatrix[{18, 28}, {}]}, 
  {MySparseMatrix[{19, 30}, {m[1, 1] -> 1/3, m[2, 7] -> -2, m[2, 11] -> 2, 
     m[3, 7] -> -2/3, m[3, 11] -> 2/3, m[4, 7] -> 2, m[4, 15] -> -2, 
     m[5, 7] -> 2, m[5, 15] -> -2, m[6, 7] -> 2, m[6, 15] -> -2, 
     m[7, 7] -> 2, m[7, 15] -> -2, m[8, 7] -> -2, m[8, 15] -> 2, 
     m[9, 7] -> -2, m[9, 15] -> 2, m[10, 7] -> -1/2, m[10, 15] -> 1/2, 
     m[11, 6] -> 1/36, m[11, 9] -> -1/18, m[11, 10] -> 1/36, 
     m[11, 20] -> 1/18, m[11, 21] -> 1/18, m[11, 24] -> -1/108, 
     m[12, 10] -> -1, m[12, 14] -> 1, m[12, 18] -> 1, m[12, 19] -> -1, 
     m[12, 21] -> -1, m[12, 23] -> -1, m[12, 24] -> -1/6, m[13, 10] -> -1, 
     m[13, 14] -> 1, m[13, 18] -> 1, m[13, 19] -> -1, m[13, 21] -> -1, 
     m[13, 23] -> -1, m[13, 24] -> -1/6, m[14, 6] -> -455/36, m[14, 7] -> 2, 
     m[14, 9] -> 455/18, m[14, 10] -> -455/36, m[14, 15] -> -2, 
     m[14, 20] -> -455/18, m[14, 21] -> -455/18, m[14, 24] -> 455/108, 
     m[15, 2] -> -1/2, m[15, 7] -> 1/2, m[15, 25] -> 1/2, m[16, 3] -> 1/2, 
     m[16, 8] -> -1, m[16, 10] -> 1/2, m[16, 22] -> 1, m[16, 23] -> 1, 
     m[16, 24] -> 1/6, m[16, 27] -> 1, m[16, 30] -> -1, m[17, 2] -> 2, 
     m[17, 15] -> -2, m[17, 25] -> -2, m[18, 1] -> 9/2, m[18, 2] -> 5/3, 
     m[18, 4] -> 5/3, m[18, 5] -> -1, m[18, 7] -> 1/2, m[18, 8] -> 1, 
     m[18, 9] -> 1, m[18, 11] -> 11/6, m[18, 12] -> 1, m[18, 13] -> -1, 
     m[18, 14] -> -1, m[18, 15] -> -1/6, m[18, 16] -> -1, m[18, 17] -> 1, 
     m[18, 18] -> -1, m[18, 19] -> 1, m[18, 24] -> 5/6, m[18, 25] -> -5/3, 
     m[18, 26] -> 1, m[18, 27] -> -1, m[18, 28] -> -1, m[18, 29] -> 1, 
     m[19, 1] -> 9/2, m[19, 2] -> 5/3, m[19, 3] -> 1/2, m[19, 4] -> 5/3, 
     m[19, 5] -> -1, m[19, 7] -> 5/2, m[19, 9] -> 1, m[19, 10] -> 1/2, 
     m[19, 11] -> 11/6, m[19, 12] -> 1, m[19, 13] -> -1, m[19, 14] -> -1, 
     m[19, 15] -> -13/6, m[19, 16] -> -1, m[19, 17] -> 1, m[19, 18] -> -1, 
     m[19, 19] -> 1, m[19, 22] -> 1, m[19, 23] -> 1, m[19, 24] -> 1, 
     m[19, 25] -> -5/3, m[19, 26] -> 1, m[19, 28] -> -1, m[19, 29] -> 1, 
     m[19, 30] -> -1}], MySparseMatrix[{19, 30}, {}]}}, {1, pflip}, 
 {f[1] -> (y[1]^2*y[3])/y[2], f[6] -> y[1]^2*y[3], 
  f[5] -> (y[1]^2*y[3]*y[5])/(y[2]*y[4]), 
  f[2] -> (y[1]^2*y[3]*y[6]*y[7])/y[8], 
  f[3] -> (y[1]*y[3]*y[9])/y[2] - (2*y[1]*y[3]^2*y[10]^3*y[11])/
     (y[12]*y[13]^2) - (4*y[1]^2*y[2]*y[3]^2*y[4]^2*y[7]^3*y[10])/
     (y[8]^2*y[13]) + (y[1]*y[3]*y[15]*y[16])/y[14] - 
    (y[1]*y[3]*y[4]^3*y[7]^3*y[18])/(y[13]^2*y[17]) - 
    (y[1]*y[3]*y[4]*y[7]^2*y[10]*y[19])/(y[8]*y[13]^2), 
  f[4] -> (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[7]^3)/y[8]^3 + 
    (3*y[1]^2*y[2]*y[3]*y[4]*y[6]*y[7]^2)/(4*y[8]^2), 
  f[13] -> (y[1]*y[20])/y[10], f[7] -> y[1]*y[21], 
  f[10] -> y[1]^2*y[3] - (y[1]^2*y[3]*y[7]*y[10])/y[22], 
  f[14] -> y[23]/(y[3]*y[10]), f[15] -> (y[4]*y[10])/y[3], 
  f[12] -> y[24]/(y[3]*y[10]) + y[10]/(y[3]*y[25]^2) - 
    (2*y[26])/(y[3]*y[25]), 
  f[8] -> (y[1]*y[2]^3*y[3]^2*y[13])/(y[11]^2*y[12]) + 
    y[1]*y[3]^2*y[4]*y[14] - (3*y[1]*y[3]*y[7]^2*y[10]^2)/(2*y[22]) - 
    (2*y[1]^2*y[2]*y[3]^2*y[4]*y[7]^3*y[10])/(y[11]*y[27]^2) - 
    (3*y[1])/(2*y[25]*y[31]) - (y[1]*y[3]^2*y[7]^3*y[29]*y[30])/
     (2*y[11]^2*y[28]*y[31]) - (y[1]*y[2]*y[3]^2*y[4]*y[7]^2*y[32])/
     (2*y[11]^2*y[27]) - (y[1]*y[33])/(2*y[10]), 
  f[9] -> (y[1]^3*y[3]^2*y[7]^3*y[10]^2)/y[27]^3 + 
    (3*y[1]*y[2]^2*y[3]^2*y[7])/(2*y[27]) + (3*y[1]*y[3]^2*y[34])/(4*y[10]) + 
    (3*y[1]^2*y[3]^2*y[7]^2*y[10]*y[35])/(4*y[27]^2), 
  f[11] -> (y[1]^2*y[3]*y[7]^2*y[10]^2)/y[22]^2 + (2*y[1]*y[4]*y[7]*y[10])/
     y[22] - y[1]*y[36], f[22] -> (y[1]*y[3])/(y[2]*y[4]) + 
    (y[1]*y[3])/(y[4]*y[37]), f[16] -> (y[1]*y[21])/(y[2]*y[4]), 
  f[18] -> (y[1]^2*y[3]^2*y[7]*y[38])/y[27], 
  f[17] -> (y[1]*y[36])/(y[2]*y[10]), 
  f[23] -> y[26]/(y[2]*y[4]) + (y[2]*y[3])/(y[4]*y[37]^2) + 
    (2*y[39])/(y[4]*y[37]), f[24] -> y[2]*y[3]^2*y[10]*y[40], 
  f[25] -> y[10]/(y[2]*y[3]), 
  f[19] -> (y[1]^4*y[3]^2*y[7]^3)/(y[11]^2*y[12]*y[13]^2) + 
    (2*y[1]^2*y[2]*y[3]^2*y[4]^2*y[7]^3*y[10])/(y[8]^2*y[13]) + 
    (2*y[1]^2*y[2]*y[3]^2*y[4]*y[7]^3*y[10])/(y[11]*y[27]^2) - 
    (3*y[1]*y[3]*y[41])/(2*y[2]*y[4]*y[10]) + 
    (3*y[1]*y[3])/(2*y[4]*y[37]*y[42]) + (y[1]*y[3]^2*y[7]^3*y[43])/
     (2*y[11]^2*y[28]) + (y[1]*y[2]*y[3]^2*y[4]*y[7]^2*y[44])/
     (2*y[11]^2*y[27]) - (y[1]*y[3]^2*y[4]^2*y[7]^3*y[45]*y[46])/
     (2*y[13]^2*y[17]*y[42]) - (y[1]*y[3]*y[4]*y[7]^2*y[10]*y[47])/
     (2*y[8]*y[13]^2), f[20] -> (4*y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[7]^3)/
     (3*y[8]^3) + (y[2]*y[3])/(y[4]*y[37]^2) + (2*y[39])/(y[4]*y[37]) + 
    y[48]/y[4] - (y[1]^2*y[2]*y[3]*y[4]*y[7]^2*y[10]*y[49])/y[8]^2 + 
    (2*y[1]*y[7]*y[10]*y[50])/y[8], 
  f[21] -> (y[1]^3*y[3]^2*y[7]^3*y[10]^2)/y[27]^3 + 
    (3*y[1]^2*y[2]*y[3]^2*y[7]^2*y[10]*y[40])/(4*y[27]^2) + 
    (3*y[1]*y[2]*y[3]^2*y[7]*y[10]*y[40])/(2*y[27]), 
  f[26] -> (y[1]*y[2]^3*y[3]^2*y[13])/(y[11]^2*y[12]) - 
    (2*y[1]^2*y[2]*y[3]^2*y[4]*y[7]^3*y[10])/(y[11]*y[27]^2) - 
    (y[1]*y[3]^2*y[7]^3*y[43])/(2*y[11]^2*y[28]) - 
    (y[1]*y[3]^2*y[51])/(2*y[10]) - (y[1]*y[3]^2*y[52]*y[53])/(2*y[14]) - 
    (y[1]*y[2]*y[3]^2*y[4]*y[7]^2*y[54])/(2*y[11]^2*y[27]), 
  f[27] -> (y[1]^3*y[3]^2*y[7]^3*y[10]^2)/y[27]^3 + 
    (3*y[1]^2*y[3]^2*y[7]^2*y[10]*y[38])/(4*y[27]^2), 
  f[32] -> (y[1]^2*y[3]^2)/y[4] - (y[1]^2*y[2]*y[3]^2*y[7])/y[55], 
  f[28] -> (y[1]*y[20])/(y[2]*y[4]), f[29] -> (y[1]*y[36])/y[2], 
  f[36] -> (y[2]*y[3]^2)/y[4], f[34] -> (y[2]*y[3])/(y[4]*y[37]^2) - 
    y[56]/(y[2]*y[4]) + (2*y[57])/(y[4]*y[37]), 
  f[30] -> (y[1]*y[3]^2*y[10]^3*y[11])/(y[12]*y[13]^2) + 
    (2*y[1]^2*y[2]*y[3]^2*y[4]^2*y[7]^3*y[10])/(y[8]^2*y[13]) + 
    y[1]*y[3]^2*y[4]*y[14] + (3*y[1]*y[3])/(2*y[4]*y[37]*y[42]) - 
    (y[1]*y[3]^2*y[4]^2*y[7]^3*y[45]*y[46])/(2*y[13]^2*y[17]*y[42]) + 
    (3*y[1]*y[2]^2*y[3]^2*y[4]*y[7]^2)/(2*y[55]) + 
    (y[1]*y[3]*y[4]*y[7]^2*y[10]*y[58])/(2*y[8]*y[13]^2) - 
    (y[1]*y[3]*y[59])/(2*y[2]*y[4]), f[35] -> y[10]^2/(y[2]*y[3]), 
  f[31] -> (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[7]^3)/y[8]^3 + 
    (3*y[1]*y[7]*y[10]^2)/(2*y[8]) + (3*y[1]*y[60])/(4*y[2]) + 
    (3*y[1]^2*y[2]*y[3]*y[4]*y[7]^2*y[61])/(4*y[8]^2), 
  f[33] -> (y[1]^2*y[2]^2*y[3]^2*y[4]*y[7]^2)/y[55]^2 + 
    (2*y[1]*y[2]*y[3]^2*y[7])/y[55] - (y[1]*y[3]^2*y[62])/y[4], 
  f[37] -> (y[1]*y[3]*y[63])/y[10], f[42] -> (y[1]*y[4])/y[10] - y[1]/y[25], 
  f[44] -> y[1]*y[3]*y[64], f[38] -> (y[1]*y[3]*y[62])/(y[2]*y[10]), 
  f[45] -> y[2]*y[3]*y[4]*y[10]*y[40], f[46] -> (y[2]*y[3])/y[10], 
  f[43] -> y[10]/(y[3]*y[25]^2) - (2*y[39])/(y[3]*y[25]) + 
    (y[4]*y[57])/(y[3]*y[10]), 
  f[41] -> (y[1]^4*y[3]^2*y[7]^3)/(y[11]^2*y[12]*y[13]^2) + 
    (2*y[1]^2*y[2]*y[3]^2*y[4]^2*y[7]^3*y[10])/(y[8]^2*y[13]) + 
    (y[1]*y[3]*y[4]^3*y[7]^3*y[18])/(2*y[13]^2*y[17]) + 
    (2*y[1]^2*y[2]*y[3]^2*y[4]*y[7]^3*y[10])/(y[11]*y[27]^2) + 
    (3*y[1])/(2*y[25]*y[31]) + (y[1]*y[3]^2*y[7]^3*y[29]*y[30])/
     (2*y[11]^2*y[28]*y[31]) - (3*y[1]*y[65])/(2*y[2]*y[10]) + 
    (y[1]*y[2]*y[3]^2*y[4]*y[7]^2*y[66])/(2*y[11]^2*y[27]) - 
    (y[1]*y[3]*y[4]*y[7]^2*y[10]*y[67])/(2*y[8]*y[13]^2), 
  f[40] -> y[10]/(y[3]*y[25]^2) - (4*y[1]^3*y[3]^2*y[7]^3*y[10]^2)/
     (3*y[27]^3) - (2*y[39])/(y[3]*y[25]) + y[68]/y[3] - 
    (y[1]^2*y[2]*y[3]^2*y[7]^2*y[10]*y[69])/y[27]^2 - 
    (2*y[1]*y[2]*y[3]^2*y[7]*y[70])/y[27], 
  f[39] -> (y[1]^3*y[2]^2*y[3]^2*y[4]^2*y[7]^3)/y[8]^3 - 
    (3*y[1]^2*y[2]*y[3]*y[4]*y[7]^2*y[10]*y[40])/(4*y[8]^2) + 
    (3*y[1]*y[2]*y[3]*y[4]*y[7]*y[10]*y[40])/(2*y[8])}, 
 {y[1] -> ex[2], y[2] -> 1 + ex[2], y[3] -> ex[3], y[4] -> 1 + ex[3], 
  y[5] -> 455 + 18*ex[2] + 455*ex[3], y[6] -> -1 - ex[3] + ex[2]*ex[3], 
  y[7] -> ex[5], y[8] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - 
    ex[3]*ex[4] - ex[2]*ex[3]*ex[4] - ex[2]*ex[5], 
  y[9] -> -9*ex[2] + 2*ex[3] + 2*ex[2]*ex[3] + 2*ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[10] -> 1 + ex[3] + ex[2]*ex[3], y[11] -> 1 + ex[2] - ex[5], 
  y[12] -> 1 + ex[4] - ex[5], y[13] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - 
    ex[3]*ex[5], y[14] -> ex[4], y[15] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[16] -> 2*ex[2]*ex[3] + 3*ex[5] + 2*ex[3]*ex[5], 
  y[17] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[18] -> 3 + 3*ex[3] + ex[2]*ex[3] - 3*ex[5] - 5*ex[3]*ex[5] - 
    2*ex[3]^2*ex[5], y[19] -> -3 - 6*ex[3] - 3*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 
    3*ex[5] + 8*ex[3]*ex[5] + ex[2]*ex[3]*ex[5] + 7*ex[3]^2*ex[5] + 
    3*ex[2]*ex[3]^2*ex[5] + 2*ex[3]^3*ex[5] + 2*ex[2]*ex[3]^3*ex[5], 
  y[20] -> 2 + 4*ex[3] + ex[2]*ex[3], y[21] -> -2 - 2*ex[3] + ex[2]*ex[3], 
  y[22] -> -ex[2] - ex[2]*ex[3] + ex[4] + ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[23] -> 1 + 3*ex[3] + 2*ex[2]*ex[3] + 3*ex[3]^2 + 4*ex[2]*ex[3]^2 + 
    ex[2]^2*ex[3]^2, y[24] -> 3 + 8*ex[3] + 6*ex[2]*ex[3] + 5*ex[3]^2 + 
    8*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 2*ex[2]*ex[3]^3 + 
    3*ex[2]^2*ex[3]^3 + ex[2]^3*ex[3]^3, 
  y[25] -> 1 + ex[2]*ex[3] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[26] -> 2 + 3*ex[3] + 2*ex[2]*ex[3], 
  y[27] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[28] -> ex[2] - ex[4], 
  y[29] -> 1 + 3*ex[2] - 3*ex[5] - 2*ex[3]*ex[5], 
  y[30] -> ex[2] - ex[5] - ex[3]*ex[5], y[31] -> 1 + ex[3]*ex[5], 
  y[32] -> 3 + 12*ex[2] + 9*ex[2]^2 - 5*ex[5] - 7*ex[2]*ex[5] - 
    2*ex[3]*ex[5] - 2*ex[2]*ex[3]*ex[5], 
  y[33] -> -3 - 3*ex[3] - 3*ex[2]*ex[3] + 2*ex[3]^2 - ex[2]*ex[3]^2 + 
    6*ex[2]^2*ex[3]^2 + 4*ex[3]^3 + 8*ex[2]*ex[3]^3 + 2*ex[2]^2*ex[3]^3 + 
    2*ex[3]^4 + 6*ex[2]*ex[3]^4 + 4*ex[2]^2*ex[3]^4 + 4*ex[3]^2*ex[5] - 
    3*ex[2]*ex[3]^2*ex[5] + 8*ex[3]^3*ex[5] + ex[2]*ex[3]^3*ex[5] - 
    3*ex[2]^2*ex[3]^3*ex[5] + 4*ex[3]^4*ex[5] + 4*ex[2]*ex[3]^4*ex[5], 
  y[34] -> 2 + 3*ex[2], y[35] -> 1 + 3*ex[2], 
  y[36] -> 2 + 2*ex[3] + ex[2]*ex[3], 
  y[37] -> -1 + ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[38] -> -1 + ex[2], y[39] -> 1 + 2*ex[3] + ex[2]*ex[3], 
  y[40] -> 1 + 2*ex[3], y[41] -> 1 + ex[3] + ex[2]*ex[3] + 3*ex[2]^2*ex[3], 
  y[42] -> -1 + ex[5] + ex[3]*ex[5], 
  y[43] -> 3 + ex[2] - ex[5] + 2*ex[3]*ex[5], 
  y[44] -> 3 + 6*ex[2] + 3*ex[2]^2 + 6*ex[3] + 12*ex[2]*ex[3] + 
    6*ex[2]^2*ex[3] - 5*ex[5] - ex[2]*ex[5] - 8*ex[3]*ex[5] - 
    8*ex[2]*ex[3]*ex[5], y[45] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[46] -> 1 + ex[3] + 3*ex[2]*ex[3] - ex[5] + ex[3]*ex[5] + 2*ex[3]^2*ex[5], 
  y[47] -> -3 - 6*ex[2]*ex[3] + 9*ex[3]^2 + 6*ex[2]*ex[3]^2 - 
    3*ex[2]^2*ex[3]^2 + 6*ex[3]^3 + 12*ex[2]*ex[3]^3 + 6*ex[2]^2*ex[3]^3 + 
    3*ex[5] - 2*ex[3]*ex[5] - ex[2]*ex[3]*ex[5] - 13*ex[3]^2*ex[5] - 
    9*ex[2]*ex[3]^2*ex[5] - 8*ex[3]^3*ex[5] - 8*ex[2]*ex[3]^3*ex[5], 
  y[48] -> 2 + 3*ex[3] + ex[2]*ex[3], y[49] -> -1 + 2*ex[3], 
  y[50] -> 1 + ex[3]^2 + ex[2]*ex[3]^2 + 2*ex[3]^3 + 2*ex[2]*ex[3]^3, 
  y[51] -> 2 - 9*ex[2] + 4*ex[3] + 2*ex[2]*ex[3] + 2*ex[3]^2 + 
    2*ex[2]*ex[3]^2, y[52] -> ex[2] + ex[5], 
  y[53] -> 2*ex[2]*ex[3] - ex[5] + 2*ex[3]*ex[5], 
  y[54] -> -3 + 3*ex[2]^2 + ex[5] - ex[2]*ex[5] - 2*ex[3]*ex[5] - 
    2*ex[2]*ex[3]*ex[5], y[55] -> -(ex[2]*ex[3]) + ex[4] + ex[3]*ex[4] + 
    ex[2]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[56] -> -2 - 5*ex[3] - 4*ex[2]*ex[3] + 2*ex[2]*ex[3]^2 + 
    3*ex[2]^2*ex[3]^2 + ex[2]^3*ex[3]^2, 
  y[57] -> 1 + 3*ex[3] + 2*ex[2]*ex[3], 
  y[58] -> 3 + 6*ex[3] + 12*ex[2]*ex[3] + 3*ex[3]^2 + 12*ex[2]*ex[3]^2 + 
    9*ex[2]^2*ex[3]^2 - 3*ex[5] - 4*ex[3]*ex[5] - 5*ex[2]*ex[3]*ex[5] + 
    ex[3]^2*ex[5] - 3*ex[2]*ex[3]^2*ex[5] + 2*ex[3]^3*ex[5] + 
    2*ex[2]*ex[3]^3*ex[5], y[59] -> 3 + 2*ex[3] + ex[2]*ex[3] + 
    8*ex[2]^2*ex[3] + 4*ex[3]^2 + 10*ex[2]*ex[3]^2 + 6*ex[2]^2*ex[3]^2 + 
    2*ex[3]^3 + 6*ex[2]*ex[3]^3 + 4*ex[2]^2*ex[3]^3 + 4*ex[3]*ex[5] + 
    7*ex[2]*ex[3]*ex[5] + 3*ex[2]^2*ex[3]*ex[5] + 8*ex[3]^2*ex[5] + 
    11*ex[2]*ex[3]^2*ex[5] + 3*ex[2]^2*ex[3]^2*ex[5] + 4*ex[3]^3*ex[5] + 
    4*ex[2]*ex[3]^3*ex[5], y[60] -> 2 + 2*ex[3] + 3*ex[2]*ex[3], 
  y[61] -> 1 + ex[3] + 3*ex[2]*ex[3], y[62] -> 2 + ex[2], 
  y[63] -> -2 + ex[2], y[64] -> -2 + ex[2] - 2*ex[3] + 2*ex[2]*ex[3], 
  y[65] -> 1 + ex[2] + ex[3] + ex[2]*ex[3] + 3*ex[2]^2*ex[3], 
  y[66] -> 9 + 18*ex[2] + 9*ex[2]^2 + 6*ex[3] + 12*ex[2]*ex[3] + 
    6*ex[2]^2*ex[3] - 11*ex[5] - 7*ex[2]*ex[5] - 8*ex[3]*ex[5] - 
    8*ex[2]*ex[3]*ex[5], y[67] -> 3 + 12*ex[3] + 6*ex[2]*ex[3] + 15*ex[3]^2 + 
    18*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 6*ex[3]^3 + 12*ex[2]*ex[3]^3 + 
    6*ex[2]^2*ex[3]^3 - 3*ex[5] - 14*ex[3]*ex[5] - 7*ex[2]*ex[3]*ex[5] - 
    19*ex[3]^2*ex[5] - 15*ex[2]*ex[3]^2*ex[5] - 8*ex[3]^3*ex[5] - 
    8*ex[2]*ex[3]^3*ex[5], y[68] -> 1 + 3*ex[3] + ex[2]*ex[3], 
  y[69] -> 3 + 2*ex[3], y[70] -> 4 + ex[2] + 5*ex[3] + 3*ex[2]*ex[3] + 
    2*ex[3]^2 + 2*ex[2]*ex[3]^2}}
