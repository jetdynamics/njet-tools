(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[2], f[5], f[6], f[11], f[39], f[16], f[18], f[36], f[4], f[8], 
   f[10], f[9], f[7], f[1]}, {f[13], f[12], f[14], f[11], f[17], f[1], 
   f[15]}, {f[20], f[18], f[21], f[22], f[23], f[12], f[15], f[36], f[19], 
   f[24], f[26], f[33], f[1], f[45], f[11], f[25], f[17], f[16]}, 
  {f[28], f[27], f[12], f[15], f[19], f[36], f[29], f[31], f[30], f[11], 
   f[33], f[35], f[34], f[32], f[17], f[1]}, {f[37], f[38], f[18], f[1], 
   f[36], f[16]}, {f[43], f[40], f[18], f[39], f[16], f[41], f[42], f[12], 
   f[44], f[1], f[9], f[25], f[46], f[45], f[8], f[33], f[11], f[36], f[15], 
   f[17]}}, {{1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
   20, 22, 24, 25, 26, 27, 28, 29, 30, 31}, {1, 34, 35, 36, 37, 38, 4, 39, 
   40, 11, 41, 42, 13, 15, 43, 44, 45, 46, 47, 48, 50, 51, 26, 27, 52, 30, 
   53, 54}, {1, 11, 15, 16, 20, 55, 56, 57, 58, 60, 61, 62, 63, 64, 43, 47, 
   65, 66, 67, 25, 68, 51, 70, 26, 27, 30, 31, 71, 53, 72}, 
  {1, 2, 3, 4, 5, 11, 12, 13, 15, 60, 74, 75, 61, 64, 43, 76, 46, 47, 67, 77, 
   51, 70, 26, 27, 28, 30, 71, 53}, {1, 34, 35, 4, 39, 11, 41, 13, 15, 16, 
   78, 18, 20, 55, 56, 79, 80, 57, 58, 25, 81, 68, 26, 27, 52, 30, 31, 72}, 
  {1, 36, 38, 7, 82, 10, 11, 42, 14, 15, 16, 83, 19, 20, 43, 45, 84, 47, 65, 
   24, 25, 50, 51, 26, 27, 29, 30, 31, 53, 54}}, 
 {{MySparseMatrix[{15, 28}, {m[1, 1] -> 1/6, m[2, 2] -> -2, m[2, 6] -> 2, 
     m[3, 6] -> 1/2, m[3, 15] -> -1/2, m[4, 10] -> 2/3, m[4, 15] -> -2/3, 
     m[5, 6] -> -2, m[5, 15] -> 2, m[6, 6] -> -2, m[6, 15] -> 2, 
     m[7, 10] -> 2, m[7, 15] -> -2, m[8, 10] -> -2, m[8, 15] -> 2, 
     m[9, 10] -> 2, m[9, 15] -> -2, m[10, 2] -> -1/2, m[10, 15] -> 1/2, 
     m[11, 9] -> 1/36, m[11, 13] -> -1/18, m[11, 14] -> 1/36, 
     m[11, 21] -> 1/18, m[11, 22] -> 1/18, m[11, 23] -> -1/108, 
     m[12, 3] -> 1/2, m[12, 13] -> -1, m[12, 14] -> 1/2, m[12, 16] -> -1, 
     m[12, 18] -> 1, m[12, 20] -> 1, m[12, 22] -> 1, m[12, 23] -> -1/3, 
     m[13, 3] -> -1/2, m[13, 13] -> 1, m[13, 14] -> -1/2, m[13, 16] -> 1, 
     m[13, 18] -> -1, m[13, 20] -> -1, m[13, 22] -> -1, m[13, 23] -> 1/3, 
     m[14, 9] -> -1/2, m[14, 13] -> 1, m[14, 14] -> -1/2, m[14, 21] -> -1, 
     m[14, 22] -> -1, m[14, 23] -> 1/6, m[15, 1] -> 9/2, m[15, 2] -> 5/3, 
     m[15, 4] -> 11/6, m[15, 5] -> -1, m[15, 6] -> 5/3, m[15, 7] -> -1, 
     m[15, 8] -> 1, m[15, 9] -> 455/36, m[15, 10] -> -2, m[15, 11] -> 1, 
     m[15, 12] -> -1, m[15, 13] -> -437/18, m[15, 14] -> 455/36, 
     m[15, 15] -> 7/3, m[15, 16] -> 1, m[15, 17] -> 1, m[15, 18] -> -1, 
     m[15, 19] -> -1, m[15, 21] -> 455/18, m[15, 22] -> 455/18, 
     m[15, 23] -> -365/108, m[15, 24] -> -11/6, m[15, 25] -> 1, 
     m[15, 26] -> -1, m[15, 27] -> 1, m[15, 28] -> -1}], 
   MySparseMatrix[{15, 28}, {}]}, 
  {MySparseMatrix[{7, 28}, {m[1, 1] -> 1/3, m[2, 10] -> 2, m[2, 15] -> -2, 
     m[3, 10] -> -2/3, m[3, 15] -> 2/3, m[4, 1] -> 9/2, m[4, 2] -> 5/3, 
     m[4, 3] -> 1/2, m[4, 4] -> 5/3, m[4, 5] -> -1, m[4, 7] -> 11/6, 
     m[4, 8] -> -1, m[4, 9] -> 1, m[4, 10] -> -3/2, m[4, 11] -> 1, 
     m[4, 13] -> -1, m[4, 14] -> 1/2, m[4, 15] -> 11/6, m[4, 18] -> 1, 
     m[4, 19] -> -1, m[4, 20] -> 1, m[4, 22] -> 1, m[4, 24] -> -31/6, 
     m[4, 25] -> 1, m[4, 27] -> -1, m[5, 1] -> 9/2, m[5, 2] -> 5/3, 
     m[5, 4] -> 5/3, m[5, 5] -> -1, m[5, 7] -> 11/6, m[5, 8] -> -1, 
     m[5, 9] -> 1, m[5, 10] -> -3/2, m[5, 11] -> 1, m[5, 12] -> 1, 
     m[5, 13] -> -1, m[5, 15] -> 11/6, m[5, 16] -> 1, m[5, 17] -> -1, 
     m[5, 18] -> 1, m[5, 19] -> -1, m[5, 23] -> -1/6, m[5, 24] -> -31/6, 
     m[5, 25] -> 2, m[5, 26] -> -1, m[5, 27] -> -1, m[6, 1] -> -9/2, 
     m[6, 2] -> -5/3, m[6, 4] -> -5/3, m[6, 5] -> 1, m[6, 7] -> -11/6, 
     m[6, 8] -> 1, m[6, 9] -> -1, m[6, 10] -> 3/2, m[6, 11] -> -1, 
     m[6, 12] -> -1, m[6, 13] -> 1, m[6, 15] -> -11/6, m[6, 16] -> -1, 
     m[6, 17] -> 1, m[6, 18] -> -1, m[6, 19] -> 1, m[6, 23] -> 1/6, 
     m[6, 24] -> 31/6, m[6, 25] -> -2, m[6, 26] -> 1, m[6, 27] -> 1, 
     m[7, 1] -> -9/2, m[7, 2] -> -5/3, m[7, 4] -> -5/3, m[7, 5] -> 1, 
     m[7, 6] -> -1/2, m[7, 7] -> -11/6, m[7, 8] -> 1, m[7, 9] -> -1, 
     m[7, 10] -> 3/2, m[7, 11] -> -1, m[7, 13] -> 1, m[7, 14] -> -1/2, 
     m[7, 15] -> -11/6, m[7, 16] -> -1, m[7, 17] -> 1, m[7, 18] -> -1, 
     m[7, 19] -> 1, m[7, 21] -> -1, m[7, 22] -> -1, m[7, 24] -> 31/6, 
     m[7, 25] -> -2, m[7, 27] -> 1, m[7, 28] -> 1}], 
   MySparseMatrix[{7, 28}, {}]}, 
  {MySparseMatrix[{18, 30}, {m[1, 1] -> 1/3, m[2, 2] -> 2, m[2, 4] -> -2, 
     m[3, 2] -> -2/3, m[3, 4] -> 2/3, m[4, 2] -> 1/2, m[4, 10] -> -1/2, 
     m[5, 2] -> 2/3, m[5, 15] -> -2/3, m[6, 2] -> -2, m[6, 15] -> 2, 
     m[7, 2] -> -2, m[7, 15] -> 2, m[8, 2] -> -2, m[8, 15] -> 2, 
     m[9, 10] -> -2, m[9, 15] -> 2, m[10, 3] -> 1/2, m[10, 11] -> -1, 
     m[10, 14] -> 1/2, m[10, 22] -> 1, m[10, 23] -> 1, m[10, 24] -> -1/6, 
     m[11, 3] -> -1, m[11, 5] -> 1, m[11, 16] -> 1, m[11, 17] -> -1, 
     m[11, 20] -> -1, m[11, 22] -> -1, m[11, 24] -> -1/6, m[12, 3] -> -1, 
     m[12, 5] -> 1, m[12, 16] -> 1, m[12, 17] -> -1, m[12, 20] -> -1, 
     m[12, 22] -> -1, m[12, 24] -> -1/6, m[13, 3] -> 2, m[13, 5] -> -2, 
     m[13, 16] -> -2, m[13, 17] -> 2, m[13, 20] -> 2, m[13, 22] -> 2, 
     m[13, 24] -> 1/3, m[14, 3] -> 1, m[14, 5] -> -1, m[14, 16] -> -1, 
     m[14, 17] -> 1, m[14, 20] -> 1, m[14, 22] -> 1, m[14, 24] -> 1/6, 
     m[15, 2] -> 2, m[15, 3] -> 1, m[15, 5] -> -1, m[15, 15] -> -2, 
     m[15, 16] -> -1, m[15, 17] -> 1, m[15, 20] -> 1, m[15, 22] -> 1, 
     m[15, 24] -> 1/6, m[16, 3] -> 1/2, m[16, 5] -> -1, m[16, 11] -> 1, 
     m[16, 14] -> -1/2, m[16, 16] -> -1, m[16, 17] -> 1, m[16, 20] -> 1, 
     m[16, 23] -> -1, m[16, 24] -> 1/3, m[17, 1] -> -9/2, m[17, 2] -> 3/2, 
     m[17, 3] -> -2, m[17, 4] -> -11/6, m[17, 5] -> 3, m[17, 7] -> -5/3, 
     m[17, 8] -> -1, m[17, 9] -> 1, m[17, 10] -> -5/3, m[17, 11] -> -1, 
     m[17, 12] -> -1, m[17, 13] -> 1, m[17, 15] -> -11/6, m[17, 16] -> 3, 
     m[17, 17] -> -3, m[17, 18] -> -1, m[17, 19] -> 1, m[17, 20] -> -2, 
     m[17, 22] -> -2, m[17, 24] -> -7/6, m[17, 25] -> 5/3, m[17, 26] -> 1, 
     m[17, 27] -> -1, m[17, 28] -> -1, m[17, 29] -> 1, m[18, 1] -> 9/2, 
     m[18, 2] -> -3/2, m[18, 3] -> 1/2, m[18, 4] -> 11/6, m[18, 5] -> -1, 
     m[18, 6] -> -1/2, m[18, 7] -> 5/3, m[18, 10] -> 5/3, m[18, 11] -> 1, 
     m[18, 12] -> 1, m[18, 13] -> -1, m[18, 15] -> 11/6, m[18, 16] -> -1, 
     m[18, 17] -> 1, m[18, 18] -> 1, m[18, 19] -> -1, m[18, 20] -> 1, 
     m[18, 21] -> -1, m[18, 24] -> 5/6, m[18, 25] -> -5/3, m[18, 27] -> 1, 
     m[18, 28] -> 1, m[18, 29] -> -1, m[18, 30] -> -1}], 
   MySparseMatrix[{18, 30}, {}]}, 
  {MySparseMatrix[{16, 28}, {m[1, 1] -> 1/3, m[2, 2] -> 2, m[2, 10] -> -2, 
     m[3, 6] -> 2, m[3, 15] -> -2, m[4, 6] -> 2, m[4, 15] -> -2, 
     m[5, 10] -> 2, m[5, 15] -> -2, m[6, 10] -> 2, m[6, 15] -> -2, 
     m[7, 2] -> 1/2, m[7, 15] -> -1/2, m[8, 10] -> 1/2, m[8, 15] -> -1/2, 
     m[9, 6] -> -2/3, m[9, 15] -> 2/3, m[10, 6] -> -2, m[10, 15] -> 2, 
     m[11, 9] -> 1/2, m[11, 13] -> -1, m[11, 14] -> 1/2, m[11, 21] -> 1, 
     m[11, 22] -> 1, m[11, 23] -> -1/6, m[12, 3] -> -1/2, m[12, 14] -> 1/2, 
     m[12, 16] -> 1, m[12, 19] -> -1, m[12, 20] -> -1, m[12, 22] -> 1, 
     m[12, 23] -> 1/6, m[13, 3] -> 1/2, m[13, 14] -> -1/2, m[13, 16] -> -1, 
     m[13, 19] -> 1, m[13, 20] -> 1, m[13, 22] -> -1, m[13, 23] -> -1/6, 
     m[14, 9] -> -1/2, m[14, 13] -> 1, m[14, 14] -> -1/2, m[14, 21] -> -1, 
     m[14, 22] -> -1, m[14, 23] -> 1/6, m[15, 1] -> 9/2, m[15, 2] -> 5/3, 
     m[15, 4] -> 11/6, m[15, 5] -> -1, m[15, 6] -> -2, m[15, 7] -> 1, 
     m[15, 8] -> -1, m[15, 9] -> 1, m[15, 10] -> 5/3, m[15, 11] -> -1, 
     m[15, 12] -> 1, m[15, 13] -> -1, m[15, 14] -> 1, m[15, 15] -> 7/3, 
     m[15, 16] -> 1, m[15, 17] -> 1, m[15, 18] -> -1, m[15, 19] -> -1, 
     m[15, 21] -> 2, m[15, 22] -> 2, m[15, 23] -> 1/2, m[15, 24] -> -11/6, 
     m[15, 25] -> 1, m[15, 26] -> 1, m[15, 27] -> -1, m[15, 28] -> -1, 
     m[16, 1] -> -9/2, m[16, 2] -> -5/3, m[16, 4] -> -11/6, m[16, 5] -> 1, 
     m[16, 6] -> 2, m[16, 7] -> -1, m[16, 8] -> 1, m[16, 9] -> -1, 
     m[16, 10] -> -5/3, m[16, 11] -> 1, m[16, 12] -> -1, m[16, 13] -> 1, 
     m[16, 14] -> -1, m[16, 15] -> -7/3, m[16, 16] -> -1, m[16, 17] -> -1, 
     m[16, 18] -> 1, m[16, 19] -> 1, m[16, 21] -> -2, m[16, 22] -> -2, 
     m[16, 23] -> -1/2, m[16, 24] -> 11/6, m[16, 25] -> -1, m[16, 26] -> -1, 
     m[16, 27] -> 1, m[16, 28] -> 1}], MySparseMatrix[{16, 28}, {}]}, 
  {MySparseMatrix[{6, 28}, {m[1, 1] -> -1/3, m[2, 6] -> 2/3, 
     m[2, 10] -> -2/3, m[3, 6] -> -2, m[3, 10] -> 2, m[4, 1] -> 9/2, 
     m[4, 2] -> 5/3, m[4, 4] -> 11/6, m[4, 5] -> -1, m[4, 6] -> -3/2, 
     m[4, 7] -> 1, m[4, 8] -> -1, m[4, 10] -> 11/6, m[4, 11] -> 1, 
     m[4, 12] -> 1, m[4, 13] -> -1, m[4, 15] -> 5/3, m[4, 16] -> -1, 
     m[4, 17] -> 1, m[4, 18] -> 1, m[4, 19] -> -1, m[4, 23] -> -1/6, 
     m[4, 24] -> -31/6, m[4, 25] -> 2, m[4, 26] -> -1, m[4, 27] -> -1, 
     m[5, 1] -> -9/2, m[5, 2] -> -5/3, m[5, 3] -> -1/2, m[5, 4] -> -11/6, 
     m[5, 5] -> 1, m[5, 6] -> 3/2, m[5, 7] -> -1, m[5, 8] -> 1, 
     m[5, 9] -> -1/2, m[5, 10] -> -11/6, m[5, 12] -> -1, m[5, 13] -> 1, 
     m[5, 15] -> -5/3, m[5, 16] -> 1, m[5, 17] -> -1, m[5, 20] -> -1, 
     m[5, 21] -> -1, m[5, 24] -> 31/6, m[5, 25] -> -1, m[5, 27] -> 1, 
     m[6, 1] -> -9/2, m[6, 2] -> -5/3, m[6, 4] -> -11/6, m[6, 5] -> 1, 
     m[6, 6] -> 3/2, m[6, 7] -> -1, m[6, 8] -> 1, m[6, 9] -> -1/2, 
     m[6, 10] -> -11/6, m[6, 11] -> -1, m[6, 12] -> -1, m[6, 13] -> 1, 
     m[6, 14] -> 1/2, m[6, 15] -> -5/3, m[6, 16] -> 1, m[6, 17] -> -1, 
     m[6, 20] -> -1, m[6, 22] -> 1, m[6, 23] -> 1/6, m[6, 24] -> 31/6, 
     m[6, 25] -> -2, m[6, 27] -> 1, m[6, 28] -> 1}], 
   MySparseMatrix[{6, 28}, {}]}, 
  {MySparseMatrix[{20, 30}, {m[1, 1] -> 1/3, m[2, 4] -> -1/2, m[2, 7] -> 1/2, 
     m[3, 7] -> 2, m[3, 11] -> -2, m[4, 4] -> 2, m[4, 11] -> -2, 
     m[5, 7] -> -2, m[5, 11] -> 2, m[6, 7] -> -1/2, m[6, 11] -> 1/2, 
     m[7, 7] -> 2/3, m[7, 15] -> -2/3, m[8, 7] -> -2, m[8, 15] -> 2, 
     m[9, 6] -> 1/2, m[9, 9] -> -1, m[9, 10] -> 1/2, m[9, 20] -> 1, 
     m[9, 21] -> 1, m[9, 24] -> -1/6, m[10, 6] -> -419/36, 
     m[10, 9] -> 419/18, m[10, 10] -> -419/36, m[10, 20] -> -419/18, 
     m[10, 21] -> -419/18, m[10, 24] -> 419/108, m[11, 6] -> 1/2, 
     m[11, 9] -> -1, m[11, 10] -> 1/2, m[11, 20] -> 1, m[11, 21] -> 1, 
     m[11, 24] -> -1/6, m[12, 6] -> 1/2, m[12, 9] -> -1, m[12, 10] -> 1/2, 
     m[12, 20] -> 1, m[12, 21] -> 1, m[12, 24] -> -1/6, m[13, 10] -> -1, 
     m[13, 14] -> 1, m[13, 18] -> 1, m[13, 19] -> -1, m[13, 21] -> -1, 
     m[13, 23] -> -1, m[13, 24] -> -1/6, m[14, 10] -> 1, m[14, 14] -> -1, 
     m[14, 18] -> -1, m[14, 19] -> 1, m[14, 21] -> 1, m[14, 23] -> 1, 
     m[14, 24] -> 1/6, m[15, 6] -> -1/36, m[15, 9] -> 1/18, 
     m[15, 10] -> -1/36, m[15, 20] -> -1/18, m[15, 21] -> -1/18, 
     m[15, 24] -> 1/108, m[16, 6] -> -1/2, m[16, 9] -> 1, m[16, 10] -> -1/2, 
     m[16, 20] -> -1, m[16, 21] -> -1, m[16, 24] -> 1/6, m[17, 6] -> 1/2, 
     m[17, 7] -> 2, m[17, 9] -> -1, m[17, 10] -> 1/2, m[17, 11] -> -2, 
     m[17, 20] -> 1, m[17, 21] -> 1, m[17, 24] -> -1/6, m[18, 6] -> -1/2, 
     m[18, 7] -> -2, m[18, 9] -> 1, m[18, 10] -> -1/2, m[18, 11] -> 2, 
     m[18, 20] -> -1, m[18, 21] -> -1, m[18, 24] -> 1/6, m[19, 1] -> 9/2, 
     m[19, 2] -> 5/3, m[19, 3] -> 1/2, m[19, 4] -> 5/3, m[19, 5] -> -1, 
     m[19, 7] -> -3/2, m[19, 9] -> 1, m[19, 10] -> 1/2, m[19, 11] -> 11/6, 
     m[19, 12] -> 1, m[19, 13] -> -1, m[19, 14] -> -1, m[19, 15] -> 11/6, 
     m[19, 16] -> -1, m[19, 17] -> 1, m[19, 18] -> -1, m[19, 19] -> 1, 
     m[19, 22] -> 1, m[19, 23] -> 1, m[19, 24] -> 1, m[19, 25] -> -5/3, 
     m[19, 26] -> 1, m[19, 28] -> -1, m[19, 29] -> 1, m[19, 30] -> -1, 
     m[20, 1] -> -9/2, m[20, 2] -> -5/3, m[20, 4] -> -5/3, m[20, 5] -> 1, 
     m[20, 6] -> -1, m[20, 7] -> 3/2, m[20, 8] -> -1, m[20, 9] -> 1, 
     m[20, 10] -> -1, m[20, 11] -> -11/6, m[20, 12] -> -1, m[20, 13] -> 1, 
     m[20, 14] -> 1, m[20, 15] -> -11/6, m[20, 16] -> 1, m[20, 17] -> -1, 
     m[20, 18] -> 1, m[20, 19] -> -1, m[20, 20] -> -2, m[20, 21] -> -2, 
     m[20, 24] -> -1/2, m[20, 25] -> 5/3, m[20, 26] -> -1, m[20, 27] -> 1, 
     m[20, 28] -> 1, m[20, 29] -> -1}], MySparseMatrix[{20, 30}, {}]}}, 
 {1, pflip}, {f[1] -> (y[1]^2*y[2]^2*y[4])/y[3], 
  f[8] -> (y[1]^2*y[2]^2*y[4]*y[6])/(y[3]*y[5]^2), 
  f[9] -> y[1]^2*y[2]^2*y[4]*y[7], 
  f[2] -> y[1]^2*y[2]^2*y[4] - (y[1]^2*y[2]^2*y[3]*y[4]^2*y[8])/y[9], 
  f[10] -> y[1]^2*y[2]^3*y[3]*y[4], 
  f[7] -> (y[1]^2*y[2]^3*y[4]*y[10])/(y[3]*y[5]^3), 
  f[5] -> (y[1]^2*y[2]^2*y[3]^2*y[4]^3*y[8]^2)/y[9]^2 + 
    (2*y[1]^2*y[2]^3*y[3]*y[4]^2*y[8])/y[9] + (y[1]^2*y[2]^4*y[4]^2*y[12])/
     (y[3]*y[5]^2*y[11]^2) + (y[1]^2*y[2]^7*y[3]*y[4]^3*y[13]^2)/
     (y[5]*y[11]^2*y[14]^2) + (2*y[1]^2*y[2]^5*y[4]^2*y[13]*y[15])/
     (y[5]^2*y[11]^2*y[14]), 
  f[3] -> (y[1]^2*y[2]*y[4]*y[16])/(y[3]*y[5]*y[11]) - 
    (2*y[1]^2*y[2]*y[4]^2*y[13]^2*y[17]^3*y[18])/(y[19]*y[20]^2) - 
    (4*y[1]^2*y[2]^4*y[3]*y[4]^4*y[8]^3*y[17])/(y[20]*y[21]^2) - 
    (3*y[1]^2*y[2]^2*y[3]^2*y[4]^2*y[5]*y[8]^2)/(y[9]*y[22]) + 
    (y[1]^2*y[2]^3*y[4]^2*y[13]^2*y[24])/(y[22]*y[23]) - 
    (3*y[1]^2*y[2]^6*y[4]^2*y[13]^2)/(y[5]*y[11]*y[14]*y[25]) - 
    (y[1]^2*y[2]^3*y[4]^2*y[5]^2*y[8]^3*y[27]*y[28])/(y[20]^2*y[25]*y[26]) + 
    (y[1]^2*y[2]^3*y[4]^3*y[8]^2*y[17]*y[29])/(y[5]*y[20]^2*y[21]), 
  f[6] -> (y[1]^2*y[2]^5*y[3]^2*y[4]^4*y[8]^3)/y[21]^3 + 
    (3*y[1]^2*y[2]^3*y[4]^2*y[8]*y[17]^2)/(2*y[5]^2*y[21]) - 
    (3*y[1]^2*y[2]^3*y[4]^2*y[30])/(4*y[3]*y[5]^2) - 
    (3*y[1]^2*y[2]^4*y[3]*y[4]^3*y[8]^2*y[31])/(4*y[5]*y[21]^2), 
  f[4] -> (y[1]^2*y[2]^2*y[3]^2*y[4]^3*y[8]^2)/y[9]^2 + 
    (2*y[1]^2*y[2]^3*y[3]*y[4]^2*y[8])/y[9] - y[1]^2*y[2]^2*y[4]*y[32], 
  f[11] -> y[1]^2*y[2]^2*y[4], f[15] -> (y[1]^2*y[2]^2*y[4])/y[17], 
  f[12] -> (y[1]^2*y[2]^3*y[4]^2*y[8]*y[33])/y[34], 
  f[13] -> (y[1]^2*y[2]*y[3]^3*y[4]^2*y[13]^2*y[20])/(y[18]^2*y[19]) + 
    y[1]^2*y[2]*y[4]^2*y[5]*y[23] - (2*y[1]^2*y[2]^4*y[3]*y[4]^2*y[5]*y[8]^3*
      y[17])/(y[18]*y[34]^2) + (y[1]^2*y[2]^3*y[4]^2*y[8]^3*y[36])/
     (2*y[18]^2*y[35]) - (y[1]^2*y[2]^3*y[3]*y[4]^2*y[5]*y[8]^2*y[37])/
     (2*y[18]^2*y[34]) + (y[1]^2*y[2]*y[4]^2*y[38])/(2*y[17]), 
  f[14] -> (y[1]^2*y[2]^5*y[4]^2*y[8]^3*y[17]^2)/y[34]^3 + 
    (3*y[1]^2*y[2]^4*y[4]^2*y[8]^2*y[17]*y[33])/(4*y[34]^2), 
  f[25] -> (y[1]^2*y[2]^3*y[4]^2*y[7])/(y[5]*y[17]), 
  f[19] -> (y[1]^2*y[2]^4*y[4]^2)/(y[17]*y[39]) - 
    (y[1]^2*y[2]^5*y[4]^2*y[13])/(y[5]*y[39]*y[40]), 
  f[16] -> (y[1]^2*y[2]^2*y[4])/(y[3]*y[5]), 
  f[18] -> (y[1]^2*y[2]^3*y[4]^2*y[8]*y[41])/(y[5]*y[21]), 
  f[17] -> (y[1]^2*y[2]^2*y[4])/(y[3]*y[17]), 
  f[26] -> (y[1]^2*y[2]^2*y[3]*y[4]^3*y[17]*y[42])/y[5], 
  f[24] -> (y[1]^2*y[2]^4*y[3]*y[4]^2)/(y[5]*y[17]), 
  f[22] -> (y[1]^2*y[2]^7*y[4]^2*y[13]^2*y[17])/(y[5]*y[39]^2*y[40]^2) - 
    (y[1]^2*y[2]^4*y[4]^2*y[43])/(y[17]*y[39]^2) + 
    (2*y[1]^2*y[2]^5*y[4]^2*y[13]*y[44])/(y[5]*y[39]^2*y[40]), 
  f[20] -> (y[1]^2*y[2]^4*y[4]^2*y[8]^3*y[13]^2)/(y[18]^2*y[19]*y[20]^2) + 
    (2*y[1]^2*y[2]^4*y[3]*y[4]^4*y[8]^3*y[17])/(y[20]*y[21]^2) + 
    (2*y[1]^2*y[2]^4*y[3]*y[4]^2*y[5]*y[8]^3*y[17])/(y[18]*y[34]^2) + 
    (3*y[1]^2*y[2]^3*y[4]^2*y[45])/(2*y[3]*y[5]*y[17]*y[39]) + 
    (3*y[1]^2*y[2]^6*y[4]^2*y[13]^2)/(2*y[5]*y[39]*y[40]*y[46]) - 
    (y[1]^2*y[2]^3*y[4]^2*y[8]^3*y[47]*y[48])/(2*y[18]^2*y[35]*y[46]) + 
    (y[1]^2*y[2]^3*y[3]*y[4]^2*y[5]*y[8]^2*y[49])/(2*y[18]^2*y[34]) - 
    (y[1]^2*y[2]^3*y[4]^3*y[5]*y[8]^3*y[50])/(2*y[20]^2*y[26]) - 
    (y[1]^2*y[2]^3*y[4]^3*y[8]^2*y[17]*y[51])/(2*y[5]*y[20]^2*y[21]), 
  f[23] -> (y[1]^2*y[2]^5*y[4]^2*y[8]^3*y[17]^2)/y[34]^3 - 
    (3*y[1]^2*y[2]^7*y[4]^2*y[13]^2*y[17])/(4*y[5]*y[39]^2*y[40]^2) - 
    (3*y[1]^2*y[2]^5*y[4]^2*y[13]*y[44])/(2*y[5]*y[39]^2*y[40]) + 
    (3*y[1]^2*y[2]^3*y[4]^2*y[52])/(4*y[5]*y[39]^2) + 
    (3*y[1]^2*y[2]^4*y[3]*y[4]^2*y[8]^2*y[17]*y[53])/(4*y[34]^2) + 
    (3*y[1]^2*y[2]^3*y[3]*y[4]^2*y[8]*y[54])/(2*y[34]), 
  f[21] -> (y[1]^2*y[2]^5*y[3]^2*y[4]^4*y[8]^3)/y[21]^3 - 
    (3*y[1]^2*y[2]^4*y[3]*y[4]^3*y[8]^2*y[17]*y[42])/(4*y[5]*y[21]^2) + 
    (3*y[1]^2*y[2]^3*y[3]*y[4]^3*y[8]*y[17]*y[42])/(2*y[5]*y[21]), 
  f[33] -> (y[1]^2*y[2]^2*y[4]^2*y[55])/y[17], 
  f[34] -> (y[1]^2*y[2]^2*y[4]^2*y[56])/y[5]^2, 
  f[27] -> (y[1]^2*y[2]^2*y[4]^2)/y[5] - (y[1]^2*y[2]^2*y[4]^2*y[8]*y[17])/
     (y[5]*y[57]), f[35] -> (y[1]^2*y[2]^3*y[4]^3*y[17])/y[5]^3, 
  f[31] -> (y[1]^2*y[2]^7*y[4]^2*y[13]^2*y[17])/(y[5]*y[39]^2*y[40]^2) - 
    (y[1]^2*y[2]^2*y[4]^2*y[8]^2*y[17]^2)/(y[5]*y[57]^2) - 
    (2*y[1]^2*y[2]^3*y[4]^3*y[8]*y[17])/(y[5]^2*y[57]) - 
    (y[1]^2*y[2]^4*y[4]^2*y[58])/(y[5]*y[17]*y[39]^2) + 
    (2*y[1]^2*y[2]^5*y[4]^2*y[13]*y[59])/(y[5]^2*y[39]^2*y[40]), 
  f[28] -> (y[1]^2*y[2]*y[3]^3*y[4]^2*y[13]^2*y[20])/(y[18]^2*y[19]) - 
    (2*y[1]^2*y[2]^4*y[3]*y[4]^2*y[5]*y[8]^3*y[17])/(y[18]*y[34]^2) - 
    (3*y[1]^2*y[2]^6*y[4]^2*y[13]^2)/(2*y[5]*y[39]*y[40]*y[46]) + 
    (y[1]^2*y[2]^3*y[4]^2*y[8]^3*y[47]*y[48])/(2*y[18]^2*y[35]*y[46]) - 
    (y[1]^2*y[2]*y[4]^2*y[60])/(2*y[5]*y[17]*y[39]) - 
    (3*y[1]^2*y[2]^2*y[4]^3*y[8]^2*y[17]^2)/(2*y[5]*y[57]*y[61]) - 
    (y[1]^2*y[2]^3*y[4]^3*y[13]^2*y[62])/(2*y[23]*y[61]) + 
    (y[1]^2*y[2]^3*y[3]*y[4]^2*y[5]*y[8]^2*y[63])/(2*y[18]^2*y[34]), 
  f[32] -> (y[1]^2*y[2]^3*y[3]^2*y[4]^2)/y[17], 
  f[30] -> (y[1]^2*y[2]^5*y[4]^2*y[8]^3*y[17]^2)/y[34]^3 + 
    (3*y[1]^2*y[2]^3*y[3]^2*y[4]^2*y[8])/(2*y[34]) - 
    (3*y[1]^2*y[2]^3*y[4]^2*y[64])/(4*y[17]) - 
    (3*y[1]^2*y[2]^4*y[4]^2*y[8]^2*y[17]*y[65])/(4*y[34]^2), 
  f[29] -> (y[1]^2*y[2]^2*y[4]^2*y[8]^2*y[17]^2)/(y[5]*y[57]^2) + 
    (2*y[1]^2*y[2]^3*y[4]^3*y[8]*y[17])/(y[5]^2*y[57]) - 
    (y[1]^2*y[2]^2*y[4]^2*y[66])/y[5]^2, 
  f[36] -> (y[1]^2*y[2]^2*y[4]^2)/y[5], 
  f[37] -> (y[1]^2*y[2]*y[4]^2*y[13]^2*y[17]^3*y[18])/(y[19]*y[20]^2) + 
    (2*y[1]^2*y[2]^4*y[3]*y[4]^4*y[8]^3*y[17])/(y[20]*y[21]^2) + 
    y[1]^2*y[2]*y[4]^2*y[5]*y[23] - (y[1]^2*y[2]^3*y[4]^3*y[5]*y[8]^3*y[50])/
     (2*y[20]^2*y[26]) + (y[1]^2*y[2]*y[4]^2*y[67])/(2*y[3]*y[5]) + 
    (y[1]^2*y[2]^3*y[4]^3*y[8]^2*y[17]*y[68])/(2*y[5]*y[20]^2*y[21]), 
  f[38] -> (y[1]^2*y[2]^5*y[3]^2*y[4]^4*y[8]^3)/y[21]^3 + 
    (3*y[1]^2*y[2]^4*y[3]*y[4]^3*y[8]^2*y[41])/(4*y[5]*y[21]^2), 
  f[45] -> y[1]^2*y[2]^2*y[4]*y[69], 
  f[39] -> (y[1]^2*y[2]^4*y[4]^2)/(y[3]*y[5]*y[11]) + 
    (y[1]^2*y[2]^5*y[4]^2*y[13])/(y[5]*y[11]*y[14]), 
  f[46] -> y[1]^2*y[2]^2*y[3]*y[4]^2*y[17]*y[42], 
  f[44] -> (y[1]^2*y[2]^4*y[4]^2*y[17])/(y[3]*y[5]^3), 
  f[40] -> (y[1]^2*y[2]^7*y[3]*y[4]^3*y[13]^2)/(y[5]*y[11]^2*y[14]^2) + 
    (y[1]^2*y[2]^4*y[4]^2*y[70])/(y[3]*y[5]^2*y[11]^2) + 
    (2*y[1]^2*y[2]^5*y[4]^2*y[13]*y[71])/(y[5]^2*y[11]^2*y[14]), 
  f[43] -> (y[1]^2*y[2]^4*y[4]^2*y[8]^3*y[13]^2)/(y[18]^2*y[19]*y[20]^2) + 
    (2*y[1]^2*y[2]^4*y[3]*y[4]^4*y[8]^3*y[17])/(y[20]*y[21]^2) + 
    (3*y[1]^2*y[2]^6*y[4]^2*y[13]^2)/(2*y[5]*y[11]*y[14]*y[25]) + 
    (y[1]^2*y[2]^3*y[4]^2*y[5]^2*y[8]^3*y[27]*y[28])/
     (2*y[20]^2*y[25]*y[26]) + (2*y[1]^2*y[2]^4*y[3]*y[4]^2*y[5]*y[8]^3*
      y[17])/(y[18]*y[34]^2) - (y[1]^2*y[2]^3*y[4]^2*y[8]^3*y[36])/
     (2*y[18]^2*y[35]) - (3*y[1]^2*y[2]^3*y[4]*y[72])/
     (2*y[3]*y[5]*y[11]*y[17]) + (y[1]^2*y[2]^3*y[3]*y[4]^2*y[5]*y[8]^2*
      y[73])/(2*y[18]^2*y[34]) - (y[1]^2*y[2]^3*y[4]^3*y[8]^2*y[17]*y[74])/
     (2*y[5]*y[20]^2*y[21]), 
  f[41] -> (y[1]^2*y[2]^7*y[3]*y[4]^3*y[13]^2)/(y[5]*y[11]^2*y[14]^2) + 
    (4*y[1]^2*y[2]^5*y[3]^2*y[4]^4*y[8]^3)/(3*y[21]^3) + 
    (2*y[1]^2*y[2]^5*y[4]^2*y[13]*y[71])/(y[5]^2*y[11]^2*y[14]) + 
    (y[1]^2*y[2]^3*y[4]*y[75])/(y[5]*y[11]^2) - 
    (y[1]^2*y[2]^4*y[3]*y[4]^3*y[8]^2*y[17]*y[76])/(y[5]*y[21]^2) + 
    (2*y[1]^2*y[2]^3*y[4]^2*y[8]*y[17]*y[77])/(y[5]^2*y[21]), 
  f[42] -> (y[1]^2*y[2]^5*y[4]^2*y[8]^3*y[17]^2)/y[34]^3 + 
    (3*y[1]^2*y[2]^4*y[3]*y[4]^2*y[8]^2*y[17]*y[42])/(4*y[34]^2) + 
    (3*y[1]^2*y[2]^3*y[3]*y[4]^2*y[8]*y[17]*y[42])/(2*y[34])}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> 1 + ex[2], y[4] -> ex[3], 
  y[5] -> 1 + ex[3], y[6] -> -455 + 18*ex[2] + 36*ex[2]^2 - 910*ex[3] + 
    18*ex[2]*ex[3] + 72*ex[2]^2*ex[3] - 455*ex[3]^2, y[7] -> -1 + 2*ex[2], 
  y[8] -> ex[5], y[9] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4] + ex[3]*ex[5], 
  y[10] -> 1 + 2*ex[2] + ex[2]^2 + 2*ex[3] + 6*ex[2]*ex[3] + 
    3*ex[2]^2*ex[3] + ex[3]^2 + 4*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2, 
  y[11] -> 1 + ex[2] + ex[3], y[12] -> 4 + 6*ex[2] + 2*ex[2]^2 + 8*ex[3] + 
    11*ex[2]*ex[3] + 4*ex[2]^2*ex[3] + 4*ex[3]^2 + 5*ex[2]*ex[3]^2, 
  y[13] -> -1 + ex[5], y[14] -> -(ex[2]*ex[3]) + ex[4] + ex[2]*ex[4] + 
    ex[3]*ex[4] + ex[2]*ex[3]*ex[5], 
  y[15] -> 2 + 3*ex[2] + ex[2]^2 + 4*ex[3] + 6*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] + 2*ex[3]^2 + 3*ex[2]*ex[3]^2, 
  y[16] -> -6*ex[2] - 3*ex[2]^2 + 3*ex[2]^3 + 2*ex[3] - 8*ex[2]*ex[3] + 
    2*ex[2]^2*ex[3] + 6*ex[2]^3*ex[3] + 6*ex[3]^2 + 4*ex[2]*ex[3]^2 + 
    7*ex[2]^2*ex[3]^2 + 6*ex[3]^3 + 8*ex[2]*ex[3]^3 + 2*ex[2]^2*ex[3]^3 + 
    2*ex[3]^4 + 2*ex[2]*ex[3]^4, y[17] -> 1 + ex[3] + ex[2]*ex[3], 
  y[18] -> 1 + ex[2] - ex[5], y[19] -> 1 + ex[4] - ex[5], 
  y[20] -> 1 + ex[3] + ex[2]*ex[3] - ex[5] - ex[3]*ex[5], 
  y[21] -> ex[2] + ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] - ex[2]*ex[5], y[22] -> ex[2] + ex[5], y[23] -> ex[4], 
  y[24] -> 2*ex[2]*ex[3] + 3*ex[5] + 2*ex[3]*ex[5], 
  y[25] -> ex[2] + ex[5] + ex[3]*ex[5], 
  y[26] -> ex[2]*ex[3] - ex[4] - ex[3]*ex[4], 
  y[27] -> -1 + ex[5] + ex[3]*ex[5], 
  y[28] -> 3 + 3*ex[3] + ex[2]*ex[3] - 3*ex[5] - 5*ex[3]*ex[5] - 
    2*ex[3]^2*ex[5], y[29] -> 9 + 18*ex[3] + 12*ex[2]*ex[3] + 9*ex[3]^2 + 
    12*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 - 9*ex[5] - 20*ex[3]*ex[5] - 
    7*ex[2]*ex[3]*ex[5] - 13*ex[3]^2*ex[5] - 9*ex[2]*ex[3]^2*ex[5] - 
    2*ex[3]^3*ex[5] - 2*ex[2]*ex[3]^3*ex[5], 
  y[30] -> 3 + 3*ex[3] + 2*ex[2]*ex[3], y[31] -> 3 + 3*ex[3] + ex[2]*ex[3], 
  y[32] -> 1 + 2*ex[2], y[33] -> -1 + ex[2], 
  y[34] -> ex[2]*ex[3] + ex[2]^2*ex[3] - ex[4] - ex[2]*ex[4] - ex[3]*ex[4] - 
    ex[2]*ex[3]*ex[4] + ex[2]*ex[5], y[35] -> ex[2] - ex[4], 
  y[36] -> 1 + 3*ex[2] - 3*ex[5] - 2*ex[3]*ex[5], 
  y[37] -> -3 + 3*ex[2]^2 + ex[5] - ex[2]*ex[5] - 2*ex[3]*ex[5] - 
    2*ex[2]*ex[3]*ex[5], y[38] -> -2 - 2*ex[2] - 9*ex[2]^2 - 4*ex[3] - 
    8*ex[2]*ex[3] - 2*ex[2]^2*ex[3] - 2*ex[3]^2 - 6*ex[2]*ex[3]^2 - 
    4*ex[2]^2*ex[3]^2 + 5*ex[2]*ex[5] + 9*ex[2]*ex[3]*ex[5] + 
    5*ex[2]^2*ex[3]*ex[5] + 4*ex[2]*ex[3]^2*ex[5] + 4*ex[2]^2*ex[3]^2*ex[5], 
  y[39] -> -1 + ex[2] - ex[3], y[40] -> -ex[2] - ex[2]*ex[3] + ex[4] - 
    ex[2]*ex[4] + ex[3]*ex[4] + ex[2]*ex[5] + ex[2]*ex[3]*ex[5], 
  y[41] -> -1 - ex[3] + ex[2]*ex[3], y[42] -> 1 + 2*ex[3], 
  y[43] -> -2 - ex[2] + 2*ex[2]^2 - 2*ex[3] - 3*ex[2]*ex[3], 
  y[44] -> -1 - ex[2] + ex[2]^2 - ex[3] - 2*ex[2]*ex[3], 
  y[45] -> 3 - 2*ex[2] + ex[2]^2 + 3*ex[3] + ex[2]*ex[3] + ex[2]^2*ex[3], 
  y[46] -> ex[2] - ex[5] - ex[3]*ex[5], y[47] -> 1 + ex[3]*ex[5], 
  y[48] -> 3 + ex[2] - ex[5] + 2*ex[3]*ex[5], 
  y[49] -> -3 - 6*ex[2] - 3*ex[2]^2 + 6*ex[3] + 12*ex[2]*ex[3] + 
    6*ex[2]^2*ex[3] + ex[5] + 5*ex[2]*ex[5] - 8*ex[3]*ex[5] - 
    8*ex[2]*ex[3]*ex[5], y[50] -> 1 + ex[3] + 3*ex[2]*ex[3] - ex[5] + 
    ex[3]*ex[5] + 2*ex[3]^2*ex[5], y[51] -> 3 + 12*ex[3] + 6*ex[2]*ex[3] + 
    15*ex[3]^2 + 18*ex[2]*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 6*ex[3]^3 + 
    12*ex[2]*ex[3]^3 + 6*ex[2]^2*ex[3]^3 - 3*ex[5] - 14*ex[3]*ex[5] - 
    7*ex[2]*ex[3]*ex[5] - 19*ex[3]^2*ex[5] - 15*ex[2]*ex[3]^2*ex[5] - 
    8*ex[3]^3*ex[5] - 8*ex[2]*ex[3]^3*ex[5], 
  y[52] -> -1 - 2*ex[2] + 2*ex[2]^2 - ex[3] - 3*ex[2]*ex[3], 
  y[53] -> -1 + 2*ex[3], y[54] -> ex[2] + ex[3] - ex[2]*ex[3] + 2*ex[3]^2 + 
    2*ex[2]*ex[3]^2, y[55] -> 1 + ex[2] + 2*ex[2]^2, 
  y[56] -> -1 - ex[3] + 2*ex[2]*ex[3], 
  y[57] -> ex[2]*ex[3] - ex[3]*ex[4] + ex[5] + ex[3]*ex[5], 
  y[58] -> -4 + ex[2] + 2*ex[2]^2 - 8*ex[3] - 4*ex[2]*ex[3] + 
    4*ex[2]^2*ex[3] - 4*ex[3]^2 - 5*ex[2]*ex[3]^2, 
  y[59] -> -2 + ex[2]^2 - 4*ex[3] - 3*ex[2]*ex[3] + 2*ex[2]^2*ex[3] - 
    2*ex[3]^2 - 3*ex[2]*ex[3]^2, y[60] -> -2 + 8*ex[2] - 6*ex[2]^2 + 
    3*ex[2]^3 - 8*ex[3] + 16*ex[2]*ex[3] - 7*ex[2]^2*ex[3] + 
    6*ex[2]^3*ex[3] - 12*ex[3]^2 + 6*ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2 - 
    8*ex[3]^3 - 4*ex[2]*ex[3]^3 + 2*ex[2]^2*ex[3]^3 - 2*ex[3]^4 - 
    2*ex[2]*ex[3]^4, y[61] -> ex[2]*ex[3] + ex[5] + ex[3]*ex[5], 
  y[62] -> 2*ex[2]*ex[3] - ex[5] + 2*ex[3]*ex[5], 
  y[63] -> 9 + 12*ex[2] + 3*ex[2]^2 - 7*ex[5] - 5*ex[2]*ex[5] + 
    2*ex[3]*ex[5] + 2*ex[2]*ex[3]*ex[5], y[64] -> 3 + 2*ex[2], 
  y[65] -> 3 + ex[2], y[66] -> 1 + ex[3] + 2*ex[2]*ex[3], 
  y[67] -> -2 - 4*ex[2] - 11*ex[2]^2 - 4*ex[3] - 10*ex[2]*ex[3] - 
    6*ex[2]^2*ex[3] - 2*ex[3]^2 - 6*ex[2]*ex[3]^2 - 4*ex[2]^2*ex[3]^2 - 
    ex[2]*ex[5] - ex[2]^2*ex[5] + 3*ex[2]*ex[3]*ex[5] + 
    3*ex[2]^2*ex[3]*ex[5] + 4*ex[2]*ex[3]^2*ex[5] + 4*ex[2]^2*ex[3]^2*ex[5], 
  y[68] -> -3 - 6*ex[3] - 3*ex[3]^2 + 3*ex[2]^2*ex[3]^2 + 3*ex[5] + 
    8*ex[3]*ex[5] + ex[2]*ex[3]*ex[5] + 7*ex[3]^2*ex[5] + 
    3*ex[2]*ex[3]^2*ex[5] + 2*ex[3]^3*ex[5] + 2*ex[2]*ex[3]^3*ex[5], 
  y[69] -> -1 - 2*ex[3] + 2*ex[2]*ex[3], 
  y[70] -> 2 + 2*ex[2] + 4*ex[3] + 5*ex[2]*ex[3] + 2*ex[2]^2*ex[3] + 
    2*ex[3]^2 + 3*ex[2]*ex[3]^2, y[71] -> 1 + ex[2] + 2*ex[3] + 
    3*ex[2]*ex[3] + ex[2]^2*ex[3] + ex[3]^2 + 2*ex[2]*ex[3]^2, 
  y[72] -> 3 + 3*ex[2] + 6*ex[3] + 4*ex[2]*ex[3] + 3*ex[3]^2 + 
    ex[2]*ex[3]^2 + ex[2]^2*ex[3]^2, 
  y[73] -> 3 + 6*ex[2] + 3*ex[2]^2 + 6*ex[3] + 12*ex[2]*ex[3] + 
    6*ex[2]^2*ex[3] - 5*ex[5] - ex[2]*ex[5] - 8*ex[3]*ex[5] - 
    8*ex[2]*ex[3]*ex[5], y[74] -> 9 + 24*ex[3] + 18*ex[2]*ex[3] + 
    21*ex[3]^2 + 30*ex[2]*ex[3]^2 + 9*ex[2]^2*ex[3]^2 + 6*ex[3]^3 + 
    12*ex[2]*ex[3]^3 + 6*ex[2]^2*ex[3]^3 - 9*ex[5] - 26*ex[3]*ex[5] - 
    13*ex[2]*ex[3]*ex[5] - 25*ex[3]^2*ex[5] - 21*ex[2]*ex[3]^2*ex[5] - 
    8*ex[3]^3*ex[5] - 8*ex[2]*ex[3]^3*ex[5], 
  y[75] -> 1 + ex[2] + 2*ex[3] + 4*ex[2]*ex[3] + 2*ex[2]^2*ex[3] + ex[3]^2 + 
    3*ex[2]*ex[3]^2, y[76] -> 3 + 2*ex[3], 
  y[77] -> 1 + 4*ex[3] + 4*ex[2]*ex[3] + 5*ex[3]^2 + 5*ex[2]*ex[3]^2 + 
    2*ex[3]^3 + 2*ex[2]*ex[3]^3}}
