ACLOCAL_AMFLAGS = -I m4

AM_CXXFLAGS = @NJET_CXXFLAGS@
AM_CPPFLAGS = -I$(top_srcdir) $(EIGEN_CFLAGS)

{%- set ext_precs = ["VC","DD","QD"] %}
{%- set precs = ["SD"] + ext_precs %}
{%- set precs2 = ["SD","VC","DDSD","DDDD","QD"] %}

EXTRA_LTLIBRARIES = \
    {% for prec in precs2 %}libnjet3an{{ amp }}{{ chan }}{{ prec }}.la {% endfor %}\
    {%- for hel in hels %}
    {% for prec in precs %}libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}{{ prec }}.la {% endfor %}{% if not loop.last %}\{% endif %}
    {%- endfor %}

lib_LTLIBRARIES = libnjet3an{{ amp }}{{ chan }}.la

PROC_HDRS = {{ amp }}-{{ chan }}.h \
            {%- for hel in hels %}
            {{ hel.chars }}.h {% if not loop.last %}\{% endif %}
            {%- endfor %}

libnjet3an{{ amp }}{{ chan }}_la_SOURCES = ../../blank-staticfix.cpp
libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES = \
    {{ amp }}-{{ chan }}.cpp \
    sf.cpp
EXTRA_libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES = \
    $(top_srcdir)/finrem/{{ amp }}/common/chan.h \
    $(PROC_HDRS)
libnjet3an{{ amp }}{{ chan }}SD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_SD $(PENTAGON_FUNCTIONS_CFLAGS)
libnjet3an{{ amp }}{{ chan }}SD_la_CXXFLAGS = $(AM_CXXFLAGS) -O2
libnjet3an{{ amp }}{{ chan }}VC_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}VC_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_VC $(PENTAGON_FUNCTIONS_CFLAGS)
libnjet3an{{ amp }}{{ chan }}VC_la_CXXFLAGS = $(AM_CXXFLAGS) -O2
libnjet3an{{ amp }}{{ chan }}DDSD_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}DDSD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_DD -DUSE_DDSD $(PENTAGON_FUNCTIONS_CFLAGS)
libnjet3an{{ amp }}{{ chan }}DDSD_la_CXXFLAGS = $(AM_CXXFLAGS) -O2
libnjet3an{{ amp }}{{ chan }}DDDD_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}DDDD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_DD -DUSE_DDDD $(PENTAGON_FUNCTIONS_CFLAGS)
libnjet3an{{ amp }}{{ chan }}DDDD_la_CXXFLAGS = $(AM_CXXFLAGS) -O2
libnjet3an{{ amp }}{{ chan }}QD_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}QD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_QD $(PENTAGON_FUNCTIONS_CFLAGS)
libnjet3an{{ amp }}{{ chan }}QD_la_CXXFLAGS = $(AM_CXXFLAGS) -O2
libnjet3an{{ amp }}{{ chan }}_la_LIBADD = \
    $(top_builddir)/finrem/common/libpentagons.la \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}chan.la \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}hel.la \
    libnjet3an{{ amp }}{{ chan }}SD.la
libnjet3an{{ amp }}{{ chan }}_la_DEPENDENCIES = \
    $(top_builddir)/finrem/common/libpentagons.la \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}chan.la \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}hel.la \
    libnjet3an{{ amp }}{{ chan }}SD.la
if ENABLE_VC
libnjet3an{{ amp }}{{ chan }}_la_LIBADD += libnjet3an{{ amp }}{{ chan }}VC.la
libnjet3an{{ amp }}{{ chan }}_la_DEPENDENCIES += libnjet3an{{ amp }}{{ chan }}VC.la
endif
if ENABLE_DD
libnjet3an{{ amp }}{{ chan }}_la_LIBADD += \
    libnjet3an{{ amp }}{{ chan }}DDDD.la \
    libnjet3an{{ amp }}{{ chan }}DDSD.la
libnjet3an{{ amp }}{{ chan }}_la_DEPENDENCIES += \
    libnjet3an{{ amp }}{{ chan }}DDDD.la \
    libnjet3an{{ amp }}{{ chan }}DDSD.la
endif
if ENABLE_QD
libnjet3an{{ amp }}{{ chan }}_la_LIBADD += libnjet3an{{ amp }}{{ chan }}QD.la
libnjet3an{{ amp }}{{ chan }}_la_DEPENDENCIES += libnjet3an{{ amp }}{{ chan }}QD.la
endif

fr{{ amp }}{{ chan }}includedir = $(pkgincludedir)/finrem/{{ amp }}/{{ chan }}
fr{{ amp }}{{ chan }}include_HEADERS = $(PROC_HDRS)

## {{ '{{' }} helicity libraries
{% for hel in hels %}
lib_LTLIBRARIES += libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}.la
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}_la_SOURCES = ../../blank-staticfix.cpp
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_SOURCES = \
    {{ hel.chars }}.cpp
    {%- if not hel.uhv %} \
    {%- for loop_obj in loops if loop_obj.parity_odd %}
    {%- for p in range(partials) %}
    {{ hel.chars }}-{{ loop_obj.id }}{{ p }}.cpp
    {%- if not loop.last %} \{% endif %}
    {%- endfor %}
    {%- if not loop.last %} \{% endif %}
    {%- endfor %}
    {%- endif %}
EXTRA_libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_SOURCES = \
    $(top_srcdir)/finrem/{{ amp }}/common/hel.h \
    {{ hel.chars }}.h
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_SD
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}VC_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}VC_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_VC
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}VC_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}DD_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}DD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_DD
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}DD_la_CXXFLAGS = $(AM_CXXFLAGS) -O1
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}QD_la_SOURCES = $(libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD_la_SOURCES)
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}QD_la_CPPFLAGS = $(AM_CPPFLAGS) -DUSE_QD
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}QD_la_CXXFLAGS = $(AM_CXXFLAGS) -O0
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}_la_LIBADD = \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}hel.la \
    libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD.la
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}_la_DEPENDENCIES = \
    $(top_builddir)/finrem/{{ amp }}/common/lib{{ amp }}hel.la \
    libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}SD.la
{%- for prec in ext_precs %}
if ENABLE_{{ prec }}
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}_la_LIBADD += libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}{{ prec }}.la
libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}_la_DEPENDENCIES += libnjet3an{{ amp }}{{ chan }}{{ hel.chars }}{{ prec }}.la
endif
{%- endfor %}
{% endfor %}
## {{ '}}' }}

CLEANFILES = $(EXTRA_LTLIBRARIES)

