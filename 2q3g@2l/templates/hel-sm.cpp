{%- from 'macros.jinja2' import matrix -%}
/*  
 * finrem/{{ amp }}/{{ channel }}/{{ h_char }}-{{ loop_id }}{{ partial }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ h_char }}.h"

template <typename T>
void Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::fill_m{{ loop_id }}{{ partial }}e() {
    {{ matrix(loop_id, partial, sme) }}
}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<double>;
#endif                                   
#ifdef USE_DD                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<dd_real>;
#endif                                   
#ifdef USE_QD                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<qd_real>;
#endif                                   
#ifdef USE_VC                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<Vc::double_v>;
#endif
