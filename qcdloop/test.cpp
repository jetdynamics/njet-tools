#include <iomanip>
#include <iostream>
#include <qcdloop/qcdloop.h>
#include <qd/dd_real.h>

template <typename T> void run() {
  T mu2{2.0};
  std::vector<std::complex<T>> res(3);
  std::vector<T> m({5.0, 3.0, 1.0});
  std::vector<T> p({2.0, 1.0, 6.0});
  ql::QCDLoop<std::complex<T>, T, T> qcdloop;
  qcdloop.integral(res, mu2, m, p);
  for (std::complex<T> e : res) {
    std::cout << e << ' ';
  }
  std::cout << '\n';
}

// GNU quadmath
template <> void run<__float128>() {
  __float128 mu2{2.0};
  std::vector<__complex128> res(3);
  std::vector<__float128> m({5.0, 3.0, 1.0});
  std::vector<__float128> p({2.0, 1.0, 6.0});
  ql::QCDLoop<__complex128, __float128, __float128> qcdloop;
  qcdloop.integral(res, mu2, m, p);
  for (__complex128 e : res) {
    std::cout << e << ' ';
  }
  std::cout << '\n';
}

// // interface doesn't support QD
// template <> void run<dd_real>() {
//   dd_real mu2{2.0};
//   std::vector<std::complex<dd_real>> res(3);
//   std::vector<dd_real> m({5.0, 3.0, 1.0});
//   std::vector<dd_real> p({2.0, 1.0, 6.0});
//   ql::QCDLoop<std::complex<dd_real>, dd_real, dd_real> qcdloop;
//   qcdloop.integral(res, mu2, m, p);
//   for (std::complex<dd_real> e : res) {
//     std::cout << e << ' ';
//   }
//   std::cout << '\n';
// }

int main() {
  std::cout << std::scientific << std::setprecision(16);
  run<double>();
  run<__float128>();
}
