// Run as ./generateMomenta.cpp <phase space points output file>
// e.g. ./generateMomenta points.dat

#include "../common/generateMomenta.hpp"

int main(int argc, char** argv)
{
    std::cout << std::endl;

    assert(argc == 2);
    const std::string resultsFile { argv[1] };

    const unsigned int num { 10 };
    const std::array<int, num> rseeds { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    genMom::genMomFile<num, 4>(rseeds, resultsFile);

    return 0;
}
