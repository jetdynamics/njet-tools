// Run as ./calcPoints.cpp <phase space points file> <output file>
// e.g. ./calcPoints points.mom

#include "../common/calcPoints.hpp"

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    assert(argc == 3);
    const std::string momFile { argv[1] };
    const std::string resultsFile { argv[2] };
    const std::string contract { "OLE_contract_diphoton.lh" };

    const int legs { 5 };
    calcPoints<legs> cp(contract, momFile, resultsFile);
    cp.calcLoop();

    return 0;
}
