// Run as ./parPoints.cpp <phase space points file> <number of point to run>
// e.g. ./parPoints points.mom 2

#include "../common/calcPoints.hpp"

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    assert(argc == 3);
    const std::string momFile { argv[1] };
    const std::string numS { argv[2] };
    const int num = std::stoi(argv[2]);
    const std::string resultsFile{momFile.substr(0, momFile.length() - 4) + "." + numS + ".part"};
    const std::string contract { "OLE_contract_diphoton.lh" };

    const int legs { 5 };
    calcPoints<legs> cp(contract, momFile, resultsFile);
    cp.calcSingle(num);

    return 0;
}
