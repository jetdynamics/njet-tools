#!/usr/bin/env bash

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]
then
  echo Use as ./gather.sh [phase space points file] [number of phase space points] [output file]
  echo Omit last option to print to terminal instead of file
else
  if [ "$3" != "" ]
  then
    echo Appending data to file "$3"
  fi

  for ((i = 0; i < "$2"; i++))
  do
    file=${1%.*}.${i}.part

    if [ "$3" == "" ]
    then
      cat $file
    else
      cat $file >> "$3"
    fi
  done
fi
