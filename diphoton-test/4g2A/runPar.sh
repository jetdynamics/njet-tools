#!/usr/bin/env bash
# To run jobs on the batch, do:
# ./runPar.sh [phase space points file] [number of phase space points]

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]
then
  echo "Use as ./runPar.sh <phase space points file> <number of phase space points>"
else
  make

  for ((i = 0; i < "$2"; i++))
  do
    par=4g2A_${1%.*}_${i}
    name=${par}
    sed "s/%j/${par}/g" runPar.sbatch > $name
    sbatch $name "$1" $i
    rm -f $name
  done
fi
