// Run as ./parPoints.cpp <phase space points file> <number of point to run>
// e.g. ./parPoints points.mom 2

#include "../common/calcPoints.hpp"

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    assert(argc == 3 || argc == 4);

    const std::string momFile { argv[1] };
    const std::string numS { argv[2] };
    const unsigned int num = std::stoi(argv[2]);
    const std::string contract { "OLE_contract_diphoton.lh" };

    const unsigned int legs { 6 };

    if(argc==3){
        const std::string resultsFile{momFile.substr(0, momFile.length() - 4) + "." + numS + ".part"};
        calcPoints<legs> cp(contract, momFile, resultsFile);
        cp.calcSingle(num);
    } else {
        const int end { std::atoi(argv[3]) };
	for(int i = num;i<end;++i){
	    const std::string resultsFile{momFile.substr(0, momFile.length() - 4) + "." + std::to_string(i) + ".part"};
            calcPoints<legs> cp(contract, momFile, resultsFile);
	    cp.calcSingle(i);
	}
    }
}
