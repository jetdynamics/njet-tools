// Run as ./generateMomenta.cpp <phase space points output file>
// e.g. ./generateMomenta points.dat

#include "../common/generateMomenta.hpp"

int main(int argc, char** argv)
{
    std::cout << std::endl;

    assert(argc == 2);
    const std::string resultsFile { argv[1] };

    const unsigned int num { 10 };
    // Omit rseed=4 as it doesn't give an easy point
    const std::array<int, num> rseeds { 1, 2, 3, 5, 6, 7, 8, 9, 10, 11 };

    genMom::genMomFile<num, 6>(rseeds, resultsFile);

    return 0;
}
