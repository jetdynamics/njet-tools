#ifndef NJET_EXAMPLES_GENERATE_MOMENTA_H
#define NJET_EXAMPLES_GENERATE_MOMENTA_H

#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <cassert>
#include <sstream>

#include "phaseSpace.hpp"

namespace genMom {

    template <int num, int mul>
        void genMomFile(const std::array<int, num> rseeds, const std::string file);

} // namespace genMom

#include "generateMomenta.ipp"

#endif //NJET_EXAMPLES_GENERATE_MOMENTA_H
