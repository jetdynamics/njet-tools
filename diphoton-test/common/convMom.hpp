#ifndef convMom_h
#define convMom_h

#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// template definitions

namespace convMom {

    template <typename T, int m>
        std::vector<std::array<std::array<T, 4>, m>> getMom(const std::string& fileName);

    template <typename T, int m>
        void printMom(const std::vector<std::array<std::array<T, 4>, m>>& all);

} // namespace convMom

// template implementations

#include "convMom.ipp"

#endif //convMom_h
