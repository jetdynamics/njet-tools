#ifndef NJET_EXAMPLES_CALCPOINTS_H
#define NJET_EXAMPLES_CALCPOINTS_H

#include <array>
#include <cassert>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "njet.h"

#include "convMom.hpp"
#include "phaseSpace.hpp"

template <int mul>
class calcPoints {
    private:
        const std::string contract, momFile, resultsFile;
        // d=4 is for length of four-momenta (i.e. 4 dimensions)
        static const int d { 4 };
        const std::vector<std::array<std::array<double, d>, mul>> momenta;
        // In BLHA the four-momenta are recorded as std::vectors of length n=5 where the last entry is the mass
        static const int n { 5 };

        std::vector<std::array<double, 3>> results;

        const double alphas, alpha, mur;

        void write();
        void calc(int pt, bool verbose = true);

    public:
        void calcSingle(int pt);
        void calcLoop(bool verbose = true);

        calcPoints(std::string contract_, std::string momFile_, std::string resultsFile_);

}; // class calcPoints

#include "calcPoints.ipp"

#endif //NJET_EXAMPLES_CALCPOINTS_H
