template <typename T, int m>
std::vector<std::array<std::array<T, 4>, m>> convMom::getMom(const std::string& fileName)
{
    std::vector<std::array<std::array<T, 4>, m>> all;
    std::array<std::array<T, 4>, m> point;

    std::ifstream data(fileName);
    std::string line;
    int j { 0 };
    std::string delimiter;
    int pos;
    std::array<T, 4> mom;

    const int dotPos { static_cast<int>(fileName.find(".")) };
    const std::string suffix {fileName.substr(dotPos + 1, fileName.length())};

    if (suffix == "dat") {
        delimiter = " ";
        
        while (std::getline(data, line)) {
            if (j != m) {
                int i { 0 };
                while (i < 4) {
                    pos = line.find(delimiter);
                    std::string comp { line.substr(0, pos) };
                    line.erase(0, pos + delimiter.length());
                    if (comp != "") {
                        mom[i++] = stod(comp);
                    }
                }
                point[j++] = mom;
            } else {
                all.push_back(point);
                j = 0;
            }
        }
    } else if (suffix == "mom") {
        delimiter = ",";

        while (std::getline(data, line)) {
            if (j != m) {
                pos = line.find("(");
                line.erase(0, pos + delimiter.length());
                pos = line.find(")");
                line.erase(pos, line.size());
                int i { 0 };
                while (i < 4) {
                    pos = line.find(delimiter);
                    std::string comp { line.substr(0, pos) };
                    line.erase(0, pos + delimiter.length());
                    if (comp != "") {
                        mom[i++] = stod(comp);
                    }
                }
                point[j++] = mom;
            } else {
                all.push_back(point);
                j = 0;
            }
        }
    }

    return all;
}

template <typename T, int m>
void convMom::printMom(const std::vector<std::array<std::array<T, 4>, m>>& all)
{
    for (const std::array<std::array<T, 4>, m>& pt : all) {
        for (const std::array<T, 4>& mom : pt) {
            for (const T& comp : mom) {
                std::cout << comp << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}
