template <int num, int mul>
void genMom::genMomFile(const std::array<int, num> rseeds, const std::string file)
{
    for (int i { 0 }; i < num; ++i) {
        std::cout << "Phase space point " << i << ":" << std::endl;

        std::vector<MOM<double>> point { ps::getMasslessPhaseSpacePoint<double>(mul, true, rseeds[i], 10.) };

        {
            std::ofstream o(file, std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);
            int j { 0 };
            for (auto mom : point) {
                // Convert from all-outgoing to 2->(mul-2) convention
                mom *= (++j < 3) ? -1 : 1;
                o << mom.x0 << "  " << mom.x1 << "  " << mom.x2 << "  " << mom.x3 << '\n';
            }
            o << '\n';
        }

        std::cout << std::endl;
    }
}
