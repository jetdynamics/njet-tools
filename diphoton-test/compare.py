#! /usr/bin/env python3

from argparse import ArgumentParser

from common.common import get_data, strip_accuracy, write_out


def cli():
    parser = ArgumentParser(
        description="Compare the results of two calculations by dividing the first result by the second for each phase "
                    "space point. Write the comparison to a file or to the terminal, including the division in the "
                    "first column and the division less unity in the second.")
    parser.add_argument(
        "input1", type=str,
        help="The name of the .dat file with the first set of results, e.g. results_1.dat.")
    parser.add_argument(
        "input2", type=str,
        help="The name of the .dat file with the second set of results, e.g. results_2.dat.")
    parser.add_argument(
        "-o", "--output", type=str,
        help="The name of the .dat file to write comparison results to, e.g. compare.dat. If not specified, the results"
             " will be printed to the terminal and not written to any file.")
    return parser.parse_args()


if __name__ == "__main__":
    args = cli()

    compare = [o / t for o, t in zip(*[strip_accuracy(get_data(getattr(args, a))) for a in ["input1", "input2"]])]
    out = [[a, abs(a - 1)] for a in compare]

    write_out(args.output, out)
