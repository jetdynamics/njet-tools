#include <algorithm>
#include <iostream>
#include <typeinfo>

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
void cf::Compare<T, Amp, RedAmp, Split, Mapping>::init_hels(const std::array<int, mulF>& hel)
{
    if (this->helCur != hel) {
        this->helCur = hel;

        std::copy_n(hel.cbegin(), o[0], this->helRm.begin());
        this->helRm[o[0]] = -1;
        std::copy(hel.cbegin() + o[0] + this->nc, hel.cend(), this->helRm.begin() + o[0] + 1);

        std::copy_n(hel.cbegin(), o[0], this->helRp.begin());
        this->helRp[o[0]] = 1;
        std::copy(hel.cbegin() + o[0] + this->nc, hel.cend(), this->helRp.begin() + o[0] + 1);

        this->helSm[0] = -1;
        std::copy_n(hel.cbegin() + o[0], this->nc, helSm.begin() + 1);

        this->helSp[0] = 1;
        std::copy_n(hel.cbegin() + o[0], this->nc, helSp.begin() + 1);
    }
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
void cf::Compare<T, Amp, RedAmp, Split, Mapping>::print(const char type, const bool sq, const bool show)
{
#ifdef DEBUG
    const int sp_ { 20 };
    const std::string nameF { flavF != "" ? flavF.substr(0, o[0]) + "[" + flavF.substr(o[0], nc) + "]" + flavF.substr(o[0] + nc) : std::to_string(mulF) + "g" };
    const std::string nameR { flavR != "" ? flavR.substr(0, o[0]) + "[" + flavR.substr(o[0], 1) + "]" + flavR.substr(o[0] + 1) : std::to_string(mulR) + "g" };
    const std::string nameS { flavS != "" ? flavS.substr(0, 1) + ">" + flavS.substr(1) : "g>" + std::to_string(mulS - 1) + "g" };
    const std::string sp(sp_, ' ');
    const std::string sp2(sp_ + 3, ' ');
    const std::string spF(sp_ - 5 - mulF - nameF.length(), ' ');
    const std::string spR(sp_ - 5 - mulR - nameR.length(), ' ');
    const std::string spS(sp_ - 3 - mulS - nameS.length(), ' ');
    const std::string hsF { cf::genHelStr(helCur) };
    const std::string hspF { hsF.substr(0, o[0]) + "[" + hsF.substr(o[0], nc) + "]" + hsF.substr(o[0] + nc) };
    const std::string hsRm { cf::genHelStr(helRm) };
    const std::string hspRm { hsRm.substr(0, o[0]) + "[" + hsRm.substr(o[0], 1) + "]" + hsRm.substr(o[0] + 1) };
    const std::string hsRp { cf::genHelStr(helRp) };
    const std::string hspRp { hsRp.substr(0, o[0]) + "[" + hsRp.substr(o[0], 1) + "]" + hsRp.substr(o[0] + 1) };
    const std::string hsSm { cf::genHelStr(helSm) };
    const std::string hspSm { hsSm.substr(0, 1) + ";" + hsSm.substr(1) };
    const std::string hsSp { cf::genHelStr(helSp) };
    const std::string hspSp { hsSp.substr(0, 1) + ";" + hsSp.substr(1) };

    if (amp_level || amp2_level) {
        if ((abs(limit.get0()) != 0.) || show) {
            if (type == 't') {
                if ((abs(ansT) != 0.) || show) {
                    std::cout << std::string(sp_ + 28 + 2 * std::cout.precision(), '=') << '\n';
                    if (((abs(ansRmT) > 0.) && (abs(ansSpT) > 0.)) || show) {
                        std::cout
                            << (sq ? "|" : "") << "amp^t_{" << nameR << "}(" << hspRm << ")" << (sq ? "|^2" : "") << spR << " = " << ansRmT << '\n'
                            << (sq ? "|" : "") << "sp^t_{" << nameS << "}(" << hspSp << ")" << (sq ? "|^2" : "") << spS << " = " << ansSpT << '\n'
                            << '\n';
                    }
                    if (((abs(ansRpT) > 0.) && (abs(ansSmT) > 0.)) || show) {
                        std::cout
                            << (sq ? "|" : "") << "amp^t_{" << nameR << "}(" << hspRp << ")" << (sq ? "|^2" : "") << spR << " = " << ansRpT << '\n'
                            << (sq ? "|" : "") << "sp^t_{" << nameS << "}(" << hspSm << ")" << (sq ? "|^2" : "") << spS << " = " << ansSmT << '\n'
                            << '\n';
                    }
                    std::cout
                        << (sq ? "|" : "") << "amp^t_{" << nameF << "}(" << hspF << ")" << (sq ? "|^2" : "") << spF << " = " << ansT << '\n'
                        << (sq ? "|" : "") << "limit^t" << (sq ? "|^2" : "") << sp << " = " << limit.get0() << '\n'
                        << '\n'
                        << "ratio" << (sq ? "2" : "") << "^t" << (sq ? sp2 : sp) << " = " << ratioLoA.get0() << '\n'
                        << '\n';
                }
            } else {
                std::cout << "====================================================================================================\n";
                if (((abs(ansRmT) > 0.) && (abs(ansSpL.get0()) > 0.)) || show) {
                    std::cout
                        << (sq ? "|" : "") << "amp^t_{" << nameR << "}(" << hspRm << ")" << (sq ? "|^2" : "") << spR << " = " << ansRmT << '\n'
                        << (sq ? "|" : "") << "sp^" << type << "_{" << nameS << "}(" << hspSp << ")" << (sq ? "|^2" : "") << spS << " = " << ansSpL << '\n'
                        << '\n';
                }
                if (((abs(ansRmL.get0()) > 0.) && (abs(ansSpT) > 0.)) || show) {
                    std::cout
                        << (sq ? "|" : "") << "amp^" << type << "_{" << nameR << "}(" << hspRm << ")" << (sq ? "|^2" : "") << spR << " = " << ansRmL << '\n'
                        << (sq ? "|" : "") << "sp^t_{" << nameS << "}(" << hspSp << ")" << (sq ? "|^2" : "") << spS << " = " << ansSpT << '\n'
                        << '\n';
                }
                if (((abs(ansRpT) > 0.) && (abs(ansSmL.get0()) > 0.)) || show) {
                    std::cout
                        << (sq ? "|" : "") << "amp^t_{" << nameR << "}(" << hspRp << ")" << (sq ? "|^2" : "") << spR << " = " << ansRpT << '\n'
                        << (sq ? "|" : "") << "sp^" << type << "_{" << nameS << "}(" << hspSm << ")" << (sq ? "|^2" : "") << spS << " = " << ansSmL << '\n'
                        << '\n';
                }
                if (((abs(ansRpL.get0()) > 0.) && (abs(ansSmT) > 0.)) || show) {
                    std::cout
                        << (sq ? "|" : "") << "amp^" << type << "_{" << nameR << "}(" << hspRp << ")" << (sq ? "|^2" : "") << spR << " = " << ansRpL << '\n'
                        << (sq ? "|" : "") << "sp^t_{" << nameS << "}(" << hspSm << ")" << (sq ? "|^2" : "") << spS << " = " << ansSmT << '\n'
                        << '\n';
                }
                std::cout
                    << (sq ? "|" : "") << "amp^" << type << "_{" << nameF << "}(" << hspF << ")" << (sq ? "|^2" : "") << spF << " = " << ansL << '\n'
                    << (sq ? "|" : "") << "limit^" << type << (sq ? "|^2" : "") << sp << " = " << limit << '\n'
                    << '\n'
                    // << "arg(amp^f_{6g})     = " << arg(ansL) << '\n'
                    // << "arg(limit)          = " << arg(limit) << '\n'
                    // << "delta(arg)          = " << arg(ansL)-arg(limit) << '\n'
                    // << "|amp^f_{6g}(" << cf::genHelStr(hel) << ")|= " << (ansL*ansL).get0() << '\n'
                    // << "|limit|             = " << (limit*limit).get0() << '\n'
                    << "ratio" << (sq ? "2" : "") << "^" << type << (sq ? sp2 : sp) << " = " << ratioLoA << '\n'
                    << '\n';
            }
        }
    } else {
        if (type == 't') {
            std::cout
                << (sq ? "|" : "") << "amp^t_{" << nameF << "}" << (sq ? "|^2" : "") << spF << std::string(4 + mulF, ' ') << " = " << ansT << '\n'
                << (sq ? "|" : "") << "limit^t" << (sq ? "|^2" : "") << sp << " = " << limit.get0() << '\n'
                << '\n'
                << "ratio" << (sq ? "2" : "") << "^t" << (sq ? sp2 : sp) << " = " << ratioLoA.get0() << '\n'
                << '\n';
        } else {
            std::cout
                << "|"
                << "amp^V_{" << nameF << "}"
                << "|^2" << spF << std::string(4 + mulF, ' ') << " = " << ansL << '\n'
                << "|"
                << "limit^V"
                << "|^2" << sp << " = " << limit << '\n'
                << '\n'
                << "ratio"
                << "2^V" << sp2 << " = " << ratioLoA << '\n'
                << '\n';
        }
    }
#else
    if (type == 't') {
        if ((abs(limit.get0()) != 0.) || show) {
            std::cout << "ratio" << (sq ? "2" : "") << "^" << type << ((amp_level || amp2_level) ? "(" + cf::genHelStr(helCur) + ")" : "") << " = " << chop(ratioLoA.get0()) << '\n';
        }
    } else {
        if ((abs(limit.get0()) != 0.) || show) {
            std::cout << "ratio" << (sq ? "2" : "") << "^" << type << ((amp_level || amp2_level) ? "(" + cf::genHelStr(helCur) + ")" : "") << " = " << chop(ratioLoA) << '\n';
        }
    }
#endif
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
cf::Compare<T, Amp, RedAmp, Split, Mapping>::Compare(const int o1, const std::array<MOM<T>, mulF>& mom)
    : Mapping(o1, mom)
    , amp(this->scale)
    , redAmp(this->scale)
    , split(this->scale)
    , flavF()
    , flavR()
    , flavS()
    , amp_level(false)
    , amp2_level(false)
    , helCur()
    , helRm()
    , helRp()
    , helSm()
    , helSp()
    , ansL()
    , ansT()
    , ansRmL()
    , ansRmT()
    , ansRpL()
    , ansRpT()
    , ansSmL()
    , ansSmT()
    , ansSpL()
    , ansSpT()
    , limit()
    , ratioLoA()
    , splitSpnMatT()
    , redAmpSpnMatT()
    , splitSpnMatV()
    , redAmpSpnMatV()
{
    amp.setMuR2(this->mur2);
    split.setMuR2(this->mur2);
    redAmp.setMuR2(this->mur2);

    amp.setMomenta(mom.data());
    split.setMomenta(this->splitPS().data(), this->recoil());
    redAmp.setMomenta(this->reducedPS().data());
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
std::complex<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::helAmpT(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    limit = ansRpT * ansSmT + ansRmT * ansSpT;

    ratioLoA = limit.get0() / ansT;

    print('t', false, show);

    return ratioLoA.get0();
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
std::complex<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::amp2T_(const bool show)
{
    // R(+)*S(-) + R(-)*S(+) (all outgoing)
    limit = redAmpSpnMatT[0] * splitSpnMatT[3] + redAmpSpnMatT[3] * splitSpnMatT[0]
        + redAmpSpnMatT[1] * splitSpnMatT[2] + redAmpSpnMatT[2] * splitSpnMatT[1];

    // limit = Eps3<T>();
    // for (int i { 0 }; i < 4; ++i) {
    //     limit += redAmpSpnMatT[i] * splitSpnMatT[3 - i];
    // }

    ratioLoA = limit.get0() / ansT;

    print('t', true, show);

    const std::complex<T> sol { ratioLoA.get0() };
    return sol;
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
std::complex<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::helAmp2T(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp2(hel);
    return this->amp2T_(show);
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
T cf::Compare<T, Amp, RedAmp, Split, Mapping>::amp2T(const bool show)
{
    init_amp2();
    const T sol { this->amp2T_(show).real() };
    return sol;
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
Eps3<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::helAmpL_(const char type, const bool show)
{
    limit = ansRpL * ansSmT + ansRmL * ansSpT + ansRpT * ansSmL + ansRmT * ansSpL;

    ratioLoA = Eps3<T>(limit.get0() / ansL.get0(), limit.get1() / ansL.get1(), limit.get2() / ansL.get2());

    print(type, false, show);

    return ratioLoA;
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
Eps3<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::amp2V_(const bool show)
{
    // where (at |amp|^2 level)
    // S = splitting amplitude
    // R = reduced amplitude
    // [n] denotes N^{n}LO
    // (h) denotes helicity of "internal" leg (all momenta outgoing)
    // limit = R[1](+) * S[0](-) + R[1](-) * S[0](+)
    //       + R[0](+) * S[1](-) + R[0](-) * S[1](+)
    //       + spin correlations
    limit = redAmpSpnMatV[0] * splitSpnMatT[3] + redAmpSpnMatV[3] * splitSpnMatT[0]
        + redAmpSpnMatT[0] * splitSpnMatV[3] + redAmpSpnMatT[3] * splitSpnMatV[0]
        + redAmpSpnMatV[1] * splitSpnMatT[2] + redAmpSpnMatV[2] * splitSpnMatT[1]
        + redAmpSpnMatT[1] * splitSpnMatV[2] + redAmpSpnMatT[2] * splitSpnMatV[1];

    ratioLoA = Eps3<T>(limit.get0() / ansL.get0(), limit.get1() / ansL.get1(), limit.get2() / ansL.get2());

    print('V', true, show);

    return ratioLoA;
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
Eps3<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::helAmp2V(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp2(hel);

    redAmp.virt_spnmatrix(o[0], helRm.data(), redAmpSpnMatV.data());
    split.virt_spnmatrix(0, helSm.data(), splitSpnMatV.data());

    ansL = amp.virt(helCur.data());

    return this->amp2V_(show);
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
Eps3<T> cf::Compare<T, Amp, RedAmp, Split, Mapping>::amp2V(const bool show)
{
    // init_amp2();
    redAmp.born_spnmatrix(o[0], redAmpSpnMatT.data());
    split.born_spnmatrix(0, splitSpnMatT.data());

    redAmp.virt_spnmatrix(o[0], redAmpSpnMatV.data());
    split.virt_spnmatrix(0, splitSpnMatV.data());

    ansL = amp.virt();

    return this->amp2V_(show);
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare5_4<T, Amp, RedAmp, Split>::init_hel_amp(std::array<int, mulF> hel)
{
    if ((helCur != hel) || (!amp_level)) {
        amp_level = true;
        amp2_level = false;
        init_hels(hel);

        amp.setHelicity(hel.data());
        ansT = amp.A0(0, 1, 2, 3, 4);

        redAmp.setHelicity(helRm.data());
        ansRmT = redAmp.A0(0, 1, 2, 3);

        redAmp.setHelicity(helRp.data());
        ansRpT = redAmp.A0(0, 1, 2, 3);

        split.setHelicity(helSm.data());
        ansSmT = split.A0(0, 1, 2);

        split.setHelicity(helSp.data());
        ansSpT = split.A0(0, 1, 2);
    }
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
void cf::Compare<T, Amp, RedAmp, Split, Mapping>::init_hel_amp2(std::array<int, mulF> hel)
{
    if ((helCur != hel) || (!amp2_level)) {
        amp2_level = true;
        amp_level = false;
        init_hels(hel);

        redAmp.born_spnmatrix(o[0], helRp.data(), redAmpSpnMatT.data());
        split.born_spnmatrix(0, helSp.data(), splitSpnMatT.data());

        ansT = amp.born(hel.data());
    }
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
void cf::Compare<T, Amp, RedAmp, Split, Mapping>::init_amp2()
{
    // init_hels(hel);

    redAmp.born_spnmatrix(o[0], redAmpSpnMatT.data());
    split.born_spnmatrix(0, splitSpnMatT.data());

    ansT = amp.born();
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
void cf::Compare<T, Amp, RedAmp, Split, Mapping>::all_(std::array<int, mulF> hel, const bool sq, const bool tree, const bool loop, const bool show)
{
    if (sq) {
        if (tree) {
            helAmp2T(hel, show);
        }
        // virt is tree*loop
        if (loop) {
            helAmp2V(hel, show);
        }
    } else {
        if (tree) {
            helAmpT(hel, show);
        }
        if (loop) {
            helAmpG(hel, show);
            helAmpF(hel, show);
        }
    }
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
cf::Compare2c<T, Amp, RedAmp, Split, Mapping>::Compare2c(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << o[0] << "||" << o[1] << " (zero indexed), precision: " << typeid(T).name() << "\n";
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
cf::Compare3c<T, Amp, RedAmp, Split, Mapping>::Compare3c(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << o[0] << "||" << o[1] << "||" << o[2] << " (zero indexed), precision: " << typeid(T).name() << "\n";
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare5_4<T, Amp, RedAmp, Split>::helAmpG(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AL(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AL(0, 1, 2, 3);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AL(0, 1, 2, 3);

    split.setHelicity(helSm.data());
    ansSmL = split.AL(0, 1, 2);

    split.setHelicity(helSp.data());
    ansSpL = split.AL(0, 1, 2);

    return helAmpL_('g', show);
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare5_4<T, Amp, RedAmp, Split>::helAmpF(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AF(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AF(0, 1, 2, 3);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AF(0, 1, 2, 3);

    split.setHelicity(helSm.data());
    ansSmL = split.AF(0, 1, 2);

    split.setHelicity(helSp.data());
    ansSpL = split.AF(0, 1, 2);

    return helAmpL_('f', show);
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare5_4<T, Amp, RedAmp, Split>::all(const bool sq, const bool tree, const bool loop, const bool show)
{
    for (int i0 { -1 }; i0 < 2; i0 += 2) {
        for (int i1 { -1 }; i1 < 2; i1 += 2) {
            for (int i2 { -1 }; i2 < 2; i2 += 2) {
                for (int i3 { -1 }; i3 < 2; i3 += 2) {
                    for (int i4 { -1 }; i4 < 2; i4 += 2) {
                        const std::array<int, mulF> hel { i0, i1, i2, i3, i4 };
                        all_(hel, sq, tree, loop, show);
                    }
                }
            }
        }
    }
}

template <typename T, class Amp, class RedAmp, class Split>
cf::Compare5_4<T, Amp, RedAmp, Split>::Compare5_4(const int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
}

template <typename T>
cf::Compare5_4_g2gg<T>::Compare5_4_g2gg(const int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << mulF << "g, "
              << "g>" << mulS - 1 << "g"
              << "\n";
}

template <typename T>
cf::Compare5_4_q2qg<T>::Compare5_4_q2qg(const int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    flavF = "qqggg";
    flavS = "qqg";
    flavR = flavF.substr(0, o[0]) + "q" + flavF.substr(o[nc]);
    std::cout << "2q3g, "
              << "q>qg"
              << "\n";
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare6_5<T, Amp, RedAmp, Split>::init_hel_amp(std::array<int, mulF> hel)
{
    if ((helCur != hel) || (!amp_level)) {
        amp_level = true;
        amp2_level = false;
        init_hels(hel);

        amp.setHelicity(hel.data());
        ansT = amp.A0(0, 1, 2, 3, 4, 5);

        redAmp.setHelicity(helRm.data());
        ansRmT = redAmp.A0(0, 1, 2, 3, 4);

        redAmp.setHelicity(helRp.data());
        ansRpT = redAmp.A0(0, 1, 2, 3, 4);

        split.setHelicity(helSm.data());
        ansSmT = split.A0(0, 1, 2);

        split.setHelicity(helSp.data());
        ansSpT = split.A0(0, 1, 2);
    }
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare6_5<T, Amp, RedAmp, Split>::helAmpG(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AL(0, 1, 2, 3, 4, 5);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AL(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AL(0, 1, 2, 3, 4);

    split.setHelicity(helSm.data());
    ansSmL = split.AL(0, 1, 2);

    split.setHelicity(helSp.data());
    ansSpL = split.AL(0, 1, 2);

    return helAmpL_('g', show);
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare6_5<T, Amp, RedAmp, Split>::helAmpF(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AF(0, 1, 2, 3, 4, 5);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AF(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AF(0, 1, 2, 3, 4);

    split.setHelicity(helSm.data());
    ansSmL = split.AF(0, 1, 2);

    split.setHelicity(helSp.data());
    ansSpL = split.AF(0, 1, 2);

    return helAmpL_('f', show);
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare6_5<T, Amp, RedAmp, Split>::all(const bool sq, const bool tree, const bool loop, const bool show)
{
    for (int i0 { -1 }; i0 < 2; i0 += 2) {
        for (int i1 { -1 }; i1 < 2; i1 += 2) {
            for (int i2 { -1 }; i2 < 2; i2 += 2) {
                for (int i3 { -1 }; i3 < 2; i3 += 2) {
                    for (int i4 { -1 }; i4 < 2; i4 += 2) {
                        for (int i5 { -1 }; i5 < 2; i5 += 2) {
                            const std::array<int, mulF> hel { i0, i1, i2, i3, i4, i5 };
                            all_(hel, sq, tree, loop, show);
                        }
                    }
                }
            }
        }
    }
}

template <typename T, class Amp, class RedAmp, class Split>
cf::Compare6_5<T, Amp, RedAmp, Split>::Compare6_5(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
}

template <typename T>
cf::Compare6_5_g2gg<T>::Compare6_5_g2gg(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << mulF << "g, "
              << "g>" << mulS - 1 << "g"
              << "\n";
}

template <typename T>
cf::Compare6_5_q2qg<T>::Compare6_5_q2qg(const int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    flavF = "qqgggg";
    flavS = "qqg";
    flavR = "q" + flavF.substr(o[2]) + flavF.substr(0, o[0]);
    std::cout << "2q4g, "
              << "q>qg"
              << "\n";
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare6_4<T, Amp, RedAmp, Split>::init_hel_amp(std::array<int, mulF> hel)
{
    if ((helCur != hel) || (!amp_level)) {
        amp_level = true;
        amp2_level = false;
        init_hels(hel);

        amp.setHelicity(hel.data());
        ansT = amp.A0(0, 1, 2, 3, 4, 5);

        redAmp.setHelicity(helRm.data());
        ansRmT = redAmp.A0(0, 1, 2, 3);

        redAmp.setHelicity(helRp.data());
        ansRpT = redAmp.A0(0, 1, 2, 3);

        split.setHelicity(helSm.data());
        ansSmT = split.A0(0, 1, 2, 3);

        split.setHelicity(helSp.data());
        ansSpT = split.A0(0, 1, 2, 3);
    }
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare6_4<T, Amp, RedAmp, Split>::helAmpG(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AL(0, 1, 2, 3, 4, 5);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AL(0, 1, 2, 3);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AL(0, 1, 2, 3);

    split.setHelicity(helSm.data());
    ansSmL = split.AL(0, 1, 2, 3);

    split.setHelicity(helSp.data());
    ansSpL = split.AL(0, 1, 2, 3);

    return helAmpL_('g', show);
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare6_4<T, Amp, RedAmp, Split>::helAmpF(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AF(0, 1, 2, 3, 4, 5);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AF(0, 1, 2, 3);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AF(0, 1, 2, 3);

    split.setHelicity(helSm.data());
    ansSmL = split.AF(0, 1, 2, 3);

    split.setHelicity(helSp.data());
    ansSpL = split.AF(0, 1, 2, 3);

    return helAmpL_('f', show);
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare6_4<T, Amp, RedAmp, Split>::all(const bool sq, const bool tree, const bool loop, const bool show)
{
    for (int i0 { -1 }; i0 < 2; i0 += 2) {
        for (int i1 { -1 }; i1 < 2; i1 += 2) {
            for (int i2 { -1 }; i2 < 2; i2 += 2) {
                for (int i3 { -1 }; i3 < 2; i3 += 2) {
                    for (int i4 { -1 }; i4 < 2; i4 += 2) {
                        for (int i5 { -1 }; i5 < 2; i5 += 2) {
                            const std::array<int, mulF> hel { i0, i1, i2, i3, i4, i5 };
                            all_(hel, sq, tree, loop, show);
                        }
                    }
                }
            }
        }
    }
}

template <typename T, class Amp, class RedAmp, class Split>
cf::Compare6_4<T, Amp, RedAmp, Split>::Compare6_4(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
}

template <typename T>
cf::Compare6_4_g2ggg<T>::Compare6_4_g2ggg(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << mulF << "g, "
              << "g>" << mulS - 1 << "g"
              << "\n";
}

template <typename T>
cf::Compare6_4_g2qqg<T>::Compare6_4_g2qqg(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    flavF = "qqgggg";
    flavS = "gqqg";
    flavR = flavF.substr(0, o[0]) + flavS.substr(0, 1) + flavF.substr(o[nc]);
    std::cout << flavF << ", "
              << flavS.substr(0, 1) << ">" << flavS.substr(1)
              << "\n";
}

template <typename T>
cf::Compare6_4_g2qqg_b<T>::Compare6_4_g2qqg_b(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    flavF = "qqqqgg";
    flavS = "gqqg";
    flavR = flavF.substr(0, o[0]) + flavS.substr(0, 1) + flavF.substr(o[nc]);
    std::cout << flavF << ", "
              << flavS.substr(0, 1) << ">" << flavS.substr(1)
              << "\n";
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare7_5<T, Amp, RedAmp, Split>::init_hel_amp(std::array<int, mulF> hel)
{
    if ((helCur != hel) || (!amp_level)) {
        amp_level = true;
        amp2_level = false;
        init_hels(hel);

        amp.setHelicity(hel.data());
        ansT = amp.A0(0, 1, 2, 3, 4, 5, 6);

        redAmp.setHelicity(helRm.data());
        ansRmT = redAmp.A0(0, 1, 2, 3, 4);

        redAmp.setHelicity(helRp.data());
        ansRpT = redAmp.A0(0, 1, 2, 3, 4);

        split.setHelicity(helSm.data());
        ansSmT = split.A0(0, 1, 2, 3);

        split.setHelicity(helSp.data());
        ansSpT = split.A0(0, 1, 2, 3);
    }
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare7_5<T, Amp, RedAmp, Split>::helAmpG(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AL(0, 1, 2, 3, 4, 5, 6);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AL(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AL(0, 1, 2, 3, 4);

    split.setHelicity(helSm.data());
    ansSmL = split.AL(0, 1, 2, 3);

    split.setHelicity(helSp.data());
    ansSpL = split.AL(0, 1, 2, 3);

    return helAmpL_('g', show);
}

template <typename T, class Amp, class RedAmp, class Split>
Eps3<T> cf::Compare7_5<T, Amp, RedAmp, Split>::helAmpF(std::array<int, mulF> hel, const bool show)
{
    init_hel_amp(hel);

    ansL = amp.AF(0, 1, 2, 3, 4, 5, 6);

    redAmp.setHelicity(helRm.data());
    ansRmL = redAmp.AF(0, 1, 2, 3, 4);

    redAmp.setHelicity(helRp.data());
    ansRpL = redAmp.AF(0, 1, 2, 3, 4);

    split.setHelicity(helSm.data());
    ansSmL = split.AF(0, 1, 2, 3);

    split.setHelicity(helSp.data());
    ansSpL = split.AF(0, 1, 2, 3);

    return helAmpL_('f', show);
}

template <typename T, class Amp, class RedAmp, class Split>
void cf::Compare7_5<T, Amp, RedAmp, Split>::all(const bool sq, const bool tree, const bool loop, const bool show)
{
    for (int i0 { -1 }; i0 < 2; i0 += 2) {
        for (int i1 { -1 }; i1 < 2; i1 += 2) {
            for (int i2 { -1 }; i2 < 2; i2 += 2) {
                for (int i3 { -1 }; i3 < 2; i3 += 2) {
                    for (int i4 { -1 }; i4 < 2; i4 += 2) {
                        for (int i5 { -1 }; i5 < 2; i5 += 2) {
                            for (int i6 { -1 }; i6 < 2; i6 += 2) {
                                const std::array<int, mulF> hel { i0, i1, i2, i3, i4, i5, i6 };
                                all_(hel, sq, tree, loop, show);
                            }
                        }
                    }
                }
            }
        }
    }
}

template <typename T, class Amp, class RedAmp, class Split>
cf::Compare7_5<T, Amp, RedAmp, Split>::Compare7_5(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
}

template <typename T>
cf::Compare7_5_g2ggg<T>::Compare7_5_g2ggg(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1, mom)
{
    std::cout << mulF << "g, "
              << "g>" << mulS - 1 << "g"
              << "\n";
}
