#ifndef AMPS_HPP
#define AMPS_HPP

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "analytic/2q2g-analytic.h"
#include "analytic/2q3g-analytic.h"
#include "chsums/0q6g.h"
#include "chsums/0q7g.h"
#include "chsums/2q4g.h"
#include "chsums/4q2g.h"
#include "ir/split_g2gg-analytic.h"
#include "ir/split_q2qg-analytic.h"
#include "ir/split_g2ggg-analytic.h"
#include "ir/split_g2qqg-analytic.h"

namespace amps {

template <typename T>
class Amp7g : public Amp0q7g<T> {
private:
    using base = Amp0q7g<T>;

public:
    using base::A0;
    Amp7g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp6g : public Amp0q6g<T> {
private:
    using base = Amp0q6g<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp6g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp5g : public Amp0q5g_a<T> {
private:
    using base = Amp0q5g_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp5g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp4g : public Amp0q4g_a<T> {
private:
    using base = Amp0q4g_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp4g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp2q4g_ : public Amp2q4g<T> {
private:
    using base = Amp2q4g<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp2q4g_(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp2q3g : public Amp2q3g_a<T> {
private:
    using base = Amp2q3g_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp2q3g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp2q2g : public Amp2q2g_a<T> {
private:
    using base = Amp2q2g_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp2q2g(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class Amp4q2g_ : public Amp4q2g<T> {
private:
    using base = Amp4q2g<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    Amp4q2g_(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class g2gg : public Splitg2gg_a<T> {
private:
    using base = Splitg2gg_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    g2gg(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class g2ggg : public Splitg2ggg_a<T> {
private:
    using base = Splitg2ggg_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    g2ggg(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class g2qqg : public Splitg2qqg_a<T> {
private:
    using base = Splitg2qqg_a<T>;

public:
    using base::A0;
    using base::AF;
    using base::AL;
    g2qqg(T scale)
        : base(scale)
    {
    }
};

template <typename T>
class q2qg : public Splitq2qg_a<T> {
private:
    using base = Splitq2qg_a<T>;

public:
    using base::A0;
    using base::AL;

    LoopResult<Eps3<T>> AF(int p1, int p2, int p3) override {
        return LoopResult<Eps3<T>>();
    }

    q2qg(T scale)
        : base(scale)
    {
    }
};

} // namespace amps

#endif // AMPS_HPP
