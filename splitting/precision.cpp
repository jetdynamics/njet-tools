#include <array>
#include <chrono>
#include <iostream>
#include <numbers>
#include <typeinfo>

#include "ngluon2/Mom.h" // for qd

template <typename T>
void squares()
{
    std::cout << typeid(T).name() << '\n';
    const T irr { static_cast<T>(M_PI) };

    // auto t00 { std::chrono::high_resolution_clock::now() };
    // const T tmp0 { pow<T>(irr, static_cast<T>(2)) };
    // auto t01 { std::chrono::high_resolution_clock::now() };
    // std::cout << tmp0 << '\n';
    // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t01 - t00).count() << "ns\n";

    auto t10 { std::chrono::high_resolution_clock::now() };
    const T tmp1 { pow(irr, static_cast<T>(2)) };
    auto t11 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp1 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t11 - t10).count() << "ns\n";

    auto t20 { std::chrono::high_resolution_clock::now() };
    const T tmp2 { pow(irr, 2) };
    auto t21 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp2 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t21 - t20).count() << "ns\n";

    auto t30 { std::chrono::high_resolution_clock::now() };
    const T tmp3 { irr * irr };
    auto t31 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp3 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t31 - t30).count() << "ns\n";
}

template <typename T>
void cubes()
{
    std::cout << typeid(T).name() << '\n';
    // std::cout << static_cast<T>(1.e64) + static_cast<T>(1.) << '\n';
    const T irr { static_cast<T>(M_PI) };

    auto t10 { std::chrono::high_resolution_clock::now() };
    const T tmp1 { pow(irr, static_cast<T>(3)) };
    auto t11 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp1 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t11 - t10).count() << "ns\n";

    auto t20 { std::chrono::high_resolution_clock::now() };
    const T tmp2 { pow(irr, 3) };
    auto t21 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp2 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t21 - t20).count() << "ns\n";

    auto t30 { std::chrono::high_resolution_clock::now() };
    const T tmp3 { irr * irr * irr };
    auto t31 { std::chrono::high_resolution_clock::now() };
    std::cout << tmp3 << '\n';
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t31 - t30).count() << "ns\n";
}

template <typename T>
void arrays()
{
    std::cout << typeid(T).name() << '\n';

    auto t10 { std::chrono::high_resolution_clock::now() };
    T arr1[10] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    arr1[0] += 1;
    auto t11 { std::chrono::high_resolution_clock::now() };
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t11 - t10).count() << "ns\n";

    auto t20 { std::chrono::high_resolution_clock::now() };
    std::array<T, 10> arr2 { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    arr2[0] += 1;
    auto t21 { std::chrono::high_resolution_clock::now() };
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t21 - t20).count() << "ns\n";
}

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(128);

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n";
    }

    std::cout << "C++2a std::numbers::pi_v<type>\n";

    std::cout << std::numbers::pi_v<float> << '\n';
    std::cout << std::numbers::pi_v<double> << '\n';
    std::cout << std::numbers::pi_v<long double> << '\n';

    std::cout << "Initialising M_PI\n";

    const float pie_hd { M_PI };
    std::cout << pie_hd << '\n';

    const double pie_sd { M_PI };
    std::cout << pie_sd << '\n';

    const dd_real pie_dd { M_PI };
    std::cout << pie_dd << '\n';

    const qd_real pie_qd { M_PI };
    std::cout << pie_qd << '\n';

    std::cout << "Casting M_PI\n";
    std::cout << (qd_real)M_PI << '\n';
    std::cout << qd_real(M_PI) << '\n';
    std::cout << static_cast<qd_real>(M_PI) << '\n';

    std::cout << "Casting integers\n";
    std::cout << 1 << '\n';
    std::cout << (qd_real)1 << '\n';
    std::cout << qd_real(1) << '\n';
    std::cout << static_cast<qd_real>(1) << '\n';

    std::cout << "Casting irrationals\n";
    std::cout << 1 / 3 << '\n';
    std::cout << 1.f / 3 << '\n';
    std::cout << 1. / 3 << '\n';
    std::cout << 1.l / 3 << '\n';
    std::cout << (qd_real)1 / 3 << '\n';
    std::cout << (qd_real)1. / 3 << '\n';
    std::cout << (qd_real)1 / 3. << '\n';
    std::cout << (qd_real)1. / 3. << '\n';
    std::cout << qd_real(1) / qd_real(3) << '\n';
    std::cout << static_cast<qd_real>(1) / static_cast<qd_real>(3) << '\n';
    std::cout << static_cast<qd_real>(1.) / static_cast<qd_real>(3) << '\n';
    std::cout << static_cast<qd_real>(1) / static_cast<qd_real>(3.) << '\n';
    std::cout << static_cast<qd_real>(1.) / static_cast<qd_real>(3.) << '\n';
    std::cout << static_cast<qd_real>(1.f) / static_cast<qd_real>(3.f) << '\n';

    std::cout << "Timing\n";
    // const int num { 100 };
    auto t00 { std::chrono::high_resolution_clock::now() };
    // for (int i { 0 }; i < num; ++i) {
    const qd_real tmp0 { static_cast<qd_real>(1) };
    // }
    auto t01 { std::chrono::high_resolution_clock::now() };
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t01 - t00).count() << "ns\n";
    // auto t10 { std::chrono::high_resolution_clock::now() };
    // const qd_real tmp1 { static_cast<qd_real>(1.l) };
    // auto t11 { std::chrono::high_resolution_clock::now() };
    // std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t11 - t10).count() << "ns\n";
    auto t20 { std::chrono::high_resolution_clock::now() };
    const qd_real tmp2 { static_cast<qd_real>(1.) };
    auto t21 { std::chrono::high_resolution_clock::now() };
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t21 - t20).count() << "ns\n";
    auto t30 { std::chrono::high_resolution_clock::now() };
    const qd_real tmp3 { static_cast<qd_real>(1.f) };
    auto t31 { std::chrono::high_resolution_clock::now() };
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t31 - t30).count() << "ns\n";

    std::cout << "Powers: squares\n";
    squares<double>();
    squares<long double>();
    squares<dd_real>();
    squares<qd_real>();

    std::cout << "Powers: cubes\n";
    cubes<double>();
    cubes<qd_real>();

    std::cout << "Arrays\n";
    arrays<double>();

    return 0;
}
