#ifndef CF_HPP
#define CF_HPP

#include <array>
#include <complex>
#include <cstddef>
#include <string>

#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "amps.hpp"
#include "maps.hpp"

namespace cf {

template <typename T>
T chop(T value)
{
    const T zero { 1e-10 };
    return (abs(value) > zero) ? value : T();
}

template <typename T>
std::complex<T> chop(const std::complex<T>& value)
{
    return std::complex<T>(chop(value.real()), chop(value.imag()));
}

template <typename T>
Eps3<T> chop(const Eps3<T>& value)
{
    return Eps3<T>(chop(value.get0()), chop(value.get1()), chop(value.get2()));
}

template <std::size_t mul>
std::string genHelStr(const std::array<int, mul>& helInt)
{
    std::string helStr;
    for (int hel : helInt) {
        helStr.append(hel == 1 ? "+" : "-");
    }
    return helStr;
}

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
class Compare : private Mapping {
private:
    using base = Mapping;

protected:
    using base::mulF;

private:
    const T scale { 1. };
    const T mur2 { 91.118 };

    void init_hel_amp2(std::array<int, mulF> hel);
    void init_amp2();

    void print(char type, bool sq, bool show = false);

    std::complex<T> amp2T_(bool show = false);
    Eps3<T> amp2V_(bool show = false);

protected:
    Amp amp;
    RedAmp redAmp;
    Split split;

    using base::mulR;
    using base::mulS;
    using base::nc;
    using base::o;

    std::string flavF;
    std::string flavR;
    std::string flavS;

    bool amp_level;
    bool amp2_level;

    std::array<int, mulF> helCur;
    std::array<int, mulR> helRm;
    std::array<int, mulR> helRp;
    std::array<int, mulS> helSm;
    std::array<int, mulS> helSp;

    Eps3<T> ansL;
    std::complex<T> ansT;
    Eps3<T> ansRmL;
    std::complex<T> ansRmT;
    Eps3<T> ansRpL;
    std::complex<T> ansRpT;
    Eps3<T> ansSmL;
    std::complex<T> ansSmT;
    Eps3<T> ansSpL;
    std::complex<T> ansSpT;
    Eps3<T> limit;
    Eps3<T> ratioLoA;

    std::array<std::complex<T>, 4> splitSpnMatT;
    std::array<std::complex<T>, 4> redAmpSpnMatT;
    std::array<Eps3<T>, 4> splitSpnMatV;
    std::array<Eps3<T>, 4> redAmpSpnMatV;

    void init_hels(const std::array<int, mulF>& hel);

    Eps3<T> helAmpL_(char type, bool show = false);

    virtual void init_hel_amp(std::array<int, mulF> hel) = 0;

    void all_(std::array<int, mulF> hel, bool sq, bool tree, bool loop, bool show);

    Compare(int o1, const std::array<MOM<T>, mulF>& mom);

public:
    std::complex<T> helAmpT(std::array<int, mulF> hel, bool show = false);
    std::complex<T> helAmp2T(std::array<int, mulF> hel, bool show = false);
    T amp2T(bool show = false);

    virtual Eps3<T> helAmpG(std::array<int, mulF> hel, bool show = false) = 0;
    virtual Eps3<T> helAmpF(std::array<int, mulF> hel, bool show = false) = 0;
    Eps3<T> helAmp2V(std::array<int, mulF> hel, bool show = false);
    Eps3<T> amp2V(bool show = false);

    // all helicities
    virtual void all(bool sq = false, bool tree = true, bool loop = true, bool show = false) = 0;
};

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
class Compare2c : public Compare<T, Amp, RedAmp, Split, Mapping> {
protected:
    using base = Compare<T, Amp, RedAmp, Split, Mapping>;
    using base::mulF;
    using base::nc;
    using base::o;

    Compare2c(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T, class Amp, class RedAmp, class Split, class Mapping>
class Compare3c : public Compare<T, Amp, RedAmp, Split, Mapping> {
protected:
    using base = Compare<T, Amp, RedAmp, Split, Mapping>;
    using base::mulF;
    using base::nc;
    using base::o;

    Compare3c(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T, class Amp, class RedAmp, class Split>
class Compare5_4 : public Compare2c<T, Amp, RedAmp, Split, maps::Map5_4<T>> {
private:
    using base = Compare2c<T, Amp, RedAmp, Split, maps::Map5_4<T>>;

protected:
    using base::mulF;
    using base::mulS;
    using base::o;

    Compare5_4(int o1, const std::array<MOM<T>, mulF>& mom);

private:
    using base::all_;
    using base::amp;
    using base::amp2_level;
    using base::amp_level;
    using base::ansL;
    using base::ansRmL;
    using base::ansRmT;
    using base::ansRpL;
    using base::ansRpT;
    using base::ansSmL;
    using base::ansSmT;
    using base::ansSpL;
    using base::ansSpT;
    using base::ansT;
    using base::helAmp2T;
    using base::helAmp2V;
    using base::helAmpL_;
    using base::helCur;
    using base::helRm;
    using base::helRp;
    using base::helSm;
    using base::helSp;
    using base::init_hels;
    using base::limit;
    using base::mulR;
    using base::ratioLoA;
    using base::redAmp;
    using base::split;

    void init_hel_amp(std::array<int, mulF> hel) override;

public:
    using base::helAmpT;
    Eps3<T> helAmpG(std::array<int, mulF> hel, bool show = false) override;
    Eps3<T> helAmpF(std::array<int, mulF> hel, bool show = false) override;

    void all(bool sq = false, bool tree = true, bool loop = true, bool show = false) override;
};

template <typename T>
class Compare5_4_g2gg : public Compare5_4<T, amps::Amp5g<T>, amps::Amp4g<T>, amps::g2gg<T>> {
private:
    using base = Compare5_4<T, amps::Amp5g<T>, amps::Amp4g<T>, amps::g2gg<T>>;
    using base::mulF;
    using base::mulS;

public:
    Compare5_4_g2gg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T>
class Compare5_4_q2qg : public Compare5_4<T, amps::Amp2q3g<T>, amps::Amp2q2g<T>, amps::q2qg<T>> {
private:
    using base = Compare5_4<T, amps::Amp2q3g<T>, amps::Amp2q2g<T>, amps::q2qg<T>>;
    using base::flavF;
    using base::flavR;
    using base::flavS;
    using base::mulF;
    using base::nc;
    using base::o;

public:
    Compare5_4_q2qg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T, class Amp, class RedAmp, class Split>
class Compare6_5 : public Compare2c<T, Amp, RedAmp, Split, maps::Map6_5<T>> {
private:
    using base = Compare2c<T, Amp, RedAmp, Split, maps::Map6_5<T>>;

protected:
    using base::mulF;
    using base::mulS;
    using base::o;

    Compare6_5(int o1, const std::array<MOM<T>, mulF>& mom);

private:
    using base::all_;
    using base::amp;
    using base::amp2_level;
    using base::amp_level;
    using base::ansL;
    using base::ansRmL;
    using base::ansRmT;
    using base::ansRpL;
    using base::ansRpT;
    using base::ansSmL;
    using base::ansSmT;
    using base::ansSpL;
    using base::ansSpT;
    using base::ansT;
    using base::helAmp2T;
    using base::helAmp2V;
    using base::helAmpL_;
    using base::helCur;
    using base::helRm;
    using base::helRp;
    using base::helSm;
    using base::helSp;
    using base::init_hels;
    using base::limit;
    using base::mulR;
    using base::ratioLoA;
    using base::redAmp;
    using base::split;

    void init_hel_amp(std::array<int, mulF> hel) override;

public:
    using base::helAmpT;
    Eps3<T> helAmpG(std::array<int, mulF> hel, bool show = false) override;
    Eps3<T> helAmpF(std::array<int, mulF> hel, bool show = false) override;

    void all(bool sq = false, bool tree = true, bool loop = true, bool show = false) override;
};

template <typename T>
class Compare6_5_g2gg : public Compare6_5<T, amps::Amp6g<T>, amps::Amp5g<T>, amps::g2gg<T>> {
private:
    using base = Compare6_5<T, amps::Amp6g<T>, amps::Amp5g<T>, amps::g2gg<T>>;
    using base::mulF;
    using base::mulS;

public:
    Compare6_5_g2gg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T>
class Compare6_5_q2qg : public Compare6_5<T, amps::Amp2q4g_<T>, amps::Amp2q3g<T>, amps::q2qg<T>> {
private:
    using base = Compare6_5<T, amps::Amp2q4g_<T>, amps::Amp2q3g<T>, amps::q2qg<T>>;
    using base::flavF;
    using base::flavR;
    using base::flavS;
    using base::mulF;
    using base::o;

public:
    Compare6_5_q2qg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T, class Amp, class RedAmp, class Split>
class Compare6_4 : public Compare3c<T, Amp, RedAmp, Split, maps::Map6_4<T>> {
private:
    using base = Compare3c<T, Amp, RedAmp, Split, maps::Map6_4<T>>;

protected:
    using base::mulF;
    using base::mulS;
    using base::o;

    Compare6_4(int o1, const std::array<MOM<T>, mulF>& mom);

private:
    using base::all_;
    using base::amp;
    using base::amp2_level;
    using base::amp_level;
    using base::ansL;
    using base::ansRmL;
    using base::ansRmT;
    using base::ansRpL;
    using base::ansRpT;
    using base::ansSmL;
    using base::ansSmT;
    using base::ansSpL;
    using base::ansSpT;
    using base::ansT;
    using base::helAmp2T;
    using base::helAmp2V;
    using base::helAmpL_;
    using base::helCur;
    using base::helRm;
    using base::helRp;
    using base::helSm;
    using base::helSp;
    using base::init_hels;
    using base::limit;
    using base::mulR;
    using base::ratioLoA;
    using base::redAmp;
    using base::split;

    void init_hel_amp(std::array<int, mulF> hel) override;

public:
    using base::helAmpT;
    Eps3<T> helAmpG(std::array<int, mulF> hel, bool show = false) override;
    Eps3<T> helAmpF(std::array<int, mulF> hel, bool show = false) override;

    void all(bool sq = false, bool tree = true, bool loop = true, bool show = false) override;
};

template <typename T>
class Compare6_4_g2ggg : public Compare6_4<T, amps::Amp6g<T>, amps::Amp4g<T>, amps::g2ggg<T>> {
private:
    using base = Compare6_4<T, amps::Amp6g<T>, amps::Amp4g<T>, amps::g2ggg<T>>;
    using base::mulF;
    using base::mulS;

public:
    Compare6_4_g2ggg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T>
class Compare6_4_g2qqg : public Compare6_4<T, amps::Amp2q4g_<T>, amps::Amp4g<T>, amps::g2qqg<T>> {
private:
    using base = Compare6_4<T, amps::Amp2q4g_<T>, amps::Amp4g<T>, amps::g2qqg<T>>;
    using base::mulF;
    using base::mulS;
    using base::flavF;
    using base::flavR;
    using base::flavS;
    using base::nc;
    using base::o;

public:
    Compare6_4_g2qqg(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T>
class Compare6_4_g2qqg_b : public Compare6_4<T, amps::Amp4q2g_<T>, amps::Amp2q2g<T>, amps::g2qqg<T>> {
private:
    using base = Compare6_4<T, amps::Amp4q2g_<T>, amps::Amp2q2g<T>, amps::g2qqg<T>>;
    using base::mulF;
    using base::mulS;
    using base::flavF;
    using base::flavR;
    using base::flavS;
    using base::nc;
    using base::o;

public:
    Compare6_4_g2qqg_b(int o1, const std::array<MOM<T>, mulF>& mom);
};

template <typename T, class Amp, class RedAmp, class Split>
class Compare7_5 : public Compare3c<T, Amp, RedAmp, Split, maps::Map7_5<T>> {
private:
    using base = Compare3c<T, Amp, RedAmp, Split, maps::Map7_5<T>>;

protected:
    using base::mulF;
    using base::mulS;
    using base::o;

    Compare7_5(int o1, const std::array<MOM<T>, mulF>& mom);

private:
    using base::all_;
    using base::amp;
    using base::amp2_level;
    using base::amp_level;
    using base::ansL;
    using base::ansRmL;
    using base::ansRmT;
    using base::ansRpL;
    using base::ansRpT;
    using base::ansSmL;
    using base::ansSmT;
    using base::ansSpL;
    using base::ansSpT;
    using base::ansT;
    using base::helAmp2T;
    using base::helAmp2V;
    using base::helAmpL_;
    using base::helCur;
    using base::helRm;
    using base::helRp;
    using base::helSm;
    using base::helSp;
    using base::init_hels;
    using base::limit;
    using base::mulR;
    using base::ratioLoA;
    using base::redAmp;
    using base::split;

    void init_hel_amp(std::array<int, mulF> hel) override;

public:
    using base::helAmpT;
    Eps3<T> helAmpG(std::array<int, mulF> hel, bool show = false) override;
    Eps3<T> helAmpF(std::array<int, mulF> hel, bool show = false) override;

    void all(bool sq = false, bool tree = true, bool loop = true, bool show = false) override;
};

template <typename T>
class Compare7_5_g2ggg : public Compare7_5<T, amps::Amp7g<T>, amps::Amp5g<T>, amps::g2ggg<T>> {
private:
    using base = Compare7_5<T, amps::Amp7g<T>, amps::Amp5g<T>, amps::g2ggg<T>>;
    using base::mulF;
    using base::mulS;

public:
    Compare7_5_g2ggg(int o1, const std::array<MOM<T>, mulF>& mom);
};

} // namespace cf

#include "cf.ipp"

#endif // CF_HPP
