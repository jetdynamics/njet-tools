#ifndef CPS_HPP
#define CPS_HPP

#include <array>
#include <cstddef>

#include "ngluon2/Mom.h"

template <typename T>
class cps {
private:
    const int r;
    const T sqrtS;

    template <std::size_t mul>
    void print(const std::array<MOM<T>, mul>& momenta);

    template <std::size_t mul>
    void test(const std::array<MOM<T>, mul>& momenta);

public:
    // momenta are reordered by rotating elements such that r denotes the first
    // e.g. (r = 1) => (y1->0 <=> 2||3) for c5

    // hard coded collinear phase-space point for 2->3
    // theta and alpha are angles [0, 2*Pi] or [-Pi, Pi]; y1 and y2 are fractions [0, 1]
    // y1->0: collinear in 3||4 (s34->0) (using zero indexing)
    // also, but non-adjacent:
    // y2->0: collinear in 2||4 (s24->0) (using zero indexing)
    // theta->0: collinear in 0||2 (s02->0) (using zero indexing)
    std::array<MOM<T>, 5> c5(T y1, T y2, T theta, T alpha);

    // alpha->0: collinear in 2||3 (s23->0) (using zero indexing)
    std::array<MOM<T>, 5> c5b(T y1, T theta, T alpha);

    // hard coded collinear phase-space point for 2->4
    // alpha, beta, gamma are angles
    // gamma->0: collinear in 3||4 (s34->0) (using zero indexing)
    // beta->0: collinear in 2||3 (s23->0) (using zero indexing)
    // also, but non-adjacent:
    // alpha->0: collinear in 0||2 (s02->0) (using zero indexing)
    std::array<MOM<T>, 6> c6(T alpha, T beta, T gamma);

    // hard coded collinear phase-space point for 2->5
    // alpha, beta, gamma, delta are angles
    std::array<MOM<T>, 7> c7(T alpha, T beta, T gamma, T delta);

    cps(int r_ = 0, T sqrtS_ = 1.);
}; // class cps

#include "cps.ipp"

#endif // CPS_HPP
