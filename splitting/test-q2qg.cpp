#include <array>
#include <cmath>
#include <iostream>

#include "ngluon2/Mom.h"

#include "cf.hpp"
#include "cps.hpp"

// degree: of collinearity (lambda=10^-degree)
// point: in phase space
template <typename T>
void run_5(T degree, T point, bool sq, bool trees, bool loops, bool hs)
{
    std::cout << "Lambda=" << degree << ", PS(" << point << ")\n";

    const T lambda { pow(10, -degree) };
    const std::array<MOM<T>, 5> mom { cps<T>(2).c5(lambda, point, point, point) };
    cf::Compare5_4_q2qg<T> compare(1, mom);
    if (hs) {
        if (trees) {
            compare.amp2T();
        }
        if (loops) {
            compare.amp2V();
        }
    } else {
        compare.all(sq, trees, loops);
    }

    std::cout << '\n';
}

template <typename T>
void run_5b(T degree, T point1, T point2, bool sq, bool trees, bool loops, bool hs)
{
    std::cout << "Lambda=" << degree << ", PS(" << point1 << ", " << point2 << ")\n";

    const T lambda { pow(10, -degree) };
    const std::array<MOM<T>, 5> mom { cps<T>(1).c5b(point1, point2, lambda) };
    cf::Compare5_4_q2qg<T> compare(1, mom);
    if (hs) {
        if (trees) {
            compare.amp2T();
        }
        if (loops) {
            compare.amp2V();
        }
    } else {
        compare.all(sq, trees, loops);
    }

    std::cout << '\n';
}

template <typename T>
void run_6(T degree, T point1, T point2, bool sq, bool trees, bool loops, bool hs)
{
    std::cout << "Lambda=" << degree << ", PS(" << point1 << ", " << point2 << ")\n";

    const T lambda { pow(10, -degree) };
    const std::array<MOM<T>, 6> mom { cps<T>(2).c6(point1, point2, lambda) };
    cf::Compare6_5_q2qg<T> compare(1, mom);

    if (hs) {
        if (trees) {
            compare.amp2T();
        }
        if (loops) {
            compare.amp2V();
        }
    } else {
        compare.all(sq, trees, loops);
    }

    std::cout << '\n';
}

int main(int argc, char** argv)
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);
    std::cout << '\n';

    if (argc != 1) {
        std::cout << "Warning: " << argv[0] << " doesn't accept command line arguments.\n\n";
    }

    // sq, trees, loops, hs

    // out by a constant factor
    // run_6<qd_real>(32, M_PI / 3, M_PI / 7, false, true, false, false);
    // run_6<qd_real>(32, M_PI / 3, M_PI / 7, true, true, false, false);
    // run_6<qd_real>(32, M_PI / 3, M_PI / 7, true, true, false, true);

    // Works
    // run_5<qd_real>(32, 0.2, false, true, false, false);
    // run_5<qd_real>(32, 0.2, true, true, false, false);
    // run_5<qd_real>(32, 0.2, true, true, false, true);

    // Works
    // run_5b<qd_real>(32, 0.2, M_PI / 3, false, true, false, false);
    // run_5b<qd_real>(32, 0.2, M_PI / 3, true, true, false, false);
    // run_5b<qd_real>(32, 0.2, M_PI / 3, true, true, false, true);

    // Works
    // run_5<qd_real>(50, 0.2, false, false, true, false);
    run_5<qd_real>(50, 0.2, true, false, true, false);

    // Fails
    run_5<qd_real>(50, 0.2, true, false, true, true);

    std::cout << '\n';
    return 0;
}
