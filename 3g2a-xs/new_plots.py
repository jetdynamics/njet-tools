#!/usr/bin/env python3

# these compare R and V, but they are of course entirely dependent on the subtraction scheme...

import argparse, math, pathlib, pickle

import matplotlib.cm, matplotlib.pyplot, numpy

from plot_histograms import OBSS, read_in, safe_div, fix_edge

LABELS = {
    "LO": "LO",
    "R": "Real",
    "V": "Virtual",
}


def plot_out(values, args, obs_name, obs_fname, folder, log=False, ll="best"):
    print("  Rendering")

    width = 4.8
    aspect_ratio = (1 + math.sqrt(5)) / 2

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(width, width / aspect_ratio),
        # tight_layout=True,
    )

    if "dphi" in obs_fname:
        for order in values.values():
            for key in order:
                order[key] *= numpy.pi

    LOcv = values["LO"]["central_value"]
    bins = values["LO"]["bins"]

    if "y" in obs_fname:
        ax.set_xlim(bins[[1, -3]])
    elif "pt" in obs_fname:
        ax.set_xlim(bins[[1, -1]])
    else:
        ax.set_xlim(bins[[0, -1]].round(decimals=1))

    colours = matplotlib.cm.tab10(range(len(values)))

    for (name, order), colour in zip(values.items(), colours):
        ax.fill_between(
            bins,
            fix_edge(numpy.abs(safe_div(values[name]["scale_variation_max"], LOcv))),
            fix_edge(numpy.abs(safe_div(values[name]["scale_variation_min"], LOcv))),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        value = safe_div(
            numpy.abs(order["central_value"]),
            LOcv,
        )

        ax.stairs(
            value,
            edges=bins,
            baseline=None,
            label=LABELS[name],
            color=colour,
        )

        if args.mc_errors:
            ax.errorbar(
                order["x"],
                value,
                yerr=safe_div(
                    order["central_value_error"],
                    LOcv,
                ),
                fmt="none",
                ecolor=colour,
            )

    if "pt" in obs_fname or "M" in obs_fname:
        ylabel = f"d$\sigma$/d{obs_name}"
        xlabel = f"{obs_name} GeV"
    elif "dphi" in obs_fname:
        ylabel = f"d$\sigma$/d({obs_name}/$\pi$)"
        xlabel = f"{obs_name}/$\pi$"
    else:
        ylabel = f"d$\sigma$/d{obs_name}"
        xlabel = obs_name

    ax.set_xlabel(xlabel)

    ax.legend(loc=ll, frameon=False)

    ax.set_ylabel(f"Ratio magnitude [{ylabel}]")

    filename = f"{folder}/RV_{obs_fname}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename, bbox_inches="tight", pad_inches=0.025)

    matplotlib.pyplot.close(fig=fig)


def plot_alt(values, args, obs_name, obs_fname, folder, log=False, ll="best"):
    print("  Rendering")

    width = 4.8
    aspect_ratio = (1 + math.sqrt(5)) / 2

    fig, ax = matplotlib.pyplot.subplots(
        figsize=(width, width / aspect_ratio),
        # tight_layout=True,
    )

    if log:
        ax.set_yscale("log")

    if "dphi" in obs_fname:
        for order in values.values():
            for key in order:
                order[key] *= numpy.pi

    LOcv = values["LO"]["central_value"]
    bins = values["LO"]["bins"]

    if "y" in obs_fname:
        ax.set_xlim(bins[[1, -3]])
    elif "pt" in obs_fname:
        ax.set_xlim(bins[[1, -1]])
    else:
        ax.set_xlim(bins[[0, -1]].round(decimals=1))

    colours = matplotlib.cm.tab10(range(len(values)))

    for (name, order), colour in zip(values.items(), colours):
        ax.fill_between(
            bins,
            fix_edge(numpy.abs(values[name]["scale_variation_max"])),
            fix_edge(numpy.abs(values[name]["scale_variation_min"])),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        value = numpy.abs(order["central_value"])

        ax.stairs(
            value,
            edges=bins,
            baseline=None,
            label=LABELS[name],
            color=colour,
        )

        if args.mc_errors:
            ax.errorbar(
                order["x"],
                value,
                yerr=order["central_value_error"],
                fmt="none",
                ecolor=colour,
            )

    if "pt" in obs_fname or "M" in obs_fname:
        ylabel = f"d$\sigma$/d{obs_name} [pb/GeV]"
        xlabel = f"{obs_name} GeV"
    elif "dphi" in obs_fname:
        ylabel = f"d$\sigma$/d({obs_name}/$\pi$) [pb]"
        xlabel = f"{obs_name}/$\pi$"
    else:
        ylabel = f"d$\sigma$/d{obs_name} [pb]"
        xlabel = obs_name

    ax.set_xlabel(xlabel)

    ax.legend(loc=ll, frameon=False)

    ax.set_ylabel(ylabel)

    filename = f"{folder}/RV_{obs_fname}_alt_{'log' if log else 'lin'}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename, bbox_inches="tight", pad_inches=0.025)

    matplotlib.pyplot.close(fig=fig)


def do_observable(fname, conf, args):
    print(f"Plotting {fname}")

    values = {
        name: read_in(f"{name}.{fname}.dat", args)
        for name in (
            "LO",
            "R",
            "V",
            # "NLOonly",
            # "NLO",
        )
    }

    ll = conf["ll"] if "ll" in conf else "best"

    plot_out(
        values,
        args,
        obs_name=conf["tex"],
        obs_fname=fname,
        ll=ll,
        folder=args.output_folder,
    )

    plot_alt(
        values,
        args,
        obs_name=conf["tex"],
        obs_fname=fname,
        ll=ll,
        folder=args.output_folder,
    )

    plot_alt(
        values,
        args,
        obs_name=conf["tex"],
        obs_fname=fname,
        ll=ll,
        folder=args.output_folder,
        log=True,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("generate histograms")
    parser.add_argument(
        "-e", "--mc-errors", action="store_true", help="Show central value MC errors"
    )
    parser.add_argument(
        "-r", "--refresh", action="store_true", help="Refresh cached data files"
    )
    parser.add_argument(
        "-o",
        "--observable",
        metavar="O",
        type=str,
        help='Plot observable O if included, paper plots if O="paper", otherwise plot all',
    )
    parser.add_argument(
        "-f",
        "--output-folder",
        metavar="F",
        type=str,
        help="Put plots into folder F",
        default="plots",
    )
    args = parser.parse_args()

    pd = pathlib.Path(args.output_folder)
    pd.mkdir(exist_ok=True)

    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    if args.observable:
        if args.observable == "paper":
            for obs in (
                "cosCS_2",
                "dphi_2",
                "dy_2",
                "Mg1g2_2",
                "pt_g1g2_2",
                "yg1g2_2",
            ):
                do_observable(obs, OBSS[obs], args)
        else:
            do_observable(args.observable, OBSS[args.observable], args)
    else:
        for item in OBSS.items():
            do_observable(*item, args)
