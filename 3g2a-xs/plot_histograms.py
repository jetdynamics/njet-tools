#!/usr/bin/env python3

import argparse
import pathlib
import pickle

import matplotlib.cm
import matplotlib.pyplot
import numpy

OBSS = {
    "cosCS_1": {
        "tex": r"$|\cos{\phi_{CS}(\gamma\gamma)}|$",
        "ll": "upper left",
    },
    "cosCS_2": {
        "tex": r"$|\cos{\phi_{CS}(\gamma\gamma)}|$",
        "ll": "upper left",
    },
    "dphi_1": {
        "tex": r"$\Delta\phi(\gamma\gamma)$",
        "logy": True,
        "ll": "upper left",
    },
    "dphi_2": {
        "tex": r"$\Delta\phi(\gamma\gamma)$",
        "logy": True,
        "ll": "upper left",
    },
    "dy_1": {
        "tex": r"$\Delta y(\gamma\gamma)$",
    },
    "dy_2": {
        "tex": r"$\Delta y(\gamma\gamma)$",
    },
    "dy_3": {
        "tex": r"$\Delta y(\gamma\gamma)$",
    },
    "Mg1g2_1": {
        "tex": r"$m(\gamma\gamma)$",
        "logy": True,
    },
    "Mg1g2_2": {
        "tex": r"$m(\gamma\gamma)$",
        "logy": True,
    },
    "Mg1g2_3": {
        "tex": r"$m(\gamma\gamma)$",
        "logy": True,
    },
    "pt_g1g2_1": {
        "tex": r"$p_T(\gamma\gamma)$",
        "logy": True,
    },
    "pt_g1g2_2": {
        "tex": r"$p_T(\gamma\gamma)$",
        "logy": True,
    },
    "pt_g1g2_3": {
        "tex": r"$p_T(\gamma\gamma)$",
        "logy": True,
    },
    "ptg1_1": {
        "tex": r"$p_T(\gamma_1)$",
        "logy": True,
    },
    "ptg1_2": {
        "tex": r"$p_T(\gamma_1)$",
        "logy": True,
    },
    "ptg1_3": {
        "tex": r"$p_T(\gamma_1)$",
        "logy": True,
    },
    "ptg2_1": {
        "tex": r"$p_T(\gamma_2)$",
        "logy": True,
    },
    "ptg2_2": {
        "tex": r"$p_T(\gamma_2)$",
        "logy": True,
    },
    "ptg2_3": {
        "tex": r"$p_T(\gamma_2)$",
        "logy": True,
    },
    "yg1g2_1": {
        "tex": r"$|y(\gamma\gamma)|$",
    },
    "yg1g2_2": {
        "tex": r"$|y(\gamma\gamma)|$",
    },
    "yg1g2_3": {
        "tex": r"$|y(\gamma\gamma)|$",
    },
    "yg1_1": {
        "tex": r"$y(\gamma_1)$",
    },
    "yg1_2": {
        "tex": r"$y(\gamma_1)$",
    },
    "yg2_1": {
        "tex": r"$y(\gamma_2)$",
    },
    "yg2_2": {
        "tex": r"$y(\gamma_2)$",
    },
}


def read_in(filename, args):
    file = "data" / pathlib.Path(filename)

    picklefile = file.parent / f".{file.name}.pickle"
    if not args.refresh and picklefile.is_file():
        print(f"  Using data cache {picklefile}")
        with open(picklefile, "rb") as f:
            return pickle.load(f)

    print(f"  Reading file {file}")
    with open(file, "r") as f:
        raw = f.readlines()

    data = numpy.genfromtxt(raw[3:-1], delimiter=" ")
    bins = numpy.append(data[:, 0], data[:, 2][-1])
    x = data[:, 1]

    valerrs = data[:, 3:] / 1e3  # pb
    vals = valerrs[:, ::2]
    errs = valerrs[:, 1::2]
    central_value = vals[:, 0]
    central_value_error = errs[:, 0]
    scale_variation_max = vals.max(axis=1)
    scale_variation_min = vals.min(axis=1)

    data = {
        "bins": bins,
        "x": x,
        "central_value": central_value,
        "central_value_error": central_value_error,
        "scale_variation_max": scale_variation_max,
        "scale_variation_min": scale_variation_min,
        "central_value_error": central_value_error,
    }

    print(f"    Saving data cache {picklefile}")
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)

    return data


def safe_div(a, b):
    return numpy.divide(
        a,
        b,
        out=numpy.zeros_like(a),
        where=b != 0,
    )


def fix_edge(a):
    return numpy.append(a, 0.0)


def plot_out(values, args, obs_name, obs_fname, log=False, ll="best"):
    print("  Rendering")

    axis_label_font_size = 17
    axis_tick_label_size = 15

    width = 6.4
    aspect_ratio = 4 / 3
    sub_ratio = 2
    ara = aspect_ratio / (sub_ratio / (sub_ratio + 1))

    fig, (axa, axr) = matplotlib.pyplot.subplots(
        nrows=2,
        sharex=True,
        figsize=(width, width / aspect_ratio),
        # figsize=(4.8, 3.6),
        subplot_kw={},
        gridspec_kw={
            "height_ratios": (sub_ratio, 1),
            "hspace": 0,
        },
        tight_layout=True,
    )

    for ax in (axa, axr):
        ax.tick_params(axis="y", labelsize=axis_tick_label_size, which="both")

    axr.tick_params(axis="x", labelsize=axis_tick_label_size, which="both")

    axa.set_yscale("log" if log else "linear")
    axr.set_yscale("linear")

    colours = matplotlib.cm.tab10(range(2))

    if "dphi" in obs_fname:
        for order in values.values():
            for key in order:
                # order[key] /= numpy.pi
                order[key] *= numpy.pi

    LO = values["LO"]
    LOcv = LO["central_value"]
    LO["ratio_min"] = safe_div(LO["scale_variation_min"], LOcv)
    LO["ratio_max"] = safe_div(LO["scale_variation_max"], LOcv)

    if "y" in obs_fname:
        axa.set_xlim(LO["bins"][[1, -3]])
    elif "pt" in obs_fname:
        axa.set_xlim(LO["bins"][[1, -1]])
    else:
        axa.set_xlim(LO["bins"][[0, -1]].round(decimals=1))

    NLO = values["NLO"]
    NLO["ratio_min"] = safe_div(NLO["scale_variation_min"], LOcv)
    NLO["ratio_max"] = safe_div(NLO["scale_variation_max"], LOcv)

    # ayl = min(NLO["scale_variation_min"].min(), LO["scale_variation_min"].min())
    # ayu = max(NLO["scale_variation_max"].max(), LO["scale_variation_max"].max())
    # axa.set_ylim(ayl, ayu)

    ryl = min(NLO["ratio_min"].min(), LO["ratio_min"].min())
    ryu = max(NLO["ratio_max"].max(), LO["ratio_max"].max())
    axr.set_ylim(ryl - 0.1, ryu + 0.1)

    for (name, order), colour in zip(values.items(), colours):
        axa.fill_between(
            order["bins"],
            fix_edge(order["scale_variation_max"]),
            fix_edge(order["scale_variation_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        axa.stairs(
            order["central_value"],
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            axa.errorbar(
                order["x"],
                order["central_value"],
                yerr=order["central_value_error"],
                fmt="none",
                ecolor=colour,
            )

        axr.fill_between(
            order["bins"],
            fix_edge(order["ratio_max"]),
            fix_edge(order["ratio_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        ratio = safe_div(
            order["central_value"],
            LOcv,
        )

        axr.stairs(
            ratio,
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            ratio_mcerr = safe_div(
                order["central_value_error"],
                LOcv,
            )

            axr.errorbar(
                order["x"],
                ratio,
                yerr=ratio_mcerr,
                fmt="none",
                ecolor=colour,
            )

    offset = 0.01
    det1 = "LHC 13 TeV NNPDF31"
    det2 = r"{\Large $gg\to\gamma\gamma g\quad\mu=m_T/2$}"
    det3 = r"{\Large\textsc{NNLOjet}+\textsc{OpenLoops2}+\textsc{NJet}}"
    if "cos" in obs_fname:
        text2 = det2 + r"\quad" + det3
        axa.text(
            x=0.475,
            y=1 - offset * ara,
            s=det1,
            fontsize=axis_label_font_size,
            transform=axa.transAxes,
            horizontalalignment="center",
            verticalalignment="top",
        )
        axa.text(
            x=offset,
            y=offset * ara,
            s=text2,
            fontsize=axis_label_font_size,
            transform=axa.transAxes,
            horizontalalignment="left",
            verticalalignment="bottom",
        )
    elif "yg1g2" in obs_fname:
        text2 = det2 + "\n" + det3
        axa.text(
            x=0.5,
            y=1 - offset * ara,
            s=det1,
            fontsize=axis_label_font_size,
            transform=axa.transAxes,
            horizontalalignment="center",
            verticalalignment="top",
        )
        axa.text(
            x=offset,
            y=offset * ara,
            s=text2,
            fontsize=axis_label_font_size,
            transform=axa.transAxes,
            horizontalalignment="left",
            verticalalignment="bottom",
        )
    else:
        if "dy" in obs_fname:
            text = det1 + "\n" + det2 + r"\quad" + det3
        else:
            text = det1 + "\n" + det2 + "\n" + det3
        if "dphi" in obs_fname:
            axa.text(
                x=1 - offset,
                y=offset * ara,
                s=text,
                fontsize=axis_label_font_size,
                transform=axa.transAxes,
                horizontalalignment="right",
                verticalalignment="bottom",
                multialignment="right",
            )
        else:
            axa.text(
                x=offset,
                y=offset * ara,
                s=text,
                fontsize=axis_label_font_size,
                transform=axa.transAxes,
                horizontalalignment="left",
                verticalalignment="bottom",
            )

    # introduce some space so tick labels don't overlap
    if "yg1g2" in obs_fname:
        margin = 0.15
        al, au = axa.get_ylim()
        axa.set_ylim(al - margin, au)

        rl, ru = axr.get_ylim()
        axr.set_ylim(rl, ru + margin)

    if "pt" in obs_fname or "M" in obs_fname:
        ylabel = f"d$\sigma$/d{obs_name} [pb/GeV]"
        xlabel = f"{obs_name} GeV"
    elif "dphi" in obs_fname:
        ylabel = f"d$\sigma$/d({obs_name}/$\pi$) [pb]"
        xlabel = f"{obs_name}/$\pi$"
    else:
        ylabel = f"d$\sigma$/d{obs_name} [pb]"
        xlabel = obs_name

    axa.set_ylabel(ylabel, fontsize=axis_label_font_size)
    axr.set_xlabel(xlabel, fontsize=axis_label_font_size)

    axa.legend(loc=ll, prop={"size": axis_label_font_size}, frameon=False)

    axr.set_ylabel("Ratio", fontsize=axis_label_font_size)

    filename = f"plots/{obs_fname}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename, bbox_inches="tight", pad_inches=0.025)

    matplotlib.pyplot.close(fig=fig)


def do_observable(fname, conf, args):
    print(f"Plotting {fname}")

    dLO = read_in(f"LO.{fname}.dat", args)
    dNLO = read_in(f"NLO.{fname}.dat", args)

    ll = conf["ll"] if "ll" in conf else "best"
    logy = conf["logy"] if "logy" in conf else False

    plot_out(
        {"LO": dLO, "NLO": dNLO},
        args,
        obs_name=conf["tex"],
        obs_fname=fname,
        ll=ll,
        log=logy,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("generate histograms")
    parser.add_argument(
        "-e", "--mc-errors", action="store_true", help="Show central value MC errors"
    )
    parser.add_argument(
        "-l", "--latex", action="store_true", help="Use LaTeX font and symbols"
    )
    parser.add_argument(
        "-r", "--refresh", action="store_true", help="Refresh cached data files"
    )
    parser.add_argument(
        "-o",
        "--observable",
        type=str,
        help="Plot OBSERVABLE if included, otherwise plot all",
    )
    args = parser.parse_args()

    pd = pathlib.Path("plots/")
    pd.mkdir(exist_ok=True)

    if args.latex:
        matplotlib.pyplot.rc("text", usetex=True)
        matplotlib.pyplot.rc("font", family="serif")

    if args.observable:
        do_observable(args.observable, OBSS[args.observable], args)
    else:
        for item in OBSS.items():
            do_observable(*item, args)
