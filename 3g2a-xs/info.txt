Components in histo_data.tar.gz:

LO
R
V
NLO (LO + V + R)
NLOonly (R + V)


Scale choices (mu=1/2*sqrt(mgg^2+ptgg^2)):

1  muf = 1.0 * mu     mur = 1.0 * mu
2  muf = 0.5 * mu     mur = 0.5 * mu
3  muf = 2.0 * mu     mur = 2.0 * mu
4  muf = 1.0 * mu     mur = 0.5 * mu
5  muf = 1.0 * mu     mur = 2.0 * mu
6  muf = 0.5 * mu     mur = 1.0 * mu
7  muf = 2.0 * mu     mur = 1.0 * mu



observable nbins min max 
pt_g1g2_1   50    0 1000
pt_g1g2_2   40    0 800
pt_g1g2_3   10    0 800
ptg1_1      50    0 1000
ptg1_2      40    0 800
ptg1_3      10    0 800
ptg2_1      50    0 1000
ptg2_2      40    0 800
ptg2_3      10    0 800
Mg1g2_1     50    0 1000
Mg1g2_2     16    100 300
Mg1g2_3     8     100 300
cosCS_1     20    0 1
cosCS_2     10    0 1
dy_1        20    0 5 0
dy_2        20    0 2.5
dy_3        10    0 2.5
dphi_1      20    0 3.14159
dphi_2      10    0 3.14159
yg1g2_1     20    0 5
yg1g2_2     20    0 2.5
yg1g2_3     10    0 2.5
yg1_1       20    0 2.4    (less statistics!!!)
yg1_2       10    0 2.4    (less statistics!!!)
yg2_1       20    0 2.4    (less statistics!!!)
yg2_2       10    0 2.4    (less statistics!!!)


Format of data file:

Header
N events
overflow
.
.
.
bin_start bin_center bin_end value error
                             |_________|
                               7 times
                               
                               
The files with "cross" in the name have a different format since they give the results for the total cross section for different scales.
