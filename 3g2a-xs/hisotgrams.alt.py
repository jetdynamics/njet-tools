#!/usr/bin/env python3

import argparse
import pathlib
import pickle

import matplotlib.cm
import matplotlib.pyplot
import numpy


def read_in(filename, args):
    file = "data" / pathlib.Path(filename)

    picklefile = file.parent / f".{file.name}.pickle"
    if not args.refresh and picklefile.is_file():
        print(f"  Using data cache {picklefile}")
        with open(picklefile, "rb") as f:
            return pickle.load(f)

    print(f"  Reading file {file}")
    with open(file, "r") as f:
        raw = f.readlines()

    data = numpy.genfromtxt(raw[3:-1], delimiter=" ")
    bins = numpy.append(data[:, 0], data[:, 2][-1])
    x = data[:, 1]

    valerrs = data[:, 3:] / 1e3  # pb
    vals = valerrs[:, ::2]
    errs = valerrs[:, 1::2]
    central_value = vals[:, 0]
    central_value_error = errs[:, 0]
    scale_variation_max = vals.max(axis=1)
    scale_variation_min = vals.min(axis=1)

    data = {
        "bins": bins,
        "x": x,
        "central_value": central_value,
        "central_value_error": central_value_error,
        "scale_variation_max": scale_variation_max,
        "scale_variation_min": scale_variation_min,
        "central_value_error": central_value_error,
    }

    print(f"    Saving data cache {picklefile}")
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)

    return data


def safe_div(a, b):
    return numpy.divide(
        a,
        b,
        out=numpy.zeros_like(a),
        where=b != 0,
    )


def fix_edge(a):
    return numpy.append(a, 0.0)


def plot_out(values, args, obs_name, obs_fname, log=False, ll="best"):
    print("  Rendering")

    axis_label_font_size = 17
    axis_tick_label_size = 15

    fig, (axa, axr) = matplotlib.pyplot.subplots(
        nrows=2,
        sharex=True,
        figsize=(6.4, 4.8),
        # figsize=(4.8, 3.6),
        subplot_kw={},
        gridspec_kw={
            "height_ratios": (2, 1),
            "hspace": 0,
        },
        tight_layout=True,
    )

    for ax in (axa, axr):
        ax.tick_params(axis="y", labelsize=axis_tick_label_size, which="both")

    axr.tick_params(axis="x", labelsize=axis_tick_label_size, which="both")
    # direction="in", top=True

    # fig.set_tight_layout(True)

    if log:
        axa.set_yscale("log")
    else:
        axa.set_yscale("linear")

    axr.set_yscale("linear")

    colours = matplotlib.cm.tab10(range(2))

    LO = values["LO"]
    LOcv = LO["central_value"]
    LO["ratio_min"] = safe_div(LO["scale_variation_min"], LOcv)
    LO["ratio_max"] = safe_div(LO["scale_variation_max"], LOcv)

    axa.set_xlim(LO["bins"][[0, -1]])

    NLO = values["NLO"]
    NLO["ratio_min"] = safe_div(NLO["scale_variation_min"], LOcv)
    NLO["ratio_max"] = safe_div(NLO["scale_variation_max"], LOcv)

    # ayl = min(NLO["scale_variation_min"].min(), LO["scale_variation_min"].min())
    # ayu = max(NLO["scale_variation_max"].max(), LO["scale_variation_max"].max())
    # axa.set_ylim(ayl, ayu)

    ryl = min(NLO["ratio_min"].min(), LO["ratio_min"].min())
    ryu = max(NLO["ratio_max"].max(), LO["ratio_max"].max())
    axr.set_ylim(ryl - 0.1, ryu + 0.1)

    for (name, order), colour in zip(values.items(), colours):
        axa.fill_between(
            order["bins"],
            fix_edge(order["scale_variation_max"]),
            fix_edge(order["scale_variation_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        axa.stairs(
            order["central_value"],
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            axa.errorbar(
                order["x"],
                order["central_value"],
                yerr=order["central_value_error"],
                fmt="none",
                ecolor=colour,
            )

        axr.fill_between(
            order["bins"],
            fix_edge(order["ratio_max"]),
            fix_edge(order["ratio_min"]),
            step="post",
            facecolor=colour,
            alpha=0.2,
        )

        ratio = safe_div(
            order["central_value"],
            LOcv,
        )

        axr.stairs(
            ratio,
            edges=order["bins"],
            baseline=None,
            label=name,
            color=colour,
        )

        if args.mc_errors:
            ratio_mcerr = safe_div(
                order["central_value_error"],
                LOcv,
            )

            axr.errorbar(
                order["x"],
                ratio,
                yerr=ratio_mcerr,
                fmt="none",
                ecolor=colour,
            )

    # introduce some space so tick labels don't overlap
    # if "yg1g2" in obs_fname:
    #     al, au = axa.get_ylim()
    #     axa.set_ylim(al - 0.1, au)

    #     rl, ru = axr.get_ylim()
    #     axr.set_ylim(rl, ru + 0.1)

    if "pt" in obs_fname or "M" in obs_fname:
        ylabel = f"d$\sigma$/d{obs_name} [pb/GeV]"
        xlabel = f"{obs_name} GeV"
    else:
        ylabel = f"d$\sigma$/d{obs_name} [pb]"
        xlabel = obs_name

    axa.set_ylabel(ylabel, fontsize=axis_label_font_size)
    axr.set_xlabel(xlabel, fontsize=axis_label_font_size)

    axa.legend(loc=ll, prop={"size": axis_label_font_size}, frameon=False)

    axr.set_ylabel("Ratio", fontsize=axis_label_font_size)

    filename = f"plots/{obs_fname}.pdf"
    print(f"  Writing {filename}")
    fig.savefig(filename, bbox_inches="tight", pad_inches=0)

    matplotlib.pyplot.close(fig=fig)


def do_observable(fname, tex, lls, logs):
    print(f"Plotting {fname}")

    dLO = read_in(f"LO.{fname}.dat", args)
    dNLO = read_in(f"NLO.{fname}.dat", args)

    ll = "best"
    for pos, obss in lls.items():
        if fname in obss:
            ll = pos
            break

    plot_out(
        {"LO": dLO, "NLO": dNLO},
        args,
        obs_name=tex,
        obs_fname=fname,
        ll=ll,
        log=fname in logs,
    )


def do_observable_2d(obs, plotname, tex):

    dLO = []
    dNLO = []
    ranges_list = []

    for fname in obs.keys():
        dLO.append(read_in(f"LO.{fname}.dat", args))
        dNLO.append(read_in(f"NLO.{fname}.dat", args))

    ll = "best"

    for key in obs.keys():
        ranges_list.append(obs[key])

    plot_out(
        {"LO": dLO, "NLO": dNLO},
        args,
        obs_name=tex,
        obs_fname=plotname,
        ll=ll,
        log=True,
        nn=len(obs.keys()),
        ranges=ranges_list,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser("generate histograms")
    parser.add_argument(
        "-e", "--mc-errors", action="store_true", help="Show central value MC errors"
    )
    parser.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="Don't use LaTeX font and symbols (quicker)",
    )
    parser.add_argument(
        "-r", "--refresh", action="store_true", help="Refresh cached data files"
    )
    parser.add_argument(
        "-o",
        "--observable",
        type=str,
        help="Plot OBSERVABLE if included, otherwise plot all",
    )
    args = parser.parse_args()

    pd = pathlib.Path("plots/")
    pd.mkdir(exist_ok=True)

    if not args.quick:
        matplotlib.pyplot.rc("text", usetex=True)
        matplotlib.pyplot.rc("font", family="serif")

    obss = {
        "cosCS_1": r"$\cos{\phi_{CS}(\gamma\gamma)}$",
        "cosCS_2": r"$\cos{\phi_{CS}(\gamma\gamma)}$",
        "dphi_1": r"$\Delta\phi(\gamma\gamma)$",
        "dphi_2": r"$\Delta\phi(\gamma\gamma)$",
        "dy_1": r"$\Delta y(\gamma\gamma)$",
        "dy_2": r"$\Delta y(\gamma\gamma)$",
        "dy_3": r"$\Delta y(\gamma\gamma)$",
        "Mg1g2_1": r"$m(\gamma\gamma)$",
        "Mg1g2_2": r"$m(\gamma\gamma)$",
        "Mg1g2_3": r"$m(\gamma\gamma)$",
        "pt_g1g2_1": r"$p_T(\gamma\gamma)$",
        "pt_g1g2_2": r"$p_T(\gamma\gamma)$",
        "pt_g1g2_3": r"$p_T(\gamma\gamma)$",
        "ptg1_1": r"$p_T(\gamma_1)$",
        "ptg1_2": r"$p_T(\gamma_1)$",
        "ptg1_3": r"$p_T(\gamma_1)$",
        "ptg2_1": r"$p_T(\gamma_2)$",
        "ptg2_2": r"$p_T(\gamma_2)$",
        "ptg2_3": r"$p_T(\gamma_2)$",
        "yg1g2_1": r"$|y(\gamma\gamma)|$",
        "yg1g2_2": r"$|y(\gamma\gamma)|$",
        "yg1g2_3": r"$|y(\gamma\gamma)|$",
        "yg1_1": r"$y(\gamma_1)$",
        "yg1_2": r"$y(\gamma_1)$",
        "yg2_1": r"$y(\gamma_2)$",
        "yg2_2": r"$y(\gamma_2)$",
    }

    obss_2d = [
        {
            "fname": "2D_mgg_ptgg_bins",
            "lines": {
                "Mg1g2_ptg1g2_1": r" 20GeV $< p_T(\gamma\gamma)< $ 160GeV",
                "Mg1g2_ptg1g2_2": r"160GeV $< p_T(\gamma\gamma)< $ 320GeV",
                "Mg1g2_ptg1g2_3": r"320GeV $< p_T(\gamma\gamma)< $ 480GeV",
                "Mg1g2_ptg1g2_4": r"480GeV $< p_T(\gamma\gamma)< $ 640GeV",
                "Mg1g2_ptg1g2_5": r"640GeV $< p_T(\gamma\gamma)< $ 800GeV",
            },
            "tex": r"$m(\gamma\gamma)$",
        },
        {
            "fname": "2D_mgg_ptgg_cuts",
            "lines": {
                "Mg1g2_3": r"$p_T(\gamma\gamma) >$  20GeV",
                "Mg1g2_ptg1g2_6": r"$p_T(\gamma\gamma) >$  50GeV",
                "Mg1g2_ptg1g2_7": r"$p_T(\gamma\gamma) >$ 100GeV",
                "Mg1g2_ptg1g2_8": r"$p_T(\gamma\gamma) >$ 200GeV",
            },
            "tex": r"$m(\gamma\gamma)$",
        },
        {
            "fname": "2D_cosCS_mgg",
            "lines": {
                "cosCS_Mg1g2_1": r"100GeV $< m(\gamma\gamma) <$ 140GeV",
                "cosCS_Mg1g2_2": r"140GeV $< m(\gamma\gamma) <$ 180GeV",
                "cosCS_Mg1g2_3": r"180GeV $< m(\gamma\gamma) <$ 220GeV",
                "cosCS_Mg1g2_4": r"220GeV $< m(\gamma\gamma) <$ 260GeV",
                "cosCS_Mg1g2_5": r"260GeV $< m(\gamma\gamma) <$ 300GeV",
            },
            "tex": r"$\cos{\phi_{CS}(\gamma\gamma)}$",
        },
        {
            "fname": "2D_ptgg_ygg",
            "lines": {
                "ptg1g2_yg1g2_1": r"0 $< |y(\gamma\gamma)| <$ 0.6",
                "ptg1g2_yg1g2_2": r"0.6 $< |y(\gamma\gamma)| <$ 1.2",
                "ptg1g2_yg1g2_3": r"1.2 $< |y(\gamma\gamma)| <$ 1.8",
                "ptg1g2_yg1g2_4": r"1.8 $< |y(\gamma\gamma)| <$ 2.4",
            },
            "tex": r"$p_T(\gamma\gamma)$",
        },
    ]

    lls = {
        "upper left": (
            "cosCS_1",
            "cosCS_2",
            "dphi_1",
            "dphi_2",
        ),
    }

    logs = (
        "dphi_1",
        "dphi_2",
        "Mg1g2_1",
        "Mg1g2_2",
        "Mg1g2_3",
        "pt_g1g2_1",
        "pt_g1g2_2",
        "pt_g1g2_3",
        "ptg1_1",
        "ptg1_2",
        "ptg1_3",
        "ptg2_1",
        "ptg2_2",
        "ptg2_3",
    )

    if args.observable:
        do_observable(args.observable, obss[args.observable], lls, logs)
    else:
        for item in obss.items():
            do_observable(*item, lls, logs)
        for item in obss_2d:
            do_observable_2d(item["lines"], item["fname"], item["tex"], lls, logs)
