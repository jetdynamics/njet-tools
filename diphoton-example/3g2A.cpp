#include <array>
#include <cassert>
#include <iostream>

#include "njet.h"

int main(int argc, char** argv)
{
    std::cout.sync_with_stdio(false);
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    std::cout << "\n~ NJet: simple example of the BLHA interface for helicity amplitudes of GG->GAA ~\n\n";

    const int pspoints { 1 };
    const int legs { 5 };
    const int d { 4 };

    std::array<std::array<std::array<double, d>, legs>, pspoints> Momenta {
        {
            {
                {
                    {
                        0.5000000000000000E+03,
                        0.0000000000000000E+00,
                        0.0000000000000000E+00,
                        0.5000000000000000E+03,
                    },
                    {
                        0.5000000000000000E+03,
                        0.0000000000000000E+00,
                        0.0000000000000000E+00,
                        -0.5000000000000000E+03,
                    },
                    {
                        0.4585787878854402E+03,
                        0.1694532203096798E+03,
                        0.3796536620781987E+03,
                        -0.1935024746502525E+03,
                    },
                    {
                        0.3640666207368177E+03,
                        -0.1832986929319185E+02,
                        -0.3477043013193671E+03,
                        0.1063496077587081E+03,
                    },
                    {
                        0.1773545913777421E+03,
                        -0.1511233510164880E+03,
                        -0.3194936075883156E+02,
                        0.8715286689154436E+02,
                    },
                },
            },
        },
    };

    int rstatus;
    OLP_Start("contract_3g2A.lh", &rstatus);
    assert(rstatus);

    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    std::cout << "Running " << olpname
              << " version " << olpversion
              << " note " << olpmessage << "\n";

    const int n { 5 };
    const int epslen { 5 };

    for (int pt { 0 }; pt < pspoints; ++pt) {
        std::cout << "==================== Test point " << pt << " ====================\n";
        std::array<double, legs * n> LHMomenta;
        for (int p { 0 }; p < legs; ++p) {
            std::cout << "p" << p << "=(";
            for (int mu { 0 }; mu < d; ++mu) {
                std::cout << (Momenta[pt][p][mu] < 0 ? "" : " ") << Momenta[pt][p][mu] << (mu == d - 1 ? "" : ",");
                LHMomenta[mu + p * n] = Momenta[pt][p][mu];
            }
            std::cout << ")\n";
            LHMomenta[d + p * n] = 0.;
        }
        std::cout << "\n";

        const int channels { 2 };
        for (int p { 1 }; p <= channels; ++p) {
            std::array<double, 2 * epslen + 1> out;
            double acc { 0. };

            const double alphas { 0.118 };
            const double alpha { 7.29735257e-3 };
            const double zero { 0. };
            const double mur { 91.188 };

            OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
            assert(rstatus == 1);

            OLP_SetParameter("alpha", &alpha, &zero, &rstatus);
            assert(rstatus == 1);

            if (p == 2) {
                const double ii { 0. };
                const double jj { 1. };
                OLP_SetParameter("#ij", &ii, &jj, &rstatus);
                assert(rstatus == 1);
            }

            // thought this was for helicity amps, but I think it's actually for OLP_Polvec(p, q, eps)
            /* const double h { 0. }; */
            /* OLP_SetParameter("#h", &h, &zero, &rstatus); */
            /* assert(rstatus == 1); */

            OLP_EvalSubProcess2(&p, LHMomenta.data(), &mur, out.data(), &acc);

            std::cout << "---- channel number " << p << " ----\n";
            std::cout << "OLP accuracy check: " << acc << "\n";

            for (int a { 0 }; a < epslen; ++a) {
                std::cout << "Loop(-" << epslen - 1 - a << ") = " << (out[a] < 0 ? "" : " ") << out[a]
                          << " +- " << 100 * out[epslen + 1 + a] / out[a] << " %\n";
            }
            std::cout << "\n";
        }
    }

    return 0;
}
