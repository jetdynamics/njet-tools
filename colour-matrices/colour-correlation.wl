(* ::Package:: *)

(* ::Section:: *)
(*Set up*)


SetAttributes[fundamentalDelta,Orderless];
SetAttributes[adjointDelta,Orderless];
Format[fundamentalDynkinIndex]:=Subscript["T","F"];
Format[A[a__]]:=Subscript["A",Length[List[a]]][a];
Format[fundamentalTrace[a__]]:=Subscript["tr","F"][a];
Format[dagger[a_]]:=Power[a,"\[ConjugateTranspose]"];
applyRules[expr_,rules_]:=FixedPoint[ExpandAll[#]/.rules&,expr];
rules={
	dagger[-1*a_]->-dagger[a],
	dagger[a___f*b___fundamentalTrace]->dagger[a]*dagger[b],
	dagger[a_+ b_]:>dagger[a]+dagger[b],
	dagger[a_fundamentalTrace]:>Reverse[a],
	dagger[a_f]:>a,
	a_fundamentalTrace:>Module[
		{p=FirstPosition[a,Min[List@@a]][[1]]},
		Join[a[[p;;]],a[[;;p-1]]]
		]/;a[[1]]!=Min[List@@a],
	a_f:>(-I/fundamentalDynkinIndex)*(fundamentalTrace@@a-Reverse[fundamentalTrace@@a]),
	t_fundamentalTrace:>Times@@(Module[
		{i},
		T[a[t[[#]]],i[#],i[Mod[#+1,Length[t],1]]]&/@Range[Length[t]]
		]),
	T[a_,i_,j_]*T[b_,j_,i_]:>fundamentalDynkinIndex*adjointDelta[a,b],
	T[a_,i1_,i2_]*T[a_,i3_,i4_]:>
		fundamentalDynkinIndex(fundamentalDelta[i1,i4]*fundamentalDelta[i2,i3]
								-(1/Nc)*fundamentalDelta[i1,i2]*fundamentalDelta[i3,i4]),
	adjointDelta[i_,i_]:>Nc^2-1,
	adjointDelta[i1_,i2_]^2:>Nc^2-1,
	adjointDelta[i1_,i2_]*adjointDelta[i2_,i3_]:>adjointDelta[i1,i3],
	T[a1_,i1_,i2_]*adjointDelta[a1_,a2_]:>T[a2,i1,i2],
	fundamentalDelta[i_,i_]:>Nc,
	fundamentalDelta[i1_,i2_]^2:>Nc,
	fundamentalDelta[i1_,i2_]*fundamentalDelta[i2_,i3_]:>fundamentalDelta[i1,i3],
	T[a1_,i1_,i2_]*fundamentalDelta[i1_,i3_]:>T[a1,i3,i2],
	T[a1_,i1_,i2_]*fundamentalDelta[i2_,i3_]:>T[a1,i1,i3],
	T[a_,i_,i_]->0
	};
nonCyclicalPermutations[l_]:=Prepend[#,l[[1]]]&/@Permutations[l[[2;;]]];
qcd=fundamentalDynkinIndex->1/2;
export[expr_]:=CForm[expr/.{Nc^6->Nc6,Nc^5->Nc5,Nc^4->Nc4,Nc^3->Nc3,Nc^2->Nc2,Nc^(-2)->1/Nc2}];


(*Everything is in the colour basis of traces over fundamental generators*)
amplitude[l_]:=Plus@@(fundamentalTrace@@#*A@@#&/@(nonCyclicalPermutations[l]));
(*Manually specify unique Einstein summed index with argument extra - nb n+1 already used!*)
ccAmp[n_,i_,extra_]:=Module[
	{legs},
	legs=Join[Range[1,i-1],{extra},Range[i+1,n]];
	Return[amplitude[legs]*f[extra,i, n+1]];
	];
processAmplitude[amplitude_]:=Module[
	{partialAmplitudes},
	partialAmplitudes=DeleteDuplicates[Cases[amplitude,_A,Infinity]];
	Return[{Coefficient[amplitude,#]&/@partialAmplitudes,partialAmplitudes}];
	]; 
normalisation[n_]:=fundamentalDynkinIndex^n*Nc^(n-6)*(Nc^2-1);
generateNJetColMat[n_,norm_,i_,j_]:=Module[
	{amp1,amp2,coefficients1,partialAmplitudesVector1,coefficients2,partialAmplitudesVector2,matrix,sol,mat,vec,size,facs,facRep,matRepd,lenC},
	amp1=ccAmp[n,i,n+2];
	amp2=ccAmp[n,j,n+3];
	(*Print[amp1*dagger[amp2]];*)
	{coefficients1,partialAmplitudesVector1}=processAmplitude[amp1];
	{coefficients2,partialAmplitudesVector2}=processAmplitude[amp2];
	lenC=Length[coefficients1];
	matrix=applyRules[
		Table[coefficients1[[i1]]*dagger[coefficients2[[i2]]],{i1,lenC},{i2,lenC}],
		rules
		];
	(*Print[norm/.qcd];*)
	mat=Simplify/@(matrix/norm);
	vec=partialAmplitudesVector1;
	Print[MatrixForm/@{Expand[mat],vec}/.qcd];
	(*Print[(#-1&/@vec[[#]])&/@Range[len]];*)
	size=Length[mat[[1]]];
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=facs[[#]]->#-1&/@Range[Length[facs]];
	matRepd=mat/.facRep;
	Print["Normalisation"];
	Print[export[norm/.qcd]];
	(*Print["Colour matrix"];
	Print[MatrixForm[matRepd]];*)
	Print["Lower triangle"];
	Print[DeleteCases[Flatten[Table[Table[If[ii<=jj,matRepd[[ii,jj]],Null],{ii,size}],{jj,size}]],Null]];
	Print["Element values"];
	Print[export/@facs/.qcd];
	];


(* ::Section:: *)
(*Run: colour correlation matrices*)


(* ::Subsection::Closed:: *)
(*2g2A*)


generateNJetColMat[2,Nc/4,1,1]


generateNJetColMat[2,Nc/4,1,2]


generateNJetColMat[2,Nc/4,2,1]


generateNJetColMat[2,Nc/4,2,2]


(* ::Subsection:: *)
(*3g2A*)


For[i=1,i<4,i++,
	For[j=1,j<4,j++,
		Print[i,j];
		generateNJetColMat[3,normalisation[3]*Nc^3,i,j]
	]
]


(* ::Subsection:: *)
(*4g2A*)


generateNJetColMat[4,normalisation[4]*Nc,1,1]


(* EOF *)
