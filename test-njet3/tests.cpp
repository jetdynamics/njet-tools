#include <iomanip>
#include <iostream>
#include <vector>

/* #include "chsums/0q4g.h" */
#include "chsums/2q2g.h"
/* #include "chsums/4q0g.h" */

#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

const int Nc { 3 };
const int W { 5 };

/* template <typename T> */
/* std::vector<MOM<T>> reorder(const std::vector<MOM<T>>& input, const std::vector<int>& order) */
/* { */
/*     std::vector<MOM<T>> output(5); */
/*     for (int i { 0 }; i < 5; ++i) { */
/*         output[i] = input[order[i]]; */
/*     } */
/*     return output; */
/* } */

template <typename T>
void run(const int num)
{
    const std::vector<double> scales2 { { 0 } };

    std::cout << '\n'
              << "4q0g" << '\n'
              << "Nc=" << Nc << '\n'
              << '\n';

    Amp2q2g<T> amp;
    amp.setNc(Nc);

    for (int rseed { 1 }; rseed < num + 1; ++rseed) {
        std::cout << "Phase-space point " << rseed << '\n';

        PhaseSpace<T> ps5(5, rseed);
        std::vector<MOM<T>> momenta { ps5.getPSpoint() };

        refineM(momenta, momenta, scales2);

        for (const MOM<T>& momentum : momenta) {
            std::cout << momentum << '\n';
        }

        amp.setMomenta(momenta);

        std::cout << "Helicity sum" << '\n';
        std::cout << std::setw(W) << "B:" << std::setw(std::cout.precision() + 7 + 1) << amp.born() << '\n';
        std::cout << std::setw(W+1) << "V: " << amp.virt() << '\n';

        /* for (auto h : Amp2q2gStatic::HSarr) { */
        /*     for (int i { 0 }; i < 4; ++i) { */
        /*         std::cout << (h[i] == 1 ? '+' : '-'); */
        /*     } */
        /*     std::cout << '\n'; */

        /* std::cout << std::setw(W) << "hA2:" << std::setw(std::cout.precision() + 8) << amp.born(h) << '\n'; */
        /* } */

        std::cout << '\n';
    }
}

int main()
{
    std::cout << std::scientific << std::setprecision(16);
    run<double>(2);
}
