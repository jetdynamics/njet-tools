#!/usr/bin/env python3

import math, itertools

import numpy, matplotlib

import reader_3j, reader_yyj, plotter


if __name__ == "__main__":
    all_5p = reader_3j.read_proc("../stability/data_100k", "result")

    all_3g2a = reader_yyj.read_proc(
        "../../3g2a@2l/test/test-stability/3g2a_res", "result"
    )

    errors = {p: {c: d[:, 8] for c, d in cs.items()} for p, cs in all_5p.items()}

    for p, d in zip(errors.keys(), all_3g2a):
        errors[p]["gggyy"] = d[:, 4]

    plotter.init_plots()

    maxs = []
    mins = []
    for channel in errors["f64f64"].keys():
        maxs.append(max(errors["f64f64"][channel]))
        try:
            mins.append(min(errors["f128f128"][channel]))
        except KeyError:
            pass

    max_pow = math.ceil(math.log10(max(maxs)))
    min_pow = math.floor(math.log10(min(mins)))

    w = 4.8
    r = 2 / (1 + math.sqrt(5))

    fig, axs = matplotlib.pyplot.subplots(
        nrows=2,
        ncols=3,
        figsize=(2 * w, 2 * w * r),
        sharex=True,
        sharey=True,
    )

    axs[0][0].invert_xaxis()

    fig.subplots_adjust(
        wspace=0,
        hspace=0,
    )

    for i, (ax, channel) in enumerate(
        zip(
            itertools.chain.from_iterable([row[:2] for row in axs]),
            ("ggggg", "qbgqgg", "uDdUg", "uuUUg"),
        )
    ):
        left = i % 2 == 0
        bottom = i in (2, 3)

        plotter.histo_single(
            ax,
            errors,
            channel,
            x_min_pow=min_pow,
            x_max_pow=max_pow,
            legend=False,
            xlabel=bottom,
            ylabel=left,
            tick_left=left,
            tick_bottom=bottom,
            tick_top=not bottom,
            tick_right=i == 3,
            cumulative=True,
        )

    plotter.histo_single(
        axs[0][2],
        errors,
        "gggyy",
        x_min_pow=min_pow,
        x_max_pow=max_pow,
        legend=True,
        xlabel=True,
        ylabel=False,
        tick_left=False,
        tick_bottom=True,
        tick_top=True,
        tick_right=True,
        cumulative=True,
    )

    axs[1][2].plot([1],[1])
    axs[1][2].set_axis_off()

    plotter.write(fig, "stability_cf", vector=True)
