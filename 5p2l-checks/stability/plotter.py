# Python library

import math, pathlib

import numpy, matplotlib.pyplot

F64 = r"$\texttt{f64}$"
F128 = r"$\texttt{f128}$"


PREC = {
    "f64f64": f"{F64}/{F64}",
    "f128f64": f"{F128}/{F64}",
    "f128f128": f"{F128}/{F128}",
}

# key is 0->5, value is 2->3
CHAN = {
    "ggggg": r"$gg \to ggg$",
    "ggqbqg": r"$gg \to \bar{q}qg$",
    "qbgqgg": r"$qg \to qgg$",
    "qbqggg": r"$q\bar{q} \to ggg$",
    "uDdUg": r"$\bar{q}Q \to Q\bar{q}g$",
    "udUDg": r"$\bar{q}\bar{Q} \to \bar{q}\bar{Q}g$",
    "ugUdD": r"$\bar{q}g \to \bar{q}Q\bar{Q}$",
    "uUdDg": r"$\bar{q}q \to Q\bar{Q}g$",
    "ugUuU": r"$\bar{q}g \to \bar{q}q\bar{q}$",
    "uuUUg": r"$\bar{q}\bar{q} \to \bar{q}\bar{q}g$",
    "uUuUg": r"$\bar{q}q \to q\bar{q}g$",
}


def init_plots():
    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")


def write(fig, name, vector):
    suffix = "pdf" if vector else "png"
    filename = name + "." + suffix

    if pathlib.Path(filename).is_file():
        print(f"Overwriting existing file {filename}")
    else:
        print(f"Writing new file {filename}")

    if vector:
        fig.savefig(
            filename,
            bbox_inches="tight",
            format=suffix,
        )
    else:
        fig.savefig(
            filename,
            bbox_inches="tight",
            format=suffix,
            dpi=300,
        )


def histo_single(
    ax,
    data,
    channel,
    cutoff=1e-3,
    x_min_pow=-16,
    x_max_pow=2,
    num_bins=100,
    xlog=True,
    ylog=True,
    xlabel=True,
    ylabel=True,
    legend=True,
    tick_left=True,
    tick_bottom=True,
    tick_right=False,
    tick_top=False,
    cumulative=False,
    m=0.05,
):
    if cumulative:
        rescue = numpy.array([])
        for channels in data.values():
            error = channels[channel][:, 8]
            error = error[error < cutoff]
            rescue = numpy.concatenate((rescue, error))

    if xlog:
        base = 10
        ax.set_xscale("log", base=base)
        bins = numpy.logspace(x_min_pow, x_max_pow, num=num_bins, base=base)
    else:
        bins = num_bins

    for precision, channels in data.items():
        errors = channels[channel][:, 8] if channels[channel] != [] else numpy.array([])
        ax.hist(
            errors,
            bins=bins,
            histtype="step",
            log=ylog,
            label=PREC[precision],
        )

    if cumulative:
        _, _, patches = ax.hist(
            rescue,
            cumulative=-1,
            bins=bins,
            histtype="step",
            log=ylog,
            label="Cumulative",
            linestyle="dashed",
            color="black",
        )
        patches[0].set_xy(patches[0].get_xy()[1:])

    if xlabel:
        ax.set_xlabel("Correct digits")

    if ylabel:
        ax.set_ylabel("Frequency")

    ax.xaxis.set_major_formatter(lambda x, _: -int(math.log10(x)))

    ax.set_xticks(
        numpy.logspace(x_min_pow, x_max_pow, num=x_max_pow - x_min_pow + 1, base=10),
        minor=True,
    )
    ax.tick_params(axis="x", which="minor", labelbottom=False)

    ax.axvline(cutoff, color="black", linewidth=1)

    ax.tick_params(
        axis="both",
        direction="in",
        which="both",
        top=tick_top,
        bottom=tick_bottom,
        left=tick_left,
        right=tick_right,
    )

    ax.text(
        x=m,
        y=1 - m,
        s=CHAN[channel],
        transform=ax.transAxes,
        ha="left",
        va="top",
    )

    if legend:
        ax.legend(loc="upper right", frameon=False)
