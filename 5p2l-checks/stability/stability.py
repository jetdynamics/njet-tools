#!/usr/bin/env python3

import argparse, math, itertools

import numpy, matplotlib

import reader, plotter


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse stability of 5p 2L run over NJet phase space"
    )
    parser.add_argument(
        "-r", "--refresh-data", action="store_true", help="Don't use cached data"
    )
    parser.add_argument(
        "-i", "--individual", action="store_true", help="Plot individual plots"
    )
    parser.add_argument("-d", "--data", default="data_100k", help="Set data directory")
    args = parser.parse_args()

    print("Using", args.data)

    all_5p = reader.read_proc(args.data, "result", use_cache=not args.refresh_data)

    print("Plotting...")

    plotter.init_plots()

    channels = all_5p["f64f64"].keys()

    maxs = []
    mins = []
    for channel in channels:
        maxs.append(max(all_5p["f64f64"][channel][:, 8]))
        if (
            isinstance(all_5p["f128f128"][channel], numpy.ndarray)
            and all_5p["f128f128"][channel].size
        ):
            mins.append(min(all_5p["f128f128"][channel][:, 8]))

    max_pow = math.ceil(math.log10(max(maxs)))
    min_pow = math.floor(math.log10(min(mins)))

    if args.individual:
        w = 6.4
        r = 3 / 4

        for channel in channels:
            fig, ax = matplotlib.pyplot.subplots(figsize=(w, w * r))
            ax.invert_xaxis()
            plotter.histo_single(
                ax,
                all_5p,
                channel,
                x_min_pow=min_pow,
                x_max_pow=max_pow,
            )
            plotter.write(fig, f"stability_{channel}", vector=True)

    w = 4.8
    r = 2 / (1 + math.sqrt(5))

    fig, axs = matplotlib.pyplot.subplots(
        nrows=2,
        ncols=2,
        figsize=(2 * w, 2 * w * r),
        sharex=True,
        sharey=True,
    )

    axs[0][0].invert_xaxis()

    fig.subplots_adjust(
        wspace=0,
        hspace=0,
    )

    for i, (ax, channel) in enumerate(
        zip(itertools.chain.from_iterable(axs), ("ggggg", "qbgqgg", "uDdUg", "uuUUg"))
    ):
        left = i % 2 == 0
        bottom = i in (2, 3)

        plotter.histo_single(
            ax,
            all_5p,
            channel,
            x_min_pow=min_pow,
            x_max_pow=max_pow,
            legend=i == 0,
            xlabel=bottom,
            ylabel=left,
            tick_left=left,
            tick_bottom=bottom,
            tick_top=not bottom,
            tick_right=not left,
            cumulative=True,
        )

    plotter.write(fig, "stability_all", vector=True)
