#include "stability.hpp"

int main(int argc, char *argv[]) {
  std::cout << '\n';

  if (argc != 3) {
    std::cerr << "Run as `./stability <initial rseed (>0)> <final rseed "
                 "(exclusive)>`, where rseed is the random number seed for the "
                 "phase space point generator."
              << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int start{std::atoi(argv[1])};
  const int end{std::atoi(argv[2])};

  run<Amp4q1g_uDdUg_a2l>(start, end, "uDdUg");

  std::cout << '\n';
}
