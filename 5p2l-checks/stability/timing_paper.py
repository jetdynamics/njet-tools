#!/usr/bin/env python3

import argparse

import numpy

import reader, plotter


def perc(d_sf, d):
    return int(round(100 * d_sf / d, 0))


def print_results(channel, d_f64, d_f64_sf, d_res, d_res_sf, show_slowdown):
    dp = 2

    d_f64_f = round(d_f64, dp)
    r_f64_f = perc(d_f64_sf, d_f64)
    d_res_f = round(d_res, dp)
    r_res_f = perc(d_res_sf, d_res)
    slowdown = round(d_res / d_f64, dp)

    line = f"{plotter.CHAN[channel]} & {d_f64_f} & {r_f64_f} & {d_res_f} & {r_res_f}"
    if show_slowdown:
        line += f"& {slowdown}"
    line += r" \\"

    print(line)


def timing_rescue(
    data,
    i_err,
    i_sf,
    i_c,
    cutoff,
    show_slowdown,
):
    channels = plotter.CHAN.keys()

    for channel in channels:
        t_sf = []
        t_c = []

        for i, errDD, tDD_sf, tDD_c in data["f64f64"][channel][
            :, [0, i_err, i_sf, i_c]
        ]:
            tt_sf = tDD_sf
            tt_c = tDD_c

            if errDD > cutoff:
                matchQD = data["f128f64"][channel][data["f128f64"][channel][:, 0] == i]
                # this checks if there exists a QD result
                # this won't be true if one was running when I killed the data generation jobs
                if matchQD.size > 0:
                    rQD = matchQD[0, [i_sf, i_c, i_err]]

                    tt_sf += rQD[0]
                    tt_c += rQD[1]

                    if rQD[2] > cutoff:
                        matchQQ = data["f128f128"][channel][
                            data["f128f128"][channel][:, 0] == i
                        ]
                        # this checks if there exists a QQ result
                        if matchQQ.size > 0:
                            rQQ = matchQQ[0, [i_sf, i_c, i_err]]

                            tt_sf += rQQ[0]
                            tt_c += rQQ[1]

                            if rQQ[2] > cutoff:
                                print(
                                    f"Warning: rseed {int(i)} fails to achieve accuracy {cutoff} at f128/f128"
                                )
                        else:
                            print(f"Error: rseed {int(i)} is missing f128/f128 result!")
                else:
                    print(f"Error: rseed {int(i)} is missing f128/f64 result!")

            t_sf.append(tt_sf)
            t_c.append(tt_c)

        t_sf = numpy.array(t_sf)
        t_c = numpy.array(t_c)

        d_sf = numpy.average(t_sf) / 1e6
        d = numpy.average(t_sf + t_c) / 1e6

        t_f64_sf, t_f64_c = numpy.transpose(data["f64f64"][channel][:, [i_sf, i_c]])

        d_f64_sf = numpy.average(t_f64_sf) / 1e6
        d_f64 = numpy.average(t_f64_sf + t_f64_c) / 1e6

        print_results(channel, d_f64, d_f64_sf, d, d_sf, show_slowdown)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "analyse timing of 5p 2L run over NJet phase space"
    )
    parser.add_argument(
        "-r",
        "--refresh-data",
        action="store_true",
        help="Don't use cached data",
    )
    parser.add_argument(
        "-s",
        "--show-slowdown",
        action="store_true",
        help="Show slowdown factor of using evaluation strategy versus direct f64 evaluation",
    )
    parser.add_argument(
        "-d",
        "--data",
        metavar="D",
        type=str,
        default="data_100k",
        help="Set data directory",
    )
    parser.add_argument(
        "-c",
        "--cutoff",
        metavar="C",
        type=float,
        default=1e-3,
        help="Set accuracy threshold",
    )
    args = parser.parse_args()

    print("Using", args.data)

    all_5p = reader.read_proc(args.data, "result", use_cache=not args.refresh_data)

    timing_rescue(
        all_5p,
        i_err=8,
        i_sf=9,
        i_c=10,
        cutoff=args.cutoff,
        show_slowdown=args.show_slowdown,
    )
