#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"
#include "finrem/common/rescue.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

template <template <typename, typename> class AMP>
void run(const int start, const int end, const std::string &name) {
  const double sqrtS{1.};
  const double mur{91.188};
  const int legs{5};
  const int Nc{3};
  const int Nf{5};

  const double cutoff{1e-3};

  Rescue2LNNLO<AMP> amp(cutoff);
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(std::pow(mur, 2));

  // rseed = p
  for (int p{start}; p < end; ++p) {
    PhaseSpace<double> ps(legs, p, sqrtS);
    std::vector<MOM<double>> mom{ps.getPSpoint()};

    amp.setMomenta(mom);

    amp.eval_timed(p, name, std::to_string(start) + "-" + std::to_string(end));
  }
}

int main(int argc, char *argv[]) {
  std::cout << '\n';

  if (argc != 3) {
    std::cerr << "Run as `./stability <initial rseed (>0)> <final rseed "
                 "(exclusive)>`, where rseed is the random number seed for the "
                 "phase space point generator."
              << '\n';
    std::exit(EXIT_FAILURE);
  }

  const int start{std::atoi(argv[1])};
  const int end{std::atoi(argv[2])};

  run<Amp0q5g_ggggg_a2l>(start, end, "ggggg");

  run<Amp2q3g_ggqbqg_a2l>(start, end, "ggqbqg");
  run<Amp2q3g_qbgqgg_a2l>(start, end, "qbgqgg");
  run<Amp2q3g_qbqggg_a2l>(start, end, "qbqggg");

  run<Amp4q1g_uDdUg_a2l>(start, end, "uDdUg");
  run<Amp4q1g_udUDg_a2l>(start, end, "udUDg");
  run<Amp4q1g_ugUdD_a2l>(start, end, "ugUdD");
  run<Amp4q1g_ugUuU_a2l>(start, end, "ugUuU");

  run<Amp4q1g_uUdDg_a2l>(start, end, "uUdDg");
  run<Amp4q1g_uuUUg_a2l>(start, end, "uuUUg");
  run<Amp4q1g_uUuUg_a2l>(start, end, "uUuUg");

  std::cout << '\n';
}
