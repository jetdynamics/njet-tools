# Python library

import csv, pickle, pathlib, collections

import numpy


def read(filename):
    if not pathlib.Path(filename).is_file():
        print(f"File {filename} not found")
        exit()

    data = collections.defaultdict(list)

    with open(filename, "r") as filecont:
        for row in csv.reader(filecont, delimiter=" "):
            data[row[0]].append(numpy.array(row[1:], dtype="float64"))

    for channel, results in data.items():
        results = numpy.array(results)
        data[channel] = results[results[:, 0].argsort()]

    return data


def write(filename, data):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        print(f"File {picklefile} already exists.")
        while True:
            decision = input("Overwrite? [y/n] ")
            if decision == "n":
                return None
            elif decision == "y":
                break
    with open(picklefile, "wb") as f:
        pickle.dump(data, f)


def read_pickle(filename):
    picklefile = filename + ".pickle"
    if pathlib.Path(picklefile).is_file():
        with open(picklefile, "rb") as f:
            return pickle.load(f)


def read_proc(folder, name="result", use_cache=True):
    data = {
        "f64f64": read_memo(f"{folder}/{name}.f64f64.csv", use_cache),
        "f128f64": read_memo(f"{folder}/{name}.f128f64.csv", use_cache),
        "f128f128": read_memo(f"{folder}/{name}.f128f128.csv", use_cache),
    }
    print(f"Length of dataset: {len(data['f64f64']['ggggg'])}")
    return data


def read_memo(filename, use_cache):
    if use_cache:
        data = read_pickle(filename)
        if data is None:
            data = read(filename)
            write(filename, data)
    else:
        data = read(filename)
        write(filename, data)
    return data
