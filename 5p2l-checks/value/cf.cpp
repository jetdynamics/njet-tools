#include <array>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "chsums/NJetAccuracy.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"

template <typename T, typename P, template <typename, typename> class AMP>
void one(const std::string &name, const std::vector<MOM<T>> &momenta) {
  std::cout << name << '\n';

  const int Nc{3};
  const int Nf{5};
  const double mur2{1.};
  const int n{4};

  const std::array<std::string, n> names{{
      "B",
      "V",
      "VV[1Lx1L]",
      "VV[2Lx0L]",
  }};

  NJetAccuracy<T> *const acc{NJetAccuracy<T>::template create<AMP<T, P>>()};
  AMP<T, P> amp;

  acc->setMuR2(mur2);
  acc->setNf(Nf);
  acc->setNc(Nc);

  amp.setMuR2(mur2);
  amp.setNf(Nf);
  amp.setNc(Nc);

  acc->setMomenta(momenta);
  acc->initFinRem();
  acc->setSpecFuncs();

  amp.setMomenta(momenta);
  amp.initFinRem();
  amp.setSpecFuncs();

  acc->c0lx0l_fin();
  acc->c1lx0l_fin();
  acc->c1lx1l_fin();
  acc->c2lx0l_fin();

  const std::array<T, n> vacc{{
      acc->c0lx0l_fin_value(),
      acc->c1lx0l_fin_value(),
      acc->c1lx1l_fin_value(),
      acc->c2lx0l_fin_value(),
  }};

  const std::array<T, n> eacc{{
      std::abs(acc->c0lx0l_fin_error()),
      std::abs(acc->c1lx0l_fin_error()),
      std::abs(acc->c1lx1l_fin_error()),
      std::abs(acc->c2lx0l_fin_error()),
  }};

  const std::array<T, n> vacc0{{
      acc->getAmp(0)->c0lx0l_fin(),
      acc->getAmp(0)->c1lx0l_fin(),
      acc->getAmp(0)->c1lx1l_fin(),
      acc->getAmp(0)->c2lx0l_fin(),
  }};

  const std::array<T, n> vacc1{{
      acc->getAmp(1)->c0lx0l_fin(),
      acc->getAmp(1)->c1lx0l_fin(),
      acc->getAmp(1)->c1lx1l_fin(),
      acc->getAmp(1)->c2lx0l_fin(),
  }};

  const std::array<T, n> vamp{{
      amp.c0lx0l_fin(),
      amp.c1lx0l_fin(),
      amp.c1lx1l_fin(),
      amp.c2lx0l_fin(),
  }};

  for (int i{0}; i < n; ++i) {
    std::cout << names[i] << ":" << '\n'
              << "  AMP: " << std::setw(23) << vamp[i] << '\n'
              << "  ACC: " << std::setw(23) << vacc[i] << '\n'
	      << "    ACC 0: " << std::setw(23) << vacc0[i] << '\n'
	      << "    ACC 1: " << std::setw(23) << vacc1[i] << '\n'
	      << "    ABS E: " << std::setw(23) << eacc[i] << '\n'
	      << "    REL E: " << std::setw(23) << eacc[i]/vacc[i] << '\n';
  }

  std::cout << '\n';
}

template <typename T, typename P> void run() {
  std::cout << std::scientific << std::setprecision(16);

  const std::vector<double> scales2{{0.}};
  std::vector<MOM<T>> mom{
      {-0.575, 0, 0, -0.575},
      {-0.575, 0, 0, 0.575},
      {0.4588582395652174, -0.21459695305953932, 0., 0.4055848021739132},
      {0.23112940869565213, -0.061481922709097844, -0.20054010887525378,
       -0.09707956260869573},
      {0.46001235173913035, 0.27607887576863716, 0.20054010887525378,
       -0.30850523956521747},
  };
  refineM(mom, mom, scales2);

  // 5g
  // ~~

  // g g -> g g g
  one<T, P, Amp0q5g_ggggg_a2l>("ggggg", mom);

  // 2q3g
  // ~~~~

  // qb q -> g g g
  one<T, P, Amp2q3g_qbqggg_a2l>("qbqggg", mom);

  // qb g -> q g g
  one<T, P, Amp2q3g_qbgqgg_a2l>("qbgqgg", mom);

  // g g -> qb q g
  one<T, P, Amp2q3g_ggqbqg_a2l>("ggqbqg", mom);

  // 2q2Q1g
  // ~~~~~~

  // qb q -> Q Qb g
  one<T, P, Amp4q1g_uUdDg_a2l>("uUdDg", mom);

  // qb Q -> Q qb g
  one<T, P, Amp4q1g_uDdUg_a2l>("uDdUg", mom);

  // qb Qb -> qb Qb g
  one<T, P, Amp4q1g_udUDg_a2l>("udUDg", mom);

  // qb g -> qb Q Qb
  one<T, P, Amp4q1g_ugUdD_a2l>("ugUdD", mom);

  // 4q1g
  // ~~~~

  // qb q -> q qb g
  one<T, P, Amp4q1g_uUuUg_a2l>("uUuUg", mom);

  // qb qb -> qb qb g
  one<T, P, Amp4q1g_uuUUg_a2l>("uuUUg", mom);

  // q g -> qb q qb
  one<T, P, Amp4q1g_ugUuU_a2l>("ugUuU", mom);
}

int main() { run<double, double>(); }
