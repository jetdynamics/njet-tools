#include <array>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"
#include "finrem/common/rescue.h"
#include "ngluon2/Mom.h"

#include "cross_check.hpp"

template <template <typename, typename> class AMP>
void finite_remainder(
    const std::vector<std::vector<double>> &input_point,
    const std::string &name,
    const std::unordered_map<std::string, std::array<double, 5>> &expectation,
    const int Nf = 5, const int Nc = 3, const double mur2 = 1.,
    const double cutoff_error = 1e-2) {
  const double nf{static_cast<double>(Nf) / static_cast<double>(Nc)};

  Rescue2LNNLO<AMP> amp(cutoff_error);
  amp.setNf(Nf);
  amp.setNc(Nc);
  amp.setMuR2(mur2);

  amp.setMomenta(input_point);

  amp.eval();

  std::cout << "    Born:          " << std::setw(23) << amp.c0lx0l_value()
            << '\n'
            << "    Born error:    " << std::setw(23) << amp.c0lx0l_rel_error()
            << '\n'
            << "    Virt:          " << std::setw(23) << amp.c1lx0l_value()
            << '\n'
            << "    Virt error:    " << std::setw(23) << amp.c1lx0l_rel_error()
            << '\n'
            << "    Virtsq:        " << std::setw(23) << amp.c1lx1l_value()
            << '\n'
            << "    Virtsq error:  " << std::setw(23) << amp.c1lx1l_rel_error()
            << '\n'
            << "    Dblvirt:       " << std::setw(23) << amp.c2lx0l_value()
            << '\n'
            << "    Dblvirt error: " << std::setw(23) << amp.c2lx0l_rel_error()
            << '\n'
            << "    2L:            " << std::setw(23) << amp.c1lx1l_value() + amp.c2lx0l_value()
            << '\n'
            << "    <1L>:          " << std::setw(23)
            << (std::pow(nf, 0) * expectation.at(name)[0] +
                std::pow(nf, 1) * expectation.at(name)[1])
            << '\n'
            << "    <2L>:          " << std::setw(23)
            << (std::pow(nf, 0) * expectation.at(name)[2] +
                std::pow(nf, 1) * expectation.at(name)[3] +
                std::pow(nf, 2) * expectation.at(name)[4])
            << '\n';
}

int main() {
  std::cout << std::scientific << std::setprecision(16);
  std::cout << '\n';

  const std::vector<std::vector<double>> phase_space_point{{
      {-0.575, 0, 0, -0.575},
      {-0.575, 0, 0, 0.575},
      {0.4588582395652174, -0.21459695305953932, 0., 0.4055848021739132},
      {0.23112940869565213, -0.061481922709097844, -0.20054010887525378,
       -0.09707956260869573},
      {0.46001235173913035, 0.27607887576863716, 0.20054010887525378,
       -0.30850523956521747},
  }};

  finite_remainder<Amp4q1g_uUdDg_a2l>(phase_space_point, "uUdDg", xc::values);

  std::cout << '\n';
}
