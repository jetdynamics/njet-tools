#pragma once

#include <array>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

#include "finrem/0q5g/ggggg.h"
#include "finrem/2q3g/ggqbqg.h"
#include "finrem/2q3g/qbgqgg.h"
#include "finrem/2q3g/qbqggg.h"
#include "finrem/4q1g/uDdUg.h"
#include "finrem/4q1g/uUdDg.h"
#include "finrem/4q1g/uUuUg.h"
#include "finrem/4q1g/udUDg.h"
#include "finrem/4q1g/ugUdD.h"
#include "finrem/4q1g/ugUuU.h"
#include "finrem/4q1g/uuUUg.h"

#include "cross_check.hpp"

template <typename T> T getNfp1(const T x0, const T x1, const T x2) {
  return (-T(3.) * x0 + T(4.) * x1 - x2) / T(2.);
}

template <typename T> T getNfp2(const T x0, const T x1, const T x2) {
  return (x0 - T(2.) * x1 + x2) / T(2.);
}

template <typename T, typename P, template <typename, typename> class AMP>
void one(
    const std::string &name, const std::vector<MOM<T>> &momenta,
    const std::unordered_map<std::string, std::array<double, 5>> &expectation,
    const T cutoff, int &passed) {
  std::cout << name << '\n';
  AMP<T, P> amp;

  const int Nc{3};
  const int Nf{5};
  const T c1 = Nc;
  const T c2{c1 * c1};

  amp.setMuR2(1.);
  amp.setNf(Nf);
  amp.setNc(Nc);

  amp.setMomenta(momenta);
  amp.initFinRem();
  amp.setSpecFuncs();

  const T born{amp.c0lx0l_fin()};

  amp.setNf(0);
  const T virt_nfe0{amp.c1lx0l_fin()};
  const T vv_nfe0{amp.c1lx1l_fin() + amp.c2lx0l_fin()};

  amp.setNf(Nf);
  const T virt_nfe5{T(Nc) / T(Nf) * (amp.c1lx0l_fin() - virt_nfe0)};

  amp.setNf(1);
  const T vv_nfe1{amp.c1lx1l_fin() + amp.c2lx0l_fin()};

  amp.setNf(2);
  const T vv_nfe2{amp.c1lx1l_fin() + amp.c2lx0l_fin()};

  const std::array<T, 5> solution{{
      virt_nfe0 / born / c1,
      virt_nfe5 / born / c1,
      vv_nfe0 / born / c2,
      getNfp1(vv_nfe0, vv_nfe1, vv_nfe2) / born / T(Nc),
      getNfp2(vv_nfe0, vv_nfe1, vv_nfe2) / born,
  }};

  const std::array<std::string, 5> names{
      {"V[nf^0]", "V[nf^1]", "VV[nf^0]", "VV[nf^1]", "VV[nf^2]"}};

  for (int i{0}; i < 5; ++i) {
    const T delta{solution[i] - static_cast<T>(expectation.at(name)[i])};

    const bool pass{delta < cutoff};

    if (pass) {
      ++passed;
    }

    std::cout << std::setw(10) << names[i]
              << ":"
              //<< std::setw(24) << delta
              << std::setw(5) << (pass ? "pass" : "fail") << '\n';
  }

  std::cout << '\n';
}

template <typename T, typename P> void run(const T cutoff = 1e-6) {
  std::cout << std::scientific << std::setprecision(16);

  std::cout << '\n'
            << "evaluate as:" << '\n'
            << "  rational coefficients: " << typeid(T).name() << '\n'
            << "      special functions: " << typeid(P).name() << '\n';

  std::cout << '\n' << "difference cutoff: " << cutoff << '\n' << '\n';

  const std::vector<double> scales2{{0.}};
  std::vector<MOM<T>> mom{
      {-0.575, 0, 0, -0.575},
      {-0.575, 0, 0, 0.575},
      {0.4588582395652174, -0.21459695305953932, 0., 0.4055848021739132},
      {0.23112940869565213, -0.061481922709097844, -0.20054010887525378,
       -0.09707956260869573},
      {0.46001235173913035, 0.27607887576863716, 0.20054010887525378,
       -0.30850523956521747},
  };
  refineM(mom, mom, scales2);

  int passed{};

  // 5g
  // ~~

  // g g -> g g g
  one<T, P, Amp0q5g_ggggg_a2l>("ggggg", mom, xc::values, cutoff, passed);

  // 2q3g
  // ~~~~

  // qb q -> g g g
  one<T, P, Amp2q3g_qbqggg_a2l>("qbqggg", mom, xc::values, cutoff, passed);

  // qb g -> q g g
  one<T, P, Amp2q3g_qbgqgg_a2l>("qbgqgg", mom, xc::values, cutoff, passed);

  // g g -> qb q g
  one<T, P, Amp2q3g_ggqbqg_a2l>("ggqbqg", mom, xc::values, cutoff, passed);

  // 2q2Q1g
  // ~~~~~~

  // qb q -> Q Qb g
  one<T, P, Amp4q1g_uUdDg_a2l>("uUdDg", mom, xc::values, cutoff, passed);

  // qb Q -> Q qb g
  one<T, P, Amp4q1g_uDdUg_a2l>("uDdUg", mom, xc::values, cutoff, passed);

  // qb Qb -> qb Qb g
  one<T, P, Amp4q1g_udUDg_a2l>("udUDg", mom, xc::values, cutoff, passed);

  // qb g -> qb Q Qb
  one<T, P, Amp4q1g_ugUdD_a2l>("ugUdD", mom, xc::values, cutoff, passed);

  // 4q1g
  // ~~~~

  // qb q -> q qb g
  one<T, P, Amp4q1g_uUuUg_a2l>("uUuUg", mom, xc::values, cutoff, passed);

  // qb qb -> qb qb g
  one<T, P, Amp4q1g_uuUUg_a2l>("uuUUg", mom, xc::values, cutoff, passed);

  // q g -> qb q qb
  one<T, P, Amp4q1g_ugUuU_a2l>("ugUuU", mom, xc::values, cutoff, passed);

  std::cout << passed << " passed out of 55" << '\n' << '\n';
}
