#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-analytic.h"
// #include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
std::complex<T> runA(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const int Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);
    amp.setMomenta(mom.data());
    amp.setHelicity(hels.data());
    // amp.setpenta1l();
    // std::array<LoopResult<T>, 12> amps { amp.AL_penta() };

    // std::array<std::complex<T>, 12> trees { {
    //     amp.A0(0, 1, 2, 3, 4),
    //     amp.A0(0, 3, 4, 2, 1),
    //     amp.A0(0, 4, 2, 3, 1),
    //     amp.A0(0, 1, 3, 4, 2),
    //     amp.A0(0, 1, 4, 2, 3),
    //     amp.A0(0, 2, 3, 4, 1),
    //     amp.A0(0, 2, 1, 3, 4),
    //     amp.A0(0, 3, 4, 1, 2),
    //     amp.A0(0, 2, 3, 1, 4),
    //     amp.A0(0, 3, 1, 4, 2),
    //     amp.A0(0, 4, 2, 1, 3),
    //     amp.A0(0, 4, 1, 2, 3),
    // } };

    // std::array<Eps3<T>, 12> old_amps { {
    //     amp.AL(0, 1, 2, 3, 4).loop,
    //     amp.AL(0, 3, 4, 2, 1).loop,
    //     amp.AL(0, 4, 2, 3, 1).loop,
    //     amp.AL(0, 1, 3, 4, 2).loop,
    //     amp.AL(0, 1, 4, 2, 3).loop,
    //     amp.AL(0, 2, 3, 4, 1).loop,
    //     amp.AL(0, 2, 1, 3, 4).loop,
    //     amp.AL(0, 3, 4, 1, 2).loop,
    //     amp.AL(0, 2, 3, 1, 4).loop,
    //     amp.AL(0, 3, 1, 4, 2).loop,
    //     amp.AL(0, 4, 2, 1, 3).loop,
    //     amp.AL(0, 4, 1, 2, 3).loop,
    // } };

    // for (int i { 0 }; i < 12; ++i) {

    //     std::cout << "AL :" << old_amps[i]/trees[i] << '\n';
    //     std::cout << "ALp:" << amps[i].loop/trees[i] << '\n';
    // }
    // std::cout << "AL: " << amp.AL().loop << '\n';
    // std::cout << "ALp/A0:" << amp.ALp() / amp.A0() << '\n';
    const std::complex<T> tree { amp.A0() };
    std::cout << " A0: " << i_ * amp.A0() << '\n';
    std::cout << "cA0: " << std::conj(i_ * amp.A0()) << '\n';
    return tree;
}

// template <typename T>
// Eps3<T> runC(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const int Nc, const T mur)
// {
//     const T scale { 1. };
//     Amp0q5g_a<T> amp(scale);
//     amp.setNf(0);
//     amp.setNc(Nc);
//     amp.setMuR2(mur * mur);
//     amp.setMomenta(mom.data());

//     // std::cout << '\n';
//     // for (int i { 0 }; i < N; ++i) {
//     //     for (int j { i + 1 }; j < N; ++j) {
//     //         std::cout << "s" << i + 1 << j + 1 << "=" << dot(mom[i], mom[j]) * 2 << '\n';
//     //     }
//     // }

//     auto t1 { std::chrono::high_resolution_clock::now() };
//     Eps3<T> part { amp.virt_part(hels.data()) };
//     auto t2 { std::chrono::high_resolution_clock::now() };

//     Amp0q5g_a<T> amp2(scale);
//     amp2.setNf(0);
//     amp2.setNc(Nc);
//     amp2.setMuR2(mur * mur);
//     amp2.setMomenta(mom.data());

//     auto t5 { std::chrono::high_resolution_clock::now() };
//     amp2.setpenta1l();
//     auto t6 { std::chrono::high_resolution_clock::now() };

//     auto t3 { std::chrono::high_resolution_clock::now() };
//     Eps3<T> penta { amp2.virt_penta(hels.data()) };
//     auto t4 { std::chrono::high_resolution_clock::now() };

//     std::cout << "Pentagon evaluation time: " << std::chrono::duration_cast<std::chrono::microseconds>(t6 - t5).count() << "us\n";
//     std::cout << "colour sum\n"
//               << "part:  " << part
//               << "\nTime:  " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
//               << "penta: " << penta
//               << "\nTime:  " << std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count() << "us\n";

//     // return hamp_virt;
//     return std::complex<T>(1.);
// }

// template <typename T>
// Eps3<T> runH(const std::array<MOM<T>, N> mom, const int Nc, const T mur)
// {
//     const T scale { 1. };

//     Amp0q5g_a<T> amp(scale);
//     amp.setNf(0);
//     amp.setNc(Nc);
//     amp.setMuR2(mur * mur);
//     amp.setMomenta(mom.data());

//     auto t9 { std::chrono::high_resolution_clock::now() };
//     Eps3<T> partH { amp.virt_part() };
//     auto t10 { std::chrono::high_resolution_clock::now() };

//     Amp0q5g_a<T> amp2(scale);
//     amp2.setNf(0);
//     amp2.setNc(Nc);
//     amp2.setMuR2(mur * mur);
//     amp2.setMomenta(mom.data());

//     auto t5 { std::chrono::high_resolution_clock::now() };
//     amp2.setpenta1l();
//     auto t6 { std::chrono::high_resolution_clock::now() };

//     auto t7 { std::chrono::high_resolution_clock::now() };
//     Eps3<T> pentaH { amp2.virt_penta() };
//     auto t8 { std::chrono::high_resolution_clock::now() };

//     std::cout << "\nPentagon evaluation time: " << std::chrono::duration_cast<std::chrono::microseconds>(t6 - t5).count() << "us\n";
//     std::cout << "Helicity sum:\n"
//               << "part:  " << partH
//               << "\nTime:  " << std::chrono::duration_cast<std::chrono::microseconds>(t10 - t9).count() << "us\n"
//               << "penta: " << pentaH
//               << "\nTime:  " << std::chrono::duration_cast<std::chrono::microseconds>(t8 - t7).count() << "us\n";

//     return std::complex<T>(1.);
// }

template <typename T>
void loop_matrix(const int num_ps = 10, const int num_nc = 1, const int num_mur = 1)
{
    for (int i { 0 }; i < num_ps; ++i) {

        const std::array<MOM<T>, N> moms { random_point<T>() };

        for (int Nc { 3 }; Nc < 3 * std::pow(10, num_nc); Nc *= 10) {
            for (T mur { 1. }; mur < std::pow(10., num_mur); mur *= 10.) {
                for (int i0 { -1 }; i0 < 2; i0 += 2) {
                    for (int i1 { -1 }; i1 < 2; i1 += 2) {
                        for (int i2 { -1 }; i2 < 2; i2 += 2) {
                            for (int i3 { -1 }; i3 < 2; i3 += 2) {
                                for (int i4 { -1 }; i4 < 2; i4 += 2) {
                                    const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
                                    if (hels.order() < 2) {
                                        // const Helicity<N> hels { { 1, 1, 1, -1, -1 } };
                                        std::cout << hels << '\n';

                                        // std::cout << "{" << '\n';

                                        const std::complex<T> hamp_virt0 { runA<T>(hels, moms, Nc, mur) };
                                        // const Eps3<T> hamp_virt { runC<T>(hels, moms, Nc, mur) };

                                        // std::cout << mur << "," << '\n';

                                        // std::cout << Nc << "," << '\n';

                                        // std::cout << hamp_virt << '\n';

                                        // std::cout << "}," << '\n';
                                    }
                                }
                            }
                        }
                    }
                }

                // std::cout << "{" << '\n';
                // for (int j { 0 }; j < N - 1; ++j) {
                //     std::cout << moms[j] << ',' << '\n';
                // }
                // std::cout << moms[N - 1] << "\n},\n";

                // const Eps3<T> hamp_virt2 { runH<T>(moms, Nc, mur) };
            }
        }
    }
    // std::cout << '}' << '\n';
}

int main()
{
    std::cout.precision(8);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>(1, 1, 1);
}
