#!/usr/bin/env bash

MAIN=main_file_2l.cpp
rm -f ${MAIN}
touch ${MAIN}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

root="`git root`/5g@2l/2l-impl-final"
l=2
dir="${root}/exprs"
for f in $dir/P${l}_*.m; do
    # echo $f
    g=${f#$dir/P${l}_}
    h=${g%_lr*.m}
    d=${h}
    df=2l_hel_${d}
    mkdir -p ${df}

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $h_symb $h_char $j

    COEFFS=${df}/coeffs.cpp
    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${COEFFS}
    perl -p -e "s|SSSSS|$h_symb|g; s|CCCCC|$h_char|g;" template_coeffs_start_2l.cpp >>${COEFFS}
    cat ${root}/${d}/ys.cpp >>${COEFFS}
    cat ${root}/${d}/fs.cpp >>${COEFFS}
    echo "}" >>${COEFFS}
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${COEFFS}
    # clang-format -i ${COEFFS}

    for i in {1..12}; do
        FILE=${df}/eps_p${i}.cpp
        perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g;" template_perm_start_2l.cpp >>${FILE}
        cat ${root}/${d}/e${i}_odd.cpp >>${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g; s|SSSSS|$h|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g;" template_perm_middle_2l.cpp >>${FILE}
        cat ${root}/${d}/e${i}_even.cpp >>${FILE}
        perl -pi -e "s|DoubleLoopValue|Eps5<T>|g" ${FILE}
    done
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${FILE}
    # clang-format -i ${FILE}

    F=`cat ${root}/${d}/fs.fcount`
    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g; s|FFF|$F|g; s|CCCCC|$h_char|g;" template_2l.cpp >>${MAIN}
done

perl -pi -e "s|hA007\(|hA07(|g" ${MAIN}
# clang-format -i ${MAIN}
