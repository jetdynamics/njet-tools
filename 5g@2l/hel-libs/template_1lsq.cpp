// SSSSS
template <typename T>
std::array<LoopResult<Eps5o2<T>>, 12> Amp0q5g_a2l<T>::hAgHHe2() {
  hamp2vHH.hAg_coeffs(x1, x2, x3, x4, x5);

  const std::array<int, 5> o{0, 1, 2, 3, 4};
  const TreeValue phase{-i_ * hA0HH(o.data()) / hA0HHX()};

  std::array<Eps5o2<T>, 12> amps{
      hamp2vHH.hAg_p1e() + hamp2vHH.hAg_p1o(),   hamp2vHH.hAg_p2e() + hamp2vHH.hAg_p2o(),
      hamp2vHH.hAg_p3e() + hamp2vHH.hAg_p3o(),   hamp2vHH.hAg_p4e() + hamp2vHH.hAg_p4o(),
      hamp2vHH.hAg_p5e() + hamp2vHH.hAg_p5o(),   hamp2vHH.hAg_p6e() + hamp2vHH.hAg_p6o(),
      hamp2vHH.hAg_p7e() + hamp2vHH.hAg_p7o(),   hamp2vHH.hAg_p8e() + hamp2vHH.hAg_p8o(),
      hamp2vHH.hAg_p9e() + hamp2vHH.hAg_p9o(),   hamp2vHH.hAg_p10e() + hamp2vHH.hAg_p10o(),
      hamp2vHH.hAg_p11e() + hamp2vHH.hAg_p11o(), hamp2vHH.hAg_p12e() + hamp2vHH.hAg_p12o(),
  };

  for (Eps5o2<T> &amp : amps) {
    amp = phase * correction1Lsq(amp);
  }

  for (int i{0}; i < FFF; ++i) {
    hamp2vHH.f[i] = std::conj(hamp2vHH.f[i]);
  }

  std::array<Eps5o2<T>, 12> camps{
      hamp2vHH.hAg_p1e() - hamp2vHH.hAg_p1o(),   hamp2vHH.hAg_p2e() - hamp2vHH.hAg_p2o(),
      hamp2vHH.hAg_p3e() - hamp2vHH.hAg_p3o(),   hamp2vHH.hAg_p4e() - hamp2vHH.hAg_p4o(),
      hamp2vHH.hAg_p5e() - hamp2vHH.hAg_p5o(),   hamp2vHH.hAg_p6e() - hamp2vHH.hAg_p6o(),
      hamp2vHH.hAg_p7e() - hamp2vHH.hAg_p7o(),   hamp2vHH.hAg_p8e() - hamp2vHH.hAg_p8o(),
      hamp2vHH.hAg_p9e() - hamp2vHH.hAg_p9o(),   hamp2vHH.hAg_p10e() - hamp2vHH.hAg_p10o(),
      hamp2vHH.hAg_p11e() - hamp2vHH.hAg_p11o(), hamp2vHH.hAg_p12e() - hamp2vHH.hAg_p12o(),
  };

  const TreeValue cphase{std::conj(phase)};

  for (Eps5o2<T> &camp : camps) {
    camp = cphase * correction1Lsq(camp);
  }

  std::array<LoopResult<Eps5o2<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}

