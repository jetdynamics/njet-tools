
#ifdef USE_SD
template class Amp0q5g_a2v_CCCCC<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a2v_CCCCC<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a2v_CCCCC<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a2v_CCCCC<Vc::double_v>;
#endif
