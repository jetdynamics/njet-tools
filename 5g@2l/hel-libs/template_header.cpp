/*
* analytic/0q5g-2v-CCCCC-analytic.h
*
* This file is part of NJet library
* Copyright (C) 2020 NJet Collaboration
*
* This software is distributed under the terms of the GNU General Public License (GPL)
*/

#ifndef ANALYTIC_0Q5G_ANALYTIC_2V_CCCCC_H
#define ANALYTIC_0Q5G_ANALYTIC_2V_CCCCC_H

#include <array>
#include <complex>
#include <vector>

#include "pentagons.h"
#include "../ngluon2/utility.h"
#include "../ngluon2/EpsQuintuplet.h"

template <typename T>
class Amp0q5g_a2v_CCCCC {
public:
    using TreeValue = std::complex<T>;

    Amp0q5g_a2v_CCCCC(const Pentagons<T> &p_);

    std::vector<TreeValue> Z;
    std::vector<TreeValue> f;

    // { 1lsq

    void hAg_coeffs(TreeValue x1, TreeValue x2, TreeValue x3, TreeValue x4, TreeValue x5);

    Eps5o2<T> hAg_p1e();
    Eps5o2<T> hAg_p2e();
    Eps5o2<T> hAg_p3e();
    Eps5o2<T> hAg_p4e();
    Eps5o2<T> hAg_p5e();
    Eps5o2<T> hAg_p6e();
    Eps5o2<T> hAg_p7e();
    Eps5o2<T> hAg_p8e();
    Eps5o2<T> hAg_p9e();
    Eps5o2<T> hAg_p10e();
    Eps5o2<T> hAg_p11e();
    Eps5o2<T> hAg_p12e();

    Eps5o2<T> hAg_p1o();
    Eps5o2<T> hAg_p2o();
    Eps5o2<T> hAg_p3o();
    Eps5o2<T> hAg_p4o();
    Eps5o2<T> hAg_p5o();
    Eps5o2<T> hAg_p6o();
    Eps5o2<T> hAg_p7o();
    Eps5o2<T> hAg_p8o();
    Eps5o2<T> hAg_p9o();
    Eps5o2<T> hAg_p10o();
    Eps5o2<T> hAg_p11o();
    Eps5o2<T> hAg_p12o();

    // }{ 2l

    void hA2g_coeffs(TreeValue x1, TreeValue x2, TreeValue x3, TreeValue x4, TreeValue x5);

    Eps5<T> hA2g_p1e();
    Eps5<T> hA2g_p2e();
    Eps5<T> hA2g_p3e();
    Eps5<T> hA2g_p4e();
    Eps5<T> hA2g_p5e();
    Eps5<T> hA2g_p6e();
    Eps5<T> hA2g_p7e();
    Eps5<T> hA2g_p8e();
    Eps5<T> hA2g_p9e();
    Eps5<T> hA2g_p10e();
    Eps5<T> hA2g_p11e();
    Eps5<T> hA2g_p12e();

    Eps5<T> hA2g_p1o();
    Eps5<T> hA2g_p2o();
    Eps5<T> hA2g_p3o();
    Eps5<T> hA2g_p4o();
    Eps5<T> hA2g_p5o();
    Eps5<T> hA2g_p6o();
    Eps5<T> hA2g_p7o();
    Eps5<T> hA2g_p8o();
    Eps5<T> hA2g_p9o();
    Eps5<T> hA2g_p10o();
    Eps5<T> hA2g_p11o();
    Eps5<T> hA2g_p12o();

    // }{ kinematics

    const Pentagons<T> &p;

    // }
};

#endif /* ANALYTIC_0Q5G_ANALYTIC_2V_CCCCC_H */
