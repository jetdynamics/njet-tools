#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"
#include "tools/PhaseSpace.h"

// https://stackoverflow.com/a/20718842
template <std::size_t n>
int permutation_parity(const std::array<int, n>& data)
{
    int swaps { 0 };
    int seen { 0 };
    for (std::size_t i { 0 }; i < n; i++) {
        int mask = (1L << i);
        if ((seen & mask) != 0)
            continue;
        seen |= mask;
        for (int j { data[i] }; (seen & (1L << j)) == 0; j = data[j]) {
            seen |= (1L << j);
            swaps++;
        }
    }
    return ((swaps % 2) == 0) ? 1 : -1;
}

template <std::size_t n>
int levi_civita(const std::array<int, n>& data)
{
    for (std::size_t i { 0 }; i < n; i++) {
        for (std::size_t j { i + 1 }; j < n; j++) {
            if (data[i] == data[j])
                return 0;
        }
    }
    return permutation_parity(data);
}

template <typename T>
T tr5_im(const std::vector<MOM<T>>& moms)
{
    T tr5 { 4. };
    std::array<int, 4> s { { 0, 1, 2, 3 } };

    do {
        tr5 += permutation_parity(s) * moms[0].x(s[0]) * moms[1].x(s[1]) * moms[2].x(s[2]) * moms[3].x(s[3]);
    } while (std::next_permutation(s.begin(), s.end()));

    return tr5;
}

template <typename T>
T delta(const std::vector<T>& v)
{
    return pow(v[3], 2) * pow(v[4], 2) + v[2] * (v[2] * pow(v[3], 2) - 2 * pow(v[3], 2) * v[4]) + v[1] * (v[1] * pow(v[2], 2) + v[2] * (-2 * v[2] * v[3] + 2 * v[3] * v[4])) + v[0] * (2 * v[2] * v[3] * v[4] - 2 * v[3] * pow(v[4], 2) + v[0] * (v[1] * (v[1] - 2 * v[4]) + pow(v[4], 2)) + v[1] * (-2 * v[1] * v[2] + 2 * v[3] * v[4] + v[2] * (2 * v[3] + 2 * v[4])));
}

template <typename T>
T delta(const T s12, const T s23, const T s34, const T s45, const T s15)
{
    return pow(s45, 2) * pow(s15, 2) + s34 * (s34 * pow(s45, 2) - 2 * pow(s45, 2) * s15) + s23 * (s23 * pow(s34, 2) + s34 * (-2 * s34 * s45 + 2 * s45 * s15)) + s12 * (2 * s34 * s45 * s15 - 2 * s45 * pow(s15, 2) + s12 * (s23 * (s23 - 2 * s15) + pow(s15, 2)) + s23 * (-2 * s23 * s34 + 2 * s45 * s15 + s34 * (2 * s45 + 2 * s15)));
}

template <typename T>
const T zero { 1e-10 };

template <typename T, std::size_t mul>
inline void test_massless(const std::array<MOM<T>, mul>& momenta)
{
    std::for_each(momenta.cbegin(), momenta.cend(), [](const MOM<T>& m) { assert(abs(m.mass()) < zero<T>); });
}

template <typename T, std::size_t mul>
inline void test_mom_cons(const std::array<MOM<T>, mul>& momenta)
{
    MOM<T> sum { std::accumulate(momenta.cbegin(), momenta.cend(), MOM<T>()) };
    assert(sum.x0 < zero<T>);
    assert(sum.x1 < zero<T>);
    assert(sum.x2 < zero<T>);
    assert(sum.x3 < zero<T>);
}

template <typename T>
std::array<MOM<T>, 5> phase_space_point_x(const T x1, const T x2, const T theta, const T alpha, const T sqrtS = T(1.), int r = 0)
{
    assert(T(0.) <= x1);
    assert(T(0.) <= x2);
    assert(x1 + x2 <= T(2.));

    assert(x1 <= T(1.));
    assert(x2 <= T(1.));
    assert(x1 + x2 >= T(1.));

    assert(r >= 0);

    r %= 5;

    // cos(beta)
    const T cb { T(1.) + T(2.) * (T(1.) - x1 - x2) / x1 / x2 };
    assert((T(-1.) <= cb) && (cb <= T(1.)));
    // sin(beta)
    const T sb { sqrt(T(1.) - pow(cb, 2)) };

    const T ct { cos(theta) };
    const T st { sin(theta) };

    const T ca { cos(alpha) };
    const T sa { sin(alpha) };

    std::array<MOM<T>, 5> momenta { {
        sqrtS / T(2.) * MOM<T>(T(-1.), T(0.), T(0.), T(-1.)),
        sqrtS / T(2.) * MOM<T>(T(-1.), T(0.), T(0.), T(1.)),
        x1 * sqrtS / T(2.) * MOM<T>(T(1.), st, T(0.), ct),
        x2 * sqrtS / T(2.) * MOM<T>(T(1.), ca * ct * sb + cb * st, sa * sb, cb * ct - ca * sb * st),
        MOM<T>(),
    } };

    momenta[4] = -std::accumulate(momenta.cbegin(), momenta.cend() - 1, MOM<T>());

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}

template <typename T>
std::array<MOM<T>, 5> phase_space_point(const T y1, const T y2, const T theta, const T alpha, const T sqrtS = T(1.), int r = 0)
{
    assert(T(0.) <= y1);
    assert(T(0.) <= y2);
    assert(y1 + y2 <= T(1.));

    const T x1 { T(1.) - y1 };
    const T x2 { T(1.) - y2 };

    return phase_space_point_x(x1, x2, theta, alpha, sqrtS, r);
}

template <typename T>
inline T invariant(const std::vector<MOM<T>>& moms, const int i, const int j)
{
    return 2 * dot(moms[i], moms[j]);
}

template <typename T, std::size_t mul>
inline T invariant(const std::array<MOM<T>, mul>& moms, const int i, const int j)
{
    return 2 * dot(moms[i], moms[j]);
}

template <typename T>
inline void export_momenta(const std::vector<MOM<T>>& moms)
{
    std::cout
        << '{'
        << '\n';
    for (const MOM<T>& mom : moms) {
        std::cout
            << '{'
            << mom.x0 << ", "
            << mom.x1 << ", "
            << mom.x2 << ", "
            << mom.x3 << ", "
            << "},"
            << '\n';
    }
    std::cout
        << "},"
        << '\n';
}

template <typename T>
inline void print_momenta(const std::vector<MOM<T>>& moms)
{
    for (const MOM<T>& mom : moms) {
        std::cout << mom.x0 << ' ';
        std::cout << mom.x1 << ' ';
        std::cout << mom.x2 << ' ';
        std::cout << mom.x3 << ' ';
        std::cout << '\n';
    }
}

template <typename T>
inline void print_invariant_ratios(const std::vector<MOM<T>>& moms)
{
    const T s12 { invariant(moms, 0, 1) };
    T smallest { std::numeric_limits<T>::max() };
    std::string name;
    for (std::size_t i { 0 }; i < moms.size(); ++i) {
        for (std::size_t j { i + 1 }; j < moms.size(); ++j) {
            if (i == 0 && j == 1) {
                continue;
            }
            const T sij { invariant(moms, i, j) };
            if (abs(sij) < smallest) {
                smallest = abs(sij);
                name = "s" + std::to_string(i + 1) + std::to_string(j + 1) + "/s12";
            }
            std::cout << "s" << i + 1 << j + 1 << "/s12  = " << (sij < 0. ? "" : " ") << sij / s12 << '\n';
        }
    }
    std::cout
        << "smallest momentum invariant:" << '\n'
        << name << " =  " << smallest << '\n';
}

template <typename T>
inline void print_spurious_poles(const std::vector<MOM<T>>& moms)
{
    T smallest { std::numeric_limits<T>::max() };
    std::string name;
    for (std::size_t i { 0 }; i < moms.size(); ++i) {
        for (std::size_t j { i + 1 }; j < moms.size(); ++j) {
            for (std::size_t k { 0 }; k < moms.size(); ++k) {
                for (std::size_t l { k + 1 }; l < moms.size(); ++l) {
                    // if ((i == k && j == l) || ((std::pow(moms.size(), i) + j) > (std::pow(moms.size(), k) + l))) {
                    if ((i == k && j == l)) {
                        continue;
                    }
                    const T diff { 1. - invariant(moms, k, l) / invariant(moms, i, j) };
                    std::cout << "1-s" << k + 1 << l + 1 << "/s" << i + 1 << j + 1 << "  = " << (diff < 0. ? "" : " ") << diff << '\n';
                    if (abs(diff) < smallest) {
                        smallest = abs(diff);
                        name = "1-s" + std::to_string(k + 1) + std::to_string(l + 1) + "/s" + std::to_string(i + 1) + std::to_string(j + 1);
                    }
                }
            }
        }
    }
    std::cout
        << "smallest spurious pole:" << '\n'
        << name << " =  " << smallest << '\n';
}

template <typename T>
inline std::array<MOM<T>, 5> random_point(const bool test = false, const T sqrtS = T(1.), int r = 0)
{
    T y1;
    T y2;
    T theta;
    T alpha;

    if (test) {
        y1 = 2.5466037534799052e-01;
        y2 = 3.9999592899158604e-01;
        theta = 1.2398476487204491e+00;
        alpha = 4.3891163982516607e-01;
    } else {
        std::random_device dev;
        std::mt19937_64 rng(dev());
        const T boundary { 0.01 };
        std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
        std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

        y1 = dist_y1(rng);
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        y2 = dist_y2(rng);
        theta = dist_a(rng);
        alpha = dist_a(rng);
    }

    return phase_space_point(y1, y2, theta, alpha, sqrtS, r);
}

template <typename T>
inline std::vector<MOM<T>> njet_random_point(const int rseed, const double sqrtS = 1., const double ptMin = 0.003)
{
    const int legs { 5 };
    PhaseSpace<T> ps(legs, rseed, sqrtS, ptMin);
    return ps.getPSpoint();
}

// convert momenta to NJet format
template <typename T>
std::vector<MOM<T>> to_njet_momenta(const std::vector<std::vector<double>>& momenta)
{
    const std::vector<double> scales2 { { 0 } };

    std::vector<MOM<T>> njet_momenta(momenta.size());
    for (std::size_t i { 0 }; i < momenta.size(); ++i) {
        njet_momenta[i] = MOM<T>(
            momenta[i][0],
            momenta[i][1],
            momenta[i][2],
            momenta[i][3]);
    }

    refineM(njet_momenta, njet_momenta, scales2);

    return njet_momenta;
}

namespace PhysMom {
template <typename T> struct Point {
    std::vector<std::vector<T>> momenta;
    T prefactor;
    T alpha_s;
    T alpha;
    T mu;
    T bare_me;
    T full_me;
};

template <typename T>
class Data {
public:
    Data(const std::string& filename, const int m_ = 5, const int d_ = 4, const std::string& delim = " ")
        : points()
        , d(d_)
        , m(m_)
        , delimiter(delim)
    {
        std::cout << "Reading data from file `" << filename << "`..." << '\n';

        std::ifstream data(filename);
        std::string line;
        std::vector<std::vector<T>> moms(m, std::vector<T>(d));
        Point<T> point;
        int j { 0 };

        while (std::getline(data, line)) {
            if (j == 0) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.prefactor = std::stod(line);
                ++j;
            } else if (j == 1) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.alpha_s = std::stod(line);
                ++j;
            } else if (j == 2) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.alpha = std::stod(line);
                ++j;
            } else if (j > 2 && j < 3 + m) {
                std::vector<T> mom(d);
                int i { 0 };
                while (i < d) {
                    std::size_t pos { line.find(delimiter) };
                    std::string comp { line.substr(0, pos) };
                    line.erase(0, pos + delimiter.length());
                    if (comp != "") {
                        mom[i++] = (j < 5 ? -1 : +1) * std::stod(comp);
                    }
                }
                moms[j++ - 3] = mom;
            } else if (j == 3 + m) {
                std::size_t pos { line.find(delimiter) };
                line.erase(0, pos + delimiter.length());
                point.mu = std::stod(line);
                ++j;
            } else if (j == m + 4) {
                for (int i { 0 }; i < 2; ++i) {
                    std::size_t pos { line.find(delimiter) };
                    line.erase(0, pos + delimiter.length());
                }
                point.bare_me = std::stod(line);
                ++j;
            } else if (j == m + 5) {
                for (int i { 0 }; i < 2; ++i) {
                    std::size_t pos { line.find(delimiter) };
                    line.erase(0, pos + delimiter.length());
                }
                point.full_me = std::stod(line);
                ++j;
            } else {
                point.momenta = moms;
                points.push_back(point);
                j = 0;
            }
        }

        std::cout << "  Read " << points.size() << " points" << '\n';
    };

    std::vector<Point<T>> points;

private:
    const int d;
    const int m;
    const std::string delimiter;
};

template <typename T>
void printMom(const std::vector<std::vector<T>>& pt)
{
    for (const std::vector<T>& mom : pt) {
        for (const T& comp : mom) {
            std::cout << comp << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

}
