#include <array>
//#include <cassert>
//#include <chrono>
//#include <complex>
#include <iostream>
//#include <random>

#include "analytic/0q5g-analytic.h"
#include "finrem/0q5g/0q5g-2l.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

//template <typename T>
//T invariant(const std::array<MOM<T>, N>& moms, const int i, const int j)
//{
//    return 2 * dot(moms[i], moms[j]);
//}

template <typename T, typename P>
void loop(const int num_ps = 1)
{
    const int Nc { 3 };
    const int Nf { 0 };
    const T mur { 1. };
    const T scale { 1. };

    // can't compare old virt since new virt is a finite remainder and old virt is full virtual contribution to amplitude (with poles)
    //Amp0q5g_a<T> old(scale);
    //old.setNf(Nf);
    //old.setNc(Nc);
    //old.setMuR2(mur * mur);

    //auto t4 { std::chrono::high_resolution_clock::now() };
    Amp0q5g_a2l<T, P> amp(scale);
    //auto t5 { std::chrono::high_resolution_clock::now() };
    //std::cout
    //    << "Init time:  " << std::chrono::duration_cast<std::chrono::milliseconds>(t5 - t4).count() << "ms"
    //    << '\n'
    //    << '\n';

    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);

    //std::random_device dev;
    //std::mt19937_64 rng(dev());
    //const T boundary { 0.01 };
    //std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    //std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

        T y1;
        T y2;
        T theta;
        T alpha;

    for (int i { 0 }; i < num_ps; ++i) {

        //y1 = dist_y1(rng);
        //std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        //y2 = dist_y2(rng);
        //theta = dist_a(rng);
        //alpha = dist_a(rng);

        // std::cout << y1 << " "
        //           << y2 << " "
        //           << theta << " "
        //           << alpha << " "
        //           << '\n';

        // point A
        y1 = 2.5466037534799052e-01;
        y2 = 3.9999592899158604e-01;
        theta = 1.2398476487204491e+00;
        alpha = 4.3891163982516607e-01;

        const std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

        // std::cout << '\n';
        // for (int i { 0 }; i < N; ++i) {
        //     for (int j { i + 1 }; j < N; ++j) {
        //         std::cout << "s" << i + 1 << j + 1 << "=" << invariant(moms, i, j) * 2 << '\n';
        //     }
        // }

        //std::cout
        //    << "Invariants:"
        //    << '\n'
        //    << '('
        //    << invariant(moms, 0, 1) << ", "
        //    << invariant(moms, 1, 2) << ", "
        //    << invariant(moms, 2, 3) << ", "
        //    << invariant(moms, 3, 4) << ", "
        //    << invariant(moms, 4, 0)
        //    << ')'
        //    << '\n'
        //    << '\n';

        //old.setMomenta(moms.data());

        amp.setMomenta(moms.data());
        //auto t0 { std::chrono::high_resolution_clock::now() };
        amp.setSpecFuncs();
        //auto t1 { std::chrono::high_resolution_clock::now() };

        amp.initFinRem();

        //std::cout
        //    << "SFeval+SFmons time:  " << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() << "ms"
        //    << '\n'
        //    << '\n';

	    //T old_virt { amp.virt().get0().real() };

	    T virt { amp.c1lx0l_fin() };
	    //T virtsq { amp.virtsq_fin() };

	    std::cout
		<< "Helicity sum:" << '\n'
		//<< "old virt:      " << old_virt << '\n'
		<< "new virt:      " << virt << '\n'
		//<< "virtsq:    " << virtsq << '\n'
		;

	    std::cout
		<< '\n';

        for (int i0 { -1 }; i0 < 2; i0 += 2) {
            for (int i1 { -1 }; i1 < 2; i1 += 2) {
                for (int i2 { -1 }; i2 < 2; i2 += 2) {
                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
                            const Helicity<N> hels(i4, i3, i2, i1, i0);
                            // const Helicity<N> hels { { -1, -1, +1, +1, +1 } };
                            if (hels.sign() == 1) { // MHVs
                                std::cout << hels << '\n';
                                //amp.setHelicity(hels.data());

                                // std::array<LoopResult<std::complex<T>>, 12> ploops { amp.ALp() };
                                // const std::complex<T> p1 { ploops[0].loop };

				//T h_old_virt { amp.virt(hels.data()).get0().real() };

                                // auto t2 { std::chrono::high_resolution_clock::now() };
                                T h_virt { amp.c1lx0l_fin(hels.data()) };
                                // auto t3 { std::chrono::high_resolution_clock::now() };

                                //T virtsq { amp.virtsq_fin(hels.data()) };

                                std::cout
                                    // << "A1(12345): " << p1 << '\n'
                                    //<< "old virt:      " << h_old_virt << '\n'
                                    << "new virt:      " << h_virt << '\n'
                                    //<< "virtsq:    " << virtsq << '\n'
                                    ;

                                // std::cout
                                //     << "Virt time:  " << std::chrono::duration_cast<std::chrono::microseconds>(t3 - t2).count() << "us"
                                //     << '\n';
                            }
                        }
                    }
                }
            }
        }
    }
    std::cout
        << '\n';
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    loop<double, double>();
}
