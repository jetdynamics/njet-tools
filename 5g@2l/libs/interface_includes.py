#!/usr/bin/env python3


def entry(name, headers):
    return (
        f"#ifdef NJET_ENABLE_AN{name.upper()}\n"
        + "".join([f'#include "../analytic/{header}"\n' for header in headers])
        + "#endif\n"
    )


with open("libs", "r") as f:
    headers = [line.strip().split(" ") for line in f]

bases = [[h[:-11] for h in hs] for hs in headers]
procs = [p[0].replace("-", "") for p in bases]

for p, hs in zip(procs, headers):
    print(entry(p, hs))
