#!/usr/bin/env python3

from static import hels

with open("Makefile.2L.am", "w") as fle:

    for hel in hels:
        fle.write(
            f"""
lib_LTLIBRARIES += libnjet2an0q5g2v{hel}.la
libnjet2an0q5g2v{hel}_la_SOURCES = blank-staticfix.cpp
libnjet2an0q5g2v{hel}SD_la_SOURCES = 0q5g-2v-{hel}-analytic.cpp
EXTRA_libnjet2an0q5g2v{hel}SD_la_SOURCES = 0q5g-2v-{hel}-analytic.h
libnjet2an0q5g2v{hel}SD_la_CPPFLAGS = -DUSE_SD $(PENTAGON_FUNCTIONS_CFLAGS) $(EIGEN_CFLAGS)
libnjet2an0q5g2v{hel}SD_la_CXXFLAGS = -O0
libnjet2an0q5g2v{hel}VC_la_SOURCES = $(libnjet2an0q5g2v{hel}SD_la_SOURCES)
libnjet2an0q5g2v{hel}VC_la_CPPFLAGS = -DUSE_VC $(PENTAGON_FUNCTIONS_CFLAGS) $(EIGEN_CFLAGS)
libnjet2an0q5g2v{hel}VC_la_CXXFLAGS = -O0
libnjet2an0q5g2v{hel}DDSD_la_SOURCES = $(libnjet2an0q5g2v{hel}SD_la_SOURCES)
libnjet2an0q5g2v{hel}DDSD_la_CPPFLAGS = -DUSE_DD -DUSE_DDSD $(PENTAGON_FUNCTIONS_CFLAGS) $(EIGEN_CFLAGS)
libnjet2an0q5g2v{hel}DDSD_la_CXXFLAGS = -O0
libnjet2an0q5g2v{hel}DDDD_la_SOURCES = $(libnjet2an0q5g2v{hel}SD_la_SOURCES)
libnjet2an0q5g2v{hel}DDDD_la_CPPFLAGS = -DUSE_DD -DUSE_DDDD $(PENTAGON_FUNCTIONS_CFLAGS) $(EIGEN_CFLAGS)
libnjet2an0q5g2v{hel}DDDD_la_CXXFLAGS = -O0
libnjet2an0q5g2v{hel}QD_la_SOURCES = $(libnjet2an0q5g2v{hel}SD_la_SOURCES)
libnjet2an0q5g2v{hel}QD_la_CPPFLAGS = -DUSE_QD $(PENTAGON_FUNCTIONS_CFLAGS) $(EIGEN_CFLAGS)
libnjet2an0q5g2v{hel}QD_la_CXXFLAGS = -O0
libnjet2an0q5g2v{hel}_la_LIBADD = libnjet2an0q5g2v{hel}SD.la libpentagons.la
libnjet2an0q5g2v{hel}_la_DEPENDENCIES = libnjet2an0q5g2v{hel}SD.la libpentagons.la
if ENABLE_VC
libnjet2an0q5g2v{hel}_la_LIBADD += libnjet2an0q5g2v{hel}VC.la
libnjet2an0q5g2v{hel}_la_DEPENDENCIES += libnjet2an0q5g2v{hel}VC.la
endif
if ENABLE_DD
libnjet2an0q5g2v{hel}_la_LIBADD += libnjet2an0q5g2v{hel}DDSD.la
libnjet2an0q5g2v{hel}_la_DEPENDENCIES += libnjet2an0q5g2v{hel}DDSD.la
libnjet2an0q5g2v{hel}_la_LIBADD += libnjet2an0q5g2v{hel}DDDD.la
libnjet2an0q5g2v{hel}_la_DEPENDENCIES += libnjet2an0q5g2v{hel}DDDD.la
endif
if ENABLE_QD
libnjet2an0q5g2v{hel}_la_LIBADD += libnjet2an0q5g2v{hel}QD.la
libnjet2an0q5g2v{hel}_la_DEPENDENCIES += libnjet2an0q5g2v{hel}QD.la
endif
        """
        )
