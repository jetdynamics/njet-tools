#!/usr/bin/env python3

def entry(name):
    return f"""
AC_ARG_ENABLE(an{name},[AS_HELP_STRING([--enable-an{name}],
            [Compile analytic {name} amplitude @<:@default=no@:>@])],
            [], [enable_an{name}=no])
test "x$enable_an{name}" == xyes && AC_DEFINE(NJET_ENABLE_AN{name.upper()},1,"enable an{name}")
AM_CONDITIONAL(ENABLE_AN{name.upper()}, test "x$enable_an{name}" == xyes)"""

with open("libs", "r") as f:
    headers = [line.strip().split(" ") for line in f]

bases = [[h[:-11] for h in hs] for hs in headers]
procs = [p[0].replace("-", "") for p in bases]

for p in procs:
    print(entry(p))
