#!/usr/bin/env bash

OUT=$2

I="
template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAgPh28oXX()
{
"
F="
const LoopResult<T> res { amp, conj(amp) };
return res;
}
"

rm -f ${OUT}

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|\{\{\{|${I}|g;
        s|\{\{|${I}|g;
        s|\}\},|);${F}|g;
        s|\}\}\}|);${F}|g;
        s|,[\s]+f\[(\d+), \"?([\ddsmp]+)\"?\] ->|};\nconst TreeValue g_\1_\2 {|g;
        s|\{\n?f\[(\d+), \"?([\ddsmp]+)\"?\] ->|{\n// {\nconst TreeValue g_\1_\2 {|g;
        s|\},[\s\n]*\{|};\n// }\nconst LoopValue amp(|g;
        s|ex\[(\d)\]|x\1|g;
        s|f\[(\d+), \"?([\ddsmp]+)\"?\]|g_\1_\2|g;
        s|F\[(\d+), (\d+)\]|fv_\1_\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fv_\1_\2_\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|pow\[Pi, 2\]|PI2|g;
        s|(?s)pow\[(.*?),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|g;
        s|pow\[(.*),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|g;
        s|Log\[MuR2\]|log(MuR2())|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|(/)[\s\n]*(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        " \
    $1 > ${OUT}

        # s|\{\{|${I}const LoopValue amp(|g;
        # s|\},[\s\n]*\{|);${F}${I}const LoopValue amp(|g;
        # s|\}\}|);${F}|g;

for i in {00..11}; do perl -0777pi -e "s|XX|$i|;" ${OUT}; done
