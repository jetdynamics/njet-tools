#!/usr/bin/env bash

OUT=penta1.c
OUT2=penta2.c

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|\{?F\[(\d), ?(\d+)\],?\}?[\s\n]*|PentagonFunctions::FunctionObjectType<T> F_\1_\2 { PentagonFunctions::FunctionID(\1, \2).get_evaluator<T>() };\n|g;
        s|\{?F\[(\d), ?(\d+), ?(\d+)\],?\}?[\s\n]*|PentagonFunctions::FunctionObjectType<T> F_\1_\2_\3 { PentagonFunctions::FunctionID(\1, \2, \3).get_evaluator<T>() };\n|g;
        " \
    $1 > ${OUT}

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|\{?F\[(\d), ?(\d+)\],?\}?[\s\n]*|TreeValue fv_\1_\2;\n|g;
        s|\{?F\[(\d), ?(\d+), ?(\d+)\],?\}?[\s\n]*|TreeValue fv_\1_\2_\3;\n|g;
        " \
    $1 >> ${OUT}

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|\{?F\[(\d), ?(\d+)\],?\}?[\s\n]*|fv_\1_\2 = F_\1_\2(k);\n|g;
        s|\{?F\[(\d), ?(\d+), ?(\d+)\],?\}?[\s\n]*|fv_\1_\2_\3 = F_\1_\2_\3(k);\n|g;
        " \
    $1 > ${OUT2}
