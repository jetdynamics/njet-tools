#!/usr/bin/env bash

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)||g;
        s|,[\s]+f\[(\d+), \"?([\ddsmp]+)\"?\] ->|};\nconst TreeValue g_\1_\2 {|g;
        s|\{f\[(\d+), \"?([\ddsmp]+)\"?\] ->|// {\nconst TreeValue g_\1_\2 {|g;
        s|ex\[(\d)\]|x\1|g;
        s|f\[(\d+), \"?([\ddsmp]+)\"?\]|g_\1_\2|g;
        s|\},\s*\n\s*\{|};\n// }\n{|g;
        s|F\[(\d+), (\d+)\]|fv_\1_\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fv_\1_\2_\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|([^ *+-/()]+)\^(\d+)|njet_pow(\1, \2)|g;
        s|(\(.*?)\)\^(\d+)|njet_pow\1, \2)|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        " \
    $1
