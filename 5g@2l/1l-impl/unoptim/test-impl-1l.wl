(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/1L_5g_analyticpartialamps"];


partialamps = {
{1,2,3,4,5},
{1,4,5,3,2},
{1,5,3,4,2},
{1,2,4,5,3},
{1,2,5,3,4},
{1,3,4,5,2},
{1,3,2,4,5},
{1,4,5,2,3},
{1,3,4,2,5},
{1,4,2,5,3},
{1,5,3,2,4},
{1,5,2,3,4}
}-1


mmppp=Get["P1_mm+++.m"];


mmppp//Dimensions


mmppp[[1,1]]


mmppp[[1,2]]


mmppp[[1,3]]


mmppp[[2,1]]


mmppp[[2]]
