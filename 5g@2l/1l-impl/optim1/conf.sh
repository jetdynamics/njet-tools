#!/usr/bin/env bash

mkdir output

for f in P1_*; do
    g=${f#P1_}
    h=${g%_lr_apart.m}
    mkdir $h
    m=proc-optim-$h.wl
    cp proc-optim-HHHHH.wl $h/$m
    cd $h
    perl -pi -e "s|HHHHH|$h|g" $m
    math -script $m
    ../proc.sh
    cp end.cpp ../output/0q5g-analytic-1l-$h.cpp
    cd ..
done
