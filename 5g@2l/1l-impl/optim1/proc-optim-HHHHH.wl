(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/optim1/HHHHH"];


data=Get["../P1_HHHHH_lr_apart.m"];


epsTrip2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res2=epsTrip2/@Range[12];


Export["eps.m",res2];
Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];
