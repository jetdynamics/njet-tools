(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


data=Get["../P1_m+m++_lr_apart.m"];


epsTrip2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res2=epsTrip2/@Range[12];


Export["eps.m",res2];
Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];
