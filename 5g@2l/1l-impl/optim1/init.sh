#!/usr/bin/env bash

l="
07
11
19
13
21
25
14
22
26
28
"

for li in ${l[@]};do 
    echo "template <typename T>
std::array<typename NJetAmp5<T>::LoopValue, 12> NJetAmp5<T>::hAgP${li}()
{
    return std::array<LoopValue, 12>();
}
"
done

for li in ${l[@]};do 
    echo "using BaseClass::hAgP${li};"
done

for li in ${l[@]};do 
    echo "virtual std::array<LoopValue, 12> hAgP${li}();"
done

for li in ${l[@]};do 
    echo "virtual std::array<LoopValue, 12> hAgP${li}() override;"
done

for li in ${l[@]};do 
    echo "hAgP[${li}] = &Amp0q5g_a<T>::hAgP${li};"
done
