(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/optim1/HHHHH2"];


data=Get["../P1_HHHHH_lr_apart.m"];


epsTrip1[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res1=epsTrip1/@Range[12];


all=Join[Reverse[data[[2;;3]]],{res1}];
Export["all.m",all//.{Power[a_,b_]->pow[a,b]}];
