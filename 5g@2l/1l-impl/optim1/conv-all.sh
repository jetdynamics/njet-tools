#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
OUT=${BASE}.c

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|F\[(\d+), (\d+), (\d+)\]|fv_\1_\2_\3|g;
        s|F\[(\d+), (\d+)\]|fv_\1_\2|g;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|pow\[(.*?), (-?\d+)\]|pow(\1, \2)|gs;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*([yf])|const TreeValue \1\{\2 \};\n\3|gs;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*(c)|const TreeValue \1\{\2 \};\n\3|gs;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*([yf])|const TreeValue \1\{\2 \};\n\3|gs;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*(\{)|const TreeValue \1\{\2 \};\n\3|gs;
        s|\},[\s\n]*\{|),\nLoopValue(|g;
        s|\{\{|std::array<LoopValue, 12> amps{\nLoopValue(|g;
        s|\}\}\}|)};|g;
        s|([ (\-={\/] ?)(-?\d+)\.?( ?[*\/;+\-\n,\}])|\1T(\2.)\3|g;
        s|([\/] ?)(-?\d+)( ?[)])|\1T(\2.)\3|g;
        " \
    ${IN} >${OUT}

clang-format -i ${OUT}
