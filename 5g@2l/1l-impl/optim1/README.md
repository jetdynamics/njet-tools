* Remember FORM is unity-indexed
    * diphoton has script to fix this
* scripts
    * conv-all : does ys,fs,ep3s
    * conv-form-f : obv
    * conv-form-y : obv
    * conv-form : everything, with FORM
    * conv-form2 : ?
    * conv-form3 : ys & fs (broken?)
    * pp-form : post process FORM
* FORM doesn't seem to help eval time with virt_penta
