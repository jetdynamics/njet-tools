    const std::array<int, 5> o { 0, 1, 2, 3, 4 };
    const TreeValue phase {
        -i_ * hA028(o.data()) / (x3 * njet_pow(x1, 3) * njet_pow(x2, 2))
    };

    for (LoopValue& amp : amps) {
        amp = phase * correction(amp);
    }

    return amps;
}

#ifdef USE_SD
template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a<Vc::double_v>;
#endif
