#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c

echo -e "#-\nS u,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|([f])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{{0,2}([f]\d+ )->( .*?)\}?,[\s\n]*|L \1=\2;\n|gs;
        s|(f\d+ )->( .*?)\}|L \1=\2;\n|gs;
        " \
    ${IN} >>${FRM}

echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

# fs
h="L H="
i=1
fs=$(rg -o "f\d+" $FRM)
# fsr=$(rg -o "f\d+" $FRM)
# declare -A fs
# for f in $fsr; do
#     fs[$f]=1
# done

for f in ${fs[@]}; do
    h+="+u^$((i++))*$f"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for f in ${fs[@]}; do
    echo "L ${f}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$C> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for f in ${fs[@]}; do
    echo "#write <$C> \"const TreeValue $f{%E};\n\", ${f}a" >>$FRM
done

echo ".end" >>$FRM
