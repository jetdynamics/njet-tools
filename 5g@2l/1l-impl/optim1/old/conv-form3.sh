#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=tmp

echo -e "#-\nS u,v,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*|L \1=\2;\n|gs;
        s|([yf]\d+ )->( .*?)\}{1,2}|L \1=\2;\n|gs;
        " \
    ${IN} >${TMP}


# ys
h="L H="
i=1
ysr=$(rg -o "(y\d+) =" $FRM)
declare -a ys
for y in ${ysr[@]}; do
    ys+=( "${y%*=}" )
done
for y in ${ys[@]}; do
    echo -e "S $y;" >>$FRM
done

echo "ExtraSymbols,array,Z;" >>$FRM
cat ${TMP} >> ${FRM}
rm -f ${TMP}
echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

for y in ${ys[@]}; do
    h+="+u^$((i++))*$y"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${ys[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for y in ${ys[@]}; do
    echo "#write <$C> \"const TreeValue $y{%E};\n\", ${y}a" >>$FRM
done

# fs
J="L J="
i=1
fsr=$(rg -o "f\d+ =" $FRM)
declare -a fs
for f in ${fsr[@]}; do
    fs+=( "${f%*=}" )
done
for f in ${fs[@]}; do
    J+="+v^$((i++))*$f"
done

echo "$J;" >>$FRM

echo -e "B v;\n.sort\n#optimize J\nB v;\n.sort" >>$FRM

i=1
for f in ${fs[@]}; do
    echo "L ${f}a = J[v^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for f in ${fs[@]}; do
    echo "#write <$C> \"const TreeValue $f{%E};\n\", ${f}a" >>$FRM
done

