(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/trees"];
<< "InitTwoLoopToolsFF.m"
<< "setupfiles/Setup_5g.m"


mmpppS=spA[p[1],p[2]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
pmmppS=spA[p[2],p[3]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
ppmmpS=spA[p[3],p[4]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
pppmmS=spA[p[4],p[5]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
mpmppS=spA[p[1],p[3]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
pmpmpS=spA[p[2],p[4]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
ppmpmS=spA[p[3],p[5]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
mppmpS=spA[p[1],p[4]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
pmppmS=spA[p[2],p[5]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);
mpppmS=spA[p[1],p[5]]^4/(spA[p[1],p[2]]*spA[p[2],p[3]]*spA[p[3],p[4]]*spA[p[4],p[5]]*spA[p[5],p[1]]);


?PSanalytic


treeX[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalytic];


strees={mmpppS,pmmppS,ppmmpS,pppmmS,mpmppS,pmpmpS,ppmpmS,mppmpS,pmppmS,mpppmS};


xtrees=treeX/@strees;


names={a28,a25,a19,a07,a26,a21,a11,a22,a13,a14};


Export["trees.m",#[[1]]->#[[2]]&/@Transpose[{names,xtrees}]];


strees


xtrees


?PSanalyticX1
treeX1[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalyticX1];
x1trees=treeX1/@strees


?PSanalyticX17
treeX17[treeS_]:=GetMomentumTwistorExpression[treeS,PSanalyticX17];
x17trees=treeX17/@strees





allPlus=1/Times@@(spA[p[#],p[Mod[#+1,5,1]]]&/@Range[5])


singleMinus[m_]:=Module[{n},n[i_]:=Mod[m+i,5,1]; (spA[p[n[0]],p[n[1]]]*spB[p[n[1]],p[n[2]]]*spA[p[n[2]],p[n[0]]]/(spA[p[n[4]],p[n[1]]]*spA[p[n[1]],p[n[2]]]*spA[p[n[2]],p[n[3]]]*spA[p[n[3]],p[n[4]]]))];


singleMinus[1]


sinMins=singleMinus/@Range[5]


sinMins//.{spA[a_,b_]->sA[a,b],spB[a_,b_]->sB[a,b]}/.{p[i_]->i-1}
Export["zeroesS.m",%];


zeroes=Join[{allPlus},sinMins]


zeroesX=(GetMomentumTwistorExpression[#,PSanalytic]//Simplify)&/@zeroes


zeroesX/.{Power[a_,b_]/;b>1->pow[a,b]}
Export["zeroes.m",%];


zeroesX//Length


GetMomentumTwistorExpression[spB[p[1],p[2]] spB[p[2],p[3]] spB[p[3],p[4]] spB[p[4],p[5]] spB[p[5],p[1]],PSanalytic]
GetMomentumTwistorExpression[1/(spA[p[1],p[2]] spA[p[2],p[3]] spA[p[3],p[4]] spA[p[4],p[5]] spA[p[5],p[1]]),PSanalytic]


GetMomentumTwistorExpression[mmpppS,PSanalytic]
GetMomentumTwistorExpression[spA[p[1],p[2]]^4,PSanalytic]


singleMinusAlt[m_]:=Module[{},n[i_]:=Mod[m+i,5,1]; 1/(spB[p[n[0]],p[n[1]]]/Times@@(spB[p[n[1]],p[n[#]]]&/@Range[2,4]))^2];


(GetMomentumTwistorExpression[singleMinusAlt[#],PSanalytic]//Simplify)&/@Range[5]


(a+I b)^2 (c+d I)^2//Expand
((a+I b) (c+d I))^2//Expand



