
template <typename T>
TreeValue Amp0q5g_a<T>::hA028X()
{
    return njet_pow(x1, 3) * njet_pow(x2, 2) * x3;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA025X()
{
    return njet_pow(x2, 2) * x3 / x1;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA019X()
{
    return x3 / (njet_pow(x2, 2) * x1);
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA007X()
{
    return T(1.) / (njet_pow(x3, 3) * njet_pow(x2, 2) * x1);
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA026X()
{
    return njet_pow(x1, 3) * njet_pow(x2, 2) * x3;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA021X()
{
    std::array<TreeValue, 3> Z;

    Z[0] = T(1.) / x2;
    Z[1] = T(4.) + Z[0];
    Z[1] *= Z[0];
    Z[2] = T(4.) + x2;
    Z[2] *= x2;
    Z[1] += Z[2] + T(6.);

    return x3 * Z[1] / x1;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA011X()
{
    std::array<TreeValue, 3> Z;

    Z[0] = T(1.) / x2;
    Z[1] = T(1.) / x3;
    Z[2] = T(4.) + Z[1];
    Z[2] *= Z[1];
    Z[2] += T(6.);
    Z[2] *= Z[1];
    Z[2] += x3 + T(4.);

    return Z[2] * njet_pow(Z[0], 2) / x1;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA022X()
{
    return njet_pow(x1, 3) * njet_pow(x2, 2) * x3;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA013X()
{
    std::array<TreeValue, 5> Z;

    Z[0] = T(1.) / x2;
    Z[1] = T(1.) / x3;
    Z[2] = T(3.) + Z[1];
    Z[2] *= Z[1];
    Z[2] += T(3.) + x3;
    Z[3] = T(4.) + Z[1];
    Z[3] *= Z[1];
    Z[3] += T(6.);
    Z[3] *= Z[1];
    Z[3] += T(4.) + x3;
    Z[3] *= Z[0];
    Z[2] = T(4.) * Z[2] + Z[3];
    Z[2] *= Z[0];
    Z[3] = T(3.) + x2;
    Z[4] = T(4.) + x2;
    Z[4] *= x2;
    Z[4] += T(6.);
    Z[4] *= x3;
    Z[2] += T(6.) * Z[1] + T(4.) * Z[3] + Z[4];

    return Z[2] / x1;
}

template <typename T>
TreeValue Amp0q5g_a<T>::hA014X()
{
    return njet_pow(x1, 3) * njet_pow(x2, 2) * x3;
}
