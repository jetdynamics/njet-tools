#!/usr/bin/env bash

# use on FORM processed file

OUT=$1pp

perl \
    -0777p \
    -e  "
        s|([ (\-={\/] ?)(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/] ?)(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|pow|njet_pow|g;
        " \
    $1 > ${OUT}

for n in {1..10}; do
    m=$((n - 1))
    perl -pi -e "s|\[$n\]|[$m]|g" ${OUT}
done
