(* ::Package:: *)

(* rseed=16176 *)


SetDirectory["/home/ryan/git/njet-tools/5g@2l/points"];
d=Get["bad_pfs_mma_dd.txt"];
q=Get["bad_pfs_mma_qq.txt"];


vals[data_]:=Select[#/.Rule[a_,b_]:>b&/@data,#!=0&];
vq=vals[q];
vd=vals[d];
aq=Abs/@vq;
aq//Max
aq//Min


delta[a_,b_]:=(a-b)/b;
diff=delta[vd,vq];
((Abs/@diff)//Sort//Reverse)[[;;3]]


dm=1.65306122448979553318793023208855
qm=1.65306122448979559209772337546422
dm-qm


dl=0.502628856561811887537771781353513
ql=0.502628856561811957997293305220361


Log[dm]
Log[qm]


Log[dm]-dl


Log[qm]-ql


tcr11 = 1.098612288668109691395245236922525704647490557822749451734694333637494293218608966873615754813732089;


td=1.09861228866810978210821758693783;
tq=1.09861228866810969139524523692256;


tcr11-td


tcr11-tq


(* s,p,q conserve momentum; p,q are massless *)
({s0 + p0 + q0 == 0, s1 + p1 + q1 == 0, s2 + p2 + q2 == 0,
 s3 + p3 + q3 == 0, p0^2 - (p1^2 + p2^2 + p3^2) == 0,
 q0^2 - (q1^2 + q2^2 + q3^2) == 0}
 /. {p0 -> p0 + dp0, p3 -> p3 + dp3, q0 -> q0 + dq0, q3 -> q3 + dq3})
sol1 = Solve[%, {dp0, dp3, dq0, dq3}] // Simplify



