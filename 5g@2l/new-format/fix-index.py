#!/usr/bin/env python3

import argparse
import re

parser = argparse.ArgumentParser(
    description="Change unity-indexing to zero-indexing in external files."
)

parser.add_argument("files", type=str, nargs="+", help="Files to change indexing in.")

args = parser.parse_args()

for filename in args.files:
    with open(filename, "r") as f:
        data = f.readlines()

    new = ""
    p = re.compile(r"(mIEm?\dP[eo]O\d{1,2}(?:pol|fin).insert)\((\d+), (\d+)\)( = )")
    for line in data:
        matches = p.search(line)
        if matches:
            i = int(matches.group(2)) - 1
            j = int(matches.group(3)) - 1
            new += p.sub(f"\\1({i}, {j})\\4", line)
        else:
            new += line

    with open(filename + ".fixed", "w") as f:
        f.write(new)
