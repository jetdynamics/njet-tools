(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/new-format/mmppp"];


fin=Get["partial1L/P1_mm+++_fin_lr_dseq4_apart.m"];


pol={Get["partial1L/P1_mm+++_pole_lr_dseq4.m"]};


es[o_,p_,data_,e_]:=Module[{a=data[[1,p,o,6-e]]},SM[Length[a[[2]]],a[[1]],a[[2]]]]


Fs[data_]:=SF[#]->data[[1,1,#]]&/@Range[Length[data[[1,1]]]]


Export["fin/Fs.m",Fs[fin]];
Export["fin/fs.m",fin[[1,5]]];
Export["fin/ys.m",fin[[1,6]]];
Export["pol/Fs.m",Fs[pol]];
Export["pol/fs.m",pol[[1,5]]];
Export["pol/ys.m",pol[[1,6]]];


Table[Export["fin/e"<>ToString[e]<>"e"<>ToString[#]<>".m",es[#,3,fin,e]]&/@Range[12],{e,5}];
Table[Export["fin/e"<>ToString[e]<>"o"<>ToString[#]<>".m",es[#,4,fin,e]]&/@Range[12],{e,5}];
Table[Export["pol/e"<>ToString[e]<>"e"<>ToString[#]<>".m",es[#,3,pol,e]]&/@Range[12],{e,5}];
Table[Export["pol/e"<>ToString[e]<>"o"<>ToString[#]<>".m",es[#,4,pol,e]]&/@Range[12],{e,5}];
