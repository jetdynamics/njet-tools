#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=tmp

echo -e "#-\n#: WorkSpace 400M\n#: Threads 60\nS u,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|F\[(\d+), (\d+)\]|fvu\1u\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fvu\1u\2u\3|g;
        s|tc([ir])\[(\d), (\d)\]|tc\1\2\3|g;
        s|\{?SF\[(\d+)\] -> (.*?),\s*|L sf\1 = \2;\n|gs;
        s|SF\[(\d+)\] -> (.*?)\}\s*|L sf\1 = \2;\n|gs;
        " \
    ${IN} >${TMP}


constsr=`rg -o "tc[ir]\d+" $TMP`
declare -A consts
for c in $constsr; do
    consts[$c]=1
done
for c in ${!consts[@]}; do
    echo -e "S $c;" >>$FRM
done

gsr=`rg -o "fv[u\d]*" $TMP`
declare -A gs
for c in $gsr; do
    gs[$c]=1
done
for c in ${!gs[@]}; do
    echo -e "S $c;" >>$FRM
done

echo "ExtraSymbols,array,Z;" >>$FRM
cat ${TMP} >> ${FRM}
rm -f ${TMP}
echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

sfr=$(rg -o "sf\d+ =" $FRM)
declare -a sf
for f in ${sfr[@]}; do
    sf+=( "${f%*=}" )
done
N=$((${#sf[@]}/${#sf}))
echo $N >${BASE}.count

h="L H="
i=1
for f in ${sf[@]}; do
    h+="+u^$((i++))*$f"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${sf[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

i=0
for f in ${sf[@]}; do
    echo "#write <$C> \"sf[${i}] = %E;\", ${f}a" >>$FRM
    echo $((i++))
done

echo ".end" >>$FRM
