#!/usr/bin/env bash

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

MAIN_HEADER="main_header.cpp"
HEADER="header.cpp"
INIT_LIST="init-list.cpp"
CONSTRUCTOR="constructor.cpp"
MAIN="main_src.cpp"
COEFFS="coeffs.cpp"
EPS="eps.cpp"
HSRC="hel_src.cpp"

h="--+++"
d="mmppp"

h_symb=""
h_char=""
j=0
for i in {0..4}; do
    if [ "${h:$i:1}" = "+" ]; then
        j=$(($j + 2 ** $i))
        h_symb+="+"
        h_char+="p"
    else
        h_symb+="-"
        h_char+="m"
    fi
done

if (($j < 10)); then
    j="0$j"
fi

cd ${d}

echo >${COEFFS}
echo >${HEADER}
echo >${MAIN_HEADER}
echo >${INIT_LIST}
echo >${CONSTRUCTOR}
echo >${MAIN}
perl -p -e "s|CCCCC|$h_char|g;" ../template_eps_comp.cpp >${HSRC}
perl -p -e "s|CCCCC|$h_char|g;" ../template_start.cpp >${EPS}
n=0
parts=("pol" "fin")
for part in ${parts[@]}; do

    perl -p -e "s|CCCCC|$h_char|g; s|AAA|$part|g;" ../template_SF.cpp >>${HSRC}
    cat ${part}/Fs.cpp >>${HSRC}
    echo -e "}\n" >>${HSRC}

    echo "std::array<LoopResult<Eps5o2<T>>, 12> hAg${j}e2_${part}();" >>${MAIN_HEADER}

    perl -p -e "s|PPP|$part|g; s|CCCCC|$h_char|g;" ../template_coeffs_start_1lsq.cpp >>${COEFFS}
    cat ${part}/ys.cpp >>${COEFFS}
    cat ${part}/fs.cpp >>${COEFFS}
    echo -e "}\n" >>${COEFFS}

    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g; s|PPP|$part|g;" ../template_hel_header.cpp >>${HEADER}
    echo "Eigen::SparseMatrix<T> " >>$HEADER

    for i in {1..5}; do
        # 1lsq
        e=$((3 - ${i}))
        if [ "$e" == "-2" ]; then
            e="m2"
        elif [ "$e" == "-1" ]; then
            e="m1"
        fi
        ps=('e' 'o')
        for p in ${ps[@]}; do
            for o in {1..12}; do
                n=$(($n + 1))
                name=mIE${e}P${p}O${o}${part}
                file=$part/e${i}${p}${o}.

                if [ $n == 120 ]; then
                    echo "${name};" >>$HEADER
                    n=0
                else
                    echo "${name}," >>$HEADER
                fi

                perl -0777p -e "s|.*SM\[\d+, \{(\d+), (\d+)\}, \{.*|  , ${name}(\2, \1)\n|gms" ${file}m >>$INIT_LIST

                cat ${file}cpp >>$CONSTRUCTOR

            done
        done
    done

    echo >>${HEADER}

    F=$(cat ${part}/fs.count)
    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g; s|FFF|$F|g; s|PPP|$part|g;" ../template_1lsq.cpp >>${MAIN}
    echo >>${MAIN}

    for i in {1..12}; do
        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|AAA|$part|g; s|SSSS|odd|g; s|TT|o|g;" ../template_perm_1lsq.cpp >>${EPS}

        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|AAA|$part|g; s|SSSS|even|g; s|TT|e|g;" ../template_perm_1lsq.cpp >>${EPS}
    done


done

perl -p -e "s|CCCCC|$h_char|g;" ../template_end.cpp >>${EPS}

clang-format -i $HEADER
clang-format -i $COEFFS
clang-format -i $EPS
clang-format -i $HSRC
clang-format -i $CONSTRUCTOR
clang-format -i $MAIN_HEADER
clang-format -i $MAIN

cd ..
