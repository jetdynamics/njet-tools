// SSSSS
template <typename T>
std::array<LoopResult<Eps5o2<T>>, 12> Amp0q5g_a2l<T>::hAgHHe2_PPP() {
  hamp2vHH.hAg_coeffs_PPP(x1, x2, x3, x4, x5);
  const std::array<int, 5> o{0, 1, 2, 3, 4};
  const TreeValue phase{-i_ * hA0HH(o.data()) / hA0HHX()};

  std::array<Eps5o2<T>, 12> amps{
      hamp2vHH.hAg_p1e_PPP() + hamp2vHH.hAg_p1o_PPP(),
      hamp2vHH.hAg_p2e_PPP() + hamp2vHH.hAg_p2o_PPP(),
      hamp2vHH.hAg_p3e_PPP() + hamp2vHH.hAg_p3o_PPP(),
      hamp2vHH.hAg_p4e_PPP() + hamp2vHH.hAg_p4o_PPP(),
      hamp2vHH.hAg_p5e_PPP() + hamp2vHH.hAg_p5o_PPP(),
      hamp2vHH.hAg_p6e_PPP() + hamp2vHH.hAg_p6o_PPP(),
      hamp2vHH.hAg_p7e_PPP() + hamp2vHH.hAg_p7o_PPP(),
      hamp2vHH.hAg_p8e_PPP() + hamp2vHH.hAg_p8o_PPP(),
      hamp2vHH.hAg_p9e_PPP() + hamp2vHH.hAg_p9o_PPP(),
      hamp2vHH.hAg_p10e_PPP() + hamp2vHH.hAg_p10o_PPP(),
      hamp2vHH.hAg_p11e_PPP() + hamp2vHH.hAg_p11o_PPP(),
      hamp2vHH.hAg_p12e_PPP() + hamp2vHH.hAg_p12o_PPP(),
  };

  for (Eps5o2<T> &amp : amps) {
    amp = phase * correction1Lsq(amp);
  }

  for (int i{0}; i < FFF; ++i) {
    hamp2vHH.f[i] = std::conj(hamp2vHH.f[i]);
  }

  std::array<Eps5o2<T>, 12> camps{
      hamp2vHH.hAg_p1e_PPP() - hamp2vHH.hAg_p1o_PPP(),
      hamp2vHH.hAg_p2e_PPP() - hamp2vHH.hAg_p2o_PPP(),
      hamp2vHH.hAg_p3e_PPP() - hamp2vHH.hAg_p3o_PPP(),
      hamp2vHH.hAg_p4e_PPP() - hamp2vHH.hAg_p4o_PPP(),
      hamp2vHH.hAg_p5e_PPP() - hamp2vHH.hAg_p5o_PPP(),
      hamp2vHH.hAg_p6e_PPP() - hamp2vHH.hAg_p6o_PPP(),
      hamp2vHH.hAg_p7e_PPP() - hamp2vHH.hAg_p7o_PPP(),
      hamp2vHH.hAg_p8e_PPP() - hamp2vHH.hAg_p8o_PPP(),
      hamp2vHH.hAg_p9e_PPP() - hamp2vHH.hAg_p9o_PPP(),
      hamp2vHH.hAg_p10e_PPP() - hamp2vHH.hAg_p10o_PPP(),
      hamp2vHH.hAg_p11e_PPP() - hamp2vHH.hAg_p11o_PPP(),
      hamp2vHH.hAg_p12e_PPP() - hamp2vHH.hAg_p12o_PPP(),
  };

  const TreeValue cphase{std::conj(phase)};

  for (Eps5o2<T> &camp : camps) {
    camp = cphase * correction1Lsq(camp);
  }

  std::array<LoopResult<Eps5o2<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}
