#!/usr/bin/env bash

FOL="new"
ROOT="expr-${FOL}/"
GLO="out-${FOL}"

NOM1="one"
SRC1="${ROOT}/${NOM1}/"

NOM2="two"
SRC2="${ROOT}/${NOM2}/"

DES="${GLO}/"

mkdir -p ${DES}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

HDR_MAIN="${GLO}/hdr_main.cpp"
SRC_MAIN="${GLO}/src_main.cpp"

echo >${HDR_MAIN}
echo >${SRC_MAIN}

uhvs=(
    "+++++"
    "-++++"
    "+-+++"
    "++-++"
    "+++-+"
    "++++-"
)

# 1L: do main hdr
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h 1L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    echo "std::array<LoopResult<std::complex<T>>, 12> hAg${j}p();" >>${HDR_MAIN}
done

echo "" >>${HDR_MAIN}

# 2L: do main hdr
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    echo "std::array<LoopResult<std::complex<T>>, 12> hA2g${j}p();" >>${HDR_MAIN}
done

echo -e "\n// }{ 1L primaries\n" >>${SRC_MAIN}

echo -e "\n// { UHVs\n" >>${SRC_MAIN}

# UHVs: main src
for h in ${uhvs[@]}; do
    echo $h 1L
    s="${SRC1}/h${h}"
    d="${DES}/h${h}"

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    F=$(cat ${s}/fm.count)

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL||g; s|RR||g;" template_one_loop.cpp >>${SRC_MAIN}
done

echo -e "\n// }{ MHVs without scale dependence\n" >>${SRC_MAIN}

# 1L: do main src
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h 1L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j != 15 && $j != 23 && $j != 27 && $j != 29 && $j != 30 && $j != 31)); then

        if (($j < 10)); then
            j="0$j"
        fi

        d="${DES}/h${h_symb}"

        F=$(cat ${s}/fm.count)

        perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL||g; s|RR|r|g;" template_one_loop.cpp >>${SRC_MAIN}
    fi
done

echo -e "// }\n\n// }{ 2L primaries [MHVs]\n" >>${SRC_MAIN}

# 2L: do main src
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g; s|LL|2|g;" template_loops.cpp >>${SRC_MAIN}
done

clang-format -i $HDR_MAIN
clang-format -i $SRC_MAIN
