#!/usr/bin/env bash

NOM="zero"
SRC="expr-one/${NOM}/"
cd ${SRC}

for f in h*; do
    h=${f:1:5}
    echo $h

    cd "$f"

    ../../../conv-y.sh ym.m
    ../../../conv-f.sh fm.m

    ps=('e')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            ../../../conv-sm-tree.sh ${p}${o}.m
            clang-format -i ${p}${o}.cpp
        done
    done

    tform ym.frm
    tform fm.frm

    ../../../pp-form.sh ym.c
    ../../../pp-form.sh fm.c

    clang-format -i ym.cpp
    clang-format -i fm.cpp

    cd ..
done

cd ../..
