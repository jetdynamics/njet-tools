(* Created with the Wolfram Language : www.wolfram.com *)
{{{f[3], f[5], f[6]}, {f[1], f[3], f[4]}, {f[1]}, {f[2], f[5], f[6]}, {f[2]}, 
  {f[3]}, {f[4]}, {f[5]}, {f[1], f[3], f[5]}, {f[6]}, {f[2], f[4], f[6]}, 
  {f[1], f[2], f[4]}}, {{1}, {1}, {1}, {1}, {1}, {1}, {1}, {1}, {1}, {1}, 
  {1}, {1}}, {{MySparseMatrix[{3, 1}, {m[1, 1] -> 1, m[2, 1] -> 1, 
     m[3, 1] -> 1}]}, {MySparseMatrix[{3, 1}, {m[1, 1] -> 1, m[2, 1] -> -1, 
     m[3, 1] -> -1}]}, {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}]}, 
  {MySparseMatrix[{3, 1}, {m[1, 1] -> 1, m[2, 1] -> -1, m[3, 1] -> -1}]}, 
  {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}]}, 
  {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}]}, 
  {MySparseMatrix[{1, 1}, {m[1, 1] -> 1}]}, 
  {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}]}, 
  {MySparseMatrix[{3, 1}, {m[1, 1] -> -1, m[2, 1] -> 1, m[3, 1] -> 1}]}, 
  {MySparseMatrix[{1, 1}, {m[1, 1] -> -1}]}, 
  {MySparseMatrix[{3, 1}, {m[1, 1] -> -1, m[2, 1] -> 1, m[3, 1] -> 1}]}, 
  {MySparseMatrix[{3, 1}, {m[1, 1] -> 1, m[2, 1] -> 1, m[3, 1] -> -1}]}}, 
 {1}, {f[1] -> y[5]^3/(y[1]*y[2]*y[3]^2*y[4]), 
  f[2] -> y[5]^3/(y[1]*y[2]*y[3]^3*y[6]), f[3] -> y[5]^3/(y[1]*y[2]*y[3]^2), 
  f[4] -> y[5]^3/(y[1]*y[2]*y[3]^2*y[4]*y[6]), 
  f[5] -> y[5]^3/(y[1]*y[2]^2*y[3]^2), f[6] -> y[5]^3/(y[1]*y[2]^2*y[3]^3)}, 
 {y[1] -> ex[1], y[2] -> ex[2], y[3] -> ex[3], y[4] -> 1 + ex[3], 
  y[5] -> 1 + ex[3] + ex[2]*ex[3], y[6] -> 1 + ex[2]}}
