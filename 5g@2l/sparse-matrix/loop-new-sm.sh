#!/usr/bin/env bash

declare -a sms

echo -e "# Automatically generated Makefile from $0 #\n.PHONY: all sms\nall: sms" >Makefile

for dir in expr-new/one/h*; do
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            sms+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t./conv-sm-new.sh $< 1" \
                >>Makefile
        done
    done
done

for dir in expr-new/two/h*; do
    ps=('e' 'o')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            sms+=("$dir/${p}${o}.cpp")
            echo -e "
$dir/${p}${o}.cpp: $dir/${p}${o}.m
\t./conv-sm-new.sh $< 2" \
                >>Makefile
        done
    done
done

echo -e "\nsms: ${sms[@]}" >>Makefile
