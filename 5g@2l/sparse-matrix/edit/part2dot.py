#!/usr/bin/env python3

# import argparse
import fileinput
import re
# import sys

# parser = argparse.ArgumentParser()
# parser.add_argument("-i", "--inplace", action="store_true")
# args = parser.parse_args()

pattern = re.compile(r"part\(f_(2?)g, fi_2?g(\d+), (sfi_2?g\d+), m_2?g\d+[eo]?")

# for line in fileinput.input(sys.argv[2 if args.inplace else 1:], inplace=args.inplace):
for line in fileinput.input(inplace=True):
    matches = pattern.search(line)
    if matches:
        i = int(matches.group(2)) - 1
        line = pattern.sub(f"dot(c_\\1g[{i}], \\3", line)
    print(line.rstrip())

