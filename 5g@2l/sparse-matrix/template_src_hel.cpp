template <typename T>
Amp0q5g_a2v_CCCCC<T>::Amp0q5g_a2v_CCCCC(const std::vector<std::complex<T>> &sf_)
    : Z(ZZZ), f(FFF), y(YYY), sf(sf_)
