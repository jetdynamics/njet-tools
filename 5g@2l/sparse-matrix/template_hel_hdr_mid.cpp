template <typename T> class Amp0q5g_a2v_CCCCC {
public:
  Amp0q5g_a2v_CCCCC(const std::vector<std::complex<T>> &sf_);

  std::vector<std::complex<T>> Z;
  std::vector<std::complex<T>> f;
  std::vector<std::complex<T>> y;

  const std::vector<std::complex<T>> &sf;

  std::complex<T> finite(const std::vector<int> fi, const std::vector<int> sfi,
                         typename Eigen::SparseMatrix<T> mat);

  // { 1lsq

  Eigen::SparseMatrix<T> m_l1pEo1, m_l1pEo2, m_l1pEo3, m_l1pEo4, m_l1pEo5,
      m_l1pEo6, m_l1pEo7, m_l1pEo8, m_l1pEo9, m_l1pEo10, m_l1pEo11, m_l1pEo12,
      m_l1pOo1, m_l1pOo2, m_l1pOo3, m_l1pOo4, m_l1pOo5, m_l1pOo6, m_l1pOo7,
      m_l1pOo8, m_l1pOo9, m_l1pOo10, m_l1pOo11, m_l1pOo12;

  std::vector<int> sfi_l1o1, sfi_l1o2, sfi_l1o3, sfi_l1o4, sfi_l1o5, sfi_l1o6,
      sfi_l1o7, sfi_l1o8, sfi_l1o9, sfi_l1o10, sfi_l1o11, sfi_l1o12;

  std::vector<int> fi_l1o1, fi_l1o2, fi_l1o3, fi_l1o4, fi_l1o5, fi_l1o6,
      fi_l1o7, fi_l1o8, fi_l1o9, fi_l1o10, fi_l1o11, fi_l1o12;

  void hAg_coeffs(std::complex<T> x1, std::complex<T> x2, std::complex<T> x3,
                  std::complex<T> x4, std::complex<T> x5);

  std::complex<T> hAg_p1e();
  std::complex<T> hAg_p2e();
  std::complex<T> hAg_p3e();
  std::complex<T> hAg_p4e();
  std::complex<T> hAg_p5e();
  std::complex<T> hAg_p6e();
  std::complex<T> hAg_p7e();
  std::complex<T> hAg_p8e();
  std::complex<T> hAg_p9e();
  std::complex<T> hAg_p10e();
  std::complex<T> hAg_p11e();
  std::complex<T> hAg_p12e();

  std::complex<T> hAg_p1o();
  std::complex<T> hAg_p2o();
  std::complex<T> hAg_p3o();
  std::complex<T> hAg_p4o();
  std::complex<T> hAg_p5o();
  std::complex<T> hAg_p6o();
  std::complex<T> hAg_p7o();
  std::complex<T> hAg_p8o();
  std::complex<T> hAg_p9o();
  std::complex<T> hAg_p10o();
  std::complex<T> hAg_p11o();
  std::complex<T> hAg_p12o();

  // }{ 2l

  Eigen::SparseMatrix<T> m_l2pEo1, m_l2pEo2, m_l2pEo3, m_l2pEo4, m_l2pEo5,
      m_l2pEo6, m_l2pEo7, m_l2pEo8, m_l2pEo9, m_l2pEo10, m_l2pEo11, m_l2pEo12,
      m_l2pOo1, m_l2pOo2, m_l2pOo3, m_l2pOo4, m_l2pOo5, m_l2pOo6, m_l2pOo7,
      m_l2pOo8, m_l2pOo9, m_l2pOo10, m_l2pOo11, m_l2pOo12;

  std::vector<int> sfi_l2o1, sfi_l2o2, sfi_l2o3, sfi_l2o4, sfi_l2o5, sfi_l2o6,
      sfi_l2o7, sfi_l2o8, sfi_l2o9, sfi_l2o10, sfi_l2o11, sfi_l2o12;

  std::vector<int> fi_l2o1, fi_l2o2, fi_l2o3, fi_l2o4, fi_l2o5, fi_l2o6,
      fi_l2o7, fi_l2o8, fi_l2o9, fi_l2o10, fi_l2o11, fi_l2o12;

  void hA2g_coeffs(std::complex<T> x1, std::complex<T> x2, std::complex<T> x3,
                   std::complex<T> x4, std::complex<T> x5);

  std::complex<T> hA2g_p1e();
  std::complex<T> hA2g_p2e();
  std::complex<T> hA2g_p3e();
  std::complex<T> hA2g_p4e();
  std::complex<T> hA2g_p5e();
  std::complex<T> hA2g_p6e();
  std::complex<T> hA2g_p7e();
  std::complex<T> hA2g_p8e();
  std::complex<T> hA2g_p9e();
  std::complex<T> hA2g_p10e();
  std::complex<T> hA2g_p11e();
  std::complex<T> hA2g_p12e();

  std::complex<T> hA2g_p1o();
  std::complex<T> hA2g_p2o();
  std::complex<T> hA2g_p3o();
  std::complex<T> hA2g_p4o();
  std::complex<T> hA2g_p5o();
  std::complex<T> hA2g_p6o();
  std::complex<T> hA2g_p7o();
  std::complex<T> hA2g_p8o();
  std::complex<T> hA2g_p9o();
  std::complex<T> hA2g_p10o();
  std::complex<T> hA2g_p11o();
  std::complex<T> hA2g_p12o();

  // }
};

