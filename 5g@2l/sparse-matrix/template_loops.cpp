// SSSSS
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T, P>::hALLgHHpr() {
  hamp2vHH.hALLg_coeffs(x1, x2, x3, x4, x5);
  const std::array<int, 5> o{0, 1, 2, 3, 4};
  const std::complex<T> phase{-i_ * hA0HH(o.data()) / hA0HHX()};

  std::array<std::complex<T>, 12> amps{
      hamp2vHH.hALLg_p1e() - hamp2vHH.hALLg_p1o(),
      hamp2vHH.hALLg_p2e() - hamp2vHH.hALLg_p2o(),
      hamp2vHH.hALLg_p3e() - hamp2vHH.hALLg_p3o(),
      hamp2vHH.hALLg_p4e() + hamp2vHH.hALLg_p4o(),
      hamp2vHH.hALLg_p5e() + hamp2vHH.hALLg_p5o(),
      hamp2vHH.hALLg_p6e() - hamp2vHH.hALLg_p6o(),
      hamp2vHH.hALLg_p7e() + hamp2vHH.hALLg_p7o(),
      hamp2vHH.hALLg_p8e() + hamp2vHH.hALLg_p8o(),
      hamp2vHH.hALLg_p9e() - hamp2vHH.hALLg_p9o(),
      hamp2vHH.hALLg_p10e() + hamp2vHH.hALLg_p10o(),
      hamp2vHH.hALLg_p11e() + hamp2vHH.hALLg_p11o(),
      hamp2vHH.hALLg_p12e() - hamp2vHH.hALLg_p12o(),
  };

  for (std::complex<T> &amp : amps) {
    amp *= phase;
  }

  for (int i{0}; i < FFF; ++i) {
    hamp2vHH.f[i] = std::conj(hamp2vHH.f[i]);
  }

  std::array<std::complex<T>, 12> camps{
      hamp2vHH.hALLg_p1e() + hamp2vHH.hALLg_p1o(),
      hamp2vHH.hALLg_p2e() + hamp2vHH.hALLg_p2o(),
      hamp2vHH.hALLg_p3e() + hamp2vHH.hALLg_p3o(),
      hamp2vHH.hALLg_p4e() - hamp2vHH.hALLg_p4o(),
      hamp2vHH.hALLg_p5e() - hamp2vHH.hALLg_p5o(),
      hamp2vHH.hALLg_p6e() + hamp2vHH.hALLg_p6o(),
      hamp2vHH.hALLg_p7e() - hamp2vHH.hALLg_p7o(),
      hamp2vHH.hALLg_p8e() - hamp2vHH.hALLg_p8o(),
      hamp2vHH.hALLg_p9e() + hamp2vHH.hALLg_p9o(),
      hamp2vHH.hALLg_p10e() - hamp2vHH.hALLg_p10o(),
      hamp2vHH.hALLg_p11e() - hamp2vHH.hALLg_p11o(),
      hamp2vHH.hALLg_p12e() + hamp2vHH.hALLg_p12o(),
  };

  const std::complex<T> cphase{std::conj(phase)};

  for (std::complex<T> &camp : camps) {
    camp *= cphase;
  }

  std::array<LoopResult<std::complex<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}

