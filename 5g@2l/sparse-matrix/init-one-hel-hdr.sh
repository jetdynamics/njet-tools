#!/usr/bin/env bash

NOM="one"
ROOT="expr-$1/"
SRC="${ROOT}/${NOM}/"
DES="out-$1/${NOM}/"

mkdir -p ${DES}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

HDR_MAIN="${DES}/hdr_main.cpp"
SRC_MAIN="${DES}/src_main.cpp"

rm -f ${HDR_MAIN}
rm -f ${SRC_MAIN}
touch ${HDR_MAIN}
touch ${SRC_MAIN}

HDR_HEL="${DES}/hdr_hel.cpp"
SRC_HEL="src_hel.cpp"

cat template_hel_hdr_srt.cpp >${HDR_HEL}

for s in ${SRC}/h*; do
    h=${s#*//*//h}
    echo $h

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    mkdir -p ${d}

    SRC_HEL_I="${d}/src_hel.cpp"

    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC_HEL_I}
    n=0

    echo "std::array<LoopResult<std::complex<T>>, 12> hAg${j}p();" >>${HDR_MAIN}

    perl -p -e "s|CCCCC|$h_char|g;" template_hel_header.cpp >>${HDR_HEL}

    F=$(cat ${s}/fm.count)
    # SF=$(cat ${ROOT}/Fm.count)
    Y=$(cat ${s}/ym.count)
    Z=$(cat ${s}/*.tmp | sort -n | tail -1)
    Z=$((Z + 1))
    perl -p -e "s|CCCCC|$h_char|g; s|ZZZ|$Z|g; s|FFF|$F|g; s|YYY|$Y|g; " template_src_hel.cpp >>${SRC_HEL_I}

    Ps=('E' 'O')
    ps=('e' 'o')
    for i in {0..1}; do
        p=${ps[i]}
        P=${Ps[i]}
        for o in {1..12}; do
            n=$(($n + 1))
            name=m_l1p${P}o${o}
            file=${s}/${p}${o}
            perl -0777p -e "s|.*SM\[\d+, \{(\d+), (\d+)\}, \{.*|  , ${name}(\2, \1)\n|gms" ${file}.m >>${SRC_HEL_I}
        done
    done

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , sfi_l1o\1(\2)\n|gs;
        " \
        ${s}/Fv.m >>${SRC_HEL_I}

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , fi_l1o\1(\2)\n|gs;
        " \
        ${s}/fv.m >>${SRC_HEL_I}

    echo -e "{\n" >>${SRC_HEL_I}
    for p in ${ps[@]}; do
        for o in {1..12}; do
            file=${s}/${p}${o}
            cat ${file}.cpp >>$SRC_HEL_I
        done
    done
    echo -e "}\n" >>${SRC_HEL_I}

    perl -p -e "s|CCCCC|$h_char|g;" template_finite.cpp >>${SRC_HEL_I}

    # perl -p -e "s|CCCCC|$h_char|g; s|LL|1l|g;" template_SF.cpp >>${SRC_HEL_I}
    # cat ${s}/Fm.cpp >>${SRC_HEL_I}
    # echo -e "}\n" >>${SRC_HEL_I}

    perl -p -e "s|CCCCC|$h_char|g;; s|GGG|g|g;" template_coeffs_start.cpp >>${SRC_HEL_I}
    cat ${s}/ym.cpp >>${SRC_HEL_I}
    cat ${s}/fm.cpp >>${SRC_HEL_I}
    echo -e "}\n" >>${SRC_HEL_I}

    perl -p -e "s|HH|$j|g; s|SSSSS|$h_symb|g; s|FFF|$F|g;" template_1lsq.cpp >>${SRC_MAIN}
    echo >>${SRC_MAIN}

    for i in {1..12}; do
        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|SSSS|odd|g; s|TT|O|g; s|AA|o|g;" template_perm_1lsq.cpp >>${SRC_HEL_I}

        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|SSSS|even|g; s|TT|E|g; s|AA|e|g;" template_perm_1lsq.cpp >>${SRC_HEL_I}
    done

    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${SRC_HEL_I}

    clang-format -i $SRC_HEL_I

done

cat template_hel_hdr_end.cpp >>${HDR_HEL}

clang-format -i $HDR_HEL
clang-format -i $HDR_MAIN
clang-format -i $SRC_MAIN
