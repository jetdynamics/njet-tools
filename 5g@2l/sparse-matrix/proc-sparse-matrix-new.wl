(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/sparse-matrix"];
in="expr-one/exprs/";
outpre="expr-new/";
uhvs={
"+++++",
"++++-",
"+++-+",
"+++--",
"++-++",
"++-+-",
"++--+",
"+-+++",
"+-++-",
"+-+-+",
"+--++",
"-++++",
"-+++-",
"-++-+",
"-+-++",
"--+++"};
mhvs={
"+++--",
"++-+-",
"++--+",
"+-++-",
"+-+-+",
"+--++",
"-+++-",
"-++-+",
"-+-++",
"--+++"
};


sm[fin_,ord_,par_]:=Module[
	{a=fin[[3,ord,par]],entries1,entries0,ncols,nrows,nnz},
	entries1=a[[2]]/.{m[i_,j_]:>m[j,i]};
	entries0=entries1/.{m[i_,j_]:>m[i-1,j-1]};
	ncols=a[[1,1]];
	nrows=a[[1,2]];
	nnz=Table[Length[Select[entries1,#[[1,2]]==i&]],{i,ncols}];
	SM[{nrows, ncols},nnz,entries0]
];
fv[fin_]:=Module[
	{a=First[#]-1&/@#&/@fin[[1]]},
	fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=Module[
	{a=#-1&/@fin[[2]]},
	Fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];


finsOne=Get[in<>"partials_5g_1L_tHV_"<>#<>".m"]&/@uhvs;


finsTwo=Get[in<>"partials_5g_2L_tHV_"<>#<>".m"]&/@mhvs;


export[fins_,hels_,out_]:=
ParallelDo[
fin=fins[[i]];
hel="h"<>hels[[i]];
o=outpre<>out<>"/";
Export[o<>hel<>"/e"<>ToString[#]<>".m",sm[fin,#,1]]&/@Range[12];
Export[o<>hel<>"/o"<>ToString[#]<>".m",sm[fin,#,2]]&/@Range[12];
,{i,Length[hels]}];


export[finsOne, uhvs, "one"];
export[finsTwo, mhvs, "two"];
