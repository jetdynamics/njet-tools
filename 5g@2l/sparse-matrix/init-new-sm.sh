#!/usr/bin/env bash

FOL="new"
ROOT="expr-${FOL}/"
GLO="out-${FOL}"

NOM1="one"
SRC1="${ROOT}/${NOM1}/"

DES="${GLO}/"

NOM2="two"
SRC2="${ROOT}/${NOM2}/"

mkdir -p ${DES}

order=(
    01234
    03421
    04231
    01342
    01423
    02341
    02134
    03412
    02314
    03142
    04213
    04123
)

# MHVs: start hel src, mkdir
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    d="${DES}/h${h_symb}"

    mkdir -p ${d}

    SRC_HEL_I="${d}/src_hel.cpp"

    F=$(cat ${s}/fm.count)
    Y=$(cat ${s}/ym.count)
    Z=$(cat ${s}/*.tmp | sort -n | tail -1)
    Z=$((Z + 1))

    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC_HEL_I}
    perl -p -e "s|CCCCC|$h_char|g; s|ZZZ|$Z|g; s|FFF|$F|g; s|YYY|$Y|g; " template_src_hel.cpp >>${SRC_HEL_I}
done

uhvs=(
    "+++++"
    "-++++"
    "+-+++"
    "++-++"
    "+++-+"
    "++++-"
)

# UHVs: start hel src, mkdir
for h in ${uhvs[@]}; do
    echo $h 1L
    s="${SRC1}/h${h}"
    d="${DES}/h${h}"
    mkdir -p ${d}
    SRC_HEL_I="${d}/src_hel.cpp"

    h_symb=""
    h_char=""
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    F=$(cat ${s}/fm.count)
    Y=$(cat ${s}/ym.count)
    Z=$(cat ${s}/*.tmp | sort -n | tail -1)
    Z=$((Z + 1))

    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC_HEL_I}
    perl -p -e "s|CCCCC|$h_char|g; s|ZZZ|$Z|g; s|FFF|$F|g; s|YYY|$Y|g; " template_src_hel.cpp >>${SRC_HEL_I}
done

# 1L: hel src: init list
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h 1L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    n=0

    ps=('e')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            n=$(($n + 1))
            name=m_l1p${p^}o${o}
            file=${s}/${p}${o}
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  , ${name}(\1, \2)\n|gms" ${file}.m >>${SRC_HEL_I}
        done
    done

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , sfi_l1o\1(\2)\n|gs;
        " \
        ${s}/Fv.m >>${SRC_HEL_I}

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , fi_l1o\1(\2)\n|gs;
        " \
        ${s}/fv.m >>${SRC_HEL_I}
done

# 2L: hel src: init list
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    n=0

    ps=('e' 'o')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            n=$(($n + 1))
            name=m_l2p${p^}o${o}
            file=${s}/${p}${o}
            perl -0777p -e "s|.*SM\[\{(\d+), (\d+)\}, \{.*|  , ${name}(\1, \2)\n|gms" ${file}.m >>${SRC_HEL_I}
        done
    done

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?Fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , sfi_l2o\1(\2)\n|gs;
        " \
        ${s}/Fv.m >>${SRC_HEL_I}

    perl \
        -0777p \
        -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)\s*||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)\s*||gm;
        s|\{?fi\[(\d{1,2})\] -> V\[\d+, (\{[\d,\s]+?\})\][,}]\s*|  , fi_l2o\1(\2)\n|gs;
        " \
        ${s}/fv.m >>${SRC_HEL_I}
done

# ALL: open constructor
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h ALL

    d="${DES}/h${h}"

    SRC_HEL_I="${d}/src_hel.cpp"

    echo -e "{\n" >>${SRC_HEL_I}
done

# 1L: hel src: constructor (sparse matrices)
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h 1L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    ps=('e')
    for p in ${ps[@]}; do
        for o in {1..12}; do
            file=${s}/${p}${o}
            cat ${file}.cpp >>$SRC_HEL_I
        done
    done
done

# 2L: hel src: constructor (sparse matrices)
# ALL: hel src: fn finite
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    p='e'
    for o in {1..12}; do
        echo "fillSM_2lo${o}();" >>${SRC_HEL_I}
        SRC_HEL_SM="${d}/src_hel_${o}.cpp"
        perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC_HEL_SM}
        echo -e "template<typename T>\nvoid Amp0q5g_a2v_${h_char}<T>::fillSM_2lo${o}() {" >>${SRC_HEL_SM}
        file=${s}/${p}${o}
        cat ${file}.cpp >>${SRC_HEL_SM}
    done
    p='o'
    for o in {1..12}; do
        SRC_HEL_SM="${d}/src_hel_${o}.cpp"
        file=${s}/${p}${o}
        cat ${file}.cpp >>${SRC_HEL_SM}
        echo -e "}" >>${SRC_HEL_SM}
        perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${SRC_HEL_SM}
    done
done

# ALL: close constructor, finite fn
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h ALL

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    echo -e "}\n" >>${SRC_HEL_I}

    perl -p -e "s|CCCCC|$h_char|g;" template_finite.cpp >>${SRC_HEL_I}
done

# 1L: do main src, hel src: coeffs, perms
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h 1L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    F=$(cat ${s}/fm.count)

    perl -p -e "s|CCCCC|$h_char|g; s|GGG|g|g;" template_coeffs_start.cpp >>${SRC_HEL_I}
    cat ${s}/ym.cpp >>${SRC_HEL_I}
    cat ${s}/fm.cpp >>${SRC_HEL_I}
    echo -e "}\n" >>${SRC_HEL_I}


    for i in {1..12}; do
        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|SSSS|even|g; s|TT|E|g; s|AA|e|g; s|GGG|g|g; s|LLL|1|g;" template_perm.cpp >>${SRC_HEL_I}
    done
done


# 2L: do main src, hel src: coeffs, perms
for s in ${SRC2}/h*; do
    h="${s#*//*//h}"
    echo $h 2L

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (($j < 10)); then
        j="0$j"
    fi

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    F=$(cat ${s}/fm.count)

    perl -p -e "s|CCCCC|$h_char|g; s|GGG|2g|g;" template_coeffs_start.cpp >>${SRC_HEL_I}
    cat ${s}/ym.cpp >>${SRC_HEL_I}
    cat ${s}/fm.cpp >>${SRC_HEL_I}
    echo -e "}\n" >>${SRC_HEL_I}


    for i in {1..12}; do
        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|SSSS|odd|g; s|TT|O|g; s|AA|o|g; s|GGG|2g|g; s|LLL|2|g;" template_perm.cpp >>${SRC_HEL_I}

        perl -p -e "s|PP|$i|g; s|OOOOO|${order[$(($i - 1))]}|g; s|CCCCC|$h_char|g; s|SSSS|even|g; s|TT|E|g; s|AA|e|g; s|GGG|2g|g; s|LLL|2|g;" template_perm.cpp >>${SRC_HEL_I}
    done
done

# ALL: templates at end of src hel
for s in ${SRC1}/h*; do
    h="${s#*//*//h}"
    echo $h ALL

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    d="${DES}/h${h_symb}"

    SRC_HEL_I="${d}/src_hel.cpp"

    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${SRC_HEL_I}

    # clang-format -i $SRC_HEL_I => use ./frmt.sh
done

