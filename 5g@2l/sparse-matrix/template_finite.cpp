
template <typename T>
std::complex<T>
Amp0q5g_a2v_CCCCC<T>::finite(const std::vector<int> fi,
                             const std::vector<int> sfi,
                             typename Eigen::SparseMatrix<T> mat) {
  std::complex<T> e;
  for (int k{0}; k < mat.outerSize(); ++k) {
    for (typename Eigen::SparseMatrix<T>::InnerIterator it(mat, k); it; ++it) {
      e += sf[sfi[it.row()]] * it.value() * f[fi[it.col()]];
    }
  }
  return e;
}
