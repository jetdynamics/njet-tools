#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
C=${BASE}.cpp

P=${BASE:0:1}
if [ "${BASE:2:1}" == "." ]; then
    O=${BASE:1:1}
else
    O=${BASE:1:2}
fi

perl \
    -0777p \
    -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)$||gm;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|SM\[\d+, \{\d+, \d+\}, \{\}\]\s*||gs;
        s|SM\[\d+, \{\d+, \d+\}, \{(.+?)\}\]\s*|\1,\nm_l1p${P^}o${O}.makeCompressed();\n\n|gs;
        s|m\[(\d+), (\d+)\] -> ([\-\d/]+),\s*|m_l1p${P^}o${O}.insert(\2, \1) = \3;\n|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        s|([ (\-={\/]\s{0,2})(\d+)\.?( ?[*\/;+\-\n])|\1T(\2.)\3|g;
        " \
    ${IN} >${C}
