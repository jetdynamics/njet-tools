# sparse-matrix
Fork of [new-format](../new-format)
## NJet compile (IPPP workstation)
Using `eigen` for sparse matrix.
```shell
CPPFLAGS=`pkg-config PentagonFunctions eigen3 --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-an0q5g2l
```
Option: ` --with-qd=no`
## Usage
```shell
math -script proc-sparse-matrix-trees.wl
math -script proc-sparse-matrix.wl
./loop-tree.sh
./loop.sh
./loop-two.sh
make -j`nproc` all
./init.sh
./frmt.sh # careful of memory usage here!
./fix-uhvs.sh one
./fin.sh
```
If make fails, try `-B`. Takes ~1/2 hour on server.
## New SMs
```shell
math -script proc-sparse-matrix-new.wl
./loop-new-sm.sh
make -j`nproc` sms
./init-new-sm.sh
./init-main.sh
./fix-uhvs.sh new
./new-template-system/hel_headers.py
./fin-new-sm.sh
./frmt-new-sm.sh

rsync -Pavh ~/git/njet-tools/5g@2l/sparse-matrix/export-new/ ~/git/njet-develop/analytic
```
