
// permutation OOOOO, parity SSSS
template <typename T> std::complex<T> Amp0q5g_a2v_CCCCC<T>::hAGGG_pPPAA() {
  return finite(fi_lLLLoPP, sfi_lLLLoPP, m_lLLLpTToPP);
}
