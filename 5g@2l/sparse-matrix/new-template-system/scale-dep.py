#!/usr/bin/env python3


def dub_dig(num):
    return f"0{num}" if num < 10 else num


def conj_hint(hint):
    return 31 - hint


def tree(hint):
    cint = conj_hint(hint)
    hstr = dub_dig(hint)

    return f"""
template <typename T>
    std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T>::hA0{hstr}p()
    {{
        std::array<LoopResult<std::complex<T>>, 12> resvec;
        for (int i {{ 0 }}; i < 12; ++i) {{
            resvec[i] = {{ hA0{hint}(ords[i].data()), -hA0{cint}(ords[i].data()) }};
        }}
        return resvec;
    }}"""


# def loop(hint):
#     hstr = dub_dig(hint)

#     return f"""
# template <typename T>
#     std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T>::hAg{hstr}p(){{
#       std::array<LoopResult<std::complex<T>>, 12> trees{{hA0{hstr}p()}};
#       std::array<LoopResult<std::complex<T>>, 12> loops{{hAg{hstr}pr()}};
#       std::array<LoopResult<std::complex<T>>, 12> resvec;
#       for (int i{{0}}; i < 12; ++i) {{
#         resvec[i] = restore_scale_dep_virt(trees[i], loops[i]);
#       }}
#       return resvec;
#     }}"""

def loop_cache(hint):
    hstr = dub_dig(hint)

    return f"""
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T, P>::hAg{hstr}p()
{{
    std::array<LoopResult<std::complex<T>>, 12> trees {{ hA0{hstr}p() }};

    if (!cached1[{hint}]) {{
        cached1[{hint}] = true;
        cache_loops[{hint}] = hAg{hstr}pr();
    }}

    std::array<LoopResult<std::complex<T>>, 12> resvec;
    for (int i {{ 0 }}; i < 12; ++i) {{
        resvec[i] = restore_scale_dep_virt(trees[i], cache_loops[{hint}][i]);
    }}
    return resvec;
}}"""


# def dblloop(hint):
#     hstr = dub_dig(hint)

#     return f"""
# template <typename T>
#     std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T>::hA2g{hstr}p(){{
#       std::array<LoopResult<std::complex<T>>, 12> trees{{hA0{hstr}p()}};
#       std::array<LoopResult<std::complex<T>>, 12> loops{{hAg{hstr}pr()}};
#       std::array<LoopResult<std::complex<T>>, 12> dblloops{{hA2g{hstr}pr()}};
#       std::array<LoopResult<std::complex<T>>, 12> resvec;
#       for (int i{{0}}; i < 12; ++i) {{
#         resvec[i] = restore_scale_dep_dblvirt(trees[i], loops[i], dblloops[i]);
#       }}
#       return resvec;
#     }}"""

def dblloop_cache(hint):
    hstr = dub_dig(hint)

    return f"""
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T, P>::hA2g{hstr}p()
{{
    std::array<LoopResult<std::complex<T>>, 12> trees {{ hA0{hstr}p() }};

    if (!cached1[{hint}]) {{
        cached1[{hint}] = true;
        cache_loops[{hint}] = hAg{hstr}pr();
    }}

    if (!cached2[{hint}]) {{
        cached2[{hint}] = true;
        cache_dblloops[{hint}] = hA2g{hstr}pr();
    }}

    std::array<LoopResult<std::complex<T>>, 12> resvec;
    for (int i{{0}}; i < 12; ++i) {{
      resvec[i] = restore_scale_dep_dblvirt(trees[i], cache_loops[{hint}][i], cache_dblloops[{hint}][i]);
    }}
    return resvec;
}}"""


if __name__ == "__main__":
    mhvs = (7, 11, 13, 14, 19, 21, 22, 25, 26, 28)

    # print("\n========================== trees ==============================\n")
    # for i in mhvs:
    #     print(tree(i))

    print("\n========================== loops ==============================\n")
    for i in mhvs:
        print(loop_cache(i))

    print("\n========================== dblloops ==============================\n")
    for i in mhvs:
        print(dblloop_cache(i))
