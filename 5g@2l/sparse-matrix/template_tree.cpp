// SSSSS
template <typename T>
std::array<LoopResult<std::complex<T>>, 12> Amp0q5g_a2l<T>::hA0HHp() {
  hamp2vHH.hA0_coeffs(x1, x2, x3, x4, x5);
  const std::complex<T> phase{i_};

  std::array<std::complex<T>, 12> amps{
      hamp2vHH.hA0_p1e(),
      hamp2vHH.hA0_p2e(),
      hamp2vHH.hA0_p3e(),
      hamp2vHH.hA0_p4e(),
      hamp2vHH.hA0_p5e(),
      hamp2vHH.hA0_p6e(),
      hamp2vHH.hA0_p7e(),
      hamp2vHH.hA0_p8e(),
      hamp2vHH.hA0_p9e(),
      hamp2vHH.hA0_p10e(),
      hamp2vHH.hA0_p11e(),
      hamp2vHH.hA0_p12e(),
  };

  for (std::complex<T> &amp : amps) {
    amp *= phase;
  }

  for (int i{0}; i < FFF; ++i) {
    hamp2vHH.f[i] = std::conj(hamp2vHH.f[i]);
  }

  std::array<std::complex<T>, 12> camps{
      hamp2vHH.hA0_p1e(),
      hamp2vHH.hA0_p2e(),
      hamp2vHH.hA0_p3e(),
      hamp2vHH.hA0_p4e(),
      hamp2vHH.hA0_p5e(),
      hamp2vHH.hA0_p6e(),
      hamp2vHH.hA0_p7e(),
      hamp2vHH.hA0_p8e(),
      hamp2vHH.hA0_p9e(),
      hamp2vHH.hA0_p10e(),
      hamp2vHH.hA0_p11e(),
      hamp2vHH.hA0_p12e(),
  };

  const std::complex<T> cphase{std::conj(phase)};

  for (std::complex<T> &camp : camps) {
    camp *= cphase;
  }

  std::array<LoopResult<std::complex<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}

