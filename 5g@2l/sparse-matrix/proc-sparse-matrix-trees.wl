(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/sparse-matrix"];
in="expr-one/exprs/"
outpre="expr-one/"
mhvs={
"+++--",
"++-+-",
"++--+",
"+-++-",
"+-+-+",
"+--++",
"-+++-",
"-++-+",
"-+-++",
"--+++"
}


sm[fin_,ord_,par_]:=Module[
	{a=fin[[3,ord,par]],entries},
	entries=a[[2]]/.{m[i_,j_]:>m[i-1,j-1]};
	SM[Length[entries],a[[1]],entries]
];
fv[fin_]:=Module[
	{a=First[#]-1&/@#&/@fin[[1]]},
	fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=Module[
	{a=#-1&/@fin[[2]]},
	Fi[#]->With[{b=a[[#]]},V[Length[b],b]]&/@Range[Length[a]]
];


finsZero=Get[in<>"partials_5g_0L_tHV_"<>#<>".m"]&/@mhvs;
finsZeroC=Get[in<>"partials_5g_0Lc_tHV_"<>#<>".m"]&/@mhvs;


export[fins_,hels_,out_]:=
Do[
fin=fins[[i]];
hel="h"<>hels[[i]];
Fvi=Fv[fin];
fvi=fv[fin];
fmsi=fms[fin];
fmsl=Length[fmsi];
ymsi=yms[fin];
ymsl=Length[ymsi];
o=outpre<>out<>"/";
Export[o<>hel<>"/Fv.m",Fvi];
Export[o<>hel<>"/fv.m",fvi];
Export[o<>hel<>"/fm.m",fmsi];
Export[o<>hel<>"/f_size.txt", fmsl];
Export[o<>hel<>"/ym.m",ymsi];
Export[o<>hel<>"/y_size.txt", ymsl];
Export[o<>hel<>"/e"<>ToString[#]<>".m",sm[fin,#,1]]&/@Range[12];
(* Export[o<>hel<>"/o"<>ToString[#]<>".m",sm[fin,#,2]]&/@Range[12]; *)
,{i,Length[hels]}];


export[finsZero, mhvs, "zero"];
