#!/usr/bin/env python3

import sys
import string
import os
from pathlib import Path

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "../../../3g2a@2l/proc")
sys.path.insert(0, filename)

from static import uhvs, mhvs, start


def gen_list(partial_name, num, par=""):
    txt = ""
    for i in range(1, num + 1):
        txt += partial_name + str(i) + par
        if i != num:
            txt += ", "
    return txt


def hdr(process, hstr, n, years, b2l, loop, mhv):
    bapp = "2L" if b2l else ""
    txt = start(f"{process}/{process}-2v-{hstr}.h", years)

    txt += f"""#ifndef FINREM_{process.upper()}_2V_{h.upper()}_H
#define FINREM_{process.upper()}_2V_{h.upper()}_H

#include <array>
#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#include \"../../ngluon2/EpsQuintuplet.h\"
#include \"../../ngluon2/utility.h\"

#include \"{process}-2v-hel.h\"

template <typename T>
class Amp{process}_a2v_{hstr} : public HelAmp{process}{bapp}<T> {{
  using Base = HelAmp{process}{bapp}<T>;
public:
  Amp{process}_a2v_{hstr}(const std::vector<std::complex<T>> &sf_);

  using Base::c_{loop};
  using Base::coeffVec;
  using Base::f_{loop};
"""

    if b2l:
        txt += """  using Base::c_2ge;
  using Base::c_2go;
  using Base::f_2g;
"""

    txt += "\n"

    if b2l:
        if mhv:
            for i in range(1, n + 1):
                txt += f"  virtual void fill_m_2g{i}() override;\n"
            txt += "\n"

            for i in range(1, n + 1):
                j = 0
                a = string.ascii_lowercase[j]
                # eg 0q5g-2v-ppmpm-2l-o4a.cpp
                while Path(f"/home/ryan/git/njet-develop/finrem/0q5g/{p}-2v-{h}-2l-o{i}{a}.cpp").is_file():
                    txt += f"  void fill_m_2g{i}{a}();\n"
                    j += 1
                    a = string.ascii_lowercase[j]

        txt += "\n  // }{ 1l\n\n"

    for i in range(1, n + 1):
        txt += f"  using Base::fi_{loop}{i};\n"
    txt += "\n"

    for i in range(1, n + 1):
        txt += f"  using Base::m_{loop}{i};\n"

    txt += f"""
  virtual void hA{loop}_coeffs(std::complex<T> x1, std::complex<T> x2,
                               std::complex<T> x3, std::complex<T> x4,
                               std::complex<T> x5) override;

"""

    if b2l:
        txt += "  // }{ 2l\n\n"

        for i in range(1, n + 1):
            txt += f"  using Base::fi_2g{i};\n"
        txt += "\n"

        for i in range(1, n + 1):
            txt += f"  using Base::m_2g{i}e;\n  using Base::m_2g{i}o;\n"

        txt += """
  virtual void hA2g_coeffs(std::complex<T> x1, std::complex<T> x2,
                            std::complex<T> x3, std::complex<T> x4,
                            std::complex<T> x5) override;

"""

        txt += """
  // }
"""

    txt += f"""
}};

#endif // FINREM_{p.upper()}_2V_{h.upper()}_H"""
    return txt


if __name__ == "__main__":
    p = "0q5g"
    PF = 12
    for h in uhvs:
        print(h)
        with open(os.path.join(dirname, f"../export/{p}-2v-{h}.h"), "w") as f:
            f.write(hdr(p, h, PF, "2020, 2021", False, "g", False))
    for h in mhvs:
        print(h)
        with open(os.path.join(dirname, f"../export/{p}-2v-{h}.h"), "w") as f:
            f.write(hdr(p, h, PF, "2020, 2021", True, "g", True))
