#!/usr/bin/env python3

import sys
import string
from pathlib import Path


def fillup(p, n, h):
    for i in range(1, n + 1):
        # eg 0q5g-2v-ppmpm-2l-o4a.cpp
        if Path(
            f"/home/ryan/git/njet-develop/finrem/{p}/{p}-2v-{h}-2l-o{i}a.cpp"
        ).is_file():
            print(f"template <typename T> void Amp{proc}_a2v_{h}<T>::fill_m_2g{i}() {{")
            j = 0
            a = string.ascii_lowercase[j]
            while Path(
                f"/home/ryan/git/njet-develop/finrem/{p}/{p}-2v-{h}-2l-o{i}{a}.cpp"
            ).is_file():
                print(f"  fill_m_2g{i}{a}();")
                j += 1
                a = string.ascii_lowercase[j]
            print("}\n")


if __name__ == "__main__":
    proc = "0q5g"
    lcol = 12
    hel = sys.argv[1]
    fillup(proc, lcol, hel)
