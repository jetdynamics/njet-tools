#include <array>
#include <chrono>
#include <iostream>

#include "analytic/0q5g-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "hel.hpp"

template <typename T>
void run()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    const int n { 5 };

    const std::array<MOM<T>, n> mom { { { -5.0000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, 5.0000000000000000e+00 },
        { -5.0000000000000000e+00, -0.0000000000000000e+00, -0.0000000000000000e+00, -5.0000000000000000e+00 },
        { 4.1073910982315063e+00, 2.0393336282060184e+00, -3.5476001585959542e+00, -3.5540554501787402e-01 },
        { 2.3727316959679570e+00, 6.2348782873773423e-01, 1.6215556401520543e+00, 1.6160680475641929e+00 },
        { 3.5198772058005368e+00, -2.6628214569437527e+00, 1.9260445184439003e+00, -1.2606625025463190e+00 } } };

    const T scale { 1. };
    Amp0q5g_a<T> amp(scale);

    const T mur { 91.118 };
    amp.setMuR2(mur * mur);

    amp.setMomenta(mom.data());

    Helicity<n> hels { -1, -1, +1, +1, +1 };
    amp.setHelicity(hels.data());

    const std::complex<T> hamp_tree { amp.A0() };

    std::cout
        << '\n'
        << "A(" << hels << ") tree"
        << '\n'
        << hamp_tree
        << '\n'
        << '\n';

    auto t1 { std::chrono::high_resolution_clock::now() };
    const EpsQuintuplet<T> hamp_val { amp.A2L() };
    auto t2 { std::chrono::high_resolution_clock::now() };

    std::cout
        << '\n'
        << "A(" << hels << ") gVV"
        << '\n'
        << hamp_val
        << '\n'
        << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "ms"
        << '\n'
        << '\n';

    // const EpsQuintuplet<T> test{T(-32.)*EpsQuintuplet<T>(T(0.), T(0.), T(1.), T(0.), T(0.))};
    // std::cout
    //     << test
    //     << '\n';

    // const EpsQuintuplet<T> hamp2_val { amp.dblvirt(hels.data()) };

    // std::cout
    //     << '\n'
    //     << "|A(" << hels << ")|^2 @ gVV x tree (col sum)"
    //     << '\n'
    //     << hamp2_val
    //     << '\n'
    //     << '\n';

    // const EpsQuintuplet<T> amp2_val { amp.dblvirt() };

    // std::cout
    //     << '\n'
    //     << "Helicity sum"
    //     << '\n'
    //     << amp2_val
    //     << '\n'
    //     << '\n';
}

// int main(int argc, char* argv[])
int main()
{
    run<double>();
}
