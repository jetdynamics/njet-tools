#include <array>
#include <iostream>

template <int mul>
struct Helicity : std::array<int, mul> {
};

template <int mul>
std::ostream& operator<<(std::ostream& out, const Helicity<mul>& hel)
{
    for (int h : hel) {
        out << (h == 1 ? '+' : '-');
    }
    return out;
}
