#!/usr/bin/env bash

OUT=$2

I="
#include \"0q5g-analytic.h\"

template <typename T>
std::array<typename Amp0q5g_a<T>::DoubleLoopValue, 12> Amp0q5g_a<T>::hA2gP28()
{
    const std::array<int, 5> p {0,1,2,3,4};
    setxi_2l(p.data());
    setpenta(p.data());
"
F="
for (DoubleLoopValue amp : amps) {
    amp = correction2L(amp);
}
return amps;
}

#ifdef USE_SD
template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a<Vc::double_v>;
#endif
"

rm -f ${OUT}

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|pow\[Pi, 2\]|PI2|g;
        s|([ (\-={\/] ?)(-?\d+)\.?( ?[*\/;+\-\n},)])|\1T(\2.)\3|g;
        s|([ (\-={\/] ?)(-?\d+)\.?( ?[*\/;+\-\n},])|\1T(\2.)\3|g;
        s|pow\[(.*?),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|gs;
        s|pow\[(.*),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|g;
        s|ep5\[(\d)\]|ep5.get\1()|g;
        " \
    $1 > ${OUT}
