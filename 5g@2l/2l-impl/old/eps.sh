#!/usr/bin/env bash

# use on FORM processed file

OUT=$1

line() {
    local LINE="DoubleLoopValue("
    for j in {0..5}; do
        LINE+="ie_${j}_${1}"
        if [[ $j != 5 ]]; then
            LINE+=","
        else
            LINE+=')'
        fi
    done
    echo $LINE
}

echo -e "std::array<DoubleLoopValue, 12> amps{" >${OUT}
for i in {1..11}; do
    echo -e "$(line $i)," >>${OUT}
done
echo "$(line 12)};" >>${OUT}
