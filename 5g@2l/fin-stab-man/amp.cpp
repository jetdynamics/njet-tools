#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

// my_scale2 = scale^2 (scaling factor for terms of mass dimension 2)
template <typename T>
T loop(Amp0q5g_a2l<T>& amp, std::array<MOM<T>, N> moms, const T my_scale = 1.)
{
    const T my_scale2 { my_scale * my_scale };
    const T bare_mur2 { 1. };
    const T mur2 { bare_mur2 * my_scale2 };
    amp.setMuR2(mur2);

    for (MOM<T>& mom : moms) {
        mom *= my_scale;
    }
    amp.setMomenta(moms.data());
    amp.setpenta2l();

    //const T born { amp.born_fin() };
    //const T virt { amp.virt_fin() };
    //const T rvob { virt / born };
    //const T virtsq { amp.virtsq_fin() };
    //const T rv2ob { virtsq / born };
    //const T dblvirt { amp.dblvirt_fin() };
    //const T r2vob { dblvirt / born };

    std::array<int, N> hels {{-1,-1,1,1,1}};
    int* hel{hels.data()};
    //const T born { amp.born_fin(hel) };
    const T virt { amp.virt_fin(hel) };
    //const T rvob { virt / born };
    //const T virtsq { amp.virtsq_fin(hel) };
    //const T rv2ob { virtsq / born };
    //const T dblvirt { amp.dblvirt_fin(hel) };
    //const T r2vob { dblvirt / born };

    std::cout
        << "scale:            " << my_scale << '\n'
        //<< "born:             " << born << '\n'
        //<< "rescale(born):    " << born*my_scale2 << '\n'
        << "virt:             " << virt << '\n'
        << "rescale(virt):    " << virt*my_scale2 << '\n'
        //<< "virt/born:        " << rvob << '\n'
        //<< "virtsq:           " << rv2ob << '\n'
        //<< "rescale(virtsq):  " << virtsq*my_scale2 << '\n'
        //<< "virtsq/born:      " << rv2ob << '\n'
        //<< "dblvirt:          " << r2vob << '\n'
        //<< "rescale(dblvirt): " << dblvirt*my_scale2 << '\n'
        //<< "dblvirt/born:     " << r2vob << '\n'
        << '\n';

    return 1.;
}

template <typename T>
void scaling(Amp0q5g_a2l<T>& amp)
{
    //std::random_device dev;
    //std::mt19937_64 rng(dev());
    //const T boundary { 0.01 };
    //std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    //std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    //T y1 { dist_y1(rng) };
    //std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
    //T y2 { dist_y2(rng) };
    //T theta { dist_a(rng) };
    //T alpha { dist_a(rng) };

    // point A
    T y1 = 2.5466037534799052e-01;
    T y2 = 3.9999592899158604e-01;
    T theta = 1.2398476487204491e+00;
    T alpha = 4.3891163982516607e-01;

    std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

    const T one { loop<T>(amp, moms, 1.) };
    const T two { loop<T>(amp, moms, 1.2857142857142855) };
    std::cout
        //<< "dblvirt diff: " << one - two << '\n'
        << '\n';
}

template <typename T>
void several(const int num = 1)
{
    std::cout
        << "Initialising amplitude class..." << '\n'
        << '\n';

    const T scale { 1. };
    const int Nc { 3 };
    Amp0q5g_a2l<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);

    for (int i { 0 }; i < num; ++i) {
        std::cout
            << "===============" << '\n'
            << "Scaling test " << i << '\n';
        scaling<T>(amp);
    }
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout
        << '\n'
        << "------------------------------" << '\n'
        << "Scaling tests of helicity sums" << '\n'
        << "------------------------------" << '\n'
        << '\n';

    several<double>(1);
}
