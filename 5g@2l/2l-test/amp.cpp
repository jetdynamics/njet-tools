#include <array>
#include <cassert>
#include <chrono>
#include <complex>
#include <iostream>
#include <random>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
Eps5<T> run(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const int Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a2l<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);

    // std::cout << '\n';
    // for (int i { 0 }; i < N; ++i) {
    //     for (int j { i + 1 }; j < N; ++j) {
    //         std::cout << "s" << i + 1 << j + 1 << "=" << dot(mom[i], mom[j]) * 2 << '\n';
    //     }
    // }

    amp.setMomenta(mom.data());
    auto t5 { std::chrono::high_resolution_clock::now() };
    amp.setpenta2l();
    auto t6 { std::chrono::high_resolution_clock::now() };
    std::cout << "\nPentagon evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count() << "ms\n";

    // const Eps5<T> hamp_virt { amp.virt(hels.data()) };

    //amp.setHelicity(hels.data());
    // std::complex<T> tree {amp.A0()};
    // std::cout << amp.A0() << ",\n";

    // std::cout << amp.AL().loop << ",\n";
    // std::cout << amp.ALp() << ",\n";
    // std::cout << amp.ALp().get2()/tree << ",\n";
    // std::cout << amp.ALp().get1()/amp.AL().loop.get1() << ",\n";

    // std::cout << amp.virt_prim(hels.data()) << ",\n";
    // auto t1 { std::chrono::high_resolution_clock::now() };
    // Eps5<T> part { amp.virt_part(hels.data()) };
    // auto t2 { std::chrono::high_resolution_clock::now() };

    std::cout << "V: " << amp.virt_part() << '\n';

    amp.setxi_penta();
    auto t3 { std::chrono::high_resolution_clock::now() };
    //Eps5<T> penta { amp.dblvirt(hels.data()) };
    //amp.born();
    Eps5<T> penta { amp.dblvirt() };
    auto t4 { std::chrono::high_resolution_clock::now() };
    //Eps5<T> loopsq { amp.virtsq(hels.data()) };
    //Eps5<T> loopsq { amp.virtsq() };

    std::cout
        // << "part:  " << part
        // << "\nTime: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
        << "2V: " << penta
        << "\nTime: " << std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count() << "ms\n";

    //std::cout << "2l/tree: " << amp.A2Lp() / amp.A0() << '\n';

    //std::cout << "V^2: " << loopsq << '\n';

    // return hamp_virt;
    return penta;
}

template <typename T>
Eps5<T> run2(const std::array<MOM<T>, N> mom, const int Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a2l<T> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur * mur);
    amp.setMomenta(mom.data());
    auto t5 { std::chrono::high_resolution_clock::now() };
    amp.setpenta2l();
    auto t6 { std::chrono::high_resolution_clock::now() };
    std::cout << "\nPentagon evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count() << "ms\n";

    auto t3 { std::chrono::high_resolution_clock::now() };
    Eps5<T> penta { amp.dblvirt() };
    auto t4 { std::chrono::high_resolution_clock::now() };

    std::cout
        // << "part:  " << part
        // << "\nTime: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << "us\n"
        << "2V: " << penta
        << "\nTime: " << std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count() << "ms\n";

    std::cout << "V: " << amp.virt_part() << '\n';

    return penta;
}

template <typename T>
void loop_matrix(const int num_ps = 10, const int num_nc = 1, const int num_mur = 1)
{
    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    //std::cout << '{' << '\n';
    for (int i { 0 }; i < num_ps; ++i) {

        T y1 { dist_y1(rng) };
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        T y2 { dist_y2(rng) };
        T theta { dist_a(rng) };
        T alpha { dist_a(rng) };
        // std::cout << y1 << " "
        //           << y2 << " "
        //           << theta << " "
        //           << alpha << " "
        //           << '\n';
        y1 = 2.5466037534799052e-01;
        y2 = 3.9999592899158604e-01;
        theta = 1.2398476487204491e+00;
        alpha = 4.3891163982516607e-01;
        const std::array<MOM<T>, N> moms { phase_space_point(y1, y2, theta, alpha) };

        const Helicity<N> hels { { -1, -1, 1, 1, 1 } };

        for (int Nc { 3 }; Nc < 3 * std::pow(10, num_nc); Nc *= 10) {
            for (T mur { 1. }; mur < std::pow(10., num_mur); mur *= 10.) {
                //std::cout << "{" << '\n';

                const Eps5<T> val2 { run2<T>(moms, Nc, mur) };
                const Eps5<T> val1 { run<T>(hels, moms, Nc, mur) };

                //std::cout << "{" << '\n';
                //for (int j { 0 }; j < N - 1; ++j) {
                //    std::cout << moms[j] << ',' << '\n';
                //}
                //std::cout << moms[N - 1] << "\n},\n";

                // std::cout << mur << "," << '\n';

                // std::cout << Nc << "," << '\n';

                // std::cout << hamp_virt << '\n';

                //std::cout << "}," << '\n';
            }
        }
    }
    //std::cout << '}' << '\n';
}

int main()
{
    std::cout.precision(8);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>(1, 1, 1);
}
