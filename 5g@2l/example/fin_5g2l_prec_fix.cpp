#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Mom.h"
#include "ngluon2/refine.h"

// function printing 5-gluon finite remainders of virtual contributions for given phase space points
void finite_remainder(const std::vector<std::vector<std::vector<double>>>& input_points)
{
    const double mur2 { 1. };
    const int Nc { 3 };
    const int Nf { 0 };
    // no external mass scales
    const std::vector<double> scales2 { {
        0,
    } };
    // cutoff for desired relative error in dblvirt scaling test
    const double cutoff_errorSD { 1e-3 };
    const dd_real cutoff_errorDD { static_cast<dd_real>(cutoff_errorSD) };

    std::cout << "Initialising amplitude classes..." << '\n';

    NJetAccuracy<double>* const ampSDSD { NJetAccuracy<double>::template create<Amp0q5g_a2l<double, double>>() };
    ampSDSD->setMuR2(mur2);
    ampSDSD->setNf(Nf);
    ampSDSD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampDDSD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, double>>() };
    ampDDSD->setMuR2(mur2);
    ampDDSD->setNf(Nf);
    ampDDSD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampDDDD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, dd_real>>() };
    ampDDDD->setMuR2(mur2);
    ampDDDD->setNf(Nf);
    ampDDDD->setNc(Nc);

    for (std::size_t p { 0 }; p < input_points.size(); ++p) {
        std::cout
            << "Evaluating point " << p << "..." << '\n'
            << "  Try coeff(double)+pentagon(double)..." << '\n';

        // convert momenta to NJet format
        std::vector<MOM<double>> momentaSD(5);
        for (int i { 0 }; i < 5; ++i) {
            momentaSD[i] = MOM<double>(
                input_points[p][i][0],
                input_points[p][i][1],
                input_points[p][i][2],
                input_points[p][i][3]);
        }
        refineM(momentaSD, momentaSD, scales2);

        ampSDSD->setMomenta(momentaSD);
        ampSDSD->setxi();
        ampSDSD->setSpecFuncs();

        ampSDSD->born_fin();
        ampSDSD->virt_fin();
        ampSDSD->virtsq_fin();
        ampSDSD->dblvirt_fin();

        const double errorSD { std::abs(ampSDSD->dblvirt_fin_error() / ampSDSD->dblvirt_fin_value()) };

        std::cout
            << "    Born:                  " << ampSDSD->born_fin_value() << '\n'
            << "    Born error:            " << std::abs(ampSDSD->born_fin_error() / ampSDSD->born_fin_value()) << '\n'
            << "    Virt:                  " << ampSDSD->virt_fin_value() << '\n'
            << "    Virt error:            " << std::abs(ampSDSD->virt_fin_error() / ampSDSD->virt_fin_value()) << '\n'
            << "    Virtsq:                " << ampSDSD->virtsq_fin_value() << '\n'
            << "    Virtsq error:          " << std::abs(ampSDSD->virtsq_fin_error() / ampSDSD->virtsq_fin_value()) << '\n'
            << "    Dblvirt:               " << ampSDSD->dblvirt_fin_value() << '\n'
            << "    Dblvirt error:         " << errorSD << '\n'
            << "    Virt/Born:             " << ampSDSD->virt_fin_value()/ampSDSD->born_fin_value() << '\n'
            << "    (Dblvirt+Virtsq)/Born: " << (ampSDSD->dblvirt_fin_value()+ampSDSD->virtsq_fin_value())/ampSDSD->born_fin_value() << '\n'
	;

        if (errorSD < cutoff_errorSD) {
            std::cout << "    Stability test passed" << '\n';
        } else {
            std::cout
                << "    Stability test failed" << '\n'
                << "  Try coeff(double-double)+pentagon(double)..." << '\n';

            dd_real errorDD;

            // convert phase space point to double-double precision, fixing momentum conservation and masslessness at higher precision
            std::vector<MOM<dd_real>> momentaDD(5);
            refineM(momentaSD, momentaDD, scales2);

            ampDDSD->setMomenta(momentaDD);
            ampDDSD->setxi();
            ampDDSD->copySpecFuncs(ampSDSD);

            ampDDSD->born_fin();
            ampDDSD->virt_fin();
            ampDDSD->virtsq_fin();
            ampDDSD->dblvirt_fin();

            errorDD = abs(ampDDSD->dblvirt_fin_error() / ampDDSD->dblvirt_fin_value());

            std::cout
                << "    Born:          " << ampDDSD->born_fin_value() << '\n'
                << "    Born error:    " << abs(ampDDSD->born_fin_error() / ampDDSD->born_fin_value()) << '\n'
                << "    Virt:          " << ampDDSD->virt_fin_value() << '\n'
                << "    Virt error:    " << abs(ampDDSD->virt_fin_error() / ampDDSD->virt_fin_value()) << '\n'
                << "    Virtsq:        " << ampDDSD->virtsq_fin_value() << '\n'
                << "    Virtsq error:  " << abs(ampDDSD->virtsq_fin_error() / ampDDSD->virtsq_fin_value()) << '\n'
                << "    Dblvirt:       " << ampDDSD->dblvirt_fin_value() << '\n'
                << "    Dblvirt error: " << errorDD << '\n';

            if (errorDD < cutoff_errorDD) {
                std::cout
                    << "    Stability test passed" << '\n';

            } else {
                std::cout
                    << "    Stability test failed" << '\n'
                    << "  Try coeff(double-double)+specialFn(double-double)..." << '\n';

                ampDDDD->setMomenta(momentaDD);
                ampDDDD->setxi();
                ampDDDD->setSpecFuncs();

                ampDDDD->born_fin();
                ampDDDD->virt_fin();
                ampDDDD->virtsq_fin();
                ampDDDD->dblvirt_fin();

                errorDD = abs(ampDDDD->dblvirt_fin_error() / ampDDDD->dblvirt_fin_value());

                std::cout
                    << "    Born:          " << ampDDDD->born_fin_value() << '\n'
                    << "    Born error:    " << abs(ampDDDD->born_fin_error() / ampDDDD->born_fin_value()) << '\n'
                    << "    Virt:          " << ampDDDD->virt_fin_value() << '\n'
                    << "    Virt error:    " << abs(ampDDDD->virt_fin_error() / ampDDDD->virt_fin_value()) << '\n'
                    << "    Virtsq:        " << ampDDDD->virtsq_fin_value() << '\n'
                    << "    Virtsq error:  " << abs(ampDDDD->virtsq_fin_error() / ampDDDD->virtsq_fin_value()) << '\n'
                    << "    Dblvirt:       " << ampDDDD->dblvirt_fin_value() << '\n'
                    << "    Dblvirt error: " << errorDD << '\n';

                if (errorDD < cutoff_errorDD) {
                    std::cout
                        << "    Stability test passed" << '\n';
                } else {
                    std::cout
                        << "    Stability test failed (END)" << '\n';
                }
            }
        }
    }
}

int main()
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(32);
    std::cout << '\n';

    // some 5-point massless kinematic points (all momenta outgoing)
    const std::vector<std::vector<std::vector<double>>> phase_space_points {
        {
            {
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, 5.000000000000000000e-01 },
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, -5.000000000000000000e-01 },
                { 4.107391098231506499e-01, 2.039333628206018434e-01, -3.547600158595954900e-01, -3.554055450178741221e-02 },
                { 2.372731695967957299e-01, 6.234878287377346173e-02, 1.621555640152054500e-01, 1.616068047564193089e-01 },
                { 3.519877205800536757e-01, -2.662821456943753051e-01, 1.926044518443900122e-01, -1.260662502546318897e-01 },
            },
            {
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, 5.000000000000000000e-01 },
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, -5.000000000000000000e-01 },
                { 4.688448615968078825e-01, -1.412668393952754287e-01, 3.007176610278845114e-02, 4.460435777101365851e-01 },
                { 4.977111060628933159e-01, 1.354360195136899148e-01, -2.365795255280786491e-02, -4.783447825550574128e-01 },
                { 3.344403234029869054e-02, 5.830819881585498285e-03, -6.413813549980587093e-03, 3.230120484492083471e-02 },
            },
            {
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, 5.000000000000000000e-01 },
                { -5.000000000000000000e-01, 0.000000000000000000e+00, 0.000000000000000000e+00, -5.000000000000000000e-01 },
                { 4.485838326229346640e-01, 1.869441196817776540e-01, 1.729674973780522262e-01, -3.692717100697800459e-01 },
                { 4.925705062018550517e-01, -2.416856369060299825e-01, -1.795888846967947372e-01, 3.898225095946098451e-01 },
                { 5.884566117521039530e-02, 5.474151722425232847e-02, 6.621387318742520504e-03, -2.055079952482980268e-02 },
            },
        }
    };

    finite_remainder(phase_space_points);

    std::cout << '\n';
}
