#!/usr/bin/env python3
# coding=UTF-8

import numpy
import matplotlib.pyplot


def read(filename):
    with open(filename, "r") as filecont:
        return numpy.genfromtxt(filecont, delimiter=" ")


def bad(data, cut):
    print("bad points:")
    for i, e in enumerate(data[:, 7], start=1):
        if e > cut:
            print(i, e)


def bad100k(data, cut):
    qs = data[data[:, 8] > cut]
    rs = qs[qs[:, 0].argsort()]
    ps = rs[:, 0]
    for i in ps:
        print(int(i))


def bad100k_stats(data, cut):
    n = 0
    for e in data[:, 8]:
        if abs(e) > cut:
            n += 1
    print(f"{n} points with error > {cut}")
    print(f"That's {100*n/len(data)}%")


def plot10k(data):
    fig, ax = matplotlib.pyplot.subplots()

    virt = numpy.absolute(data[:, 3])
    virtsq = numpy.absolute(data[:, 5])
    dblvirt = numpy.absolute(data[:, 7])

    matplotlib.pyplot.xscale("log")
    bins = numpy.logspace(-16, 2, num=100, base=10)

    ax.hist(
        dblvirt,
        bins=bins,
        histtype="step",
        log=True,
        label="dblvirt",
    )
    ax.hist(
        virtsq,
        bins=bins,
        histtype="step",
        log=True,
        label="virtsq",
    )
    ax.hist(
        virt,
        bins=bins,
        histtype="step",
        log=True,
        label="virt",
    )
    ax.text(
        0.95,
        0.75,
        f"Sample size: {len(data)}",
        transform=ax.transAxes,
        horizontalalignment="right",
        verticalalignment="top",
    )
    ax.legend(loc="best")
    ax.set_xlabel("Relative error")
    ax.set_ylabel("Frequency")
    ax.set_title(f"Stability")

    fig.savefig("py_histo_dblvirt.png", bbox_inches="tight", format="png", dpi=300)


def plot100k(data):
    fig, ax = matplotlib.pyplot.subplots()

    virt = numpy.absolute(data[:, 4])
    virtsq = numpy.absolute(data[:, 6])
    dblvirt = numpy.absolute(data[:, 8])

    matplotlib.pyplot.xscale("log")
    bins = numpy.logspace(-16, 2, num=100, base=10)

    ax.hist(
        dblvirt,
        bins=bins,
        histtype="step",
        log=True,
        label="dblvirt",
    )
    ax.hist(
        virtsq,
        bins=bins,
        histtype="step",
        log=True,
        label="virtsq",
    )
    ax.hist(
        virt,
        bins=bins,
        histtype="step",
        log=True,
        label="virt",
    )
    ax.text(
        0.95,
        0.75,
        f"Sample size: {len(data)}",
        transform=ax.transAxes,
        horizontalalignment="right",
        verticalalignment="top",
    )
    ax.legend(loc="best")
    ax.set_xlabel("Relative error")
    ax.set_ylabel("Frequency")
    ax.set_title(f"Stability")

    fig.savefig("py_histo_100k.png", bbox_inches="tight", format="png", dpi=300)


def study_bads(data):
    new = []
    for line in data:
        if line[15] in (1.0, 2.0, 3.0, 4.0):
            new.append([*line[:9], line[15], *line[9:15]])
        else:
            new.append(line)
    d = numpy.array(new)

    print(f"cutoff=1e-3")
    print()

    dd = (d[:, 9] == 1).sum()
    qd = (d[:, 9] == 2).sum()
    qq = (d[:, 9] == 3).sum()
    ff = (d[:, 9] == 4).sum()
    tt = len(data)
    print(f"{tt} bad points in 100k: {100*tt/100000}%")
    print(f"{qd} passed qd: {round(100*qd/tt,1)}%")
    print(f"{qq} passed qq: {round(100*qq/tt,1)}%")
    print(f"{ff} failed: {round(100*ff/tt,1)}%")
    print()

    print("Random point timing:")
    print(f"coeffs(double): {round(d[2, 11]/1e3)} ms")  # d c
    print(f"special(double): {round(d[2, 10]/1e6,1)} s")  # d sf
    print(f"coeffs(quad): {round(d[2, 15]/1e6,1)} s")  # q c
    print(f"special(quad): {round(d[2, 14]/6e7,1)} mins")  # q sf
    print()

    # 10 | 11 || 12 | 13 || 14 | 15
    # ds | dc || ds | qc || qs | qc

    ran_dd = tt
    ran_qd = tt - dd
    ran_qq = tt - qd
    assert ran_qq == ff + qq
    print("Average times:")
    print(f"coeffs(double): {round(d[:, 11].sum() / 1e3 / ran_dd,1)} ms")  # d c
    print(
        f"special(double): {round((d[:, 10].sum()+d[:, 12].sum()) / 1e6 / (2*ran_dd),1)} s"
    )  # d sf
    print(
        f"coeffs(quad): {round((d[:, 13].sum()+d[:, 15].sum()) / 1e6 / (2*ran_qd),1)} s"
    )  # q c
    print(f"special(quad): {round(d[:, 14].sum() / 1e6 / 60 / ran_qq,1)} mins")  # q sf


if __name__ == "__main__":
    # data10k = read("all.csv")
    # bad(data10k, 1e-2)
    # plot10k(data10k)

    # data100k = read("all100k.csv")
    # bad100k(data100k, 1e-3)
    # plot100k(data100k)

    bad = read("bad_details_num.csv")
    study_bads(bad)
