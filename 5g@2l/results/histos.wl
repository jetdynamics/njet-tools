(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/results"];


alphas=0.118;
fac=alphas/4/Pi;


(* ::Subsection:: *)
(*Stability*)


data=Import["all.csv", "Table", "FieldSeparators" -> " "]//Transpose;


Print[Length[data[[8]]]];


tickSpecification = Table[{10^(-i), -i}, {i, -3, 30}];
extent={{10^(-16),10^(2)},Automatic};


histoBornLin=Histogram[
Abs/@data[[8]],
{"Log",36},
Ticks->{tickSpecification,Automatic},
AxesLabel->{"Rel err exp","Freq"},
PlotLabel->"Dblvirt 10k",
PlotRange->extent
]


histoDblvirt=Histogram[
Abs/@data[[8]],
{"Log",36},
ScalingFunctions->"Log",
Ticks->{tickSpecification,Automatic},
AxesLabel->{"Rel err exp","Freq"},
PlotLabel->"Dblvirt 10k",
PlotRange->extent
]


histoVirt=Histogram[
Abs/@data[[4]],
"Log",
ScalingFunctions->"Log",
Ticks->{tickSpecification,Automatic},
AxesLabel->{"Rel err exp","Freq"},
PlotLabel->"Virt 10k",
PlotRange->extent
]


histoVirtsq=Histogram[
Abs/@data[[6]],
"Log",
ScalingFunctions->"Log",
Ticks->{tickSpecification,Automatic},
AxesLabel->{"Rel err exp","Freq"},
PlotLabel->"Virtsq 10k",
PlotRange->extent
]


tickSpecificationBorn = Table[{10^(-i), -i}, {i, 0, 30,2}];
histoBorn=Histogram[
Abs/@data[[2]],
"Log",
ScalingFunctions->"Log",
Ticks->{tickSpecificationBorn,Automatic},
AxesLabel->{"Rel err exp","Freq"},
PlotLabel->"Born 10k",
PlotRange->extent
]


histoAll=GraphicsRow[{histoVirt,histoVirtsq,histoDblvirt},ImageSize->Full]


Export["all_err_histo.png",histoAll];
Export["all_err_histo.pdf",histoAll];


Export["dblvirt_err_histo.png",histoDblvirt];
Export["dblvirt_err_histo.pdf",histoDblvirt];
Export["dblvirt_err_histo.webp",histoDblvirt];


Export["virt_err_histo.png",histoVirt];
Export["virt_err_histo.pdf",histoVirt];


Export["virtsq_err_histo.png",histoVirtsq];
Export["virtsq_err_histo.pdf",histoVirtsq];


Export["born_err_histo.png",histoBorn];
Export["born_err_histo.pdf",histoBorn];


(* ::Subsection::Closed:: *)
(*Slice x1*)


slice=Import["slice.csv", "Table", "FieldSeparators" -> " "]//Transpose;


dblvirts=DeleteCases[Transpose[{slice[[1]],slice[[8]]}],{_,x_}/;Abs[x]>10^11];
Length[slice[[1]]]
Length[dblvirts]


Bs=slice[[2]];
Br=Bs/Bs;
borns=Transpose[{slice[[1]],Bs}];
RB=Transpose[{slice[[1]],Br}];
virts=Transpose[{slice[[1]],Abs/@slice[[4]]}];
Vs=Bs+fac*slice[[4]];
Vr=Vs/Bs;
V=Transpose[{slice[[1]],Vs}];
RV=Transpose[{slice[[1]],Vr}];
virtsqs=Transpose[{slice[[1]],Abs/@slice[[6]]}];
dblvirts=Transpose[{slice[[1]],Abs/@slice[[8]]}];
VVs=Vs+fac^2*(slice[[6]]+slice[[8]]);
VVr=VVs/Bs;
VV=Transpose[{slice[[1]],VVs}];
RVV=Transpose[{slice[[1]],VVr}];


mag=ListPlot[
{borns,virts,virtsqs,dblvirts},
PlotLabel->"Magnitude of contributions",
AxesLabel->{"x1","Matrix element"},
PlotLegends->{"|born|","|virt|","|virtsq|","|dblvirt|"}
]


cum=ListPlot[
{borns,V,VV},
PlotLabel->"Cumulative virtual corrections",
AxesLabel->{"\!\(\*SubscriptBox[\(x\), \(1\)]\)","Finite remainder"},
PlotLegends->{"B","B +\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(\(\\\ \)\(4\\\ \[Pi]\)\)],
DisplayForm]\) V","B + \!\(\*TagBox[\(\*FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)]\(\\\ \)\),
DisplayForm]\)V + (\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)],
DisplayForm]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) VV"}
]


Export["x1.png",cum];
Export["x1.pdf",cum];


rat=ListPlot[
{RB,RV,RVV},
PlotLabel->"Cumulative virtual correction ratios",
AxesLabel->{"\!\(\*SubscriptBox[\(x\), \(1\)]\)","Ratio to B"},
PlotLegends->{"B/B","(B +\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(\(\\\ \)\(4\\\ \[Pi]\)\)],
DisplayForm]\) V)/B","(B + \!\(\*TagBox[\(\*FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)]\(\\\ \)\),
DisplayForm]\)V + (\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)],
DisplayForm]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) VV)/B"}
]


Export["x1r.png",rat];
Export["x1r.pdf",rat];


(* ::Subsection::Closed:: *)
(*Slice x2*)


slice2=Import["slice_x2.csv", "Table", "FieldSeparators" -> " "]//Transpose;


x2=Range[0.4,0.997,0.003];

Bs2=slice2[[2]];
Br2=Bs2/Bs2;
B2=Transpose[{x2,Bs2}];
RB2=Transpose[{x2,Br2}];

Vs2=Bs2+fac*slice2[[4]];
Vr2=Vs2/Bs2;
V2=Transpose[{x2,Vs2}];
RV2=Transpose[{x2,Vr2}];

VVs2=Vs2+fac^2*(slice2[[6]]+slice2[[8]]);
VVr2=VVs2/Bs2;
VV2=Transpose[{x2,VVs2}];


Length[V2]
V2=DeleteCases[V2,{_,x_}/;Abs[x]>10^16];
Length[V2]


Length[VV2]
VV2=DeleteCases[VV2,{_,x_}/;Abs[x]>10^16];
Length[VV2]


cum=ListPlot[
{B2,V2,VV2},
PlotLabel->"Cumulative virtual corrections",
AxesLabel->{"\!\(\*SubscriptBox[\(x\), \(2\)]\)","Finite remainder"},
PlotLegends->{"B","B +\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(\(\\\ \)\(4\\\ \[Pi]\)\)],
DisplayForm]\) V","B + \!\(\*TagBox[\(\*FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)]\(\\\ \)\),
DisplayForm]\)V + (\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)],
DisplayForm]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) VV"}
]


cum2log=ListPlot[
{B2,V2,VV2},
ScalingFunctions->"Log",
PlotLabel->"Cumulative virtual corrections",
AxesLabel->{"\!\(\*SubscriptBox[\(x\), \(2\)]\)","Finite remainder"},
PlotLegends->{"B","B +\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(\(\\\ \)\(4\\\ \[Pi]\)\)],
DisplayForm]\) V","B + \!\(\*TagBox[\(\*FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)]\(\\\ \)\),
DisplayForm]\)V + (\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)],
DisplayForm]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) VV"}
]


rat2=ListPlot[
{RB2,RV2,RVV2},
PlotLabel->"Cumulative virtual correction ratios",
AxesLabel->{"\!\(\*SubscriptBox[\(x\), \(2\)]\)","Ratio to B"},
PlotLegends->{"B/B","(B +\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(\(\\\ \)\(4\\\ \[Pi]\)\)],
DisplayForm]\) V)/B","(B + \!\(\*TagBox[\(\*FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)]\(\\\ \)\),
DisplayForm]\)V + (\!\(\*TagBox[FractionBox[SubscriptBox[\(\[Alpha]\), \(s\)], \(4\\\ \[Pi]\)],
DisplayForm]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) VV)/B"}
]


Export["x2.png",cum2];
Export["x2.pdf",cum2];
Export["x2r.png",rat2];
Export["x2r.pdf",rat2];
