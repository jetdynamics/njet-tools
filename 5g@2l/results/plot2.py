#!/usr/bin/env python3
# coding=UTF-8

import numpy
import matplotlib.pyplot

# import pandas
# def read(filename):
#     with open(filename, "r") as filecont:
#         return pandas.read_csv(filecont, delim_whitespace=True)


def read(filename):
    with open(filename, "r") as filecont:
        data = numpy.genfromtxt(filecont, delimiter=" ")
    return data[data[:, 0].argsort()]


def histo(data, names, label=""):
    fig, ax = matplotlib.pyplot.subplots()

    matplotlib.pyplot.xscale("log")
    bins = numpy.logspace(-21, 2, num=100, base=10)

    for datum, name in zip(data, names):
        ax.hist(
            datum,
            bins=bins,
            histtype="step",
            log=True,
            label=name,
        )
    # ax.text(
    #     0.95,
    #     0.75,
    #     "Sample sizes: " + ", ".join([str(len(datum)) for datum in data]),
    #     transform=ax.transAxes,
    #     horizontalalignment="right",
    #     verticalalignment="top",
    # )
    ax.legend(loc="best")
    ax.set_xlabel("Relative error")
    ax.set_ylabel("Frequency")
    ax.set_title(f"Stability")

    fig.savefig(
        f"histo{'_' if label else ''}{label}.png",
        bbox_inches="tight",
        format="png",
        dpi=300,
    )


def std_hist(data, name="", all=True):
    dblvirt = numpy.absolute(data[:, 8])
    if all:
        virt = numpy.absolute(data[:, 4])
        virtsq = numpy.absolute(data[:, 6])
        histo([virt, virtsq, dblvirt], ["virt", "virtsq", "dblvirt"], name)
    else:
        histo([dblvirt], ["dblvirt"], name)


def passed(data, cutoff):
    return data[data[:, 8] < cutoff]


def failed(data, cutoff):
    return data[data[:, 8] > cutoff]


def perc(subset, data, dp=1):
    return round(100 * len(subset) / len(data), dp)


def an_cut(total, this_prec, passed, failed, name, dp=1):
    print(f"{name} all:  {len(this_prec)}")
    print(
        f"{name} pass: {len(passed)} ({perc(passed, this_prec, dp)}% {name}; {perc(passed, total, dp)}% all)"
    )
    print(
        f"{name} fail: {len(failed)} ({perc(failed, this_prec, dp)}% {name}; {perc(failed, total, dp)}% all)"
    )


if __name__ == "__main__":
    name = "result"
    print(name)
    all_dd = read(f"{name}.DD.csv")
    all_qd = read(f"{name}.QD.csv")
    all_qq = read(f"{name}.QQ.csv")

    cutoff = 1e-3

    pass_dd = passed(all_dd, cutoff)
    fail_dd = failed(all_dd, cutoff)

    pass_qd = passed(all_qd, cutoff)
    fail_qd = failed(all_qd, cutoff)

    pass_qq = passed(all_qq, cutoff)
    fail_qq = failed(all_qq, cutoff)

    dp = 2
    an_cut(all_dd, all_dd, pass_dd, fail_dd, "SD/SD", dp)
    an_cut(all_dd, all_qd, pass_qd, fail_qd, "DD/SD", dp)
    an_cut(all_dd, all_qq, pass_qq, fail_qq, "DD/DD", dp)

    histo(
        [
            pass_dd[:, 8],
            pass_qd[:, 8],
            pass_qq[:, 8],
            fail_dd[:, 8],
            fail_qd[:, 8],
        ],
        [
            "SD/SD pass",
            "DD/SD pass",
            "DD/DD pass",
            "SD/SD fail",
            "DD/SD fail",
        ],
        f"{name}_cut_{cutoff}",
    )
