#!/usr/bin/env bash

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}

    cd ${h}2

    j=0
    for i in {0..5}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(( $j+2**$i ))
        fi
    done

    echo $h $j

    COEFFS=coeffs.cpp
    perl -p -e "s|HH|$j|g" ../template_coeffs_start.cpp >${COEFFS}
    cat ys.cpp >>${COEFFS}
    cat fs.cpp >>${COEFFS}
    cat ../template_coeffs_end.cpp >>${COEFFS}
    # clang-format -i ${COEFFS}

    for i in {1..12}; do
        FILE=eps_p${i}.cpp
        perl -p -e "s|HH|$j|g; s|PP|$i|g;" ../template_perm_start.cpp >${FILE}
        cat e${i}_odd.cpp >>${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g;" ../template_perm_middle.cpp >>${FILE}
        cat e${i}_even.cpp >>${FILE}
        cat ../template_perm_end.cpp >>${FILE}
        # clang-format -i ${FILE}
    done

    cd ..
done
