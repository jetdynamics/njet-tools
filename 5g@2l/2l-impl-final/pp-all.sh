#!/usr/bin/env bash

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}
    cd $h
    ../pp-form.sh ys.c
    ../pp-form.sh fs.c
    for i in {1..12}; do
        ../pp-form.sh e${i}_even.c
        ../pp-form.sh e${i}_odd.c
    done
    cd ..
done
