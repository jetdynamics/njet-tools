// SSSSS
template <typename T>
std::array<LoopResult<Eps5<T>>, 12> Amp0q5g_a2l<T>::hA2gHH() {
  hA2gHHcoeffs();

  const std::array<int, 5> o{0, 1, 2, 3, 4};
  const TreeValue phase{-i_ * hA0HH(o.data()) / hA0HHX()};

  std::array<Eps5<T>, 12> amps{
      hA2gHHp1e() + hA2gHHp1o(),   hA2gHHp2e() + hA2gHHp2o(),
      hA2gHHp3e() + hA2gHHp3o(),   hA2gHHp4e() + hA2gHHp4o(),
      hA2gHHp5e() + hA2gHHp5o(),   hA2gHHp6e() + hA2gHHp6o(),
      hA2gHHp7e() + hA2gHHp7o(),   hA2gHHp8e() + hA2gHHp8o(),
      hA2gHHp9e() + hA2gHHp9o(),   hA2gHHp10e() + hA2gHHp10o(),
      hA2gHHp11e() + hA2gHHp11o(), hA2gHHp12e() + hA2gHHp12o(),
  };

  for (Eps5<T> &amp : amps) {
    amp = phase * correction2L(amp);
  }

  for (TreeValue &i : f) {
    i = std::conj(i);
  }

  std::array<Eps5<T>, 12> camps{
      hA2gHHp1e() - hA2gHHp1o(),   hA2gHHp2e() - hA2gHHp2o(),
      hA2gHHp3e() - hA2gHHp3o(),   hA2gHHp4e() - hA2gHHp4o(),
      hA2gHHp5e() - hA2gHHp5o(),   hA2gHHp6e() - hA2gHHp6o(),
      hA2gHHp7e() - hA2gHHp7o(),   hA2gHHp8e() - hA2gHHp8o(),
      hA2gHHp9e() - hA2gHHp9o(),   hA2gHHp10e() - hA2gHHp10o(),
      hA2gHHp11e() - hA2gHHp11o(), hA2gHHp12e() - hA2gHHp12o(),
  };

  const TreeValue cphase{std::conj(phase)};

  for (Eps5<T> &camp : camps) {
    camp = cphase * correction2L(camp);
  }

  std::array<LoopResult<Eps5<T>>, 12> resvec;

  for (int i{0}; i < 12; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}
