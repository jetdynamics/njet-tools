#!/usr/bin/env bash

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}2
    cd $h
    ../pp-form.sh ys.c
    ../pp-form.sh fs.c
    for i in {1..12}; do
        ../pp-form.sh e${i}_even.c
        ../pp-form.sh e${i}_odd.c
    done
    cd ..
done
