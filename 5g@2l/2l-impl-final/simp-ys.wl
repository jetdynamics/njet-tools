(* ::Package:: *)

SetDirectory["/scratch/rmoodie/njet-tools/5g@2l/2l-impl-final/HHHHH"];


ys=Get["P2_HHHHH.m"][[3]];


ysSimp=ParallelMap[Simplify, ys];


Export["ys.m",ysSimp];
