(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl-final/HHHHH"];


data=Get["P2_HHHHH.m"];


epss[o_,p_]:=Module[{one},
one=(((Plus@@(data[[1,o,p,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],If[p==1,"e","o"],o]->Coefficient[one,eps,#]&/@Range[0,-4,-1]
]
epsPe[o_]:=epss[o,1];
epsPo[o_]:=epss[o,2];


epsE=epsPe/@Range[12];


epsO=epsPo/@Range[12];


Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];


es=Transpose[{epsE,epsO}];


Export["e"<>ToString[#]<>".m",es[[#]]]&/@Range[12];
