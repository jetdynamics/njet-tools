(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl-final/exprs"];


data={
Get["P2_m+++m_lr_bcfw_1_2.m"][[1]],
Get["P2_m++m+_lr_bcfw_4_5.m"][[1]],
Get["P2_m+m++_lr_bcfw_1_2.m"][[1]],
Get["P2_++m+m_lr_bcfw_3_2.m"][[1]],
Get["P2_++mm+_lr_bcfw_4_5.m"][[1]],
Get["P2_+m++m_lr_bcfw_5_4.m"][[1]],
Get["P2_+m+m+_lr_bcfw_4_5.m"][[1]],
Get["P2_+mm++_lr_bcfw_4_5.m"][[1]],
Get["P2_mm+++_lr_bcfw_4_5.m"][[1]],
Get["P2_+++mm_lr_bcfw_4_5.m"][[1]]};


Table[Select[Variables[data[[i]]],MatchQ[#,_F]&],{i,Length[data]}];


pentas2l=DeleteDuplicates[Flatten[%]]


pentas2l//Length


Export["Fs.m",pentas2l];
