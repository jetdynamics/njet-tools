#!/usr/bin/env bash

MAIN=output/0q5g-2l-analytic.cpp

cat template_main_start.cpp > ${MAIN}

l=2
dir=exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_}
    h=${g%_lr*.m}
    d=${h}

    cd ${d}

    j=0
    for i in {0..5}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $j

    perl -p -e "s|HH|$j|g; s|SSSSS|$h|g;" ../template_main.cpp >>../${MAIN}

    cd ..
done

cat template_main_end.cpp >> ${MAIN}

perl -pi -e "s|hA007\(|hA07(|g" ${MAIN}
clang-format -i ${MAIN}
