#!/usr/bin/env bash

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}
    cd $h

    i=`cat ys.ycount`
    for n in $(seq 1 $i); do
        m=$((n - 1))
        perl -pi -e "s|y\[$n\]|y[$m]|g" fs.cpp
    done

    cd ..
done
