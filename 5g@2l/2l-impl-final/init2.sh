#!/usr/bin/env bash

mkdir output2

dir=new_exprs
for f in $dir/P2_*_lr_bcfw_*_*.m; do
    g=${f#$dir/P2_}
    h=${g%_lr_bcfw_*_*.m}
    d=${h}2
    mkdir $d
    cp $f $d/P2_$h.m
    m=proc-2l-${h}2.wl
    perl -p -e "s|HHHHH|$h|g;" proc-2l-HHHHH2.wl >$d/$m
    cd $d
    math -script $m
    cd ..
done
