    std::array<LoopResult<Eps5<T>>, 12> hA2g07();
    void hA2g07coeffs();

    LoopValue<Eps5<T>> hA2g07p1e();
    LoopValue<Eps5<T>> hA2g07p2e();
    LoopValue<Eps5<T>> hA2g07p3e();
    LoopValue<Eps5<T>> hA2g07p4e();
    LoopValue<Eps5<T>> hA2g07p5e();
    LoopValue<Eps5<T>> hA2g07p6e();
    LoopValue<Eps5<T>> hA2g07p7e();
    LoopValue<Eps5<T>> hA2g07p8e();
    LoopValue<Eps5<T>> hA2g07p9e();
    LoopValue<Eps5<T>> hA2g07p10e();
    LoopValue<Eps5<T>> hA2g07p11e();
    LoopValue<Eps5<T>> hA2g07p12e();

    LoopValue<Eps5<T>> hA2g07p1o();
    LoopValue<Eps5<T>> hA2g07p2o();
    LoopValue<Eps5<T>> hA2g07p3o();
    LoopValue<Eps5<T>> hA2g07p4o();
    LoopValue<Eps5<T>> hA2g07p5o();
    LoopValue<Eps5<T>> hA2g07p6o();
    LoopValue<Eps5<T>> hA2g07p7o();
    LoopValue<Eps5<T>> hA2g07p8o();
    LoopValue<Eps5<T>> hA2g07p9o();
    LoopValue<Eps5<T>> hA2g07p10o();
    LoopValue<Eps5<T>> hA2g07p11o();
    LoopValue<Eps5<T>> hA2g07p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g11();
    void hA2g11coeffs();

    LoopValue<Eps5<T>> hA2g11p1e();
    LoopValue<Eps5<T>> hA2g11p2e();
    LoopValue<Eps5<T>> hA2g11p3e();
    LoopValue<Eps5<T>> hA2g11p4e();
    LoopValue<Eps5<T>> hA2g11p5e();
    LoopValue<Eps5<T>> hA2g11p6e();
    LoopValue<Eps5<T>> hA2g11p7e();
    LoopValue<Eps5<T>> hA2g11p8e();
    LoopValue<Eps5<T>> hA2g11p9e();
    LoopValue<Eps5<T>> hA2g11p10e();
    LoopValue<Eps5<T>> hA2g11p11e();
    LoopValue<Eps5<T>> hA2g11p12e();

    LoopValue<Eps5<T>> hA2g11p1o();
    LoopValue<Eps5<T>> hA2g11p2o();
    LoopValue<Eps5<T>> hA2g11p3o();
    LoopValue<Eps5<T>> hA2g11p4o();
    LoopValue<Eps5<T>> hA2g11p5o();
    LoopValue<Eps5<T>> hA2g11p6o();
    LoopValue<Eps5<T>> hA2g11p7o();
    LoopValue<Eps5<T>> hA2g11p8o();
    LoopValue<Eps5<T>> hA2g11p9o();
    LoopValue<Eps5<T>> hA2g11p10o();
    LoopValue<Eps5<T>> hA2g11p11o();
    LoopValue<Eps5<T>> hA2g11p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g13();
    void hA2g13coeffs();

    LoopValue<Eps5<T>> hA2g13p1e();
    LoopValue<Eps5<T>> hA2g13p2e();
    LoopValue<Eps5<T>> hA2g13p3e();
    LoopValue<Eps5<T>> hA2g13p4e();
    LoopValue<Eps5<T>> hA2g13p5e();
    LoopValue<Eps5<T>> hA2g13p6e();
    LoopValue<Eps5<T>> hA2g13p7e();
    LoopValue<Eps5<T>> hA2g13p8e();
    LoopValue<Eps5<T>> hA2g13p9e();
    LoopValue<Eps5<T>> hA2g13p10e();
    LoopValue<Eps5<T>> hA2g13p11e();
    LoopValue<Eps5<T>> hA2g13p12e();

    LoopValue<Eps5<T>> hA2g13p1o();
    LoopValue<Eps5<T>> hA2g13p2o();
    LoopValue<Eps5<T>> hA2g13p3o();
    LoopValue<Eps5<T>> hA2g13p4o();
    LoopValue<Eps5<T>> hA2g13p5o();
    LoopValue<Eps5<T>> hA2g13p6o();
    LoopValue<Eps5<T>> hA2g13p7o();
    LoopValue<Eps5<T>> hA2g13p8o();
    LoopValue<Eps5<T>> hA2g13p9o();
    LoopValue<Eps5<T>> hA2g13p10o();
    LoopValue<Eps5<T>> hA2g13p11o();
    LoopValue<Eps5<T>> hA2g13p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g14();
    void hA2g14coeffs();

    LoopValue<Eps5<T>> hA2g14p1e();
    LoopValue<Eps5<T>> hA2g14p2e();
    LoopValue<Eps5<T>> hA2g14p3e();
    LoopValue<Eps5<T>> hA2g14p4e();
    LoopValue<Eps5<T>> hA2g14p5e();
    LoopValue<Eps5<T>> hA2g14p6e();
    LoopValue<Eps5<T>> hA2g14p7e();
    LoopValue<Eps5<T>> hA2g14p8e();
    LoopValue<Eps5<T>> hA2g14p9e();
    LoopValue<Eps5<T>> hA2g14p10e();
    LoopValue<Eps5<T>> hA2g14p11e();
    LoopValue<Eps5<T>> hA2g14p12e();

    LoopValue<Eps5<T>> hA2g14p1o();
    LoopValue<Eps5<T>> hA2g14p2o();
    LoopValue<Eps5<T>> hA2g14p3o();
    LoopValue<Eps5<T>> hA2g14p4o();
    LoopValue<Eps5<T>> hA2g14p5o();
    LoopValue<Eps5<T>> hA2g14p6o();
    LoopValue<Eps5<T>> hA2g14p7o();
    LoopValue<Eps5<T>> hA2g14p8o();
    LoopValue<Eps5<T>> hA2g14p9o();
    LoopValue<Eps5<T>> hA2g14p10o();
    LoopValue<Eps5<T>> hA2g14p11o();
    LoopValue<Eps5<T>> hA2g14p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g19();
    void hA2g19coeffs();

    LoopValue<Eps5<T>> hA2g19p1e();
    LoopValue<Eps5<T>> hA2g19p2e();
    LoopValue<Eps5<T>> hA2g19p3e();
    LoopValue<Eps5<T>> hA2g19p4e();
    LoopValue<Eps5<T>> hA2g19p5e();
    LoopValue<Eps5<T>> hA2g19p6e();
    LoopValue<Eps5<T>> hA2g19p7e();
    LoopValue<Eps5<T>> hA2g19p8e();
    LoopValue<Eps5<T>> hA2g19p9e();
    LoopValue<Eps5<T>> hA2g19p10e();
    LoopValue<Eps5<T>> hA2g19p11e();
    LoopValue<Eps5<T>> hA2g19p12e();

    LoopValue<Eps5<T>> hA2g19p1o();
    LoopValue<Eps5<T>> hA2g19p2o();
    LoopValue<Eps5<T>> hA2g19p3o();
    LoopValue<Eps5<T>> hA2g19p4o();
    LoopValue<Eps5<T>> hA2g19p5o();
    LoopValue<Eps5<T>> hA2g19p6o();
    LoopValue<Eps5<T>> hA2g19p7o();
    LoopValue<Eps5<T>> hA2g19p8o();
    LoopValue<Eps5<T>> hA2g19p9o();
    LoopValue<Eps5<T>> hA2g19p10o();
    LoopValue<Eps5<T>> hA2g19p11o();
    LoopValue<Eps5<T>> hA2g19p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g21();
    void hA2g21coeffs();

    LoopValue<Eps5<T>> hA2g21p1e();
    LoopValue<Eps5<T>> hA2g21p2e();
    LoopValue<Eps5<T>> hA2g21p3e();
    LoopValue<Eps5<T>> hA2g21p4e();
    LoopValue<Eps5<T>> hA2g21p5e();
    LoopValue<Eps5<T>> hA2g21p6e();
    LoopValue<Eps5<T>> hA2g21p7e();
    LoopValue<Eps5<T>> hA2g21p8e();
    LoopValue<Eps5<T>> hA2g21p9e();
    LoopValue<Eps5<T>> hA2g21p10e();
    LoopValue<Eps5<T>> hA2g21p11e();
    LoopValue<Eps5<T>> hA2g21p12e();

    LoopValue<Eps5<T>> hA2g21p1o();
    LoopValue<Eps5<T>> hA2g21p2o();
    LoopValue<Eps5<T>> hA2g21p3o();
    LoopValue<Eps5<T>> hA2g21p4o();
    LoopValue<Eps5<T>> hA2g21p5o();
    LoopValue<Eps5<T>> hA2g21p6o();
    LoopValue<Eps5<T>> hA2g21p7o();
    LoopValue<Eps5<T>> hA2g21p8o();
    LoopValue<Eps5<T>> hA2g21p9o();
    LoopValue<Eps5<T>> hA2g21p10o();
    LoopValue<Eps5<T>> hA2g21p11o();
    LoopValue<Eps5<T>> hA2g21p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g22();
    void hA2g22coeffs();

    LoopValue<Eps5<T>> hA2g22p1e();
    LoopValue<Eps5<T>> hA2g22p2e();
    LoopValue<Eps5<T>> hA2g22p3e();
    LoopValue<Eps5<T>> hA2g22p4e();
    LoopValue<Eps5<T>> hA2g22p5e();
    LoopValue<Eps5<T>> hA2g22p6e();
    LoopValue<Eps5<T>> hA2g22p7e();
    LoopValue<Eps5<T>> hA2g22p8e();
    LoopValue<Eps5<T>> hA2g22p9e();
    LoopValue<Eps5<T>> hA2g22p10e();
    LoopValue<Eps5<T>> hA2g22p11e();
    LoopValue<Eps5<T>> hA2g22p12e();

    LoopValue<Eps5<T>> hA2g22p1o();
    LoopValue<Eps5<T>> hA2g22p2o();
    LoopValue<Eps5<T>> hA2g22p3o();
    LoopValue<Eps5<T>> hA2g22p4o();
    LoopValue<Eps5<T>> hA2g22p5o();
    LoopValue<Eps5<T>> hA2g22p6o();
    LoopValue<Eps5<T>> hA2g22p7o();
    LoopValue<Eps5<T>> hA2g22p8o();
    LoopValue<Eps5<T>> hA2g22p9o();
    LoopValue<Eps5<T>> hA2g22p10o();
    LoopValue<Eps5<T>> hA2g22p11o();
    LoopValue<Eps5<T>> hA2g22p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g25();
    void hA2g25coeffs();

    LoopValue<Eps5<T>> hA2g25p1e();
    LoopValue<Eps5<T>> hA2g25p2e();
    LoopValue<Eps5<T>> hA2g25p3e();
    LoopValue<Eps5<T>> hA2g25p4e();
    LoopValue<Eps5<T>> hA2g25p5e();
    LoopValue<Eps5<T>> hA2g25p6e();
    LoopValue<Eps5<T>> hA2g25p7e();
    LoopValue<Eps5<T>> hA2g25p8e();
    LoopValue<Eps5<T>> hA2g25p9e();
    LoopValue<Eps5<T>> hA2g25p10e();
    LoopValue<Eps5<T>> hA2g25p11e();
    LoopValue<Eps5<T>> hA2g25p12e();

    LoopValue<Eps5<T>> hA2g25p1o();
    LoopValue<Eps5<T>> hA2g25p2o();
    LoopValue<Eps5<T>> hA2g25p3o();
    LoopValue<Eps5<T>> hA2g25p4o();
    LoopValue<Eps5<T>> hA2g25p5o();
    LoopValue<Eps5<T>> hA2g25p6o();
    LoopValue<Eps5<T>> hA2g25p7o();
    LoopValue<Eps5<T>> hA2g25p8o();
    LoopValue<Eps5<T>> hA2g25p9o();
    LoopValue<Eps5<T>> hA2g25p10o();
    LoopValue<Eps5<T>> hA2g25p11o();
    LoopValue<Eps5<T>> hA2g25p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g26();
    void hA2g26coeffs();

    LoopValue<Eps5<T>> hA2g26p1e();
    LoopValue<Eps5<T>> hA2g26p2e();
    LoopValue<Eps5<T>> hA2g26p3e();
    LoopValue<Eps5<T>> hA2g26p4e();
    LoopValue<Eps5<T>> hA2g26p5e();
    LoopValue<Eps5<T>> hA2g26p6e();
    LoopValue<Eps5<T>> hA2g26p7e();
    LoopValue<Eps5<T>> hA2g26p8e();
    LoopValue<Eps5<T>> hA2g26p9e();
    LoopValue<Eps5<T>> hA2g26p10e();
    LoopValue<Eps5<T>> hA2g26p11e();
    LoopValue<Eps5<T>> hA2g26p12e();

    LoopValue<Eps5<T>> hA2g26p1o();
    LoopValue<Eps5<T>> hA2g26p2o();
    LoopValue<Eps5<T>> hA2g26p3o();
    LoopValue<Eps5<T>> hA2g26p4o();
    LoopValue<Eps5<T>> hA2g26p5o();
    LoopValue<Eps5<T>> hA2g26p6o();
    LoopValue<Eps5<T>> hA2g26p7o();
    LoopValue<Eps5<T>> hA2g26p8o();
    LoopValue<Eps5<T>> hA2g26p9o();
    LoopValue<Eps5<T>> hA2g26p10o();
    LoopValue<Eps5<T>> hA2g26p11o();
    LoopValue<Eps5<T>> hA2g26p12o();

    std::array<LoopResult<Eps5<T>>, 12> hA2g28();
    void hA2g28coeffs();

    LoopValue<Eps5<T>> hA2g28p1e();
    LoopValue<Eps5<T>> hA2g28p2e();
    LoopValue<Eps5<T>> hA2g28p3e();
    LoopValue<Eps5<T>> hA2g28p4e();
    LoopValue<Eps5<T>> hA2g28p5e();
    LoopValue<Eps5<T>> hA2g28p6e();
    LoopValue<Eps5<T>> hA2g28p7e();
    LoopValue<Eps5<T>> hA2g28p8e();
    LoopValue<Eps5<T>> hA2g28p9e();
    LoopValue<Eps5<T>> hA2g28p10e();
    LoopValue<Eps5<T>> hA2g28p11e();
    LoopValue<Eps5<T>> hA2g28p12e();

    LoopValue<Eps5<T>> hA2g28p1o();
    LoopValue<Eps5<T>> hA2g28p2o();
    LoopValue<Eps5<T>> hA2g28p3o();
    LoopValue<Eps5<T>> hA2g28p4o();
    LoopValue<Eps5<T>> hA2g28p5o();
    LoopValue<Eps5<T>> hA2g28p6o();
    LoopValue<Eps5<T>> hA2g28p7o();
    LoopValue<Eps5<T>> hA2g28p8o();
    LoopValue<Eps5<T>> hA2g28p9o();
    LoopValue<Eps5<T>> hA2g28p10o();
    LoopValue<Eps5<T>> hA2g28p11o();
    LoopValue<Eps5<T>> hA2g28p12o();

