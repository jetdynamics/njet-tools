
#ifdef USE_SD
template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a<Vc::double_v>;
#endif
