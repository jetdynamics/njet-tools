#include <array>
#include <chrono>
#include <complex>
#include <iostream>

#include "analytic/0q5g-analytic.h"
#include "finrem/0q5g/0q5g-2l.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T, typename P>
void single(const int rseed)
{
    const int Nc { 3 };
    const T mur2 { 1.6530612244897955 };
    const T scale { 1. };
    Amp0q5g_a2l<T, P> amp(scale);
    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    const std::vector<MOM<T>> moms { njet_random_point<T>(rseed) };
    amp.setMomenta(moms);
    amp.setSpecFuncVals();
    //amp.printSpecFuncs();
    amp.setSpecFuncMons();
    amp.initFinRem();
    T born { amp.c0lx0l_fin() };
    T virt { amp.c1lx0l_fin() };
    T virtsq { amp.c1lx1l_fin() };
    T dblvirt { amp.c2lx0l_fin() };

    std::cout
        << "Helicity sum:" << '\n'
        << "born:    " << born << '\n'
        << "virt:    " << virt << '\n'
        << "virtsq:  " << virtsq << '\n'
        << "dblvirt: " << dblvirt << '\n'
        << '\n';
}

template <typename T, typename P>
void single_hel()
{
    const T scale { 1. };
    const double mur2 { 1. };
    const int Nc { 3 };
    const int Nf { 0 };

    Amp0q5g_a2l<T, P> amp(scale);
    amp.setNf(Nf);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    //Amp0q5g_a<T> oldamp(scale);
    //oldamp.setNf(Nf);
    //oldamp.setNc(Nc);
    //oldamp.setMuR2(mur2);


    std::vector<MOM<T>> moms {
        {
            MOM<T>(
                T(-1.00000000000000000000000000000000000000000000000000000000000),
                T(0.00000000000000000000000000000000000000000000000000000000000),
                T(0.00000000000000000000000000000000000000000000000000000000000),
                T(-1.00000000000000000000000000000000000000000000000000000000000)),
            MOM<T>(
                T(-1.00000000000000000000000000000000000000000000000000000000000),
                T(0.00000000000000000000000000000000000000000000000000000000000),
                T(0.00000000000000000000000000000000000000000000000000000000000),
                T(1.00000000000000000000000000000000000000000000000000000000000)),
            MOM<T>(
                T(0.79801432988009493509299193946693117648845572656403722773605),
                T(-0.37321209227219715336961491716384960479976544988797363796317),
                T(0.00000000000000000000000000000000000000000000000000000000000),
                T(0.70536487357663762372101581871455453779506327500969540878180)),
            MOM<T>(
                T(0.40196418903039649460001703524901669912363945919763672614771),
                T(-0.106925082985625395349262270507191683021523814706628453),
                T(-0.348765406536464789995026085568356619560893178427670075),
                T(-0.168834022326176578389258991881108237185364783105412933)),
            MOM<T>(
                T(0.8000214810895085703069910252840521243879048142383260461162),
                T(0.480137175257822548718877187671041287821289264594602091),
                T(0.348765406536464789995026085568356619560893178427670075),
                T(-0.536530851250461045331756826833446300609698491904282475)),
        },
    };

    //oldamp.setMomenta(moms);

    amp.setMomenta(moms);
    amp.setSpecFuncs();
    amp.initFinRem();

    std::array<Helicity<N>, 1> hels { {
        //{ 1, 1, 1, 1, 1 },
        //{ 1, 1, 1, 1, -1 },
        //{ 1, 1, 1, -1, 1 },
        //{ 1, 1, -1, 1, 1 },
        //{ 1, -1, 1, 1, 1 },
        //{ -1, 1, 1, 1, 1 },
        { -1, -1, 1, 1, 1 },
    } };

    for (Helicity<N>& hel : hels) {

        //T ob { oldamp.born(hel.data()) };
        //T ov2 { oldamp.virtsq(hel.data()).get0().real() };

        //T born { amp.c0lx0l_fin(hel.data()) };
        //T virtsq { amp.c1lx1l_fin(hel.data()) };
        T dblvirt { amp.c2lx0l_fin(hel.data()) };

        std::cout
            << hel << '\n'
            //<< "old born:     " << ob << '\n'
            //<< "new born:     " << born << '\n'
            //<< "born ratio:   " << born/ob << '\n'
            //<< "old virtsq:   " << ov2 << '\n'
            //<< "new virtsq:   " << virtsq << '\n'
            //<< "virtsq ratio: " << virtsq/ov2 << '\n'
            << "dblvirt:   " << dblvirt << '\n'
            << '\n';
    }
}

template <typename T, typename P>
void loop(const int num_ps = 1)
{
    long sfv_time_ms { 0 };
    long sfm_time_us { 0 };
    long virt_time_us { 0 };
    long virtsq_time_us { 0 };
    long dblvirt_time_ms { 0 };

    const int Nc { 3 };
    const T mur2 { 1.6530612244897955 };
    const T scale { 1. };

    std::cout
        << "Nc=" << Nc << '\n'
        << "MuR2=" << mur2 << '\n'
        << '\n'
        << "initialising sparse matrices..." << '\n'
        << '\n';

    auto t4 { std::chrono::high_resolution_clock::now() };
    Amp0q5g_a2l<T, P> amp(scale);
    auto t5 { std::chrono::high_resolution_clock::now() };
    const long sm_time_ms { std::chrono::duration_cast<std::chrono::milliseconds>(t5 - t4).count() };

    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    std::cout << "Start phase space points" << '\n';
    for (int i { 1 }; i <= num_ps; ++i) {

        const std::vector<MOM<T>> moms { njet_random_point<T>(i) };

        std::cout
            << "Point " << i << '\n'
            << "Invariants:"
            << '\n'
            << '(';
        for (int i { 0 }; i < N - 1; ++i) {
            std::cout
                << invariant(moms, i, i + 1)
                << ", ";
        }
        std::cout
            << invariant(moms, N - 1, 0)
            << ')'
            << '\n'
            << '\n';

        amp.setMomenta(moms.data());

        auto t0 { std::chrono::high_resolution_clock::now() };
        amp.setSpecFuncVals();
        auto t1 { std::chrono::high_resolution_clock::now() };
        sfv_time_ms += std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

        auto t00 { std::chrono::high_resolution_clock::now() };
        amp.setSpecFuncMons();
        auto t01 { std::chrono::high_resolution_clock::now() };
        sfm_time_us += std::chrono::duration_cast<std::chrono::microseconds>(t01 - t00).count();

        T born { amp.born_fin() };
        auto t8 { std::chrono::high_resolution_clock::now() };
        T virt { amp.virt_fin() };
        auto t9 { std::chrono::high_resolution_clock::now() };
        T virtsq { amp.virtsq_fin() };
        auto t10 { std::chrono::high_resolution_clock::now() };
        T dblvirt { amp.dblvirt_fin() };
        auto t11 { std::chrono::high_resolution_clock::now() };

        std::cout
            << "Helicity sum:" << '\n'
            << "born:    " << born << '\n'
            << "virt:    " << virt << '\n'
            << "virtsq:  " << virtsq << '\n'
            << "dblvirt: " << dblvirt << '\n'
            << '\n';

        const long virt_time_us_i { std::chrono::duration_cast<std::chrono::microseconds>(t9 - t8).count() };
        const long virtsq_time_us_i { std::chrono::duration_cast<std::chrono::microseconds>(t10 - t9).count() };
        const long dblvirt_time_ms_i { std::chrono::duration_cast<std::chrono::milliseconds>(t11 - t10).count() };

        virt_time_us += virt_time_us_i;
        virtsq_time_us += virtsq_time_us_i;
        dblvirt_time_ms += dblvirt_time_ms_i;
    }

    std::cout
        << "Init time (mainly filling sparse matrices):  " << sm_time_ms << "ms" << '\n'
        << "SFeval time:  " << sfv_time_ms / num_ps << "ms" << '\n'
        << "SFmons time:  " << sfm_time_us / num_ps << "us" << '\n'
        << "virt time:    " << virt_time_us / num_ps << "us" << '\n'
        << "virtsq time:  " << virtsq_time_us / num_ps << "us" << '\n'
        << "dblvirt time: " << dblvirt_time_ms / num_ps << "ms" << '\n'
        << '\n'
        << '\n';
}

template <typename T>
void loop_hels(const int num_ps = 1)
{
    const int Nc { 3 };
    const T mur2 { 1.6530612244897955 };
    const T scale { 1. };

    std::cout
        << "Nc=" << Nc << '\n'
        << "MuR2=" << mur2 << '\n'
        << '\n';

    Amp0q5g_a2l<T, T> amp(scale);

    amp.setNf(0);
    amp.setNc(Nc);
    amp.setMuR2(mur2);

    for (int i { 0 }; i < num_ps; ++i) {

        const std::array<MOM<T>, N> moms { random_point<T>() };

        std::cout
            << "Invariants:"
            << '\n'
            << '(';
        for (int i { 0 }; i < N - 1; ++i) {
            std::cout
                << invariant(moms, i, i + 1)
                << ", ";
        }
        std::cout
            << invariant(moms, N - 1, 0)
            << ')'
            << '\n'
            << '\n';

        amp.setMomenta(moms.data());
        amp.setSpecFuncVals();
        amp.setSpecFuncMons();

        std::array<Helicity<N>, 32> HSarr { {
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, -1 },
            { 1, 1, 1, -1, 1 },
            { 1, 1, 1, -1, -1 },
            { 1, 1, -1, 1, 1 },
            { 1, 1, -1, 1, -1 },
            { 1, 1, -1, -1, 1 },
            { 1, 1, -1, -1, -1 },
            { -1, 1, 1, 1, 1 },
            { -1, 1, 1, 1, -1 },
            { -1, 1, 1, -1, 1 },
            { -1, 1, 1, -1, -1 },
            { -1, 1, -1, 1, 1 },
            { -1, 1, -1, 1, -1 },
            { -1, 1, -1, -1, 1 },
            { -1, 1, -1, -1, -1 },

            { -1, -1, -1, -1, -1 },
            { -1, -1, -1, -1, 1 },
            { -1, -1, -1, 1, -1 },
            { -1, -1, -1, 1, 1 },
            { -1, -1, 1, -1, -1 },
            { -1, -1, 1, -1, 1 },
            { -1, -1, 1, 1, -1 },
            { -1, -1, 1, 1, 1 },
            { 1, -1, -1, -1, -1 },
            { 1, -1, -1, -1, 1 },
            { 1, -1, -1, 1, -1 },
            { 1, -1, -1, 1, 1 },
            { 1, -1, 1, -1, -1 },
            { 1, -1, 1, -1, 1 },
            { 1, -1, 1, 1, -1 },
            { 1, -1, 1, 1, 1 },
        } };

        std::array<int, 32> HSskip { { 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 } };

        //        for (int i0 { -1 }; i0 < 2; i0 += 2) {
        //            for (int i1 { -1 }; i1 < 2; i1 += 2) {
        //                for (int i2 { -1 }; i2 < 2; i2 += 2) {
        //                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
        //                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
        //const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
        for (int i { 0 }; i < 32; ++i) {
            Helicity<N> hels { HSarr[i] };
            //if (hels.order() == 1) {
            if (HSskip[i] == 0) {
                std::cout << hels << '\n';
                amp.setHelicity(hels.data());

                std::array<LoopResult<std::complex<T>>, 12> p0s { amp.A0p() };
                const std::complex<T> p0 { p0s[0].loop };

                std::array<LoopResult<std::complex<T>>, 12> p1s { amp.ALp() };
                const std::complex<T> p1 { p1s[0].loop };

                //std::array<LoopResult<std::complex<T>>, 12> p2s { amp.A2Lp() };
                //const std::complex<T> p2 { p2s[0].loop };

                //				for (int i{0}; i<12;++i) {
                //std::cout
                //<< "A0'[" << i << "]=" << p0s[i].loop << '\n'
                //<< "A0 [" << i << "]=" << p0s2[i] << '\n'
                //;
                //}
                //for (int i{0}; i<12;++i) std::cout << "A0[" << i << "]=" << p0s[i].loop << '\n';
                //for (int i{0}; i<12;++i) std::cout << "A1[" << i << "]=" << p1s[i].loop << '\n';
                //for (int i{0}; i<12;++i) std::cout << "A1[" << i << "]/A0[" << i << "]=" << p1s[i].loop/p0s[i].loop << '\n';
                //std::complex<T> ans;
                //				for (int i{0}; i<12;++i) {
                //ans += p1s[i].loop*std::conj(p1s[i].loop);
                //std::cout << "A1[" << i << "]=" << p1s[i].loop << '\n';
                //std::cout << "A1c[" << i << "]=" << std::conj(p1s[i].loop) << '\n';
                //}
                //std::cout << "ans="<<ans << '\n';

                T born { amp.born_fin(hels.data()) };
                T virt { amp.virt_fin(hels.data()) };
                T virtsq { amp.virtsq_fin(hels.data()) };
                T dblvirt { amp.dblvirt_fin(hels.data()) };

                std::cout
                    //<< i << ' ' << virt << '\n'
                    //<< "A1(12345)/A0: " << p1/p0 << '\n'
                    //<< "A2(12345)/A0: " << p2/p0 << '\n'
                    << "born:         " << born << '\n'
                    << "virt:         " << virt << '\n'
                    << "virt/born:    " << virt / born << '\n'
                    << "virtsq:       " << virtsq << '\n'
                    << "virtsq/born:  " << virtsq / born << '\n'
                    << "dblvirt:      " << dblvirt << '\n'
                    << "dblvirt/born: " << dblvirt / born << '\n';
            }
        }
        //                    }
        //                }
        //            }
        //        }
        std::cout
            << '\n';
    }
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    // loop<double, double>(100);
    // loop<dd_real, dd_real>(1);
    //single<double, double>(61437);
    // single<dd_real, dd_real>(61437);
    single_hel<double, double>();
}
