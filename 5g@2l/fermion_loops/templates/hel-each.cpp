{%- from 'macros.jinja2' import comma, matrix, lst -%}
/*  
 * finrem/{{ amp }}/{{ channel }}/{{ h_char }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ h_char }}.h"

template <typename T>
Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::Amp{{ amp }}_{{ channel }}_{{ h_char }}(const std::vector<std::complex<T>> &sf_)
    : Base(sf_,
             {%- for loop_obj in ld if loop_obj.order %}
             {{ loop_obj.fl }}, 
             {% for sme in loop_obj.smes -%}{{ sme.nrows }}, {{ sme.ncols }}, {% endfor %}
             {% for sfiv in loop_obj.sfis -%}
             { {%- for i in sfiv %}{{ i }}{{- comma(loop) }}{% endfor -%} },
             {% endfor %}
             {%- for fiv in loop_obj.fis -%}
             { {%- for i in fiv %}{{ i }}{{- comma(loop) }}{% endfor -%} }{{- comma(loop) }}
             {% endfor -%}
             {{- comma(loop) }}
             {%- endfor -%}) {
    {% for loop_obj in ld if loop_obj.order %}
    {% if loop_obj.smos %}
    {% for sme in loop_obj.smes %}
    {% if sme.split %}
    fill_m{{ loop_obj.id }}{{ loop.index0 }}e_0();
    fill_m{{ loop_obj.id }}{{ loop.index0 }}e_1();
    {% else %}
    fill_m{{ loop_obj.id }}{{ loop.index0 }}e();
    {% endif %}
    {%- endfor %}
    {% for smo in loop_obj.smos %}
    {{ matrix(loop_obj.id, loop.index0, smo, "o") }}
    {%- endfor %}
    {% else %}
    {% for sme in loop_obj.smes %}
    {{ matrix(loop_obj.id, loop.index0, sme) }}
    {%- endfor %}
    {% endif %}
    {% endfor %}
    }

{% for loop_obj in ld if (loop_obj.order and not loop_obj.parity_odd) %}
template <typename T>
void Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::hA{{ loop_obj.id }}_coeffs(const std::complex<T> x1, const std::complex<T> x2, const std::complex<T> x3, const std::complex<T> x4, const std::complex<T> x5) {
    {{ lst(loop_obj.yz, "u") }}
    {{ lst(loop_obj.yl, "y") }}
    {{ loop_obj.yms }}
    {{ lst(loop_obj.fz, "v") }}
    {{ loop_obj.fms }}

}
{% endfor %}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<double>;
#endif
#ifdef USE_DD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<Vc::double_v>;
#endif
