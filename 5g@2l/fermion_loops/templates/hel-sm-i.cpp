{%- from 'macros.jinja2' import comma -%}
/*  
 * finrem/{{ amp }}/{{ channel }}/{{ h_char }}-{{ loop_id }}{{ partial }}-sm-{{ index }}.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include "{{ h_char }}.h"

template <typename T>
void Amp{{ amp }}_{{ channel }}_{{ h_char }}<T>::fill_m{{ loop_id }}{{ partial }}e_{{ index }}() {
    {%- set mat = "m" ~ loop_id ~ partial ~ post %}
    {% if index == 0 %}
    {{ mat }}.reserve(std::array<int, {{ sme.ncols }}>({
            {%- for i in sme.nnz %}{{ i }}{{- comma(loop) }}{% endfor -%}
            }));
    {% endif %}
    {% for el in sme.hentries %}
    {{ mat }}.insert({{ el.i }}, {{ el.j }}) = T({{ el.n }}.){% if el.d != 1 %}/T({{ el.d }}.){% endif %};
    {%- endfor %}
    {% if index == 1 %}
    {{ mat }}.makeCompressed();
    {% endif %}
}

#ifdef USE_SD
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<double>;
#endif                                   
#ifdef USE_DD                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<dd_real>;
#endif                                   
#ifdef USE_QD                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<qd_real>;
#endif                                   
#ifdef USE_VC                            
template class Amp{{ amp }}_{{ channel }}_{{ h_char }}<Vc::double_v>;
#endif
