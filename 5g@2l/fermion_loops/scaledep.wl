(* ::Package:: *)

Clear[I1,I2,A1,A2,F1,F2,SS]


(* ::Subsection:: *)
(*Scale dependence*)


I1[x_,mu_] := n[x]*Sum[SS[x,i,mu],{i,1,5}]*(1/(x*eps)^2+gg[1]/eps/x)


I2[mu_] := -1/2*I1[1,mu]*(I1[1,mu]+gg[2]/eps) + n[1]/n[2]*I1[2,mu]*(gg[3]/eps+gg[4]) + HH/eps


epsexp = {
  SS[x_,i_,mu_]:>Normal@Series[Exp[x*eps*(Log[mu^2]-Log[-s[i,Mod[i+1,5,1]]])],{eps,0,4}],
  n[i_]:>Normal@Series[Exp[-i*eps*EulerGamma]/Gamma[1+i*eps],{eps,0,4}] /. PolyGamma[2,1]->-2*Zeta[3]
};


(* bare coupling: a
   renormalised coupling: aR *)
a -> Zuv[mu] aR[mu] mu^(2 eps)
Zuvrule = Zuv[mu] -> 1 + aR[mu] Zuv1/eps + aR[mu]^2 (Zuv2a/eps^2 + Zuv2b/eps);


fullamplitude = a^(3/2) (A[0] + a Abare[1] + a^2 Abare[2])


(* we expand in aR[mu]*mu^(2 eps):=aR for now *)


tmp = fullamplitude /. a -> Zuv[mu] aR[mu] mu^(2 eps) /. Zuvrule /. aR[mu]->aR/mu^(2 eps);


fullamplituderen = Collect[Normal@Series[tmp/aR^(3/2), {aR,0,2}], aR, Collect[#,{_A,_Abare},Simplify]&]


A1ren[mu_] := mu^(2 eps) ((3 mu^(-2 eps) Zuv1 A[0])/(2 eps)+Abare[1]);
A2ren[mu_] := mu^(4 eps) ((3 mu^(-4 eps) (Zuv1^2+4 (Zuv2a+eps Zuv2b)) A[0])/(8 eps^2)+(5 mu^(-2 eps) Zuv1 Abare[1])/(2 eps)+Abare[2]);


Abare2epsexp = {
Abare[1]->Abare[1,-2]/eps^2+Abare[1,-1]/eps^1+Abare[1,0]+Abare[1,1]eps+Abare[1,2] eps^2,
Abare[2]->Abare[2,-4]/eps^4+Abare[2,-3]/eps^3+Abare[2,-2]/eps^2+Abare[2,-1]/eps+Abare[2,0]};


F1[mu_] := A1ren[mu] - I1[1,mu] A[0];
F2[mu_] := A2ren[mu] - I1[1,mu] A1ren[mu] - I2[mu] A[0];


A1rules = Flatten@Solve[CoefficientList[Expand[eps^2 Normal[Series[F1[1]/.Abare2epsexp/.epsexp,{eps,0,-1}]]],eps]==0,{Abare[1,-2],Abare[1,-1]}];


A2rules = Flatten@Solve[CoefficientList[eps^4*Normal[Series[F2[1]/.Abare2epsexp/.epsexp/.A1rules,{eps,0,-1}]],eps]==0,
  {Abare[2,-4],Abare[2,-3],Abare[2,-2],Abare[2,-1]}] /. Log[mu^2]->2 Log[mu];


Normal@Series[F1[mu]/.Abare2epsexp/.epsexp/.A1rules,{eps,0,-1}]/.Log[mu^2]->2 Log[mu]


(gam1 - gam1cusp*gam0/4+Pi^2/16*b0*gam0cusp*CA)/4;
% /. gam1->CA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+CA*nf/2(256/27-2*Pi^2/9)+CF*2*nf;
% /. gam0->-b0 /. b0->11/3*CA-nf*2/3;
% /. gam0cusp->4 /. gam1cusp->(268/9-4*Pi^2/3)*CA-80/9*nf/2;
Collect[% /. CA->Nc /. CF->Nc/2,{nf,Nc},Expand]


Collect[((25 Nf^2)/108 - 5/864 Nf (178 + 3 \[Pi]^2) + 
 5/576 (60 + 11 \[Pi]^2 + 72 Zeta[3]))/5*4,Nf,Expand]


fixconstants = {
gg[1]->\[Beta]0/2, \[Beta]0->11/3-2/3*nf, gg[2]->-2*\[Beta]0, gg[3]->gg[2]/2, Zuv1->\[Beta]0, Zuv2a->\[Beta]0^2,
Zuv2b->\[Beta]1/2, \[Beta]1->34/3-13/3*nf,
HH->-5*(5/12 + 11/144*Pi^2 + 1/2*Zeta[3] + nf*(-89/108-Pi^2/72)+nf^2*5/27)
}


Normal@Series[F1[1]/.Abare2epsexp/.epsexp/.A1rules,{eps,0,0}];
toF1 = Solve[F[1][1]==%,Abare[1,0]][[1]];


Normal@Series[F1[mu]-F1[1]/.Abare2epsexp/.A1rules/.epsexp,{eps,0,0}]/.Log[mu]->1/2 Log[mu^2]//.fixconstants//Collect[#,nf,Simplify]&


Coefficient[Normal[Series[F2[mu]/.Abare2epsexp/.A1rules/.A2rules/.epsexp,{eps,0,-1}]]/.Log[mu]->1/2 Log[mu^2],eps,-2]//.fixconstants//Simplify


Coefficient[Normal[Series[F2[mu]/.Abare2epsexp/.A1rules/.A2rules/.epsexp,{eps,0,-1}]]/.Log[mu]->1/2 Log[mu^2],eps,-1]//.fixconstants//Simplify


Coefficient[Normal[Series[F2[mu]-F2[1]/.Abare2epsexp/.A1rules/.A2rules/.epsexp,{eps,0,0}]]/.Log[mu]->1/2 Log[mu^2],eps,0]//.fixconstants//Simplify;
Collect[%//.fixconstants,{Log[mu],Abare[1,0]}];
Collect[%% /. toF1 /. F[1][1]->F[1][1]+nf*Fnf[1][1]//.fixconstants,{nf,A[0],Log[mu^2]},Expand]


15/8 \[Beta]0^2 A[0] Log[mu^2]^2 + 1/2 Log[mu^2] (1/
     108 A[0] (400 Nf^2 + 165 \[Pi]^2 - 10 Nf (178 + 3 \[Pi]^2) + 
       36 (25 + 9 \[Beta]1 + 30 Zeta[3])) + 5 \[Beta]0 F[1][1])/. F[1][1]->F[1][1]+nf*Fnf[1][1] /. {\[Beta]0 -> 11/3 - (2 Nf)/3, \[Beta]1 -> 34/3 - (13 Nf)/3} /. Nf->nf;
Collect[-%,{nf,A[0],Log[mu^2]},Expand]
