(* ::Package:: *)

SetDirectory["/scratch/rmoodie/njet-tools/5g@2l/fermion_loops/${channel}"];
in="src/";
outpre="build/";
mhvs={
${mhvs}
};
uhvs={
${uhvs}
};
allHels=Join[mhvs,uhvs];
PF=12;


sm[src_,ord_,par_]:=Module[
    {a=src[[3,ord,par]],entries,ncols,nrows,nnz},
    entries=a[[2]]/.Rule[m[i_,j_],b_]:>{"i"->j-1, "j"->i-1, "n"->Numerator[b], "d"->Denominator[b] };
    nrows=a[[1,2]];
    ncols=a[[1,1]];
    nnz=Table[Length[Select[a[[2]],#[[1,1]]==i&]],{i,a[[1,1]]}];
    {"nrows"->nrows, "ncols"->ncols,"nnz"->nnz,"entries"->entries}
];
fv[fin_]:=#[[1]]-1&/@#&/@fin[[1]];
fms[fin_]:=Sort[fin[[5]]]/.{(fn:(f|y))[i_]:>fn[i-1]};
yms[fin_]:=Sort[fin[[6]]]/.{y[i_]:>y[i-1]};
Fv[fin_]:=#-1&/@fin[[2]];
export[fins_,hels_,out_,odd_] := Module[{o},
    Print["Starting " <> out];
    o=outpre<>out<>"/";
    ParallelTable[
        Module[{fin,hel,Fvi,fvi,fmsi,fmsl,ymsi,ymsl},
            hel="h"<>hels[[i]];
            Print["Starting " <> out <> " " <> hel];
            fin=fins[[i]];
            Fvi=Fv[fin];
            fvi=fv[fin];
            fmsi=fms[fin];
            fmsl=Length[fmsi];
            ymsi=yms[fin];
            ymsl=Length[ymsi];
            Export[o<>hel<>"/Fv.json",Fvi];
            Export[o<>hel<>"/fv.json",fvi];
            Export[o<>hel<>"/fm.m",fmsi];
            Export[o<>hel<>"/f_size.txt", fmsl];
            Export[o<>hel<>"/ym.m",ymsi];
            Export[o<>hel<>"/y_size.txt", ymsl];
            Export[o<>hel<>"/e"<>ToString[#]<>".json",sm[fin,#,1]]&/@Range[PF];
            If[odd,Export[o<>hel<>"/o"<>ToString[#]<>".json",sm[fin,#,2]]&/@Range[PF]];
            Print["  Finished " <> out <> " " <> hel];
        ];
        ,{i,Length[hels]}
    ];
];
exportF[Fmons_]:=Module[{Fml,Fms},
    Fml=Length[Fmons];
    Fms=SF[#-1]->Fmons[[#]]&/@Range[Fml];
    Export[outpre<>"Fm.m",Fms];
    Export[outpre<>"F_size.txt", Fml];
];
dedup[a_]:=Select[Variables[a],MatchQ[#,_F]&];


Fmons=Get[in<>"AllPfuncMonomials.m"];


p1LNfp0Ncp1=Get[in<>"partials_5g_1L_tHV_"<>#<>".m"]&/@allHels;
p1LNfp1Ncp0=Get[in<>"partials_5g_1L_nf_tHV_"<>#<>".m"]&/@allHels;


p2LNfp0Ncp2=Get[in<>"partials_5g_2L_tHV_"<>#<>".m"]&/@mhvs;
p2LNfp1Ncp1=Get[in<>"partials_5g_2L_nf_tHV_"<>#<>".m"]&/@mhvs;
p2LNfp2Ncp0=Get[in<>"partials_5g_2L_nf2_tHV_"<>#<>".m"]&/@mhvs;


export[p1LNfp0Ncp1, allHels, "p1LNfp0Ncp1", False];
export[p1LNfp1Ncp0, allHels, "p1LNfp1Ncp0", False];
export[p2LNfp2Ncp0, mhvs, "p2LNfp2Ncp0", False];
export[p2LNfp1Ncp1, mhvs, "p2LNfp1Ncp1", True];
export[p2LNfp0Ncp2, mhvs, "p2LNfp0Ncp2", True];


Print["Doing SFs"];
exportF[Fmons];
Ffns=dedup[Fmons];
Export[outpre<>"specFns.json",List@@@Ffns];


Fmoni1L=Sort[DeleteDuplicates[Flatten[Join[p1LNfp0Ncp1,p1LNfp1Ncp0][[;;,2]]]]];
Ffns1l=dedup[Fmons[[#]]&/@Fmoni1L];
Export[outpre<>"specFns1L.json",List@@@Ffns1l];
Fmons1L=Table[SF[i-1]->If[MemberQ[Fmoni1L,i],Fmons[[i]],0],{i,Max[Fmoni1L]}];
Export[outpre<>"Fm1L.m",Fmons1L];
Export[outpre<>"F_size1L.txt", Fmons1L//Length];


(* EOF *)
