#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=tmp

echo -e "#-\nS w,x1,x2,x3,x4,x5;" >${FRM}

perl \
    -0777p \
    -e "
        s|^\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)$||gm;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|F\[(\d+), (\d+)\]|fvu\1u\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fvu\1u\2u\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|\{{0,2}inveps\[(\d), \"([eo])\", (\d+)\]|ieu\1u\2u\3|g;
        s|ex\[(\d)\]|x\1|g;
        # s|\{{0,2}([yf]\d+ )->( .*?)\}?,\s*|L \1=\2;\n|gs;
        s|\{{0,2}(ieu\du[eo]u\d+ )->( .*?)\}{0,2},\s*|L \1=\2;\n|gs;
        s|\{{0,2}(ieu\du[eo]u\d+ )->( .*?)\}{3}\s*|L \1=\2;\n|gs;
        s|,[ \n]*(L gs)|;\n\1|g;
        " \
    ${IN} >${TMP}

constsr=$(rg -o "tc[ir]\d+" $TMP)
declare -A consts
for c in $constsr; do
    consts[$c]=1
done
for c in ${!consts[@]}; do
    echo -e "S $c;" >>$FRM
done

pentasr=$(rg -o "f\d+" $TMP)
declare -A pentas
for c in $pentasr; do
    pentas[$c]=1
done
for c in ${!pentas[@]}; do
    echo -e "S $c;" >>$FRM
done

gsr=$(rg -o "fv[u\d]*" $TMP)
declare -A gs
for c in $gsr; do
    gs[$c]=1
done
for c in ${!gs[@]}; do
    echo -e "S $c;" >>$FRM
done

echo "ExtraSymbols,array,Z;" >>$FRM
cat ${TMP} >>${FRM}
rm -f ${TMP}

echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

# es
esor=`rg -o "ie[ou\d]+ =" $FRM`
eser=`rg -o "ie[eu\d]+ =" $FRM`
declare -a eso
declare -a ese
for e in ${esor[@]}; do
    eso+=( "${e%*=}" )
done
for e in ${eser[@]}; do
    ese+=( "${e%*=}" )
done
K="L K="
i=1
for e in ${eso[@]}; do
    K+="+w^$((i++))*$e"
done
for e in ${ese[@]}; do
    K+="+w^$((i++))*$e"
done

echo "$K;" >>$FRM

echo -e "B w;\n.sort\n#optimize K\nB w;\n.sort" >>$FRM

i=1
for e in ${eso[@]}; do
    echo "L ${e}a = K[w^$((i++))];" >>$FRM
done
for e in ${ese[@]}; do
    echo "L ${e}a = K[w^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

i=0

echo "#write <$C> \"std::array<DoubleLoopValue, 12> amps_odd {\"" >>$FRM
for e in ${eso[@]}; do
    if [[ $((i % 5)) == 0 ]]; then
        echo "#write <$C> \"DoubleLoopValue(\"" >>$FRM
    fi
    echo "#write <$C> \"%E\", ${e}a" >>$FRM
    if [[ $((i % 5 )) == 4 ]]; then
        echo "#write <$C> \"),\"" >>$FRM
    else
        echo "#write <$C> \", \"" >>$FRM
    fi
    echo $((i++))
done
echo "#write <$C> \"};\"" >>$FRM

echo "#write <$C> \"std::array<DoubleLoopValue, 12> amps_even {\"" >>$FRM
for e in ${ese[@]}; do
    if [[ $((i % 5)) == 0 ]]; then
        echo "#write <$C> \"DoubleLoopValue(\"" >>$FRM
    fi
    echo "#write <$C> \"%E\", ${e}a" >>$FRM
    if [[ $((i % 5 )) == 4 ]]; then
        echo "#write <$C> \"),\"" >>$FRM
    else
        echo "#write <$C> \", \"" >>$FRM
    fi
    echo $((i++))
done
echo "#write <$C> \"};\"" >>$FRM

echo ".end" >>$FRM
