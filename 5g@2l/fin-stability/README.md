# finite remainder stability tests
* 5g @ 2 gluon loops
* call from NJet directly
* Use NJet PS constructor
## File details
### amp
* reference implementation
### par
* NJet PS gen
### par2
* recursive PS gen, random
### par3
* recursive PS gen, slice
### par4
* NJet PS gen
* with precision corrector
* writes values to file
### par5
* `./par5 <init seed> <end seed>` (unity indexed, exclusive end) for many sequential points
* `./par5 <seed>` for one point only
* NJet PS gen
* with precision corrector
* outputs to terminal
### par6_f
* `./par6_f <points file> <start point> <end point>` (zero indexed)
* read points from file
* no precision correction
* writes values to file
### par6
* NJet PS gen `./par6 <start point> <end point>` (unity indexed)
* points from file `./par6 <file> <start point> <end point>` (unity indexed)
* with precision corrector
* with timing information
* writes values to file
### run.sh
* to run `par#` in parallel
