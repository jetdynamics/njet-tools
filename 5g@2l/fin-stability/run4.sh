#!/usr/bin/env bash

strt=$1
points=$2
cores=$3

points_per_core=$((points/cores))
real_num_point=$(($cores*$points_per_core))
last_point=$(($strt+$real_num_point))

echo Requested $points points over $cores cores, starting from random number seed $strt
echo So get $points_per_core points per core, for $real_num_point points total, from rseed $strt to $last_point

for i in $(seq $strt $points_per_core $(($last_point-1))); do
    ./par4 ${i} $((${i} + ${points_per_core})) >run.${i}.log 2>&1 &
done
