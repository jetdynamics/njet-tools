#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

template <typename T>
void run(const T inc)
{
    std::cout << "Initialising amplitude class..." << '\n';
    const int legs { 5 };
    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, T>>() };
    amp->setMuR2(mur * mur);
    amp->setNc(3);
    amp->setNf(0);

    for (T x1 { 0.4 }; x1 < 1.; x1 += inc) {
        std::cout << "Evaluating point x1 = " << x1 << " ..." << '\n';

        const T x2 { 0.7 };
        const T theta { M_PI / 3. };
        const T alpha { M_PI / 5. };

        const std::array<MOM<T>, legs> moms { phase_space_point_x(x1, x2, theta, alpha, sqrtS) };

        amp->setMomenta(moms.data());
        amp->setSpecFuncVals();
        amp->setSpecFuncMons();

        amp->born_fin();
        amp->virt_fin();
        amp->virtsq_fin();
        amp->dblvirt_fin();

        std::ofstream o("slice", std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);

        o
            << x1 << ' '
            << amp->born_fin_value() << ' '
            << amp->born_fin_error() / amp->born_fin_value() << ' '
            << amp->virt_fin_value() << ' '
            << amp->virt_fin_error() / amp->virt_fin_value() << ' '
            << amp->virtsq_fin_value() << ' '
            << amp->virtsq_fin_error() / amp->virtsq_fin_value() << ' '
            << amp->dblvirt_fin_value() << ' '
            << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << ' '
            << '\n';
    }
}

int main(int argc, char* argv[])
{
    assert(argc == 1);
    run<double>(0.003);
}
