#include <array>
#include <chrono>
#include <iostream>
#include <random>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

template <typename T, typename P>
void value(const int rseed)
{
    const T mur2 { 1.65306122448979559209772337546422 };
    const int Nc { 3 };
    const std::vector<MOM<T>> moms { njet_random_point<T>(rseed) };

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, P>>() };
    amp->setMuR2(mur2);
    amp->setNf(0);
    amp->setNc(Nc);
    amp->setMomenta(moms);
    amp->setSpecFuncVals();
    amp->setSpecFuncMons();

    amp->born_fin();
    amp->virt_fin();
    // amp->virtsq_fin();
    // amp->dblvirt_fin();

    std::cout
        << "Born:                  " << amp->born_fin_value() << "\n"
        << "    Relative error:    " << amp->born_fin_error() / amp->born_fin_value() << '\n'
        << "Virt:                  " << amp->virt_fin_value() << "\n"
        << "    Relative error:    " << amp->virt_fin_error() / amp->virt_fin_value() << '\n'
        // << "VirtSq:                " << amp->virtsq_fin_value() << "\n"
        // << "    Relative error:    " << amp->virtsq_fin_error() / amp->virtsq_fin_value() << '\n'
        // << "DblVirt:               " << amp->dblvirt_fin_value() << "\n"
        // << "    Relative error:    " << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << '\n'
        << '\n';
}

template <typename T>
void single()
{
    const int legs { 5 };
    const double mur2 { 1. };
    const int Nc { 3 };

    std::chrono::time_point<std::chrono::high_resolution_clock> t0;
    std::chrono::time_point<std::chrono::high_resolution_clock> t1;

    t0 = std::chrono::high_resolution_clock::now();
    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, T>>() };
    t1 = std::chrono::high_resolution_clock::now();
    const long int dsm { std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() };

    amp->setMuR2(mur2);
    amp->setNf(0);
    amp->setNc(Nc);

    // random point
    //std::random_device dev;
    //std::mt19937_64 rng(dev());
    //const T boundary { 0.01 };
    //std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    //std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);
    //
    //T y1 { dist_y1(rng) };
    //std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
    //T y2 { dist_y2(rng) };
    //T theta { dist_a(rng) };
    //T alpha { dist_a(rng) };

    // point A
    const T y1 { 2.5466037534799052e-01 };
    const T y2 { 3.9999592899158604e-01 };
    const T theta { 1.2398476487204491e+00 };
    const T alpha { 4.3891163982516607e-01 };

    const std::array<MOM<T>, legs> moms { phase_space_point(y1, y2, theta, alpha) };

    std::cout
        << "Nc=" << Nc << '\n'
        << "MuR2=" << mur2 << '\n'
        << "Invariants:"
        << '\n'
        << '('
        << invariant(moms, 0, 1) << ", "
        << invariant(moms, 1, 2) << ", "
        << invariant(moms, 2, 3) << ", "
        << invariant(moms, 3, 4) << ", "
        << invariant(moms, 4, 0)
        << ')'
        << '\n'
        << '\n';

    amp->setMomenta(moms.data());

    t0 = std::chrono::high_resolution_clock::now();
    amp->setSpecFuncVals();
    t1 = std::chrono::high_resolution_clock::now();
    const long int dsf {
        std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
    };

    amp->setSpecFuncMons();

    amp->born_fin();

    t0 = std::chrono::high_resolution_clock::now();
    amp->virt_fin();
    t1 = std::chrono::high_resolution_clock::now();
    const long int dl {
        std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
    };

    t0 = std::chrono::high_resolution_clock::now();
    amp->virtsq_fin();
    t1 = std::chrono::high_resolution_clock::now();
    const long int dl2 {
        std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
    };

    t0 = std::chrono::high_resolution_clock::now();
    amp->dblvirt_fin();
    t1 = std::chrono::high_resolution_clock::now();
    const long int d2l {
        std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
    };

    std::cout
        << "Sparse matrix filling time: " << dsm << "ms\n"
        << "Pentagon functions eval time: " << dsf << "ms\n"
        << "L eval time:           " << dl << "ms\n"
        << "2L eval time:          " << d2l << "ms\n"
        << "L^2 eval time:         " << dl2 << "ms\n"
        << "VV eval time per PS.:  " << dsf + dl2 + d2l << "ms\n"
        << '\n'
        << "Born:                  " << amp->born_fin_value() << "\n"
        << "    Absolute error:    " << amp->born_fin_error() << '\n'
        << "    Relative error:    " << amp->born_fin_error() / amp->born_fin_value() << '\n'
        << "Virt:                  " << amp->virt_fin_value() << "\n"
        << "    Absolute error:    " << amp->virt_fin_error() << '\n'
        << "    Relative error:    " << amp->virt_fin_error() / amp->virt_fin_value() << '\n'
        << "VirtSq:                " << amp->virtsq_fin_value() << "\n"
        << "    Absolute error:    " << amp->virtsq_fin_error() << '\n'
        << "    Relative error:    " << amp->virtsq_fin_error() / amp->virtsq_fin_value() << '\n'
        << "DblVirt:               " << amp->dblvirt_fin_value() << "\n"
        << "    Absolute error:    " << amp->dblvirt_fin_error() << '\n'
        << "    Relative error:    " << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << '\n'
        << '\n';
}

template <typename T>
void run(const int pspoints)
{
    const int legs { 5 };
    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };

    std::vector<Flavour<double>> flavours(legs, StandardModel::G());

    long int tdsf { 0 };
    long int tdl { 0 };
    long int tdl2 { 0 };
    long int td2l { 0 };

    std::chrono::time_point<std::chrono::high_resolution_clock> t0;
    std::chrono::time_point<std::chrono::high_resolution_clock> t1;

    t0 = std::chrono::high_resolution_clock::now();
    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T, T>>() };
    t1 = std::chrono::high_resolution_clock::now();
    const long int dsm { std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() };

    amp->setMuR2(mur * mur);
    amp->setNf(Nf);
    amp->setNc(Nc);

    for (int p { 1 }; p < pspoints + 1; ++p) {
        std::cout << "==================== Test point " << p
                  << " ====================" << '\n';

        // rseed = p
        PhaseSpace<T> ps(legs, flavours.data(), p, sqrtS);
        const std::vector<MOM<T>> momenta { ps.getPSpoint() };
        //ps.showPSpoint();

        // std::cout << '\n';
        // for (int i { 0 }; i < legs; ++i) {
        //     for (int j { i + 1 }; j < legs; ++j) {
        //         std::cout << "s" << i + 1 << j + 1 << "/s12=" <<
        //         dot(Momenta[p][i], Momenta[p][j]) / dot(Momenta[p][0],
        //         Momenta[p][1]) * 2 << '\n';
        //     }
        // }

        amp->setMomenta(momenta);

        t0 = std::chrono::high_resolution_clock::now();
        amp->setSpecFuncVals();
        t1 = std::chrono::high_resolution_clock::now();
        const long int dsf {
            std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
        };
        tdsf += dsf;

        amp->setSpecFuncMons();

        amp->born_fin();

        t0 = std::chrono::high_resolution_clock::now();
        amp->virt_fin();
        t1 = std::chrono::high_resolution_clock::now();
        const long int dl {
            std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
        };
        tdl += dl;

        t0 = std::chrono::high_resolution_clock::now();
        amp->virtsq_fin();
        t1 = std::chrono::high_resolution_clock::now();
        const long int dl2 {
            std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
        };
        tdl2 += dl2;

        t0 = std::chrono::high_resolution_clock::now();
        amp->dblvirt_fin();
        t1 = std::chrono::high_resolution_clock::now();
        const long int d2l {
            std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
        };
        td2l += d2l;

        std::cout
            << "Born:             " << amp->born_fin_value() << "\n"
            << "  Relative error: " << amp->born_fin_error() / amp->born_fin_value() << '\n'
            << "Virt:             " << amp->virt_fin_value() << "\n"
            << "  Relative error: " << amp->virt_fin_error() / amp->virt_fin_value() << '\n'
            << "VirtSq:           " << amp->virtsq_fin_value() << "\n"
            << "  Relative error: " << amp->virtsq_fin_error() / amp->virtsq_fin_value() << '\n'
            << "DblVirt:          " << amp->dblvirt_fin_value() << "\n"
            << "  Relative error: " << amp->dblvirt_fin_error() / amp->dblvirt_fin_value() << '\n'
            << '\n';
    }

    tdsf /= pspoints;
    tdl /= pspoints;
    tdl2 /= pspoints;
    td2l /= pspoints;

    std::cout
        << "Sparse matrix filling time: " << dsm << "ms\n"
        << "Average times per phase space point:" << '\n'
        << "  Pentagon functions eval time: " << tdsf << "ms\n"
        << "  L eval time:                  " << tdl << "ms\n"
        << "  2L eval time:                 " << td2l << "ms\n"
        << "  L^2 eval time:                " << tdl2 << "ms\n"
        << "  VV total eval time per PS.:   " << tdsf + tdl2 + td2l << "ms\n"
        << '\n'
        << '\n';
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);

    if (argc == 2) {
        std::cout.precision(32);
        const int rseed { std::atoi(argv[1]) };
std::cout << "D/D\n";
        value<double, double>(rseed);
std::cout << "D/Q\n";
        value<dd_real, double>(rseed);
std::cout << "Q/Q\n";
        value<dd_real, dd_real>(rseed);
    } else {
        std::cout.precision(16);

        std::cout << '\n'
                  << "  NJet: simple example of PentagonFunctions-cpp based two-loop "
                     "finite remainder evaluation"
                  << '\n'
                  << '\n'
                  << "  Notation: DblVirt = 2*Re(A2.cA0)" << '\n'
                  << "            VirtSq  = A1.cA1" << '\n'
                  << "            Virt    = 2*Re(A1.cA0)" << '\n'
                  << "            Born    = A0.cA0" << '\n'
                  << '\n';

        //single<double>();
        run<double>(3);
    }
}
