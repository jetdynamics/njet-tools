#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

void run(const int start, const int end)
{
    std::cout << "Initialising amplitude classes..." << '\n';

    const int num { end - start };

    int count_DD { 0 };
    int count_QD { 0 };
    int count_QQ { 0 };
    int count_fail { 0 };

    const double sqrtS { 1. };
    const double mur { sqrtS / 2. };
    const int Nc { 3 };
    const int Nf { 0 };
    const double cutoff_errD { 1e-2 };
    const dd_real cutoff_errQ { static_cast<dd_real>(cutoff_errD) };

    const std::chrono::time_point<std::chrono::high_resolution_clock> t0 { std::chrono::high_resolution_clock::now() };

    NJetAccuracy<double>* ampDD { NJetAccuracy<double>::template create<Amp0q5g_a2l<double, double>>() };
    ampDD->setMuR2(mur * mur);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* ampQD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, double>>() };
    ampQD->setMuR2(mur * mur);
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* ampQQ { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, dd_real>>() };
    ampQQ->setMuR2(mur * mur);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);

    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::cout
            << "Evaluating point " << p << " ..." << '\n'
            << "  try coeff(double)+specialFn(double)..." << '\n';

        const std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS) };
        ampDD->setMomenta(psD);
        ampDD->setSpecFuncVals();
        ampDD->setSpecFuncMons();

        const double born_valD { ampDD->born_fin() };
        const double virt_valD { ampDD->virt_fin() };
        const double virtsq_valD { ampDD->virtsq_fin() };
        const double dblvirt_valD { ampDD->dblvirt_fin() };

        const double born_errD { std::abs(ampDD->born_fin_error() / ampDD->born_fin_value()) };
        const double virt_errD { std::abs(ampDD->virt_fin_error() / ampDD->virt_fin_value()) };
        const double virtsq_errD { std::abs(ampDD->virtsq_fin_error() / ampDD->virtsq_fin_value()) };
        const double dblvirt_errD { std::abs(ampDD->dblvirt_fin_error() / ampDD->dblvirt_fin_value()) };

        std::cout
            << "    error=" << dblvirt_errD << '\n';

        if (dblvirt_errD < cutoff_errD) {
            std::cout
                << "    stability test passed" << '\n';

            ++count_DD;

            std::ofstream o("result" + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(16);

            o
                << p << ' '
                << born_valD << ' '
                << born_errD << ' '
                << virt_valD << ' '
                << virt_errD << ' '
                << virtsq_valD << ' '
                << virtsq_errD << ' '
                << dblvirt_valD << ' '
                << dblvirt_errD << ' '
                << '\n';
        } else {
            std::cout
                << "    stability test failed" << '\n'
                << "  try coeff(quad)+specialFn(double)..." << '\n';

            dd_real born_valQ;
            dd_real virt_valQ;
            dd_real virtsq_valQ;
            dd_real dblvirt_valQ;

            dd_real born_errQ;
            dd_real virt_errQ;
            dd_real virtsq_errQ;
            dd_real dblvirt_errQ;

            const std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS) };

            ampQD->setMomenta(psQ);
            ampQD->setSpecFuncVals();
            ampQD->setSpecFuncMons();

            born_valQ = ampQD->born_fin();
            virt_valQ = ampQD->virt_fin();
            virtsq_valQ = ampQD->virtsq_fin();
            dblvirt_valQ = ampQD->dblvirt_fin();

            born_errQ = abs(ampQD->born_fin_error() / ampQD->born_fin_value());
            virt_errQ = abs(ampQD->virt_fin_error() / ampQD->virt_fin_value());
            virtsq_errQ = abs(ampQD->virtsq_fin_error() / ampQD->virtsq_fin_value());
            dblvirt_errQ = abs(ampQD->dblvirt_fin_error() / ampQD->dblvirt_fin_value());

            std::cout
                << "    error=" << dblvirt_errQ << '\n';

            if (dblvirt_errQ < cutoff_errQ) {
                std::cout
                    << "    stability test passed" << '\n';

                ++count_QD;
            } else {
                std::cout
                    << "    stability test failed" << '\n'
                    << "  try coeff(quad)+specialFn(quad)..." << '\n';

                ampQQ->setMomenta(psQ);
                ampQQ->setSpecFuncVals();
                ampQQ->setSpecFuncMons();

                born_valQ = ampQQ->born_fin();
                virt_valQ = ampQQ->virt_fin();
                virtsq_valQ = ampQQ->virtsq_fin();
                dblvirt_valQ = ampQQ->dblvirt_fin();

                born_errQ = abs(ampQQ->born_fin_error() / ampQQ->born_fin_value());
                virt_errQ = abs(ampQQ->virt_fin_error() / ampQQ->virt_fin_value());
                virtsq_errQ = abs(ampQQ->virtsq_fin_error() / ampQQ->virtsq_fin_value());
                dblvirt_errQ = abs(ampQQ->dblvirt_fin_error() / ampQQ->dblvirt_fin_value());

                std::cout
                    << "    error=" << dblvirt_errQ << '\n';

                if (dblvirt_errQ < cutoff_errQ) {
                    std::cout
                        << "    stability test passed" << '\n';
                    ++count_QQ;
                } else {
                    std::cout
                        << "    stability test failed" << '\n';
                    ++count_fail;
                }
            }

            std::ofstream o("result" + std::to_string(start), std::ios::app);
            o.setf(std::ios_base::scientific);
            o.precision(32);

            o
                << p << ' '
                << born_valQ << ' '
                << born_errQ << ' '
                << virt_valQ << ' '
                << virt_errQ << ' '
                << virtsq_valQ << ' '
                << virtsq_errQ << ' '
                << dblvirt_valQ << ' '
                << dblvirt_errQ << ' '
                << '\n';
        }
    }

    const std::chrono::time_point<std::chrono::high_resolution_clock> t1 { std::chrono::high_resolution_clock::now() };
    const long int dur { std::chrono::duration_cast<std::chrono::seconds>(t1 - t0).count() };

    std::ofstream o("timing" + std::to_string(start), std::ios::app);
    o
        << "Did " << num << " points, of which" << '\n'
        << "  " << count_DD << " passed as coeff(double)+specialFn(double)" << '\n'
        << "  " << count_QD << " passed as coeff(quad)+specialFn(double)" << '\n'
        << "  " << count_QQ << " passed as coeff(quad)+specialFn(quad)" << '\n'
        << "  " << count_fail << " failed the stability test" << '\n'
        << "with dblvirt relative error cut off " << cutoff_errD << '\n'
        << "taking " << dur << " seconds" << '\n'
        << "giving an average time per point of " << dur / num << " seconds" << '\n'
        << '\n';
}

int main(int argc, char* argv[])
{
    std::cout << '\n';
    assert(argc == 3);
    const int start { std::atoi(argv[1]) };
    const int end { std::atoi(argv[2]) };

    run(start, end);
    std::cout << '\n';
}
