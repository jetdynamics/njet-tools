#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "finrem/0q5g/0q5g-2l.h"
#include "chsums/NJetAccuracy.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

void run(const int start, const int end)
{
    std::cout << "Initialising the three amplitude classes..." << '\n';

    using TP = std::chrono::time_point<std::chrono::high_resolution_clock>;

    TP t0;
    TP t1;
    long int d0;
    long int d1;
    long int d2;

    double totratDD { 0. };
    double totratQD { 0. };
    double totratQQ { 0. };

    const double cutoff { 1e-3 };
    const dd_real cutoffQ { static_cast<dd_real>(cutoff) };
    std::vector<int> bad_points;
    const int num { end - start };

    const double sqrtS { 1. };
    const double ptMin { 0.003 };

    const double mur2 { 0.25 };
    const int Nc { 3 };
    const int Nf { 0 };

    t0 = std::chrono::high_resolution_clock::now();
    NJetAccuracy<double>* const ampDD { NJetAccuracy<double>::template create<Amp0q5g_a2l<double, double>>() };
    ampDD->setMuR2(mur2);
    ampDD->setNf(Nf);
    ampDD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQD { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, double>>() };
    ampQD->setMuR2(mur2);
    ampQD->setNf(Nf);
    ampQD->setNc(Nc);

    NJetAccuracy<dd_real>* const ampQQ { NJetAccuracy<dd_real>::template create<Amp0q5g_a2l<dd_real, dd_real>>() };
    ampQQ->setMuR2(mur2);
    ampQQ->setNf(Nf);
    ampQQ->setNc(Nc);
    t1 = std::chrono::high_resolution_clock::now();
    d0 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
    std::cout
	<< "  Took " << d0 << " ms"
	<< '\n';
    // rseed = p
    for (int p { start }; p < end; ++p) {
        std::cout
            << "Evaluating point " << p << " ..." << '\n'
            << "  in coeff(double)+specialFn(double)..." << '\n';

        const std::vector<MOM<double>> psD { njet_random_point<double>(p, sqrtS, ptMin) };
	//print_momenta(psD);
        ampDD->setMomenta(psD);

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->setSpecFuncs();
        t1 = std::chrono::high_resolution_clock::now();
        d1 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

        t0 = std::chrono::high_resolution_clock::now();
        ampDD->initFinRem();
        ampDD->c0lx0l_fin();
        ampDD->c1lx0l_fin();
        ampDD->c1lx1l_fin();
        ampDD->c2lx0l_fin();
        t1 = std::chrono::high_resolution_clock::now();
        d2 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

        const double dblvirt_err { std::abs(ampDD->c2lx0l_fin_error() / ampDD->c2lx0l_fin_value()) };
        const double ratDD { static_cast<double>(d1) / static_cast<double>(d2) };
        totratDD += ratDD;

        std::cout
            << "    born value =      " << ampDD->c0lx0l_fin_value() << '\n'
            << "    born rel err =    " << std::abs(ampDD->c0lx0l_fin_error() / ampDD->c0lx0l_fin_value()) << '\n'
            << "    virt value =      " << ampDD->c1lx0l_fin_value() << '\n'
            << "    virt rel err =    " << std::abs(ampDD->c1lx0l_fin_error() / ampDD->c1lx0l_fin_value()) << '\n'
            << "    virtsq value =    " << ampDD->c1lx1l_fin_value() << '\n'
            << "    virtsq rel err =  " << std::abs(ampDD->c1lx1l_fin_error() / ampDD->c1lx1l_fin_value()) << '\n'
            << "    dblvirt value =   " << ampDD->c2lx0l_fin_value() << '\n'
            << "    dblvirt rel err = " << dblvirt_err << '\n'
            << "    spec func time =  " << d1 << "ms" << '\n'
            << "    else time =       " << d2 << "ms" << '\n'
            << "    ratio =           " << ratDD << '\n';

        if (dblvirt_err > cutoff) {
            bad_points.push_back(p);

            std::cout
                << "    stability test failed" << '\n'
                << "  in coeff(quad)+specialFn(double)..." << '\n';

            const std::vector<MOM<dd_real>> psQ { njet_random_point<dd_real>(p, sqrtS, ptMin) };

            ampQD->setMomenta(psQ);

            t0 = std::chrono::high_resolution_clock::now();
            ampQD->copySpecFuncs(ampDD);
            t1 = std::chrono::high_resolution_clock::now();
            d1 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

            t0 = std::chrono::high_resolution_clock::now();
	    ampQD->initFinRem();
            ampQD->c0lx0l_fin();
            ampQD->c1lx0l_fin();
            ampQD->c1lx1l_fin();
            ampQD->c2lx0l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            d2 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

            const dd_real dblvirt_errQ { abs(ampQD->c2lx0l_fin_error() / ampQD->c2lx0l_fin_value()) };

            const double ratQD { static_cast<double>(d1) / static_cast<double>(d2) };
            totratQD += ratQD;

            std::cout
                << "    born value =      " << ampQD->c0lx0l_fin_value() << '\n'
                << "    born rel err =    " << abs(ampQD->c0lx0l_fin_error() / ampQD->c0lx0l_fin_value()) << '\n'
                << "    virt value =      " << ampQD->c1lx0l_fin_value() << '\n'
                << "    virt rel err =    " << abs(ampQD->c1lx0l_fin_error() / ampQD->c1lx0l_fin_value()) << '\n'
                << "    virtsq value =    " << ampQD->c1lx1l_fin_value() << '\n'
                << "    virtsq rel err =  " << abs(ampQD->c1lx1l_fin_error() / ampQD->c1lx1l_fin_value()) << '\n'
                << "    dblvirt value =   " << ampQD->c2lx0l_fin_value() << '\n'
                << "    dblvirt rel err = " << dblvirt_errQ << '\n'
                << "    spec func time =  " << d1 << "ms" << '\n'
                << "    else time =       " << d2 << "ms" << '\n'
                << "    ratio =           " << ratQD << '\n';
	    if (dblvirt_errQ > cutoff) {

	    std::cout
                << "    stability test failed" << '\n'
                << "  in coeff(quad)+specialFn(quad)..." << '\n';

            ampQQ->setMomenta(psQ);

            t0 = std::chrono::high_resolution_clock::now();
            ampQQ->setSpecFuncs();
            t1 = std::chrono::high_resolution_clock::now();
            d1 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

            t0 = std::chrono::high_resolution_clock::now();
            ampQQ->initFinRem();
            ampQQ->copyCoeffs(ampQD); // TODO this doesn't yet work!
            ampQQ->c0lx0l_fin();
            ampQQ->c1lx0l_fin();
            ampQQ->c1lx1l_fin();
            ampQQ->c2lx0l_fin();
            t1 = std::chrono::high_resolution_clock::now();
            d2 = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

            const double ratQQ { static_cast<double>(d1) / static_cast<double>(d2) };
            totratQQ += ratQQ;

            std::cout
                << "    born value =      " << ampQQ->c0lx0l_fin_value() << '\n'
                << "    born rel err =    " << ampQQ->c0lx0l_fin_error() / ampQQ->c0lx0l_fin_value() << '\n'
                << "    virt value =      " << ampQQ->c1lx0l_fin_value() << '\n'
                << "    virt rel err =    " << ampQQ->c1lx0l_fin_error() / ampQQ->c1lx0l_fin_value() << '\n'
                << "    virtsq value =    " << ampQQ->c1lx1l_fin_value() << '\n'
                << "    virtsq rel err =  " << ampQQ->c1lx1l_fin_error() / ampQQ->c1lx1l_fin_value() << '\n'
                << "    dblvirt value =   " << ampQQ->c2lx0l_fin_value() << '\n'
                << "    dblvirt rel err = " << ampQQ->c2lx0l_fin_error() / ampQQ->c2lx0l_fin_value() << '\n'
                << "    spec func time =  " << d1 << "ms" << '\n'
                << "    else time =       " << d2 << "ms" << '\n'
                << "    ratio =           " << ratQQ << '\n';
	    }
        }
    }

    std::cout
        << '\n'
        << "The average time ratios as (spec func time)/(else time) were:" << '\n'
        << "  coeff(double)+specialFn(double) = " << totratDD / num << '\n'
        << "  coeff(quad)+specialFn(double) = " << totratQD / bad_points.size() << '\n'
        << "  coeff(quad)+specialFn(quad) = " << totratQQ / bad_points.size() << '\n'
        << '\n'
        << "There were " << bad_points.size() << " bad points out of " << num << " total points" << '\n'
        << "which had dblvirt rel err > " << cutoff << '\n'
        << "They were at rseeds:" << '\n';
    for (int b : bad_points) {
        std::cout << "  " << b << '\n';
    }
}

int main(int argc, char* argv[])
{
	std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
	std::cout.precision(16);

	std::cout << '\n';

	int start, end;
	if (argc == 3) {
		start = std::atoi(argv[1]) ;
		end = std::atoi(argv[2]) ;
	} else if (argc == 2) {
		start = std::atoi(argv[1]) ;
		end = start + 1;
	} else {
		std::cerr << "Provide arguments! (see `README.md`)\n";
		std::exit(EXIT_FAILURE);
	}

	run(start, end);

	std::cout << '\n';
}
