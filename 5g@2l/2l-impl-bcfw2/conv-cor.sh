#!/usr/bin/env bash

IN=$1
BASE=${IN%.m}
OUT=${BASE}.c

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|([^ *+-/()]+)\^(\d+)|njet_pow(\1, \2)|g;
        s|(\(.*?)\)\^(\d+)|njet_pow\1, \2)|g;
        s|Log\[MuR2\]|LR|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        # s|Pi|pi()|g;
        s|a\[(\d)\]|ep5.get\1()|g;
        s|\{(.*)\}|\1|gs;
        " \
    ${IN} > ${OUT}
