(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl-bcfw2/"];


data=Get["P2_mm+++_lr_bcfw_1_2_SZ.m"];


epss[o_,p_]:=Module[{one},
one=(((Plus@@(data[[1,o,p,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],If[p==1,"e","o"],o]->Coefficient[one,eps,#]&/@Range[0,-4,-1]
]
epsPe[o_]:=epss[o,1];
epsPo[o_]:=epss[o,2];


epsE=epsPe/@Range[12];


epsO=epsPo/@Range[12];


Export["eps.m",{epsE,epsO}];
Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];


es=Transpose[{epsE,epsO}];


Export["e"<>ToString[#]<>".m",es[[#]]]&/@Range[12];


cGamma = Gamma[1+e]*Gamma[1-e]^2/Gamma[1-2*e]*Exp[e*EulerGamma]
norm2l = Normal@Series[cGamma^2,{e,0,4}] // FullSimplify;
norm2l = Collect[norm2l,e]


full=Normal@Series[cGamma^2*(MuR2)^(2*e),{e,0,4}] // FullSimplify//Collect[#,e]&


wrong=((1+eps^2*Pi^2/12)*(MuR2)^eps)^2//Series[#,{eps,0,4}]&


murexp=Normal@Series[(MuR2)^(2*e),{e,0,4}]//Collect[#,e]&


generic=Plus@@(a[#]/e^#&/@Range[0,4])


murexp*generic//Series[#,{e,0,0}]&//Normal//Collect[#,e]&
Coefficient[%,e,#]&/@Range[0,-4,-1]
Export["murcor.m",%];



