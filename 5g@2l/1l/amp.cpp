#include <array>
#include <cassert>
#include <iostream>
#include <random>

#include "analytic/0q5g-analytic.h"
#include "ngluon2/EpsTriplet.h"
#include "ngluon2/Mom.h"

#include "Hel.hpp"
#include "PhaseSpace.hpp"

const int N { 5 };

template <typename T>
EpsTriplet<T> run(const Helicity<N> hels, const std::array<MOM<T>, N> mom, const int Nc, const T mur)
{
    const T scale { 1. };
    Amp0q5g_a<T> amp(scale);

    amp.setNc(Nc);

    amp.setMuR2(mur * mur);

    // std::cout << '\n';
    // for (int i { 0 }; i < N; ++i) {
    //     for (int j { i + 1 }; j < N; ++j) {
    //         std::cout << "s" << i + 1 << j + 1 << "=" << dot(mom[i], mom[j]) * 2 << '\n';
    //     }
    // }

    amp.setMomenta(mom.data());

    const EpsTriplet<T> hamp_virt { amp.virt(hels.data()) };

    return hamp_virt;

    // amp.setHelicity(hels.data());
    // std::cout<<amp.AL().loop/amp.A0()<<'\n';
}

template <typename T>
void loop_matrix()
{
    std::random_device dev;
    std::mt19937_64 rng(dev());
    const T boundary { 0.01 };
    std::uniform_real_distribution<T> dist_y1(boundary, 1. - boundary);
    std::uniform_real_distribution<T> dist_a(boundary, M_PI - boundary);

    std::cout << '{' << '\n';
    for (int i { 0 }; i < 10; ++i) {

        T y1 { dist_y1(rng) };
        std::uniform_real_distribution<T> dist_y2(boundary, 1. - y1 - boundary);
        const std::array<MOM<T>, N> moms { phase_space_point(y1, dist_y2(rng), dist_a(rng), dist_a(rng)) };

        for (int i0 { -1 }; i0 < 2; i0 += 2) {
            for (int i1 { -1 }; i1 < 2; i1 += 2) {
                for (int i2 { -1 }; i2 < 2; i2 += 2) {
                    for (int i3 { -1 }; i3 < 2; i3 += 2) {
                        for (int i4 { -1 }; i4 < 2; i4 += 2) {
                            const Helicity<N> hels { { i4, i3, i2, i1, i0 } };
                            if (hels.order() < 2) {

                                for (int Nc { 3 }; Nc < 1000; Nc *= 10) {

                                    for (T mur { 1. }; mur < 100.; mur *= 10.) {

                                        const EpsTriplet<T> hamp_virt { run<T>(hels, moms, Nc, mur) };

                                        std::cout << "{" << '\n';

                                        std::cout << "{" << '\n';
                                        for (int j { 0 }; j < N - 1; ++j) {
                                            std::cout << moms[j] << ',' << '\n';
                                        }
                                        std::cout << moms[N - 1] << "\n},\n";

                                        std::cout << "\"" << hels << "\"," << '\n';

                                        std::cout << Nc << "," << '\n';

                                        std::cout << mur << "," << '\n';

                                        std::cout << hamp_virt << '\n';

                                        std::cout << "}," << '\n';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    std::cout << '}' << '\n';
}

int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);

    loop_matrix<double>();
}
