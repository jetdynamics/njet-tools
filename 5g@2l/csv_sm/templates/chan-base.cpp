{%- from 'macros.jinja2' import comma -%}
/*
 * finrem/{{ amp }}/common/chan.cpp
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#include <type_traits>

#include "chan.h"

template <typename T, typename P>
Amp{{ amp }}_a2l<T, P>::Amp{{ amp }}_a2l(const std::array<std::array<int, {{ legs }}>, {{ partials }}> &ords_, const T scalefactor, const int mFC,
                               const NJetAmpTables &tables)
    : Base(scalefactor, mFC, tables), FinRem(*Base::njetan, Loops::Number),
    coeffs_cached(false),
    {%- for lo in loops %}
    hA{{ lo.id }}r(), hA{{ lo.id }}p(),
    {%- endfor %}
    ords(ords_) {}

template <typename T, typename P> void Amp{{ amp }}_a2l<T, P>::initFinRem() {
  FinRem::setTr5Sgn();
  FinRem::resetCacheScaleless();
  FinRem::setxi();
  coeffs_cached = false;
};

// { cached calls

{% for lo in loops %}
{% if lo.order -%}
// TODO: enter manually
{% endif -%}
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, {{ partials }}>
Amp{{ amp }}_a2l<T, P>::A{{ lo.id }}_mem(const int i) {
  return call_cached(
  {%- if lo.order == 0 -%}
  L{{ lo.id }}
  {%- elif lo.order == 1 -%}
  L00, L{{ lo.id }}
  {%- elif lo.order == 2 -%}
  L00, L01, L10, L{{ lo.id }}
  {%- endif -%}, i,
  {%- if lo.order == 0 -%}
  hA00r
  {%- elif lo.order == 1 -%}
  hA00r, hA{{ lo.id }}r
  {%- elif lo.order == 2 -%}
  hA00r, hA01r, hA10r, hA{{ lo.id }}r
  {%- endif -%}
  {%- if lo.order -%}
  ,
  [this](
  {%- if lo.order == 1 -%}
  const std::complex<T> tree, const std::complex<T> loop
  {%- elif lo.order == 2 -%}
  const std::complex<T> tree, const std::complex<T> loop_nfp0, const std::complex<T> loop_nfp1, const std::complex<T> dblloop
  {%- endif -%}
  ) {
    return tree * log(MuR2());
  }
  {%- endif -%}
  );
};
{% if lo.order == 1 %}
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, {{ partials }}>
Amp{{ amp }}_a2l<T, P>::A{{ lo.id }}_uhv_mem(const int i) {
  return call_cached(L{{lo.id}}, i, hA{{ lo.id }}r);
};
{% endif %}
{%- endfor %}

// }{ debugging methods

template <typename T, typename P>
void Amp{{ amp }}_a2l<T, P>::printSpecFuncs() const {
  p.printSpecFuncs();
}

// }{ helicity amplitude partial vector call

{% for lo in loops %}
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, {{ partials }}> Amp{{ amp }}_a2l<T, P>::A{{ lo.id }}p() {
  const HelPairVecFn hampvec{hA{{ lo.id }}p[mhelint]};
  if (hampvec) {
    return callLoopVec(hampvec);
  } else {
    return {};
  }
}
{% endfor %}

{% for lo in loops if lo.order %}
template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, {{ partials }}>
Amp{{ amp }}_a2l<T, P>::A{{ lo.id }}_scaleless(HelAmp{{ amp }}{% if lo.order == 1 %}1L{% else %}{% if lo.parity_odd %}Odd{% endif %}{% endif %}<T> &hamp,
                                      const std::complex<T> phase) {
  if (!coeffs_cached) {
    hamp.hA{{ lo.id }}_coeffs(x1, x2, x3, x4, x5);
    if constexpr (!std::is_same<T, double>::value) {
      hamp.cacheCoeffs_{{ lo.id }}();
    }
  }

  std::array<std::complex<T>, {{ partials }}> amps{
    {%- for i in range(partials) %}
    {%- if lo.parity_odd %}
    hamp.pA{{ lo.id }}{{ i }}e() + T(gbl_sgn) * hamp.pA{{ lo.id }}{{ i }}o()
    {%- else %}
    hamp.pA{{ lo.id }}{{ i }}e()
    {%- endif %}{{ comma(loop) }}
    {%- endfor %}
  };

  for (std::complex<T> &amp : amps) {
    amp *= phase;
  }

  if constexpr (std::is_same<T, double>::value) {
    hamp.conjVec(hamp.f{{ lo.id }});
  } else {
    {%- if lo.parity_odd %}
    hamp.conjVecs(hamp.c{{ lo.id }}e);
    hamp.conjVecs(hamp.c{{ lo.id }}o);
    {%- else %}
    hamp.conjVecs(hamp.c{{ lo.id }}e);
    {%- endif %}
  }

  std::array<std::complex<T>, {{ partials }}> camps{
    {%- for i in range(partials) %}
    {%- if lo.parity_odd %}
    hamp.pA{{ lo.id }}{{ i }}e() - T(gbl_sgn) * hamp.pA{{ lo.id }}{{ i }}o()
    {%- else %}
    hamp.pA{{ lo.id }}{{ i }}e()
    {%- endif %}{{ comma(loop) }}
    {%- endfor %}
  };

  const std::complex<T> cphase{std::conj(phase)};

  for (std::complex<T> &camp : camps) {
    camp *= cphase;
  }

  std::array<LoopResult<std::complex<T>>, {{ partials }}> resvec;

  for (int i{0}; i < {{ partials }}; ++i) {
    resvec[i] = {amps[i], camps[i]};
  }

  return resvec;
}
{% endfor %}

// }{ trees primaries [MHVs] w/o caching
// this bundles up the tree partial amplitudes from the base class in a
// compatible format for the finite remainder setup

// grab the value of the tree partial amplitude from Amp{{ amp }}_a<T>
template <typename T, typename P>
std::complex<T> Amp{{ amp }}_a2l<T, P>::A00_single(const int binhel,
                                              const int part) {
  const int *ord{ords[part].data()};
  const int hel{HelicityOrder(binhel, ord)};
  const HelAmp hamp{hA0[hel]};
  return i_ * (static_cast<Base *>(this)->*hamp)(ord);
}

template <typename T, typename P>
std::array<LoopResult<std::complex<T>>, {{ partials }}>
Amp{{ amp }}_a2l<T, P>::A00_vec(int binhel) {
  int conjhel{ {{- (2**legs)-1 }} - binhel};
  std::array<LoopResult<std::complex<T>>, {{ partials }}> resvec;
  for (int i{0}; i < {{ partials }}; ++i) {
    resvec[i] = {A00_single(binhel, i), -A00_single(conjhel, i)};
  }
  return resvec;
}

#ifdef USE_SD
template class Amp{{ amp }}_a2l<double, double>;
#endif
#ifdef USE_DDSD
template class Amp{{ amp }}_a2l<dd_real, double>;
#endif
#ifdef USE_DDDD
template class Amp{{ amp }}_a2l<dd_real, dd_real>;
#endif
#ifdef USE_QD
template class Amp{{ amp }}_a2l<qd_real, qd_real>;
#endif
#ifdef USE_VC
template class Amp{{ amp }}_a2l<Vc::double_v, Vc::double_v>;
#endif
