/*
 * finrem/{{ amp }}/{{ chan }}/{{ h_char }}.h
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#pragma once

#include <array>
#include <complex>
#include <vector>

#include <Eigen/SparseCore>

#include "ngluon2/EpsQuintuplet.h"
#include "ngluon2/utility.h"
#include "finrem/{{ amp }}/common/hel.h"

template <typename T>
class Amp{{ amp }}_{{ chan }}_{{ h_char }} final : public HelAmp{{ amp }}{% if mhv %}Odd{% else %}1L{% endif %}<T> {
  using Base = HelAmp{{ amp }}{% if mhv %}Odd{% else %}1L{% endif %}<T>;

public:
  explicit Amp{{ amp }}_{{ chan }}_{{ h_char }}(const std::vector<std::complex<T>> &sf_);

  {% for loop_obj in loops %}
  {%- if (mhv and loop_obj.order) or (not mhv and loop_obj.order == 1) %}
  virtual void hA{{ loop_obj.id }}_coeffs(std::complex<T> x1, std::complex<T> x2,
                           std::complex<T> x3, std::complex<T> x4,
                           std::complex<T> x5) override final;
  {%- endif %}
  {%- endfor %}

private:
  {% for loop_obj in loops %}
  {%- if (mhv and loop_obj.order) or (not mhv and loop_obj.order == 1) %}
  {%- if loop_obj.parity_odd and mhv %}
  using Base::c{{ loop_obj.id }}e;
  using Base::c{{ loop_obj.id }}o;
  {%- else %}
  using Base::c{{ loop_obj.id }}e;
  {%- endif %}
  {%- endif %}
  {%- endfor %}
  {%- for loop_obj in loops %}
  {%- if (mhv and loop_obj.order) or (not mhv and loop_obj.order == 1) %}
  using Base::f{{ loop_obj.id }};
  {%- endif %}
  {%- endfor %}

  {%- for loop_obj in loops %}
  {%- if (mhv and loop_obj.order) or (not mhv and loop_obj.order == 1) %}
  {%- for i in range(partials) %}
  {%- if loop_obj.parity_odd and mhv %}
  using Base::m{{ loop_obj.id }}{{ i }};
  using Base::m{{ loop_obj.id }}{{ i }}o;
  {%- else %}
  using Base::m{{ loop_obj.id }}{{ i }};
  {%- endif %}
  {%- endfor %}
  {%- endif %}
  {%- endfor %}
};
