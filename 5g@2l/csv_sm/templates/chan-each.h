/*
 * finrem/{{ amp }}/{{ chan }}/{{ amp }}-{{ chan }}.h
 *
 * This file is part of NJet library
 * Copyright (C) {{ year }} NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#pragma once

#include "finrem/{{ amp }}/common/chan.h"
{% for hel in hels %}
#include "{{ hel.chars }}.h"
{%- endfor %}

template <typename T, typename P>
class Amp{{ amp }}_{{ chan }}_a2l : public Amp{{ amp }}_a2l<T, P> {
  using Base = Amp{{ amp }}_a2l<T, P>;

public:
  Amp{{ amp }}_{{ chan }}_a2l(T scalefactor = 1., int mFC = 1,
              const NJetAmpTables &tables = Base::amptables());

  // all this SpecFunc stuff can be called by setSpecFuncs() or copySpecFuncs()
  // from chsums/NJetAmp.h

  // this is called after setMomenta, before setSpecFuncMons()
  virtual void setSpecFuncVals() override final;
  virtual void setSpecFuncVals1L() override final;

  // this is called after setSpecFuncVals, before dblvirt() etc
  virtual void setSpecFuncMons() override final;
  virtual void setSpecFuncMons1L() override final;

  using Base::printSpecFuncs;
  using Base::copySpecFuncs;
  using Base::getSpecFuncs;

#ifdef USE_DDDD
  virtual void copyCoeffs(const NJetAmp<dd_real> *const other) override final;
#endif
{% for hel in hels %}
  Amp{{ amp }}_{{ chan }}_{{ hel.chars }}<T> hamp{{ '%02d' % hel.dec }};
{%- endfor %}

protected:
  {%- for hel in hels %}
  using Base::phase{{ '%02d' % hel.dec }};
  {%- endfor %}
  using Base::sf;
  using Base::p;
  using Base::coeffs_cached;
  using Base::conjugate;
  using Base::setsij;
  using Base::s12;
  using Base::s15;
  using Base::s23;
  using Base::s34;
  using Base::s45;
  using Base::hA00r;
  using Base::hA00p;
  {%- for lo in loops %}
  using Base::hA{{ lo.id }}r;
  using Base::hA{{ lo.id }}p;
  {%- endfor %}
  using Base::A00_vec;
  {%- for lo in loops if lo.order %}
  using Base::A{{ lo.id }}_scaleless;
  {%- endfor %}
  using Base::A00_mem;
  {%- for lo in loops %}
  using Base::A{{ lo.id }}_mem;
  {%- if lo.order == 1 %}
  using Base::A{{ lo.id }}_uhv_mem;
  {%- endif %}
  {%- endfor %}

  template <typename TT, typename PP>
  void copyCoeffsT(const Amp{{ amp }}_{{ chan }}_a2l<TT, PP> *const other);
};
