(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl-new/"];


data=Get["P2_mm+++_lr_bcfw12.m"];


epsTrip2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-4,-1]
]


eps=epsTrip2/@Range[12];


Select[Variables[data[[1]]],MatchQ[#,_F]&];
Export["Fs.m",%];


eps[[1,2]]//Simplify


Rationalize[N[268341205 /4270449664],10^(-16)]


inveps[0,1]/.(eps[[1,1]]/.{f[1]->32 f[1]/25})
Export["test.m",%];
