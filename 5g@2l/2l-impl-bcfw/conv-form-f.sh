#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=tmp

echo -e "#-\nS u,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*|L \1=\2;\n|gs;
        s|([yf]\d+ )->( .*?)\}{1,2}|L \1=\2;\n|gs;
        " \
    ${IN} >${TMP}


ysr=$(rg -o "y\d+" $TMP)
declare -A ys
for y in $ysr; do
    ys[$y]=1
done
for y in ${!ys[@]}; do
    echo -e "S $y;" >>$FRM
done

echo "ExtraSymbols,array,Z;" >>$FRM
cat ${TMP} >> ${FRM}
rm -f ${TMP}
echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

fsr=$(rg -o "f\d+ =" $FRM)
declare -a fs
for f in ${fsr[@]}; do
    fs+=( "${f%*=}" )
done
N=$((${#fs[@]}/${#fs}))

h="L H="
i=1
for f in ${fs[@]}; do
    h+="+u^$((i++))*$f"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${fs[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

echo "#write <$C> \"const std::array<TreeValue, $N> f{\"" >>$FRM
for f in ${fs[@]}; do
    echo "#write <$C> \"%E,\", ${f}a" >>$FRM
done
echo "#write <$C> \"};\"" >>$FRM

echo ".end" >>$FRM
