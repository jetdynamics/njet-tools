#!/usr/bin/env bash

d=hel_$1
cd ${d}
tform ys.frm >form.ys.log 2>form.ys.err &
tform fs.frm >form.fs.log 2>form.fs.err &
for i in {1..12}; do
    tform e${i}.frm >form.${i}.log 2>form.${i}.err &
done
cd ..
