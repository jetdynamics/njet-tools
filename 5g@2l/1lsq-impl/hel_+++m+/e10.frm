#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f136;
S f66;
S f183;
S f134;
S f63;
S f132;
S f131;
S f184;
S f61;
S f130;
S f60;
S f124;
S f74;
S f125;
S f75;
S f127;
S f120;
S f71;
S f72;
S f123;
S f78;
S f129;
S f151;
S f150;
S f153;
S f152;
S f155;
S f154;
S f157;
S f159;
S f158;
S f8;
S f148;
S f149;
S f81;
S f3;
S f17;
S f7;
S f5;
S f4;
S f179;
S f29;
S f178;
S f173;
S f20;
S f27;
S f177;
S f176;
S f26;
S f25;
S f175;
S f118;
S f45;
S f160;
S f117;
S f32;
S f162;
S f46;
S f33;
S f34;
S f111;
S f164;
S f36;
S f166;
S f112;
S f58;
S f59;
S f104;
S f105;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu10 = (-11*f17 + 11*f20 + 110*f59 - 11*f61 + 
      11*f120 + 74*f150 + 11*f159)/27 - (f164*fvu3u1)/3 + 
    (f164*fvu3u2)/6 + (2*f164*fvu3u3)/9 + (11*f164*fvu3u5)/18 - 
    (f164*fvu3u6)/9 + (f164*fvu3u7)/3 + (16*f164*fvu3u8)/9 - 
    (f164*fvu3u10)/6 + (2*f164*fvu3u11)/9 + (2*f164*fvu3u12)/9 - 
    (f164*fvu3u13)/18 + (f164*fvu3u14)/3 + (f164*fvu3u17)/6 + 
    (f164*fvu3u18)/6 - (f164*fvu3u19)/9 - (2*f164*fvu3u24)/9 + 
    ((-4*f148 - f164)*fvu3u31)/9 + ((-14*f148 + 3*f160)*fvu3u32)/
     18 - (f148*fvu3u33)/2 - (16*f148*fvu3u34)/9 - 
    (2*f148*fvu3u36)/9 + ((2*f148 - f164)*fvu3u37)/18 - 
    (f164*fvu3u38)/6 - (f148*fvu3u40)/3 + (f164*fvu3u42)/3 + 
    (2*f148*fvu3u44)/9 - (2*f148*fvu3u51)/9 - (f148*fvu3u52)/6 - 
    (f148*fvu3u53)/6 + (f148*fvu3u56)/18 - (f162*fvu3u57)/6 + 
    ((-f148 - f158)*fvu3u59)/6 + (f164*fvu1u1u1^3)/2 + 
    (11*f164*fvu1u1u2^3)/27 + ((2*f148 + f164)*fvu1u1u3^3)/18 - 
    (f148*fvu1u1u4^3)/3 + (2*(f148 - f164)*fvu1u1u5^3)/9 + 
    (35*f148*fvu1u1u6^3)/27 + ((4*f148 + 3*f164)*fvu1u1u7^3)/27 + 
    ((3*f148 + 4*f164)*fvu1u1u9^3)/27 + 
    fvu1u1u8^2*((-6*f111 + f112 + 3*f149 + 3*f150 - 3*f151 - 
        3*f153 + 3*f154 + 6*f162 + f166)/18 + 
      (f164*fvu1u1u9)/9) + ((-6*f111 + f112 + 6*f162 + f166)*
      fvu2u1u12)/9 + ((-6*f148 + 6*f155 + 6*f158 - f173 - 
       6*f183 + f184)*fvu2u1u14)/9 + 
    ((6*f111 - f112 + 6*f158 - 6*f162 - 6*f183 + f184)*
      fvu2u1u15)/9 + (2*f148*fvu1u2u2*tci11^2)/9 - 
    (2*f148*fvu1u2u3*tci11^2)/3 - (2*f164*fvu1u2u6*tci11^2)/
     3 - (f164*fvu1u2u7*tci11^2)/3 + 
    ((-2*f3 + 16*f4 - 2*f5 + 8*f7 - f8 + f17 + 32*f26 - 
       4*f29 + 16*f32 - 2*f33 - 8*f34 + f36 - 36*f58 + 13*f59 + 
       4*f60 - 2*f61 + 16*f63 - 2*f66 - 40*f71 + 16*f72 + 
       5*f74 - 2*f75 + 8*f78 - f81 + 16*f104 - 2*f105 - 
       16*f117 + 2*f118 - 2*f120 + 32*f123 - 4*f124 + 16*f125 - 
       2*f127 + 16*f129 + 16*f130 - 2*f131 - 2*f132 - 16*f134 + 
       2*f136 + 16*f149 + 16*f150 - 16*f151 - 16*f153 + 16*f154 + 
       4*f159 + 16*f175 - 2*f176 - 2*f177 - 16*f178 + 2*f179)*
      tci12)/18 + ((6*f111 - f112 - 6*f162 - f166)*fvu1u2u9*
      tci12)/9 - (4*f164*fvu2u1u2*tci12)/9 - 
    (2*f164*fvu2u1u3*tci12)/3 + (f164*fvu2u1u5*tci12)/3 + 
    (2*f148*fvu2u1u6*tci12)/3 - (16*f164*fvu2u1u9*tci12)/9 - 
    (4*f164*fvu2u1u11*tci12)/9 - (2*f148*fvu2u1u13*tci12)/9 + 
    fvu2u1u8*((6*f45 - f46 - 6*f148 + 6*f155 - 6*f160 - f173)/
       9 - (4*f148*tci12)/9) + fvu1u1u10^2*
     (f153/6 - (f148*tci12)/6) + fvu1u1u4^2*
     ((f164*fvu1u1u5)/18 - (f148*fvu1u1u6)/9 + 
      (5*f148*fvu1u1u7)/18 + (2*f164*fvu1u1u8)/9 + 
      (7*f164*fvu1u1u9)/18 + (f148*fvu1u1u10)/6 + 
      (f148*tci12)/3) + fvu1u1u6^2*
     ((6*f45 - f46 - 12*f148 - 3*f154 + 12*f155 + 6*f158 - 
        6*f160 - 2*f173 - 6*f183 + f184)/18 - 
      (4*f148*fvu1u1u7)/9 - (f148*fvu1u1u8)/3 - 
      (f148*fvu1u1u9)/6 + (f148*tci12)/2) + 
    fvu1u1u9^2*((6*f111 - f112 - 3*f149 + 6*f158 - 6*f162 - 
        6*f183 + f184)/18 + (f148*fvu1u1u10)/9 + 
      ((f148 - 10*f164)*tci12)/18) + 
    fvu1u1u3^2*(-(f148*fvu1u1u6)/6 + (f164*fvu1u1u7)/6 + 
      (f164*fvu1u1u8)/6 - (f148*fvu1u1u9)/18 + 
      (f148*fvu1u1u10)/9 + ((-2*f148 - 9*f164)*tci12)/18) + 
    fvu1u1u7^2*((-6*f45 + f46 + 3*f151 + 6*f160 - f166)/18 + 
      ((4*f148 + f164)*fvu1u1u8)/18 - (f164*fvu1u1u9)/6 + 
      (f148*fvu1u1u10)/6 + ((3*f148 - 2*f164)*tci12)/18) + 
    fvu1u1u5^2*(-(f148*fvu1u1u6)/3 + ((4*f148 + f164)*fvu1u1u7)/
       18 + ((-2*f148 + f164)*fvu1u1u8)/18 + (f164*fvu1u1u9)/6 + 
      ((f148 - f164)*tci12)/9) + 
    fvu2u1u10*((-6*f45 + f46 + 6*f160 - f166)/9 + 
      (2*(4*f148 - f164)*tci12)/9) + 
    fvu1u1u1^2*(-(f164*fvu1u1u2)/9 - (f164*fvu1u1u3)/6 - 
      (f164*fvu1u1u4)/9 - (f148*fvu1u1u6)/9 + 
      ((f148 - 3*f164)*fvu1u1u7)/9 + (f164*fvu1u1u8)/18 - 
      (f164*fvu1u1u9)/18 - (23*f164*tci12)/18) + 
    fvu1u1u2^2*((f164*fvu1u1u4)/9 - (f164*fvu1u1u5)/9 + 
      (f164*fvu1u1u8)/9 + (2*f164*fvu1u1u9)/9 - 
      (4*f164*tci12)/9) + tci11^2*
     ((-12*f111 + 2*f112 - 12*f148 + 18*f149 + 21*f150 - 
        18*f151 - 18*f153 + 18*f154 + 12*f155 - 12*f158 + 
        12*f162 + 8*f166 - 2*f173 + 12*f183 - 2*f184)/108 + 
      ((12*f148 - 3*f160 + 3*f162 + 7*f164)*tci12)/108) + 
    fvu1u2u8*((2*(4*f148 - f164)*tci11^2)/9 + 
      ((-6*f45 + f46 + 6*f160 - f166)*tci12)/9) + 
    fvu1u1u10*((-f3 - f20 + 8*f25 + 8*f26 - f27 - f29 + 
        8*f32 - f33 - 18*f58 - 2*f59 + 2*f60 + 8*f63 - f66 - 
        8*f71 + f74 + 8*f78 - f81 - 8*f104 + f105 - 2*f120 + 
        8*f123 - f124 + 8*f125 - f127 + 8*f129 + 8*f130 - 
        f131 - f132 - 2*f150 - 8*f151 + f152 - 8*f153 + f159 - 
        f177 - 8*f178 + f179)/9 + (2*f148*fvu2u1u13)/9 + 
      (2*f148*fvu2u1u14)/3 + (2*f148*fvu2u1u15)/9 + 
      (7*f148*tci11^2)/27 + ((-6*f111 + f112 + 6*f162)*tci12)/
       9 - (2*f148*fvu1u2u2*tci12)/9) + 
    fvu1u1u9*((16*f4 - 2*f5 + 8*f7 - f8 + 3*f17 + 16*f26 - 
        2*f29 - 8*f34 + f36 - 3*f59 - 24*f71 + 16*f72 + 3*f74 - 
        2*f75 + 8*f78 - f81 + 16*f104 - 2*f105 - 16*f117 + 
        2*f118 + 16*f123 - 2*f124 - 16*f134 + 2*f136 + 16*f149 + 
        16*f175 - 2*f176)/18 + (14*f164*fvu2u1u11)/9 + 
      (2*f148*fvu2u1u15)/9 - (2*(f148 - 6*f164)*tci11^2)/27 + 
      (f166*tci12)/9 + fvu1u1u10*((-6*f111 + f112 + 6*f162)/9 + 
        (2*f148*tci12)/9)) + fvu1u1u3*((f164*fvu1u1u5^2)/6 - 
      (f148*fvu1u1u6^2)/6 - (f164*fvu1u1u4*fvu1u1u8)/3 + 
      (f164*fvu1u1u8^2)/6 - (f148*fvu1u1u9^2)/18 - 
      (f164*fvu2u1u5)/3 + (2*f148*fvu2u1u13)/9 + 
      ((f148 + 12*f164)*tci11^2)/18 - (f164*fvu1u1u5*tci12)/
       3 - (f164*fvu1u1u7*tci12)/3 - (f164*fvu1u1u8*tci12)/
       3 - (2*f148*fvu1u2u2*tci12)/9 + (f164*fvu1u2u7*tci12)/
       3 + fvu1u1u9*((-2*f148*fvu1u1u10)/9 + (f148*tci12)/9) + 
      fvu1u1u6*((f148*fvu1u1u9)/3 + (f148*tci12)/3)) + 
    fvu1u1u6*((-8*f25 + f27 - 8*f78 + f81 + 8*f104 - f105 + 
        8*f154)/9 + ((6*f148 - 6*f155 + f173)*fvu1u1u7)/9 - 
      (8*f148*fvu1u1u7^2)/9 - (5*f148*fvu1u1u8^2)/9 - 
      (f148*fvu1u1u9^2)/6 + ((6*f148 - 6*f155 + f173)*fvu1u1u10)/
       9 - (f148*fvu1u1u10^2)/3 - (2*f148*fvu2u1u6)/3 + 
      (20*f148*fvu2u1u8)/9 + (2*f148*fvu2u1u14)/3 - 
      f148*tci11^2 + ((6*f45 - f46 - 6*f160)*tci12)/9 + 
      (2*f148*fvu1u2u3*tci12)/3 + fvu1u1u9*
       ((-6*f158 + 6*f183 - f184)/9 - (f148*tci12)/3) + 
      fvu1u1u8*((-6*f45 + f46 + 6*f160)/9 + (10*f148*tci12)/
         9)) + fvu1u1u7*(-f152/9 + ((12*f148 - f164)*fvu1u1u8^2)/
       18 - (f164*fvu1u1u9^2)/6 + (f148*fvu1u1u10^2)/6 + 
      (2*(2*f148 - f164)*fvu2u1u7)/9 + (16*f148*fvu2u1u8)/9 - 
      (2*(4*f148 - f164)*fvu2u1u10)/9 + 
      ((8*f148 - 3*f164)*tci11^2)/27 + (f166*tci12)/9 - 
      (2*(4*f148 - f164)*fvu1u2u8*tci12)/9 + 
      fvu1u1u10*((-6*f148 + 6*f155 - f173)/9 + 
        (f148*tci12)/3) + fvu1u1u8*((6*f45 - f46 - 6*f160)/9 + 
        ((-4*f148 - f164)*tci12)/9) + 
      fvu1u1u9*(f166/9 - (f164*tci12)/3)) + 
    fvu1u1u8*((2*f3 - 16*f4 + 2*f5 - 8*f7 + f8 - f17 - 
        32*f26 + 4*f29 - 16*f32 + 2*f33 + 8*f34 - f36 + 
        36*f58 - 13*f59 - 4*f60 + 2*f61 - 16*f63 + 2*f66 + 
        40*f71 - 16*f72 - 5*f74 + 2*f75 - 8*f78 + f81 - 
        16*f104 + 2*f105 + 16*f117 - 2*f118 + 2*f120 - 32*f123 + 
        4*f124 - 16*f125 + 2*f127 - 16*f129 - 16*f130 + 2*f131 + 
        2*f132 + 16*f134 - 2*f136 - 16*f149 - 16*f150 + 16*f151 + 
        16*f153 - 16*f154 - 4*f159 - 16*f175 + 2*f176 + 2*f177 + 
        16*f178 - 2*f179)/18 + (f164*fvu1u1u9^2)/9 + 
      ((6*f111 - f112 - 6*f162)*fvu1u1u10)/9 + 
      (4*f148*fvu2u1u8)/9 + (8*f164*fvu2u1u9)/9 - 
      (2*(4*f148 - f164)*fvu2u1u10)/9 + (4*f164*fvu2u1u11)/9 + 
      ((4*f148 + 3*f164)*tci11^2)/54 + 
      ((-f149 - f150 + f151 + f153 - f154)*tci12)/3 - 
      (2*(4*f148 - f164)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*(-f166/9 - (2*f164*tci12)/9)) + 
    fvu1u1u4*((f164*fvu1u1u5^2)/18 + (4*f148*fvu1u1u6^2)/9 + 
      (5*f148*fvu1u1u7^2)/18 + (2*f164*fvu1u1u8^2)/9 + 
      (7*f164*fvu1u1u9^2)/18 + (f148*fvu1u1u10^2)/6 + 
      (2*f164*fvu2u1u2)/9 + (2*f164*fvu2u1u3)/3 - 
      (2*f148*fvu2u1u6)/3 + (4*f148*fvu2u1u8)/9 + 
      (4*f164*fvu2u1u11)/9 + ((-9*f148 - 2*f164)*tci11^2)/54 - 
      (7*f164*fvu1u1u9*tci12)/9 - (f148*fvu1u1u10*tci12)/3 + 
      (2*f148*fvu1u2u3*tci12)/3 + (2*f164*fvu1u2u6*tci12)/3 + 
      fvu1u1u7*((2*f148*fvu1u1u8)/9 - (f148*fvu1u1u10)/3 - 
        (5*f148*tci12)/9) + fvu1u1u6*((-2*f148*fvu1u1u7)/9 - 
        (2*f148*fvu1u1u8)/9 - (4*f148*tci12)/9) + 
      fvu1u1u8*((2*f164*fvu1u1u9)/9 - (4*f164*tci12)/9) + 
      fvu1u1u5*(-(f164*fvu1u1u9)/3 - (f164*tci12)/9)) + 
    fvu1u1u2*((-4*f164*fvu1u1u4^2)/9 - (2*f164*fvu1u1u5^2)/9 - 
      (f164*fvu1u1u8^2)/9 - (5*f164*fvu1u1u9^2)/9 + 
      (2*f164*fvu2u1u3)/3 + (2*f164*fvu2u1u4)/9 + 
      (10*f164*fvu2u1u11)/9 - (2*f164*tci11^2)/27 - 
      (2*f164*fvu1u1u5*tci12)/9 + (4*f164*fvu1u1u9*tci12)/9 + 
      (2*f164*fvu1u2u6*tci12)/3 + fvu1u1u4*
       ((2*f164*fvu1u1u5)/9 - (2*f164*fvu1u1u8)/9 - 
        (4*f164*fvu1u1u9)/9 + (2*f164*tci12)/9) + 
      fvu1u1u8*((-2*f164*fvu1u1u9)/9 + (2*f164*tci12)/9)) + 
    fvu1u1u5*(-(f148*fvu1u1u6^2)/3 + (f164*fvu1u1u7^2)/6 + 
      ((-2*f148 + f164)*fvu1u1u8^2)/18 + (f164*fvu1u1u9^2)/6 + 
      (2*f164*fvu2u1u4)/9 - (f164*fvu2u1u5)/3 + 
      (2*(2*f148 - f164)*fvu2u1u7)/9 + 
      ((-16*f148 + 13*f164)*tci11^2)/108 + 
      ((2*f148 - f164)*fvu1u1u8*tci12)/9 + 
      (f164*fvu1u1u9*tci12)/3 + (f164*fvu1u2u7*tci12)/3 + 
      fvu1u1u6*((2*f148*fvu1u1u8)/3 - (2*f148*tci12)/3) + 
      fvu1u1u7*(((-4*f148 - f164)*fvu1u1u8)/9 + 
        ((4*f148 + f164)*tci12)/9)) + 
    fvu1u1u1*((-4*f164*fvu1u1u2^2)/9 - (f164*fvu1u1u3^2)/6 - 
      (2*f164*fvu1u1u4^2)/9 - (f164*fvu1u1u5^2)/6 - 
      (4*f148*fvu1u1u6^2)/9 + ((4*f148 - 3*f164)*fvu1u1u7^2)/18 - 
      (7*f164*fvu1u1u8^2)/18 + (f164*fvu1u1u9^2)/18 + 
      (2*f164*fvu2u1u2)/9 - (4*f148*fvu2u1u8)/9 + 
      (8*f164*fvu2u1u9)/9 - (4*f164*fvu2u1u11)/9 + 
      (10*f164*tci11^2)/27 + (5*f164*fvu1u1u9*tci12)/9 + 
      fvu1u1u6*((2*f148*fvu1u1u7)/9 + (2*f148*fvu1u1u8)/9 - 
        (2*f148*tci12)/9) + fvu1u1u2*((2*f164*fvu1u1u4)/9 + 
        (2*f164*fvu1u1u8)/9 + (2*f164*fvu1u1u9)/9 - 
        (2*f164*tci12)/9) + fvu1u1u3*((f164*fvu1u1u4)/3 + 
        (f164*tci12)/3) + fvu1u1u4*((2*f148*fvu1u1u6)/9 - 
        (2*f148*fvu1u1u7)/9 - (f164*fvu1u1u8)/9 - 
        (2*f164*fvu1u1u9)/9 + (4*f164*tci12)/9) + 
      fvu1u1u8*((-2*f164*fvu1u1u9)/9 + (7*f164*tci12)/9) + 
      fvu1u1u7*((-2*f148*fvu1u1u8)/9 + (f164*fvu1u1u9)/3 + 
        (2*(f148 + 3*f164)*tci12)/9)) + 
    (29*f164*tci12*tcr11^2)/18 - (f164*tcr11^3)/2 + 
    ((-8*f148 + 3*f160 - 3*f162 + 16*f164)*tci11^2*tcr12)/
     18 + (26*f164*tci12*tcr21)/9 + 
    tcr11*((-4*f164*tci11^2)/27 - (2*f164*tci12*tcr12)/
       3 - (10*f164*tcr21)/9) + 
    ((12*f148 + 12*f158 + 9*f160 - 9*f162 - 26*f164)*tcr33)/36;
L ieu1ueu10 = (-f17 + f20 + 10*f59 - f61 + f120 + 
      10*f150 + f159)/9 + ((-2*f148 + f158 - f160)*fvu1u1u6^2)/
     6 + ((f160 - f164)*fvu1u1u7^2)/6 + 
    ((f162 + f164)*fvu1u1u8^2)/6 + ((f158 - f162)*fvu1u1u9^2)/6 + 
    fvu1u1u8*((-f149 - f150 + f151 + f153 - f154)/3 - 
      (f164*fvu1u1u9)/3 - (f162*fvu1u1u10)/3) + 
    ((-f148 - f160)*fvu2u1u8)/3 + ((f160 - f164)*fvu2u1u10)/3 + 
    ((f162 + f164)*fvu2u1u12)/3 + ((-f148 + f158)*fvu2u1u14)/3 + 
    ((f158 - f162)*fvu2u1u15)/3 + 
    ((-f148 - f158 + f162 + 4*f164)*tci11^2)/18 + 
    ((f149 + f150 - f151 - f153 + f154)*tci12)/3 + 
    ((f160 - f164)*fvu1u2u8*tci12)/3 + 
    ((-f162 - f164)*fvu1u2u9*tci12)/3 + 
    fvu1u1u6*(f154/3 + (f148*fvu1u1u7)/3 + (f160*fvu1u1u8)/3 - 
      (f158*fvu1u1u9)/3 + (f148*fvu1u1u10)/3 - 
      (f160*tci12)/3) + fvu1u1u10*(-f153/3 + 
      (f162*tci12)/3) + fvu1u1u7*(-f151/3 - (f160*fvu1u1u8)/3 + 
      (f164*fvu1u1u9)/3 - (f148*fvu1u1u10)/3 + 
      (f164*tci12)/3) + fvu1u1u9*(f149/3 + (f162*fvu1u1u10)/3 + 
      (f164*tci12)/3);
L ieu0ueu10 = f150/3;
L ieum1ueu10 = 0;
L ieum2ueu10 = 0;
L ieu2uou10 = (-2*f157*fvu4u28)/3 - (2*f157*fvu4u39)/3 + 
    (2*f157*fvu4u49)/9 - (2*f157*fvu4u51)/9 + (f157*fvu4u80)/12 - 
    (17*f157*fvu4u83)/12 - (3*f157*fvu4u91)/4 + (f157*fvu4u93)/12 + 
    (4*f157*fvu4u100)/9 - (7*f157*fvu4u102)/6 + (f157*fvu4u111)/4 + 
    (f157*fvu4u113)/2 + f157*fvu4u114 - (2*f157*fvu4u129)/3 + 
    (13*f157*fvu4u132)/3 - (7*f157*fvu4u139)/2 - 
    (2*f157*fvu4u141)/3 + (4*f157*fvu4u146)/9 + (f157*fvu4u148)/2 + 
    (2*f157*fvu4u174)/3 + (2*f157*fvu4u182)/3 - 
    (2*f157*fvu4u190)/9 + (2*f157*fvu4u192)/9 - (f157*fvu4u213)/12 - 
    (43*f157*fvu4u215)/12 + (17*f157*fvu4u219)/4 - 
    (f157*fvu4u221)/12 - (4*f157*fvu4u225)/9 - (f157*fvu4u233)/4 - 
    (f157*fvu4u234)/2 - f157*fvu4u235 + (14*f157*fvu4u244)/3 + 
    (2*f157*fvu4u246)/3 - (4*f157*fvu4u252)/3 - 
    (10*f157*fvu4u255)/9 - 2*f157*fvu4u273 + 2*f157*fvu4u277 + 
    fvu3u25*(-(f157*fvu1u1u3)/3 + (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u7)/3) + fvu3u71*((f157*fvu1u1u3)/3 - 
      (f157*fvu1u1u6)/3 + (f157*fvu1u1u7)/3) + 
    fvu3u63*((5*f157*fvu1u1u3)/2 + (2*f157*fvu1u1u5)/3 + 
      (13*f157*fvu1u1u6)/6 + (f157*fvu1u1u7)/3 - 
      (f157*fvu1u1u8)/3 - (2*f157*fvu1u1u9)/3 - 
      (5*f157*fvu1u1u10)/2) + fvu3u45*(-(f157*fvu1u1u3)/3 - 
      (2*f157*fvu1u1u5)/3 - (f157*fvu1u1u6)/3 + f157*fvu1u1u7 + 
      (2*f157*fvu1u1u9)/3 - (2*f157*fvu1u1u10)/3) + 
    fvu3u23*((f157*fvu1u1u1)/3 + (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u8)/3 - (f157*fvu1u1u10)/3) + 
    fvu3u43*(-(f157*fvu1u1u2)/3 - (f157*fvu1u1u4)/3 - 
      (f157*fvu1u1u6)/3 + (f157*fvu1u1u7)/3 + 
      (2*f157*fvu1u1u9)/3 - (f157*fvu1u1u10)/3) + 
    fvu3u62*(-(f157*fvu1u1u2)/3 - (f157*fvu1u1u4)/3 + 
      (2*f157*fvu1u1u5)/3 - (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u7)/3 + (f157*fvu1u1u10)/3) + 
    fvu3u70*(-(f157*fvu1u1u6)/3 + (f157*fvu1u1u8)/3 + 
      (f157*fvu1u1u10)/3) + (151*f157*tci11^3*tci12)/135 + 
    fvu3u78*((f157*fvu1u1u2)/3 - (f157*fvu1u1u3)/3 + 
      (f157*fvu1u1u4)/3 - (f157*fvu1u1u6)/3 + (f157*fvu1u1u8)/3 - 
      (2*f157*fvu1u1u9)/3 - (2*f157*tci12)/3) + 
    fvu3u80*((f157*fvu1u1u2)/3 + (f157*fvu1u1u3)/3 + 
      (f157*fvu1u1u4)/3 - (2*f157*fvu1u1u5)/3 - 
      (4*f157*fvu1u1u6)/3 + 2*f157*fvu1u1u7 - 
      (4*f157*fvu1u1u8)/3 + 2*f157*fvu1u1u9 + f157*fvu1u1u10 + 
      (4*f157*tci12)/3) + (2*f157*tci11^2*tci21)/9 + 
    fvu1u1u8*((-8*f157*tci11^3)/81 + (8*f157*tci12*tci21)/
       3) + tci12*((48*f157*tci31)/5 + 16*f157*tci32) + 
    (4*f157*tci41)/3 + ((35*f157*tci11^3)/108 - 
      (f157*tci12*tci21)/3 + 12*f157*tci31)*tcr11 + 
    (-(f157*tci11*tci12)/5 + 5*f157*tci21)*tcr11^2 - 
    (f157*tci11*tcr11^3)/4 + 
    fvu1u1u3*((41*f157*tci11^3)/324 + f157*tci12*tci21 + 
      12*f157*tci31 + 5*f157*tci21*tcr11 - 
      (f157*tci11*tcr11^2)/4) + 
    fvu1u1u6*((f157*tci11^3)/36 + (11*f157*tci12*tci21)/3 + 
      12*f157*tci31 + 5*f157*tci21*tcr11 - 
      (f157*tci11*tcr11^2)/4) + 
    fvu1u1u5*(-(f157*tci11^3)/45 + (16*f157*tci12*tci21)/
       9 + (16*f157*tci31)/5 + (4*f157*tci21*tcr11)/3 - 
      (f157*tci11*tcr11^2)/15) + 
    fvu1u1u7*((89*f157*tci11^3)/810 - (32*f157*tci12*tci21)/
       9 - (8*f157*tci31)/5 - (2*f157*tci21*tcr11)/3 + 
      (f157*tci11*tcr11^2)/30) + 
    fvu1u1u9*((23*f157*tci11^3)/135 - (52*f157*tci12*tci21)/
       9 - (16*f157*tci31)/5 - (4*f157*tci21*tcr11)/3 + 
      (f157*tci11*tcr11^2)/15) + 
    fvu1u1u10*((17*f157*tci11^3)/1620 - 
      (37*f157*tci12*tci21)/9 - (52*f157*tci31)/5 - 
      (13*f157*tci21*tcr11)/3 + (13*f157*tci11*tcr11^2)/
       60) + fvu1u1u1*((f157*fvu3u25)/3 + (f157*fvu3u43)/3 + 
      (f157*fvu3u45)/3 + (f157*fvu3u62)/3 - (13*f157*fvu3u63)/6 - 
      (f157*fvu3u70)/3 - (f157*fvu3u71)/3 + (f157*fvu3u78)/3 - 
      (2*f157*fvu3u80)/3 - (19*f157*tci11^3)/108 + 
      (f157*tci12*tci21)/3 - 12*f157*tci31 - 
      5*f157*tci21*tcr11 + (f157*tci11*tcr11^2)/4) - 
    (40*f157*tci12*tci21*tcr12)/3 - 
    4*f157*tci11*tci12*tcr12^2;
L ieu1uou10 = 
   2*f157*fvu3u80 + (4*f157*tci11^3)/27 - 
    4*f157*tci12*tci21;
L ieu0uou10 = 0;
L ieum1uou10 = 0;
L ieum2uou10 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou10+w^2*ieu1uou10+w^3*ieu0uou10+w^4*ieum1uou10+w^5*ieum2uou10;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou10a = K[w^1];
L ieu1uou10a = K[w^2];
L ieu0uou10a = K[w^3];
L ieum1uou10a = K[w^4];
L ieum2uou10a = K[w^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_odd.c> "%O"
#write <e10_odd.c> "return Eps5o2<T>("
#write <e10_odd.c> "%E", ieu2uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu0uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum2uou10a
#write <e10_odd.c> ");\n}"
L H=+u^1*ieu2ueu10+u^2*ieu1ueu10+u^3*ieu0ueu10+u^4*ieum1ueu10+u^5*ieum2ueu10;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu10a = H[u^1];
L ieu1ueu10a = H[u^2];
L ieu0ueu10a = H[u^3];
L ieum1ueu10a = H[u^4];
L ieum2ueu10a = H[u^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_even.c> "%O"
#write <e10_even.c> "return Eps5o2<T>("
#write <e10_even.c> "%E", ieu2ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu0ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum2ueu10a
#write <e10_even.c> ");\n}"
.end
