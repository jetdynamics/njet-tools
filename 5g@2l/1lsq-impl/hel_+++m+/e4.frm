#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f61;
S f70;
S f9;
S f3;
S f17;
S f14;
S f12;
S f11;
S f29;
S f22;
S f20;
S f26;
S f24;
S f39;
S f48;
S f45;
S f32;
S f33;
S f46;
S f41;
S f35;
S f43;
S f37;
S f58;
S f59;
S f53;
S f50;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u7;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u2;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu4 = (-11*f3 + 63*f17 + 11*f20 + 11*f58 + 
      99*f59 - 11*f61 + 11*f70)/27 - (f37*fvu3u1)/3 + 
    (f37*fvu3u2)/6 - (f37*fvu3u4)/18 - (f37*fvu3u6)/9 + 
    ((-3*f9 + f37)*fvu3u12)/18 + (f37*fvu3u14)/9 - 
    (f37*fvu3u19)/9 + (f37*fvu3u31)/9 + ((f37 - 3*f45)*fvu3u32)/
     18 + (f48*fvu3u33)/6 + (f41*fvu3u35)/6 - (2*f37*fvu3u36)/9 - 
    (f37*fvu3u37)/9 - (f37*fvu3u38)/6 + (f37*fvu3u40)/6 + 
    (f37*fvu3u42)/3 - (f37*fvu1u1u1^3)/18 + (f37*fvu1u1u3^3)/18 - 
    (f37*fvu1u1u5^3)/3 - (f37*fvu1u1u6^3)/9 + 
    (f26*fvu1u1u8^2)/6 + fvu1u1u4^2*((3*f22 + f43 + f50)/18 + 
      (f37*fvu1u1u8)/18) + ((-6*f9 + 6*f12 - f43 - f53)*
      fvu2u1u2)/9 + ((f43 + f50)*fvu2u1u6)/9 + 
    ((f46 - f50)*fvu2u1u8)/9 + ((f39 - f46)*fvu2u1u10)/9 - 
    (f37*fvu1u2u7*tci11^2)/3 + ((f24 + f29 + f33)*tci12)/
     9 + ((-f43 - f50)*fvu1u2u3*tci12)/9 + 
    ((f39 - f46)*fvu1u2u8*tci12)/9 + (4*f37*fvu2u1u1*tci12)/
     9 + (f37*fvu2u1u5*tci12)/3 + 
    fvu1u1u8*(-f29/9 + (2*f37*fvu2u1u9)/9 + (f37*tci11^2)/54 - 
      (f26*tci12)/3) + fvu1u1u3^2*(-(f37*fvu1u1u4)/18 + 
      (f37*fvu1u1u7)/6 - (f37*tci12)/2) + 
    fvu2u1u9*((-6*f9 + 6*f12 + f39 - f53)/9 - 
      (4*f37*tci12)/9) + fvu1u1u5^2*((f37*fvu1u1u6)/6 - 
      (f37*fvu1u1u7)/18 + (f37*fvu1u1u8)/9 - (f37*tci12)/9) + 
    fvu1u1u6^2*((-3*f11 + 3*f17 - 3*f22 - 3*f26 - 3*f32 + f46 - 
        f50)/18 - (f37*fvu1u1u7)/9 + (f37*fvu1u1u8)/18 - 
      (f37*tci12)/18) + fvu1u1u7^2*((3*f11 + f39 - f46)/18 - 
      (f37*fvu1u1u8)/6 + (f37*tci12)/6) + 
    fvu1u1u1^2*((-12*f9 + 12*f12 + 3*f32 + f39 - f43 - 2*f53)/
       18 - (f37*fvu1u1u3)/18 - (f37*fvu1u1u7)/6 + 
      (f37*fvu1u1u8)/18 + (f37*tci12)/6) + 
    tci11^2*((-12*f9 + 12*f12 + 3*f17 + 18*f22 + 18*f26 + 
        18*f32 + 2*f39 + 2*f50 - 2*f53)/108 + 
      ((-2*f9 + 4*f37 - 3*f41 + f45 + f48)*tci12)/36) + 
    fvu1u1u3*(-(f37*fvu1u1u4^2)/18 + (f37*fvu1u1u5^2)/6 - 
      (2*f37*fvu2u1u1)/9 - (f37*fvu2u1u5)/3 + 
      (35*f37*tci11^2)/54 + (f37*fvu1u1u4*tci12)/9 - 
      (f37*fvu1u1u5*tci12)/3 - (f37*fvu1u1u7*tci12)/3 + 
      (f37*fvu1u2u7*tci12)/3) + 
    fvu1u1u6*((f3 + f14 - 9*f17 - f20 + f24 + f29 + f33 - 
        f58 - 9*f59 + f61 - f70)/9 + (f37*fvu1u1u8^2)/18 - 
      (2*f37*fvu2u1u8)/9 + (2*f37*tci11^2)/27 + 
      (f46*tci12)/9 + fvu1u1u7*(f50/9 + (2*f37*fvu1u1u8)/9 - 
        (2*f37*tci12)/9) + fvu1u1u8*(-f46/9 - 
        (f37*tci12)/9)) + fvu1u1u4*(-f24/9 - (f43*fvu1u1u6)/9 - 
      (f50*fvu1u1u7)/9 + (f37*fvu1u1u8^2)/18 - (f22*tci12)/3 + 
      fvu1u1u8*((-6*f9 + 6*f12 - f53)/9 - (f37*tci12)/9)) + 
    fvu1u1u1*(-f33/9 + (f37*fvu1u1u3^2)/18 - (f37*fvu1u1u5^2)/6 + 
      (f43*fvu1u1u6)/9 - (f37*fvu1u1u8^2)/18 + 
      fvu1u1u4*((6*f9 - 6*f12 + f53)/9 - (f37*fvu1u1u8)/9) - 
      (2*f37*fvu2u1u1)/9 + (2*f37*fvu2u1u9)/9 - 
      (5*f37*tci11^2)/18 + ((-3*f32 - f39 + f43)*tci12)/9 + 
      fvu1u1u3*((f37*fvu1u1u4)/9 - (f37*tci12)/9) + 
      fvu1u1u8*((6*f9 - 6*f12 + f53)/9 + (f37*tci12)/9) + 
      fvu1u1u7*(-f39/9 + (f37*tci12)/3)) + 
    fvu1u1u7*(-f14/9 - (f37*fvu1u1u8^2)/6 - (4*f37*fvu2u1u7)/9 - 
      (2*f37*fvu2u1u8)/9 - (f37*tci11^2)/9 + (f50*tci12)/9 + 
      fvu1u1u8*(f46/9 + (f37*tci12)/3)) + 
    fvu1u1u5*((f37*fvu1u1u6^2)/6 + (f37*fvu1u1u7^2)/6 + 
      (f37*fvu1u1u8^2)/9 - (f37*fvu2u1u5)/3 - 
      (4*f37*fvu2u1u7)/9 + (13*f37*tci11^2)/108 - 
      (2*f37*fvu1u1u8*tci12)/9 + (f37*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*((f37*fvu1u1u8)/9 - (f37*tci12)/9) + 
      fvu1u1u6*(-(f37*fvu1u1u8)/3 + (f37*tci12)/3)) + 
    ((-2*f9 + 2*f37 - 3*f41)*tci12*tcr11^2)/6 + 
    ((4*f9 + f41)*tcr11^3)/18 + 
    ((2*f9 + 19*f37 - 7*f41 - 6*f45 - 6*f48)*tci11^2*tcr12)/
     36 + ((-2*f9 - f37 + f41)*tcr12^3)/9 - 
    (2*(f9 - f37 + f41)*tci12*tcr21)/3 + 
    tcr11*(((3*f37 + 7*f41)*tci11^2)/36 + 
      ((-f37 + f41)*tci12*tcr12)/3 + (2*f9*tcr21)/3) + 
    ((2*f9 + f37 - f41)*tcr31)/3 + 
    ((2*f9 + f37 - f41)*tcr32)/12 + 
    ((-74*f9 - 11*f37 + 11*f41 - 18*f45 - 18*f48)*tcr33)/72;
L ieu1ueu4 = (-f3 + 9*f17 + f20 + f58 + 9*f59 - f61 + 
      f70)/9 + ((-2*f9 + f37 - f41)*fvu1u1u1^2)/6 + 
    ((f41 + f48)*fvu1u1u4^2)/6 + ((f45 - f48)*fvu1u1u6^2)/6 + 
    ((f37 - f45)*fvu1u1u7^2)/6 - (f26*fvu1u1u8)/3 + 
    fvu1u1u4*(-f22/3 - (f41*fvu1u1u6)/3 - (f48*fvu1u1u7)/3 - 
      (f9*fvu1u1u8)/3) + ((-f9 - f41)*fvu2u1u2)/3 + 
    ((f41 + f48)*fvu2u1u6)/3 + ((f45 - f48)*fvu2u1u8)/3 + 
    ((-f9 + f37)*fvu2u1u9)/3 + ((f37 - f45)*fvu2u1u10)/3 + 
    ((-f9 + f37 + f48)*tci11^2)/18 + 
    ((f22 + f26 + f32)*tci12)/3 + 
    ((-f41 - f48)*fvu1u2u3*tci12)/3 + 
    ((f37 - f45)*fvu1u2u8*tci12)/3 + 
    fvu1u1u1*(-f32/3 + (f9*fvu1u1u4)/3 + (f41*fvu1u1u6)/3 - 
      (f37*fvu1u1u7)/3 + (f9*fvu1u1u8)/3 + 
      ((-f37 + f41)*tci12)/3) + 
    fvu1u1u6*((f11 - f17 + f22 + f26 + f32)/3 + 
      (f48*fvu1u1u7)/3 - (f45*fvu1u1u8)/3 + (f45*tci12)/3) + 
    fvu1u1u7*(-f11/3 + (f45*fvu1u1u8)/3 + (f48*tci12)/3);
L ieu0ueu4 = f17/3;
L ieum1ueu4 = 0;
L ieum2ueu4 = 0;
L ieu2uou4 = -(f35*fvu4u25)/3 + (7*f35*fvu4u28)/3 + 
    (f35*fvu4u39)/3 - (f35*fvu4u41)/3 + (f35*fvu4u49)/9 + 
    (f35*fvu4u51)/3 + (f35*fvu4u80)/3 - (11*f35*fvu4u83)/3 + 
    3*f35*fvu4u91 + (f35*fvu4u93)/3 - (f35*fvu4u100)/9 - 
    (7*f35*fvu4u102)/9 - (2*f35*fvu4u139)/3 + (2*f35*fvu4u148)/9 + 
    (5*f35*fvu4u171)/12 - (3*f35*fvu4u174)/4 - (11*f35*fvu4u182)/4 + 
    (5*f35*fvu4u184)/12 + (f35*fvu4u190)/3 - (17*f35*fvu4u192)/18 + 
    (f35*fvu4u199)/4 + (f35*fvu4u201)/2 + f35*fvu4u202 + 
    (2*f35*fvu4u213)/3 - (2*f35*fvu4u215)/3 + (2*f35*fvu4u221)/3 + 
    3*f35*fvu4u244 + (8*f35*fvu4u246)/3 - 2*f35*fvu4u250 - 
    (13*f35*fvu4u252)/6 - (f35*fvu4u255)/2 - (5*f35*fvu4u271)/12 - 
    (43*f35*fvu4u273)/12 + (19*f35*fvu4u277)/12 - 
    (5*f35*fvu4u279)/12 - (f35*fvu4u282)/3 - (f35*fvu4u289)/4 - 
    (f35*fvu4u290)/2 - f35*fvu4u291 - (2*f35*fvu4u293)/3 - 
    (2*f35*fvu4u295)/3 - 2*f35*fvu4u309 + 2*f35*fvu4u313 + 
    fvu3u43*((f35*fvu1u1u6)/6 - (f35*fvu1u1u7)/6 - 
      (f35*fvu1u1u9)/6) + fvu3u23*((f35*fvu1u1u2)/6 + 
      (f35*fvu1u1u6)/6 - (f35*fvu1u1u7)/6 - (f35*fvu1u1u9)/6) + 
    fvu3u63*((f35*fvu1u1u6)/3 + (f35*fvu1u1u7)/3 + 
      (f35*fvu1u1u9)/3 - (2*f35*fvu1u1u10)/3) + 
    fvu3u70*((f35*fvu1u1u2)/6 - (f35*fvu1u1u3)/6 + 
      (f35*fvu1u1u6)/3 - (f35*fvu1u1u8)/3 - (f35*fvu1u1u9)/6 - 
      (f35*fvu1u1u10)/3) + fvu3u25*((-7*f35*fvu1u1u1)/6 - 
      (f35*fvu1u1u2)/3 + (f35*fvu1u1u3)/6 + f35*fvu1u1u4 - 
      (2*f35*fvu1u1u5)/3 - (f35*fvu1u1u6)/2 + 
      (7*f35*fvu1u1u7)/6 + (f35*fvu1u1u10)/3) + 
    fvu3u81*((f35*fvu1u1u3)/6 + (f35*fvu1u1u4)/3 + 
      (f35*fvu1u1u6)/6 - (f35*fvu1u1u7)/6 - (f35*fvu1u1u9)/3 + 
      (f35*fvu1u1u10)/3) - (13*f35*tci11^3*tci12)/15 + 
    fvu3u71*(-(f35*fvu1u1u2)/3 + f35*fvu1u1u4 - 
      (2*f35*fvu1u1u5)/3 - f35*fvu1u1u7 + (f35*fvu1u1u8)/3 - 
      2*f35*tci12) + fvu3u82*((f35*fvu1u1u3)/3 + 
      (f35*fvu1u1u4)/3 - (f35*fvu1u1u7)/3 + (f35*fvu1u1u8)/3 - 
      (f35*fvu1u1u9)/3 - (2*f35*tci12)/3) + 
    fvu3u78*((f35*fvu1u1u2)/3 - (f35*fvu1u1u3)/3 - 
      (f35*fvu1u1u4)/3 + (f35*fvu1u1u6)/3 - (f35*fvu1u1u8)/3 + 
      (2*f35*tci12)/3) + fvu3u80*((-2*f35*fvu1u1u2)/3 - 
      (f35*fvu1u1u3)/3 + (2*f35*fvu1u1u4)/3 + 
      (2*f35*fvu1u1u5)/3 - (11*f35*fvu1u1u6)/6 + 
      (7*f35*fvu1u1u7)/6 - (3*f35*fvu1u1u8)/2 + 
      (13*f35*fvu1u1u9)/6 + (3*f35*tci12)/2) + 
    fvu3u45*((f35*fvu1u1u2)/2 + (f35*fvu1u1u3)/6 - 
      f35*fvu1u1u4 + (2*f35*fvu1u1u5)/3 - (7*f35*fvu1u1u6)/3 + 
      (5*f35*fvu1u1u7)/3 - 2*f35*fvu1u1u8 - (f35*fvu1u1u9)/6 - 
      (f35*fvu1u1u10)/3 + 2*f35*tci12) + 
    (10*f35*tci11^2*tci21)/27 + 
    fvu1u1u10*((-4*f35*tci11^3)/81 + (4*f35*tci12*tci21)/
       3) + tci12*((-16*f35*tci31)/5 - 16*f35*tci32) + 
    (2072*f35*tci41)/135 + (64*f35*tci42)/5 + 
    (16*f35*tci43)/3 + ((-217*f35*tci11^3)/405 - 
      (4*f35*tci12*tci21)/3 - 16*f35*tci32)*tcr11 - 
    (13*f35*tci11*tcr11^3)/45 + 
    fvu1u1u6*((-97*f35*tci11^3)/1620 + (49*f35*tci12*tci21)/
       9 + (52*f35*tci31)/5 + (13*f35*tci21*tcr11)/3 - 
      (13*f35*tci11*tcr11^2)/60) + 
    fvu1u1u4*((11*f35*tci11^3)/135 + (4*f35*tci12*tci21)/
       3 + (48*f35*tci31)/5 + 4*f35*tci21*tcr11 - 
      (f35*tci11*tcr11^2)/5) + 
    fvu1u1u8*((-4*f35*tci11^3)/135 + (13*f35*tci12*tci21)/
       3 + (48*f35*tci31)/5 + 4*f35*tci21*tcr11 - 
      (f35*tci11*tcr11^2)/5) + 
    fvu1u1u9*((91*f35*tci11^3)/540 - (11*f35*tci12*tci21)/
       3 + (12*f35*tci31)/5 + f35*tci21*tcr11 - 
      (f35*tci11*tcr11^2)/20) + 
    fvu1u1u7*((229*f35*tci11^3)/1620 - (37*f35*tci12*tci21)/
       9 - (4*f35*tci31)/5 - (f35*tci21*tcr11)/3 + 
      (f35*tci11*tcr11^2)/60) + 
    fvu1u1u2*((f35*fvu3u43)/6 - (f35*fvu3u63)/3 - 
      (137*f35*tci11^3)/1620 + (2*f35*tci12*tci21)/9 - 
      (28*f35*tci31)/5 - (7*f35*tci21*tcr11)/3 + 
      (7*f35*tci11*tcr11^2)/60) + 
    fvu1u1u5*((2*f35*tci11^3)/45 - (32*f35*tci12*tci21)/9 - 
      (32*f35*tci31)/5 - (8*f35*tci21*tcr11)/3 + 
      (2*f35*tci11*tcr11^2)/15) + 
    fvu1u1u1*((5*f35*fvu3u45)/6 + (f35*fvu3u70)/2 + 
      (2*f35*fvu3u71)/3 + (f35*fvu3u78)/3 - (f35*fvu3u80)/3 - 
      (f35*fvu3u81)/2 - (f35*fvu3u82)/3 - (53*f35*tci11^3)/405 - 
      (48*f35*tci31)/5 - 4*f35*tci21*tcr11 + 
      (f35*tci11*tcr11^2)/5) + 
    ((218*f35*tci11^3)/243 + (40*f35*tci12*tci21)/3)*
     tcr12 + (4*f35*tci11*tci12 + (40*f35*tci21)/3)*
     tcr12^2 + tcr11^2*((f35*tci11*tci12)/15 + 
      2*f35*tci21 + 2*f35*tci11*tcr12) - 
    (130*f35*tci11*tcr33)/27;
L ieu1uou4 = 
   -2*f35*fvu3u45 + (11*f35*tci11^3)/135 + 
    (4*f35*tci12*tci21)/3 + (48*f35*tci31)/5 + 
    4*f35*tci21*tcr11 - (f35*tci11*tcr11^2)/5;
L ieu0uou4 = 0;
L ieum1uou4 = 0;
L ieum2uou4 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou4+w^2*ieu1uou4+w^3*ieu0uou4+w^4*ieum1uou4+w^5*ieum2uou4;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou4a = K[w^1];
L ieu1uou4a = K[w^2];
L ieu0uou4a = K[w^3];
L ieum1uou4a = K[w^4];
L ieum2uou4a = K[w^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_odd.c> "%O"
#write <e4_odd.c> "return Eps5o2<T>("
#write <e4_odd.c> "%E", ieu2uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu0uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum2uou4a
#write <e4_odd.c> ");\n}"
L H=+u^1*ieu2ueu4+u^2*ieu1ueu4+u^3*ieu0ueu4+u^4*ieum1ueu4+u^5*ieum2ueu4;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu4a = H[u^1];
L ieu1ueu4a = H[u^2];
L ieu0ueu4a = H[u^3];
L ieum1ueu4a = H[u^4];
L ieum2ueu4a = H[u^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_even.c> "%O"
#write <e4_even.c> "return Eps5o2<T>("
#write <e4_even.c> "%E", ieu2ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu0ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum2ueu4a
#write <e4_even.c> ");\n}"
.end
