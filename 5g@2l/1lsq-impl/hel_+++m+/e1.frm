#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f66;
S f64;
S f63;
S f132;
S f61;
S f130;
S f60;
S f68;
S f124;
S f74;
S f76;
S f126;
S f120;
S f70;
S f121;
S f71;
S f122;
S f123;
S f128;
S f78;
S f79;
S f159;
S f8;
S f18;
S f81;
S f3;
S f17;
S f14;
S f15;
S f82;
S f7;
S f85;
S f87;
S f11;
S f179;
S f178;
S f95;
S f93;
S f90;
S f22;
S f20;
S f177;
S f27;
S f176;
S f175;
S f25;
S f98;
S f24;
S f99;
S f34;
S f36;
S f58;
S f59;
S f56;
S f104;
S f54;
S f105;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u132;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu4u113;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu1 = (36*f58 - 11*f60)/27 - (f85*fvu3u1)/3 + 
    ((f85 - f90)*fvu3u2)/6 + (f85*fvu3u3)/9 + 
    ((-2*f85 + 3*f95)*fvu3u4)/18 + (f85*fvu3u5)/18 - 
    (2*f85*fvu3u6)/9 - (2*f85*fvu3u11)/9 - (f85*fvu3u12)/18 + 
    ((-3*f54 - 2*f85)*fvu3u13)/18 - (f85*fvu3u14)/9 + 
    ((-f79 - f85)*fvu3u15)/6 + (f85*fvu3u17)/6 - (f85*fvu3u18)/6 - 
    (f85*fvu3u19)/9 + (f85*fvu3u22)/3 - (7*f85*fvu1u1u1^3)/18 + 
    (f85*fvu1u1u2^3)/3 + (f85*fvu1u1u3^3)/18 - 
    (f85*fvu1u1u5^3)/9 + fvu1u1u4^2*(-f63/6 + (f85*fvu1u1u5)/9 - 
      (f85*fvu1u1u8)/18 + (f85*fvu1u1u9)/18) + 
    ((f87 + f98)*fvu2u1u2)/9 + ((f87 + f99)*fvu2u1u3)/9 + 
    ((f82 + f99)*fvu2u1u4)/9 + fvu1u1u8*((-2*f85*fvu2u1u9)/9 - 
      (f85*tci11^2)/54) + fvu1u1u9*((2*f85*fvu2u1u11)/9 + 
      (2*f85*tci11^2)/27) + 
    ((8*f11 - f14 - 8*f15 + f18 - f60 + 8*f68 - f74 - 
       8*f130 + f132)*tci12)/9 - (f85*fvu1u1u9^2*tci12)/18 + 
    ((f87 + f99)*fvu1u2u6*tci12)/9 + (4*f85*fvu2u1u9*tci12)/
     9 + fvu1u1u3^2*((-3*f56 + f82 + f93)/18 - (f85*fvu1u1u4)/9 - 
      (f85*fvu1u1u8)/6 - (f85*tci12)/2) + 
    fvu1u1u5^2*(-f68/6 + (f85*fvu1u1u9)/6 - (f85*tci12)/9) + 
    fvu1u1u2^2*((3*f71 + f82 + f87 + 2*f99)/18 - 
      (f85*fvu1u1u4)/6 - (f85*fvu1u1u5)/18 - (f85*fvu1u1u9)/9 + 
      (f85*tci12)/6) + fvu2u1u5*((f82 + f93)/9 + 
      (f85*tci12)/3) + fvu1u1u1^2*
     ((3*f56 - 3*f58 + 3*f63 + 3*f68 - 3*f71 + f87 - f93 + 
        2*f98)/18 - (f85*fvu1u1u2)/6 + (f85*fvu1u1u3)/18 + 
      (f85*fvu1u1u4)/6 + (f85*fvu1u1u8)/9 + (5*f85*tci12)/6) + 
    fvu2u1u1*((-f93 + f98)/9 + (8*f85*tci12)/9) + 
    fvu1u2u7*(-(f85*tci11^2)/3 + ((-f82 - f93)*tci12)/9) + 
    tci11^2*((-21*f58 + 18*f68 - 18*f71 + 2*f82 + 2*f87 + 
        2*f98)/108 + ((-f54 + f79 + 4*f85 - 3*f90 + 2*f95)*
        tci12)/36) + fvu1u1u5*
     ((8*f11 - f14 - 8*f15 + f18 + 8*f68 - 8*f130 + f132)/9 + 
      (f85*fvu1u1u9^2)/6 + (4*f85*fvu2u1u4)/9 - 
      (f85*fvu2u1u5)/3 + (13*f85*tci11^2)/108 - 
      (f99*tci12)/9 + (f85*fvu1u1u9*tci12)/3 + 
      (f85*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*(f66/9 + (f85*fvu1u1u5^2)/9 - (f85*fvu1u1u8^2)/18 + 
      (f85*fvu1u1u9^2)/18 + (f63*tci12)/3 + 
      (f85*fvu1u1u8*tci12)/9 - (f85*fvu1u1u9*tci12)/9 + 
      fvu1u1u5*(f99/9 - (f85*fvu1u1u9)/3 - (2*f85*tci12)/9)) + 
    fvu1u1u3*((-2*f3 - 8*f7 + f8 + 16*f15 - f17 - 2*f18 + 
        2*f20 + 16*f22 - 2*f24 + 16*f25 - 2*f27 + 8*f34 - f36 + 
        16*f56 + 2*f58 + 19*f59 - 2*f61 + 16*f63 + 16*f64 - 
        2*f66 - 2*f67 + 2*f70 - 8*f71 + f74 + 8*f78 - f81 - 
        16*f104 + 2*f105 + 2*f120 - 16*f121 + 2*f122 + 16*f123 - 
        2*f124 - 16*f126 + 2*f128 + 2*f159 + 16*f175 - 2*f176 + 
        2*f177 - 16*f178 + 2*f179)/18 - (f85*fvu1u1u4^2)/9 + 
      (f85*fvu1u1u5^2)/6 - (f85*fvu1u1u8^2)/6 - 
      (4*f85*fvu2u1u1)/9 - (f85*fvu2u1u5)/3 + 
      (17*f85*tci11^2)/27 + (f56*tci12)/3 + 
      (f85*fvu1u1u8*tci12)/3 + (f85*fvu1u2u7*tci12)/3 + 
      fvu1u1u5*(-f93/9 - (f85*tci12)/3) + 
      fvu1u1u4*(f98/9 + (f85*fvu1u1u8)/3 + (2*f85*tci12)/9)) + 
    fvu1u1u1*((2*f3 + 8*f7 - f8 - 16*f11 + 2*f14 + f17 - 
        2*f20 - 16*f22 + 2*f24 - 16*f25 + 2*f27 - 8*f34 + f36 - 
        16*f56 - 2*f58 - 19*f59 + 2*f60 + 2*f61 - 16*f63 - 
        16*f64 + 2*f67 - 16*f68 - 2*f70 + 8*f71 + f74 - 8*f78 + 
        f81 + 16*f104 - 2*f105 - 2*f120 + 16*f121 - 2*f122 - 
        16*f123 + 2*f124 + 16*f126 - 2*f128 + 16*f130 - 2*f132 - 
        2*f159 - 16*f175 + 2*f176 - 2*f177 + 16*f178 - 2*f179)/
       18 + (5*f85*fvu1u1u3^2)/18 + (f85*fvu1u1u4^2)/6 + 
      (f93*fvu1u1u5)/9 - (f85*fvu1u1u5^2)/6 + 
      (2*f85*fvu1u1u8^2)/9 - (4*f85*fvu2u1u1)/9 - 
      (2*f85*fvu2u1u9)/9 - (f85*tci11^2)/3 + 
      ((-3*f56 + 3*f58 - 3*f63 - 3*f68 + 3*f71 - f87 + f93)*
        tci12)/9 - (4*f85*fvu1u1u8*tci12)/9 + 
      fvu1u1u3*(-f98/9 - (f85*fvu1u1u4)/9 - (5*f85*tci12)/9) + 
      fvu1u1u4*(-f98/9 - (2*f85*fvu1u1u8)/9 - (f85*tci12)/3) + 
      fvu1u1u2*(-f87/9 + (f85*tci12)/3)) + 
    fvu1u1u2*(-f74/9 + (f85*fvu1u1u3^2)/6 - (f85*fvu1u1u4^2)/6 - 
      (5*f85*fvu1u1u5^2)/18 - (2*f85*fvu1u1u9^2)/9 + 
      (4*f85*fvu2u1u4)/9 + (2*f85*fvu2u1u11)/9 - 
      (f85*tci11^2)/9 + (f82*tci12)/9 - 
      (2*f85*fvu1u1u9*tci12)/9 + fvu1u1u3*
       (-f82/9 - (f85*tci12)/3) + fvu1u1u5*
       (-f99/9 - (f85*tci12)/9) + fvu1u1u4*
       (-f99/9 + (f85*fvu1u1u5)/9 + (2*f85*fvu1u1u9)/9 + 
        (f85*tci12)/3)) + ((-2*f85 - 3*f90 + 2*f95)*tci12*
      tcr11^2)/6 + ((6*f85 + f90 - 4*f95)*tcr11^3)/18 + 
    ((6*f54 - 6*f79 + 19*f85 - 7*f90 - 2*f95)*tci11^2*
      tcr12)/36 + ((-f85 + f90 + 2*f95)*tcr12^3)/9 - 
    (2*(f85 + f90 - f95)*tci12*tcr21)/3 + 
    tcr11*(((5*f85 + 7*f90)*tci11^2)/36 + 
      ((-f85 + f90)*tci12*tcr12)/3 + 
      (2*(f85 - f95)*tcr21)/3) + 
    ((f85 - f90 - 2*f95)*tcr31)/3 + 
    ((f85 - f90 - 2*f95)*tcr32)/12 + 
    ((18*f54 - 18*f79 - 11*f85 + 11*f90 + 74*f95)*tcr33)/72;
L ieu1ueu1 = -f60/9 + ((f85 - f90 + 2*f95)*fvu1u1u1^2)/
     6 + ((2*f54 + f79 + f85)*fvu1u1u2^2)/6 + 
    ((f79 + f90)*fvu1u1u3^2)/6 + 
    fvu1u1u4*(f63/3 + (f54*fvu1u1u5)/3) + 
    fvu1u1u3*(f56/3 + (f95*fvu1u1u4)/3 - (f90*fvu1u1u5)/3) + 
    ((-f90 + f95)*fvu2u1u1)/3 + ((f85 + f95)*fvu2u1u2)/3 + 
    ((f54 + f85)*fvu2u1u3)/3 + ((f54 + f79)*fvu2u1u4)/3 + 
    ((f79 + f90)*fvu2u1u5)/3 + ((f79 + f85 + f95)*tci11^2)/
     18 + ((-f58 + f68 - f71)*tci12)/3 + 
    ((f54 + f85)*fvu1u2u6*tci12)/3 + 
    ((-f79 - f90)*fvu1u2u7*tci12)/3 + 
    fvu1u1u5*(f68/3 - (f54*tci12)/3) + 
    fvu1u1u2*(-f71/3 - (f79*fvu1u1u3)/3 - (f54*fvu1u1u4)/3 - 
      (f54*fvu1u1u5)/3 + (f79*tci12)/3) + 
    fvu1u1u1*((-f56 + f58 - f63 - f68 + f71)/3 - 
      (f85*fvu1u1u2)/3 - (f95*fvu1u1u3)/3 - (f95*fvu1u1u4)/3 + 
      (f90*fvu1u1u5)/3 + ((-f85 + f90)*tci12)/3);
L ieu0ueu1 = -f58/3;
L ieum1ueu1 = 0;
L ieum2ueu1 = 0;
L ieu2uou1 = (5*f76*fvu4u25)/3 - (f76*fvu4u28)/3 - 
    (7*f76*fvu4u39)/3 - (f76*fvu4u41)/3 - (f76*fvu4u49)/3 - 
    (5*f76*fvu4u51)/9 + (5*f76*fvu4u80)/12 - (37*f76*fvu4u83)/12 - 
    (7*f76*fvu4u91)/4 + (5*f76*fvu4u93)/12 + (f76*fvu4u100)/3 - 
    (23*f76*fvu4u102)/18 + (f76*fvu4u111)/4 + (f76*fvu4u113)/2 + 
    f76*fvu4u114 - (8*f76*fvu4u129)/3 + (7*f76*fvu4u132)/3 - 
    (f76*fvu4u139)/6 - (8*f76*fvu4u141)/3 + (4*f76*fvu4u146)/9 + 
    (f76*fvu4u148)/18 + (5*f76*fvu4u171)/12 - (f76*fvu4u174)/12 - 
    (f76*fvu4u182)/12 + (5*f76*fvu4u184)/12 + (f76*fvu4u190)/9 - 
    (f76*fvu4u192)/18 + (f76*fvu4u199)/4 + (f76*fvu4u201)/2 + 
    f76*fvu4u202 + (7*f76*fvu4u213)/12 - (f76*fvu4u215)/4 + 
    (9*f76*fvu4u219)/4 + (7*f76*fvu4u221)/12 - (4*f76*fvu4u225)/9 - 
    (f76*fvu4u233)/4 - (f76*fvu4u234)/2 - f76*fvu4u235 + 
    (17*f76*fvu4u244)/3 + (4*f76*fvu4u246)/3 - (7*f76*fvu4u252)/2 - 
    (17*f76*fvu4u255)/18 - (5*f76*fvu4u271)/12 - 
    (43*f76*fvu4u273)/12 + (67*f76*fvu4u277)/12 - 
    (5*f76*fvu4u279)/12 - (f76*fvu4u282)/3 - (f76*fvu4u289)/4 - 
    (f76*fvu4u290)/2 - f76*fvu4u291 + (4*f76*fvu4u293)/3 - 
    (2*f76*fvu4u295)/3 + 2*f76*fvu4u313 + 2*f76*fvu4u330 + 
    fvu3u71*(-(f76*fvu1u1u2)/3 + (f76*fvu1u1u3)/3 + 
      (f76*fvu1u1u5)/3 - (f76*fvu1u1u6)/3 + (f76*fvu1u1u7)/3 + 
      (f76*fvu1u1u8)/3) + fvu3u78*((2*f76*fvu1u1u2)/3 - 
      (2*f76*fvu1u1u3)/3 - (2*f76*fvu1u1u9)/3) + 
    fvu3u70*((f76*fvu1u1u2)/6 - (f76*fvu1u1u3)/6 - 
      (f76*fvu1u1u9)/6) + fvu3u45*(-(f76*fvu1u1u2)/2 - 
      (7*f76*fvu1u1u3)/6 - (2*f76*fvu1u1u6)/3 + 
      (2*f76*fvu1u1u7)/3 + (f76*fvu1u1u9)/2) + 
    fvu3u43*(-(f76*fvu1u1u2)/6 - (f76*fvu1u1u4)/3 - 
      (f76*fvu1u1u6)/6 + (f76*fvu1u1u7)/6 + (f76*fvu1u1u9)/2 - 
      (f76*fvu1u1u10)/3) + fvu3u63*((2*f76*fvu1u1u2)/3 + 
      (3*f76*fvu1u1u3)/2 - (f76*fvu1u1u5)/3 + (f76*fvu1u1u6)/2 - 
      (f76*fvu1u1u7)/3 - (f76*fvu1u1u8)/3 - (f76*fvu1u1u9)/3 - 
      (f76*fvu1u1u10)/6) + fvu3u25*(-(f76*fvu1u1u2)/3 - 
      (7*f76*fvu1u1u3)/6 + f76*fvu1u1u4 - (2*f76*fvu1u1u5)/3 + 
      (5*f76*fvu1u1u6)/6 - (f76*fvu1u1u7)/6 + 
      (f76*fvu1u1u10)/3) + fvu3u81*((f76*fvu1u1u3)/6 + 
      (f76*fvu1u1u4)/3 + (f76*fvu1u1u6)/6 - (f76*fvu1u1u7)/6 - 
      (f76*fvu1u1u9)/3 + (f76*fvu1u1u10)/3) + 
    fvu3u62*((-4*f76*fvu1u1u2)/3 + (2*f76*fvu1u1u4)/3 + 
      (2*f76*fvu1u1u5)/3 - (4*f76*fvu1u1u6)/3 + 
      (2*f76*fvu1u1u7)/3 + (4*f76*fvu1u1u10)/3) + 
    (13*f76*tci11^3*tci12)/15 + 
    fvu3u23*((f76*fvu1u1u1)/3 + (f76*fvu1u1u2)/6 + 
      f76*fvu1u1u3 + f76*fvu1u1u4 + f76*fvu1u1u5 - 
      (f76*fvu1u1u6)/2 - (f76*fvu1u1u7)/6 - (f76*fvu1u1u8)/3 - 
      (f76*fvu1u1u9)/6 - (f76*fvu1u1u10)/3 - 2*f76*tci12) + 
    fvu3u82*((-2*f76*fvu1u1u3)/3 - (2*f76*fvu1u1u4)/3 + 
      f76*fvu1u1u5 + f76*fvu1u1u6 - (f76*fvu1u1u7)/3 + 
      (f76*fvu1u1u8)/3 - (f76*fvu1u1u9)/3 + (4*f76*tci12)/3) + 
    fvu3u80*((2*f76*fvu1u1u2)/3 + f76*fvu1u1u3 - 
      (19*f76*fvu1u1u6)/6 + (19*f76*fvu1u1u7)/6 - 
      (17*f76*fvu1u1u8)/6 + (13*f76*fvu1u1u9)/6 + 
      (17*f76*tci12)/6) - (10*f76*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f76*tci11^3)/81 + (17*f76*tci12*tci21)/
       3) + tci12*((16*f76*tci31)/5 + 16*f76*tci32) - 
    (2072*f76*tci41)/135 - (64*f76*tci42)/5 - 
    (16*f76*tci43)/3 + ((1259*f76*tci11^3)/1620 + 
      f76*tci12*tci21 + (84*f76*tci31)/5 + 
      16*f76*tci32)*tcr11 - (11*f76*tci11*tcr11^3)/180 + 
    fvu1u1u6*((f76*tci11^3)/810 + (58*f76*tci12*tci21)/9 + 
      (88*f76*tci31)/5 + (22*f76*tci21*tcr11)/3 - 
      (11*f76*tci11*tcr11^2)/30) + 
    fvu1u1u2*((181*f76*tci11^3)/1620 + (2*f76*tci12*tci21)/
       9 + (44*f76*tci31)/5 + (11*f76*tci21*tcr11)/3 - 
      (11*f76*tci11*tcr11^2)/60) + 
    fvu1u1u3*((73*f76*tci11^3)/1620 - (f76*tci12*tci21)/3 + 
      (12*f76*tci31)/5 + f76*tci21*tcr11 - 
      (f76*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f76*tci11^3)/540 - (49*f76*tci12*tci21)/
       9 - (4*f76*tci31)/5 - (f76*tci21*tcr11)/3 + 
      (f76*tci11*tcr11^2)/60) + 
    fvu1u1u4*((-31*f76*tci11^3)/270 + (4*f76*tci12*tci21)/
       3 - (24*f76*tci31)/5 - 2*f76*tci21*tcr11 + 
      (f76*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f76*tci11^3)/180 - (f76*tci12*tci21)/
       9 - (28*f76*tci31)/5 - (7*f76*tci21*tcr11)/3 + 
      (7*f76*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f76*tci11^3)/1620 - (19*f76*tci12*tci21)/
       3 - (36*f76*tci31)/5 - 3*f76*tci21*tcr11 + 
      (3*f76*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f76*fvu3u25)/6 + (f76*fvu3u43)/3 + 
      (7*f76*fvu3u45)/6 - (2*f76*fvu3u62)/3 - (7*f76*fvu3u63)/6 + 
      (f76*fvu3u70)/6 - (2*f76*fvu3u71)/3 + (2*f76*fvu3u78)/3 - 
      f76*fvu3u80 - (f76*fvu3u81)/2 - (f76*fvu3u82)/3 - 
      (179*f76*tci11^3)/1620 + (f76*tci12*tci21)/3 - 
      (36*f76*tci31)/5 - 3*f76*tci21*tcr11 + 
      (3*f76*tci11*tcr11^2)/20) + 
    fvu1u1u5*((-8*f76*tci11^3)/135 - (28*f76*tci12*tci21)/
       9 - (64*f76*tci31)/5 - (16*f76*tci21*tcr11)/3 + 
      (4*f76*tci11*tcr11^2)/15) + 
    ((-218*f76*tci11^3)/243 - (40*f76*tci12*tci21)/3)*
     tcr12 + (-4*f76*tci11*tci12 - (40*f76*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f76*tci11*tci12)/15 + 
      5*f76*tci21 - 2*f76*tci11*tcr12) + 
    (130*f76*tci11*tcr33)/27;
L ieu1uou1 = 
   2*f76*fvu3u23 - (11*f76*tci11^3)/135 - 
    (4*f76*tci12*tci21)/3 - (48*f76*tci31)/5 - 
    4*f76*tci21*tcr11 + (f76*tci11*tcr11^2)/5;
L ieu0uou1 = 0;
L ieum1uou1 = 0;
L ieum2uou1 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou1+w^2*ieu1uou1+w^3*ieu0uou1+w^4*ieum1uou1+w^5*ieum2uou1;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou1a = K[w^1];
L ieu1uou1a = K[w^2];
L ieu0uou1a = K[w^3];
L ieum1uou1a = K[w^4];
L ieum2uou1a = K[w^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_odd.c> "%O"
#write <e1_odd.c> "return Eps5o2<T>("
#write <e1_odd.c> "%E", ieu2uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu0uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum2uou1a
#write <e1_odd.c> ");\n}"
L H=+u^1*ieu2ueu1+u^2*ieu1ueu1+u^3*ieu0ueu1+u^4*ieum1ueu1+u^5*ieum2ueu1;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu1a = H[u^1];
L ieu1ueu1a = H[u^2];
L ieu0ueu1a = H[u^3];
L ieum1ueu1a = H[u^4];
L ieum2ueu1a = H[u^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_even.c> "%O"
#write <e1_even.c> "return Eps5o2<T>("
#write <e1_even.c> "%E", ieu2ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu0ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum2ueu1a
#write <e1_even.c> ");\n}"
.end
