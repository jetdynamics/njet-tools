#!/usr/bin/env bash

OUT=conj.cpp
rm -f ${OUT}
touch ${OUT}
OUT2=conj_header.cpp
rm -f ${OUT2}
touch ${OUT2}

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}

    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(( $j+2**$i ))
        fi
    done

    jc=$((31-$j))
    hc=""
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            hc+="-"
        else
            hc+="+"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    if (( $jc < 10 )); then
        jc="0$jc"
    fi

    echo "$h $j -> $hc $jc"

    perl -p -e "s|HH|$jc|g; s|SSSSS|$hc|g; s|CC|$j|g;" template_conj.cpp >> $OUT

    echo "std::array<TwoLoopResult<Eps5o2<T>>, 12> hAg${jc}e2();" >> ${OUT2}
done

