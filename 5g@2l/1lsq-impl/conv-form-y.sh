#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp

echo -e "#-\n#: WorkSpace 400M\n#: Threads 60\nS u,x1,x2,x3,x4,x5;" >$FRM
echo "ExtraSymbols,array,Z;" >>$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|^\(\* Created with the Wolfram Language : www\.wolfram\.com \*\)$||gm;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,\s*|L \1=\2;\n|gs;
        s|([yf]\d+ )->( .*?)\}{1,2}|L \1=\2;\n|gs;
        " \
    ${IN} >>${FRM}

echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

ysr=`rg -o "y\d+ =" $FRM`
declare -a ys
for y in ${ysr[@]}; do
    ys+=( "${y%*=}" )
done
N=$((${#ys[@]}/${#ys}))
echo $N >${BASE}.ycount

h="L H="
i=1
for y in ${ys[@]}; do
    h+="+u^$((i++))*$y"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${ys[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"\`optimmaxvar_'\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

echo "#write <$C> \"const std::vector<TreeValue> y {\"" >>$FRM
for y in ${ys[@]}; do
    echo "#write <$C> \"%E,\", ${y}a" >>$FRM
done
echo "#write <$C> \"};\"" >>$FRM

echo ".end" >>$FRM
