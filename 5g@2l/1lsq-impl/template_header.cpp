    // SSSSS
    std::array<TwoLoopResult<Eps5o2<T>, 12> hAgHHe2();
    void hAgHHe2coeffs();

    Eps5o2<T> hAgHHe2p1e();
    Eps5o2<T> hAgHHe2p2e();
    Eps5o2<T> hAgHHe2p3e();
    Eps5o2<T> hAgHHe2p4e();
    Eps5o2<T> hAgHHe2p5e();
    Eps5o2<T> hAgHHe2p6e();
    Eps5o2<T> hAgHHe2p7e();
    Eps5o2<T> hAgHHe2p8e();
    Eps5o2<T> hAgHHe2p9e();
    Eps5o2<T> hAgHHe2p10e();
    Eps5o2<T> hAgHHe2p11e();
    Eps5o2<T> hAgHHe2p12e();

    Eps5o2<T> hAgHHe2p1o();
    Eps5o2<T> hAgHHe2p2o();
    Eps5o2<T> hAgHHe2p3o();
    Eps5o2<T> hAgHHe2p4o();
    Eps5o2<T> hAgHHe2p5o();
    Eps5o2<T> hAgHHe2p6o();
    Eps5o2<T> hAgHHe2p7o();
    Eps5o2<T> hAgHHe2p8o();
    Eps5o2<T> hAgHHe2p9o();
    Eps5o2<T> hAgHHe2p10o();
    Eps5o2<T> hAgHHe2p11o();
    Eps5o2<T> hAgHHe2p12o();

