#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f182;
S f183;
S f180;
S f181;
S f186;
S f187;
S f184;
S f185;
S f188;
S f125;
S f75;
S f76;
S f72;
S f73;
S f3;
S f140;
S f141;
S f179;
S f178;
S f173;
S f172;
S f22;
S f177;
S f176;
S f175;
S f174;
S f38;
S f31;
S f35;
S f100;
S f106;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu8 = (11*f3 - 11*f22 - 11*f35 + 11*f38 + 
      110*f75 - 11*f76 + 11*f106 - 11*f125 + 74*f175)/27 + 
    (f183*fvu3u1)/3 - (f183*fvu3u2)/6 + ((f183 + 4*f184)*fvu3u3)/
     9 + (f183*fvu3u4)/6 + ((-3*f172 + 14*f184)*fvu3u5)/18 + 
    (4*f183*fvu3u6)/9 + (f184*fvu3u7)/2 + (16*f184*fvu3u8)/9 + 
    (2*f184*fvu3u11)/9 + (f183*fvu3u12)/9 + 
    ((f183 - 2*f184)*fvu3u13)/18 + (f183*fvu3u14)/3 + 
    (f183*fvu3u15)/6 + (f184*fvu3u17)/3 + (f183*fvu3u18)/3 + 
    (f183*fvu3u19)/9 - (f183*fvu3u22)/3 - (2*f184*fvu3u24)/9 - 
    (2*f183*fvu3u31)/9 - (11*f183*fvu3u32)/18 - (f183*fvu3u33)/3 - 
    (16*f183*fvu3u34)/9 + (f183*fvu3u35)/6 - (2*f183*fvu3u36)/9 + 
    (f183*fvu3u37)/18 - (f183*fvu3u40)/6 + (2*f183*fvu3u44)/9 + 
    (f184*fvu3u52)/18 - (f186*fvu3u53)/6 - (2*f184*fvu3u55)/9 - 
    (f184*fvu3u56)/6 - (f184*fvu3u57)/6 + ((f184 + f187)*fvu3u59)/
     6 + (f183*fvu1u1u1^3)/2 - (2*(3*f183 - 13*f184)*fvu1u1u2^3)/
     27 + ((-f183 - 2*f184)*fvu1u1u3^3)/18 + 
    (2*f183*fvu1u1u5^3)/9 + ((17*f183 - 3*f184)*fvu1u1u6^3)/27 + 
    (7*f183*fvu1u1u7^3)/27 + (f184*fvu1u1u8^3)/3 - 
    (5*f184*fvu1u1u9^3)/27 + fvu1u1u4^2*
     ((6*f73 - f100 - 6*f140 + f141 + 3*f178 - 6*f183 - 6*f186)/
       18 + ((-f183 + 2*f184)*fvu1u1u5)/18 - (f183*fvu1u1u6)/9 + 
      (f183*fvu1u1u7)/9 + (f183*fvu1u1u8)/9 + 
      (5*f184*fvu1u1u9)/9) + 
    ((6*f73 - f100 - 6*f140 + f141 - 6*f183 - 6*f186)*fvu2u1u6)/
     9 + ((6*f140 - f141 + 6*f186 - f188)*fvu2u1u14)/9 + 
    ((f185 - f188)*fvu2u1u15)/9 - (2*f184*fvu1u2u2*tci11^2)/9 + 
    (f183*fvu1u2u7*tci11^2)/3 + (2*f183*fvu1u2u8*tci11^2)/3 + 
    (2*f184*fvu1u2u9*tci11^2)/3 + (f179*tci12)/9 + 
    ((-6*f73 + f100 + 6*f140 - f141 + 6*f183 + 6*f186)*fvu1u2u3*
      tci12)/9 - (4*f183*fvu2u1u1*tci12)/3 + 
    (4*f183*fvu2u1u2*tci12)/9 - (f183*fvu2u1u5*tci12)/3 - 
    (4*f183*fvu2u1u8*tci12)/9 - (8*f183*fvu2u1u9*tci12)/9 + 
    (2*f183*fvu2u1u10*tci12)/3 - (2*f184*fvu2u1u12*tci12)/3 + 
    (2*f184*fvu2u1u13*tci12)/9 + fvu1u1u1^2*
     (((3*f183 - f184)*fvu1u1u2)/9 - (f183*fvu1u1u3)/6 - 
      (7*f183*fvu1u1u4)/18 + (f183*fvu1u1u6)/18 + 
      (f183*fvu1u1u7)/9 - (2*f183*fvu1u1u8)/9 + 
      (f184*fvu1u1u9)/9 - (13*f183*tci12)/18) + 
    fvu1u1u7^2*((f183*fvu1u1u8)/9 + (2*f183*tci12)/9) + 
    fvu1u2u6*((2*(f183 - 4*f184)*tci11^2)/9 + 
      ((-6*f31 + f72 - 6*f73 + f100 - 6*f172 + 6*f183)*tci12)/
       9) + fvu1u1u2^2*((-12*f31 + 2*f72 - 6*f73 + f100 - 
        12*f172 - 3*f180 + 6*f183 - f185)/18 - 
      (f183*fvu1u1u4)/18 + ((-f183 - 4*f184)*fvu1u1u5)/18 + 
      (f183*fvu1u1u6)/6 - (f184*fvu1u1u8)/18 + 
      (2*f184*fvu1u1u9)/9 - (f184*fvu1u1u10)/6 + 
      ((2*f183 - 7*f184)*tci12)/18) + 
    fvu2u1u3*((-6*f31 + f72 - 6*f73 + f100 - 6*f172 + 6*f183)/
       9 + (2*(f183 - 4*f184)*tci12)/9) + 
    fvu1u1u5^2*(-(f183*fvu1u1u6)/6 + (f183*fvu1u1u7)/9 - 
      (f183*fvu1u1u8)/18 + (f184*fvu1u1u9)/3 + 
      ((f183 - f184)*tci12)/9) + fvu1u1u6^2*
     ((6*f140 - f141 + 3*f173 + 6*f186 - f188)/18 - 
      (4*f183*fvu1u1u7)/9 - (f183*fvu1u1u8)/6 + 
      (f184*fvu1u1u9)/6 - (f184*fvu1u1u10)/9 + 
      ((6*f183 - f184)*tci12)/18) + 
    fvu1u1u9^2*((3*f176 + f185 - f188)/18 - (13*f184*tci12)/
       18) + fvu2u1u11*((-6*f31 + f72 - 6*f172 - f185)/9 - 
      (4*f184*tci12)/9) + fvu1u1u8^2*((f184*fvu1u1u9)/9 - 
      (f184*fvu1u1u10)/6 - (f184*tci12)/3) + 
    fvu1u1u10^2*((-f173 + f175 - f176 - f178 + f180)/6 + 
      (f184*tci12)/6) + fvu1u1u3^2*((f183*fvu1u1u4)/6 + 
      (f184*fvu1u1u6)/18 + (f183*fvu1u1u8)/3 + 
      (f184*fvu1u1u9)/6 - (f184*fvu1u1u10)/9 + 
      ((9*f183 + 2*f184)*tci12)/18) + 
    tci11^2*((48*f73 - 8*f100 - 12*f140 + 2*f141 + 3*f175 + 
        18*f178 - 48*f183 + 2*f185 - 12*f186 + 2*f188)/108 + 
      ((3*f172 - 7*f183 - 12*f184 - 3*f186)*tci12)/108) + 
    fvu1u1u10*((-f3 + f22 + f35 - f38 - 10*f75 + f76 - f106 + 
        f125 + f174 - 10*f175 + f177 + f179 - f181)/9 - 
      (2*f184*fvu2u1u13)/9 - (2*f184*fvu2u1u14)/9 - 
      (2*f184*fvu2u1u15)/3 - (7*f184*tci11^2)/27 + 
      ((-6*f140 + f141 - 6*f186)*tci12)/9 + 
      (2*f184*fvu1u2u2*tci12)/9) + 
    fvu1u1u7*((4*f183*fvu1u1u8^2)/9 + (2*f183*fvu2u1u7)/9 + 
      (10*f183*fvu2u1u8)/9 - (2*f183*fvu2u1u10)/3 + 
      (2*f183*tci11^2)/27 - (2*f183*fvu1u1u8*tci12)/9 - 
      (2*f183*fvu1u2u8*tci12)/3) + 
    fvu1u1u9*(-f177/9 - (f185*fvu1u1u10)/9 + 
      (f184*fvu1u1u10^2)/3 + (20*f184*fvu2u1u11)/9 + 
      (2*f184*fvu2u1u12)/3 - (2*f184*fvu2u1u15)/3 + 
      f184*tci11^2 + ((6*f31 - f72 + 6*f172)*tci12)/9 - 
      (2*f184*fvu1u2u9*tci12)/3) + 
    fvu1u1u8*((-2*f184*fvu1u1u9^2)/9 - (f184*fvu1u1u10^2)/6 + 
      (4*f183*fvu2u1u8)/9 + (4*f183*fvu2u1u9)/9 - 
      (2*f183*fvu2u1u10)/3 + (4*f184*fvu2u1u11)/9 + 
      (2*f184*fvu2u1u12)/3 + ((5*f183 + 9*f184)*tci11^2)/54 + 
      (4*f184*fvu1u1u9*tci12)/9 + (f184*fvu1u1u10*tci12)/3 - 
      (2*f183*fvu1u2u8*tci12)/3 - (2*f184*fvu1u2u9*tci12)/
       3) + fvu1u1u5*(-(f183*fvu1u1u6^2)/6 - (f183*fvu1u1u8^2)/18 + 
      (f184*fvu1u1u9^2)/3 - (2*(f183 - 2*f184)*fvu2u1u4)/9 + 
      (f183*fvu2u1u5)/3 + (2*f183*fvu2u1u7)/9 + 
      ((-13*f183 + 16*f184)*tci11^2)/108 + 
      (f183*fvu1u1u8*tci12)/9 + (2*f184*fvu1u1u9*tci12)/3 - 
      (f183*fvu1u2u7*tci12)/3 + fvu1u1u6*((f183*fvu1u1u8)/3 - 
        (f183*tci12)/3) + fvu1u1u7*((-2*f183*fvu1u1u8)/9 + 
        (2*f183*tci12)/9)) + fvu1u1u4*
     (-f179/9 + ((-f183 + 2*f184)*fvu1u1u5^2)/18 + 
      (f183*fvu1u1u6^2)/9 + (f183*fvu1u1u7^2)/9 + 
      (f183*fvu1u1u8^2)/9 + (5*f184*fvu1u1u9^2)/9 + 
      ((6*f140 - f141 + 6*f186)*fvu1u1u10)/9 - 
      (2*f183*fvu2u1u2)/9 - (2*(f183 - 4*f184)*fvu2u1u3)/9 + 
      (4*f183*fvu2u1u8)/9 + (4*f184*fvu2u1u11)/9 - 
      (2*f184*tci11^2)/27 - (f178*tci12)/3 - 
      (2*(f183 - 4*f184)*fvu1u2u6*tci12)/9 + 
      fvu1u1u7*((2*f183*fvu1u1u8)/9 - (2*f183*tci12)/9) + 
      fvu1u1u8*((2*f184*fvu1u1u9)/9 - (2*f183*tci12)/9) + 
      fvu1u1u6*((-6*f73 + f100 + 6*f183)/9 - (2*f183*fvu1u1u7)/
         9 - (2*f183*fvu1u1u8)/9 + (2*f183*tci12)/9) + 
      fvu1u1u5*((-2*f184*fvu1u1u9)/3 + ((f183 - 2*f184)*tci12)/
         9) + fvu1u1u9*((-6*f31 + f72 - 6*f172)/9 - 
        (10*f184*tci12)/9)) + fvu1u1u3*((f183*fvu1u1u4^2)/6 - 
      (f183*fvu1u1u5^2)/6 + (f184*fvu1u1u6^2)/18 + 
      (f183*fvu1u1u8^2)/3 + (f184*fvu1u1u9^2)/6 + 
      (2*f183*fvu2u1u1)/3 + (f183*fvu2u1u5)/3 - 
      (2*f184*fvu2u1u13)/9 + ((-11*f183 - f184)*tci11^2)/18 + 
      (f183*fvu1u1u5*tci12)/3 - (2*f183*fvu1u1u8*tci12)/3 - 
      (f184*fvu1u1u9*tci12)/3 + (2*f184*fvu1u2u2*tci12)/9 - 
      (f183*fvu1u2u7*tci12)/3 + fvu1u1u4*
       ((-2*f183*fvu1u1u8)/3 - (f183*tci12)/3) + 
      fvu1u1u6*(-(f184*fvu1u1u9)/3 + (2*f184*fvu1u1u10)/9 - 
        (f184*tci12)/9)) + fvu1u1u6*(-f174/9 - 
      (5*f183*fvu1u1u7^2)/9 - (7*f183*fvu1u1u8^2)/18 + 
      (f184*fvu1u1u9^2)/6 + (14*f183*fvu2u1u8)/9 - 
      (2*f184*fvu2u1u14)/9 - (2*(6*f183 - f184)*tci11^2)/27 + 
      ((6*f73 - f100 - 6*f183)*tci12)/9 + 
      (7*f183*fvu1u1u8*tci12)/9 + fvu1u1u10*
       ((-6*f140 + f141 - 6*f186)/9 - (2*f184*tci12)/9) + 
      fvu1u1u9*(f188/9 + (f184*tci12)/3)) + 
    fvu1u1u1*(((3*f183 - 8*f184)*fvu1u1u2^2)/18 - 
      (f183*fvu1u1u3^2)/2 - (5*f183*fvu1u1u4^2)/18 + 
      (f183*fvu1u1u5^2)/6 - (5*f183*fvu1u1u6^2)/18 + 
      (2*f183*fvu1u1u7^2)/9 - (4*f183*fvu1u1u8^2)/9 + 
      (2*f184*fvu1u1u9^2)/9 + (2*f183*fvu2u1u1)/3 - 
      (2*f183*fvu2u1u2)/9 - (4*f183*fvu2u1u8)/9 + 
      (4*f183*fvu2u1u9)/9 - (4*f184*fvu2u1u11)/9 - 
      (11*f183*tci11^2)/54 + (2*f184*fvu1u1u9*tci12)/9 + 
      fvu1u1u6*((2*f183*fvu1u1u7)/9 + (2*f183*fvu1u1u8)/9 - 
        (5*f183*tci12)/9) + fvu1u1u7*((-2*f183*fvu1u1u8)/9 + 
        (2*f183*tci12)/9) + fvu1u1u4*((2*f183*fvu1u1u6)/9 - 
        (2*f183*fvu1u1u7)/9 + (4*f183*fvu1u1u8)/9 - 
        (2*f184*fvu1u1u9)/9 + (5*f183*tci12)/9) + 
      fvu1u1u8*((-2*f184*fvu1u1u9)/9 + (8*f183*tci12)/9) + 
      fvu1u1u3*((f183*fvu1u1u4)/3 + f183*tci12) + 
      fvu1u1u2*((2*f184*fvu1u1u4)/9 - (f183*fvu1u1u6)/3 + 
        (2*f184*fvu1u1u8)/9 + (2*f184*fvu1u1u9)/9 - 
        (2*(3*f183 + f184)*tci12)/9)) + 
    fvu1u1u2*(f181/9 - (f183*fvu1u1u3^2)/6 + 
      ((f183 - 12*f184)*fvu1u1u4^2)/18 + 
      ((f183 - 8*f184)*fvu1u1u5^2)/18 + (f183*fvu1u1u6^2)/6 - 
      (5*f184*fvu1u1u8^2)/18 - (8*f184*fvu1u1u9^2)/9 - 
      (f184*fvu1u1u10^2)/6 - (2*(f183 - 4*f184)*fvu2u1u3)/9 - 
      (2*(f183 - 2*f184)*fvu2u1u4)/9 + (16*f184*fvu2u1u11)/9 + 
      ((3*f183 - 8*f184)*tci11^2)/27 + 
      ((6*f73 - f100 - 6*f183)*tci12)/9 + 
      (f183*fvu1u1u3*tci12)/3 + ((-f183 - 4*f184)*fvu1u1u5*
        tci12)/9 - (2*(f183 - 4*f184)*fvu1u2u6*tci12)/9 + 
      fvu1u1u6*((6*f73 - f100 - 6*f183)/9 + (f183*tci12)/3) + 
      fvu1u1u10*(f185/9 - (f184*tci12)/3) + 
      fvu1u1u9*((6*f31 - f72 + 6*f172)/9 + (4*f184*tci12)/9) + 
      fvu1u1u8*((-2*f184*fvu1u1u9)/9 + (f184*fvu1u1u10)/3 + 
        (5*f184*tci12)/9) + fvu1u1u4*
       ((6*f31 - f72 + 6*f172)/9 + ((f183 + 4*f184)*fvu1u1u5)/9 - 
        (2*f184*fvu1u1u8)/9 - (4*f184*fvu1u1u9)/9 + 
        ((f183 + 4*f184)*tci12)/9)) + 
    (7*f183*tci12*tcr11^2)/18 - (f183*tcr11^3)/2 + 
    ((-3*f172 - 16*f183 + 8*f184 + 3*f186)*tci11^2*tcr12)/
     18 + (10*f183*tci12*tcr21)/9 + 
    tcr11*(-(f183*tci11^2)/54 + (2*f183*tci12*tcr12)/3 - 
      (8*f183*tcr21)/9) + 
    ((-9*f172 + 26*f183 - 12*f184 + 9*f186 - 12*f187)*tcr33)/
     36;
L ieu1ueu8 = (f3 - f22 - f35 + f38 + 10*f75 - 
      f76 + f106 - f125 + 10*f175)/9 + 
    ((-2*f172 + f183 - f184)*fvu1u1u2^2)/6 + 
    ((-f183 - f186)*fvu1u1u4^2)/6 + ((f186 - f187)*fvu1u1u6^2)/
     6 + ((f184 - f187)*fvu1u1u9^2)/6 + 
    fvu1u1u4*(-f178/3 + (f183*fvu1u1u6)/3 - (f172*fvu1u1u9)/3 + 
      (f186*fvu1u1u10)/3) + ((-f172 + f183)*fvu2u1u3)/3 + 
    ((-f183 - f186)*fvu2u1u6)/3 + ((-f172 - f184)*fvu2u1u11)/3 + 
    ((f186 - f187)*fvu2u1u14)/3 + ((f184 - f187)*fvu2u1u15)/3 + 
    ((-4*f183 + f184 - f186 + f187)*tci11^2)/18 + 
    (f178*tci12)/3 + ((f183 + f186)*fvu1u2u3*tci12)/3 + 
    ((-f172 + f183)*fvu1u2u6*tci12)/3 + 
    fvu1u1u9*(-f176/3 - (f184*fvu1u1u10)/3 + (f172*tci12)/3) + 
    fvu1u1u2*(f180/3 + (f172*fvu1u1u4)/3 - (f183*fvu1u1u6)/3 + 
      (f172*fvu1u1u9)/3 + (f184*fvu1u1u10)/3 - 
      (f183*tci12)/3) + fvu1u1u6*(-f173/3 + (f187*fvu1u1u9)/3 - 
      (f186*fvu1u1u10)/3 - (f183*tci12)/3) + 
    fvu1u1u10*((f173 - f175 + f176 + f178 - f180)/3 - 
      (f186*tci12)/3);
L ieu0ueu8 = f175/3;
L ieum1ueu8 = 0;
L ieum2ueu8 = 0;
L ieu2uou8 = (2*f182*fvu4u25)/3 + (2*f182*fvu4u28)/3 + 
    (2*f182*fvu4u39)/3 + (2*f182*fvu4u41)/3 - (2*f182*fvu4u51)/9 + 
    (17*f182*fvu4u80)/12 - (f182*fvu4u83)/12 - (3*f182*fvu4u91)/4 + 
    (17*f182*fvu4u93)/12 - (5*f182*fvu4u102)/18 + 
    (f182*fvu4u111)/4 + (f182*fvu4u113)/2 + f182*fvu4u114 - 
    (2*f182*fvu4u129)/3 + (13*f182*fvu4u132)/3 - 
    (25*f182*fvu4u139)/6 - (2*f182*fvu4u141)/3 + 
    (4*f182*fvu4u146)/9 + (13*f182*fvu4u148)/18 - 
    (7*f182*fvu4u171)/12 + (23*f182*fvu4u174)/12 - 
    (25*f182*fvu4u182)/12 - (7*f182*fvu4u184)/12 + 
    (4*f182*fvu4u190)/9 - (f182*fvu4u192)/18 + (f182*fvu4u199)/4 + 
    (f182*fvu4u201)/2 + f182*fvu4u202 + (7*f182*fvu4u213)/12 - 
    (17*f182*fvu4u215)/4 + (17*f182*fvu4u219)/4 + 
    (7*f182*fvu4u221)/12 - (4*f182*fvu4u225)/9 - (f182*fvu4u233)/4 - 
    (f182*fvu4u234)/2 - f182*fvu4u235 + (5*f182*fvu4u244)/3 - 
    (2*f182*fvu4u246)/3 - (3*f182*fvu4u252)/2 - 
    (5*f182*fvu4u255)/18 - (17*f182*fvu4u271)/12 - 
    (19*f182*fvu4u273)/12 + (19*f182*fvu4u277)/12 - 
    (17*f182*fvu4u279)/12 - (f182*fvu4u289)/4 - (f182*fvu4u290)/2 - 
    f182*fvu4u291 - (2*f182*fvu4u293)/3 - (2*f182*fvu4u295)/3 - 
    2*f182*fvu4u309 + fvu3u45*((f182*fvu1u1u3)/3 + 
      (f182*fvu1u1u6)/3 - (f182*fvu1u1u7)/3) + 
    fvu3u70*((2*f182*fvu1u1u2)/3 - (2*f182*fvu1u1u3)/3 - 
      (2*f182*fvu1u1u9)/3) + fvu3u78*((2*f182*fvu1u1u2)/3 - 
      (2*f182*fvu1u1u3)/3 - (2*f182*fvu1u1u9)/3) + 
    fvu3u63*(-(f182*fvu1u1u2)/3 + (5*f182*fvu1u1u3)/2 + 
      (2*f182*fvu1u1u5)/3 + (5*f182*fvu1u1u6)/2 + 
      (2*f182*fvu1u1u7)/3 - (f182*fvu1u1u8)/3 - 
      (f182*fvu1u1u9)/3 - (19*f182*fvu1u1u10)/6) + 
    fvu3u25*((2*f182*fvu1u1u2)/3 + (f182*fvu1u1u3)/3 - 
      (2*f182*fvu1u1u5)/3 + (f182*fvu1u1u6)/3 + 
      (f182*fvu1u1u7)/3 - (2*f182*fvu1u1u10)/3) + 
    fvu3u43*((f182*fvu1u1u2)/3 - (f182*fvu1u1u4)/3 + 
      (f182*fvu1u1u6)/3 - (f182*fvu1u1u7)/3 - 
      (f182*fvu1u1u10)/3) + fvu3u23*((f182*fvu1u1u1)/3 - 
      (f182*fvu1u1u2)/3 + (f182*fvu1u1u7)/3 - (f182*fvu1u1u8)/3 + 
      (f182*fvu1u1u9)/3 - (f182*fvu1u1u10)/3) + 
    fvu3u62*(-(f182*fvu1u1u2)/3 - (f182*fvu1u1u4)/3 + 
      (2*f182*fvu1u1u5)/3 - (f182*fvu1u1u6)/3 - 
      (f182*fvu1u1u7)/3 + (f182*fvu1u1u10)/3) + 
    fvu3u81*((f182*fvu1u1u4)/3 - (f182*fvu1u1u6)/3 + 
      (f182*fvu1u1u7)/3 - (f182*fvu1u1u9)/3 + 
      (f182*fvu1u1u10)/3) - (151*f182*tci11^3*tci12)/135 + 
    fvu3u71*(-(f182*fvu1u1u2)/3 + (f182*fvu1u1u3)/3 + 
      2*f182*fvu1u1u4 - (2*f182*fvu1u1u5)/3 - (f182*fvu1u1u6)/3 - 
      (2*f182*fvu1u1u7)/3 + (f182*fvu1u1u8)/3 + f182*fvu1u1u9 + 
      f182*fvu1u1u10 - 2*f182*tci12) + 
    fvu3u82*((f182*fvu1u1u3)/3 + (f182*fvu1u1u4)/3 - 
      (f182*fvu1u1u7)/3 + (f182*fvu1u1u8)/3 - (f182*fvu1u1u9)/3 - 
      (2*f182*tci12)/3) + fvu3u80*((-7*f182*fvu1u1u6)/6 + 
      (7*f182*fvu1u1u7)/6 - (5*f182*fvu1u1u8)/6 + 
      (7*f182*fvu1u1u9)/6 + (5*f182*tci12)/6) - 
    (2*f182*tci11^2*tci21)/9 + 
    fvu1u1u8*((-5*f182*tci11^3)/81 + (5*f182*tci12*tci21)/
       3) + fvu1u1u4*((-4*f182*tci11^3)/27 + 
      4*f182*tci12*tci21) + tci12*((-48*f182*tci31)/5 - 
      16*f182*tci32) - (4*f182*tci41)/3 + 
    ((-23*f182*tci11^3)/324 + (7*f182*tci12*tci21)/3 + 
      12*f182*tci31)*tcr11 + ((f182*tci11*tci12)/5 + 
      5*f182*tci21)*tcr11^2 - (f182*tci11*tcr11^3)/4 + 
    fvu1u1u6*((149*f182*tci11^3)/1620 + 
      (28*f182*tci12*tci21)/9 + (76*f182*tci31)/5 + 
      (19*f182*tci21*tcr11)/3 - (19*f182*tci11*tcr11^2)/
       60) + fvu1u1u3*(-(f182*fvu3u81)/3 + (41*f182*tci11^3)/324 + 
      f182*tci12*tci21 + 12*f182*tci31 + 
      5*f182*tci21*tcr11 - (f182*tci11*tcr11^2)/4) + 
    fvu1u1u2*(-(f182*fvu3u80)/3 + (22*f182*tci11^3)/405 + 
      (8*f182*tci12*tci21)/9 + (32*f182*tci31)/5 + 
      (8*f182*tci21*tcr11)/3 - (2*f182*tci11*tcr11^2)/
       15) + fvu1u1u7*((103*f182*tci11^3)/810 - 
      (5*f182*tci12*tci21)/3 + (24*f182*tci31)/5 + 
      2*f182*tci21*tcr11 - (f182*tci11*tcr11^2)/10) + 
    fvu1u1u5*((f182*tci11^3)/45 - (16*f182*tci12*tci21)/9 - 
      (16*f182*tci31)/5 - (4*f182*tci21*tcr11)/3 + 
      (f182*tci11*tcr11^2)/15) + 
    fvu1u1u9*((-2*f182*tci11^3)/135 - (7*f182*tci12*tci21)/
       9 - (16*f182*tci31)/5 - (4*f182*tci21*tcr11)/3 + 
      (f182*tci11*tcr11^2)/15) + 
    fvu1u1u1*(-(f182*fvu3u25)/3 + (f182*fvu3u43)/3 - 
      (f182*fvu3u45)/3 + (f182*fvu3u62)/3 - (13*f182*fvu3u63)/6 + 
      (2*f182*fvu3u70)/3 - (2*f182*fvu3u71)/3 + (2*f182*fvu3u78)/3 - 
      (f182*fvu3u82)/3 - (25*f182*tci11^3)/324 - 
      (7*f182*tci12*tci21)/3 - 12*f182*tci31 - 
      5*f182*tci21*tcr11 + (f182*tci11*tcr11^2)/4) + 
    fvu1u1u10*((-29*f182*tci11^3)/108 - (f182*tci12*tci21)/
       9 - 20*f182*tci31 - (25*f182*tci21*tcr11)/3 + 
      (5*f182*tci11*tcr11^2)/12) + 
    (40*f182*tci12*tci21*tcr12)/3 + 
    4*f182*tci11*tci12*tcr12^2;
L ieu1uou8 = 2*f182*fvu3u71 - (4*f182*tci11^3)/27 + 
    4*f182*tci12*tci21;
L ieu0uou8 = 0;
L ieum1uou8 = 0;
L ieum2uou8 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou8+w^2*ieu1uou8+w^3*ieu0uou8+w^4*ieum1uou8+w^5*ieum2uou8;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou8a = K[w^1];
L ieu1uou8a = K[w^2];
L ieu0uou8a = K[w^3];
L ieum1uou8a = K[w^4];
L ieum2uou8a = K[w^5];
.sort
#write <e8.tmp> "`optimmaxvar_'"
#write <e8_odd.c> "%O"
#write <e8_odd.c> "return Eps5o2<T>("
#write <e8_odd.c> "%E", ieu2uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieu1uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieu0uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieum1uou8a
#write <e8_odd.c> ", "
#write <e8_odd.c> "%E", ieum2uou8a
#write <e8_odd.c> ");\n}"
L H=+u^1*ieu2ueu8+u^2*ieu1ueu8+u^3*ieu0ueu8+u^4*ieum1ueu8+u^5*ieum2ueu8;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu8a = H[u^1];
L ieu1ueu8a = H[u^2];
L ieu0ueu8a = H[u^3];
L ieum1ueu8a = H[u^4];
L ieum2ueu8a = H[u^5];
.sort
#write <e8.tmp> "`optimmaxvar_'"
#write <e8_even.c> "%O"
#write <e8_even.c> "return Eps5o2<T>("
#write <e8_even.c> "%E", ieu2ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieu1ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieu0ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieum1ueu8a
#write <e8_even.c> ", "
#write <e8_even.c> "%E", ieum2ueu8a
#write <e8_even.c> ");\n}"
.end
