#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f64;
S f62;
S f74;
S f75;
S f76;
S f122;
S f123;
S f73;
S f78;
S f153;
S f155;
S f88;
S f80;
S f82;
S f12;
S f85;
S f84;
S f87;
S f11;
S f96;
S f97;
S f94;
S f92;
S f90;
S f177;
S f176;
S f99;
S f160;
S f161;
S f100;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu1u1u9;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u291;
S fvu4u293;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu7 = (-36*f75 + 11*f76)/27 + 
    ((f73 + 2*f94)*fvu3u1)/3 + ((-f73 - 2*f94)*fvu3u2)/6 + 
    (f73*fvu3u3)/9 + ((f73 + f94)*fvu3u4)/6 + (f94*fvu3u5)/18 + 
    (2*(2*f73 + f94)*fvu3u6)/9 - (2*f94*fvu3u11)/9 + 
    ((2*f73 + f94)*fvu3u12)/18 + ((f73 + 3*f90 - 3*f94)*fvu3u13)/
     18 + (f73*fvu3u14)/3 + ((f73 - f94)*fvu3u15)/6 + 
    (f94*fvu3u17)/6 + ((2*f73 + f94)*fvu3u18)/6 + 
    ((f73 + 2*f94)*fvu3u19)/9 - (f73*fvu3u22)/3 - 
    (2*f73*fvu3u31)/9 + ((-11*f73 + f94)*fvu3u32)/18 + 
    ((-2*f73 - f92)*fvu3u33)/6 - (16*f73*fvu3u34)/9 + 
    (f73*fvu3u35)/6 - (2*(f73 + f94)*fvu3u36)/9 + 
    ((f73 - 3*f94)*fvu3u37)/18 - (f94*fvu3u38)/6 + 
    ((-f73 + f94 + f97)*fvu3u40)/6 + (2*f73*fvu3u44)/9 + 
    ((9*f73 + 4*f94)*fvu1u1u1^3)/18 + 
    ((-2*f73 + 5*f94)*fvu1u1u2^3)/9 + ((-f73 + 6*f94)*fvu1u1u3^3)/
     18 + ((2*f73 - f94)*fvu1u1u5^3)/9 + 
    ((17*f73 - 3*f94)*fvu1u1u6^3)/27 + 
    ((7*f73 + 3*f94)*fvu1u1u7^3)/27 + 
    fvu1u1u4^2*((-6*f62 + f64 + 3*f85 - 6*f92 - f100)/18 + 
      ((-f73 + 3*f94)*fvu1u1u5)/18 - (f73*fvu1u1u6)/9 + 
      (f73*fvu1u1u7)/9 + ((2*f73 + f94)*fvu1u1u8)/18 + 
      (f94*fvu1u1u9)/18) + ((-6*f11 + f12 - 6*f90 - f96)*
      fvu2u1u4)/9 + ((-6*f62 + f64 - 6*f92 - f100)*fvu2u1u6)/9 + 
    ((f96 - f99)*fvu2u1u7)/9 + ((f73 + 2*f94)*fvu1u2u7*
      tci11^2)/3 + (2*(3*f73 - f94)*fvu1u2u8*tci11^2)/9 + 
    fvu1u1u9*((2*f94*fvu2u1u11)/9 + (2*f94*tci11^2)/27) + 
    (f87*tci12)/9 - (f94*fvu1u1u9^2*tci12)/18 + 
    ((6*f62 - f64 + 6*f92 + f100)*fvu1u2u3*tci12)/9 - 
    (4*(f73 + f94)*fvu2u1u1*tci12)/3 + 
    (4*(f73 - f94)*fvu2u1u2*tci12)/9 + 
    ((-f73 - 2*f94)*fvu2u1u5*tci12)/3 - 
    (4*(2*f73 + f94)*fvu2u1u9*tci12)/9 + 
    (2*(3*f73 - f94)*fvu2u1u10*tci12)/9 + 
    fvu2u1u8*((6*f62 - f64 + 6*f92 - f99)/9 - 
      (4*f73*tci12)/9) + tci11^2*
     ((-12*f62 + 2*f64 + 3*f75 + 18*f85 - 12*f92 + 2*f96 + 
        2*f99 - 8*f100)/108 + ((-7*f73 + 3*f90 - 3*f92 - 10*f94)*
        tci12)/108) + fvu1u1u1^2*((f73*fvu1u1u2)/3 - 
      (f73*fvu1u1u3)/6 + ((-7*f73 - 2*f94)*fvu1u1u4)/18 + 
      (f73*fvu1u1u6)/18 + (f73*fvu1u1u7)/9 + 
      ((-2*f73 - f94)*fvu1u1u8)/9 + ((-13*f73 - 8*f94)*tci12)/
       18) + fvu1u1u5^2*((3*f74 + f96 - f99)/18 + 
      ((-f73 + f94)*fvu1u1u6)/6 + (f73*fvu1u1u7)/9 + 
      ((-f73 + 3*f94)*fvu1u1u8)/18 + (f94*fvu1u1u9)/6 + 
      ((f73 - 3*f94)*tci12)/9) + fvu1u1u3^2*
     (((f73 + f94)*fvu1u1u4)/6 - (f94*fvu1u1u7)/6 + 
      ((2*f73 + f94)*fvu1u1u8)/6 + ((3*f73 - 2*f94)*tci12)/6) + 
    fvu2u1u3*((-6*f11 + f12 - 6*f90 + f100)/9 + 
      (2*(f73 - f94)*tci12)/9) + fvu1u1u6^2*
     ((6*f62 - f64 + 3*f78 + 6*f92 - f99)/18 + 
      ((-4*f73 - f94)*fvu1u1u7)/9 + ((-3*f73 + f94)*fvu1u1u8)/
       18 + ((6*f73 - f94)*tci12)/18) + 
    fvu1u1u2^2*((-12*f11 + 2*f12 - 3*f82 - 12*f90 - f96 + f100)/
       18 - (f94*fvu1u1u3)/6 + ((-f73 - 2*f94)*fvu1u1u4)/18 - 
      (f73*fvu1u1u5)/18 + (f73*fvu1u1u6)/6 - (f94*fvu1u1u7)/6 - 
      (f94*fvu1u1u9)/9 + ((2*f73 + f94)*tci12)/18) + 
    fvu1u1u7^2*((-f74 + f75 - f78 + f82 - f85)/6 + 
      ((f73 - f94)*fvu1u1u8)/9 + ((4*f73 + f94)*tci12)/18) + 
    fvu1u2u6*((2*(f73 - f94)*tci11^2)/9 + 
      ((-6*f11 + f12 - 6*f90 + f100)*tci12)/9) + 
    fvu1u1u8*((4*f73*fvu2u1u8)/9 + (2*(2*f73 + f94)*fvu2u1u9)/9 - 
      (2*(3*f73 - f94)*fvu2u1u10)/9 + (5*f73*tci11^2)/54 - 
      (2*(3*f73 - f94)*fvu1u2u8*tci12)/9) + 
    fvu1u1u7*((8*f74 - f76 + f80 - 8*f82 + f87 + 8*f122 - 
        f123 + 8*f153 - f155 - 8*f160 + f161 - 8*f176 + f177)/
       9 + (2*(2*f73 - f94)*fvu1u1u8^2)/9 + 
      (2*(f73 - 3*f94)*fvu2u1u7)/9 + (2*(5*f73 - f94)*fvu2u1u8)/
       9 - (2*(3*f73 - f94)*fvu2u1u10)/9 + 
      (2*(f73 - 3*f94)*tci11^2)/27 + 
      ((-6*f62 + f64 - 6*f92)*tci12)/9 - 
      (2*(f73 - f94)*fvu1u1u8*tci12)/9 - 
      (2*(3*f73 - f94)*fvu1u2u8*tci12)/9) + 
    fvu1u1u4*(-f87/9 + ((-f73 + 3*f94)*fvu1u1u5^2)/18 + 
      (f73*fvu1u1u6^2)/9 + (f73*fvu1u1u7^2)/9 + 
      ((2*f73 + f94)*fvu1u1u8^2)/18 + (f94*fvu1u1u9^2)/18 - 
      (2*(f73 - f94)*fvu2u1u2)/9 - (2*(f73 - f94)*fvu2u1u3)/9 + 
      (4*f73*fvu2u1u8)/9 - (f85*tci12)/3 + 
      ((-2*f73 - f94)*fvu1u1u8*tci12)/9 - 
      (f94*fvu1u1u9*tci12)/9 - (2*(f73 - f94)*fvu1u2u6*
        tci12)/9 + fvu1u1u7*((6*f62 - f64 + 6*f92)/9 + 
        (2*f73*fvu1u1u8)/9 - (2*f73*tci12)/9) + 
      fvu1u1u6*(f100/9 - (2*f73*fvu1u1u7)/9 - (2*f73*fvu1u1u8)/
         9 + (2*f73*tci12)/9) + fvu1u1u5*
       ((-6*f11 + f12 - 6*f90)/9 - (f94*fvu1u1u9)/3 + 
        ((f73 - 3*f94)*tci12)/9)) + 
    fvu1u1u3*(((f73 + f94)*fvu1u1u4^2)/6 + 
      ((-f73 - 2*f94)*fvu1u1u5^2)/6 - (f94*fvu1u1u7^2)/6 + 
      ((2*f73 + f94)*fvu1u1u8^2)/6 + (2*(f73 + f94)*fvu2u1u1)/3 + 
      ((f73 + 2*f94)*fvu2u1u5)/3 + ((-11*f73 + 4*f94)*tci11^2)/
       18 + ((f73 + 2*f94)*fvu1u1u5*tci12)/3 + 
      (f94*fvu1u1u7*tci12)/3 + ((-2*f73 - f94)*fvu1u1u8*
        tci12)/3 + ((-f73 - 2*f94)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-2*f73 - f94)*fvu1u1u8)/3 + 
        ((-f73 - f94)*tci12)/3)) + 
    fvu1u1u6*(-f80/9 - (5*f73*fvu1u1u7^2)/9 + 
      ((-7*f73 + f94)*fvu1u1u8^2)/18 + (2*(7*f73 - f94)*fvu2u1u8)/
       9 - (2*(6*f73 - f94)*tci11^2)/27 - (f100*tci12)/9 + 
      ((7*f73 - f94)*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-6*f62 + f64 - 6*f92)/9 + (2*f94*fvu1u1u8)/9 - 
        (2*f94*tci12)/9)) + fvu1u1u5*
     ((-8*f74 + 8*f82 - f84 - 8*f122 + f123 - 8*f153 + f155 + 
        8*f160 - f161 + 8*f176 - f177)/9 + 
      ((-f73 + f94)*fvu1u1u6^2)/6 + (f94*fvu1u1u7^2)/3 + 
      ((-f73 + 3*f94)*fvu1u1u8^2)/18 + (f94*fvu1u1u9^2)/6 - 
      (2*(f73 - 3*f94)*fvu2u1u4)/9 + ((f73 + 2*f94)*fvu2u1u5)/3 + 
      (2*(f73 - 3*f94)*fvu2u1u7)/9 + 
      ((-13*f73 + 54*f94)*tci11^2)/108 + 
      ((6*f11 - f12 + 6*f90)*tci12)/9 + 
      ((f73 - 3*f94)*fvu1u1u8*tci12)/9 + 
      (f94*fvu1u1u9*tci12)/3 + ((-f73 - 2*f94)*fvu1u2u7*
        tci12)/3 + fvu1u1u7*(-f96/9 - (2*f73*fvu1u1u8)/9 + 
        (2*f73*tci12)/9) + fvu1u1u6*(f99/9 + 
        ((f73 - f94)*fvu1u1u8)/3 + ((-f73 + f94)*tci12)/3)) + 
    fvu1u1u2*(f84/9 + ((-f73 - f94)*fvu1u1u3^2)/6 + 
      ((f73 - 4*f94)*fvu1u1u4^2)/18 + ((f73 - 6*f94)*fvu1u1u5^2)/
       18 + (f73*fvu1u1u6^2)/6 - (f94*fvu1u1u7^2)/6 - 
      (2*f94*fvu1u1u9^2)/9 - (2*(f73 - f94)*fvu2u1u3)/9 - 
      (2*(f73 - 3*f94)*fvu2u1u4)/9 + (2*f94*fvu2u1u11)/9 + 
      ((f73 - 2*f94)*tci11^2)/9 - (f100*tci12)/9 - 
      (2*f94*fvu1u1u9*tci12)/9 - (2*(f73 - f94)*fvu1u2u6*
        tci12)/9 + fvu1u1u5*((6*f11 - f12 + 6*f90)/9 - 
        (f73*tci12)/9) + fvu1u1u6*(-f100/9 + (f73*tci12)/3) + 
      fvu1u1u7*(f96/9 - (f94*tci12)/3) + 
      fvu1u1u3*((f94*fvu1u1u7)/3 + ((f73 + f94)*tci12)/3) + 
      fvu1u1u4*((6*f11 - f12 + 6*f90)/9 + (f73*fvu1u1u5)/9 + 
        (2*f94*fvu1u1u9)/9 + ((f73 + 2*f94)*tci12)/9)) + 
    fvu1u1u1*((f73*fvu1u1u2^2)/6 + ((-3*f73 - 2*f94)*fvu1u1u3^2)/
       6 + ((-5*f73 - 4*f94)*fvu1u1u4^2)/18 + 
      ((f73 + 2*f94)*fvu1u1u5^2)/6 - (5*f73*fvu1u1u6^2)/18 + 
      (2*f73*fvu1u1u7^2)/9 - (2*(2*f73 + f94)*fvu1u1u8^2)/9 + 
      (2*(f73 + f94)*fvu2u1u1)/3 - (2*(f73 - f94)*fvu2u1u2)/9 - 
      (4*f73*fvu2u1u8)/9 + (2*(2*f73 + f94)*fvu2u1u9)/9 + 
      ((-11*f73 - 16*f94)*tci11^2)/54 + 
      (4*(2*f73 + f94)*fvu1u1u8*tci12)/9 + 
      fvu1u1u2*(-(f73*fvu1u1u6)/3 - (2*f73*tci12)/3) + 
      fvu1u1u6*((2*f73*fvu1u1u7)/9 + (2*f73*fvu1u1u8)/9 - 
        (5*f73*tci12)/9) + fvu1u1u7*((-2*f73*fvu1u1u8)/9 + 
        (2*f73*tci12)/9) + fvu1u1u3*((f73*fvu1u1u4)/3 + 
        ((3*f73 + 2*f94)*tci12)/3) + 
      fvu1u1u4*((2*f73*fvu1u1u6)/9 - (2*f73*fvu1u1u7)/9 + 
        (2*(2*f73 + f94)*fvu1u1u8)/9 + ((5*f73 + 4*f94)*tci12)/
         9)) + ((7*f73 + 8*f94)*tci12*tcr11^2)/18 + 
    ((-9*f73 - 4*f94)*tcr11^3)/18 + 
    ((-16*f73 - 3*f90 + 3*f92 - 4*f94)*tci11^2*tcr12)/18 + 
    (10*(f73 + 2*f94)*tci12*tcr21)/9 + 
    tcr11*(((-f73 + 16*f94)*tci11^2)/54 + 
      (2*f73*tci12*tcr12)/3 - (2*(4*f73 + 5*f94)*tcr21)/
       9) + ((26*f73 - 9*f90 + 9*f92 - 12*f94 - 12*f97)*tcr33)/
     36;
L ieu1ueu7 = f76/9 + 
    ((f73 - 2*f90 - f94)*fvu1u1u2^2)/6 + 
    ((-f73 - f92)*fvu1u1u4^2)/6 + ((f94 - f97)*fvu1u1u5^2)/6 + 
    ((f92 - f97)*fvu1u1u6^2)/6 + 
    fvu1u1u4*(-f85/3 - (f90*fvu1u1u5)/3 + (f73*fvu1u1u6)/3 + 
      (f92*fvu1u1u7)/3) + ((f73 - f90)*fvu2u1u3)/3 + 
    ((-f90 - f94)*fvu2u1u4)/3 + ((-f73 - f92)*fvu2u1u6)/3 + 
    ((f94 - f97)*fvu2u1u7)/3 + ((f92 - f97)*fvu2u1u8)/3 + 
    ((-4*f73 - f92 + f94 + f97)*tci11^2)/18 + 
    (f85*tci12)/3 + ((f73 + f92)*fvu1u2u3*tci12)/3 + 
    ((f73 - f90)*fvu1u2u6*tci12)/3 + 
    fvu1u1u6*(-f78/3 - (f92*fvu1u1u7)/3 - (f73*tci12)/3) + 
    fvu1u1u2*(f82/3 + (f90*fvu1u1u4)/3 + (f90*fvu1u1u5)/3 - 
      (f73*fvu1u1u6)/3 + (f94*fvu1u1u7)/3 - (f73*tci12)/3) + 
    fvu1u1u5*(-f74/3 + (f97*fvu1u1u6)/3 - (f94*fvu1u1u7)/3 + 
      (f90*tci12)/3) + fvu1u1u7*
     ((f74 - f75 + f78 - f82 + f85)/3 - (f92*tci12)/3);
L ieu0ueu7 = f75/3;
L ieum1ueu7 = 0;
L ieum2ueu7 = 0;
L ieu2uou7 = -(f88*fvu4u25) - f88*fvu4u28 - f88*fvu4u39 - 
    f88*fvu4u41 + (f88*fvu4u49)/3 - (f88*fvu4u51)/3 - 
    f88*fvu4u80 - f88*fvu4u83 - f88*fvu4u91 - f88*fvu4u93 + 
    (f88*fvu4u100)/3 - (f88*fvu4u102)/3 + (7*f88*fvu4u171)/4 + 
    (5*f88*fvu4u174)/4 - (3*f88*fvu4u182)/4 - (f88*fvu4u184)/4 - 
    (f88*fvu4u190)/3 - (f88*fvu4u192)/2 + (f88*fvu4u199)/4 + 
    (f88*fvu4u201)/2 + f88*fvu4u202 - 2*f88*fvu4u213 + 
    (2*f88*fvu4u225)/3 + 7*f88*fvu4u244 - (3*f88*fvu4u252)/2 - 
    (7*f88*fvu4u255)/6 + (f88*fvu4u271)/4 - (17*f88*fvu4u273)/4 + 
    (17*f88*fvu4u277)/4 + (f88*fvu4u279)/4 - (f88*fvu4u282)/3 - 
    (f88*fvu4u289)/4 - (f88*fvu4u290)/2 - f88*fvu4u291 - 
    2*f88*fvu4u293 - 2*f88*fvu4u309 + 2*f88*fvu4u325 - 
    2*f88*fvu4u330 + ((f88*fvu3u45)/2 - (f88*fvu3u70)/2 - 
      (f88*fvu3u81)/2)*fvu1u1u1 + fvu3u78*(-(f88*fvu1u1u4) - 
      f88*fvu1u1u5 + f88*fvu1u1u6) + 
    fvu3u25*((f88*fvu1u1u1)/2 - (f88*fvu1u1u3)/2 + 
      (f88*fvu1u1u6)/2 - (f88*fvu1u1u7)/2) + 
    fvu3u81*((f88*fvu1u1u3)/2 + (f88*fvu1u1u6)/2 - 
      (f88*fvu1u1u7)/2) + fvu3u23*((f88*fvu1u1u2)/2 + 
      (f88*fvu1u1u6)/2 - (f88*fvu1u1u7)/2 - (f88*fvu1u1u9)/2) + 
    fvu3u70*((f88*fvu1u1u2)/2 - (f88*fvu1u1u3)/2 + 
      f88*fvu1u1u4 + f88*fvu1u1u5 + f88*fvu1u1u7 - 
      (f88*fvu1u1u9)/2) + fvu3u45*(-(f88*fvu1u1u2)/2 - 
      (f88*fvu1u1u3)/2 + (f88*fvu1u1u9)/2) + 
    fvu3u43*(-(f88*fvu1u1u6)/2 + (f88*fvu1u1u7)/2 + 
      (f88*fvu1u1u9)/2) + (151*f88*tci11^3*tci12)/135 + 
    fvu3u71*(f88*fvu1u1u4 - f88*fvu1u1u5 - f88*fvu1u1u6 - 
      2*f88*tci12) + fvu3u82*(f88*fvu1u1u4 - f88*fvu1u1u5 - 
      f88*fvu1u1u6 - 2*f88*tci12) + 
    fvu3u80*((-5*f88*fvu1u1u6)/2 + (5*f88*fvu1u1u7)/2 - 
      (7*f88*fvu1u1u8)/2 + (5*f88*fvu1u1u9)/2 + 
      (7*f88*tci12)/2) + (2*f88*tci11^2*tci21)/9 + 
    fvu1u1u4*((4*f88*tci11^3)/27 - 4*f88*tci12*tci21) + 
    fvu1u1u5*((4*f88*tci11^3)/27 - 4*f88*tci12*tci21) + 
    fvu1u1u3*(f88*fvu3u71 + f88*fvu3u78 + f88*fvu3u82 - 
      (4*f88*tci11^3)/27 + 4*f88*tci12*tci21) + 
    fvu1u1u8*((-7*f88*tci11^3)/27 + 7*f88*tci12*tci21) + 
    tci12*((48*f88*tci31)/5 + 16*f88*tci32) + 
    (4*f88*tci41)/3 + (4*f88*tci11^3*tcr11)/27 - 
    (f88*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u7*((19*f88*tci11^3)/60 - (23*f88*tci12*tci21)/
       3 + (12*f88*tci31)/5 + f88*tci21*tcr11 - 
      (f88*tci11*tcr11^2)/20) + 
    fvu1u1u9*((91*f88*tci11^3)/540 - (11*f88*tci12*tci21)/
       3 + (12*f88*tci31)/5 + f88*tci21*tcr11 - 
      (f88*tci11*tcr11^2)/20) + 
    fvu1u1u2*(-(f88*fvu3u43)/2 + f88*fvu3u80 + 
      (49*f88*tci11^3)/540 - (10*f88*tci12*tci21)/3 - 
      (12*f88*tci31)/5 - f88*tci21*tcr11 + 
      (f88*tci11*tcr11^2)/20) + 
    fvu1u1u6*((-19*f88*tci11^3)/60 + (23*f88*tci12*tci21)/
       3 - (12*f88*tci31)/5 - f88*tci21*tcr11 + 
      (f88*tci11*tcr11^2)/20) - 
    (40*f88*tci12*tci21*tcr12)/3 - 4*f88*tci11*tci12*
     tcr12^2;
L ieu1uou7 = 2*f88*fvu3u70 + 
    (4*f88*tci11^3)/27 - 4*f88*tci12*tci21;
L ieu0uou7 = 0;
L ieum1uou7 = 0;
L ieum2uou7 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou7+w^2*ieu1uou7+w^3*ieu0uou7+w^4*ieum1uou7+w^5*ieum2uou7;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou7a = K[w^1];
L ieu1uou7a = K[w^2];
L ieu0uou7a = K[w^3];
L ieum1uou7a = K[w^4];
L ieum2uou7a = K[w^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_odd.c> "%O"
#write <e7_odd.c> "return Eps5o2<T>("
#write <e7_odd.c> "%E", ieu2uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu0uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum2uou7a
#write <e7_odd.c> ");\n}"
L H=+u^1*ieu2ueu7+u^2*ieu1ueu7+u^3*ieu0ueu7+u^4*ieum1ueu7+u^5*ieum2ueu7;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu7a = H[u^1];
L ieu1ueu7a = H[u^2];
L ieu0ueu7a = H[u^3];
L ieum1ueu7a = H[u^4];
L ieum2ueu7a = H[u^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_even.c> "%O"
#write <e7_even.c> "return Eps5o2<T>("
#write <e7_even.c> "%E", ieu2ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu0ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum2ueu7a
#write <e7_even.c> ");\n}"
.end
