#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f136;
S f180;
S f181;
S f134;
S f132;
S f184;
S f130;
S f185;
S f139;
S f124;
S f75;
S f125;
S f126;
S f127;
S f121;
S f122;
S f123;
S f128;
S f78;
S f129;
S f79;
S f153;
S f155;
S f9;
S f8;
S f148;
S f81;
S f16;
S f3;
S f147;
S f80;
S f83;
S f82;
S f15;
S f7;
S f142;
S f84;
S f6;
S f4;
S f173;
S f22;
S f177;
S f27;
S f176;
S f26;
S f25;
S f175;
S f174;
S f24;
S f38;
S f119;
S f49;
S f48;
S f118;
S f160;
S f30;
S f161;
S f46;
S f33;
S f34;
S f40;
S f110;
S f35;
S f43;
S f36;
S f113;
S f166;
S f42;
S f112;
S f167;
S f109;
S f52;
S f102;
S f103;
S f50;
S f106;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu12 = (-99*f3 + 11*f4 - 11*f38 - 11*f106 + 
      63*f121 - 11*f125 - 11*f175)/27 - (2*f118*fvu3u1)/3 + 
    (f118*fvu3u2)/6 + ((f118 - 4*f139)*fvu3u3)/9 - 
    (5*f118*fvu3u4)/12 + ((11*f118 - 28*f139)*fvu3u5)/36 - 
    (5*f118*fvu3u6)/9 + ((f118 - 3*f139)*fvu3u7)/6 + 
    (8*(f118 - 2*f139)*fvu3u8)/9 + ((f118 - 2*f139)*fvu3u11)/9 - 
    (5*f118*fvu3u12)/36 + ((-f118 + 4*f139)*fvu3u13)/36 - 
    (f136*fvu3u15)/6 + ((f118 - 4*f139 - 2*f142)*fvu3u17)/12 - 
    (5*f118*fvu3u18)/12 - (2*f118*fvu3u19)/9 + 
    ((-f118 + 2*f139)*fvu3u24)/9 + (f118*fvu3u51)/9 - 
    (f139*fvu3u52)/18 + (2*f139*fvu3u55)/9 + 
    ((f118 + 3*f134 + 3*f139)*fvu3u56)/18 + 
    ((f118 + f139)*fvu3u57)/6 - (f139*fvu3u59)/6 - 
    (f118*fvu3u61)/3 - (5*f118*fvu1u1u1^3)/6 + 
    ((11*f118 - 52*f139)*fvu1u1u2^3)/54 + 
    ((f118 + f139)*fvu1u1u3^3)/9 - (2*f118*fvu1u1u5^3)/9 + 
    (f139*fvu1u1u6^3)/9 + ((-f118 - 6*f139)*fvu1u1u8^3)/18 + 
    (5*(f118 + f139)*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(((f118 - 4*f139)*fvu1u1u5)/36 - 
      (5*f118*fvu1u1u8)/36 + ((7*f118 - 20*f139)*fvu1u1u9)/36) + 
    ((6*f15 - f16 + 6*f102 - f103 + 6*f136 - 6*f142)*fvu2u1u4)/
     9 + ((6*f15 - f16 - 6*f118 + 6*f136 + 6*f166 - f167)*
      fvu2u1u5)/9 + ((6*f112 - f113 + 6*f134 - 6*f139 + 6*f184 - 
       f185)*fvu2u1u15)/9 + ((-3*f118 + 8*f139)*fvu1u2u6*
      tci11^2)/9 + ((f118 - 2*f139)*fvu1u2u9*tci11^2)/3 + 
    ((19*f3 - 2*f4 - 16*f6 + 2*f7 - 16*f8 + 2*f9 + f22 + 
       16*f24 - 2*f25 - 16*f26 + 2*f27 + 16*f30 - 2*f33 - 
       8*f34 + 2*f35 + f36 + 2*f38 + 16*f43 - 2*f46 - 8*f48 + 
       8*f49 + f50 - f52 + 2*f75 + 16*f78 + 16*f79 - 2*f80 - 
       2*f81 + 8*f82 - 2*f83 - f84 + 16*f109 - 2*f110 + 
       2*f121 - 16*f122 + 2*f123 + 16*f124 + 2*f125 - 2*f126 - 
       8*f130 + 16*f160 - 2*f161 + 8*f173 - f174 + 2*f175)*
      tci12)/9 + ((-6*f15 + f16 + 6*f118 - 6*f136 - 6*f166 + 
       f167)*fvu1u2u7*tci12)/9 + (10*f118*fvu2u1u1*tci12)/3 + 
    (10*f118*fvu2u1u2*tci12)/9 + 
    ((-3*f118 + 8*f139)*fvu2u1u3*tci12)/9 + 
    (10*f118*fvu2u1u9*tci12)/9 + 
    ((-f118 + 2*f139)*fvu2u1u12*tci12)/3 + 
    fvu1u1u1^2*(((-f118 + 2*f139)*fvu1u1u2)/18 + 
      (5*f118*fvu1u1u4)/18 + (f118*fvu1u1u5)/6 + 
      (5*f118*fvu1u1u8)/18 + ((f118 - 2*f139)*fvu1u1u9)/18 + 
      (f118*fvu1u1u10)/3 + (35*f118*tci12)/18) + 
    fvu2u1u11*((6*f102 - f103 + 6*f139 - 6*f142 - 6*f184 + 
        f185)/9 - (2*(f118 - 2*f139)*tci12)/9) + 
    fvu1u1u3^2*((6*f15 - f16 + 6*f112 - f113 - 12*f118 - 
        3*f130 + 6*f134 + 6*f136 + 12*f166 - 2*f167)/18 - 
      (5*f118*fvu1u1u4)/12 - (f139*fvu1u1u6)/18 - 
      (5*f118*fvu1u1u8)/12 + ((-f118 - 3*f139)*fvu1u1u9)/18 + 
      ((-f118 + 2*f139)*fvu1u1u10)/18 + ((-f118 - f139)*tci12)/
       9) + fvu1u1u10^2*(-f127/6 - (f139*tci12)/6) + 
    fvu1u1u6^2*(-(f139*fvu1u1u9)/6 + (f139*fvu1u1u10)/9 + 
      (f139*tci12)/18) + fvu2u1u13*
     ((6*f112 - f113 - 6*f118 + 6*f134 + 6*f166 - f167)/9 - 
      (2*(f118 + f139)*tci12)/9) + 
    fvu1u1u8^2*(((f118 - 2*f139)*fvu1u1u9)/18 + 
      ((-f118 + f139)*fvu1u1u10)/6 + ((3*f118 + 2*f139)*tci12)/
       6) + fvu1u1u5^2*(-f124/6 + ((f118 - 4*f139)*fvu1u1u9)/12 + 
      (f118*fvu1u1u10)/6 + ((5*f118 + 4*f139)*tci12)/36) + 
    fvu1u1u2^2*((6*f15 - f16 + 12*f102 - 2*f103 - 3*f119 + 
        3*f121 + 3*f124 + 3*f127 + 3*f130 + 6*f136 + 6*f139 - 
        12*f142 - 6*f184 + f185)/18 + (f118*fvu1u1u4)/18 + 
      ((-f118 + 4*f139)*fvu1u1u5)/18 + ((f118 + f139)*fvu1u1u8)/
       18 + ((f118 - 2*f139)*fvu1u1u9)/9 + (f139*fvu1u1u10)/6 + 
      ((-4*f118 + 7*f139)*tci12)/18) + 
    tci11^2*((12*f15 - 2*f16 + 12*f102 - 2*f103 + 12*f112 - 
        2*f113 - 60*f118 + 3*f121 - 18*f130 + 12*f134 + 12*f136 - 
        12*f139 - 12*f142 + 60*f166 - 10*f167 + 12*f184 - 2*f185)/
       108 + ((-20*f118 + 3*f134 + 3*f136 + 12*f139)*tci12)/
       108) + fvu1u1u9^2*((6*f112 - f113 + 3*f119 + 6*f134 - 
        6*f139 + 6*f184 - f185)/18 - (f118*fvu1u1u10)/18 + 
      ((-5*f118 + 26*f139)*tci12)/36) + 
    fvu1u2u2*((2*(f118 + f139)*tci11^2)/9 + 
      ((-6*f112 + f113 + 6*f118 - 6*f134 - 6*f166 + f167)*
        tci12)/9) + fvu1u1u6*(-(f139*fvu1u1u9^2)/6 + 
      (2*f139*fvu2u1u14)/9 - (2*f139*tci11^2)/27 - 
      (f139*fvu1u1u9*tci12)/3 + (2*f139*fvu1u1u10*tci12)/9) + 
    fvu1u1u10*((20*f3 - 2*f4 - 16*f6 + 2*f7 - 8*f8 + f9 + 
        2*f22 + 16*f24 - 2*f25 - 16*f26 + 2*f27 + 2*f35 + f38 - 
        16*f40 + 2*f42 + 16*f43 - 2*f46 - 16*f48 + 8*f49 + 
        2*f50 - f52 + 2*f75 + 16*f79 - 2*f81 + 8*f82 - f84 + 
        f106 + 16*f109 - 2*f110 + 8*f122 - f123 + 16*f124 + 
        2*f125 - 2*f126 + 8*f127 + 16*f128 - 2*f129 + 8*f153 - 
        f155 + 8*f160 - f161 + 16*f173 - 2*f174 + 2*f175 - 
        8*f180 + f181)/9 + (2*(f118 + f139)*fvu2u1u13)/9 + 
      (2*f139*fvu2u1u14)/9 + (2*(f118 + 3*f139)*fvu2u1u15)/9 + 
      ((4*f118 + 7*f139)*tci11^2)/27 + 
      ((-6*f118 + 6*f166 - f167)*tci12)/9 - 
      (2*(f118 + f139)*fvu1u2u2*tci12)/9) + 
    fvu1u1u8*(((-f118 + 2*f139)*fvu1u1u9^2)/9 + 
      (f139*fvu1u1u10^2)/6 - (5*f118*fvu2u1u9)/9 + 
      (2*(f118 - 2*f139)*fvu2u1u11)/9 + 
      ((f118 - 2*f139)*fvu2u1u12)/3 + 
      ((-77*f118 - 18*f139)*tci11^2)/108 + 
      (2*(f118 - 2*f139)*fvu1u1u9*tci12)/9 + 
      ((f118 - f139)*fvu1u1u10*tci12)/3 + 
      ((-f118 + 2*f139)*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((8*f3 - f4 - 8*f6 + f7 - 8*f8 + f9 - f22 + 
        8*f30 - f33 + f35 + f38 + 8*f40 - f42 + 8*f43 - f46 + 
        f75 + 8*f78 + 8*f79 - f80 - f81 - 8*f82 - f83 + f84 + 
        8*f109 - f110 - 8*f119 + f121 - 8*f122 + f123 + 
        8*f124 + f125 - f126 - 8*f128 + f129 + 8*f147 - f148 - 
        8*f173 + f174 + f175 - 8*f176 + f177)/9 + 
      ((-f118 - 2*f139)*fvu1u1u10^2)/6 + 
      ((7*f118 - 20*f139)*fvu2u1u11)/9 + 
      ((f118 - 2*f139)*fvu2u1u12)/3 + 
      (2*(f118 + 3*f139)*fvu2u1u15)/9 + 
      ((11*f118 - 54*f139)*tci11^2)/54 + 
      ((6*f112 - f113 + 6*f134)*tci12)/9 + 
      ((-f118 + 2*f139)*fvu1u2u9*tci12)/3 + 
      fvu1u1u10*((6*f139 - 6*f184 + f185)/9 - (f118*tci12)/
         9)) + fvu1u1u5*(f126/9 + ((f118 - 4*f139)*fvu1u1u9^2)/12 + 
      (f118*fvu1u1u10^2)/6 + ((f118 - 4*f139)*fvu2u1u4)/9 + 
      ((5*f118 - 8*f139)*tci11^2)/54 + 
      ((-6*f118 + 6*f166 - f167)*tci12)/9 + 
      fvu1u1u10*((-6*f118 + 6*f166 - f167)/9 + 
        (f118*tci12)/3) + fvu1u1u9*((6*f102 - f103 - 6*f142)/
         9 + ((f118 - 4*f139)*tci12)/6)) + 
    fvu1u1u1*((-2*(f118 - 2*f139)*fvu1u1u2^2)/9 + 
      (5*f118*fvu1u1u3^2)/6 + (5*f118*fvu1u1u4^2)/9 - 
      (f118*fvu1u1u5^2)/6 + (5*f118*fvu1u1u8^2)/9 + 
      ((f118 - 2*f139)*fvu1u1u9^2)/9 + (f118*fvu1u1u10^2)/6 - 
      (5*f118*fvu2u1u1)/3 - (5*f118*fvu2u1u2)/9 - 
      (5*f118*fvu2u1u9)/9 - (2*(f118 - 2*f139)*fvu2u1u11)/9 + 
      (2*f118*tci11^2)/27 - (5*f118*fvu1u1u3*tci12)/3 + 
      ((f118 - 2*f139)*fvu1u1u9*tci12)/9 - 
      (2*f118*fvu1u1u10*tci12)/3 + fvu1u1u8*
       (((-f118 + 2*f139)*fvu1u1u9)/9 - (10*f118*tci12)/9) + 
      fvu1u1u4*((-5*f118*fvu1u1u8)/9 + 
        ((-f118 + 2*f139)*fvu1u1u9)/9 - (10*f118*tci12)/9) + 
      fvu1u1u5*(-(f118*fvu1u1u10)/3 - (f118*tci12)/3) + 
      fvu1u1u2*(((f118 - 2*f139)*fvu1u1u4)/9 + 
        ((f118 - 2*f139)*fvu1u1u8)/9 + ((f118 - 2*f139)*fvu1u1u9)/
         9 + ((-f118 + 2*f139)*tci12)/9)) + 
    fvu1u1u3*((-19*f3 + 2*f4 + 16*f6 - 2*f7 + 16*f8 - 2*f9 - 
        f22 - 16*f24 + 2*f25 + 16*f26 - 2*f27 - 16*f30 + 
        2*f33 + 8*f34 - 2*f35 - f36 - 2*f38 - 16*f43 + 2*f46 + 
        8*f48 - 8*f49 - f50 + f52 - 2*f75 - 16*f78 - 16*f79 + 
        2*f80 + 2*f81 - 8*f82 + 2*f83 + f84 - 16*f109 + 
        2*f110 - 2*f121 + 16*f122 - 2*f123 - 16*f124 - 2*f125 + 
        2*f126 + 8*f130 - 16*f160 + 2*f161 - 8*f173 + f174 - 
        2*f175)/9 - (5*f118*fvu1u1u4^2)/12 + 
      ((6*f118 - 6*f166 + f167)*fvu1u1u5)/9 - 
      (f139*fvu1u1u6^2)/18 - (5*f118*fvu1u1u8^2)/12 + 
      ((-f118 - 3*f139)*fvu1u1u9^2)/18 - (f118*fvu1u1u10^2)/6 - 
      (5*f118*fvu2u1u1)/3 + (2*(f118 + f139)*fvu2u1u13)/9 + 
      ((-3*f118 + 2*f139)*tci11^2)/36 + (f130*tci12)/3 + 
      (5*f118*fvu1u1u8*tci12)/6 - (2*(f118 + f139)*fvu1u2u2*
        tci12)/9 + fvu1u1u10*((6*f118 - 6*f166 + f167)/9 + 
        (f118*tci12)/3) + fvu1u1u4*((5*f118*fvu1u1u8)/6 + 
        (5*f118*tci12)/6) + fvu1u1u6*((f139*fvu1u1u9)/3 - 
        (2*f139*fvu1u1u10)/9 + (f139*tci12)/9) + 
      fvu1u1u9*((-6*f112 + f113 - 6*f134)/9 + (f118*fvu1u1u10)/
         9 + ((f118 + 3*f139)*tci12)/9)) + 
    fvu1u1u4*(((f118 - 4*f139)*fvu1u1u5^2)/36 - 
      (5*f118*fvu1u1u8^2)/36 + ((7*f118 - 20*f139)*fvu1u1u9^2)/36 - 
      (5*f118*fvu2u1u2)/9 + ((3*f118 - 8*f139)*fvu2u1u3)/9 + 
      (2*(f118 - 2*f139)*fvu2u1u11)/9 - 
      (2*(f118 - f139)*tci11^2)/27 + 
      ((-7*f118 + 20*f139)*fvu1u1u9*tci12)/18 + 
      ((3*f118 - 8*f139)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(((f118 - 2*f139)*fvu1u1u9)/9 + (5*f118*tci12)/
         18) + fvu1u1u5*(((-f118 + 4*f139)*fvu1u1u9)/6 + 
        ((-f118 + 4*f139)*tci12)/18)) + 
    fvu1u1u2*((8*f6 - f7 + 8*f30 - f33 - 8*f34 - f35 + f36 + 
        f38 + 8*f40 - f42 - 8*f43 + f46 + 8*f48 - f50 - f75 + 
        8*f78 - 8*f79 - f80 + f81 + 8*f82 - f83 - f84 - 
        8*f109 + f110 + 8*f119 - 8*f121 - 16*f122 + 2*f123 - 
        8*f124 - 8*f127 - 8*f128 + f129 - 8*f130 - 8*f147 + 
        f148 - 8*f153 + f155 + 8*f160 - f161 + 8*f176 - f177 + 
        8*f180 - f181)/9 + ((-6*f15 + f16 - 6*f136)*fvu1u1u3)/9 - 
      (2*(f118 - 3*f139)*fvu1u1u4^2)/9 + 
      ((-f118 + 4*f139)*fvu1u1u5^2)/9 + 
      ((-f118 + 5*f139)*fvu1u1u8^2)/18 + 
      ((-5*f118 + 16*f139)*fvu1u1u9^2)/18 + (f139*fvu1u1u10^2)/6 + 
      ((3*f118 - 8*f139)*fvu2u1u3)/9 + ((f118 - 4*f139)*fvu2u1u4)/
       9 + ((5*f118 - 16*f139)*fvu2u1u11)/9 + 
      ((-f118 + 8*f139)*tci11^2)/27 + 
      ((6*f15 - f16 + 6*f136)*tci12)/9 + 
      ((3*f118 - 8*f139)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(((-f118 + 2*f139)*fvu1u1u9)/9 - 
        (f139*fvu1u1u10)/3 + ((f118 - 5*f139)*tci12)/9) + 
      fvu1u1u4*(((f118 - 4*f139)*fvu1u1u5)/9 + 
        ((-f118 + 2*f139)*fvu1u1u8)/9 - 
        (2*(f118 - 2*f139)*fvu1u1u9)/9 + 
        ((f118 - 4*f139)*tci12)/9) + 
      fvu1u1u9*((-6*f102 + f103 + 6*f142)/9 + 
        (2*(f118 - 2*f139)*tci12)/9) + 
      fvu1u1u10*((-6*f139 + 6*f184 - f185)/9 + 
        (f139*tci12)/3) + fvu1u1u5*((-6*f102 + f103 + 6*f142)/
         9 + ((-f118 + 4*f139)*tci12)/9)) - 
    (41*f118*tci12*tcr11^2)/18 + (5*f118*tcr11^3)/6 + 
    ((2*f118 - 3*f134 - 3*f136 - 8*f139)*tci11^2*tcr12)/18 - 
    (56*f118*tci12*tcr21)/9 + 
    tcr11*((-8*f118*tci11^2)/27 + (2*f118*tci12*tcr12)/
       3 + (25*f118*tcr21)/9) + 
    ((26*f118 - 9*f134 - 9*f136 + 12*f139 + 12*f142)*tcr33)/36;
L ieu1ueu12 = (-9*f3 + f4 - f38 - f106 + 9*f121 - 
      f125 - f175)/9 + ((f136 + f139 - 2*f142)*fvu1u1u2^2)/6 + 
    ((-2*f118 + f134 + f136)*fvu1u1u3^2)/6 + 
    ((f134 - f139)*fvu1u1u9^2)/6 + 
    fvu1u1u3*(f130/3 + (f118*fvu1u1u5)/3 - (f134*fvu1u1u9)/3 + 
      (f118*fvu1u1u10)/3) + ((f136 - f142)*fvu2u1u4)/3 + 
    ((-f118 + f136)*fvu2u1u5)/3 + ((f139 - f142)*fvu2u1u11)/3 + 
    ((-f118 + f134)*fvu2u1u13)/3 + ((f134 - f139)*fvu2u1u15)/3 + 
    ((-5*f118 + f134 + f136 - f139 - f142)*tci11^2)/18 - 
    (f130*tci12)/3 + ((f118 - f134)*fvu1u2u2*tci12)/3 + 
    ((f118 - f136)*fvu1u2u7*tci12)/3 + 
    fvu1u1u10*(f127/3 - (f118*tci12)/3) + 
    fvu1u1u5*(f124/3 - (f142*fvu1u1u9)/3 - (f118*fvu1u1u10)/3 - 
      (f118*tci12)/3) + fvu1u1u9*(-f119/3 + 
      (f139*fvu1u1u10)/3 + (f134*tci12)/3) + 
    fvu1u1u2*((f119 - f121 - f124 - f127 - f130)/3 - 
      (f136*fvu1u1u3)/3 + (f142*fvu1u1u5)/3 + (f142*fvu1u1u9)/3 - 
      (f139*fvu1u1u10)/3 + (f136*tci12)/3);
L ieu0ueu12 = f121/3;
L ieum1ueu12 = 0;
L ieum2ueu12 = 0;
L ieu2uou12 = (-2*f132*fvu4u25)/3 - (8*f132*fvu4u28)/3 - 
    (8*f132*fvu4u39)/3 - (2*f132*fvu4u41)/3 - (10*f132*fvu4u51)/9 - 
    (23*f132*fvu4u80)/12 - (41*f132*fvu4u83)/12 - 
    (19*f132*fvu4u91)/4 - (23*f132*fvu4u93)/12 + 
    (4*f132*fvu4u100)/3 - (25*f132*fvu4u102)/18 + 
    (f132*fvu4u111)/4 + (f132*fvu4u113)/2 + f132*fvu4u114 + 
    (2*f132*fvu4u129)/3 + (17*f132*fvu4u132)/3 - 
    (5*f132*fvu4u139)/6 + (2*f132*fvu4u141)/3 + 
    (2*f132*fvu4u146)/9 + (5*f132*fvu4u148)/18 + 
    (f132*fvu4u171)/12 + (31*f132*fvu4u174)/12 + 
    (31*f132*fvu4u182)/12 + (f132*fvu4u184)/12 + 
    (2*f132*fvu4u190)/9 + (7*f132*fvu4u192)/18 + (f132*fvu4u199)/4 + 
    (f132*fvu4u201)/2 + f132*fvu4u202 - (25*f132*fvu4u213)/12 - 
    (9*f132*fvu4u215)/4 + (17*f132*fvu4u219)/4 - 
    (25*f132*fvu4u221)/12 + (4*f132*fvu4u225)/9 - 
    (f132*fvu4u233)/4 - (f132*fvu4u234)/2 - f132*fvu4u235 + 
    (49*f132*fvu4u244)/3 - (4*f132*fvu4u246)/3 - 
    (3*f132*fvu4u252)/2 - (49*f132*fvu4u255)/18 + 
    (23*f132*fvu4u271)/12 - (107*f132*fvu4u273)/12 + 
    (107*f132*fvu4u277)/12 + (23*f132*fvu4u279)/12 - 
    (4*f132*fvu4u282)/3 - (f132*fvu4u289)/4 - (f132*fvu4u290)/2 - 
    f132*fvu4u291 - (4*f132*fvu4u293)/3 + (8*f132*fvu4u295)/3 - 
    2*f132*fvu4u330 + fvu3u71*((f132*fvu1u1u2)/3 + 
      (5*f132*fvu1u1u3)/3 - (f132*fvu1u1u5)/3 - 
      (5*f132*fvu1u1u6)/3 + (5*f132*fvu1u1u7)/3 - 
      (f132*fvu1u1u8)/3) + fvu3u70*((f132*fvu1u1u2)/3 - 
      (f132*fvu1u1u3)/3 - (f132*fvu1u1u9)/3) + 
    fvu3u78*((-2*f132*fvu1u1u2)/3 + (2*f132*fvu1u1u3)/3 + 
      (2*f132*fvu1u1u9)/3) + fvu3u45*(-2*f132*fvu1u1u2 - 
      (4*f132*fvu1u1u3)/3 + (2*f132*fvu1u1u6)/3 - 
      (2*f132*fvu1u1u7)/3 + 2*f132*fvu1u1u9) + 
    fvu3u63*((4*f132*fvu1u1u2)/3 + (5*f132*fvu1u1u3)/2 + 
      (f132*fvu1u1u5)/3 + (3*f132*fvu1u1u6)/2 - 
      (2*f132*fvu1u1u7)/3 + (f132*fvu1u1u8)/3 - 
      (5*f132*fvu1u1u9)/3 - (5*f132*fvu1u1u10)/6) + 
    fvu3u43*((-4*f132*fvu1u1u2)/3 - (2*f132*fvu1u1u4)/3 - 
      (4*f132*fvu1u1u6)/3 + (4*f132*fvu1u1u7)/3 + 
      2*f132*fvu1u1u9 - (2*f132*fvu1u1u10)/3) + 
    fvu3u25*((f132*fvu1u1u2)/3 - (4*f132*fvu1u1u3)/3 - 
      (f132*fvu1u1u5)/3 + (5*f132*fvu1u1u6)/3 - 
      (4*f132*fvu1u1u7)/3 - (f132*fvu1u1u10)/3) + 
    fvu3u62*((f132*fvu1u1u2)/3 - (2*f132*fvu1u1u4)/3 + 
      (f132*fvu1u1u5)/3 + (f132*fvu1u1u6)/3 - 
      (2*f132*fvu1u1u7)/3 - (f132*fvu1u1u10)/3) + 
    fvu3u23*(-(f132*fvu1u1u1)/3 + (f132*fvu1u1u2)/3 - 
      (f132*fvu1u1u7)/3 + (f132*fvu1u1u8)/3 - (f132*fvu1u1u9)/3 + 
      (f132*fvu1u1u10)/3) + fvu3u81*((4*f132*fvu1u1u3)/3 + 
      (2*f132*fvu1u1u4)/3 + (4*f132*fvu1u1u6)/3 - 
      (4*f132*fvu1u1u7)/3 - (2*f132*fvu1u1u9)/3 + 
      (2*f132*fvu1u1u10)/3) - (151*f132*tci11^3*tci12)/135 + 
    fvu3u82*(-(f132*fvu1u1u2) - (f132*fvu1u1u3)/3 + 
      (2*f132*fvu1u1u4)/3 - 2*f132*fvu1u1u5 - f132*fvu1u1u6 + 
      (4*f132*fvu1u1u7)/3 - (f132*fvu1u1u8)/3 + 
      (f132*fvu1u1u9)/3 - f132*fvu1u1u10 - (4*f132*tci12)/3) + 
    fvu3u80*((-29*f132*fvu1u1u6)/6 + (29*f132*fvu1u1u7)/6 - 
      (49*f132*fvu1u1u8)/6 + (29*f132*fvu1u1u9)/6 + 
      (49*f132*tci12)/6) - (2*f132*tci11^2*tci21)/9 + 
    fvu1u1u8*((-49*f132*tci11^3)/81 + (49*f132*tci12*tci21)/
       3) + tci12*((-48*f132*tci31)/5 - 16*f132*tci32) - 
    (4*f132*tci41)/3 + ((-151*f132*tci11^3)/324 + 
      13*f132*tci12*tci21 + 12*f132*tci31)*tcr11 + 
    ((f132*tci11*tci12)/5 + 5*f132*tci21)*tcr11^2 - 
    (f132*tci11*tcr11^3)/4 + 
    fvu1u1u3*((-71*f132*tci11^3)/324 + 
      (31*f132*tci12*tci21)/3 + 12*f132*tci31 + 
      5*f132*tci21*tcr11 - (f132*tci11*tcr11^2)/4) + 
    fvu1u1u2*((10*f132*fvu3u80)/3 + (47*f132*tci11^3)/162 - 
      (44*f132*tci12*tci21)/9 + 8*f132*tci31 + 
      (10*f132*tci21*tcr11)/3 - (f132*tci11*tcr11^2)/6) + 
    fvu1u1u7*((383*f132*tci11^3)/810 - 11*f132*tci12*
       tci21 + (24*f132*tci31)/5 + 2*f132*tci21*tcr11 - 
      (f132*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-121*f132*tci11^3)/324 + 
      (104*f132*tci12*tci21)/9 + 4*f132*tci31 + 
      (5*f132*tci21*tcr11)/3 - (f132*tci11*tcr11^2)/12) + 
    fvu1u1u5*((-37*f132*tci11^3)/270 + 
      (28*f132*tci12*tci21)/9 - (8*f132*tci31)/5 - 
      (2*f132*tci21*tcr11)/3 + (f132*tci11*tcr11^2)/30) + 
    fvu1u1u9*((41*f132*tci11^3)/135 - (95*f132*tci12*tci21)/
       9 - (32*f132*tci31)/5 - (8*f132*tci21*tcr11)/3 + 
      (2*f132*tci11*tcr11^2)/15) + 
    fvu1u1u10*((-107*f132*tci11^3)/540 + 
      (19*f132*tci12*tci21)/9 - (44*f132*tci31)/5 - 
      (11*f132*tci21*tcr11)/3 + (11*f132*tci11*tcr11^2)/
       60) + fvu1u1u1*((4*f132*fvu3u25)/3 + (2*f132*fvu3u43)/3 + 
      (4*f132*fvu3u45)/3 + (2*f132*fvu3u62)/3 - 
      (17*f132*fvu3u63)/6 + (f132*fvu3u70)/3 - (4*f132*fvu3u71)/3 - 
      (2*f132*fvu3u78)/3 - 2*f132*fvu3u81 + (4*f132*fvu3u82)/3 + 
      (103*f132*tci11^3)/324 - 13*f132*tci12*tci21 - 
      12*f132*tci31 - 5*f132*tci21*tcr11 + 
      (f132*tci11*tcr11^2)/4) + 
    (40*f132*tci12*tci21*tcr12)/3 + 
    4*f132*tci11*tci12*tcr12^2;
L ieu1uou12 = 
   -2*f132*fvu3u82 - (4*f132*tci11^3)/27 + 
    4*f132*tci12*tci21;
L ieu0uou12 = 0;
L ieum1uou12 = 0;
L ieum2uou12 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou12+w^2*ieu1uou12+w^3*ieu0uou12+w^4*ieum1uou12+w^5*ieum2uou12;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou12a = K[w^1];
L ieu1uou12a = K[w^2];
L ieu0uou12a = K[w^3];
L ieum1uou12a = K[w^4];
L ieum2uou12a = K[w^5];
.sort
#write <e12.tmp> "`optimmaxvar_'"
#write <e12_odd.c> "%O"
#write <e12_odd.c> "return Eps5o2<T>("
#write <e12_odd.c> "%E", ieu2uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieu1uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieu0uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieum1uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieum2uou12a
#write <e12_odd.c> ");\n}"
L H=+u^1*ieu2ueu12+u^2*ieu1ueu12+u^3*ieu0ueu12+u^4*ieum1ueu12+u^5*ieum2ueu12;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu12a = H[u^1];
L ieu1ueu12a = H[u^2];
L ieu0ueu12a = H[u^3];
L ieum1ueu12a = H[u^4];
L ieum2ueu12a = H[u^5];
.sort
#write <e12.tmp> "`optimmaxvar_'"
#write <e12_even.c> "%O"
#write <e12_even.c> "return Eps5o2<T>("
#write <e12_even.c> "%E", ieu2ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieu1ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieu0ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieum1ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieum2ueu12a
#write <e12_even.c> ");\n}"
.end
