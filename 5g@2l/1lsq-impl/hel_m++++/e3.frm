#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f180;
S f181;
S f125;
S f75;
S f76;
S f70;
S f121;
S f122;
S f123;
S f128;
S f78;
S f79;
S f129;
S f153;
S f155;
S f9;
S f8;
S f18;
S f81;
S f3;
S f17;
S f80;
S f83;
S f85;
S f87;
S f4;
S f29;
S f179;
S f178;
S f28;
S f173;
S f23;
S f22;
S f21;
S f20;
S f27;
S f26;
S f175;
S f25;
S f174;
S f24;
S f38;
S f39;
S f48;
S f160;
S f30;
S f44;
S f161;
S f47;
S f32;
S f33;
S f34;
S f41;
S f40;
S f35;
S f36;
S f42;
S f37;
S f53;
S f50;
S f51;
S f106;
S f55;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u12;
S fvu2u1u10;
S fvu4u132;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu4u213;
S fvu2u1u5;
S fvu4u215;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u4;
S fvu3u2;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u91;
S fvu4u93;
S fvu4u113;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu3 = (99*f3 - 11*f4 - 63*f22 + 11*f35 + 11*f38 + 
      11*f75 - 11*f83)/27 - (f32*fvu3u1)/3 + 
    ((f32 - f47)*fvu3u2)/6 - (f32*fvu3u4)/18 - (f32*fvu3u6)/9 + 
    (f32*fvu3u12)/18 + (f32*fvu3u14)/9 + (f41*fvu3u18)/6 - 
    (f32*fvu3u19)/9 + (f32*fvu3u31)/9 + (f32*fvu3u32)/18 - 
    (2*f32*fvu3u36)/9 + ((-3*f20 - 2*f32)*fvu3u37)/18 + 
    ((-f32 + f37)*fvu3u38)/6 + (f32*fvu3u40)/6 + (f32*fvu3u42)/3 - 
    (f32*fvu1u1u1^3)/18 + (f32*fvu1u1u3^3)/18 - 
    (f32*fvu1u1u5^3)/3 - (f32*fvu1u1u6^3)/9 + 
    (f32*fvu1u1u4^2*fvu1u1u8)/18 - (f24*fvu1u1u8^2)/6 + 
    ((f39 - f51)*fvu2u1u7)/9 + ((-6*f28 + 6*f32 + f51 + f70)*
      fvu2u1u10)/9 + ((11*f3 - f4 + 16*f8 - 2*f9 - 7*f22 + 
       8*f23 - 8*f26 - 8*f30 + f33 + f35 - f38 - 16*f40 + 
       2*f42 - 16*f48 + 2*f50 - 8*f53 + f55 + f75 - 16*f78 + 
       2*f80 + 2*f106 - f121 + 24*f122 - 3*f123 + 16*f128 - 
       2*f129 + 8*f153 - f155 - 8*f160 + f161 + 16*f173 - 
       2*f174 - 8*f180 + f181)*tci12)/9 + 
    ((-6*f28 + 6*f32 + f51 + f70)*fvu1u2u8*tci12)/9 + 
    fvu1u1u8*(f25/9 + (2*f32*fvu2u1u9)/9 + (f32*tci11^2)/54 + 
      (f24*tci12)/3) + fvu1u1u3^2*
     ((-6*f17 + f18 + 3*f21 - 3*f22 + 3*f23 + 3*f24 - 3*f26 - 
        f39 + 6*f47)/18 - (f32*fvu1u1u4)/18 + (f32*fvu1u1u7)/6 - 
      (f32*tci12)/2) + fvu2u1u9*((-6*f28 + 6*f32 + f44 + f70)/
       9 - (4*f32*tci12)/9) + fvu1u1u5^2*
     ((-3*f23 + f39 - f51)/18 + (f32*fvu1u1u6)/6 - 
      (f32*fvu1u1u7)/18 + (f32*fvu1u1u8)/9 - (f32*tci12)/9) + 
    fvu1u1u6^2*(-(f32*fvu1u1u7)/9 + (f32*fvu1u1u8)/18 - 
      (f32*tci12)/18) + fvu1u1u7^2*
     ((3*f26 - 6*f28 + 6*f32 + f51 + f70)/18 - 
      (f32*fvu1u1u8)/6 + (f32*tci12)/6) + 
    fvu1u1u1^2*((6*f17 - f18 - 3*f21 - 6*f28 + 6*f32 + 2*f44 - 
        6*f47 + f70)/18 - (f32*fvu1u1u3)/18 - (f32*fvu1u1u7)/6 + 
      (f32*fvu1u1u8)/18 + (f32*tci12)/6) + 
    fvu2u1u5*((-6*f17 + f18 - f39 + 6*f47)/9 + 
      (f32*tci12)/3) + fvu2u1u1*((6*f17 - f18 + f44 - 6*f47)/
       9 + (4*f32*tci12)/9) + fvu1u2u7*(-(f32*tci11^2)/3 + 
      ((6*f17 - f18 + f39 - 6*f47)*tci12)/9) + 
    tci11^2*((-21*f22 + 18*f23 - 18*f26 - 12*f28 + 12*f32 - 
        2*f39 + 2*f44 + 2*f70)/108 + 
      ((-f20 + 4*f32 - f37 + 2*f41 - 3*f47)*tci12)/36) + 
    fvu1u1u4*((f32*fvu1u1u8^2)/18 - (f32*fvu1u1u8*tci12)/9) + 
    fvu1u1u3*((-11*f3 + f4 - 8*f21 + 9*f22 - 8*f23 - 8*f24 + 
        8*f26 - 8*f30 + f33 + 8*f34 - f36 - f38 + 8*f48 - 
        f50 + 8*f53 - f55 - 19*f75 + 2*f76 - 8*f79 + f81 + 
        2*f83 + 8*f85 - f87 - 2*f106 - f121 - 8*f122 + f123 + 
        f125 - 8*f153 + f155 + 8*f160 - f161 - 8*f173 + f174 - 
        2*f175 - 8*f178 + f179 + 8*f180 - f181)/9 - 
      (f32*fvu1u1u4^2)/18 + (f32*fvu1u1u5^2)/6 + 
      (f44*fvu1u1u8)/9 - (2*f32*fvu2u1u1)/9 - (f32*fvu2u1u5)/3 + 
      (35*f32*tci11^2)/54 + ((-f21 + f22 - f23 - f24 + f26)*
        tci12)/3 + (f32*fvu1u1u4*tci12)/9 + 
      (f32*fvu1u2u7*tci12)/3 + fvu1u1u7*
       (f39/9 - (f32*tci12)/3) + fvu1u1u5*
       ((6*f17 - f18 - 6*f47)/9 - (f32*tci12)/3)) + 
    fvu1u1u6*((f32*fvu1u1u8^2)/18 - (2*f32*fvu2u1u8)/9 + 
      (2*f32*tci11^2)/27 - (f32*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((2*f32*fvu1u1u8)/9 - (2*f32*tci12)/9)) + 
    fvu1u1u7*(-f27/9 - (f32*fvu1u1u8^2)/6 - (4*f32*fvu2u1u7)/9 - 
      (2*f32*fvu2u1u8)/9 - (f32*tci11^2)/9 - (f39*tci12)/9 + 
      fvu1u1u8*(-f51/9 + (f32*tci12)/3)) + 
    fvu1u1u1*((-16*f8 + 2*f9 + 8*f21 - 2*f22 + 8*f24 - f25 + 
        16*f30 - 2*f33 - 8*f34 - f35 + f36 + 2*f38 + 16*f40 - 
        2*f42 + 8*f48 - f50 + 18*f75 - 2*f76 + 16*f78 + 8*f79 - 
        2*f80 - f81 - 2*f83 - 8*f85 + f87 + 2*f121 - 16*f122 + 
        2*f123 - f125 - 16*f128 + 2*f129 - 8*f173 + f174 + 
        2*f175 + 8*f178 - f179)/9 + (f32*fvu1u1u3^2)/18 + 
      ((-6*f17 + f18 + 6*f47)*fvu1u1u5)/9 - (f32*fvu1u1u5^2)/6 - 
      (f32*fvu1u1u4*fvu1u1u8)/9 - (f32*fvu1u1u8^2)/18 - 
      (2*f32*fvu2u1u1)/9 + (2*f32*fvu2u1u9)/9 - 
      (5*f32*tci11^2)/18 + ((-6*f17 + f18 + 3*f21 + 6*f28 - 
         6*f32 + 6*f47 - f70)*tci12)/9 + 
      fvu1u1u3*(-f44/9 + (f32*fvu1u1u4)/9 - (f32*tci12)/9) + 
      fvu1u1u8*(-f44/9 + (f32*tci12)/9) + 
      fvu1u1u7*((6*f28 - 6*f32 - f70)/9 + (f32*tci12)/3)) + 
    fvu1u1u5*((2*f3 + 16*f8 - 2*f9 + 2*f22 + 8*f23 - 8*f26 + 
        f27 - 8*f30 + f33 - 2*f38 - 16*f40 + 2*f42 - 16*f48 + 
        2*f50 - 8*f53 + f55 - 16*f78 + 2*f80 + f83 + 2*f106 - 
        f121 + 24*f122 - 3*f123 + 16*f128 - 2*f129 + 8*f153 - 
        f155 - 8*f160 + f161 + 16*f173 - 2*f174 - 8*f180 + 
        f181)/9 + (f32*fvu1u1u6^2)/6 + (f32*fvu1u1u7^2)/6 + 
      (f32*fvu1u1u8^2)/9 - (f32*fvu2u1u5)/3 - 
      (4*f32*fvu2u1u7)/9 + (13*f32*tci11^2)/108 - 
      (f51*tci12)/9 + (f32*fvu1u2u7*tci12)/3 + 
      fvu1u1u8*(f51/9 - (2*f32*tci12)/9) + 
      fvu1u1u7*(-f39/9 + (f32*fvu1u1u8)/9 - (f32*tci12)/9) + 
      fvu1u1u6*(-(f32*fvu1u1u8)/3 + (f32*tci12)/3)) + 
    ((2*f32 + 2*f41 - 3*f47)*tci12*tcr11^2)/6 + 
    ((-4*f41 + f47)*tcr11^3)/18 + 
    ((6*f20 + 19*f32 + 6*f37 - 2*f41 - 7*f47)*tci11^2*
      tcr12)/36 + ((-f32 + 2*f41 + f47)*tcr12^3)/9 + 
    (2*(f32 + f41 - f47)*tci12*tcr21)/3 + 
    tcr11*(((3*f32 + 7*f47)*tci11^2)/36 + 
      ((-f32 + f47)*tci12*tcr12)/3 - (2*f41*tcr21)/3) + 
    ((f32 - 2*f41 - f47)*tcr31)/3 + 
    ((f32 - 2*f41 - f47)*tcr32)/12 + 
    ((18*f20 - 11*f32 + 18*f37 + 74*f41 + 11*f47)*tcr33)/72;
L ieu1ueu3 = (9*f3 - f4 - 9*f22 + f35 + f38 + f75 - 
      f83)/9 + ((f32 + 2*f41 - f47)*fvu1u1u1^2)/6 + 
    ((-f37 + f47)*fvu1u1u3^2)/6 + ((-f20 + f37)*fvu1u1u5^2)/6 + 
    ((f20 + f32)*fvu1u1u7^2)/6 + (f24*fvu1u1u8)/3 + 
    fvu1u1u3*((-f21 + f22 - f23 - f24 + f26)/3 - 
      (f47*fvu1u1u5)/3 + (f37*fvu1u1u7)/3 + (f41*fvu1u1u8)/3) + 
    ((f41 - f47)*fvu2u1u1)/3 + ((-f37 + f47)*fvu2u1u5)/3 + 
    ((-f20 + f37)*fvu2u1u7)/3 + ((f32 + f41)*fvu2u1u9)/3 + 
    ((f20 + f32)*fvu2u1u10)/3 + ((f32 - f37 + f41)*tci11^2)/
     18 + ((-f22 + f23 - f26)*tci12)/3 + 
    ((f37 - f47)*fvu1u2u7*tci12)/3 + 
    ((f20 + f32)*fvu1u2u8*tci12)/3 + 
    fvu1u1u5*(f23/3 - (f37*fvu1u1u7)/3 + (f20*fvu1u1u8)/3 - 
      (f20*tci12)/3) + fvu1u1u7*(-f26/3 - (f20*fvu1u1u8)/3 - 
      (f37*tci12)/3) + fvu1u1u1*(f21/3 - (f41*fvu1u1u3)/3 + 
      (f47*fvu1u1u5)/3 - (f32*fvu1u1u7)/3 - (f41*fvu1u1u8)/3 + 
      ((-f32 + f47)*tci12)/3);
L ieu0ueu3 = -f22/3;
L ieum1ueu3 = 0;
L ieum2ueu3 = 0;
L ieu2uou3 = (-4*f29*fvu4u28)/3 - (4*f29*fvu4u39)/3 - 
    (2*f29*fvu4u49)/9 - (4*f29*fvu4u51)/9 - (25*f29*fvu4u80)/12 - 
    (7*f29*fvu4u83)/12 + (3*f29*fvu4u91)/4 - (f29*fvu4u93)/12 + 
    (2*f29*fvu4u100)/9 + (f29*fvu4u102)/2 - (f29*fvu4u111)/4 - 
    (f29*fvu4u113)/2 - f29*fvu4u114 - (4*f29*fvu4u129)/3 - 
    (13*f29*fvu4u132)/3 + (7*f29*fvu4u139)/2 - (4*f29*fvu4u141)/3 + 
    (2*f29*fvu4u146)/9 - (7*f29*fvu4u148)/6 + (4*f29*fvu4u174)/3 + 
    (4*f29*fvu4u182)/3 + (2*f29*fvu4u190)/9 + (4*f29*fvu4u192)/9 - 
    (23*f29*fvu4u213)/12 + (67*f29*fvu4u215)/12 - 
    (9*f29*fvu4u219)/4 + (f29*fvu4u221)/12 + (4*f29*fvu4u225)/9 + 
    (f29*fvu4u233)/4 + (f29*fvu4u234)/2 + f29*fvu4u235 + 
    (4*f29*fvu4u244)/3 - (2*f29*fvu4u246)/3 - 2*f29*fvu4u250 - 
    (2*f29*fvu4u252)/3 - (2*f29*fvu4u255)/9 + 2*f29*fvu4u277 + 
    2*f29*fvu4u325 + fvu3u25*((-2*f29*fvu1u1u3)/3 + 
      (2*f29*fvu1u1u6)/3 - (2*f29*fvu1u1u7)/3) + 
    fvu3u71*((2*f29*fvu1u1u3)/3 - (2*f29*fvu1u1u6)/3 + 
      (2*f29*fvu1u1u7)/3) + fvu3u70*((f29*fvu1u1u6)/3 - 
      (f29*fvu1u1u8)/3 - (f29*fvu1u1u10)/3) + 
    fvu3u45*(-(f29*fvu1u1u2) + (f29*fvu1u1u3)/3 + 
      (2*f29*fvu1u1u5)/3 + (f29*fvu1u1u6)/3 - f29*fvu1u1u8 + 
      (f29*fvu1u1u9)/3 - (f29*fvu1u1u10)/3) + 
    fvu3u23*(-(f29*fvu1u1u1)/3 - (f29*fvu1u1u6)/3 + 
      (f29*fvu1u1u8)/3 + (f29*fvu1u1u10)/3) + 
    fvu3u62*((-2*f29*fvu1u1u2)/3 + (f29*fvu1u1u4)/3 + 
      (f29*fvu1u1u5)/3 - (2*f29*fvu1u1u6)/3 + (f29*fvu1u1u7)/3 + 
      (2*f29*fvu1u1u10)/3) + fvu3u63*(f29*fvu1u1u2 - 
      (3*f29*fvu1u1u3)/2 - (2*f29*fvu1u1u5)/3 - 
      (19*f29*fvu1u1u6)/6 - (f29*fvu1u1u7)/3 - 
      (2*f29*fvu1u1u8)/3 - (f29*fvu1u1u9)/3 + 
      (7*f29*fvu1u1u10)/2) + (13*f29*tci11^3*tci12)/15 + 
    fvu3u78*(-(f29*fvu1u1u2)/3 + (4*f29*fvu1u1u3)/3 - 
      (f29*fvu1u1u4)/3 - f29*fvu1u1u5 + (f29*fvu1u1u6)/3 + 
      (2*f29*fvu1u1u8)/3 - (f29*fvu1u1u9)/3 - 
      (4*f29*tci12)/3) + fvu3u80*((2*f29*fvu1u1u2)/3 + 
      (2*f29*fvu1u1u3)/3 - (f29*fvu1u1u4)/3 - (f29*fvu1u1u5)/3 - 
      (2*f29*fvu1u1u6)/3 + f29*fvu1u1u7 - (2*f29*fvu1u1u8)/3 + 
      (2*f29*tci12)/3) + fvu3u43*((f29*fvu1u1u2)/3 - 
      f29*fvu1u1u3 + (f29*fvu1u1u4)/3 - f29*fvu1u1u5 + 
      (f29*fvu1u1u6)/3 - (f29*fvu1u1u7)/3 - f29*fvu1u1u8 + 
      (f29*fvu1u1u9)/3 + (f29*fvu1u1u10)/3 + 2*f29*tci12) - 
    (10*f29*tci11^2*tci21)/27 + 
    tci12*((16*f29*tci31)/5 + 16*f29*tci32) - 
    (2072*f29*tci41)/135 - (64*f29*tci42)/5 - 
    (16*f29*tci43)/3 + ((53*f29*tci11^3)/108 + 
      (5*f29*tci12*tci21)/3 - (12*f29*tci31)/5 + 
      16*f29*tci32)*tcr11 + (61*f29*tci11*tcr11^3)/180 + 
    fvu1u1u10*((169*f29*tci11^3)/1620 + 
      (25*f29*tci12*tci21)/9 + (76*f29*tci31)/5 + 
      (19*f29*tci21*tcr11)/3 - (19*f29*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f29*tci11^3)/270 + 
      (72*f29*tci31)/5 + 6*f29*tci21*tcr11 - 
      (3*f29*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f29*fvu3u25)/3 - (f29*fvu3u43)/3 + 
      (2*f29*fvu3u45)/3 - (f29*fvu3u62)/3 + (13*f29*fvu3u63)/6 + 
      (f29*fvu3u70)/3 - (2*f29*fvu3u71)/3 - (f29*fvu3u78)/3 - 
      (f29*fvu3u80)/3 + (19*f29*tci11^3)/108 - 
      (f29*tci12*tci21)/3 + 12*f29*tci31 + 
      5*f29*tci21*tcr11 - (f29*tci11*tcr11^2)/4) + 
    fvu1u1u9*((f29*tci11^3)/90 - (8*f29*tci12*tci21)/9 - 
      (8*f29*tci31)/5 - (2*f29*tci21*tcr11)/3 + 
      (f29*tci11*tcr11^2)/30) + 
    fvu1u1u8*((-133*f29*tci11^3)/810 + (8*f29*tci12*tci21)/
       3 - (24*f29*tci31)/5 - 2*f29*tci21*tcr11 + 
      (f29*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f29*tci11^3)/180 - f29*tci12*tci21 - 
      (36*f29*tci31)/5 - 3*f29*tci21*tcr11 + 
      (3*f29*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f29*tci11^3)/162 - (16*f29*tci12*tci21)/
       9 - 8*f29*tci31 - (10*f29*tci21*tcr11)/3 + 
      (f29*tci11*tcr11^2)/6) + 
    fvu1u1u5*((-8*f29*tci11^3)/135 - (28*f29*tci12*tci21)/
       9 - (64*f29*tci31)/5 - (16*f29*tci21*tcr11)/3 + 
      (4*f29*tci11*tcr11^2)/15) + 
    fvu1u1u3*((-391*f29*tci11^3)/1620 + (f29*tci12*tci21)/
       3 - (84*f29*tci31)/5 - 7*f29*tci21*tcr11 + 
      (7*f29*tci11*tcr11^2)/20) + 
    ((-218*f29*tci11^3)/243 - (40*f29*tci12*tci21)/3)*
     tcr12 + (-4*f29*tci11*tci12 - (40*f29*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f29*tci11*tci12)/15 - 
      3*f29*tci21 - 2*f29*tci11*tcr12) + 
    (130*f29*tci11*tcr33)/27;
L ieu1uou3 = 
   -2*f29*fvu3u43 - (11*f29*tci11^3)/135 - 
    (4*f29*tci12*tci21)/3 - (48*f29*tci31)/5 - 
    4*f29*tci21*tcr11 + (f29*tci11*tcr11^2)/5;
L ieu0uou3 = 0;
L ieum1uou3 = 0;
L ieum2uou3 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou3+w^2*ieu1uou3+w^3*ieu0uou3+w^4*ieum1uou3+w^5*ieum2uou3;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou3a = K[w^1];
L ieu1uou3a = K[w^2];
L ieu0uou3a = K[w^3];
L ieum1uou3a = K[w^4];
L ieum2uou3a = K[w^5];
.sort
#write <e3.tmp> "`optimmaxvar_'"
#write <e3_odd.c> "%O"
#write <e3_odd.c> "return Eps5o2<T>("
#write <e3_odd.c> "%E", ieu2uou3a
#write <e3_odd.c> ", "
#write <e3_odd.c> "%E", ieu1uou3a
#write <e3_odd.c> ", "
#write <e3_odd.c> "%E", ieu0uou3a
#write <e3_odd.c> ", "
#write <e3_odd.c> "%E", ieum1uou3a
#write <e3_odd.c> ", "
#write <e3_odd.c> "%E", ieum2uou3a
#write <e3_odd.c> ");\n}"
L H=+u^1*ieu2ueu3+u^2*ieu1ueu3+u^3*ieu0ueu3+u^4*ieum1ueu3+u^5*ieum2ueu3;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu3a = H[u^1];
L ieu1ueu3a = H[u^2];
L ieu0ueu3a = H[u^3];
L ieum1ueu3a = H[u^4];
L ieum2ueu3a = H[u^5];
.sort
#write <e3.tmp> "`optimmaxvar_'"
#write <e3_even.c> "%O"
#write <e3_even.c> "return Eps5o2<T>("
#write <e3_even.c> "%E", ieu2ueu3a
#write <e3_even.c> ", "
#write <e3_even.c> "%E", ieu1ueu3a
#write <e3_even.c> ", "
#write <e3_even.c> "%E", ieu0ueu3a
#write <e3_even.c> ", "
#write <e3_even.c> "%E", ieum1ueu3a
#write <e3_even.c> ", "
#write <e3_even.c> "%E", ieum2ueu3a
#write <e3_even.c> ");\n}"
.end
