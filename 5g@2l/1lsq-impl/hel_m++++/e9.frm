#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f124;
S f125;
S f75;
S f126;
S f76;
S f120;
S f121;
S f122;
S f123;
S f128;
S f78;
S f79;
S f129;
S f153;
S f152;
S f155;
S f158;
S f9;
S f8;
S f148;
S f81;
S f3;
S f147;
S f80;
S f83;
S f82;
S f145;
S f7;
S f84;
S f6;
S f4;
S f97;
S f179;
S f178;
S f173;
S f22;
S f171;
S f170;
S f177;
S f176;
S f25;
S f175;
S f174;
S f24;
S f99;
S f38;
S f168;
S f169;
S f39;
S f48;
S f30;
S f160;
S f161;
S f46;
S f33;
S f163;
S f34;
S f165;
S f110;
S f40;
S f35;
S f43;
S f36;
S f166;
S f42;
S f167;
S f37;
S f109;
S f50;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu9 = (-556*f3 + 63*f4 - 63*f22 - 63*f35 + 
      666*f75 - 74*f76 + 11*f121 - 74*f125 + 11*f175)/27 - 
    (2*f166*fvu3u1)/3 + (f166*fvu3u2)/6 + (f166*fvu3u3)/9 - 
    (5*f166*fvu3u4)/12 + (11*f166*fvu3u5)/36 - (5*f166*fvu3u6)/9 + 
    (f166*fvu3u7)/6 + (8*f166*fvu3u8)/9 + (f166*fvu3u11)/9 - 
    (5*f166*fvu3u12)/36 - (f166*fvu3u13)/36 + (f166*fvu3u17)/12 - 
    (5*f166*fvu3u18)/12 - (2*f166*fvu3u19)/9 - (f166*fvu3u24)/9 - 
    (4*f168*fvu3u31)/9 - (7*f168*fvu3u32)/9 - (f168*fvu3u33)/2 - 
    (16*f168*fvu3u34)/9 - (2*f168*fvu3u36)/9 + (f168*fvu3u37)/9 + 
    (f170*fvu3u38)/6 + ((-2*f168 - f171)*fvu3u40)/6 + 
    (2*f168*fvu3u44)/9 + ((f166 - 2*f168)*fvu3u51)/9 + 
    ((f152 - f168)*fvu3u52)/6 - (f168*fvu3u53)/6 + 
    ((f166 + f168)*fvu3u56)/18 + (f166*fvu3u57)/6 - 
    (f168*fvu3u59)/6 - (f166*fvu3u61)/3 - (5*f166*fvu1u1u1^3)/6 + 
    (11*f166*fvu1u1u2^3)/54 + ((f166 + f168)*fvu1u1u3^3)/9 - 
    (f168*fvu1u1u4^3)/3 - (2*(f166 - f168)*fvu1u1u5^3)/9 + 
    (35*f168*fvu1u1u6^3)/27 + (4*f168*fvu1u1u7^3)/27 - 
    (f166*fvu1u1u8^3)/18 + ((5*f166 + 3*f168)*fvu1u1u9^3)/27 + 
    ((-6*f37 + f39 - f167 - 6*f170)*fvu2u1u5)/9 + 
    ((6*f37 - f39 - 6*f97 + f99 + 6*f170 + 6*f171)*fvu2u1u7)/
     9 + ((-6*f120 + f145 - 6*f152 - f169)*fvu2u1u14)/9 - 
    (2*f168*fvu1u2u3*tci11^2)/3 - (f166*fvu1u2u6*tci11^2)/3 + 
    (8*f168*fvu1u2u8*tci11^2)/9 + (f166*fvu1u2u9*tci11^2)/3 - 
    (f161*tci12)/9 + ((6*f37 - f39 + f167 + 6*f170)*fvu1u2u7*
      tci12)/9 + (10*f166*fvu2u1u1*tci12)/3 + 
    (10*f166*fvu2u1u2*tci12)/9 - (f166*fvu2u1u3*tci12)/3 + 
    (2*f168*fvu2u1u6*tci12)/3 + (10*f166*fvu2u1u9*tci12)/9 + 
    (8*f168*fvu2u1u10*tci12)/9 - (2*f166*fvu2u1u11*tci12)/9 - 
    (f166*fvu2u1u12*tci12)/3 + fvu1u1u2^2*
     ((f166*fvu1u1u4)/18 - (f166*fvu1u1u5)/18 + 
      (f166*fvu1u1u8)/18 + (f166*fvu1u1u9)/9 - 
      (2*f166*tci12)/9) + fvu1u1u8^2*((f166*fvu1u1u9)/18 - 
      (f166*fvu1u1u10)/6 + (f166*tci12)/2) + 
    fvu1u1u1^2*(-(f166*fvu1u1u2)/18 + (5*f166*fvu1u1u4)/18 + 
      (f166*fvu1u1u5)/6 - (f168*fvu1u1u6)/9 + (f168*fvu1u1u7)/9 + 
      (5*f166*fvu1u1u8)/18 + (f166*fvu1u1u9)/18 + 
      (f166*fvu1u1u10)/3 + (35*f166*tci12)/18) + 
    fvu1u2u2*((2*(f166 + f168)*tci11^2)/9 + 
      ((6*f120 - f145 + 6*f152 + f167)*tci12)/9) + 
    fvu1u1u3^2*((-6*f37 + f39 - 6*f120 + f145 - 6*f152 - 
        3*f160 - 2*f167 - 6*f170)/18 - (5*f166*fvu1u1u4)/12 - 
      (f168*fvu1u1u6)/6 - (5*f166*fvu1u1u8)/12 + 
      ((-f166 - f168)*fvu1u1u9)/18 + ((-f166 + 2*f168)*fvu1u1u10)/
       18 + ((-f166 - f168)*tci12)/9) + 
    fvu2u1u8*((-6*f97 + f99 - f169 + 6*f171)/9 - 
      (4*f168*tci12)/9) + fvu1u1u10^2*
     (f163/6 - (f168*tci12)/6) + fvu1u1u7^2*
     ((-9*f3 + f4 - f22 - f35 + 9*f75 - f76 - f125 - f153 - 
        f158 + f160 - f163)/6 + (2*f168*fvu1u1u8)/9 + 
      (f168*fvu1u1u10)/6 + (f168*tci12)/6) + 
    fvu1u1u4^2*((f166*fvu1u1u5)/36 - (f168*fvu1u1u6)/9 + 
      (5*f168*fvu1u1u7)/18 - (5*f166*fvu1u1u8)/36 + 
      (7*f166*fvu1u1u9)/36 + (f168*fvu1u1u10)/6 + 
      (f168*tci12)/3) + fvu1u1u6^2*
     ((-6*f97 + f99 - 6*f120 + f145 - 6*f152 + 3*f158 - 
        2*f169 + 6*f171)/18 - (4*f168*fvu1u1u7)/9 - 
      (f168*fvu1u1u8)/3 - (f168*fvu1u1u9)/6 + (f168*tci12)/2) + 
    fvu2u1u13*((-6*f120 + f145 - 6*f152 - f167)/9 - 
      (2*(f166 + f168)*tci12)/9) + 
    fvu1u1u9^2*(((-f166 + 2*f168)*fvu1u1u10)/18 + 
      ((-5*f166 + 2*f168)*tci12)/36) + 
    fvu1u1u5^2*((6*f37 - f39 - 6*f97 + f99 + 3*f153 + 6*f170 + 
        6*f171)/18 - (f168*fvu1u1u6)/3 + (2*f168*fvu1u1u7)/9 - 
      (f168*fvu1u1u8)/9 + (f166*fvu1u1u9)/12 + 
      (f166*fvu1u1u10)/6 + ((5*f166 + 4*f168)*tci12)/36) + 
    tci11^2*((-27*f3 + 3*f4 - 3*f22 - 3*f35 - 12*f37 + 
        2*f39 + 27*f75 - 3*f76 + 12*f97 - 2*f99 - 12*f120 - 
        3*f125 + 2*f145 - 12*f152 - 18*f160 - 10*f167 - 2*f169 - 
        12*f170 - 12*f171)/108 + 
      ((-3*f152 - 20*f166 + 12*f168 - 3*f170)*tci12)/108) + 
    fvu1u1u10*((16*f8 - 2*f9 + 2*f22 - 8*f24 + f25 - 16*f30 + 
        2*f33 + 8*f34 + f35 - f36 - 2*f38 - 16*f40 + 2*f42 - 
        8*f48 + f50 - 18*f75 + 2*f76 - 16*f78 + 2*f80 + 2*f83 - 
        2*f121 + 16*f122 - 2*f123 + f125 + 16*f128 - 2*f129 - 
        8*f163 + 8*f173 - f174 - 2*f175 - 8*f178 + f179)/9 + 
      (2*(f166 + f168)*fvu2u1u13)/9 + (2*f168*fvu2u1u14)/3 + 
      (2*(f166 + f168)*fvu2u1u15)/9 + 
      ((4*f166 + 7*f168)*tci11^2)/27 - (f167*tci12)/9 - 
      (2*(f166 + f168)*fvu1u2u2*tci12)/9) + 
    fvu1u1u6*((8*f3 - f4 - 8*f6 + f7 - 16*f8 + 2*f9 - f22 + 
        8*f24 - f25 + 8*f30 - f33 + f35 + f38 + 8*f40 - f42 + 
        8*f43 - f46 + f75 + 8*f78 + 8*f79 - f80 - f81 - 
        8*f82 - f83 + f84 + 8*f109 - f110 + f121 - 8*f122 + 
        f123 + 8*f124 + f125 - f126 - 8*f128 + f129 + 8*f147 - 
        f148 - 8*f158 - 8*f173 + f174 + f175 - 8*f176 + f177)/
       9 + (f169*fvu1u1u7)/9 - (8*f168*fvu1u1u7^2)/9 - 
      (5*f168*fvu1u1u8^2)/9 - (f168*fvu1u1u9^2)/6 + 
      (f169*fvu1u1u10)/9 - (f168*fvu1u1u10^2)/3 - 
      (2*f168*fvu2u1u6)/3 + (20*f168*fvu2u1u8)/9 + 
      (2*f168*fvu2u1u14)/3 - f168*tci11^2 + 
      ((-6*f120 + f145 - 6*f152)*tci12)/9 + 
      (10*f168*fvu1u1u8*tci12)/9 - (f168*fvu1u1u9*tci12)/3 + 
      (2*f168*fvu1u2u3*tci12)/3) + 
    fvu1u1u9*(-(f166*fvu1u1u10^2)/6 + (7*f166*fvu2u1u11)/9 + 
      (f166*fvu2u1u12)/3 + (2*(f166 + f168)*fvu2u1u15)/9 + 
      ((11*f166 - 4*f168)*tci11^2)/54 + 
      ((-f166 + 2*f168)*fvu1u1u10*tci12)/9 - 
      (f166*fvu1u2u9*tci12)/3) + 
    fvu1u1u8*(-(f166*fvu1u1u9^2)/9 + (4*f168*fvu2u1u8)/9 - 
      (5*f166*fvu2u1u9)/9 - (8*f168*fvu2u1u10)/9 + 
      (2*f166*fvu2u1u11)/9 + (f166*fvu2u1u12)/3 + 
      ((-77*f166 + 8*f168)*tci11^2)/108 + 
      (2*f166*fvu1u1u9*tci12)/9 + (f166*fvu1u1u10*tci12)/3 - 
      (8*f168*fvu1u2u8*tci12)/9 - (f166*fvu1u2u9*tci12)/3) + 
    fvu1u1u2*((-2*f166*fvu1u1u4^2)/9 - (f166*fvu1u1u5^2)/9 - 
      (f166*fvu1u1u8^2)/18 - (5*f166*fvu1u1u9^2)/18 + 
      (f166*fvu2u1u3)/3 + (f166*fvu2u1u4)/9 + 
      (5*f166*fvu2u1u11)/9 - (f166*tci11^2)/27 - 
      (f166*fvu1u1u5*tci12)/9 + (2*f166*fvu1u1u9*tci12)/9 + 
      (f166*fvu1u2u6*tci12)/3 + fvu1u1u4*((f166*fvu1u1u5)/9 - 
        (f166*fvu1u1u8)/9 - (2*f166*fvu1u1u9)/9 + 
        (f166*tci12)/9) + fvu1u1u8*(-(f166*fvu1u1u9)/9 + 
        (f166*tci12)/9)) + fvu1u1u4*((f166*fvu1u1u5^2)/36 + 
      (4*f168*fvu1u1u6^2)/9 + (5*f168*fvu1u1u7^2)/18 - 
      (5*f166*fvu1u1u8^2)/36 + (7*f166*fvu1u1u9^2)/36 + 
      (f168*fvu1u1u10^2)/6 - (5*f166*fvu2u1u2)/9 + 
      (f166*fvu2u1u3)/3 - (2*f168*fvu2u1u6)/3 + 
      (4*f168*fvu2u1u8)/9 + (2*f166*fvu2u1u11)/9 + 
      ((-4*f166 - 9*f168)*tci11^2)/54 - 
      (7*f166*fvu1u1u9*tci12)/18 - (f168*fvu1u1u10*tci12)/3 + 
      (2*f168*fvu1u2u3*tci12)/3 + (f166*fvu1u2u6*tci12)/3 + 
      fvu1u1u5*(-(f166*fvu1u1u9)/6 - (f166*tci12)/18) + 
      fvu1u1u8*((f166*fvu1u1u9)/9 + (5*f166*tci12)/18) + 
      fvu1u1u7*((2*f168*fvu1u1u8)/9 - (f168*fvu1u1u10)/3 - 
        (5*f168*tci12)/9) + fvu1u1u6*((-2*f168*fvu1u1u7)/9 - 
        (2*f168*fvu1u1u8)/9 - (4*f168*tci12)/9)) + 
    fvu1u1u1*((-2*f166*fvu1u1u2^2)/9 + (5*f166*fvu1u1u3^2)/6 + 
      (5*f166*fvu1u1u4^2)/9 - (f166*fvu1u1u5^2)/6 - 
      (4*f168*fvu1u1u6^2)/9 + (2*f168*fvu1u1u7^2)/9 + 
      (5*f166*fvu1u1u8^2)/9 + (f166*fvu1u1u9^2)/9 + 
      (f166*fvu1u1u10^2)/6 - (5*f166*fvu2u1u1)/3 - 
      (5*f166*fvu2u1u2)/9 - (4*f168*fvu2u1u8)/9 - 
      (5*f166*fvu2u1u9)/9 - (2*f166*fvu2u1u11)/9 + 
      (2*f166*tci11^2)/27 - (5*f166*fvu1u1u3*tci12)/3 + 
      (f166*fvu1u1u9*tci12)/9 - (2*f166*fvu1u1u10*tci12)/3 + 
      fvu1u1u8*(-(f166*fvu1u1u9)/9 - (10*f166*tci12)/9) + 
      fvu1u1u4*((2*f168*fvu1u1u6)/9 - (2*f168*fvu1u1u7)/9 - 
        (5*f166*fvu1u1u8)/9 - (f166*fvu1u1u9)/9 - 
        (10*f166*tci12)/9) + fvu1u1u5*(-(f166*fvu1u1u10)/3 - 
        (f166*tci12)/3) + fvu1u1u2*((f166*fvu1u1u4)/9 + 
        (f166*fvu1u1u8)/9 + (f166*fvu1u1u9)/9 - 
        (f166*tci12)/9) + fvu1u1u6*((2*f168*fvu1u1u7)/9 + 
        (2*f168*fvu1u1u8)/9 - (2*f168*tci12)/9) + 
      fvu1u1u7*((-2*f168*fvu1u1u8)/9 + (2*f168*tci12)/9)) + 
    fvu1u1u7*((72*f3 - 8*f4 + 8*f6 - f7 + 8*f22 + 8*f30 - 
        f33 - 8*f34 + 7*f35 + f36 + f38 + 8*f40 - f42 - 
        8*f43 + f46 + 8*f48 - f50 - 73*f75 + 8*f76 + 8*f78 - 
        8*f79 - f80 + f81 + 8*f82 - f83 - f84 - 8*f109 + 
        f110 - 8*f122 + f123 - 8*f124 + 8*f125 + f126 - 
        8*f128 + f129 - 8*f147 + f148 + f155 + 8*f158 - f161 + 
        8*f163 + 8*f176 - f177 + 8*f178 - f179)/9 + 
      (2*f168*fvu1u1u8^2)/3 + (f168*fvu1u1u10^2)/6 + 
      (4*f168*fvu2u1u7)/9 + (16*f168*fvu2u1u8)/9 - 
      (8*f168*fvu2u1u10)/9 + (8*f168*tci11^2)/27 + 
      ((-6*f37 + f39 - 6*f170)*tci12)/9 - 
      (4*f168*fvu1u1u8*tci12)/9 - (8*f168*fvu1u2u8*tci12)/9 + 
      fvu1u1u10*(-f169/9 + (f168*tci12)/3)) + 
    fvu1u1u5*(-f155/9 - (f168*fvu1u1u6^2)/3 - 
      (f168*fvu1u1u8^2)/9 + (f166*fvu1u1u9^2)/12 + 
      (f166*fvu1u1u10^2)/6 + (f166*fvu2u1u4)/9 + 
      (4*f168*fvu2u1u7)/9 + ((5*f166 - 8*f168)*tci11^2)/54 - 
      (f167*tci12)/9 + (2*f168*fvu1u1u8*tci12)/9 + 
      (f166*fvu1u1u9*tci12)/6 + fvu1u1u10*
       (-f167/9 + (f166*tci12)/3) + fvu1u1u6*
       ((6*f97 - f99 - 6*f171)/9 + (2*f168*fvu1u1u8)/3 - 
        (2*f168*tci12)/3) + fvu1u1u7*((-6*f37 + f39 - 6*f170)/
         9 - (4*f168*fvu1u1u8)/9 + (4*f168*tci12)/9)) + 
    fvu1u1u3*(f161/9 - (5*f166*fvu1u1u4^2)/12 + 
      (f167*fvu1u1u5)/9 - (f168*fvu1u1u6^2)/6 + 
      ((6*f37 - f39 + 6*f170)*fvu1u1u7)/9 - (5*f166*fvu1u1u8^2)/
       12 + ((-f166 - f168)*fvu1u1u9^2)/18 - (f166*fvu1u1u10^2)/6 - 
      (5*f166*fvu2u1u1)/3 + (2*(f166 + f168)*fvu2u1u13)/9 + 
      ((-3*f166 + 2*f168)*tci11^2)/36 + (f160*tci12)/3 + 
      (5*f166*fvu1u1u8*tci12)/6 - (2*(f166 + f168)*fvu1u2u2*
        tci12)/9 + fvu1u1u10*(f167/9 + (f166*tci12)/3) + 
      fvu1u1u4*((5*f166*fvu1u1u8)/6 + (5*f166*tci12)/6) + 
      fvu1u1u6*((6*f120 - f145 + 6*f152)/9 + (f168*fvu1u1u9)/3 + 
        (f168*tci12)/3) + fvu1u1u9*(((f166 - 2*f168)*fvu1u1u10)/
         9 + ((f166 + f168)*tci12)/9)) - 
    (41*f166*tci12*tcr11^2)/18 + (5*f166*tcr11^3)/6 + 
    ((3*f152 + 2*f166 - 8*f168 + 3*f170)*tci11^2*tcr12)/18 - 
    (56*f166*tci12*tcr21)/9 + 
    tcr11*((-8*f166*tci11^2)/27 + (2*f166*tci12*tcr12)/
       3 + (25*f166*tcr21)/9) + 
    ((9*f152 + 26*f166 + 12*f168 + 9*f170 + 12*f171)*tcr33)/36;
L ieu1ueu9 = (-80*f3 + 9*f4 - 9*f22 - 9*f35 + 90*f75 - 
      10*f76 + f121 - 10*f125 + f175)/9 + 
    ((-f152 - 2*f166 - f170)*fvu1u1u3^2)/6 + 
    ((f170 + f171)*fvu1u1u5^2)/6 + 
    ((-f152 - 2*f168 + f171)*fvu1u1u6^2)/6 + 
    fvu1u1u3*(f160/3 + (f166*fvu1u1u5)/3 + (f152*fvu1u1u6)/3 + 
      (f170*fvu1u1u7)/3 + (f166*fvu1u1u10)/3) + 
    ((-f166 - f170)*fvu2u1u5)/3 + ((f170 + f171)*fvu2u1u7)/3 + 
    ((-f168 + f171)*fvu2u1u8)/3 + ((-f152 - f166)*fvu2u1u13)/3 + 
    ((-f152 - f168)*fvu2u1u14)/3 + 
    ((-f152 - 5*f166 - f168 - f170 - f171)*tci11^2)/18 - 
    (f160*tci12)/3 + ((f152 + f166)*fvu1u2u2*tci12)/3 + 
    ((f166 + f170)*fvu1u2u7*tci12)/3 + 
    fvu1u1u6*(-f158/3 + (f168*fvu1u1u7)/3 + (f168*fvu1u1u10)/3 - 
      (f152*tci12)/3) + fvu1u1u10*(-f163/3 - 
      (f166*tci12)/3) + fvu1u1u5*(-f153/3 - (f171*fvu1u1u6)/3 - 
      (f170*fvu1u1u7)/3 - (f166*fvu1u1u10)/3 - 
      (f166*tci12)/3) + fvu1u1u7*
     ((9*f3 - f4 + f22 + f35 - 9*f75 + f76 + f125 + f153 + 
        f158 - f160 + f163)/3 - (f168*fvu1u1u10)/3 - 
      (f170*tci12)/3);
L ieu0ueu9 = 
   (-9*f3 + f4 - f22 - f35 + 9*f75 - f76 - f125)/3;
L ieum1ueu9 = 0;
L ieum2ueu9 = 0;
L ieu2uou9 = (-4*f165*fvu4u28)/3 - (4*f165*fvu4u39)/3 + 
    (4*f165*fvu4u49)/9 - (4*f165*fvu4u51)/9 - (7*f165*fvu4u80)/12 - 
    (25*f165*fvu4u83)/12 - (3*f165*fvu4u91)/4 - 
    (7*f165*fvu4u93)/12 + (2*f165*fvu4u100)/9 - 
    (7*f165*fvu4u102)/6 + (f165*fvu4u111)/4 + (f165*fvu4u113)/2 + 
    f165*fvu4u114 - (4*f165*fvu4u129)/3 + (11*f165*fvu4u132)/3 - 
    (7*f165*fvu4u139)/2 - (4*f165*fvu4u141)/3 + 
    (2*f165*fvu4u146)/9 + (f165*fvu4u148)/2 + (4*f165*fvu4u174)/3 + 
    (4*f165*fvu4u182)/3 - (4*f165*fvu4u190)/9 + 
    (4*f165*fvu4u192)/9 - (41*f165*fvu4u213)/12 - 
    (35*f165*fvu4u215)/12 + (17*f165*fvu4u219)/4 + 
    (7*f165*fvu4u221)/12 + (4*f165*fvu4u225)/9 - (f165*fvu4u233)/4 - 
    (f165*fvu4u234)/2 - f165*fvu4u235 + (4*f165*fvu4u244)/3 + 
    (4*f165*fvu4u246)/3 - (2*f165*fvu4u252)/3 - 
    (2*f165*fvu4u255)/9 + 2*f165*fvu4u277 + 2*f165*fvu4u325 + 
    fvu3u25*((-2*f165*fvu1u1u3)/3 + (2*f165*fvu1u1u6)/3 - 
      (2*f165*fvu1u1u7)/3) + fvu3u71*((2*f165*fvu1u1u3)/3 - 
      (2*f165*fvu1u1u6)/3 + (2*f165*fvu1u1u7)/3) + 
    fvu3u63*((5*f165*fvu1u1u3)/2 + (f165*fvu1u1u5)/3 + 
      (11*f165*fvu1u1u6)/6 + (2*f165*fvu1u1u7)/3 - 
      (2*f165*fvu1u1u8)/3 - (f165*fvu1u1u9)/3 - 
      (5*f165*fvu1u1u10)/2) + fvu3u23*((2*f165*fvu1u1u1)/3 + 
      (2*f165*fvu1u1u6)/3 - (2*f165*fvu1u1u8)/3 - 
      (2*f165*fvu1u1u10)/3) + fvu3u45*((-2*f165*fvu1u1u3)/3 - 
      (f165*fvu1u1u5)/3 - (2*f165*fvu1u1u6)/3 + f165*fvu1u1u7 + 
      (f165*fvu1u1u9)/3 - (f165*fvu1u1u10)/3) + 
    fvu3u43*((-2*f165*fvu1u1u2)/3 + (f165*fvu1u1u4)/3 - 
      (2*f165*fvu1u1u6)/3 + (2*f165*fvu1u1u7)/3 + 
      (f165*fvu1u1u9)/3 + (f165*fvu1u1u10)/3) + 
    fvu3u62*((-2*f165*fvu1u1u2)/3 + (f165*fvu1u1u4)/3 + 
      (f165*fvu1u1u5)/3 - (2*f165*fvu1u1u6)/3 + 
      (f165*fvu1u1u7)/3 + (2*f165*fvu1u1u10)/3) + 
    fvu3u70*((-2*f165*fvu1u1u6)/3 + (2*f165*fvu1u1u8)/3 + 
      (2*f165*fvu1u1u10)/3) + (151*f165*tci11^3*tci12)/135 + 
    fvu3u78*((2*f165*fvu1u1u2)/3 + (f165*fvu1u1u3)/3 - 
      (f165*fvu1u1u4)/3 - 2*f165*fvu1u1u5 + (f165*fvu1u1u6)/3 - 
      f165*fvu1u1u7 + (2*f165*fvu1u1u8)/3 - (f165*fvu1u1u9)/3 - 
      f165*fvu1u1u10 - (4*f165*tci12)/3) + 
    fvu3u80*((2*f165*fvu1u1u2)/3 + (2*f165*fvu1u1u3)/3 - 
      (f165*fvu1u1u4)/3 - (f165*fvu1u1u5)/3 - 
      (2*f165*fvu1u1u6)/3 + f165*fvu1u1u7 - (2*f165*fvu1u1u8)/3 + 
      (2*f165*tci12)/3) + (2*f165*tci11^2*tci21)/9 + 
    fvu1u1u8*((-4*f165*tci11^3)/81 + (4*f165*tci12*tci21)/
       3) + tci12*((48*f165*tci31)/5 + 16*f165*tci32) + 
    (4*f165*tci41)/3 + ((35*f165*tci11^3)/108 - 
      (f165*tci12*tci21)/3 + 12*f165*tci31)*tcr11 + 
    (-(f165*tci11*tci12)/5 + 5*f165*tci21)*tcr11^2 - 
    (f165*tci11*tcr11^3)/4 + 
    fvu1u1u3*((25*f165*tci11^3)/324 + (7*f165*tci12*tci21)/
       3 + 12*f165*tci31 + 5*f165*tci21*tcr11 - 
      (f165*tci11*tcr11^2)/4) + 
    fvu1u1u6*((f165*tci11^3)/36 + (11*f165*tci12*tci21)/3 + 
      12*f165*tci31 + 5*f165*tci21*tcr11 - 
      (f165*tci11*tcr11^2)/4) + 
    fvu1u1u5*((37*f165*tci11^3)/270 - (28*f165*tci12*tci21)/
       9 + (8*f165*tci31)/5 + (2*f165*tci21*tcr11)/3 - 
      (f165*tci11*tcr11^2)/30) + 
    fvu1u1u9*((f165*tci11^3)/90 - (8*f165*tci12*tci21)/9 - 
      (8*f165*tci31)/5 - (2*f165*tci21*tcr11)/3 + 
      (f165*tci11*tcr11^2)/30) + 
    fvu1u1u7*((29*f165*tci11^3)/405 - (28*f165*tci12*tci21)/
       9 - (16*f165*tci31)/5 - (4*f165*tci21*tcr11)/3 + 
      (f165*tci11*tcr11^2)/15) + 
    fvu1u1u10*((79*f165*tci11^3)/1620 - 
      (41*f165*tci12*tci21)/9 - (44*f165*tci31)/5 - 
      (11*f165*tci21*tcr11)/3 + (11*f165*tci11*tcr11^2)/
       60) + fvu1u1u1*((2*f165*fvu3u25)/3 - (f165*fvu3u43)/3 + 
      (2*f165*fvu3u45)/3 - (f165*fvu3u62)/3 - (11*f165*fvu3u63)/6 - 
      (2*f165*fvu3u70)/3 - (2*f165*fvu3u71)/3 + (2*f165*fvu3u78)/3 - 
      (f165*fvu3u80)/3 - (19*f165*tci11^3)/108 + 
      (f165*tci12*tci21)/3 - 12*f165*tci31 - 
      5*f165*tci21*tcr11 + (f165*tci11*tcr11^2)/4) - 
    (40*f165*tci12*tci21*tcr12)/3 - 
    4*f165*tci11*tci12*tcr12^2;
L ieu1uou9 = -2*f165*fvu3u78 + (4*f165*tci11^3)/27 - 
    4*f165*tci12*tci21;
L ieu0uou9 = 0;
L ieum1uou9 = 0;
L ieum2uou9 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou9+w^2*ieu1uou9+w^3*ieu0uou9+w^4*ieum1uou9+w^5*ieum2uou9;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou9a = K[w^1];
L ieu1uou9a = K[w^2];
L ieu0uou9a = K[w^3];
L ieum1uou9a = K[w^4];
L ieum2uou9a = K[w^5];
.sort
#write <e9.tmp> "`optimmaxvar_'"
#write <e9_odd.c> "%O"
#write <e9_odd.c> "return Eps5o2<T>("
#write <e9_odd.c> "%E", ieu2uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieu1uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieu0uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieum1uou9a
#write <e9_odd.c> ", "
#write <e9_odd.c> "%E", ieum2uou9a
#write <e9_odd.c> ");\n}"
L H=+u^1*ieu2ueu9+u^2*ieu1ueu9+u^3*ieu0ueu9+u^4*ieum1ueu9+u^5*ieum2ueu9;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu9a = H[u^1];
L ieu1ueu9a = H[u^2];
L ieu0ueu9a = H[u^3];
L ieum1ueu9a = H[u^4];
L ieum2ueu9a = H[u^5];
.sort
#write <e9.tmp> "`optimmaxvar_'"
#write <e9_even.c> "%O"
#write <e9_even.c> "return Eps5o2<T>("
#write <e9_even.c> "%E", ieu2ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieu1ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieu0ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieum1ueu9a
#write <e9_even.c> ", "
#write <e9_even.c> "%E", ieum2ueu9a
#write <e9_even.c> ");\n}"
.end
