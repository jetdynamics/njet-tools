#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f137;
S f66;
S f135;
S f133;
S f131;
S f138;
S f68;
S f125;
S f75;
S f76;
S f120;
S f121;
S f122;
S f123;
S f78;
S f128;
S f129;
S f153;
S f155;
S f9;
S f8;
S f3;
S f80;
S f14;
S f83;
S f144;
S f82;
S f145;
S f7;
S f13;
S f84;
S f6;
S f143;
S f140;
S f4;
S f141;
S f179;
S f178;
S f173;
S f22;
S f175;
S f174;
S f38;
S f49;
S f48;
S f160;
S f30;
S f161;
S f33;
S f34;
S f40;
S f35;
S f36;
S f42;
S f52;
S f50;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu6 = (11*f22 + 11*f35 - 110*f75 + 11*f76 - 
      11*f121 + 74*f125 - 11*f175)/27 - (f137*fvu3u3)/9 + 
    ((f137 - 2*f143)*fvu3u4)/12 - (11*f137*fvu3u5)/36 + 
    (f137*fvu3u6)/9 - (f137*fvu3u7)/6 - (8*f137*fvu3u8)/9 - 
    (f137*fvu3u11)/9 + (f137*fvu3u12)/36 + (f137*fvu3u13)/36 - 
    (f137*fvu3u17)/12 + (f137*fvu3u18)/12 + (f137*fvu3u24)/9 - 
    (f144*fvu3u35)/6 + (f137*fvu3u51)/9 + 
    ((3*f120 + f137)*fvu3u52)/18 - (f140*fvu3u53)/6 - 
    (2*f137*fvu3u55)/9 - (f137*fvu3u56)/9 - (f137*fvu3u57)/6 + 
    (f137*fvu3u59)/6 + (f137*fvu3u61)/3 + (f137*fvu1u1u1^3)/18 - 
    (11*f137*fvu1u1u2^3)/54 - (f137*fvu1u1u6^3)/9 + 
    (f137*fvu1u1u8^3)/18 - (8*f137*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*((6*f66 - f68 - 3*f122 + 3*f125 - 3*f128 + 
        3*f131 + 3*f133 - f141 - 6*f144)/18 - 
      (f137*fvu1u1u5)/36 + (f137*fvu1u1u8)/36 - 
      (7*f137*fvu1u1u9)/36) + fvu1u1u3^2*
     ((-3*f131 - f138 - f145)/18 + (f137*fvu1u1u4)/12 + 
      (f137*fvu1u1u6)/18 + (f137*fvu1u1u8)/12 + 
      (f137*fvu1u1u9)/9 - (f137*fvu1u1u10)/6) + 
    ((6*f66 - f68 - f141 - 6*f144)*fvu2u1u6)/9 + 
    ((-f138 - f145)*fvu2u1u13)/9 + ((f141 - f145)*fvu2u1u14)/9 + 
    (f137*fvu1u2u6*tci11^2)/3 - (f137*fvu1u2u9*tci11^2)/3 + 
    ((8*f3 - f4 - 8*f6 + f7 - 16*f8 + 2*f9 - f22 + 16*f30 - 
       2*f33 - 8*f34 + f36 + 2*f38 + 16*f40 - 2*f42 + 8*f48 - 
       8*f49 - f50 + f52 + 16*f78 - 2*f80 - 8*f82 - 2*f83 + 
       f84 + f121 - 16*f122 + 2*f123 + 9*f125 - 16*f128 + 
       f129 + 8*f133 - 8*f173 + f174 + f175 + 8*f178 - f179)*
      tci12)/9 + ((f138 + f145)*fvu1u2u2*tci12)/9 + 
    ((-6*f66 + f68 + f141 + 6*f144)*fvu1u2u3*tci12)/9 + 
    (f137*fvu2u1u3*tci12)/3 - (2*f137*fvu2u1u9*tci12)/9 + 
    (2*f137*fvu2u1u11*tci12)/9 + (f137*fvu2u1u12*tci12)/3 + 
    fvu2u1u1*((6*f13 - f14 + f138 - 6*f143)/9 - 
      (2*f137*tci12)/3) + fvu1u1u8^2*(-(f137*fvu1u1u9)/18 + 
      (f137*fvu1u1u10)/6 - (f137*tci12)/2) + 
    fvu2u1u2*((6*f13 - f14 - 6*f66 + f68 - 6*f143 + 6*f144)/9 - 
      (2*f137*tci12)/9) + fvu1u1u1^2*
     ((12*f13 - 2*f14 - 6*f66 + f68 + 3*f122 + f138 - 12*f143 + 
        6*f144)/18 + (f137*fvu1u1u2)/18 - (f137*fvu1u1u4)/18 - 
      (f137*fvu1u1u8)/18 - (f137*fvu1u1u9)/18 - 
      (f137*fvu1u1u10)/6 - (f137*tci12)/18) + 
    fvu1u1u6^2*((3*f128 + f141 - f145)/18 + (f137*fvu1u1u9)/6 - 
      (f137*fvu1u1u10)/9 - (f137*tci12)/18) + 
    fvu1u1u5^2*(-(f137*fvu1u1u9)/12 + (f137*tci12)/36) + 
    fvu1u1u9^2*(-(f137*fvu1u1u10)/18 + (f137*tci12)/12) + 
    fvu1u1u10^2*(-f133/6 + (f137*tci12)/6) + 
    fvu1u1u2^2*(-(f137*fvu1u1u4)/18 + (f137*fvu1u1u5)/18 - 
      (f137*fvu1u1u8)/18 - (f137*fvu1u1u9)/9 + 
      (2*f137*tci12)/9) + fvu1u1u10*
     ((8*f3 - f4 - 8*f6 + f7 - 16*f8 + 2*f9 - 2*f22 + 
        16*f30 - 2*f33 - 8*f34 - f35 + f36 + 2*f38 + 16*f40 - 
        2*f42 + 8*f48 - 8*f49 - f50 + f52 + 10*f75 - f76 + 
        16*f78 - 2*f80 - 8*f82 - 2*f83 + f84 + 2*f121 - 
        16*f122 + 2*f123 - f125 - 16*f128 + 2*f129 + 8*f133 - 
        8*f173 + f174 + 2*f175 + 8*f178 - f179)/9 - 
      (2*f137*fvu2u1u14)/9 - (4*f137*fvu2u1u15)/9 - 
      (f137*tci11^2)/9 - (f141*tci12)/9) + 
    tci11^2*((12*f13 - 2*f14 + 21*f125 - 18*f128 + 18*f133 - 
        2*f141 - 12*f143 - 2*f145)/108 + 
      ((-f120 + 5*f137 - f140 - 2*f143 + 3*f144)*tci12)/36) + 
    fvu1u1u5*(-(f137*fvu1u1u9^2)/12 - (f137*fvu2u1u4)/9 - 
      (f137*tci11^2)/27 - (f137*fvu1u1u9*tci12)/6) + 
    fvu1u1u8*((f137*fvu1u1u9^2)/9 + (f137*fvu2u1u9)/9 - 
      (2*f137*fvu2u1u11)/9 - (f137*fvu2u1u12)/3 + 
      (73*f137*tci11^2)/108 - (2*f137*fvu1u1u9*tci12)/9 - 
      (f137*fvu1u1u10*tci12)/3 + (f137*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((f137*fvu1u1u10^2)/6 - (7*f137*fvu2u1u11)/9 - 
      (f137*fvu2u1u12)/3 - (4*f137*fvu2u1u15)/9 - 
      (7*f137*tci11^2)/54 - (f137*fvu1u1u10*tci12)/9 + 
      (f137*fvu1u2u9*tci12)/3) + 
    fvu1u1u6*(-f129/9 + (f137*fvu1u1u9^2)/6 - 
      (2*f137*fvu2u1u14)/9 + (2*f137*tci11^2)/27 - 
      (f145*tci12)/9 + (f137*fvu1u1u9*tci12)/3 + 
      fvu1u1u10*(-f141/9 - (2*f137*tci12)/9)) + 
    fvu1u1u2*((2*f137*fvu1u1u4^2)/9 + (f137*fvu1u1u5^2)/9 + 
      (f137*fvu1u1u8^2)/18 + (5*f137*fvu1u1u9^2)/18 - 
      (f137*fvu2u1u3)/3 - (f137*fvu2u1u4)/9 - 
      (5*f137*fvu2u1u11)/9 + (f137*tci11^2)/27 + 
      (f137*fvu1u1u5*tci12)/9 - (2*f137*fvu1u1u9*tci12)/9 - 
      (f137*fvu1u2u6*tci12)/3 + fvu1u1u8*((f137*fvu1u1u9)/9 - 
        (f137*tci12)/9) + fvu1u1u4*(-(f137*fvu1u1u5)/9 + 
        (f137*fvu1u1u8)/9 + (2*f137*fvu1u1u9)/9 - 
        (f137*tci12)/9)) + fvu1u1u4*
     ((-8*f3 + f4 + 8*f6 - f7 + 16*f8 - 2*f9 + f22 - 16*f30 + 
        2*f33 + 8*f34 - f36 - 2*f38 - 16*f40 + 2*f42 - 8*f48 + 
        f50 - 16*f78 + 2*f80 + 2*f83 - f121 + 24*f122 - 
        2*f123 - 9*f125 + 16*f128 - f129 - 8*f131 - 8*f133 + 
        8*f153 - f155 - 8*f160 + f161 + 8*f173 - f174 - f175 - 
        8*f178 + f179)/9 - (f137*fvu1u1u5^2)/36 + 
      ((-6*f66 + f68 + 6*f144)*fvu1u1u6)/9 + 
      (f137*fvu1u1u8^2)/36 - (7*f137*fvu1u1u9^2)/36 + 
      (f141*fvu1u1u10)/9 + (f137*fvu2u1u2)/9 - 
      (f137*fvu2u1u3)/3 - (2*f137*fvu2u1u11)/9 + 
      (f137*tci11^2)/27 + ((f122 - f125 + f128 - f131 - f133)*
        tci12)/3 + (7*f137*fvu1u1u9*tci12)/18 - 
      (f137*fvu1u2u6*tci12)/3 + fvu1u1u8*(-(f137*fvu1u1u9)/9 - 
        (f137*tci12)/18) + fvu1u1u5*((f137*fvu1u1u9)/6 + 
        (f137*tci12)/18)) + fvu1u1u3*
     ((8*f49 - f52 + 8*f82 - f84 - 8*f122 + f123 + 8*f131 - 
        8*f153 + f155 + 8*f160 - f161)/9 + (f137*fvu1u1u4^2)/12 + 
      (f137*fvu1u1u6^2)/18 + (f137*fvu1u1u8^2)/12 + 
      (f137*fvu1u1u9^2)/9 - (f137*fvu1u1u10^2)/6 + 
      (f137*fvu2u1u1)/3 + (f137*tci11^2)/36 + 
      (f131*tci12)/3 - (f137*fvu1u1u8*tci12)/6 + 
      fvu1u1u9*((f137*fvu1u1u10)/9 - (2*f137*tci12)/9) + 
      fvu1u1u4*((6*f13 - f14 - 6*f143)/9 - (f137*fvu1u1u8)/6 - 
        (f137*tci12)/6) + fvu1u1u6*(f145/9 - (f137*fvu1u1u9)/
         3 + (2*f137*fvu1u1u10)/9 - (f137*tci12)/9) + 
      fvu1u1u10*(f138/9 + (f137*tci12)/3)) + 
    fvu1u1u1*(-f123/9 + (2*f137*fvu1u1u2^2)/9 - 
      (f137*fvu1u1u3^2)/6 - (f137*fvu1u1u4^2)/9 + 
      ((6*f66 - f68 - 6*f144)*fvu1u1u6)/9 - (f137*fvu1u1u8^2)/9 - 
      (f137*fvu1u1u9^2)/9 + (f137*fvu2u1u1)/3 + 
      (f137*fvu2u1u2)/9 + (f137*fvu2u1u9)/9 + 
      (2*f137*fvu2u1u11)/9 - (23*f137*tci11^2)/54 + 
      ((6*f66 - f68 - 3*f122 - f138 - 6*f144)*tci12)/9 - 
      (f137*fvu1u1u9*tci12)/9 + fvu1u1u2*(-(f137*fvu1u1u4)/9 - 
        (f137*fvu1u1u8)/9 - (f137*fvu1u1u9)/9 + 
        (f137*tci12)/9) + fvu1u1u8*((f137*fvu1u1u9)/9 + 
        (2*f137*tci12)/9) + fvu1u1u4*((-6*f13 + f14 + 6*f143)/
         9 + (f137*fvu1u1u8)/9 + (f137*fvu1u1u9)/9 + 
        (2*f137*tci12)/9) + fvu1u1u10*(-f138/9 + 
        (f137*tci12)/3) + fvu1u1u3*((-6*f13 + f14 + 6*f143)/9 + 
        (f137*tci12)/3)) + ((10*f137 - 6*f143 + 9*f144)*tci12*
      tcr11^2)/18 + ((-2*f137 + 4*f143 - f144)*tcr11^3)/18 + 
    ((6*f120 + 7*f137 + 6*f140 + 2*f143 + 7*f144)*tci11^2*
      tcr12)/36 + ((-f137 - 2*f143 - f144)*tcr12^3)/9 + 
    (2*(8*f137 - 3*f143 + 3*f144)*tci12*tcr21)/9 + 
    tcr11*(((25*f137 - 21*f144)*tci11^2)/108 + 
      ((-f137 - f144)*tci12*tcr12)/3 + 
      ((-5*f137 + 6*f143)*tcr21)/9) + 
    ((f137 + 2*f143 + f144)*tcr31)/3 + 
    ((f137 + 2*f143 + f144)*tcr32)/12 + 
    ((18*f120 - 11*f137 + 18*f140 - 74*f143 - 11*f144)*tcr33)/
     72;
L ieu1ueu6 = (f22 + f35 - 10*f75 + f76 - f121 + 
      10*f125 - f175)/9 + ((f137 - 2*f143 + f144)*fvu1u1u1^2)/6 + 
    ((-f120 - f137)*fvu1u1u3^2)/6 + ((-f140 - f144)*fvu1u1u4^2)/
     6 + ((-f120 + f140)*fvu1u1u6^2)/6 + 
    fvu1u1u3*(f131/3 - (f143*fvu1u1u4)/3 + (f120*fvu1u1u6)/3 + 
      (f137*fvu1u1u10)/3) + fvu1u1u4*
     ((f122 - f125 + f128 - f131 - f133)/3 + 
      (f144*fvu1u1u6)/3 + (f140*fvu1u1u10)/3) + 
    ((f137 - f143)*fvu2u1u1)/3 + ((-f143 + f144)*fvu2u1u2)/3 + 
    ((-f140 - f144)*fvu2u1u6)/3 + ((-f120 - f137)*fvu2u1u13)/3 + 
    ((-f120 + f140)*fvu2u1u14)/3 + 
    ((-f120 - f140 - f143)*tci11^2)/18 + 
    ((f125 - f128 + f133)*tci12)/3 + 
    ((f120 + f137)*fvu1u2u2*tci12)/3 + 
    ((f140 + f144)*fvu1u2u3*tci12)/3 + 
    fvu1u1u6*(-f128/3 - (f140*fvu1u1u10)/3 - (f120*tci12)/3) + 
    fvu1u1u10*(f133/3 - (f140*tci12)/3) + 
    fvu1u1u1*(-f122/3 + (f143*fvu1u1u3)/3 + (f143*fvu1u1u4)/3 - 
      (f144*fvu1u1u6)/3 - (f137*fvu1u1u10)/3 + 
      ((-f137 - f144)*tci12)/3);
L ieu0ueu6 = f125/3;
L ieum1ueu6 = 0;
L ieum2ueu6 = 0;
L ieu2uou6 = -(f135*fvu4u25)/3 - (f135*fvu4u28)/3 - 
    (7*f135*fvu4u39)/3 - (f135*fvu4u41)/3 + (f135*fvu4u49)/3 - 
    (5*f135*fvu4u51)/9 + (5*f135*fvu4u80)/12 - 
    (37*f135*fvu4u83)/12 - (7*f135*fvu4u91)/4 + 
    (5*f135*fvu4u93)/12 + (f135*fvu4u100)/3 - 
    (23*f135*fvu4u102)/18 + (f135*fvu4u111)/4 + (f135*fvu4u113)/2 + 
    f135*fvu4u114 - (8*f135*fvu4u129)/3 + (f135*fvu4u132)/3 - 
    (f135*fvu4u139)/6 - (8*f135*fvu4u141)/3 + (4*f135*fvu4u146)/9 - 
    (11*f135*fvu4u148)/18 + (5*f135*fvu4u171)/12 + 
    (23*f135*fvu4u174)/12 - (f135*fvu4u182)/12 + 
    (5*f135*fvu4u184)/12 + (f135*fvu4u190)/9 - (f135*fvu4u192)/18 + 
    (f135*fvu4u199)/4 + (f135*fvu4u201)/2 + f135*fvu4u202 + 
    (7*f135*fvu4u213)/12 - (f135*fvu4u215)/4 + (9*f135*fvu4u219)/4 + 
    (7*f135*fvu4u221)/12 - (4*f135*fvu4u225)/9 - (f135*fvu4u233)/4 - 
    (f135*fvu4u234)/2 - f135*fvu4u235 + (17*f135*fvu4u244)/3 + 
    (4*f135*fvu4u246)/3 - (7*f135*fvu4u252)/2 - 
    (17*f135*fvu4u255)/18 - (5*f135*fvu4u271)/12 - 
    (43*f135*fvu4u273)/12 + (67*f135*fvu4u277)/12 - 
    (5*f135*fvu4u279)/12 - (f135*fvu4u282)/3 - (f135*fvu4u289)/4 - 
    (f135*fvu4u290)/2 - f135*fvu4u291 - (2*f135*fvu4u293)/3 - 
    (2*f135*fvu4u295)/3 - 2*f135*fvu4u309 + 2*f135*fvu4u313 + 
    fvu3u78*((2*f135*fvu1u1u2)/3 - (2*f135*fvu1u1u3)/3 - 
      (2*f135*fvu1u1u9)/3) + fvu3u70*((f135*fvu1u1u2)/6 - 
      (f135*fvu1u1u3)/6 - (f135*fvu1u1u9)/6) + 
    fvu3u45*(-(f135*fvu1u1u2)/2 - (7*f135*fvu1u1u3)/6 - 
      (2*f135*fvu1u1u6)/3 + (2*f135*fvu1u1u7)/3 + 
      (f135*fvu1u1u9)/2) + fvu3u23*((f135*fvu1u1u1)/3 + 
      (f135*fvu1u1u2)/6 + (f135*fvu1u1u6)/2 - (f135*fvu1u1u7)/6 - 
      (f135*fvu1u1u8)/3 - (f135*fvu1u1u9)/6 - 
      (f135*fvu1u1u10)/3) + fvu3u43*(-(f135*fvu1u1u2)/6 - 
      (f135*fvu1u1u4)/3 - (f135*fvu1u1u6)/6 + (f135*fvu1u1u7)/6 + 
      (f135*fvu1u1u9)/2 - (f135*fvu1u1u10)/3) + 
    fvu3u25*(-(f135*fvu1u1u2)/3 - (7*f135*fvu1u1u3)/6 + 
      f135*fvu1u1u4 - (2*f135*fvu1u1u5)/3 + (5*f135*fvu1u1u6)/6 - 
      (f135*fvu1u1u7)/6 + (f135*fvu1u1u10)/3) + 
    fvu3u81*((f135*fvu1u1u3)/6 + (f135*fvu1u1u4)/3 + 
      (f135*fvu1u1u6)/6 - (f135*fvu1u1u7)/6 - (f135*fvu1u1u9)/3 + 
      (f135*fvu1u1u10)/3) + fvu3u62*((-4*f135*fvu1u1u2)/3 + 
      (2*f135*fvu1u1u4)/3 + (2*f135*fvu1u1u5)/3 - 
      (4*f135*fvu1u1u6)/3 + (2*f135*fvu1u1u7)/3 + 
      (4*f135*fvu1u1u10)/3) + (13*f135*tci11^3*tci12)/15 + 
    fvu3u71*(-(f135*fvu1u1u2)/3 + (4*f135*fvu1u1u3)/3 + 
      f135*fvu1u1u4 - (2*f135*fvu1u1u5)/3 - (4*f135*fvu1u1u6)/3 + 
      (f135*fvu1u1u7)/3 + (f135*fvu1u1u8)/3 - 2*f135*tci12) + 
    fvu3u82*((f135*fvu1u1u3)/3 + (f135*fvu1u1u4)/3 - 
      (f135*fvu1u1u7)/3 + (f135*fvu1u1u8)/3 - (f135*fvu1u1u9)/3 - 
      (2*f135*tci12)/3) + fvu3u63*((2*f135*fvu1u1u2)/3 + 
      (f135*fvu1u1u3)/2 - f135*fvu1u1u4 + (2*f135*fvu1u1u5)/3 - 
      (f135*fvu1u1u6)/2 - (f135*fvu1u1u7)/3 - (f135*fvu1u1u8)/3 - 
      (f135*fvu1u1u9)/3 - (f135*fvu1u1u10)/6 + 2*f135*tci12) + 
    fvu3u80*((2*f135*fvu1u1u2)/3 + f135*fvu1u1u3 - 
      (19*f135*fvu1u1u6)/6 + (19*f135*fvu1u1u7)/6 - 
      (17*f135*fvu1u1u8)/6 + (13*f135*fvu1u1u9)/6 + 
      (17*f135*tci12)/6) - (10*f135*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f135*tci11^3)/81 + (17*f135*tci12*tci21)/
       3) + tci12*((16*f135*tci31)/5 + 16*f135*tci32) - 
    (2072*f135*tci41)/135 - (64*f135*tci42)/5 - 
    (16*f135*tci43)/3 + ((1259*f135*tci11^3)/1620 + 
      f135*tci12*tci21 + (84*f135*tci31)/5 + 
      16*f135*tci32)*tcr11 - (11*f135*tci11*tcr11^3)/
     180 + fvu1u1u2*((181*f135*tci11^3)/1620 + 
      (2*f135*tci12*tci21)/9 + (44*f135*tci31)/5 + 
      (11*f135*tci21*tcr11)/3 - (11*f135*tci11*tcr11^2)/
       60) + fvu1u1u6*((-13*f135*tci11^3)/162 + 
      (46*f135*tci12*tci21)/9 + 8*f135*tci31 + 
      (10*f135*tci21*tcr11)/3 - (f135*tci11*tcr11^2)/6) + 
    fvu1u1u3*((73*f135*tci11^3)/1620 - (f135*tci12*tci21)/
       3 + (12*f135*tci31)/5 + f135*tci21*tcr11 - 
      (f135*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f135*tci11^3)/540 - 
      (49*f135*tci12*tci21)/9 - (4*f135*tci31)/5 - 
      (f135*tci21*tcr11)/3 + (f135*tci11*tcr11^2)/60) + 
    fvu1u1u5*((f135*tci11^3)/45 - (16*f135*tci12*tci21)/9 - 
      (16*f135*tci31)/5 - (4*f135*tci21*tcr11)/3 + 
      (f135*tci11*tcr11^2)/15) + 
    fvu1u1u4*((-31*f135*tci11^3)/270 + (4*f135*tci12*tci21)/
       3 - (24*f135*tci31)/5 - 2*f135*tci21*tcr11 + 
      (f135*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f135*tci11^3)/180 - (f135*tci12*tci21)/
       9 - (28*f135*tci31)/5 - (7*f135*tci21*tcr11)/3 + 
      (7*f135*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f135*tci11^3)/1620 - 
      (19*f135*tci12*tci21)/3 - (36*f135*tci31)/5 - 
      3*f135*tci21*tcr11 + (3*f135*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f135*fvu3u25)/6 + (f135*fvu3u43)/3 + 
      (7*f135*fvu3u45)/6 - (2*f135*fvu3u62)/3 - (7*f135*fvu3u63)/6 + 
      (f135*fvu3u70)/6 - (2*f135*fvu3u71)/3 + (2*f135*fvu3u78)/3 - 
      f135*fvu3u80 - (f135*fvu3u81)/2 - (f135*fvu3u82)/3 - 
      (179*f135*tci11^3)/1620 + (f135*tci12*tci21)/3 - 
      (36*f135*tci31)/5 - 3*f135*tci21*tcr11 + 
      (3*f135*tci11*tcr11^2)/20) + 
    ((-218*f135*tci11^3)/243 - (40*f135*tci12*tci21)/3)*
     tcr12 + (-4*f135*tci11*tci12 - (40*f135*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f135*tci11*tci12)/15 + 
      5*f135*tci21 - 2*f135*tci11*tcr12) + 
    (130*f135*tci11*tcr33)/27;
L ieu1uou6 = 
   -2*f135*fvu3u63 - (11*f135*tci11^3)/135 - 
    (4*f135*tci12*tci21)/3 - (48*f135*tci31)/5 - 
    4*f135*tci21*tcr11 + (f135*tci11*tcr11^2)/5;
L ieu0uou6 = 0;
L ieum1uou6 = 0;
L ieum2uou6 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou6+w^2*ieu1uou6+w^3*ieu0uou6+w^4*ieum1uou6+w^5*ieum2uou6;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou6a = K[w^1];
L ieu1uou6a = K[w^2];
L ieu0uou6a = K[w^3];
L ieum1uou6a = K[w^4];
L ieum2uou6a = K[w^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_odd.c> "%O"
#write <e6_odd.c> "return Eps5o2<T>("
#write <e6_odd.c> "%E", ieu2uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu0uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum2uou6a
#write <e6_odd.c> ");\n}"
L H=+u^1*ieu2ueu6+u^2*ieu1ueu6+u^3*ieu0ueu6+u^4*ieum1ueu6+u^5*ieum2ueu6;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu6a = H[u^1];
L ieu1ueu6a = H[u^2];
L ieu0ueu6a = H[u^3];
L ieum1ueu6a = H[u^4];
L ieum2ueu6a = H[u^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_even.c> "%O"
#write <e6_even.c> "return Eps5o2<T>("
#write <e6_even.c> "%E", ieu2ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu0ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum2ueu6a
#write <e6_even.c> ");\n}"
.end
