#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f137;
S f66;
S f65;
S f135;
S f64;
S f63;
S f133;
S f132;
S f62;
S f131;
S f130;
S f60;
S f69;
S f68;
S f124;
S f126;
S f70;
S f71;
S f121;
S f122;
S f123;
S f151;
S f159;
S f148;
S f16;
S f3;
S f83;
S f1;
S f86;
S f29;
S f28;
S f177;
S f176;
S f26;
S f175;
S f118;
S f30;
S f115;
S f31;
S f114;
S f117;
S f116;
S f41;
S f111;
S f110;
S f43;
S f113;
S f112;
S f108;
S f58;
S f59;
S f109;
S f53;
S f56;
S f106;
S f57;
S f107;
S f54;
S f104;
S f55;
S f105;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (11*f3 - 11*f16 - 110*f58 + 11*f60 + 
      63*f107 - 11*f122 + 11*f126 - 11*f151)/27 - 
    (f113*fvu3u3)/9 + (f113*fvu3u4)/12 - (11*f113*fvu3u5)/36 + 
    (f113*fvu3u6)/9 - (f113*fvu3u7)/6 - (8*f113*fvu3u8)/9 + 
    (f114*fvu3u10)/6 - (f113*fvu3u11)/9 + (f113*fvu3u12)/36 + 
    (f113*fvu3u13)/36 - (f113*fvu3u17)/12 + 
    ((-2*f104 + f113)*fvu3u18)/12 + (f113*fvu3u24)/9 + 
    (f113*fvu3u51)/9 + (f113*fvu3u52)/18 - (2*f113*fvu3u55)/9 + 
    ((-2*f113 - 3*f115)*fvu3u56)/18 + ((-f113 + f117)*fvu3u57)/6 + 
    (f113*fvu3u59)/6 + (f113*fvu3u61)/3 + (f113*fvu1u1u1^3)/18 - 
    (11*f113*fvu1u1u2^3)/54 - (f113*fvu1u1u6^3)/9 + 
    (f113*fvu1u1u8^3)/18 - (8*f113*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(-(f113*fvu1u1u5)/36 + (f113*fvu1u1u8)/36 - 
      (7*f113*fvu1u1u9)/36) + fvu1u1u3^2*
     ((-3*f109 - 6*f113 - f116 + 6*f121 - f148)/18 + 
      (f113*fvu1u1u4)/12 + (f113*fvu1u1u6)/18 + 
      (f113*fvu1u1u8)/12 + (f113*fvu1u1u9)/9 - 
      (f113*fvu1u1u10)/6) + ((-6*f113 - f116 + 6*f121 - f148)*
      fvu2u1u13)/9 + ((-f116 + f118)*fvu2u1u15)/9 + 
    (f113*fvu1u2u6*tci11^2)/3 + 
    ((f3 - f16 + 8*f26 - 8*f28 - f29 + f31 + 9*f57 - 9*f58 - 
       f59 + f60 + f107 + 8*f108 - 8*f109 + 8*f111 - 8*f123 + 
       f124 - 8*f131 + f133 - 8*f135 + f137 - f151 - f159 + 
       f177)*tci12)/9 + ((6*f113 + f116 - 6*f121 + f148)*
      fvu1u2u2*tci12)/9 - (2*f113*fvu2u1u2*tci12)/9 + 
    (f113*fvu2u1u3*tci12)/3 + (2*f113*fvu2u1u11*tci12)/9 + 
    fvu2u1u1*((6*f1 - f30 - 6*f104 + 6*f113 - 6*f121 + f148)/
       9 - (2*f113*tci12)/3) + fvu1u1u8^2*
     ((-6*f41 + f43 + 3*f108 + 6*f114 - f118)/18 - 
      (f113*fvu1u1u9)/18 + (f113*fvu1u1u10)/6 - 
      (f113*tci12)/2) + fvu2u1u9*
     ((6*f1 - f30 + 6*f41 - f43 - 6*f104 - 6*f114)/9 - 
      (2*f113*tci12)/9) + fvu1u1u1^2*
     ((12*f1 - 2*f30 + 6*f41 - f43 - 12*f104 + 3*f111 + 
        6*f113 - 6*f114 - 6*f121 + f148)/18 + 
      (f113*fvu1u1u2)/18 - (f113*fvu1u1u4)/18 - 
      (f113*fvu1u1u8)/18 - (f113*fvu1u1u9)/18 - 
      (f113*fvu1u1u10)/6 - (f113*tci12)/18) + 
    fvu1u1u6^2*((f113*fvu1u1u9)/6 - (f113*fvu1u1u10)/9 - 
      (f113*tci12)/18) + fvu1u1u5^2*(-(f113*fvu1u1u9)/12 + 
      (f113*tci12)/36) + fvu1u1u9^2*
     ((-3*f105 + 3*f107 - 3*f108 + 3*f109 - 3*f111 - f116 + 
        f118)/18 - (f113*fvu1u1u10)/18 + (f113*tci12)/12) + 
    fvu1u1u10^2*(f105/6 + (f113*tci12)/6) + 
    fvu1u1u2^2*(-(f113*fvu1u1u4)/18 + (f113*fvu1u1u5)/18 - 
      (f113*fvu1u1u8)/18 - (f113*fvu1u1u9)/9 + 
      (2*f113*tci12)/9) + fvu2u1u12*
     ((-6*f41 + f43 + 6*f114 - f118)/9 + (f113*tci12)/3) + 
    tci11^2*((12*f1 - 2*f30 - 12*f104 + 3*f107 + 18*f108 - 
        18*f109 + 18*f111 - 2*f116 - 2*f118)/108 + 
      ((-2*f104 + 5*f113 - 3*f114 - f115 - f117)*tci12)/36) + 
    fvu1u1u10*(-f106/9 - (2*f113*fvu2u1u14)/9 - 
      (4*f113*fvu2u1u15)/9 - (f113*tci11^2)/9 - 
      (f118*tci12)/9) + fvu1u2u9*(-(f113*tci11^2)/3 + 
      ((6*f41 - f43 - 6*f114 + f118)*tci12)/9) + 
    fvu1u1u5*(-(f113*fvu1u1u9^2)/12 - (f113*fvu2u1u4)/9 - 
      (f113*tci11^2)/27 - (f113*fvu1u1u9*tci12)/6) + 
    fvu1u1u6*((f113*fvu1u1u9^2)/6 - (2*f113*fvu2u1u14)/9 + 
      (2*f113*tci11^2)/27 + (f113*fvu1u1u9*tci12)/3 - 
      (2*f113*fvu1u1u10*tci12)/9) + 
    fvu1u1u8*((-2*f3 + 2*f16 - 16*f26 + 8*f28 + 2*f29 - f31 + 
        16*f53 - 16*f54 - 2*f55 + 2*f56 - 18*f57 + 18*f58 + 
        2*f59 - 2*f60 + 16*f62 + 16*f63 + 16*f64 - 2*f65 - 
        2*f66 - 2*f67 - 8*f68 + 8*f69 + f70 - f71 + 8*f83 - 
        f86 + f107 - 16*f108 + 16*f123 - 2*f124 + 8*f130 + 
        16*f131 - f132 - 2*f133 + 8*f135 - f137 - f151 + 
        2*f159 + 16*f175 - 2*f176 - 2*f177)/18 + 
      (f113*fvu1u1u9^2)/9 + (f113*fvu2u1u9)/9 - 
      (2*f113*fvu2u1u11)/9 - (f113*fvu2u1u12)/3 + 
      (73*f113*tci11^2)/108 - (f108*tci12)/3 + 
      (f113*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (f118/9 - (f113*tci12)/3) + fvu1u1u9*
       ((6*f41 - f43 - 6*f114)/9 - (2*f113*tci12)/9)) + 
    fvu1u1u9*((8*f26 - 8*f28 - f29 + f31 + 9*f57 + f58 - 
        f59 + f106 - 8*f107 + 8*f108 - 8*f109 + 8*f111 + f122 - 
        8*f123 + f124 - f126 - 8*f131 + f133 - 8*f135 + f137 - 
        f159 + f177)/9 + (f113*fvu1u1u10^2)/6 - 
      (7*f113*fvu2u1u11)/9 - (f113*fvu2u1u12)/3 - 
      (4*f113*fvu2u1u15)/9 - (7*f113*tci11^2)/54 - 
      (f116*tci12)/9 + (f113*fvu1u2u9*tci12)/3 + 
      fvu1u1u10*(-f118/9 - (f113*tci12)/9)) + 
    fvu1u1u2*((2*f113*fvu1u1u4^2)/9 + (f113*fvu1u1u5^2)/9 + 
      (f113*fvu1u1u8^2)/18 + (5*f113*fvu1u1u9^2)/18 - 
      (f113*fvu2u1u3)/3 - (f113*fvu2u1u4)/9 - 
      (5*f113*fvu2u1u11)/9 + (f113*tci11^2)/27 + 
      (f113*fvu1u1u5*tci12)/9 - (2*f113*fvu1u1u9*tci12)/9 - 
      (f113*fvu1u2u6*tci12)/3 + fvu1u1u8*((f113*fvu1u1u9)/9 - 
        (f113*tci12)/9) + fvu1u1u4*(-(f113*fvu1u1u5)/9 + 
        (f113*fvu1u1u8)/9 + (2*f113*fvu1u1u9)/9 - 
        (f113*tci12)/9)) + fvu1u1u4*(-(f113*fvu1u1u5^2)/36 + 
      (f113*fvu1u1u8^2)/36 - (7*f113*fvu1u1u9^2)/36 + 
      (f113*fvu2u1u2)/9 - (f113*fvu2u1u3)/3 - 
      (2*f113*fvu2u1u11)/9 + (f113*tci11^2)/27 + 
      (7*f113*fvu1u1u9*tci12)/18 - (f113*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f113*fvu1u1u9)/9 - (f113*tci12)/18) + 
      fvu1u1u5*((f113*fvu1u1u9)/6 + (f113*tci12)/18)) + 
    fvu1u1u1*((8*f28 - f31 - 16*f53 + 16*f54 + 2*f55 - 2*f56 - 
        16*f62 - 16*f63 - 16*f64 + 2*f65 + 2*f66 + 2*f67 + 
        8*f68 - 8*f69 - f70 + f71 - 8*f83 + f86 - 3*f107 + 
        16*f109 - 2*f110 - 16*f111 - 8*f130 + f132 + 8*f135 - 
        f137 + 3*f151 - 16*f175 + 2*f176)/18 + 
      (2*f113*fvu1u1u2^2)/9 - (f113*fvu1u1u3^2)/6 - 
      (f113*fvu1u1u4^2)/9 - (f113*fvu1u1u8^2)/9 - 
      (f113*fvu1u1u9^2)/9 + (f113*fvu2u1u1)/3 + 
      (f113*fvu2u1u2)/9 + (f113*fvu2u1u9)/9 + 
      (2*f113*fvu2u1u11)/9 - (23*f113*tci11^2)/54 + 
      ((-6*f41 + f43 - 3*f111 - 6*f113 + 6*f114 + 6*f121 - 
         f148)*tci12)/9 + fvu1u1u9*((-6*f41 + f43 + 6*f114)/9 - 
        (f113*tci12)/9) + fvu1u1u2*(-(f113*fvu1u1u4)/9 - 
        (f113*fvu1u1u8)/9 - (f113*fvu1u1u9)/9 + 
        (f113*tci12)/9) + fvu1u1u8*((-6*f1 + f30 + 6*f104)/9 + 
        (f113*fvu1u1u9)/9 + (2*f113*tci12)/9) + 
      fvu1u1u4*((f113*fvu1u1u8)/9 + (f113*fvu1u1u9)/9 + 
        (2*f113*tci12)/9) + fvu1u1u3*
       ((-6*f1 + f30 + 6*f104)/9 + (f113*tci12)/3) + 
      fvu1u1u10*((-6*f113 + 6*f121 - f148)/9 + 
        (f113*tci12)/3)) + fvu1u1u3*
     (f110/9 + (f113*fvu1u1u4^2)/12 + (f113*fvu1u1u6^2)/18 + 
      (f113*fvu1u1u8^2)/12 + (f113*fvu1u1u9^2)/9 - 
      (f113*fvu1u1u10^2)/6 + (f113*fvu2u1u1)/3 + 
      (f113*tci11^2)/36 + (f109*tci12)/3 + 
      fvu1u1u9*(f116/9 + (f113*fvu1u1u10)/9 - (2*f113*tci12)/
         9) + fvu1u1u8*((6*f1 - f30 - 6*f104)/9 - 
        (f113*tci12)/6) + fvu1u1u4*(-(f113*fvu1u1u8)/6 - 
        (f113*tci12)/6) + fvu1u1u6*(-(f113*fvu1u1u9)/3 + 
        (2*f113*fvu1u1u10)/9 - (f113*tci12)/9) + 
      fvu1u1u10*((6*f113 - 6*f121 + f148)/9 + (f113*tci12)/
         3)) + ((-6*f104 + 10*f113 - 9*f114)*tci12*tcr11^2)/
     18 + ((4*f104 - 2*f113 + f114)*tcr11^3)/18 + 
    ((2*f104 + 7*f113 - 7*f114 + 6*f115 + 6*f117)*tci11^2*
      tcr12)/36 + ((-2*f104 - f113 + f114)*tcr12^3)/9 - 
    (2*(3*f104 - 8*f113 + 3*f114)*tci12*tcr21)/9 + 
    tcr11*(((25*f113 + 21*f114)*tci11^2)/108 + 
      ((-f113 + f114)*tci12*tcr12)/3 + 
      ((6*f104 - 5*f113)*tcr21)/9) + 
    ((2*f104 + f113 - f114)*tcr31)/3 + 
    ((2*f104 + f113 - f114)*tcr32)/12 + 
    ((-74*f104 - 11*f113 + 11*f114 + 18*f115 + 18*f117)*tcr33)/
     72;
L ieu1ueu5 = (f3 - f16 - 10*f58 + f60 + 9*f107 - 
      f122 + f126 - f151)/9 + ((-2*f104 + f113 - f114)*
      fvu1u1u1^2)/6 + ((-f113 - f115)*fvu1u1u3^2)/6 + 
    ((f114 - f117)*fvu1u1u8^2)/6 + ((-f115 + f117)*fvu1u1u9^2)/
     6 + fvu1u1u3*(f109/3 - (f104*fvu1u1u8)/3 + 
      (f115*fvu1u1u9)/3 + (f113*fvu1u1u10)/3) + 
    fvu1u1u8*(-f108/3 - (f114*fvu1u1u9)/3 + (f117*fvu1u1u10)/3) + 
    ((-f104 + f113)*fvu2u1u1)/3 + ((-f104 - f114)*fvu2u1u9)/3 + 
    ((f114 - f117)*fvu2u1u12)/3 + ((-f113 - f115)*fvu2u1u13)/3 + 
    ((-f115 + f117)*fvu2u1u15)/3 + 
    ((-f104 - f115 - f117)*tci11^2)/18 + 
    ((f108 - f109 + f111)*tci12)/3 + 
    ((f113 + f115)*fvu1u2u2*tci12)/3 + 
    ((-f114 + f117)*fvu1u2u9*tci12)/3 + 
    fvu1u1u1*(-f111/3 + (f104*fvu1u1u3)/3 + (f104*fvu1u1u8)/3 + 
      (f114*fvu1u1u9)/3 - (f113*fvu1u1u10)/3 + 
      ((-f113 + f114)*tci12)/3) + 
    fvu1u1u9*((f105 - f107 + f108 - f109 + f111)/3 - 
      (f117*fvu1u1u10)/3 - (f115*tci12)/3) + 
    fvu1u1u10*(-f105/3 - (f117*tci12)/3);
L ieu0ueu5 = f107/3;
L ieum1ueu5 = 0;
L ieum2ueu5 = 0;
L ieu2uou5 = (-4*f112*fvu4u28)/3 - (4*f112*fvu4u39)/3 - 
    (2*f112*fvu4u49)/9 - (4*f112*fvu4u51)/9 - (f112*fvu4u80)/12 - 
    (7*f112*fvu4u83)/12 + (3*f112*fvu4u91)/4 - (f112*fvu4u93)/12 - 
    (4*f112*fvu4u100)/9 + (f112*fvu4u102)/2 - (f112*fvu4u111)/4 - 
    (f112*fvu4u113)/2 - f112*fvu4u114 + (2*f112*fvu4u129)/3 - 
    (13*f112*fvu4u132)/3 + (7*f112*fvu4u139)/2 - 
    (4*f112*fvu4u141)/3 - (4*f112*fvu4u146)/9 - 
    (7*f112*fvu4u148)/6 + (4*f112*fvu4u174)/3 + 
    (4*f112*fvu4u182)/3 + (2*f112*fvu4u190)/9 + 
    (4*f112*fvu4u192)/9 + (f112*fvu4u213)/12 + 
    (67*f112*fvu4u215)/12 - (9*f112*fvu4u219)/4 + 
    (f112*fvu4u221)/12 + (4*f112*fvu4u225)/9 + (f112*fvu4u233)/4 + 
    (f112*fvu4u234)/2 + f112*fvu4u235 + (10*f112*fvu4u244)/3 - 
    (2*f112*fvu4u246)/3 - 2*f112*fvu4u250 - (2*f112*fvu4u252)/3 - 
    (2*f112*fvu4u255)/9 - 2*f112*fvu4u273 + 2*f112*fvu4u277 + 
    fvu3u25*((-2*f112*fvu1u1u3)/3 + (2*f112*fvu1u1u6)/3 - 
      (2*f112*fvu1u1u7)/3) + fvu3u71*((2*f112*fvu1u1u3)/3 - 
      (2*f112*fvu1u1u6)/3 + (2*f112*fvu1u1u7)/3) + 
    fvu3u70*((f112*fvu1u1u6)/3 - (f112*fvu1u1u8)/3 - 
      (f112*fvu1u1u10)/3) + fvu3u45*(-(f112*fvu1u1u2) + 
      (f112*fvu1u1u3)/3 + (2*f112*fvu1u1u5)/3 + 
      (f112*fvu1u1u6)/3 - f112*fvu1u1u8 + (f112*fvu1u1u9)/3 - 
      (f112*fvu1u1u10)/3) + fvu3u23*(-(f112*fvu1u1u1)/3 - 
      (f112*fvu1u1u6)/3 + (f112*fvu1u1u8)/3 + 
      (f112*fvu1u1u10)/3) + fvu3u43*((f112*fvu1u1u2)/3 + 
      (f112*fvu1u1u4)/3 + (f112*fvu1u1u6)/3 - (f112*fvu1u1u7)/3 - 
      (2*f112*fvu1u1u9)/3 + (f112*fvu1u1u10)/3) + 
    fvu3u63*(f112*fvu1u1u2 - (3*f112*fvu1u1u3)/2 - 
      (2*f112*fvu1u1u5)/3 - (19*f112*fvu1u1u6)/6 - 
      (f112*fvu1u1u7)/3 - (2*f112*fvu1u1u8)/3 - 
      (f112*fvu1u1u9)/3 + (7*f112*fvu1u1u10)/2) + 
    (13*f112*tci11^3*tci12)/15 + 
    fvu3u62*((-2*f112*fvu1u1u2)/3 + f112*fvu1u1u3 + 
      (f112*fvu1u1u4)/3 - (2*f112*fvu1u1u5)/3 - 
      (2*f112*fvu1u1u6)/3 + (f112*fvu1u1u7)/3 + f112*fvu1u1u8 + 
      f112*fvu1u1u9 + (2*f112*fvu1u1u10)/3 - 2*f112*tci12) + 
    fvu3u78*(-(f112*fvu1u1u2)/3 + (f112*fvu1u1u3)/3 - 
      (f112*fvu1u1u4)/3 + (f112*fvu1u1u6)/3 - (f112*fvu1u1u8)/3 + 
      (2*f112*fvu1u1u9)/3 + (2*f112*tci12)/3) + 
    fvu3u80*((2*f112*fvu1u1u2)/3 - (f112*fvu1u1u3)/3 - 
      (f112*fvu1u1u4)/3 + (2*f112*fvu1u1u5)/3 - 
      (2*f112*fvu1u1u6)/3 + f112*fvu1u1u7 - (5*f112*fvu1u1u8)/3 + 
      f112*fvu1u1u9 + (8*f112*tci12)/3) - 
    (10*f112*tci11^2*tci21)/27 + 
    tci12*((16*f112*tci31)/5 + 16*f112*tci32) - 
    (2072*f112*tci41)/135 - (64*f112*tci42)/5 - 
    (16*f112*tci43)/3 + ((53*f112*tci11^3)/108 + 
      (5*f112*tci12*tci21)/3 - (12*f112*tci31)/5 + 
      16*f112*tci32)*tcr11 + (61*f112*tci11*tcr11^3)/
     180 + fvu1u1u10*((169*f112*tci11^3)/1620 + 
      (25*f112*tci12*tci21)/9 + (76*f112*tci31)/5 + 
      (19*f112*tci21*tcr11)/3 - (19*f112*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f112*tci11^3)/270 + 
      (72*f112*tci31)/5 + 6*f112*tci21*tcr11 - 
      (3*f112*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f112*fvu3u25)/3 - (f112*fvu3u43)/3 + 
      (2*f112*fvu3u45)/3 - (f112*fvu3u62)/3 + (13*f112*fvu3u63)/6 + 
      (f112*fvu3u70)/3 - (2*f112*fvu3u71)/3 - (f112*fvu3u78)/3 - 
      (f112*fvu3u80)/3 + (19*f112*tci11^3)/108 - 
      (f112*tci12*tci21)/3 + 12*f112*tci31 + 
      5*f112*tci21*tcr11 - (f112*tci11*tcr11^2)/4) + 
    fvu1u1u5*((f112*tci11^3)/45 - (16*f112*tci12*tci21)/9 - 
      (16*f112*tci31)/5 - (4*f112*tci21*tcr11)/3 + 
      (f112*tci11*tcr11^2)/15) + 
    fvu1u1u8*((-133*f112*tci11^3)/810 + 
      (8*f112*tci12*tci21)/3 - (24*f112*tci31)/5 - 
      2*f112*tci21*tcr11 + (f112*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f112*tci11^3)/180 - f112*tci12*tci21 - 
      (36*f112*tci31)/5 - 3*f112*tci21*tcr11 + 
      (3*f112*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f112*tci11^3)/162 - (16*f112*tci12*tci21)/
       9 - 8*f112*tci31 - (10*f112*tci21*tcr11)/3 + 
      (f112*tci11*tcr11^2)/6) + 
    fvu1u1u9*((-19*f112*tci11^3)/270 - 
      (20*f112*tci12*tci21)/9 - (56*f112*tci31)/5 - 
      (14*f112*tci21*tcr11)/3 + (7*f112*tci11*tcr11^2)/
       30) + fvu1u1u3*((-391*f112*tci11^3)/1620 + 
      (f112*tci12*tci21)/3 - (84*f112*tci31)/5 - 
      7*f112*tci21*tcr11 + (7*f112*tci11*tcr11^2)/20) + 
    ((-218*f112*tci11^3)/243 - (40*f112*tci12*tci21)/3)*
     tcr12 + (-4*f112*tci11*tci12 - (40*f112*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f112*tci11*tci12)/15 - 
      3*f112*tci21 - 2*f112*tci11*tcr12) + 
    (130*f112*tci11*tcr33)/27;
L ieu1uou5 = 
   2*f112*fvu3u62 - (11*f112*tci11^3)/135 - 
    (4*f112*tci12*tci21)/3 - (48*f112*tci31)/5 - 
    4*f112*tci21*tcr11 + (f112*tci11*tcr11^2)/5;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
