#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f137;
S f67;
S f66;
S f180;
S f135;
S f65;
S f64;
S f133;
S f63;
S f132;
S f62;
S f131;
S f184;
S f130;
S f60;
S f185;
S f124;
S f74;
S f75;
S f126;
S f122;
S f123;
S f151;
S f150;
S f153;
S f152;
S f154;
S f157;
S f159;
S f158;
S f9;
S f19;
S f149;
S f16;
S f3;
S f17;
S f14;
S f7;
S f12;
S f13;
S f179;
S f29;
S f28;
S f23;
S f22;
S f20;
S f177;
S f176;
S f26;
S f175;
S f25;
S f168;
S f169;
S f48;
S f118;
S f160;
S f31;
S f162;
S f117;
S f163;
S f110;
S f165;
S f58;
S f109;
S f59;
S f53;
S f106;
S f56;
S f107;
S f57;
S f54;
S f105;
S f55;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu10 = (11*f3 + 11*f16 + 99*f57 + 11*f58 - 
      11*f59 + 11*f122 + 11*f126 + 74*f151)/27 - (f160*fvu3u1)/3 + 
    (f160*fvu3u2)/6 + (2*f160*fvu3u3)/9 + (11*f160*fvu3u5)/18 - 
    (f160*fvu3u6)/9 + (f160*fvu3u7)/3 + (16*f160*fvu3u8)/9 - 
    (f160*fvu3u10)/6 + (2*f160*fvu3u11)/9 + (2*f160*fvu3u12)/9 - 
    (f160*fvu3u13)/18 + (f160*fvu3u14)/3 + (f160*fvu3u17)/6 + 
    (f160*fvu3u18)/6 - (f160*fvu3u19)/9 - (2*f160*fvu3u24)/9 + 
    ((-4*f158 - f160)*fvu3u31)/9 + ((3*f149 - 14*f158)*fvu3u32)/
     18 - (f158*fvu3u33)/2 - (16*f158*fvu3u34)/9 - 
    (2*f158*fvu3u36)/9 + ((2*f158 - f160)*fvu3u37)/18 - 
    (f160*fvu3u38)/6 - (f158*fvu3u40)/3 + (f160*fvu3u42)/3 + 
    (2*f158*fvu3u44)/9 - (2*f158*fvu3u51)/9 - (f158*fvu3u52)/6 - 
    (f158*fvu3u53)/6 + (f158*fvu3u56)/18 - (f165*fvu3u57)/6 + 
    ((-f158 - f163)*fvu3u59)/6 + (f160*fvu1u1u1^3)/2 + 
    (11*f160*fvu1u1u2^3)/27 + ((2*f158 + f160)*fvu1u1u3^3)/18 - 
    (f158*fvu1u1u4^3)/3 + (2*(f158 - f160)*fvu1u1u5^3)/9 + 
    (35*f158*fvu1u1u6^3)/27 + ((4*f158 + 3*f160)*fvu1u1u7^3)/27 + 
    ((3*f158 + 4*f160)*fvu1u1u9^3)/27 + 
    fvu1u1u8^2*((-6*f117 + f118 + 3*f152 + f162 + 6*f165)/18 + 
      (f160*fvu1u1u9)/9) + ((-6*f117 + f118 + f162 + 6*f165)*
      fvu2u1u12)/9 + ((-6*f158 + 6*f163 + 6*f168 - f169 - 
       6*f184 + f185)*fvu2u1u14)/9 + 
    ((6*f117 - f118 + 6*f163 - 6*f165 - 6*f184 + f185)*
      fvu2u1u15)/9 + (2*f158*fvu1u2u2*tci11^2)/9 - 
    (2*f158*fvu1u2u3*tci11^2)/3 - (2*f160*fvu1u2u6*tci11^2)/
     3 - (f160*fvu1u2u7*tci11^2)/3 + 
    ((-8*f9 + f12 + 8*f13 - f14 + f16 - 8*f17 + f19 + 8*f20 - 
       8*f22 - f23 + f25 - 16*f26 + 2*f29 + 8*f53 - 8*f54 - 
       f55 + f56 + f57 + 10*f58 - f60 + 8*f62 + 16*f63 + 
       16*f64 - f65 - 2*f66 - 2*f67 - 8*f74 + f75 + 8*f105 - 
       f106 + f107 + 8*f123 - f124 + f126 + 8*f130 + 8*f131 - 
       f132 - f133 + 8*f135 - f137 + 8*f152 + f159 + 8*f175 - 
       f176 - f177)*tci12)/9 + 
    ((6*f117 - f118 - f162 - 6*f165)*fvu1u2u9*tci12)/9 - 
    (4*f160*fvu2u1u2*tci12)/9 - (2*f160*fvu2u1u3*tci12)/3 + 
    (f160*fvu2u1u5*tci12)/3 + (2*f158*fvu2u1u6*tci12)/3 - 
    (16*f160*fvu2u1u9*tci12)/9 - (4*f160*fvu2u1u11*tci12)/9 - 
    (2*f158*fvu2u1u13*tci12)/9 + 
    fvu2u1u8*((-6*f7 + f48 - 6*f149 - 6*f158 + 6*f168 - f169)/
       9 - (4*f158*tci12)/9) + fvu1u1u10^2*
     (-f150/6 - (f158*tci12)/6) + fvu1u1u4^2*
     ((f160*fvu1u1u5)/18 - (f158*fvu1u1u6)/9 + 
      (5*f158*fvu1u1u7)/18 + (2*f160*fvu1u1u8)/9 + 
      (7*f160*fvu1u1u9)/18 + (f158*fvu1u1u10)/6 + 
      (f158*tci12)/3) + fvu1u1u6^2*
     ((-6*f7 + f48 - 6*f149 - 3*f153 - 12*f158 + 6*f163 + 
        12*f168 - 2*f169 - 6*f184 + f185)/18 - 
      (4*f158*fvu1u1u7)/9 - (f158*fvu1u1u8)/3 - 
      (f158*fvu1u1u9)/6 + (f158*tci12)/2) + 
    fvu1u1u9^2*((6*f117 - f118 + 3*f150 + 3*f151 - 3*f152 + 
        3*f153 - 3*f154 + 6*f163 - 6*f165 - 6*f184 + f185)/18 + 
      (f158*fvu1u1u10)/9 + ((f158 - 10*f160)*tci12)/18) + 
    fvu1u1u3^2*(-(f158*fvu1u1u6)/6 + (f160*fvu1u1u7)/6 + 
      (f160*fvu1u1u8)/6 - (f158*fvu1u1u9)/18 + 
      (f158*fvu1u1u10)/9 + ((-2*f158 - 9*f160)*tci12)/18) + 
    fvu1u1u7^2*((6*f7 - f48 + 6*f149 + 3*f154 - f162)/18 + 
      ((4*f158 + f160)*fvu1u1u8)/18 - (f160*fvu1u1u9)/6 + 
      (f158*fvu1u1u10)/6 + ((3*f158 - 2*f160)*tci12)/18) + 
    fvu1u1u5^2*(-(f158*fvu1u1u6)/3 + ((4*f158 + f160)*fvu1u1u7)/
       18 + ((-2*f158 + f160)*fvu1u1u8)/18 + (f160*fvu1u1u9)/6 + 
      ((f158 - f160)*tci12)/9) + 
    fvu2u1u10*((6*f7 - f48 + 6*f149 - f162)/9 + 
      (2*(4*f158 - f160)*tci12)/9) + 
    fvu1u1u1^2*(-(f160*fvu1u1u2)/9 - (f160*fvu1u1u3)/6 - 
      (f160*fvu1u1u4)/9 - (f158*fvu1u1u6)/9 + 
      ((f158 - 3*f160)*fvu1u1u7)/9 + (f160*fvu1u1u8)/18 - 
      (f160*fvu1u1u9)/18 - (23*f160*tci12)/18) + 
    fvu1u1u2^2*((f160*fvu1u1u4)/9 - (f160*fvu1u1u5)/9 + 
      (f160*fvu1u1u8)/9 + (2*f160*fvu1u1u9)/9 - 
      (4*f160*tci12)/9) + fvu1u2u8*
     ((2*(4*f158 - f160)*tci11^2)/9 + 
      ((6*f7 - f48 + 6*f149 - f162)*tci12)/9) + 
    tci11^2*((-12*f117 + 2*f118 + 3*f151 + 18*f152 - 12*f158 + 
        8*f162 - 12*f163 + 12*f165 + 12*f168 - 2*f169 + 12*f184 - 
        2*f185)/108 + ((-3*f149 + 12*f158 + 7*f160 + 3*f165)*
        tci12)/108) + fvu1u1u10*
     ((-8*f9 + f12 + 8*f13 - f14 - 8*f22 + f25 - 8*f26 + 
        f29 + 8*f64 - f67 + 8*f150)/9 + (2*f158*fvu2u1u13)/9 + 
      (2*f158*fvu2u1u14)/3 + (2*f158*fvu2u1u15)/9 + 
      (7*f158*tci11^2)/27 + ((-6*f117 + f118 + 6*f165)*tci12)/
       9 - (2*f158*fvu1u2u2*tci12)/9) + 
    fvu1u1u9*((-2*f3 + 8*f13 - f14 + f16 - 8*f17 + f19 + 
        8*f20 - f23 - 16*f26 + 8*f28 + 2*f29 - f31 + 8*f53 - 
        8*f54 - f55 + f56 - 17*f57 + 18*f58 + 2*f59 - 2*f60 + 
        8*f62 + 16*f63 + 16*f64 - f65 - 2*f66 - 2*f67 - 8*f74 + 
        f75 + 8*f105 - f106 - f122 + 8*f123 - f124 + 8*f130 + 
        8*f131 - f132 - f133 + 16*f135 - 2*f137 - 8*f150 - 
        9*f151 + 8*f152 - 8*f153 + 8*f154 + 2*f159 + 8*f175 - 
        f176 - 2*f177 + 8*f179 - f180)/9 + (14*f160*fvu2u1u11)/
       9 + (2*f158*fvu2u1u15)/9 - (2*(f158 - 6*f160)*tci11^2)/
       27 + (f162*tci12)/9 + fvu1u1u10*
       ((-6*f117 + f118 + 6*f165)/9 + (2*f158*tci12)/9)) + 
    fvu1u1u3*((f160*fvu1u1u5^2)/6 - (f158*fvu1u1u6^2)/6 - 
      (f160*fvu1u1u4*fvu1u1u8)/3 + (f160*fvu1u1u8^2)/6 - 
      (f158*fvu1u1u9^2)/18 - (f160*fvu2u1u5)/3 + 
      (2*f158*fvu2u1u13)/9 + ((f158 + 12*f160)*tci11^2)/18 - 
      (f160*fvu1u1u5*tci12)/3 - (f160*fvu1u1u7*tci12)/3 - 
      (f160*fvu1u1u8*tci12)/3 - (2*f158*fvu1u2u2*tci12)/9 + 
      (f160*fvu1u2u7*tci12)/3 + fvu1u1u9*
       ((-2*f158*fvu1u1u10)/9 + (f158*tci12)/9) + 
      fvu1u1u6*((f158*fvu1u1u9)/3 + (f158*tci12)/3)) + 
    fvu1u1u6*((-8*f13 + f14 - 8*f64 + f67 + 8*f109 - f110 + 
        8*f153)/9 + ((6*f158 - 6*f168 + f169)*fvu1u1u7)/9 - 
      (8*f158*fvu1u1u7^2)/9 - (5*f158*fvu1u1u8^2)/9 - 
      (f158*fvu1u1u9^2)/6 + ((6*f158 - 6*f168 + f169)*fvu1u1u10)/
       9 - (f158*fvu1u1u10^2)/3 - (2*f158*fvu2u1u6)/3 + 
      (20*f158*fvu2u1u8)/9 + (2*f158*fvu2u1u14)/3 - 
      f158*tci11^2 + ((-6*f7 + f48 - 6*f149)*tci12)/9 + 
      (2*f158*fvu1u2u3*tci12)/3 + fvu1u1u9*
       ((-6*f163 + 6*f184 - f185)/9 - (f158*tci12)/3) + 
      fvu1u1u8*((6*f7 - f48 + 6*f149)/9 + (10*f158*tci12)/9)) + 
    fvu1u1u7*((f3 - f16 + 8*f26 - 8*f28 - f29 + f31 + 9*f57 - 
        9*f58 - f59 + f60 + f107 - 8*f109 + f110 - 8*f135 + 
        f137 - f151 - 8*f154 - f159 + f177 - 8*f179 + f180)/9 + 
      ((12*f158 - f160)*fvu1u1u8^2)/18 - (f160*fvu1u1u9^2)/6 + 
      (f158*fvu1u1u10^2)/6 + (2*(2*f158 - f160)*fvu2u1u7)/9 + 
      (16*f158*fvu2u1u8)/9 - (2*(4*f158 - f160)*fvu2u1u10)/9 + 
      ((8*f158 - 3*f160)*tci11^2)/27 + (f162*tci12)/9 - 
      (2*(4*f158 - f160)*fvu1u2u8*tci12)/9 + 
      fvu1u1u10*((-6*f158 + 6*f168 - f169)/9 + 
        (f158*tci12)/3) + fvu1u1u8*((-6*f7 + f48 - 6*f149)/9 + 
        ((-4*f158 - f160)*tci12)/9) + 
      fvu1u1u9*(f162/9 - (f160*tci12)/3)) + 
    fvu1u1u8*((8*f9 - f12 - 8*f13 + f14 - f16 + 8*f17 - f19 - 
        8*f20 + 8*f22 + f23 - f25 + 16*f26 - 2*f29 - 8*f53 + 
        8*f54 + f55 - f56 - f57 - 10*f58 + f60 - 8*f62 - 
        16*f63 - 16*f64 + f65 + 2*f66 + 2*f67 + 8*f74 - f75 - 
        8*f105 + f106 - f107 - 8*f123 + f124 - f126 - 8*f130 - 
        8*f131 + f132 + f133 - 8*f135 + f137 - 8*f152 - f159 - 
        8*f175 + f176 + f177)/9 + (f160*fvu1u1u9^2)/9 + 
      ((6*f117 - f118 - 6*f165)*fvu1u1u10)/9 + 
      (4*f158*fvu2u1u8)/9 + (8*f160*fvu2u1u9)/9 - 
      (2*(4*f158 - f160)*fvu2u1u10)/9 + (4*f160*fvu2u1u11)/9 + 
      ((4*f158 + 3*f160)*tci11^2)/54 - (f152*tci12)/3 - 
      (2*(4*f158 - f160)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*(-f162/9 - (2*f160*tci12)/9)) + 
    fvu1u1u4*((f160*fvu1u1u5^2)/18 + (4*f158*fvu1u1u6^2)/9 + 
      (5*f158*fvu1u1u7^2)/18 + (2*f160*fvu1u1u8^2)/9 + 
      (7*f160*fvu1u1u9^2)/18 + (f158*fvu1u1u10^2)/6 + 
      (2*f160*fvu2u1u2)/9 + (2*f160*fvu2u1u3)/3 - 
      (2*f158*fvu2u1u6)/3 + (4*f158*fvu2u1u8)/9 + 
      (4*f160*fvu2u1u11)/9 + ((-9*f158 - 2*f160)*tci11^2)/54 - 
      (7*f160*fvu1u1u9*tci12)/9 - (f158*fvu1u1u10*tci12)/3 + 
      (2*f158*fvu1u2u3*tci12)/3 + (2*f160*fvu1u2u6*tci12)/3 + 
      fvu1u1u7*((2*f158*fvu1u1u8)/9 - (f158*fvu1u1u10)/3 - 
        (5*f158*tci12)/9) + fvu1u1u6*((-2*f158*fvu1u1u7)/9 - 
        (2*f158*fvu1u1u8)/9 - (4*f158*tci12)/9) + 
      fvu1u1u8*((2*f160*fvu1u1u9)/9 - (4*f160*tci12)/9) + 
      fvu1u1u5*(-(f160*fvu1u1u9)/3 - (f160*tci12)/9)) + 
    fvu1u1u2*((-4*f160*fvu1u1u4^2)/9 - (2*f160*fvu1u1u5^2)/9 - 
      (f160*fvu1u1u8^2)/9 - (5*f160*fvu1u1u9^2)/9 + 
      (2*f160*fvu2u1u3)/3 + (2*f160*fvu2u1u4)/9 + 
      (10*f160*fvu2u1u11)/9 - (2*f160*tci11^2)/27 - 
      (2*f160*fvu1u1u5*tci12)/9 + (4*f160*fvu1u1u9*tci12)/9 + 
      (2*f160*fvu1u2u6*tci12)/3 + fvu1u1u4*
       ((2*f160*fvu1u1u5)/9 - (2*f160*fvu1u1u8)/9 - 
        (4*f160*fvu1u1u9)/9 + (2*f160*tci12)/9) + 
      fvu1u1u8*((-2*f160*fvu1u1u9)/9 + (2*f160*tci12)/9)) + 
    fvu1u1u5*(-(f158*fvu1u1u6^2)/3 + (f160*fvu1u1u7^2)/6 + 
      ((-2*f158 + f160)*fvu1u1u8^2)/18 + (f160*fvu1u1u9^2)/6 + 
      (2*f160*fvu2u1u4)/9 - (f160*fvu2u1u5)/3 + 
      (2*(2*f158 - f160)*fvu2u1u7)/9 + 
      ((-16*f158 + 13*f160)*tci11^2)/108 + 
      ((2*f158 - f160)*fvu1u1u8*tci12)/9 + 
      (f160*fvu1u1u9*tci12)/3 + (f160*fvu1u2u7*tci12)/3 + 
      fvu1u1u6*((2*f158*fvu1u1u8)/3 - (2*f158*tci12)/3) + 
      fvu1u1u7*(((-4*f158 - f160)*fvu1u1u8)/9 + 
        ((4*f158 + f160)*tci12)/9)) + 
    fvu1u1u1*((-4*f160*fvu1u1u2^2)/9 - (f160*fvu1u1u3^2)/6 - 
      (2*f160*fvu1u1u4^2)/9 - (f160*fvu1u1u5^2)/6 - 
      (4*f158*fvu1u1u6^2)/9 + ((4*f158 - 3*f160)*fvu1u1u7^2)/18 - 
      (7*f160*fvu1u1u8^2)/18 + (f160*fvu1u1u9^2)/18 + 
      (2*f160*fvu2u1u2)/9 - (4*f158*fvu2u1u8)/9 + 
      (8*f160*fvu2u1u9)/9 - (4*f160*fvu2u1u11)/9 + 
      (10*f160*tci11^2)/27 + (5*f160*fvu1u1u9*tci12)/9 + 
      fvu1u1u6*((2*f158*fvu1u1u7)/9 + (2*f158*fvu1u1u8)/9 - 
        (2*f158*tci12)/9) + fvu1u1u2*((2*f160*fvu1u1u4)/9 + 
        (2*f160*fvu1u1u8)/9 + (2*f160*fvu1u1u9)/9 - 
        (2*f160*tci12)/9) + fvu1u1u3*((f160*fvu1u1u4)/3 + 
        (f160*tci12)/3) + fvu1u1u4*((2*f158*fvu1u1u6)/9 - 
        (2*f158*fvu1u1u7)/9 - (f160*fvu1u1u8)/9 - 
        (2*f160*fvu1u1u9)/9 + (4*f160*tci12)/9) + 
      fvu1u1u8*((-2*f160*fvu1u1u9)/9 + (7*f160*tci12)/9) + 
      fvu1u1u7*((-2*f158*fvu1u1u8)/9 + (f160*fvu1u1u9)/3 + 
        (2*(f158 + 3*f160)*tci12)/9)) + 
    (29*f160*tci12*tcr11^2)/18 - (f160*tcr11^3)/2 + 
    ((3*f149 - 8*f158 + 16*f160 - 3*f165)*tci11^2*tcr12)/18 + 
    (26*f160*tci12*tcr21)/9 + 
    tcr11*((-4*f160*tci11^2)/27 - (2*f160*tci12*tcr12)/
       3 - (10*f160*tcr21)/9) + 
    ((9*f149 + 12*f158 - 26*f160 + 12*f163 - 9*f165)*tcr33)/36;
L ieu1ueu10 = (f3 + f16 + 9*f57 + f58 - f59 + f122 + 
      f126 + 10*f151)/9 + ((-f149 - 2*f158 + f163)*fvu1u1u6^2)/
     6 + ((f149 - f160)*fvu1u1u7^2)/6 + 
    ((f160 + f165)*fvu1u1u8^2)/6 + ((f163 - f165)*fvu1u1u9^2)/6 + 
    fvu1u1u8*(-f152/3 - (f160*fvu1u1u9)/3 - (f165*fvu1u1u10)/3) + 
    ((-f149 - f158)*fvu2u1u8)/3 + ((f149 - f160)*fvu2u1u10)/3 + 
    ((f160 + f165)*fvu2u1u12)/3 + ((-f158 + f163)*fvu2u1u14)/3 + 
    ((f163 - f165)*fvu2u1u15)/3 + 
    ((-f158 + 4*f160 - f163 + f165)*tci11^2)/18 + 
    (f152*tci12)/3 + ((f149 - f160)*fvu1u2u8*tci12)/3 + 
    ((-f160 - f165)*fvu1u2u9*tci12)/3 + 
    fvu1u1u6*(f153/3 + (f158*fvu1u1u7)/3 + (f149*fvu1u1u8)/3 - 
      (f163*fvu1u1u9)/3 + (f158*fvu1u1u10)/3 - 
      (f149*tci12)/3) + fvu1u1u7*(-f154/3 - (f149*fvu1u1u8)/3 + 
      (f160*fvu1u1u9)/3 - (f158*fvu1u1u10)/3 + 
      (f160*tci12)/3) + fvu1u1u9*
     ((-f150 - f151 + f152 - f153 + f154)/3 + 
      (f165*fvu1u1u10)/3 + (f160*tci12)/3) + 
    fvu1u1u10*(f150/3 + (f165*tci12)/3);
L ieu0ueu10 = f151/3;
L ieum1ueu10 = 0;
L ieum2ueu10 = 0;
L ieu2uou10 = (-2*f157*fvu4u28)/3 - (2*f157*fvu4u39)/3 + 
    (2*f157*fvu4u49)/9 - (2*f157*fvu4u51)/9 + (f157*fvu4u80)/12 - 
    (17*f157*fvu4u83)/12 - (3*f157*fvu4u91)/4 + (f157*fvu4u93)/12 + 
    (4*f157*fvu4u100)/9 - (7*f157*fvu4u102)/6 + (f157*fvu4u111)/4 + 
    (f157*fvu4u113)/2 + f157*fvu4u114 - (2*f157*fvu4u129)/3 + 
    (13*f157*fvu4u132)/3 - (7*f157*fvu4u139)/2 - 
    (2*f157*fvu4u141)/3 + (4*f157*fvu4u146)/9 + (f157*fvu4u148)/2 + 
    (2*f157*fvu4u174)/3 + (2*f157*fvu4u182)/3 - 
    (2*f157*fvu4u190)/9 + (2*f157*fvu4u192)/9 - (f157*fvu4u213)/12 - 
    (43*f157*fvu4u215)/12 + (17*f157*fvu4u219)/4 - 
    (f157*fvu4u221)/12 - (4*f157*fvu4u225)/9 - (f157*fvu4u233)/4 - 
    (f157*fvu4u234)/2 - f157*fvu4u235 + (14*f157*fvu4u244)/3 + 
    (2*f157*fvu4u246)/3 - (4*f157*fvu4u252)/3 - 
    (10*f157*fvu4u255)/9 - 2*f157*fvu4u273 + 2*f157*fvu4u277 + 
    fvu3u25*(-(f157*fvu1u1u3)/3 + (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u7)/3) + fvu3u71*((f157*fvu1u1u3)/3 - 
      (f157*fvu1u1u6)/3 + (f157*fvu1u1u7)/3) + 
    fvu3u63*((5*f157*fvu1u1u3)/2 + (2*f157*fvu1u1u5)/3 + 
      (13*f157*fvu1u1u6)/6 + (f157*fvu1u1u7)/3 - 
      (f157*fvu1u1u8)/3 - (2*f157*fvu1u1u9)/3 - 
      (5*f157*fvu1u1u10)/2) + fvu3u45*(-(f157*fvu1u1u3)/3 - 
      (2*f157*fvu1u1u5)/3 - (f157*fvu1u1u6)/3 + f157*fvu1u1u7 + 
      (2*f157*fvu1u1u9)/3 - (2*f157*fvu1u1u10)/3) + 
    fvu3u23*((f157*fvu1u1u1)/3 + (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u8)/3 - (f157*fvu1u1u10)/3) + 
    fvu3u43*(-(f157*fvu1u1u2)/3 - (f157*fvu1u1u4)/3 - 
      (f157*fvu1u1u6)/3 + (f157*fvu1u1u7)/3 + 
      (2*f157*fvu1u1u9)/3 - (f157*fvu1u1u10)/3) + 
    fvu3u62*(-(f157*fvu1u1u2)/3 - (f157*fvu1u1u4)/3 + 
      (2*f157*fvu1u1u5)/3 - (f157*fvu1u1u6)/3 - 
      (f157*fvu1u1u7)/3 + (f157*fvu1u1u10)/3) + 
    fvu3u70*(-(f157*fvu1u1u6)/3 + (f157*fvu1u1u8)/3 + 
      (f157*fvu1u1u10)/3) + (151*f157*tci11^3*tci12)/135 + 
    fvu3u78*((f157*fvu1u1u2)/3 - (f157*fvu1u1u3)/3 + 
      (f157*fvu1u1u4)/3 - (f157*fvu1u1u6)/3 + (f157*fvu1u1u8)/3 - 
      (2*f157*fvu1u1u9)/3 - (2*f157*tci12)/3) + 
    fvu3u80*((f157*fvu1u1u2)/3 + (f157*fvu1u1u3)/3 + 
      (f157*fvu1u1u4)/3 - (2*f157*fvu1u1u5)/3 - 
      (4*f157*fvu1u1u6)/3 + 2*f157*fvu1u1u7 - 
      (4*f157*fvu1u1u8)/3 + 2*f157*fvu1u1u9 + f157*fvu1u1u10 + 
      (4*f157*tci12)/3) + (2*f157*tci11^2*tci21)/9 + 
    fvu1u1u8*((-8*f157*tci11^3)/81 + (8*f157*tci12*tci21)/
       3) + tci12*((48*f157*tci31)/5 + 16*f157*tci32) + 
    (4*f157*tci41)/3 + ((35*f157*tci11^3)/108 - 
      (f157*tci12*tci21)/3 + 12*f157*tci31)*tcr11 + 
    (-(f157*tci11*tci12)/5 + 5*f157*tci21)*tcr11^2 - 
    (f157*tci11*tcr11^3)/4 + 
    fvu1u1u3*((41*f157*tci11^3)/324 + f157*tci12*tci21 + 
      12*f157*tci31 + 5*f157*tci21*tcr11 - 
      (f157*tci11*tcr11^2)/4) + 
    fvu1u1u6*((f157*tci11^3)/36 + (11*f157*tci12*tci21)/3 + 
      12*f157*tci31 + 5*f157*tci21*tcr11 - 
      (f157*tci11*tcr11^2)/4) + 
    fvu1u1u5*(-(f157*tci11^3)/45 + (16*f157*tci12*tci21)/
       9 + (16*f157*tci31)/5 + (4*f157*tci21*tcr11)/3 - 
      (f157*tci11*tcr11^2)/15) + 
    fvu1u1u7*((89*f157*tci11^3)/810 - (32*f157*tci12*tci21)/
       9 - (8*f157*tci31)/5 - (2*f157*tci21*tcr11)/3 + 
      (f157*tci11*tcr11^2)/30) + 
    fvu1u1u9*((23*f157*tci11^3)/135 - (52*f157*tci12*tci21)/
       9 - (16*f157*tci31)/5 - (4*f157*tci21*tcr11)/3 + 
      (f157*tci11*tcr11^2)/15) + 
    fvu1u1u10*((17*f157*tci11^3)/1620 - 
      (37*f157*tci12*tci21)/9 - (52*f157*tci31)/5 - 
      (13*f157*tci21*tcr11)/3 + (13*f157*tci11*tcr11^2)/
       60) + fvu1u1u1*((f157*fvu3u25)/3 + (f157*fvu3u43)/3 + 
      (f157*fvu3u45)/3 + (f157*fvu3u62)/3 - (13*f157*fvu3u63)/6 - 
      (f157*fvu3u70)/3 - (f157*fvu3u71)/3 + (f157*fvu3u78)/3 - 
      (2*f157*fvu3u80)/3 - (19*f157*tci11^3)/108 + 
      (f157*tci12*tci21)/3 - 12*f157*tci31 - 
      5*f157*tci21*tcr11 + (f157*tci11*tcr11^2)/4) - 
    (40*f157*tci12*tci21*tcr12)/3 - 
    4*f157*tci11*tci12*tcr12^2;
L ieu1uou10 = 
   2*f157*fvu3u80 + (4*f157*tci11^3)/27 - 
    4*f157*tci12*tci21;
L ieu0uou10 = 0;
L ieum1uou10 = 0;
L ieum2uou10 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou10+w^2*ieu1uou10+w^3*ieu0uou10+w^4*ieum1uou10+w^5*ieum2uou10;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou10a = K[w^1];
L ieu1uou10a = K[w^2];
L ieu0uou10a = K[w^3];
L ieum1uou10a = K[w^4];
L ieum2uou10a = K[w^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_odd.c> "%O"
#write <e10_odd.c> "return Eps5o2<T>("
#write <e10_odd.c> "%E", ieu2uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu0uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum2uou10a
#write <e10_odd.c> ");\n}"
L H=+u^1*ieu2ueu10+u^2*ieu1ueu10+u^3*ieu0ueu10+u^4*ieum1ueu10+u^5*ieum2ueu10;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu10a = H[u^1];
L ieu1ueu10a = H[u^2];
L ieu0ueu10a = H[u^3];
L ieum1ueu10a = H[u^4];
L ieum2ueu10a = H[u^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_even.c> "%O"
#write <e10_even.c> "return Eps5o2<T>("
#write <e10_even.c> "%E", ieu2ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu0ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum2ueu10a
#write <e10_even.c> ");\n}"
.end
