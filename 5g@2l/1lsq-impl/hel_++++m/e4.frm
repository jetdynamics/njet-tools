#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f60;
S f126;
S f122;
S f151;
S f159;
S f9;
S f19;
S f3;
S f17;
S f12;
S f7;
S f28;
S f22;
S f177;
S f25;
S f38;
S f49;
S f48;
S f31;
S f44;
S f47;
S f33;
S f46;
S f34;
S f40;
S f36;
S f42;
S f58;
S f59;
S f107;
S f57;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u7;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u2;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu4 = (-74*f3 - 666*f57 + 556*f58 + 74*f59 - 
      63*f60 - 11*f107 - 11*f122 - 63*f126 - 11*f151 + 63*f159 - 
      11*f177)/27 - (f38*fvu3u1)/3 + (f38*fvu3u2)/6 - 
    (f38*fvu3u4)/18 - (f38*fvu3u6)/9 + ((f38 - 3*f46)*fvu3u12)/
     18 + (f38*fvu3u14)/9 - (f38*fvu3u19)/9 + (f38*fvu3u31)/9 + 
    ((3*f7 + f38)*fvu3u32)/18 - (f34*fvu3u33)/6 + 
    (f42*fvu3u35)/6 - (2*f38*fvu3u36)/9 - (f38*fvu3u37)/9 - 
    (f38*fvu3u38)/6 + (f38*fvu3u40)/6 + (f38*fvu3u42)/3 - 
    (f38*fvu1u1u1^3)/18 + (f38*fvu1u1u3^3)/18 - 
    (f38*fvu1u1u5^3)/3 - (f38*fvu1u1u6^3)/9 + 
    (f17*fvu1u1u8^2)/6 + fvu1u1u4^2*((3*f9 - f36 + f44)/18 + 
      (f38*fvu1u1u8)/18) + ((-f44 - 6*f46 + 6*f47 - f49)*
      fvu2u1u2)/9 + ((-f36 + f44)*fvu2u1u6)/9 + 
    ((f36 - f48)*fvu2u1u8)/9 + ((f40 + f48)*fvu2u1u10)/9 - 
    (f38*fvu1u2u7*tci11^2)/3 + 
    ((-10*f3 - f25 - f31 - 90*f57 + 80*f58 + 10*f59 - 9*f60 - 
       f107 - f122 - 9*f126 - f151 + 9*f159 - f177)*tci12)/
     9 + ((f36 - f44)*fvu1u2u3*tci12)/9 + 
    ((f40 + f48)*fvu1u2u8*tci12)/9 + (4*f38*fvu2u1u1*tci12)/
     9 + (f38*fvu2u1u5*tci12)/3 + 
    fvu1u1u8*(-f19/9 + (2*f38*fvu2u1u9)/9 + (f38*tci11^2)/54 - 
      (f17*tci12)/3) + fvu1u1u3^2*(-(f38*fvu1u1u4)/18 + 
      (f38*fvu1u1u7)/6 - (f38*tci12)/2) + 
    fvu2u1u9*((f40 - 6*f46 + 6*f47 - f49)/9 - 
      (4*f38*tci12)/9) + fvu1u1u5^2*((f38*fvu1u1u6)/6 - 
      (f38*fvu1u1u7)/18 + (f38*fvu1u1u8)/9 - (f38*tci12)/9) + 
    fvu1u1u6^2*((3*f22 + f36 - f48)/18 - (f38*fvu1u1u7)/9 + 
      (f38*fvu1u1u8)/18 - (f38*tci12)/18) + 
    fvu1u1u7^2*((3*f28 + f40 + f48)/18 - (f38*fvu1u1u8)/6 + 
      (f38*tci12)/6) + fvu1u1u1^2*
     ((-3*f3 - 3*f9 - 3*f17 - 3*f22 - 3*f28 + f40 - f44 - 
        12*f46 + 12*f47 - 2*f49 - 27*f57 + 27*f58 + 3*f59 - 
        3*f60 - 3*f126 + 3*f159)/18 - (f38*fvu1u1u3)/18 - 
      (f38*fvu1u1u7)/6 + (f38*fvu1u1u8)/18 + (f38*tci12)/6) + 
    tci11^2*((-21*f3 - 18*f22 - 18*f28 - 2*f36 + 2*f40 - 
        12*f46 + 12*f47 - 2*f49 - 189*f57 + 189*f58 + 21*f59 - 
        21*f60 - 21*f126 + 21*f159)/108 + 
      ((-f7 - f34 + 4*f38 - 3*f42 - 2*f46)*tci12)/36) + 
    fvu1u1u3*(-(f38*fvu1u1u4^2)/18 + (f38*fvu1u1u5^2)/6 - 
      (2*f38*fvu2u1u1)/9 - (f38*fvu2u1u5)/3 + 
      (35*f38*tci11^2)/54 + (f38*fvu1u1u4*tci12)/9 - 
      (f38*fvu1u1u5*tci12)/3 - (f38*fvu1u1u7*tci12)/3 + 
      (f38*fvu1u2u7*tci12)/3) + 
    fvu1u1u6*(-f25/9 + (f38*fvu1u1u8^2)/18 - (2*f38*fvu2u1u8)/9 + 
      (2*f38*tci11^2)/27 - (f48*tci12)/9 + 
      fvu1u1u7*(-f36/9 + (2*f38*fvu1u1u8)/9 - (2*f38*tci12)/
         9) + fvu1u1u8*(f48/9 - (f38*tci12)/9)) + 
    fvu1u1u4*(-f12/9 - (f44*fvu1u1u6)/9 + (f36*fvu1u1u7)/9 + 
      (f38*fvu1u1u8^2)/18 - (f9*tci12)/3 + 
      fvu1u1u8*((-6*f46 + 6*f47 - f49)/9 - (f38*tci12)/9)) + 
    fvu1u1u1*((10*f3 + f12 + f19 + f25 + f31 + 90*f57 - 
        80*f58 - 10*f59 + 9*f60 + f107 + f122 + 9*f126 + f151 - 
        9*f159 + f177)/9 + (f38*fvu1u1u3^2)/18 - 
      (f38*fvu1u1u5^2)/6 + (f44*fvu1u1u6)/9 - 
      (f38*fvu1u1u8^2)/18 + fvu1u1u4*((6*f46 - 6*f47 + f49)/9 - 
        (f38*fvu1u1u8)/9) - (2*f38*fvu2u1u1)/9 + 
      (2*f38*fvu2u1u9)/9 - (5*f38*tci11^2)/18 + 
      ((3*f3 + 3*f9 + 3*f17 + 3*f22 + 3*f28 - f40 + f44 + 
         27*f57 - 27*f58 - 3*f59 + 3*f60 + 3*f126 - 3*f159)*
        tci12)/9 + fvu1u1u3*((f38*fvu1u1u4)/9 - 
        (f38*tci12)/9) + fvu1u1u8*((6*f46 - 6*f47 + f49)/9 + 
        (f38*tci12)/9) + fvu1u1u7*(-f40/9 + (f38*tci12)/3)) + 
    fvu1u1u7*(-f31/9 - (f38*fvu1u1u8^2)/6 - (4*f38*fvu2u1u7)/9 - 
      (2*f38*fvu2u1u8)/9 - (f38*tci11^2)/9 - (f36*tci12)/9 + 
      fvu1u1u8*(-f48/9 + (f38*tci12)/3)) + 
    fvu1u1u5*((f38*fvu1u1u6^2)/6 + (f38*fvu1u1u7^2)/6 + 
      (f38*fvu1u1u8^2)/9 - (f38*fvu2u1u5)/3 - 
      (4*f38*fvu2u1u7)/9 + (13*f38*tci11^2)/108 - 
      (2*f38*fvu1u1u8*tci12)/9 + (f38*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*((f38*fvu1u1u8)/9 - (f38*tci12)/9) + 
      fvu1u1u6*(-(f38*fvu1u1u8)/3 + (f38*tci12)/3)) + 
    ((2*f38 - 3*f42 - 2*f46)*tci12*tcr11^2)/6 + 
    ((f42 + 4*f46)*tcr11^3)/18 + 
    ((6*f7 + 6*f34 + 19*f38 - 7*f42 + 2*f46)*tci11^2*tcr12)/
     36 + ((-f38 + f42 - 2*f46)*tcr12^3)/9 + 
    (2*(f38 - f42 - f46)*tci12*tcr21)/3 + 
    tcr11*(((3*f38 + 7*f42)*tci11^2)/36 + 
      ((-f38 + f42)*tci12*tcr12)/3 + (2*f46*tcr21)/3) + 
    ((f38 - f42 + 2*f46)*tcr31)/3 + 
    ((f38 - f42 + 2*f46)*tcr32)/12 + 
    ((18*f7 + 18*f34 - 11*f38 + 11*f42 - 74*f46)*tcr33)/72;
L ieu1ueu4 = (-10*f3 - 90*f57 + 80*f58 + 10*f59 - 9*f60 - 
      f107 - f122 - 9*f126 - f151 + 9*f159 - f177)/9 + 
    ((f38 - f42 - 2*f46)*fvu1u1u1^2)/6 + 
    ((-f34 + f42)*fvu1u1u4^2)/6 + ((-f7 + f34)*fvu1u1u6^2)/6 + 
    ((f7 + f38)*fvu1u1u7^2)/6 - (f17*fvu1u1u8)/3 + 
    fvu1u1u4*(-f9/3 - (f42*fvu1u1u6)/3 + (f34*fvu1u1u7)/3 - 
      (f46*fvu1u1u8)/3) + ((-f42 - f46)*fvu2u1u2)/3 + 
    ((-f34 + f42)*fvu2u1u6)/3 + ((-f7 + f34)*fvu2u1u8)/3 + 
    ((f38 - f46)*fvu2u1u9)/3 + ((f7 + f38)*fvu2u1u10)/3 + 
    ((-f34 + f38 - f46)*tci11^2)/18 + 
    ((-f3 - f22 - f28 - 9*f57 + 9*f58 + f59 - f60 - f126 + 
       f159)*tci12)/3 + ((f34 - f42)*fvu1u2u3*tci12)/3 + 
    ((f7 + f38)*fvu1u2u8*tci12)/3 + 
    fvu1u1u6*(-f22/3 - (f34*fvu1u1u7)/3 + (f7*fvu1u1u8)/3 - 
      (f7*tci12)/3) + fvu1u1u7*(-f28/3 - (f7*fvu1u1u8)/3 - 
      (f34*tci12)/3) + fvu1u1u1*
     ((f3 + f9 + f17 + f22 + f28 + 9*f57 - 9*f58 - f59 + 
        f60 + f126 - f159)/3 + (f46*fvu1u1u4)/3 + 
      (f42*fvu1u1u6)/3 - (f38*fvu1u1u7)/3 + (f46*fvu1u1u8)/3 + 
      ((-f38 + f42)*tci12)/3);
L ieu0ueu4 = 
   (-f3 - 9*f57 + 9*f58 + f59 - f60 - f126 + f159)/3;
L ieum1ueu4 = 0;
L ieum2ueu4 = 0;
L ieu2uou4 = -(f33*fvu4u25)/3 + (7*f33*fvu4u28)/3 + 
    (f33*fvu4u39)/3 - (f33*fvu4u41)/3 + (f33*fvu4u49)/9 + 
    (f33*fvu4u51)/3 + (f33*fvu4u80)/3 - (11*f33*fvu4u83)/3 + 
    3*f33*fvu4u91 + (f33*fvu4u93)/3 - (f33*fvu4u100)/9 - 
    (7*f33*fvu4u102)/9 - (2*f33*fvu4u139)/3 + (2*f33*fvu4u148)/9 + 
    (5*f33*fvu4u171)/12 - (3*f33*fvu4u174)/4 - (11*f33*fvu4u182)/4 + 
    (5*f33*fvu4u184)/12 + (f33*fvu4u190)/3 - (17*f33*fvu4u192)/18 + 
    (f33*fvu4u199)/4 + (f33*fvu4u201)/2 + f33*fvu4u202 + 
    (2*f33*fvu4u213)/3 - (2*f33*fvu4u215)/3 + (2*f33*fvu4u221)/3 + 
    3*f33*fvu4u244 + (8*f33*fvu4u246)/3 - 2*f33*fvu4u250 - 
    (13*f33*fvu4u252)/6 - (f33*fvu4u255)/2 - (5*f33*fvu4u271)/12 - 
    (43*f33*fvu4u273)/12 + (19*f33*fvu4u277)/12 - 
    (5*f33*fvu4u279)/12 - (f33*fvu4u282)/3 - (f33*fvu4u289)/4 - 
    (f33*fvu4u290)/2 - f33*fvu4u291 - (2*f33*fvu4u293)/3 - 
    (2*f33*fvu4u295)/3 - 2*f33*fvu4u309 + 2*f33*fvu4u313 + 
    fvu3u43*((f33*fvu1u1u6)/6 - (f33*fvu1u1u7)/6 - 
      (f33*fvu1u1u9)/6) + fvu3u23*((f33*fvu1u1u2)/6 + 
      (f33*fvu1u1u6)/6 - (f33*fvu1u1u7)/6 - (f33*fvu1u1u9)/6) + 
    fvu3u63*((f33*fvu1u1u6)/3 + (f33*fvu1u1u7)/3 + 
      (f33*fvu1u1u9)/3 - (2*f33*fvu1u1u10)/3) + 
    fvu3u70*((f33*fvu1u1u2)/6 - (f33*fvu1u1u3)/6 + 
      (f33*fvu1u1u6)/3 - (f33*fvu1u1u8)/3 - (f33*fvu1u1u9)/6 - 
      (f33*fvu1u1u10)/3) + fvu3u25*((-7*f33*fvu1u1u1)/6 - 
      (f33*fvu1u1u2)/3 + (f33*fvu1u1u3)/6 + f33*fvu1u1u4 - 
      (2*f33*fvu1u1u5)/3 - (f33*fvu1u1u6)/2 + 
      (7*f33*fvu1u1u7)/6 + (f33*fvu1u1u10)/3) + 
    fvu3u81*((f33*fvu1u1u3)/6 + (f33*fvu1u1u4)/3 + 
      (f33*fvu1u1u6)/6 - (f33*fvu1u1u7)/6 - (f33*fvu1u1u9)/3 + 
      (f33*fvu1u1u10)/3) - (13*f33*tci11^3*tci12)/15 + 
    fvu3u71*(-(f33*fvu1u1u2)/3 + f33*fvu1u1u4 - 
      (2*f33*fvu1u1u5)/3 - f33*fvu1u1u7 + (f33*fvu1u1u8)/3 - 
      2*f33*tci12) + fvu3u82*((f33*fvu1u1u3)/3 + 
      (f33*fvu1u1u4)/3 - (f33*fvu1u1u7)/3 + (f33*fvu1u1u8)/3 - 
      (f33*fvu1u1u9)/3 - (2*f33*tci12)/3) + 
    fvu3u78*((f33*fvu1u1u2)/3 - (f33*fvu1u1u3)/3 - 
      (f33*fvu1u1u4)/3 + (f33*fvu1u1u6)/3 - (f33*fvu1u1u8)/3 + 
      (2*f33*tci12)/3) + fvu3u80*((-2*f33*fvu1u1u2)/3 - 
      (f33*fvu1u1u3)/3 + (2*f33*fvu1u1u4)/3 + 
      (2*f33*fvu1u1u5)/3 - (11*f33*fvu1u1u6)/6 + 
      (7*f33*fvu1u1u7)/6 - (3*f33*fvu1u1u8)/2 + 
      (13*f33*fvu1u1u9)/6 + (3*f33*tci12)/2) + 
    fvu3u45*((f33*fvu1u1u2)/2 + (f33*fvu1u1u3)/6 - 
      f33*fvu1u1u4 + (2*f33*fvu1u1u5)/3 - (7*f33*fvu1u1u6)/3 + 
      (5*f33*fvu1u1u7)/3 - 2*f33*fvu1u1u8 - (f33*fvu1u1u9)/6 - 
      (f33*fvu1u1u10)/3 + 2*f33*tci12) + 
    (10*f33*tci11^2*tci21)/27 + 
    fvu1u1u10*((-4*f33*tci11^3)/81 + (4*f33*tci12*tci21)/
       3) + tci12*((-16*f33*tci31)/5 - 16*f33*tci32) + 
    (2072*f33*tci41)/135 + (64*f33*tci42)/5 + 
    (16*f33*tci43)/3 + ((-217*f33*tci11^3)/405 - 
      (4*f33*tci12*tci21)/3 - 16*f33*tci32)*tcr11 - 
    (13*f33*tci11*tcr11^3)/45 + 
    fvu1u1u6*((-97*f33*tci11^3)/1620 + (49*f33*tci12*tci21)/
       9 + (52*f33*tci31)/5 + (13*f33*tci21*tcr11)/3 - 
      (13*f33*tci11*tcr11^2)/60) + 
    fvu1u1u4*((11*f33*tci11^3)/135 + (4*f33*tci12*tci21)/
       3 + (48*f33*tci31)/5 + 4*f33*tci21*tcr11 - 
      (f33*tci11*tcr11^2)/5) + 
    fvu1u1u8*((-4*f33*tci11^3)/135 + (13*f33*tci12*tci21)/
       3 + (48*f33*tci31)/5 + 4*f33*tci21*tcr11 - 
      (f33*tci11*tcr11^2)/5) + 
    fvu1u1u9*((91*f33*tci11^3)/540 - (11*f33*tci12*tci21)/
       3 + (12*f33*tci31)/5 + f33*tci21*tcr11 - 
      (f33*tci11*tcr11^2)/20) + 
    fvu1u1u7*((229*f33*tci11^3)/1620 - (37*f33*tci12*tci21)/
       9 - (4*f33*tci31)/5 - (f33*tci21*tcr11)/3 + 
      (f33*tci11*tcr11^2)/60) + 
    fvu1u1u2*((f33*fvu3u43)/6 - (f33*fvu3u63)/3 - 
      (137*f33*tci11^3)/1620 + (2*f33*tci12*tci21)/9 - 
      (28*f33*tci31)/5 - (7*f33*tci21*tcr11)/3 + 
      (7*f33*tci11*tcr11^2)/60) + 
    fvu1u1u5*((2*f33*tci11^3)/45 - (32*f33*tci12*tci21)/9 - 
      (32*f33*tci31)/5 - (8*f33*tci21*tcr11)/3 + 
      (2*f33*tci11*tcr11^2)/15) + 
    fvu1u1u1*((5*f33*fvu3u45)/6 + (f33*fvu3u70)/2 + 
      (2*f33*fvu3u71)/3 + (f33*fvu3u78)/3 - (f33*fvu3u80)/3 - 
      (f33*fvu3u81)/2 - (f33*fvu3u82)/3 - (53*f33*tci11^3)/405 - 
      (48*f33*tci31)/5 - 4*f33*tci21*tcr11 + 
      (f33*tci11*tcr11^2)/5) + 
    ((218*f33*tci11^3)/243 + (40*f33*tci12*tci21)/3)*
     tcr12 + (4*f33*tci11*tci12 + (40*f33*tci21)/3)*
     tcr12^2 + tcr11^2*((f33*tci11*tci12)/15 + 
      2*f33*tci21 + 2*f33*tci11*tcr12) - 
    (130*f33*tci11*tcr33)/27;
L ieu1uou4 = 
   -2*f33*fvu3u45 + (11*f33*tci11^3)/135 + 
    (4*f33*tci12*tci21)/3 + (48*f33*tci31)/5 + 
    4*f33*tci21*tcr11 - (f33*tci11*tcr11^2)/5;
L ieu0uou4 = 0;
L ieum1uou4 = 0;
L ieum2uou4 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou4+w^2*ieu1uou4+w^3*ieu0uou4+w^4*ieum1uou4+w^5*ieum2uou4;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou4a = K[w^1];
L ieu1uou4a = K[w^2];
L ieu0uou4a = K[w^3];
L ieum1uou4a = K[w^4];
L ieum2uou4a = K[w^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_odd.c> "%O"
#write <e4_odd.c> "return Eps5o2<T>("
#write <e4_odd.c> "%E", ieu2uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu0uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum2uou4a
#write <e4_odd.c> ");\n}"
L H=+u^1*ieu2ueu4+u^2*ieu1ueu4+u^3*ieu0ueu4+u^4*ieum1ueu4+u^5*ieum2ueu4;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu4a = H[u^1];
L ieu1ueu4a = H[u^2];
L ieu0ueu4a = H[u^3];
L ieum1ueu4a = H[u^4];
L ieum2ueu4a = H[u^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_even.c> "%O"
#write <e4_even.c> "return Eps5o2<T>("
#write <e4_even.c> "%E", ieu2ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu0ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum2ueu4a
#write <e4_even.c> ");\n}"
.end
