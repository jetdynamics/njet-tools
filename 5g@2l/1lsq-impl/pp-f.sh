#!/usr/bin/env bash

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    echo $h
    cd $d

    i=`cat fs.fcount`
    for n in `seq 1 $i`; do
        m=$((n - 1))
        perl -pi -e "s|f\[$n\]|f[$m]|g" fs.cpp
        for j in {1..12}; do
            perl -pi -e "s|f\[$n\]|f[$m]|g" e${j}_odd.cpp
            perl -pi -e "s|f\[$n\]|f[$m]|g" e${j}_even.cpp
        done
    done

    cd ..
done
