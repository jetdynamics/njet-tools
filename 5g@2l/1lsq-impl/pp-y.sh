#!/usr/bin/env bash

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    cd $d

    i=`cat ys.ycount`
    for n in `seq 1 $i`; do
        m=$((n - 1))
        perl -pi -e "s|y\[$n\]|y[$m]|g" fs.cpp
    done

    cd ..
done
