(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1lsq-impl"];


(* ::Section::Closed:: *)
(*Test data file layout*)


data=Get["new_exprs/P1_epsexp_m++m+_lr_bcfw_4_5.m"];


Dimensions[data]
Dimensions[data[[1]]]
Dimensions[data[[2]]]
Dimensions[data[[3]]]


epss[o_,p_]:=inveps[3-#,If[p==1,"e","o"],o]->data[[1,o,p,6-#]]&/@Range[5];
epsPe[o_]:=epss[o,1];
epsPo[o_]:=epss[o,2];


epsE=epsPe/@Range[12];


epsO=epsPo/@Range[12];


Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];


es=Transpose[{epsE,epsO}];


Export["e"<>ToString[#]<>".m",es[[#]]]&/@Range[12];


epsE[[1,5]]


data[[2,1]]


data[[3,69]]


(* ::Section::Closed:: *)
(*MuR2 expansion*)


gen[a_]:=Plus@@(a[#]*eps^#&/@Range[-2,2]);


gen[a]


gen[A]*gen[B]//Series[#,{eps,0,0}]&


gen2[a_]:=Plus@@(a[#]*eps^(2-#)&/@Range[0,4]);


gen2[a]


gen2[this]*gen2[other]//Series[#,{eps,0,0}]&


epsexp=(MuR2^eps)


epsexp//Series[#,{eps,0,0}]&


epsexp*gen[v]//Series[#,{eps,0,2}]&


(* ::Section::Closed:: *)
(*unique pentas*)


pd={
Get["new_exprs/P1_epsexp_+++++_lr.m"][[1]],
Get["new_exprs/P1_epsexp_++++m_lr_bcfw_1_2.m"][[1]],
Get["new_exprs/P1_epsexp_m++++_lr_bcfw_1_2.m"][[1]],
Get["new_exprs/P1_epsexp_+m+++_lr_bcfw_2_3.m"][[1]],
Get["new_exprs/P1_epsexp_++m++_lr_bcfw_3_2.m"][[1]],
Get["new_exprs/P1_epsexp_+++m+_lr_bcfw_4_2.m"][[1]],
Get["new_exprs/P1_epsexp_m+++m_lr_bcfw_1_2.m"][[1]],
Get["new_exprs/P1_epsexp_++m+m_lr_bcfw_3_2.m"][[1]],
Get["new_exprs/P1_epsexp_m+m++_lr_bcfw_3_2.m"][[1]],
Get["new_exprs/P1_epsexp_+++mm_lr_bcfw_4_2.m"][[1]],
Get["new_exprs/P1_epsexp_++mm+_lr_bcfw_4_5.m"][[1]],
Get["new_exprs/P1_epsexp_+m+m+_lr_bcfw_4_5.m"][[1]],
Get["new_exprs/P1_epsexp_+mm++_lr_bcfw_4_5.m"][[1]],
Get["new_exprs/P1_epsexp_m++m+_lr_bcfw_4_5.m"][[1]],
Get["new_exprs/P1_epsexp_mm+++_lr_bcfw_4_5.m"][[1]],
Get["new_exprs/P1_epsexp_+m++m_lr_bcfw_5_4.m"][[1]]};


Table[Select[Variables[pd[[i]]],MatchQ[#,_F]&],{i,Length[pd]}];
pentas1lsq=DeleteDuplicates[Flatten[%]]


pentas1lsq//Length


Export["Fs.m",pentas1lsq];


Complement[pentas1lsq,pentas2l]


Intersection[pentas1lsq,pentas2l]//Length


(* ::Section:: *)
(*Make concise?*)


ap=Get["new_exprs/P1_epsexp_mm+++_lr_bcfw_4_5.m"];


ap=Get["new_exprs/P1_epsexp_+++++_lr.m"];


es=ap[[1]];
es//Dimensions


Export["test-es.m",es];


Export["test-coeffs.m",ap[[2;;3]]];


fs=ap[[2]];
fl=fs//Length


Export["test-fs.m",fs];


ys=ap[[3]];
yl=ys//Length


Export["test-ys.m",ys];


xs=ex/@Range[5]


p={ysp[[5]],ysp[[7]]}
f=ysp[[8]]
{q,r}=PolynomialReduce[f,p,xs]
f==q.p+r//Simplify


ysp=ys/.Rule[a_,b_]->b;


GroebnerBasis[ysp,xs]


ysp[[;;5]]
GroebnerBasis[%,xs]


ysp[[;;6]]
GroebnerBasis[%,xs]


CoefficientRules


CoefficientRules[x^2+y^2+2*x*y,{x,y,z}]


CoefficientRules[x^2+y^2+x*y,{x,y,z}]
cfs = %[[;;,2]];
mon = Times@@({x,y,z}^#)&/@%%[[;;,1]]
cfs.mon


RowReduce
