#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f66;
S f64;
S f61;
S f69;
S f74;
S f76;
S f71;
S f79;
S f88;
S f82;
S f15;
S f85;
S f13;
S f97;
S f93;
S f90;
S f49;
S f162;
S f164;
S f102;
S f53;
S f103;
S f100;
S f51;
S f104;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (-36*f69 + 11*f71)/27 + 
    ((-2*f61 - f103)*fvu3u1)/3 + ((2*f61 + f103)*fvu3u2)/6 + 
    (2*f103*fvu3u3)/9 - (f61*fvu3u4)/6 + ((-f61 + 11*f103)*fvu3u5)/
     18 + ((-2*f61 - f103)*fvu3u6)/9 + ((2*f103 + f104)*fvu3u7)/6 + 
    (16*f103*fvu3u8)/9 - (f103*fvu3u10)/6 + 
    (2*(f61 + f103)*fvu3u11)/9 + ((-f61 + 4*f103)*fvu3u12)/18 + 
    ((3*f61 - f103)*fvu3u13)/18 + (f103*fvu3u14)/3 + 
    (f61*fvu3u15)/6 + ((-f61 + f97 + f103)*fvu3u17)/6 + 
    ((-f61 + f103)*fvu3u18)/6 + ((-2*f61 - f103)*fvu3u19)/9 - 
    (2*f103*fvu3u24)/9 - (f103*fvu3u31)/9 - (f61*fvu3u32)/18 + 
    (2*f61*fvu3u36)/9 + ((3*f61 - 3*f93 - f103)*fvu3u37)/18 + 
    ((f61 - f103)*fvu3u38)/6 - (f61*fvu3u40)/6 + 
    (f103*fvu3u42)/3 + ((-4*f61 + 9*f103)*fvu1u1u1^3)/18 + 
    ((-15*f61 + 11*f103)*fvu1u1u2^3)/27 + 
    ((-6*f61 + f103)*fvu1u1u3^3)/18 + ((f61 - 2*f103)*fvu1u1u5^3)/
     9 + (f61*fvu1u1u6^3)/9 + ((-f61 + f103)*fvu1u1u7^3)/9 + 
    (4*f103*fvu1u1u9^3)/27 + fvu1u1u8^2*
     ((-6*f49 + f51 - 3*f64 + 6*f103 + 6*f104 - 6*f162 + f164)/
       18 + (f103*fvu1u1u9)/9) + fvu1u1u4^2*
     (((-3*f61 + f103)*fvu1u1u5)/18 + ((-f61 + 4*f103)*fvu1u1u8)/
       18 + ((-f61 + 7*f103)*fvu1u1u9)/18) + 
    ((-6*f53 + 6*f61 + f100 + f102)*fvu2u1u4)/9 + 
    ((-6*f13 + f15 + 6*f53 - 6*f61 - 6*f93 - f102)*fvu2u1u7)/
     9 + ((-6*f49 + f51 + 6*f103 + 6*f104 - 6*f162 + f164)*
      fvu2u1u12)/9 + (2*(f61 - 3*f103)*fvu1u2u6*tci11^2)/9 + 
    ((-2*f61 - f103)*fvu1u2u7*tci11^2)/3 - (f66*tci12)/9 + 
    ((6*f49 - f51 - 6*f103 - 6*f104 + 6*f162 - f164)*fvu1u2u9*
      tci12)/9 + (4*f61*fvu2u1u1*tci12)/3 + 
    (4*(f61 - f103)*fvu2u1u2*tci12)/9 + 
    (2*(f61 - 3*f103)*fvu2u1u3*tci12)/9 + 
    ((2*f61 + f103)*fvu2u1u5*tci12)/3 + 
    (4*(f61 - 4*f103)*fvu2u1u9*tci12)/9 + 
    fvu1u1u6^2*((f61*fvu1u1u7)/9 - (f61*fvu1u1u8)/18 + 
      (f61*tci12)/18) + fvu1u1u1^2*(-(f103*fvu1u1u2)/9 - 
      (f103*fvu1u1u3)/6 + ((f61 - f103)*fvu1u1u4)/9 - 
      (f103*fvu1u1u7)/3 + ((2*f61 + f103)*fvu1u1u8)/18 - 
      (f103*fvu1u1u9)/18 + ((8*f61 - 23*f103)*tci12)/18) + 
    fvu1u1u9^2*((f64 + f69 - f74 + f79 + f85)/6 + 
      ((f61 - 10*f103)*tci12)/18) + 
    fvu1u1u2^2*((-6*f49 + f51 - 6*f53 + 6*f61 + 3*f74 + 
        2*f100 + f102 + 6*f104)/18 + (f61*fvu1u1u3)/6 + 
      ((f61 + f103)*fvu1u1u4)/9 - (f103*fvu1u1u5)/9 + 
      (f61*fvu1u1u7)/6 + (f103*fvu1u1u8)/9 + 
      ((f61 + 2*f103)*fvu1u1u9)/9 + ((-f61 - 8*f103)*tci12)/
       18) + fvu1u1u3^2*(-(f61*fvu1u1u4)/6 + 
      ((f61 + f103)*fvu1u1u7)/6 + ((-f61 + f103)*fvu1u1u8)/6 + 
      ((2*f61 - 3*f103)*tci12)/6) + 
    fvu1u1u7^2*((6*f13 - f15 - 3*f79 + 6*f93 - 6*f103 + 
        6*f162 - f164)/18 + ((2*f61 + f103)*fvu1u1u8)/18 - 
      (f103*fvu1u1u9)/6 + ((-f61 - 2*f103)*tci12)/18) + 
    fvu2u1u10*((6*f13 - f15 + 6*f93 - 6*f103 + 6*f162 - f164)/
       9 + (2*(f61 - f103)*tci12)/9) + 
    fvu1u1u5^2*((-6*f13 + f15 + 6*f53 - 6*f61 - 3*f85 - 6*f93 - 
        f102)/18 - (f61*fvu1u1u6)/6 + (f103*fvu1u1u7)/18 + 
      ((-3*f61 + f103)*fvu1u1u8)/18 + ((-f61 + f103)*fvu1u1u9)/
       6 + ((3*f61 - f103)*tci12)/9) + 
    fvu2u1u11*((-6*f49 + f51 + f100 + 6*f104)/9 - 
      (4*f103*tci12)/9) + tci11^2*
     ((-12*f49 + 2*f51 + 12*f53 - 12*f61 - 18*f64 + 3*f69 + 
        2*f100 - 2*f102 + 48*f103 + 12*f104 - 48*f162 + 8*f164)/
       108 + ((10*f61 - 3*f93 + 7*f103 + 3*f104)*tci12)/108) + 
    fvu1u2u8*((2*(f61 - f103)*tci11^2)/9 + 
      ((6*f13 - f15 + 6*f93 - 6*f103 + 6*f162 - f164)*tci12)/
       9) + fvu1u1u9*((-f66 - f71 + f76 - f82 - f88)/9 - 
      (2*(f61 - 7*f103)*fvu2u1u11)/9 - 
      (2*(f61 - 6*f103)*tci11^2)/27 + 
      ((6*f103 - 6*f162 + f164)*tci12)/9) + 
    fvu1u1u6*(-(f61*fvu1u1u8^2)/18 + (2*f61*fvu2u1u8)/9 - 
      (2*f61*tci11^2)/27 + (f61*fvu1u1u8*tci12)/9 + 
      fvu1u1u7*((-2*f61*fvu1u1u8)/9 + (2*f61*tci12)/9)) + 
    fvu1u1u3*(-(f61*fvu1u1u4^2)/6 + ((2*f61 + f103)*fvu1u1u5^2)/
       6 + (f61*fvu1u1u7^2)/6 + ((-f61 + f103)*fvu1u1u8^2)/6 - 
      (2*f61*fvu2u1u1)/3 + ((-2*f61 - f103)*fvu2u1u5)/3 - 
      (2*(f61 - 3*f103)*tci11^2)/9 + 
      ((-2*f61 - f103)*fvu1u1u5*tci12)/3 + 
      ((-f61 - f103)*fvu1u1u7*tci12)/3 + 
      ((f61 - f103)*fvu1u1u8*tci12)/3 + 
      ((2*f61 + f103)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((f61 - f103)*fvu1u1u8)/3 + (f61*tci12)/3)) + 
    fvu1u1u4*(((-3*f61 + f103)*fvu1u1u5^2)/18 + 
      ((-f61 + 4*f103)*fvu1u1u8^2)/18 + 
      ((-f61 + 7*f103)*fvu1u1u9^2)/18 - (2*(f61 - f103)*fvu2u1u2)/
       9 - (2*(f61 - 3*f103)*fvu2u1u3)/9 + (4*f103*fvu2u1u11)/9 - 
      (f103*tci11^2)/27 + ((f61 - 7*f103)*fvu1u1u9*tci12)/9 - 
      (2*(f61 - 3*f103)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*((2*f103*fvu1u1u9)/9 + ((f61 - 4*f103)*tci12)/
         9) + fvu1u1u5*(((f61 - f103)*fvu1u1u9)/3 + 
        ((3*f61 - f103)*tci12)/9)) + 
    fvu1u1u7*(f82/9 + ((4*f61 - f103)*fvu1u1u8^2)/18 - 
      (f103*fvu1u1u9^2)/6 + (2*(3*f61 - f103)*fvu2u1u7)/9 + 
      (2*f61*fvu2u1u8)/9 - (2*(f61 - f103)*fvu2u1u10)/9 + 
      ((2*f61 - f103)*tci11^2)/9 + ((6*f103 - 6*f162 + f164)*
        tci12)/9 - (2*(f61 - f103)*fvu1u2u8*tci12)/9 + 
      fvu1u1u8*((-6*f13 + f15 - 6*f93)/9 + 
        ((-2*f61 - f103)*tci12)/9) + 
      fvu1u1u9*((6*f103 - 6*f162 + f164)/9 - (f103*tci12)/3)) + 
    fvu1u1u8*(f66/9 + (f103*fvu1u1u9^2)/9 - 
      (2*(f61 - 4*f103)*fvu2u1u9)/9 - (2*(f61 - f103)*fvu2u1u10)/
       9 + (4*f103*fvu2u1u11)/9 + (f103*tci11^2)/18 + 
      (f64*tci12)/3 - (2*(f61 - f103)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*((-6*f103 + 6*f162 - f164)/9 - (2*f103*tci12)/
         9)) + fvu1u1u5*(f88/9 - (f61*fvu1u1u6^2)/6 + 
      ((-2*f61 + f103)*fvu1u1u7^2)/6 + 
      ((-3*f61 + f103)*fvu1u1u8^2)/18 + 
      ((-f61 + f103)*fvu1u1u9^2)/6 - (2*(3*f61 - f103)*fvu2u1u4)/
       9 + ((-2*f61 - f103)*fvu2u1u5)/3 + 
      (2*(3*f61 - f103)*fvu2u1u7)/9 + 
      ((-54*f61 + 13*f103)*tci11^2)/108 + 
      ((-6*f13 + f15 - 6*f93)*tci12)/9 + 
      ((2*f61 + f103)*fvu1u2u7*tci12)/3 + 
      fvu1u1u6*((f61*fvu1u1u8)/3 - (f61*tci12)/3) + 
      fvu1u1u8*((6*f13 - f15 + 6*f93)/9 + 
        ((3*f61 - f103)*tci12)/9) + fvu1u1u7*
       ((-6*f53 + 6*f61 + f102)/9 - (f103*fvu1u1u8)/9 + 
        (f103*tci12)/9) + fvu1u1u9*(f100/9 + 
        ((-f61 + f103)*tci12)/3)) + 
    fvu1u1u2*(-f76/9 + (f61*fvu1u1u3^2)/6 + 
      (2*(f61 - 2*f103)*fvu1u1u4^2)/9 + 
      ((3*f61 - 2*f103)*fvu1u1u5^2)/9 + (f61*fvu1u1u7^2)/6 - 
      (f103*fvu1u1u8^2)/9 + ((2*f61 - 5*f103)*fvu1u1u9^2)/9 - 
      (2*(f61 - 3*f103)*fvu2u1u3)/9 - (2*(3*f61 - f103)*fvu2u1u4)/
       9 - (2*(f61 - 5*f103)*fvu2u1u11)/9 + 
      (2*(3*f61 - f103)*tci11^2)/27 + 
      ((-6*f49 + f51 + 6*f104)*tci12)/9 - 
      (2*(f61 - 3*f103)*fvu1u2u6*tci12)/9 + 
      fvu1u1u3*(-(f61*fvu1u1u7)/3 - (f61*tci12)/3) + 
      fvu1u1u7*((6*f53 - 6*f61 - f102)/9 + (f61*tci12)/3) + 
      fvu1u1u4*((2*f103*fvu1u1u5)/9 - (2*f103*fvu1u1u8)/9 - 
        (2*(f61 + 2*f103)*fvu1u1u9)/9 - (2*(f61 - f103)*tci12)/
         9) + fvu1u1u5*(-f100/9 - (2*f103*tci12)/9) + 
      fvu1u1u8*((6*f49 - f51 - 6*f104)/9 - (2*f103*fvu1u1u9)/9 + 
        (2*f103*tci12)/9) + fvu1u1u9*(-f100/9 + 
        (2*(f61 + 2*f103)*tci12)/9)) + 
    fvu1u1u1*((-4*f103*fvu1u1u2^2)/9 + 
      ((2*f61 - f103)*fvu1u1u3^2)/6 + (2*(f61 - f103)*fvu1u1u4^2)/
       9 + ((-2*f61 - f103)*fvu1u1u5^2)/6 - (f103*fvu1u1u7^2)/6 + 
      ((4*f61 - 7*f103)*fvu1u1u8^2)/18 + (f103*fvu1u1u9^2)/18 - 
      (2*f61*fvu2u1u1)/3 - (2*(f61 - f103)*fvu2u1u2)/9 - 
      (2*(f61 - 4*f103)*fvu2u1u9)/9 - (4*f103*fvu2u1u11)/9 + 
      (2*(4*f61 + 5*f103)*tci11^2)/27 + 
      (5*f103*fvu1u1u9*tci12)/9 + fvu1u1u4*
       (((-2*f61 - f103)*fvu1u1u8)/9 - (2*f103*fvu1u1u9)/9 - 
        (4*(f61 - f103)*tci12)/9) + fvu1u1u2*
       ((2*f103*fvu1u1u4)/9 + (2*f103*fvu1u1u8)/9 + 
        (2*f103*fvu1u1u9)/9 - (2*f103*tci12)/9) + 
      fvu1u1u7*((f103*fvu1u1u9)/3 + (2*f103*tci12)/3) + 
      fvu1u1u3*((f103*fvu1u1u4)/3 + ((-2*f61 + f103)*tci12)/
         3) + fvu1u1u8*((-2*f103*fvu1u1u9)/9 + 
        ((-4*f61 + 7*f103)*tci12)/9)) + 
    ((-8*f61 + 29*f103)*tci12*tcr11^2)/18 + 
    ((4*f61 - 9*f103)*tcr11^3)/18 + 
    ((4*f61 + 3*f93 + 16*f103 - 3*f104)*tci11^2*tcr12)/18 - 
    (2*(10*f61 - 13*f103)*tci12*tcr21)/9 + 
    tcr11*((-4*(2*f61 + f103)*tci11^2)/27 - 
      (2*f103*tci12*tcr12)/3 + (10*(f61 - f103)*tcr21)/9) + 
    ((12*f61 + 9*f93 - 12*f97 - 26*f103 - 9*f104)*tcr33)/36;
L ieu1ueu11 = f71/9 + ((f61 + 2*f97 + f104)*fvu1u1u2^2)/
     6 + ((-f61 - f93)*fvu1u1u5^2)/6 + ((f93 - f103)*fvu1u1u7^2)/
     6 + ((f103 + f104)*fvu1u1u8^2)/6 + 
    fvu1u1u8*(f64/3 - (f103*fvu1u1u9)/3) + 
    ((f61 + f97)*fvu2u1u4)/3 + ((-f61 - f93)*fvu2u1u7)/3 + 
    ((f93 - f103)*fvu2u1u10)/3 + ((f97 + f104)*fvu2u1u11)/3 + 
    ((f103 + f104)*fvu2u1u12)/3 + ((-f61 + f97 + 4*f103 + f104)*
      tci11^2)/18 - (f64*tci12)/3 + 
    ((f93 - f103)*fvu1u2u8*tci12)/3 + 
    ((-f103 - f104)*fvu1u2u9*tci12)/3 + 
    fvu1u1u5*(f85/3 + (f61*fvu1u1u7)/3 + (f93*fvu1u1u8)/3 + 
      (f97*fvu1u1u9)/3 - (f93*tci12)/3) + 
    fvu1u1u9*((-f64 - f69 + f74 - f79 - f85)/3 + 
      (f103*tci12)/3) + fvu1u1u7*(f79/3 - (f93*fvu1u1u8)/3 + 
      (f103*fvu1u1u9)/3 + (f103*tci12)/3) + 
    fvu1u1u2*(-f74/3 - (f97*fvu1u1u5)/3 - (f61*fvu1u1u7)/3 - 
      (f104*fvu1u1u8)/3 - (f97*fvu1u1u9)/3 + (f104*tci12)/3);
L ieu0ueu11 = f69/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = -2*f90*fvu4u28 - 2*f90*fvu4u39 - 
    (2*f90*fvu4u51)/3 - 2*f90*fvu4u80 - 2*f90*fvu4u83 - 
    2*f90*fvu4u91 - 2*f90*fvu4u93 + (2*f90*fvu4u100)/3 - 
    (2*f90*fvu4u102)/3 + 2*f90*fvu4u139 - (2*f90*fvu4u148)/3 - 
    (3*f90*fvu4u171)/4 + (11*f90*fvu4u174)/4 + (11*f90*fvu4u182)/4 - 
    (3*f90*fvu4u184)/4 + (7*f90*fvu4u192)/6 - (f90*fvu4u199)/4 - 
    (f90*fvu4u201)/2 - f90*fvu4u202 - 4*f90*fvu4u213 + 
    2*f90*fvu4u215 - 2*f90*fvu4u221 + (2*f90*fvu4u225)/3 - 
    f90*fvu4u244 + (3*f90*fvu4u252)/2 - (f90*fvu4u255)/6 + 
    (11*f90*fvu4u271)/4 + (9*f90*fvu4u273)/4 - (f90*fvu4u277)/4 + 
    (3*f90*fvu4u279)/4 - (2*f90*fvu4u282)/3 + (f90*fvu4u289)/4 + 
    (f90*fvu4u290)/2 + f90*fvu4u291 + 2*f90*fvu4u295 + 
    2*f90*fvu4u325 - 2*f90*fvu4u330 + 
    fvu3u25*(f90*fvu1u1u1 - f90*fvu1u1u3 + f90*fvu1u1u6 - 
      f90*fvu1u1u7) + fvu3u71*(f90*fvu1u1u3 - f90*fvu1u1u6 + 
      f90*fvu1u1u7) + fvu3u81*(f90*fvu1u1u2 + f90*fvu1u1u5 + 
      f90*fvu1u1u8) + fvu3u45*(-(f90*fvu1u1u2) - f90*fvu1u1u3 + 
      f90*fvu1u1u9) + fvu3u43*(-(f90*fvu1u1u2) - f90*fvu1u1u6 + 
      f90*fvu1u1u7 + f90*fvu1u1u9) + 
    fvu3u82*(-(f90*fvu1u1u5) - f90*fvu1u1u6 + f90*fvu1u1u7 - 
      f90*fvu1u1u8 + f90*fvu1u1u9) + 
    fvu3u63*(-(f90*fvu1u1u6) - f90*fvu1u1u7 - f90*fvu1u1u9 + 
      2*f90*fvu1u1u10) - (151*f90*tci11^3*tci12)/135 + 
    fvu3u78*(-(f90*fvu1u1u2) + 2*f90*fvu1u1u3 - f90*fvu1u1u5 + 
      f90*fvu1u1u8 - 2*f90*tci12) + 
    fvu3u80*(f90*fvu1u1u3 - f90*fvu1u1u5 + (f90*fvu1u1u6)/2 - 
      (f90*fvu1u1u7)/2 + (f90*fvu1u1u8)/2 - (3*f90*fvu1u1u9)/2 - 
      (3*f90*tci12)/2) - (2*f90*tci11^2*tci21)/9 + 
    fvu1u1u1*(f90*fvu3u45 - f90*fvu3u71 - f90*fvu3u78 - 
      f90*fvu3u81 + f90*fvu3u82 + (8*f90*tci11^3)/27 - 
      8*f90*tci12*tci21) + fvu1u1u3*((-4*f90*tci11^3)/27 + 
      4*f90*tci12*tci21) + fvu1u1u5*((-4*f90*tci11^3)/27 + 
      4*f90*tci12*tci21) + fvu1u1u8*((-5*f90*tci11^3)/27 + 
      5*f90*tci12*tci21) + tci12*((-48*f90*tci31)/5 - 
      16*f90*tci32) - (4*f90*tci41)/3 + 
    ((-4*f90*tci11^3)/9 + 8*f90*tci12*tci21)*tcr11 + 
    (f90*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u10*((11*f90*tci11^3)/135 + (4*f90*tci12*tci21)/
       3 + (48*f90*tci31)/5 + 4*f90*tci21*tcr11 - 
      (f90*tci11*tcr11^2)/5) + 
    fvu1u1u2*(f90*fvu3u63 + f90*fvu3u80 + (31*f90*tci11^3)/
       270 - (4*f90*tci12*tci21)/3 + (24*f90*tci31)/5 + 
      2*f90*tci21*tcr11 - (f90*tci11*tcr11^2)/10) + 
    fvu1u1u6*(-(f90*tci11^3)/270 - (5*f90*tci12*tci21)/3 - 
      (24*f90*tci31)/5 - 2*f90*tci21*tcr11 + 
      (f90*tci11*tcr11^2)/10) + 
    fvu1u1u7*((-7*f90*tci11^3)/90 + (f90*tci12*tci21)/3 - 
      (24*f90*tci31)/5 - 2*f90*tci21*tcr11 + 
      (f90*tci11*tcr11^2)/10) + 
    fvu1u1u9*((-7*f90*tci11^3)/90 + (f90*tci12*tci21)/3 - 
      (24*f90*tci31)/5 - 2*f90*tci21*tcr11 + 
      (f90*tci11*tcr11^2)/10) + 
    (40*f90*tci12*tci21*tcr12)/3 + 4*f90*tci11*tci12*
     tcr12^2;
L ieu1uou11 = 2*f90*fvu3u81 - 
    (4*f90*tci11^3)/27 + 4*f90*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
