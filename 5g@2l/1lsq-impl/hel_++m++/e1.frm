#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f136;
S f66;
S f65;
S f134;
S f64;
S f133;
S f63;
S f62;
S f131;
S f130;
S f60;
S f69;
S f138;
S f124;
S f74;
S f125;
S f75;
S f126;
S f76;
S f77;
S f127;
S f120;
S f70;
S f71;
S f121;
S f72;
S f123;
S f73;
S f128;
S f129;
S f151;
S f150;
S f89;
S f88;
S f18;
S f19;
S f16;
S f3;
S f2;
S f80;
S f83;
S f85;
S f7;
S f6;
S f86;
S f94;
S f178;
S f95;
S f91;
S f22;
S f21;
S f177;
S f27;
S f176;
S f25;
S f175;
S f98;
S f24;
S f160;
S f30;
S f161;
S f32;
S f33;
S f35;
S f108;
S f58;
S f59;
S f52;
S f101;
S f56;
S f107;
S f57;
S f54;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u132;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu4u113;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu1 = (-36*f57 - 11*f59)/27 - (f80*fvu3u1)/3 + 
    ((f80 - f86)*fvu3u2)/6 + (f80*fvu3u3)/9 + 
    ((3*f52 - 2*f80)*fvu3u4)/18 + (f80*fvu3u5)/18 - 
    (2*f80*fvu3u6)/9 - (2*f80*fvu3u11)/9 - (f80*fvu3u12)/18 + 
    ((-2*f80 - 3*f91)*fvu3u13)/18 - (f80*fvu3u14)/9 + 
    ((-f80 + f95)*fvu3u15)/6 + (f80*fvu3u17)/6 - (f80*fvu3u18)/6 - 
    (f80*fvu3u19)/9 + (f80*fvu3u22)/3 - (7*f80*fvu1u1u1^3)/18 + 
    (f80*fvu1u1u2^3)/3 + (f80*fvu1u1u3^3)/18 - 
    (f80*fvu1u1u5^3)/9 + fvu1u1u4^2*(-f67/6 + (f80*fvu1u1u5)/9 - 
      (f80*fvu1u1u8)/18 + (f80*fvu1u1u9)/18) + 
    ((f83 + f101)*fvu2u1u2)/9 + ((f83 + f94)*fvu2u1u3)/9 + 
    ((f94 - f98)*fvu2u1u4)/9 + fvu1u1u8*((-2*f80*fvu2u1u9)/9 - 
      (f80*tci11^2)/54) + fvu1u1u9*((2*f80*fvu2u1u11)/9 + 
      (2*f80*tci11^2)/27) + 
    ((8*f2 - f3 - 8*f6 + f7 - 2*f16 - f18 - 8*f19 + f21 - 
       8*f24 + f27 - 8*f30 + f32 + 8*f33 - f35 + 16*f54 - 
       2*f56 + 11*f57 + 10*f58 + f59 - f60 - 8*f62 + 16*f63 - 
       8*f64 - 2*f65 + f66 + f69 - f70 - 8*f72 - 8*f73 + 
       8*f74 + f75 - f76 + 8*f85 - f88 + 8*f107 - f108 - 
       8*f120 + f121 + 3*f123 + 8*f124 - f125 - 8*f126 + f127 + 
       8*f128 - 8*f129 - f130 + f131 + 8*f133 - f134 + 8*f136 - 
       f138 - 8*f150 + f151 + 24*f160 - 3*f161 - 16*f175 + 
       2*f176 + 8*f177 - f178)*tci12)/9 - 
    (f80*fvu1u1u9^2*tci12)/18 + 
    ((f83 + f94)*fvu1u2u6*tci12)/9 + (4*f80*fvu2u1u9*tci12)/
     9 + fvu1u1u3^2*((-3*f62 + f89 - f98)/18 - (f80*fvu1u1u4)/9 - 
      (f80*fvu1u1u8)/6 - (f80*tci12)/2) + 
    fvu1u1u5^2*((-f54 + f57 + f62 + f67 + f72)/6 + 
      (f80*fvu1u1u9)/6 - (f80*tci12)/9) + 
    fvu1u1u2^2*((3*f54 + f83 + 2*f94 - f98)/18 - 
      (f80*fvu1u1u4)/6 - (f80*fvu1u1u5)/18 - (f80*fvu1u1u9)/9 + 
      (f80*tci12)/6) + fvu2u1u5*((f89 - f98)/9 + 
      (f80*tci12)/3) + fvu1u1u1^2*
     ((-3*f72 + f83 - f89 + 2*f101)/18 - (f80*fvu1u1u2)/6 + 
      (f80*fvu1u1u3)/18 + (f80*fvu1u1u4)/6 + (f80*fvu1u1u8)/9 + 
      (5*f80*tci12)/6) + fvu2u1u1*((-f89 + f101)/9 + 
      (8*f80*tci12)/9) + tci11^2*
     ((3*f57 - 18*f62 - 18*f67 - 18*f72 + 2*f83 - 2*f98 + 
        2*f101)/108 + ((2*f52 + 4*f80 - 3*f86 - f91 - f95)*
        tci12)/36) + fvu1u2u7*(-(f80*tci11^2)/3 + 
      ((-f89 + f98)*tci12)/9) + 
    fvu1u1u5*((8*f2 - f3 - 8*f6 + f7 - 2*f16 - f18 - 8*f19 + 
        f21 - 8*f24 + f27 - 8*f30 + f32 + 8*f33 - f35 + 
        16*f54 - f56 + 11*f57 + 10*f58 + 2*f59 - f60 - 8*f62 + 
        16*f63 - 8*f64 - 2*f65 + f66 + f69 - f70 - 8*f72 - 
        8*f73 + 8*f74 + f75 - f76 + 8*f85 - f88 + 8*f107 - 
        f108 - 8*f120 + f121 + 3*f123 + 8*f124 - f125 - 
        8*f126 + f127 + 8*f128 - 8*f129 - f130 + f131 + 
        8*f133 - f134 + 8*f136 - f138 - 8*f150 + f151 + 
        24*f160 - 3*f161 - 16*f175 + 2*f176 + 8*f177 - f178)/9 + 
      (f80*fvu1u1u9^2)/6 + (4*f80*fvu2u1u4)/9 - 
      (f80*fvu2u1u5)/3 + (13*f80*tci11^2)/108 - 
      (f94*tci12)/9 + (f80*fvu1u1u9*tci12)/3 + 
      (f80*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*(f70/9 + (f80*fvu1u1u5^2)/9 - (f80*fvu1u1u8^2)/18 + 
      (f80*fvu1u1u9^2)/18 + (f67*tci12)/3 + 
      (f80*fvu1u1u8*tci12)/9 - (f80*fvu1u1u9*tci12)/9 + 
      fvu1u1u5*(f94/9 - (f80*fvu1u1u9)/3 - (2*f80*tci12)/9)) + 
    fvu1u1u3*((8*f6 - f7 + 3*f18 - 8*f19 + f21 - 8*f22 + 
        8*f24 + f25 - f27 + 8*f30 - f32 + 8*f33 - f35 - 
        24*f54 + 3*f56 - 19*f58 + 2*f60 + 16*f62 + 19*f69 - 
        2*f71 + 8*f73 - 16*f74 - f75 + 2*f76 + 16*f120 - 
        2*f121 - 8*f124 + f125 + 16*f126 - 2*f127 - 8*f133 + 
        f134 - 16*f160 + 2*f161 + 3*f177)/18 - 
      (f80*fvu1u1u4^2)/9 + (f80*fvu1u1u5^2)/6 - 
      (f80*fvu1u1u8^2)/6 - (4*f80*fvu2u1u1)/9 - 
      (f80*fvu2u1u5)/3 + (17*f80*tci11^2)/27 + 
      (f62*tci12)/3 + (f80*fvu1u1u8*tci12)/3 + 
      (f80*fvu1u2u7*tci12)/3 + fvu1u1u5*
       (-f89/9 - (f80*tci12)/3) + fvu1u1u4*
       (f101/9 + (f80*fvu1u1u8)/3 + (2*f80*tci12)/9)) + 
    fvu1u1u1*((-16*f2 + 2*f3 + 8*f6 - f7 + 4*f16 - f18 + 
        24*f19 - 3*f21 + 8*f22 + 8*f24 - f25 - f27 + 8*f30 - 
        f32 - 24*f33 + 3*f35 - 8*f54 + f56 - 22*f57 - f58 - 
        2*f59 - 32*f63 + 16*f64 + 4*f65 - 2*f66 - 21*f69 + 
        2*f71 + 16*f72 + 8*f73 - f75 - 16*f85 + 2*f88 - 
        16*f107 + 2*f108 - 6*f123 - 8*f124 + f125 - 16*f128 + 
        16*f129 + 2*f130 - 2*f131 - 8*f133 + f134 - 16*f136 + 
        2*f138 + 16*f150 - 2*f151 - 32*f160 + 4*f161 + 32*f175 - 
        4*f176 - 19*f177 + 2*f178)/18 + (5*f80*fvu1u1u3^2)/18 + 
      (f80*fvu1u1u4^2)/6 + (f89*fvu1u1u5)/9 - 
      (f80*fvu1u1u5^2)/6 + (2*f80*fvu1u1u8^2)/9 - 
      (4*f80*fvu2u1u1)/9 - (2*f80*fvu2u1u9)/9 - 
      (f80*tci11^2)/3 + ((3*f72 - f83 + f89)*tci12)/9 - 
      (4*f80*fvu1u1u8*tci12)/9 + fvu1u1u3*
       (-f101/9 - (f80*fvu1u1u4)/9 - (5*f80*tci12)/9) + 
      fvu1u1u4*(-f101/9 - (2*f80*fvu1u1u8)/9 - (f80*tci12)/3) + 
      fvu1u1u2*(-f83/9 + (f80*tci12)/3)) + 
    fvu1u1u2*(-f56/9 + (f80*fvu1u1u3^2)/6 - (f80*fvu1u1u4^2)/6 - 
      (5*f80*fvu1u1u5^2)/18 - (2*f80*fvu1u1u9^2)/9 + 
      (4*f80*fvu2u1u4)/9 + (2*f80*fvu2u1u11)/9 - 
      (f80*tci11^2)/9 - (f98*tci12)/9 - 
      (2*f80*fvu1u1u9*tci12)/9 + fvu1u1u3*
       (f98/9 - (f80*tci12)/3) + fvu1u1u5*
       (-f94/9 - (f80*tci12)/9) + fvu1u1u4*
       (-f94/9 + (f80*fvu1u1u5)/9 + (2*f80*fvu1u1u9)/9 + 
        (f80*tci12)/3)) + ((2*f52 - 2*f80 - 3*f86)*tci12*
      tcr11^2)/6 + ((-4*f52 + 6*f80 + f86)*tcr11^3)/18 + 
    ((-2*f52 + 19*f80 - 7*f86 + 6*f91 + 6*f95)*tci11^2*
      tcr12)/36 + ((2*f52 - f80 + f86)*tcr12^3)/9 + 
    (2*(f52 - f80 - f86)*tci12*tcr21)/3 + 
    tcr11*(((5*f80 + 7*f86)*tci11^2)/36 + 
      ((-f80 + f86)*tci12*tcr12)/3 - 
      (2*(f52 - f80)*tcr21)/3) + 
    ((-2*f52 + f80 - f86)*tcr31)/3 + 
    ((-2*f52 + f80 - f86)*tcr32)/12 + 
    ((74*f52 - 11*f80 + 11*f86 + 18*f91 + 18*f95)*tcr33)/72;
L ieu1ueu1 = -f59/9 + ((2*f52 + f80 - f86)*fvu1u1u1^2)/
     6 + ((f80 + 2*f91 - f95)*fvu1u1u2^2)/6 + 
    ((f86 - f95)*fvu1u1u3^2)/6 + 
    fvu1u1u3*(f62/3 + (f52*fvu1u1u4)/3 - (f86*fvu1u1u5)/3) + 
    fvu1u1u4*(f67/3 + (f91*fvu1u1u5)/3) + 
    ((f52 - f86)*fvu2u1u1)/3 + ((f52 + f80)*fvu2u1u2)/3 + 
    ((f80 + f91)*fvu2u1u3)/3 + ((f91 - f95)*fvu2u1u4)/3 + 
    ((f86 - f95)*fvu2u1u5)/3 + ((f52 + f80 - f95)*tci11^2)/
     18 + ((-f62 - f67 - f72)*tci12)/3 + 
    ((f80 + f91)*fvu1u2u6*tci12)/3 + 
    ((-f86 + f95)*fvu1u2u7*tci12)/3 + 
    fvu1u1u1*(f72/3 - (f80*fvu1u1u2)/3 - (f52*fvu1u1u3)/3 - 
      (f52*fvu1u1u4)/3 + (f86*fvu1u1u5)/3 + 
      ((-f80 + f86)*tci12)/3) + 
    fvu1u1u5*((f54 - f57 - f62 - f67 - f72)/3 - 
      (f91*tci12)/3) + fvu1u1u2*(-f54/3 + (f95*fvu1u1u3)/3 - 
      (f91*fvu1u1u4)/3 - (f91*fvu1u1u5)/3 - (f95*tci12)/3);
L ieu0ueu1 = f57/3;
L ieum1ueu1 = 0;
L ieum2ueu1 = 0;
L ieu2uou1 = (5*f77*fvu4u25)/3 - (f77*fvu4u28)/3 - 
    (7*f77*fvu4u39)/3 - (f77*fvu4u41)/3 - (f77*fvu4u49)/3 - 
    (5*f77*fvu4u51)/9 + (5*f77*fvu4u80)/12 - (37*f77*fvu4u83)/12 - 
    (7*f77*fvu4u91)/4 + (5*f77*fvu4u93)/12 + (f77*fvu4u100)/3 - 
    (23*f77*fvu4u102)/18 + (f77*fvu4u111)/4 + (f77*fvu4u113)/2 + 
    f77*fvu4u114 - (8*f77*fvu4u129)/3 + (7*f77*fvu4u132)/3 - 
    (f77*fvu4u139)/6 - (8*f77*fvu4u141)/3 + (4*f77*fvu4u146)/9 + 
    (f77*fvu4u148)/18 + (5*f77*fvu4u171)/12 - (f77*fvu4u174)/12 - 
    (f77*fvu4u182)/12 + (5*f77*fvu4u184)/12 + (f77*fvu4u190)/9 - 
    (f77*fvu4u192)/18 + (f77*fvu4u199)/4 + (f77*fvu4u201)/2 + 
    f77*fvu4u202 + (7*f77*fvu4u213)/12 - (f77*fvu4u215)/4 + 
    (9*f77*fvu4u219)/4 + (7*f77*fvu4u221)/12 - (4*f77*fvu4u225)/9 - 
    (f77*fvu4u233)/4 - (f77*fvu4u234)/2 - f77*fvu4u235 + 
    (17*f77*fvu4u244)/3 + (4*f77*fvu4u246)/3 - (7*f77*fvu4u252)/2 - 
    (17*f77*fvu4u255)/18 - (5*f77*fvu4u271)/12 - 
    (43*f77*fvu4u273)/12 + (67*f77*fvu4u277)/12 - 
    (5*f77*fvu4u279)/12 - (f77*fvu4u282)/3 - (f77*fvu4u289)/4 - 
    (f77*fvu4u290)/2 - f77*fvu4u291 + (4*f77*fvu4u293)/3 - 
    (2*f77*fvu4u295)/3 + 2*f77*fvu4u313 + 2*f77*fvu4u330 + 
    fvu3u71*(-(f77*fvu1u1u2)/3 + (f77*fvu1u1u3)/3 + 
      (f77*fvu1u1u5)/3 - (f77*fvu1u1u6)/3 + (f77*fvu1u1u7)/3 + 
      (f77*fvu1u1u8)/3) + fvu3u78*((2*f77*fvu1u1u2)/3 - 
      (2*f77*fvu1u1u3)/3 - (2*f77*fvu1u1u9)/3) + 
    fvu3u70*((f77*fvu1u1u2)/6 - (f77*fvu1u1u3)/6 - 
      (f77*fvu1u1u9)/6) + fvu3u45*(-(f77*fvu1u1u2)/2 - 
      (7*f77*fvu1u1u3)/6 - (2*f77*fvu1u1u6)/3 + 
      (2*f77*fvu1u1u7)/3 + (f77*fvu1u1u9)/2) + 
    fvu3u43*(-(f77*fvu1u1u2)/6 - (f77*fvu1u1u4)/3 - 
      (f77*fvu1u1u6)/6 + (f77*fvu1u1u7)/6 + (f77*fvu1u1u9)/2 - 
      (f77*fvu1u1u10)/3) + fvu3u63*((2*f77*fvu1u1u2)/3 + 
      (3*f77*fvu1u1u3)/2 - (f77*fvu1u1u5)/3 + (f77*fvu1u1u6)/2 - 
      (f77*fvu1u1u7)/3 - (f77*fvu1u1u8)/3 - (f77*fvu1u1u9)/3 - 
      (f77*fvu1u1u10)/6) + fvu3u25*(-(f77*fvu1u1u2)/3 - 
      (7*f77*fvu1u1u3)/6 + f77*fvu1u1u4 - (2*f77*fvu1u1u5)/3 + 
      (5*f77*fvu1u1u6)/6 - (f77*fvu1u1u7)/6 + 
      (f77*fvu1u1u10)/3) + fvu3u81*((f77*fvu1u1u3)/6 + 
      (f77*fvu1u1u4)/3 + (f77*fvu1u1u6)/6 - (f77*fvu1u1u7)/6 - 
      (f77*fvu1u1u9)/3 + (f77*fvu1u1u10)/3) + 
    fvu3u62*((-4*f77*fvu1u1u2)/3 + (2*f77*fvu1u1u4)/3 + 
      (2*f77*fvu1u1u5)/3 - (4*f77*fvu1u1u6)/3 + 
      (2*f77*fvu1u1u7)/3 + (4*f77*fvu1u1u10)/3) + 
    (13*f77*tci11^3*tci12)/15 + 
    fvu3u23*((f77*fvu1u1u1)/3 + (f77*fvu1u1u2)/6 + 
      f77*fvu1u1u3 + f77*fvu1u1u4 + f77*fvu1u1u5 - 
      (f77*fvu1u1u6)/2 - (f77*fvu1u1u7)/6 - (f77*fvu1u1u8)/3 - 
      (f77*fvu1u1u9)/6 - (f77*fvu1u1u10)/3 - 2*f77*tci12) + 
    fvu3u82*((-2*f77*fvu1u1u3)/3 - (2*f77*fvu1u1u4)/3 + 
      f77*fvu1u1u5 + f77*fvu1u1u6 - (f77*fvu1u1u7)/3 + 
      (f77*fvu1u1u8)/3 - (f77*fvu1u1u9)/3 + (4*f77*tci12)/3) + 
    fvu3u80*((2*f77*fvu1u1u2)/3 + f77*fvu1u1u3 - 
      (19*f77*fvu1u1u6)/6 + (19*f77*fvu1u1u7)/6 - 
      (17*f77*fvu1u1u8)/6 + (13*f77*fvu1u1u9)/6 + 
      (17*f77*tci12)/6) - (10*f77*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f77*tci11^3)/81 + (17*f77*tci12*tci21)/
       3) + tci12*((16*f77*tci31)/5 + 16*f77*tci32) - 
    (2072*f77*tci41)/135 - (64*f77*tci42)/5 - 
    (16*f77*tci43)/3 + ((1259*f77*tci11^3)/1620 + 
      f77*tci12*tci21 + (84*f77*tci31)/5 + 
      16*f77*tci32)*tcr11 - (11*f77*tci11*tcr11^3)/180 + 
    fvu1u1u6*((f77*tci11^3)/810 + (58*f77*tci12*tci21)/9 + 
      (88*f77*tci31)/5 + (22*f77*tci21*tcr11)/3 - 
      (11*f77*tci11*tcr11^2)/30) + 
    fvu1u1u2*((181*f77*tci11^3)/1620 + (2*f77*tci12*tci21)/
       9 + (44*f77*tci31)/5 + (11*f77*tci21*tcr11)/3 - 
      (11*f77*tci11*tcr11^2)/60) + 
    fvu1u1u3*((73*f77*tci11^3)/1620 - (f77*tci12*tci21)/3 + 
      (12*f77*tci31)/5 + f77*tci21*tcr11 - 
      (f77*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f77*tci11^3)/540 - (49*f77*tci12*tci21)/
       9 - (4*f77*tci31)/5 - (f77*tci21*tcr11)/3 + 
      (f77*tci11*tcr11^2)/60) + 
    fvu1u1u4*((-31*f77*tci11^3)/270 + (4*f77*tci12*tci21)/
       3 - (24*f77*tci31)/5 - 2*f77*tci21*tcr11 + 
      (f77*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f77*tci11^3)/180 - (f77*tci12*tci21)/
       9 - (28*f77*tci31)/5 - (7*f77*tci21*tcr11)/3 + 
      (7*f77*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f77*tci11^3)/1620 - (19*f77*tci12*tci21)/
       3 - (36*f77*tci31)/5 - 3*f77*tci21*tcr11 + 
      (3*f77*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f77*fvu3u25)/6 + (f77*fvu3u43)/3 + 
      (7*f77*fvu3u45)/6 - (2*f77*fvu3u62)/3 - (7*f77*fvu3u63)/6 + 
      (f77*fvu3u70)/6 - (2*f77*fvu3u71)/3 + (2*f77*fvu3u78)/3 - 
      f77*fvu3u80 - (f77*fvu3u81)/2 - (f77*fvu3u82)/3 - 
      (179*f77*tci11^3)/1620 + (f77*tci12*tci21)/3 - 
      (36*f77*tci31)/5 - 3*f77*tci21*tcr11 + 
      (3*f77*tci11*tcr11^2)/20) + 
    fvu1u1u5*((-8*f77*tci11^3)/135 - (28*f77*tci12*tci21)/
       9 - (64*f77*tci31)/5 - (16*f77*tci21*tcr11)/3 + 
      (4*f77*tci11*tcr11^2)/15) + 
    ((-218*f77*tci11^3)/243 - (40*f77*tci12*tci21)/3)*
     tcr12 + (-4*f77*tci11*tci12 - (40*f77*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f77*tci11*tci12)/15 + 
      5*f77*tci21 - 2*f77*tci11*tcr12) + 
    (130*f77*tci11*tcr33)/27;
L ieu1uou1 = 
   2*f77*fvu3u23 - (11*f77*tci11^3)/135 - 
    (4*f77*tci12*tci21)/3 - (48*f77*tci31)/5 - 
    4*f77*tci21*tcr11 + (f77*tci11*tcr11^2)/5;
L ieu0uou1 = 0;
L ieum1uou1 = 0;
L ieum2uou1 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou1+w^2*ieu1uou1+w^3*ieu0uou1+w^4*ieum1uou1+w^5*ieum2uou1;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou1a = K[w^1];
L ieu1uou1a = K[w^2];
L ieu0uou1a = K[w^3];
L ieum1uou1a = K[w^4];
L ieum2uou1a = K[w^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_odd.c> "%O"
#write <e1_odd.c> "return Eps5o2<T>("
#write <e1_odd.c> "%E", ieu2uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu0uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum2uou1a
#write <e1_odd.c> ");\n}"
L H=+u^1*ieu2ueu1+u^2*ieu1ueu1+u^3*ieu0ueu1+u^4*ieum1ueu1+u^5*ieum2ueu1;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu1a = H[u^1];
L ieu1ueu1a = H[u^2];
L ieu0ueu1a = H[u^3];
L ieum1ueu1a = H[u^4];
L ieum2ueu1a = H[u^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_even.c> "%O"
#write <e1_even.c> "return Eps5o2<T>("
#write <e1_even.c> "%E", ieu2ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu0ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum2ueu1a
#write <e1_even.c> ");\n}"
.end
