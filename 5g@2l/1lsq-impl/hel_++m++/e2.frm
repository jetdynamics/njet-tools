#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f66;
S f65;
S f64;
S f63;
S f131;
S f130;
S f60;
S f69;
S f124;
S f74;
S f125;
S f75;
S f126;
S f76;
S f127;
S f120;
S f70;
S f121;
S f71;
S f73;
S f128;
S f129;
S f88;
S f18;
S f3;
S f16;
S f2;
S f80;
S f83;
S f14;
S f85;
S f7;
S f12;
S f6;
S f4;
S f178;
S f28;
S f22;
S f27;
S f177;
S f176;
S f175;
S f25;
S f24;
S f38;
S f49;
S f160;
S f45;
S f161;
S f44;
S f47;
S f33;
S f35;
S f40;
S f37;
S f42;
S f108;
S f58;
S f51;
S f56;
S f107;
S f57;
S f54;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu2 = (11*f4 - 11*f16 - 63*f18 + 11*f57 + 11*f58 + 
      99*f69 - 11*f71)/27 - (f12*fvu3u1)/3 + (f12*fvu3u2)/6 + 
    (f12*fvu3u3)/9 - (f12*fvu3u4)/9 + ((f12 + 3*f42)*fvu3u5)/18 - 
    (2*f12*fvu3u6)/9 - (f49*fvu3u7)/6 + (f45*fvu3u10)/6 - 
    (2*f12*fvu3u11)/9 + ((-f12 + 3*f38)*fvu3u12)/18 - 
    (f12*fvu3u13)/9 - (f12*fvu3u14)/9 - (f12*fvu3u15)/6 + 
    (f12*fvu3u17)/6 - (f12*fvu3u18)/6 - (f12*fvu3u19)/9 + 
    (f12*fvu3u22)/3 - (7*f12*fvu1u1u1^3)/18 + (f12*fvu1u1u2^3)/3 + 
    (f12*fvu1u1u3^3)/18 - (f12*fvu1u1u5^3)/9 + 
    ((-3*f14 + f47 - f51)*fvu1u1u8^2)/18 + 
    fvu1u1u4^2*(-f33/6 + (f12*fvu1u1u5)/9 - (f12*fvu1u1u8)/18 + 
      (f12*fvu1u1u9)/18) + ((6*f12 + f40 - 6*f80 + f83)*
      fvu2u1u2)/9 + ((6*f12 + f44 - 6*f80 + f83)*fvu2u1u3)/9 + 
    ((f44 - f51)*fvu2u1u11)/9 + ((f47 - f51)*fvu2u1u12)/9 - 
    (f12*fvu1u2u7*tci11^2)/3 + 
    ((8*f2 - f3 + f4 - 8*f6 + f7 - 8*f14 + f16 - 3*f18 + 
       8*f22 - f25 - 8*f28 - f35 + 8*f54 - f56 + f57 + 
       10*f58 - f60 + 8*f63 - 8*f64 - f65 + f66 + 8*f67 - 
       11*f69 - f70 + f71 - 8*f73 + 8*f74 + f75 - f76 + 
       8*f85 - f88 + 8*f107 - f108 - 8*f120 + f121 - 8*f126 + 
       f127 + 8*f128 - 8*f129 - f130 + f131 + 16*f160 - 
       2*f161 - 8*f175 + f176 - 11*f177 + f178)*tci12)/9 + 
    ((6*f12 + f44 - 6*f80 + f83)*fvu1u2u6*tci12)/9 + 
    ((-f47 + f51)*fvu1u2u9*tci12)/9 + 
    (8*f12*fvu2u1u1*tci12)/9 + (f12*fvu2u1u5*tci12)/3 + 
    fvu1u1u3^2*(-(f12*fvu1u1u4)/9 - (f12*fvu1u1u8)/6 - 
      (f12*tci12)/2) + fvu1u1u5^2*((f12*fvu1u1u9)/6 - 
      (f12*tci12)/9) + fvu1u1u9^2*
     ((f14 - f18 - f22 + f28 + f33)/6 - (f12*tci12)/18) + 
    fvu1u1u2^2*((6*f12 + 3*f22 + 2*f44 - f51 - 6*f80 + f83)/
       18 - (f12*fvu1u1u4)/6 - (f12*fvu1u1u5)/18 - 
      (f12*fvu1u1u9)/9 + (f12*tci12)/6) + 
    fvu2u1u9*((f40 - f47)/9 + (4*f12*tci12)/9) + 
    fvu1u1u1^2*((6*f12 - 3*f28 + 2*f40 - f47 - 6*f80 + f83)/
       18 - (f12*fvu1u1u2)/6 + (f12*fvu1u1u3)/18 + 
      (f12*fvu1u1u4)/6 + (f12*fvu1u1u8)/9 + (5*f12*tci12)/6) + 
    fvu1u1u8*((-2*f4 + 8*f6 - f7 + 16*f14 - 2*f16 + 3*f18 - 
        16*f22 + 8*f24 + 2*f25 - f27 - 16*f54 + 2*f56 - 2*f57 - 
        20*f58 + 2*f60 - 16*f63 + 16*f64 + 2*f65 - 2*f66 + 
        8*f67 + 19*f69 - f70 - 2*f71 + 8*f73 - 16*f74 - f75 + 
        2*f76 + 16*f120 - 2*f121 - 16*f124 + 2*f125 - 16*f160 + 
        2*f161 + 16*f175 - 2*f176 + 22*f177 - 2*f178)/18 - 
      (f47*fvu1u1u9)/9 - (2*f12*fvu2u1u9)/9 - 
      (f12*tci11^2)/54 + (f14*tci12)/3) + 
    fvu1u1u9*((8*f2 - f3 - 8*f6 + f7 - 8*f14 + 2*f16 + 
        6*f18 + 8*f22 - 8*f28 - f35 + 8*f54 - f56 + 9*f58 - 
        f60 + 8*f63 - 8*f64 - f65 + f66 + 8*f67 - 20*f69 - 
        f70 + 2*f71 - 8*f73 + 8*f74 + f75 - f76 + 8*f85 - 
        f88 + 8*f107 - f108 - 8*f120 + f121 - 8*f126 + f127 + 
        8*f128 - 8*f129 - f130 + f131 + 16*f160 - 2*f161 - 
        8*f175 + f176 - 11*f177 + f178)/9 + (2*f12*fvu2u1u11)/9 + 
      (2*f12*tci11^2)/27 - (f44*tci12)/9) + 
    tci11^2*((12*f12 - 18*f14 - 3*f18 - 18*f28 - 18*f33 + 
        2*f40 - 2*f51 - 12*f80 + 2*f83)/108 + 
      ((4*f12 + 2*f38 - f42 - 3*f45 - f49)*tci12)/36) + 
    fvu1u1u5*((f12*fvu1u1u9^2)/6 + (4*f12*fvu2u1u4)/9 - 
      (f12*fvu2u1u5)/3 + (13*f12*tci11^2)/108 + 
      (f12*fvu1u1u9*tci12)/3 + (f12*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*(f35/9 + (f12*fvu1u1u5^2)/9 - (f12*fvu1u1u8^2)/18 + 
      (f12*fvu1u1u9^2)/18 + (f33*tci12)/3 + 
      fvu1u1u5*(-(f12*fvu1u1u9)/3 - (2*f12*tci12)/9) + 
      fvu1u1u9*(f44/9 - (f12*tci12)/9) + 
      fvu1u1u8*(f40/9 + (f12*tci12)/9)) + 
    fvu1u1u3*(-(f12*fvu1u1u4^2)/9 + (f12*fvu1u1u5^2)/6 - 
      (f12*fvu1u1u8^2)/6 - (4*f12*fvu2u1u1)/9 - 
      (f12*fvu2u1u5)/3 + (17*f12*tci11^2)/27 - 
      (f12*fvu1u1u5*tci12)/3 + (f12*fvu1u1u8*tci12)/3 + 
      (f12*fvu1u2u7*tci12)/3 + fvu1u1u4*((f12*fvu1u1u8)/3 + 
        (2*f12*tci12)/9)) + fvu1u1u1*
     ((-16*f2 + 2*f3 + 8*f6 - f7 + 3*f18 - 8*f24 + f27 + 
        16*f28 - 24*f67 + 3*f69 + 3*f70 + 8*f73 - f75 - 
        16*f85 + 2*f88 - 16*f107 + 2*f108 + 16*f124 - 2*f125 + 
        16*f126 - 2*f127 - 16*f128 + 16*f129 + 2*f130 - 2*f131 - 
        16*f160 + 2*f161)/18 + (5*f12*fvu1u1u3^2)/18 + 
      (f12*fvu1u1u4^2)/6 - (f12*fvu1u1u5^2)/6 + 
      (2*f12*fvu1u1u8^2)/9 + (f47*fvu1u1u9)/9 - 
      (4*f12*fvu2u1u1)/9 - (2*f12*fvu2u1u9)/9 - 
      (f12*tci11^2)/3 + ((-6*f12 + 3*f28 + f47 + 6*f80 - f83)*
        tci12)/9 + fvu1u1u3*(-(f12*fvu1u1u4)/9 - 
        (5*f12*tci12)/9) + fvu1u1u8*(-f40/9 - 
        (4*f12*tci12)/9) + fvu1u1u4*(-f40/9 - 
        (2*f12*fvu1u1u8)/9 - (f12*tci12)/3) + 
      fvu1u1u2*((-6*f12 + 6*f80 - f83)/9 + (f12*tci12)/3)) + 
    fvu1u1u2*(-f25/9 + (f12*fvu1u1u3^2)/6 - (f12*fvu1u1u4^2)/6 - 
      (5*f12*fvu1u1u5^2)/18 + (f51*fvu1u1u8)/9 - 
      (2*f12*fvu1u1u9^2)/9 + (4*f12*fvu2u1u4)/9 + 
      (2*f12*fvu2u1u11)/9 - (f12*tci11^2)/9 - (f51*tci12)/9 - 
      (f12*fvu1u1u3*tci12)/3 - (f12*fvu1u1u5*tci12)/9 + 
      fvu1u1u9*(-f44/9 - (2*f12*tci12)/9) + 
      fvu1u1u4*(-f44/9 + (f12*fvu1u1u5)/9 + (2*f12*fvu1u1u9)/9 + 
        (f12*tci12)/3)) + ((-2*f12 + 2*f38 - 3*f45)*tci12*
      tcr11^2)/6 + ((6*f12 - 4*f38 + f45)*tcr11^3)/18 + 
    ((19*f12 - 2*f38 + 6*f42 - 7*f45 + 6*f49)*tci11^2*
      tcr12)/36 + ((-f12 + 2*f38 + f45)*tcr12^3)/9 - 
    (2*(f12 - f38 + f45)*tci12*tcr21)/3 + 
    tcr11*(((5*f12 + 7*f45)*tci11^2)/36 + 
      ((-f12 + f45)*tci12*tcr12)/3 + 
      (2*(f12 - f38)*tcr21)/3) + 
    ((f12 - 2*f38 - f45)*tcr31)/3 + 
    ((f12 - 2*f38 - f45)*tcr32)/12 + 
    ((-11*f12 + 74*f38 + 18*f42 + 11*f45 + 18*f49)*tcr33)/72;
L ieu1ueu2 = (f4 - f16 - 9*f18 + f57 + f58 + 9*f69 - 
      f71)/9 + ((f12 + 2*f38 - f45)*fvu1u1u1^2)/6 + 
    ((f12 + 2*f42 - f49)*fvu1u1u2^2)/6 + 
    ((f45 - f49)*fvu1u1u8^2)/6 + 
    fvu1u1u4*(f33/3 + (f38*fvu1u1u8)/3 + (f42*fvu1u1u9)/3) + 
    fvu1u1u8*(f14/3 - (f45*fvu1u1u9)/3) + 
    ((f12 + f38)*fvu2u1u2)/3 + ((f12 + f42)*fvu2u1u3)/3 + 
    ((f38 - f45)*fvu2u1u9)/3 + ((f42 - f49)*fvu2u1u11)/3 + 
    ((f45 - f49)*fvu2u1u12)/3 + ((f12 + f38 - f49)*tci11^2)/
     18 + ((-f14 - f28 - f33)*tci12)/3 + 
    ((f12 + f42)*fvu1u2u6*tci12)/3 + 
    ((-f45 + f49)*fvu1u2u9*tci12)/3 + 
    fvu1u1u9*((-f14 + f18 + f22 - f28 - f33)/3 - 
      (f42*tci12)/3) + fvu1u1u1*(f28/3 - (f12*fvu1u1u2)/3 - 
      (f38*fvu1u1u4)/3 - (f38*fvu1u1u8)/3 + (f45*fvu1u1u9)/3 + 
      ((-f12 + f45)*tci12)/3) + 
    fvu1u1u2*(-f22/3 - (f42*fvu1u1u4)/3 + (f49*fvu1u1u8)/3 - 
      (f42*fvu1u1u9)/3 - (f49*tci12)/3);
L ieu0ueu2 = -f18/3;
L ieum1ueu2 = 0;
L ieum2ueu2 = 0;
L ieu2uou2 = (-4*f37*fvu4u25)/3 + (10*f37*fvu4u28)/3 - 
    (2*f37*fvu4u39)/3 - (4*f37*fvu4u41)/3 + (4*f37*fvu4u49)/9 + 
    (2*f37*fvu4u51)/3 - (2*f37*fvu4u80)/3 - (8*f37*fvu4u83)/3 + 
    2*f37*fvu4u91 - (2*f37*fvu4u93)/3 + (2*f37*fvu4u100)/9 - 
    (4*f37*fvu4u102)/9 - (2*f37*fvu4u139)/3 + (2*f37*fvu4u148)/9 + 
    (17*f37*fvu4u171)/12 - (3*f37*fvu4u174)/4 - (3*f37*fvu4u182)/4 + 
    (17*f37*fvu4u184)/12 - (17*f37*fvu4u192)/18 + (f37*fvu4u199)/4 + 
    (f37*fvu4u201)/2 + f37*fvu4u202 + (2*f37*fvu4u213)/3 - 
    (2*f37*fvu4u215)/3 + (2*f37*fvu4u221)/3 + 5*f37*fvu4u244 + 
    (8*f37*fvu4u246)/3 - 2*f37*fvu4u250 - (13*f37*fvu4u252)/6 - 
    (f37*fvu4u255)/2 + (7*f37*fvu4u271)/12 - (43*f37*fvu4u273)/12 + 
    (43*f37*fvu4u277)/12 + (7*f37*fvu4u279)/12 - 
    (2*f37*fvu4u282)/3 - (f37*fvu4u289)/4 - (f37*fvu4u290)/2 - 
    f37*fvu4u291 - (2*f37*fvu4u293)/3 - (2*f37*fvu4u295)/3 + 
    2*f37*fvu4u313 + fvu3u71*((2*f37*fvu1u1u2)/3 - 
      (2*f37*fvu1u1u5)/3 - (2*f37*fvu1u1u8)/3) + 
    fvu3u23*((2*f37*fvu1u1u2)/3 + (2*f37*fvu1u1u6)/3 - 
      (2*f37*fvu1u1u7)/3 - (2*f37*fvu1u1u9)/3) + 
    fvu3u43*(-(f37*fvu1u1u6)/3 + (f37*fvu1u1u7)/3 + 
      (f37*fvu1u1u9)/3) + fvu3u63*((f37*fvu1u1u6)/3 + 
      (f37*fvu1u1u7)/3 + (f37*fvu1u1u9)/3 - (2*f37*fvu1u1u10)/
       3) + fvu3u45*(-(f37*fvu1u1u3)/3 + (2*f37*fvu1u1u5)/3 - 
      (4*f37*fvu1u1u6)/3 + (5*f37*fvu1u1u7)/3 - f37*fvu1u1u8 - 
      (2*f37*fvu1u1u9)/3 - (f37*fvu1u1u10)/3) + 
    fvu3u70*((f37*fvu1u1u3)/3 + (f37*fvu1u1u6)/3 - 
      (f37*fvu1u1u8)/3 + (f37*fvu1u1u9)/3 - (f37*fvu1u1u10)/3) + 
    fvu3u81*((2*f37*fvu1u1u3)/3 + (f37*fvu1u1u4)/3 + 
      (2*f37*fvu1u1u6)/3 - (2*f37*fvu1u1u7)/3 - 
      (f37*fvu1u1u9)/3 + (f37*fvu1u1u10)/3) - 
    (13*f37*tci11^3*tci12)/15 + 
    fvu3u25*((-2*f37*fvu1u1u1)/3 - (f37*fvu1u1u2)/3 - 
      (f37*fvu1u1u3)/3 + 2*f37*fvu1u1u4 - (2*f37*fvu1u1u5)/3 - 
      f37*fvu1u1u6 + (2*f37*fvu1u1u7)/3 + f37*fvu1u1u8 + 
      f37*fvu1u1u9 + (f37*fvu1u1u10)/3 - 2*f37*tci12) + 
    fvu3u82*((f37*fvu1u1u3)/3 + (f37*fvu1u1u4)/3 - 
      (f37*fvu1u1u7)/3 + (f37*fvu1u1u8)/3 - (f37*fvu1u1u9)/3 - 
      (2*f37*tci12)/3) + fvu3u78*((f37*fvu1u1u2)/3 - 
      (f37*fvu1u1u3)/3 - (f37*fvu1u1u4)/3 + (f37*fvu1u1u6)/3 - 
      (f37*fvu1u1u8)/3 + (2*f37*tci12)/3) + 
    fvu3u80*((f37*fvu1u1u2)/3 - (f37*fvu1u1u3)/3 - 
      (f37*fvu1u1u4)/3 + (2*f37*fvu1u1u5)/3 - 
      (11*f37*fvu1u1u6)/6 + (13*f37*fvu1u1u7)/6 - 
      (5*f37*fvu1u1u8)/2 + (13*f37*fvu1u1u9)/6 + 
      (7*f37*tci12)/2) + (10*f37*tci11^2*tci21)/27 + 
    fvu1u1u10*((-4*f37*tci11^3)/81 + (4*f37*tci12*tci21)/
       3) + tci12*((-16*f37*tci31)/5 - 16*f37*tci32) + 
    (2072*f37*tci41)/135 + (64*f37*tci42)/5 + 
    (16*f37*tci43)/3 + ((-217*f37*tci11^3)/405 - 
      (4*f37*tci12*tci21)/3 - 16*f37*tci32)*tcr11 - 
    (13*f37*tci11*tcr11^3)/45 + 
    fvu1u1u9*((83*f37*tci11^3)/270 - 3*f37*tci12*tci21 + 
      (72*f37*tci31)/5 + 6*f37*tci21*tcr11 - 
      (3*f37*tci11*tcr11^2)/10) + 
    fvu1u1u4*((11*f37*tci11^3)/135 + (4*f37*tci12*tci21)/
       3 + (48*f37*tci31)/5 + 4*f37*tci21*tcr11 - 
      (f37*tci11*tcr11^2)/5) + 
    fvu1u1u8*((-4*f37*tci11^3)/135 + (13*f37*tci12*tci21)/
       3 + (48*f37*tci31)/5 + 4*f37*tci21*tcr11 - 
      (f37*tci11*tcr11^2)/5) + 
    fvu1u1u7*((161*f37*tci11^3)/810 - (43*f37*tci12*tci21)/
       9 + (8*f37*tci31)/5 + (2*f37*tci21*tcr11)/3 - 
      (f37*tci11*tcr11^2)/30) + 
    fvu1u1u6*((-161*f37*tci11^3)/810 + (43*f37*tci12*tci21)/
       9 - (8*f37*tci31)/5 - (2*f37*tci21*tcr11)/3 + 
      (f37*tci11*tcr11^2)/30) + 
    fvu1u1u5*((2*f37*tci11^3)/45 - (32*f37*tci12*tci21)/9 - 
      (32*f37*tci31)/5 - (8*f37*tci21*tcr11)/3 + 
      (2*f37*tci11*tcr11^2)/15) + 
    fvu1u1u2*(-(f37*fvu3u43)/3 - (f37*fvu3u63)/3 - 
      (f37*fvu3u70)/3 - (23*f37*tci11^3)/162 + 
      (8*f37*tci12*tci21)/9 - 8*f37*tci31 - 
      (10*f37*tci21*tcr11)/3 + (f37*tci11*tcr11^2)/6) + 
    fvu1u1u1*((4*f37*fvu3u45)/3 + (2*f37*fvu3u71)/3 + 
      (f37*fvu3u78)/3 - (f37*fvu3u80)/3 - f37*fvu3u81 - 
      (f37*fvu3u82)/3 - (53*f37*tci11^3)/405 - 
      (48*f37*tci31)/5 - 4*f37*tci21*tcr11 + 
      (f37*tci11*tcr11^2)/5) + 
    ((218*f37*tci11^3)/243 + (40*f37*tci12*tci21)/3)*
     tcr12 + (4*f37*tci11*tci12 + (40*f37*tci21)/3)*
     tcr12^2 + tcr11^2*((f37*tci11*tci12)/15 + 
      2*f37*tci21 + 2*f37*tci11*tcr12) - 
    (130*f37*tci11*tcr33)/27;
L ieu1uou2 = 
   2*f37*fvu3u25 + (11*f37*tci11^3)/135 + 
    (4*f37*tci12*tci21)/3 + (48*f37*tci31)/5 + 
    4*f37*tci21*tcr11 - (f37*tci11*tcr11^2)/5;
L ieu0uou2 = 0;
L ieum1uou2 = 0;
L ieum2uou2 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou2+w^2*ieu1uou2+w^3*ieu0uou2+w^4*ieum1uou2+w^5*ieum2uou2;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou2a = K[w^1];
L ieu1uou2a = K[w^2];
L ieu0uou2a = K[w^3];
L ieum1uou2a = K[w^4];
L ieum2uou2a = K[w^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_odd.c> "%O"
#write <e2_odd.c> "return Eps5o2<T>("
#write <e2_odd.c> "%E", ieu2uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieu0uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum1uou2a
#write <e2_odd.c> ", "
#write <e2_odd.c> "%E", ieum2uou2a
#write <e2_odd.c> ");\n}"
L H=+u^1*ieu2ueu2+u^2*ieu1ueu2+u^3*ieu0ueu2+u^4*ieum1ueu2+u^5*ieum2ueu2;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu2a = H[u^1];
L ieu1ueu2a = H[u^2];
L ieu0ueu2a = H[u^3];
L ieum1ueu2a = H[u^4];
L ieum2ueu2a = H[u^5];
.sort
#write <e2.tmp> "`optimmaxvar_'"
#write <e2_even.c> "%O"
#write <e2_even.c> "return Eps5o2<T>("
#write <e2_even.c> "%E", ieu2ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieu0ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum1ueu2a
#write <e2_even.c> ", "
#write <e2_even.c> "%E", ieum2ueu2a
#write <e2_even.c> ");\n}"
.end
