#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f136;
S f65;
S f134;
S f133;
S f63;
S f130;
S f60;
S f69;
S f138;
S f124;
S f125;
S f120;
S f70;
S f121;
S f71;
S f123;
S f128;
S f151;
S f150;
S f18;
S f19;
S f16;
S f7;
S f6;
S f29;
S f178;
S f22;
S f21;
S f27;
S f177;
S f26;
S f176;
S f175;
S f25;
S f24;
S f118;
S f45;
S f160;
S f30;
S f115;
S f161;
S f114;
S f47;
S f32;
S f117;
S f116;
S f111;
S f110;
S f113;
S f112;
S f108;
S f58;
S f109;
S f59;
S f106;
S f56;
S f107;
S f57;
S f54;
S f105;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (-96*f16 + 11*f18 + 729*f57 - 25*f58 + 
      74*f59 + 11*f60 + 839*f69 - 85*f71 + 85*f123 + 121*f177 - 
      11*f178)/27 - (f114*fvu3u3)/9 + (f114*fvu3u4)/12 - 
    (11*f114*fvu3u5)/36 + (f114*fvu3u6)/9 - (f114*fvu3u7)/6 - 
    (8*f114*fvu3u8)/9 + (f116*fvu3u10)/6 - (f114*fvu3u11)/9 + 
    (f114*fvu3u12)/36 + (f114*fvu3u13)/36 - (f114*fvu3u17)/12 + 
    ((f114 - 2*f117)*fvu3u18)/12 + (f114*fvu3u24)/9 + 
    (f114*fvu3u51)/9 + (f114*fvu3u52)/18 - (2*f114*fvu3u55)/9 + 
    ((3*f105 - 2*f114)*fvu3u56)/18 + ((-f112 - f114)*fvu3u57)/6 + 
    (f114*fvu3u59)/6 + (f114*fvu3u61)/3 + (f114*fvu1u1u1^3)/18 - 
    (11*f114*fvu1u1u2^3)/54 - (f114*fvu1u1u6^3)/9 + 
    (f114*fvu1u1u8^3)/18 - (8*f114*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(-(f114*fvu1u1u5)/36 + (f114*fvu1u1u8)/36 - 
      (7*f114*fvu1u1u9)/36) + fvu1u1u3^2*
     ((3*f107 - f115 + f118)/18 + (f114*fvu1u1u4)/12 + 
      (f114*fvu1u1u6)/18 + (f114*fvu1u1u8)/12 + 
      (f114*fvu1u1u9)/9 - (f114*fvu1u1u10)/6) + 
    ((-f115 + f118)*fvu2u1u13)/9 + ((-f113 + f118)*fvu2u1u15)/9 + 
    (f114*fvu1u2u6*tci11^2)/3 + 
    ((-8*f16 + 8*f19 - f21 + 8*f22 + 8*f24 - f25 - f27 + 
       8*f30 - f32 + 80*f57 + 8*f59 + f60 - 8*f63 + f65 + 
       88*f69 - 9*f71 + 8*f109 - 8*f110 + 8*f123 - 8*f133 + 
       f134 - 8*f136 + f138 + 8*f150 - f151 - 8*f160 + f161 + 
       8*f175 - f176 - 8*f177 + f178)*tci12)/9 + 
    ((f115 - f118)*fvu1u2u2*tci12)/9 - 
    (2*f114*fvu2u1u2*tci12)/9 + (f114*fvu2u1u3*tci12)/3 + 
    (2*f114*fvu2u1u11*tci12)/9 + 
    fvu1u1u10*((8*f6 - f7 + 4*f16 - f18 + 8*f19 - f21 + 
        8*f22 + 8*f24 - f25 - f27 + 8*f30 - f32 - 8*f54 + 
        f56 - 19*f57 - f58 - 2*f59 - 8*f63 + f65 - 21*f69 + 
        2*f71 - 8*f110 - 3*f123 - 8*f124 + f125 - 8*f133 + 
        f134 - 8*f136 + f138 + 8*f150 - f151 - 8*f160 + f161 + 
        8*f175 - f176 - 19*f177 + 2*f178)/9 - 
      (2*f114*fvu2u1u14)/9 - (4*f114*fvu2u1u15)/9 - 
      (f114*tci11^2)/9 + (f113*tci12)/9) + 
    fvu2u1u1*((6*f26 - f29 + f115 - 6*f117)/9 - 
      (2*f114*tci12)/3) + fvu1u1u8^2*
     ((-6*f45 + f47 - 3*f106 + f113 + 6*f116)/18 - 
      (f114*fvu1u1u9)/18 + (f114*fvu1u1u10)/6 - 
      (f114*tci12)/2) + fvu2u1u9*
     ((6*f26 - f29 + 6*f45 - f47 - 6*f116 - 6*f117)/9 - 
      (2*f114*tci12)/9) + fvu1u1u1^2*
     ((-3*f16 + 12*f26 - 2*f29 + 6*f45 - f47 + 30*f57 + 3*f58 + 
        3*f59 + 30*f69 - 3*f71 + 3*f106 - 3*f107 + 3*f109 - 
        3*f110 + f115 - 6*f116 - 12*f117 + 3*f123)/18 + 
      (f114*fvu1u1u2)/18 - (f114*fvu1u1u4)/18 - 
      (f114*fvu1u1u8)/18 - (f114*fvu1u1u9)/18 - 
      (f114*fvu1u1u10)/6 - (f114*tci12)/18) + 
    fvu1u1u6^2*((f114*fvu1u1u9)/6 - (f114*fvu1u1u10)/9 - 
      (f114*tci12)/18) + fvu1u1u5^2*(-(f114*fvu1u1u9)/12 + 
      (f114*tci12)/36) + fvu1u1u9^2*
     ((-3*f109 - f113 + f118)/18 - (f114*fvu1u1u10)/18 + 
      (f114*tci12)/12) + fvu1u1u10^2*
     (f110/6 + (f114*tci12)/6) + fvu1u1u2^2*
     (-(f114*fvu1u1u4)/18 + (f114*fvu1u1u5)/18 - 
      (f114*fvu1u1u8)/18 - (f114*fvu1u1u9)/9 + 
      (2*f114*tci12)/9) + fvu2u1u12*
     ((-6*f45 + f47 + f113 + 6*f116)/9 + (f114*tci12)/3) + 
    fvu1u2u9*(-(f114*tci11^2)/3 + 
      ((6*f45 - f47 - f113 - 6*f116)*tci12)/9) + 
    tci11^2*((-21*f16 + 12*f26 - 2*f29 + 210*f57 + 21*f58 + 
        21*f59 + 210*f69 - 21*f71 + 18*f109 - 18*f110 + 2*f113 - 
        12*f117 + 2*f118 + 21*f123)/108 + 
      ((f105 + f112 + 5*f114 - 3*f116 - 2*f117)*tci12)/36) + 
    fvu1u1u5*(-(f114*fvu1u1u9^2)/12 - (f114*fvu2u1u4)/9 - 
      (f114*tci11^2)/27 - (f114*fvu1u1u9*tci12)/6) + 
    fvu1u1u6*((f114*fvu1u1u9^2)/6 - (2*f114*fvu2u1u14)/9 + 
      (2*f114*tci11^2)/27 + (f114*fvu1u1u9*tci12)/3 - 
      (2*f114*fvu1u1u10*tci12)/9) + 
    fvu1u1u8*((-8*f54 + f56 + 8*f67 - f70 + 8*f106 + 8*f120 - 
        f121 - 8*f124 + f125 + 8*f128 - f130)/9 + 
      (f114*fvu1u1u9^2)/9 + (f114*fvu2u1u9)/9 - 
      (2*f114*fvu2u1u11)/9 - (f114*fvu2u1u12)/3 + 
      (73*f114*tci11^2)/108 + (f106*tci12)/3 + 
      (f114*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (-f113/9 - (f114*tci12)/3) + fvu1u1u9*
       ((6*f45 - f47 - 6*f116)/9 - (2*f114*tci12)/9)) + 
    fvu1u1u9*((-8*f6 + f7 + 8*f54 - f56 + 8*f109 + 8*f124 - 
        f125)/9 + (f114*fvu1u1u10^2)/6 - (7*f114*fvu2u1u11)/9 - 
      (f114*fvu2u1u12)/3 - (4*f114*fvu2u1u15)/9 - 
      (7*f114*tci11^2)/54 + (f118*tci12)/9 + 
      (f114*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (f113/9 - (f114*tci12)/9)) + 
    fvu1u1u2*((2*f114*fvu1u1u4^2)/9 + (f114*fvu1u1u5^2)/9 + 
      (f114*fvu1u1u8^2)/18 + (5*f114*fvu1u1u9^2)/18 - 
      (f114*fvu2u1u3)/3 - (f114*fvu2u1u4)/9 - 
      (5*f114*fvu2u1u11)/9 + (f114*tci11^2)/27 + 
      (f114*fvu1u1u5*tci12)/9 - (2*f114*fvu1u1u9*tci12)/9 - 
      (f114*fvu1u2u6*tci12)/3 + fvu1u1u8*((f114*fvu1u1u9)/9 - 
        (f114*tci12)/9) + fvu1u1u4*(-(f114*fvu1u1u5)/9 + 
        (f114*fvu1u1u8)/9 + (2*f114*fvu1u1u9)/9 - 
        (f114*tci12)/9)) + fvu1u1u4*(-(f114*fvu1u1u5^2)/36 + 
      (f114*fvu1u1u8^2)/36 - (7*f114*fvu1u1u9^2)/36 + 
      (f114*fvu2u1u2)/9 - (f114*fvu2u1u3)/3 - 
      (2*f114*fvu2u1u11)/9 + (f114*tci11^2)/27 + 
      (7*f114*fvu1u1u9*tci12)/18 - (f114*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f114*fvu1u1u9)/9 - (f114*tci12)/18) + 
      fvu1u1u5*((f114*fvu1u1u9)/6 + (f114*tci12)/18)) + 
    fvu1u1u3*(-f108/9 + (f114*fvu1u1u4^2)/12 + 
      (f114*fvu1u1u6^2)/18 + (f114*fvu1u1u8^2)/12 + 
      (f114*fvu1u1u9^2)/9 - (f114*fvu1u1u10^2)/6 + 
      (f114*fvu2u1u1)/3 + (f114*tci11^2)/36 - 
      (f107*tci12)/3 + fvu1u1u9*(-f118/9 + (f114*fvu1u1u10)/9 - 
        (2*f114*tci12)/9) + fvu1u1u8*
       ((6*f26 - f29 - 6*f117)/9 - (f114*tci12)/6) + 
      fvu1u1u4*(-(f114*fvu1u1u8)/6 - (f114*tci12)/6) + 
      fvu1u1u6*(-(f114*fvu1u1u9)/3 + (2*f114*fvu1u1u10)/9 - 
        (f114*tci12)/9) + fvu1u1u10*(f115/9 + 
        (f114*tci12)/3)) + fvu1u1u1*
     ((8*f16 - 8*f19 + f21 - 8*f22 - 8*f24 + f25 + f27 - 
        8*f30 + f32 + 8*f54 - f56 - 80*f57 - 8*f59 - f60 + 
        8*f63 - f65 - 8*f67 - 88*f69 + f70 + 9*f71 - 8*f106 + 
        f108 - 8*f109 + 8*f110 - 8*f120 + f121 - 8*f123 + 
        8*f124 - f125 - 8*f128 + f130 + 8*f133 - f134 + 
        8*f136 - f138 - 8*f150 + f151 + 8*f160 - f161 - 
        8*f175 + f176 + 8*f177 - f178)/9 + (2*f114*fvu1u1u2^2)/
       9 - (f114*fvu1u1u3^2)/6 - (f114*fvu1u1u4^2)/9 - 
      (f114*fvu1u1u8^2)/9 - (f114*fvu1u1u9^2)/9 + 
      (f114*fvu2u1u1)/3 + (f114*fvu2u1u2)/9 + (f114*fvu2u1u9)/9 + 
      (2*f114*fvu2u1u11)/9 - (23*f114*tci11^2)/54 + 
      ((3*f16 - 6*f45 + f47 - 30*f57 - 3*f58 - 3*f59 - 30*f69 + 
         3*f71 - 3*f106 + 3*f107 - 3*f109 + 3*f110 - f115 + 
         6*f116 - 3*f123)*tci12)/9 + 
      fvu1u1u9*((-6*f45 + f47 + 6*f116)/9 - (f114*tci12)/9) + 
      fvu1u1u2*(-(f114*fvu1u1u4)/9 - (f114*fvu1u1u8)/9 - 
        (f114*fvu1u1u9)/9 + (f114*tci12)/9) + 
      fvu1u1u8*((-6*f26 + f29 + 6*f117)/9 + (f114*fvu1u1u9)/9 + 
        (2*f114*tci12)/9) + fvu1u1u4*((f114*fvu1u1u8)/9 + 
        (f114*fvu1u1u9)/9 + (2*f114*tci12)/9) + 
      fvu1u1u10*(-f115/9 + (f114*tci12)/3) + 
      fvu1u1u3*((-6*f26 + f29 + 6*f117)/9 + (f114*tci12)/3)) + 
    ((10*f114 - 9*f116 - 6*f117)*tci12*tcr11^2)/18 + 
    ((-2*f114 + f116 + 4*f117)*tcr11^3)/18 + 
    ((-6*f105 - 6*f112 + 7*f114 - 7*f116 + 2*f117)*tci11^2*
      tcr12)/36 + ((-f114 + f116 - 2*f117)*tcr12^3)/9 + 
    (2*(8*f114 - 3*f116 - 3*f117)*tci12*tcr21)/9 + 
    tcr11*(((25*f114 + 21*f116)*tci11^2)/108 + 
      ((-f114 + f116)*tci12*tcr12)/3 + 
      ((-5*f114 + 6*f117)*tcr21)/9) + 
    ((f114 - f116 + 2*f117)*tcr31)/3 + 
    ((f114 - f116 + 2*f117)*tcr32)/12 + 
    ((-18*f105 - 18*f112 - 11*f114 + 11*f116 - 74*f117)*tcr33)/
     72;
L ieu1ueu5 = 
   (-12*f16 + f18 + 99*f57 + f58 + 10*f59 + f60 + 109*f69 - 
      11*f71 + 11*f123 + 11*f177 - f178)/9 + 
    ((f114 - f116 - 2*f117)*fvu1u1u1^2)/6 + 
    ((f105 - f114)*fvu1u1u3^2)/6 + ((f112 + f116)*fvu1u1u8^2)/6 + 
    ((f105 - f112)*fvu1u1u9^2)/6 + 
    fvu1u1u8*(f106/3 - (f116*fvu1u1u9)/3 - (f112*fvu1u1u10)/3) + 
    fvu1u1u3*(-f107/3 - (f117*fvu1u1u8)/3 - (f105*fvu1u1u9)/3 + 
      (f114*fvu1u1u10)/3) + ((f114 - f117)*fvu2u1u1)/3 + 
    ((-f116 - f117)*fvu2u1u9)/3 + ((f112 + f116)*fvu2u1u12)/3 + 
    ((f105 - f114)*fvu2u1u13)/3 + ((f105 - f112)*fvu2u1u15)/3 + 
    ((f105 + f112 - f117)*tci11^2)/18 + 
    ((-f16 + 10*f57 + f58 + f59 + 10*f69 - f71 + f109 - 
       f110 + f123)*tci12)/3 + ((-f105 + f114)*fvu1u2u2*
      tci12)/3 + ((-f112 - f116)*fvu1u2u9*tci12)/3 + 
    fvu1u1u9*(f109/3 + (f112*fvu1u1u10)/3 + (f105*tci12)/3) + 
    fvu1u1u10*(-f110/3 + (f112*tci12)/3) + 
    fvu1u1u1*((f16 - 10*f57 - f58 - f59 - 10*f69 + f71 - 
        f106 + f107 - f109 + f110 - f123)/3 + 
      (f117*fvu1u1u3)/3 + (f117*fvu1u1u8)/3 + (f116*fvu1u1u9)/3 - 
      (f114*fvu1u1u10)/3 + ((-f114 + f116)*tci12)/3);
L ieu0ueu5 = (-f16 + 10*f57 + f58 + f59 + 10*f69 - 
     f71 + f123)/3;
L ieum1ueu5 = 0;
L ieum2ueu5 = 0;
L ieu2uou5 = (-4*f111*fvu4u28)/3 - (4*f111*fvu4u39)/3 - 
    (2*f111*fvu4u49)/9 - (4*f111*fvu4u51)/9 - (f111*fvu4u80)/12 - 
    (7*f111*fvu4u83)/12 + (3*f111*fvu4u91)/4 - (f111*fvu4u93)/12 - 
    (4*f111*fvu4u100)/9 + (f111*fvu4u102)/2 - (f111*fvu4u111)/4 - 
    (f111*fvu4u113)/2 - f111*fvu4u114 + (2*f111*fvu4u129)/3 - 
    (13*f111*fvu4u132)/3 + (7*f111*fvu4u139)/2 - 
    (4*f111*fvu4u141)/3 - (4*f111*fvu4u146)/9 - 
    (7*f111*fvu4u148)/6 + (4*f111*fvu4u174)/3 + 
    (4*f111*fvu4u182)/3 + (2*f111*fvu4u190)/9 + 
    (4*f111*fvu4u192)/9 + (f111*fvu4u213)/12 + 
    (67*f111*fvu4u215)/12 - (9*f111*fvu4u219)/4 + 
    (f111*fvu4u221)/12 + (4*f111*fvu4u225)/9 + (f111*fvu4u233)/4 + 
    (f111*fvu4u234)/2 + f111*fvu4u235 + (10*f111*fvu4u244)/3 - 
    (2*f111*fvu4u246)/3 - 2*f111*fvu4u250 - (2*f111*fvu4u252)/3 - 
    (2*f111*fvu4u255)/9 - 2*f111*fvu4u273 + 2*f111*fvu4u277 + 
    fvu3u25*((-2*f111*fvu1u1u3)/3 + (2*f111*fvu1u1u6)/3 - 
      (2*f111*fvu1u1u7)/3) + fvu3u71*((2*f111*fvu1u1u3)/3 - 
      (2*f111*fvu1u1u6)/3 + (2*f111*fvu1u1u7)/3) + 
    fvu3u70*((f111*fvu1u1u6)/3 - (f111*fvu1u1u8)/3 - 
      (f111*fvu1u1u10)/3) + fvu3u45*(-(f111*fvu1u1u2) + 
      (f111*fvu1u1u3)/3 + (2*f111*fvu1u1u5)/3 + 
      (f111*fvu1u1u6)/3 - f111*fvu1u1u8 + (f111*fvu1u1u9)/3 - 
      (f111*fvu1u1u10)/3) + fvu3u23*(-(f111*fvu1u1u1)/3 - 
      (f111*fvu1u1u6)/3 + (f111*fvu1u1u8)/3 + 
      (f111*fvu1u1u10)/3) + fvu3u43*((f111*fvu1u1u2)/3 + 
      (f111*fvu1u1u4)/3 + (f111*fvu1u1u6)/3 - (f111*fvu1u1u7)/3 - 
      (2*f111*fvu1u1u9)/3 + (f111*fvu1u1u10)/3) + 
    fvu3u63*(f111*fvu1u1u2 - (3*f111*fvu1u1u3)/2 - 
      (2*f111*fvu1u1u5)/3 - (19*f111*fvu1u1u6)/6 - 
      (f111*fvu1u1u7)/3 - (2*f111*fvu1u1u8)/3 - 
      (f111*fvu1u1u9)/3 + (7*f111*fvu1u1u10)/2) + 
    (13*f111*tci11^3*tci12)/15 + 
    fvu3u62*((-2*f111*fvu1u1u2)/3 + f111*fvu1u1u3 + 
      (f111*fvu1u1u4)/3 - (2*f111*fvu1u1u5)/3 - 
      (2*f111*fvu1u1u6)/3 + (f111*fvu1u1u7)/3 + f111*fvu1u1u8 + 
      f111*fvu1u1u9 + (2*f111*fvu1u1u10)/3 - 2*f111*tci12) + 
    fvu3u78*(-(f111*fvu1u1u2)/3 + (f111*fvu1u1u3)/3 - 
      (f111*fvu1u1u4)/3 + (f111*fvu1u1u6)/3 - (f111*fvu1u1u8)/3 + 
      (2*f111*fvu1u1u9)/3 + (2*f111*tci12)/3) + 
    fvu3u80*((2*f111*fvu1u1u2)/3 - (f111*fvu1u1u3)/3 - 
      (f111*fvu1u1u4)/3 + (2*f111*fvu1u1u5)/3 - 
      (2*f111*fvu1u1u6)/3 + f111*fvu1u1u7 - (5*f111*fvu1u1u8)/3 + 
      f111*fvu1u1u9 + (8*f111*tci12)/3) - 
    (10*f111*tci11^2*tci21)/27 + 
    tci12*((16*f111*tci31)/5 + 16*f111*tci32) - 
    (2072*f111*tci41)/135 - (64*f111*tci42)/5 - 
    (16*f111*tci43)/3 + ((53*f111*tci11^3)/108 + 
      (5*f111*tci12*tci21)/3 - (12*f111*tci31)/5 + 
      16*f111*tci32)*tcr11 + (61*f111*tci11*tcr11^3)/
     180 + fvu1u1u10*((169*f111*tci11^3)/1620 + 
      (25*f111*tci12*tci21)/9 + (76*f111*tci31)/5 + 
      (19*f111*tci21*tcr11)/3 - (19*f111*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f111*tci11^3)/270 + 
      (72*f111*tci31)/5 + 6*f111*tci21*tcr11 - 
      (3*f111*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f111*fvu3u25)/3 - (f111*fvu3u43)/3 + 
      (2*f111*fvu3u45)/3 - (f111*fvu3u62)/3 + (13*f111*fvu3u63)/6 + 
      (f111*fvu3u70)/3 - (2*f111*fvu3u71)/3 - (f111*fvu3u78)/3 - 
      (f111*fvu3u80)/3 + (19*f111*tci11^3)/108 - 
      (f111*tci12*tci21)/3 + 12*f111*tci31 + 
      5*f111*tci21*tcr11 - (f111*tci11*tcr11^2)/4) + 
    fvu1u1u5*((f111*tci11^3)/45 - (16*f111*tci12*tci21)/9 - 
      (16*f111*tci31)/5 - (4*f111*tci21*tcr11)/3 + 
      (f111*tci11*tcr11^2)/15) + 
    fvu1u1u8*((-133*f111*tci11^3)/810 + 
      (8*f111*tci12*tci21)/3 - (24*f111*tci31)/5 - 
      2*f111*tci21*tcr11 + (f111*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f111*tci11^3)/180 - f111*tci12*tci21 - 
      (36*f111*tci31)/5 - 3*f111*tci21*tcr11 + 
      (3*f111*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f111*tci11^3)/162 - (16*f111*tci12*tci21)/
       9 - 8*f111*tci31 - (10*f111*tci21*tcr11)/3 + 
      (f111*tci11*tcr11^2)/6) + 
    fvu1u1u9*((-19*f111*tci11^3)/270 - 
      (20*f111*tci12*tci21)/9 - (56*f111*tci31)/5 - 
      (14*f111*tci21*tcr11)/3 + (7*f111*tci11*tcr11^2)/
       30) + fvu1u1u3*((-391*f111*tci11^3)/1620 + 
      (f111*tci12*tci21)/3 - (84*f111*tci31)/5 - 
      7*f111*tci21*tcr11 + (7*f111*tci11*tcr11^2)/20) + 
    ((-218*f111*tci11^3)/243 - (40*f111*tci12*tci21)/3)*
     tcr12 + (-4*f111*tci11*tci12 - (40*f111*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f111*tci11*tci12)/15 - 
      3*f111*tci21 - 2*f111*tci11*tcr12) + 
    (130*f111*tci11*tcr33)/27;
L ieu1uou5 = 
   2*f111*fvu3u62 - (11*f111*tci11^3)/135 - 
    (4*f111*tci12*tci21)/3 - (48*f111*tci31)/5 - 
    4*f111*tci21*tcr11 + (f111*tci11*tcr11^2)/5;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
