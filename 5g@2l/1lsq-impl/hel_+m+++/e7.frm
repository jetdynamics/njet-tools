#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f66;
S f180;
S f65;
S f181;
S f64;
S f63;
S f62;
S f132;
S f131;
S f130;
S f60;
S f69;
S f68;
S f74;
S f124;
S f125;
S f76;
S f127;
S f77;
S f70;
S f120;
S f121;
S f71;
S f122;
S f72;
S f123;
S f73;
S f128;
S f129;
S f79;
S f157;
S f9;
S f148;
S f149;
S f19;
S f3;
S f80;
S f17;
S f14;
S f15;
S f82;
S f85;
S f84;
S f87;
S f11;
S f86;
S f4;
S f97;
S f94;
S f178;
S f28;
S f90;
S f22;
S f21;
S f177;
S f176;
S f25;
S f24;
S f118;
S f160;
S f161;
S f31;
S f117;
S f32;
S f165;
S f166;
S f58;
S f59;
S f53;
S f50;
S f106;
S f56;
S f57;
S f54;
S f104;
S f105;
S f55;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu1u1u9;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u291;
S fvu4u293;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u279;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu7 = (-121*f3 + 11*f4 + 110*f19 - 11*f22 + 
      99*f58 + 74*f59 + 11*f60 - 11*f104 + 11*f120 - 11*f124 - 
      11*f157)/27 + ((f79 + 2*f94)*fvu3u1)/3 + 
    ((-f79 - 2*f94)*fvu3u2)/6 + (f79*fvu3u3)/9 + 
    ((f79 + f94)*fvu3u4)/6 + (f94*fvu3u5)/18 + 
    (2*(2*f79 + f94)*fvu3u6)/9 - (2*f94*fvu3u11)/9 + 
    ((2*f79 + f94)*fvu3u12)/18 + ((f79 + 3*f90 - 3*f94)*fvu3u13)/
     18 + (f79*fvu3u14)/3 + ((f79 - f94)*fvu3u15)/6 + 
    (f94*fvu3u17)/6 + ((2*f79 + f94)*fvu3u18)/6 + 
    ((f79 + 2*f94)*fvu3u19)/9 - (f79*fvu3u22)/3 - 
    (2*f79*fvu3u31)/9 + ((-11*f79 + f94)*fvu3u32)/18 + 
    ((-f53 - 2*f79)*fvu3u33)/6 - (16*f79*fvu3u34)/9 + 
    (f79*fvu3u35)/6 - (2*(f79 + f94)*fvu3u36)/9 + 
    ((f79 - 3*f94)*fvu3u37)/18 - (f94*fvu3u38)/6 + 
    ((-f79 + f85 + f94)*fvu3u40)/6 + (2*f79*fvu3u44)/9 + 
    ((9*f79 + 4*f94)*fvu1u1u1^3)/18 + 
    ((-2*f79 + 5*f94)*fvu1u1u2^3)/9 + ((-f79 + 6*f94)*fvu1u1u3^3)/
     18 + ((2*f79 - f94)*fvu1u1u5^3)/9 + 
    ((17*f79 - 3*f94)*fvu1u1u6^3)/27 + 
    ((7*f79 + 3*f94)*fvu1u1u7^3)/27 + 
    fvu1u1u4^2*((-6*f9 + f50 - 6*f53 + 3*f68 - f82)/18 + 
      ((-f79 + 3*f94)*fvu1u1u5)/18 - (f79*fvu1u1u6)/9 + 
      (f79*fvu1u1u7)/9 + ((2*f79 + f94)*fvu1u1u8)/18 + 
      (f94*fvu1u1u9)/18) + ((6*f84 - f86 - 6*f90 - f97)*
      fvu2u1u4)/9 + ((-6*f9 + f50 - 6*f53 - f82)*fvu2u1u6)/9 + 
    ((-f87 + f97)*fvu2u1u7)/9 + ((f79 + 2*f94)*fvu1u2u7*
      tci11^2)/3 + (2*(3*f79 - f94)*fvu1u2u8*tci11^2)/9 + 
    fvu1u1u9*((2*f94*fvu2u1u11)/9 + (2*f94*tci11^2)/27) + 
    (f69*tci12)/9 - (f94*fvu1u1u9^2*tci12)/18 + 
    ((6*f9 - f50 + 6*f53 + f82)*fvu1u2u3*tci12)/9 - 
    (4*(f79 + f94)*fvu2u1u1*tci12)/3 + 
    (4*(f79 - f94)*fvu2u1u2*tci12)/9 + 
    ((-f79 - 2*f94)*fvu2u1u5*tci12)/3 - 
    (4*(2*f79 + f94)*fvu2u1u9*tci12)/9 + 
    (2*(3*f79 - f94)*fvu2u1u10*tci12)/9 + 
    fvu2u1u8*((6*f9 - f50 + 6*f53 - f87)/9 - 
      (4*f79*tci12)/9) + fvu1u2u6*
     ((2*(f79 - f94)*tci11^2)/9 + 
      ((f82 + 6*f84 - f86 - 6*f90)*tci12)/9) + 
    tci11^2*((-12*f9 + 2*f50 - 12*f53 + 3*f59 + 18*f68 - 
        8*f82 + 2*f87 + 2*f97)/108 + 
      ((-3*f53 - 7*f79 + 3*f90 - 10*f94)*tci12)/108) + 
    fvu1u1u1^2*((f79*fvu1u1u2)/3 - (f79*fvu1u1u3)/6 + 
      ((-7*f79 - 2*f94)*fvu1u1u4)/18 + (f79*fvu1u1u6)/18 + 
      (f79*fvu1u1u7)/9 + ((-2*f79 - f94)*fvu1u1u8)/9 + 
      ((-13*f79 - 8*f94)*tci12)/18) + 
    fvu1u1u5^2*((3*f55 + 3*f59 + 3*f63 - 3*f68 - 3*f71 - f87 + 
        f97)/18 + ((-f79 + f94)*fvu1u1u6)/6 + (f79*fvu1u1u7)/9 + 
      ((-f79 + 3*f94)*fvu1u1u8)/18 + (f94*fvu1u1u9)/6 + 
      ((f79 - 3*f94)*tci12)/9) + fvu1u1u3^2*
     (((f79 + f94)*fvu1u1u4)/6 - (f94*fvu1u1u7)/6 + 
      ((2*f79 + f94)*fvu1u1u8)/6 + ((3*f79 - 2*f94)*tci12)/6) + 
    fvu2u1u3*((f82 + 6*f84 - f86 - 6*f90)/9 + 
      (2*(f79 - f94)*tci12)/9) + fvu1u1u6^2*
     ((6*f9 - f50 + 6*f53 + 3*f71 - f87)/18 + 
      ((-4*f79 - f94)*fvu1u1u7)/9 + ((-3*f79 + f94)*fvu1u1u8)/
       18 + ((6*f79 - f94)*tci12)/18) + 
    fvu1u1u2^2*((-3*f55 + f82 + 12*f84 - 2*f86 - 12*f90 - f97)/
       18 - (f94*fvu1u1u3)/6 + ((-f79 - 2*f94)*fvu1u1u4)/18 - 
      (f79*fvu1u1u5)/18 + (f79*fvu1u1u6)/6 - (f94*fvu1u1u7)/6 - 
      (f94*fvu1u1u9)/9 + ((2*f79 + f94)*tci12)/18) + 
    fvu1u1u7^2*(-f63/6 + ((f79 - f94)*fvu1u1u8)/9 + 
      ((4*f79 + f94)*tci12)/18) + 
    fvu1u1u8*((4*f79*fvu2u1u8)/9 + (2*(2*f79 + f94)*fvu2u1u9)/9 - 
      (2*(3*f79 - f94)*fvu2u1u10)/9 + (5*f79*tci11^2)/54 - 
      (2*(3*f79 - f94)*fvu1u2u8*tci12)/9) + 
    fvu1u1u7*((-16*f11 + 2*f14 - 20*f19 - 16*f21 + 2*f22 + 
        2*f24 + 16*f25 - 2*f28 - 16*f54 + 16*f55 + 2*f56 - 
        2*f57 + 2*f58 - 2*f59 + 16*f63 + 16*f64 - 2*f66 - 
        8*f68 + f69 - 8*f70 + 16*f72 + f73 - 2*f74 + 2*f104 - 
        16*f105 + 2*f106 - f120 + 16*f121 - 2*f122 - 16*f123 + 
        2*f125 + 8*f130 - f132 - 2*f157 + 8*f165 - f166 - 
        f176 + 16*f177 - 2*f178)/18 + 
      (2*(2*f79 - f94)*fvu1u1u8^2)/9 + (2*(f79 - 3*f94)*fvu2u1u7)/
       9 + (2*(5*f79 - f94)*fvu2u1u8)/9 - 
      (2*(3*f79 - f94)*fvu2u1u10)/9 + (2*(f79 - 3*f94)*tci11^2)/
       27 + ((-6*f9 + f50 - 6*f53)*tci12)/9 - 
      (2*(f79 - f94)*fvu1u1u8*tci12)/9 - 
      (2*(3*f79 - f94)*fvu1u2u8*tci12)/9) + 
    fvu1u1u4*(-f69/9 + ((-f79 + 3*f94)*fvu1u1u5^2)/18 + 
      (f79*fvu1u1u6^2)/9 + (f79*fvu1u1u7^2)/9 + 
      ((2*f79 + f94)*fvu1u1u8^2)/18 + (f94*fvu1u1u9^2)/18 - 
      (2*(f79 - f94)*fvu2u1u2)/9 - (2*(f79 - f94)*fvu2u1u3)/9 + 
      (4*f79*fvu2u1u8)/9 - (f68*tci12)/3 + 
      ((-2*f79 - f94)*fvu1u1u8*tci12)/9 - 
      (f94*fvu1u1u9*tci12)/9 - (2*(f79 - f94)*fvu1u2u6*
        tci12)/9 + fvu1u1u7*((6*f9 - f50 + 6*f53)/9 + 
        (2*f79*fvu1u1u8)/9 - (2*f79*tci12)/9) + 
      fvu1u1u6*(f82/9 - (2*f79*fvu1u1u7)/9 - (2*f79*fvu1u1u8)/9 + 
        (2*f79*tci12)/9) + fvu1u1u5*((6*f84 - f86 - 6*f90)/9 - 
        (f94*fvu1u1u9)/3 + ((f79 - 3*f94)*tci12)/9)) + 
    fvu1u1u3*(((f79 + f94)*fvu1u1u4^2)/6 + 
      ((-f79 - 2*f94)*fvu1u1u5^2)/6 - (f94*fvu1u1u7^2)/6 + 
      ((2*f79 + f94)*fvu1u1u8^2)/6 + (2*(f79 + f94)*fvu2u1u1)/3 + 
      ((f79 + 2*f94)*fvu2u1u5)/3 + ((-11*f79 + 4*f94)*tci11^2)/
       18 + ((f79 + 2*f94)*fvu1u1u5*tci12)/3 + 
      (f94*fvu1u1u7*tci12)/3 + ((-2*f79 - f94)*fvu1u1u8*
        tci12)/3 + ((-f79 - 2*f94)*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((-2*f79 - f94)*fvu1u1u8)/3 + 
        ((-f79 - f94)*tci12)/3)) + 
    fvu1u1u6*((-16*f3 + 2*f4 - 16*f11 + 2*f14 + 16*f15 - 
        2*f17 - 36*f19 - 16*f21 + 4*f22 + 2*f24 + 16*f25 - 
        2*f28 - 16*f31 + 2*f32 - 16*f54 + 2*f56 - 16*f58 - 
        4*f59 - 2*f60 - 16*f62 + 2*f65 - 8*f68 + f69 - 8*f70 - 
        16*f71 + f73 - 16*f77 + 2*f80 + 2*f104 - 16*f105 + 
        2*f106 + 16*f117 - 2*f118 - f120 + 32*f121 - 4*f122 - 
        2*f124 + 32*f127 - 4*f128 + 16*f129 + 8*f130 - 2*f131 - 
        f132 + 16*f148 - 2*f149 - 4*f157 + 16*f160 - 2*f161 + 
        8*f165 - f166 - f176 + 32*f180 - 4*f181)/18 - 
      (5*f79*fvu1u1u7^2)/9 + ((-7*f79 + f94)*fvu1u1u8^2)/18 + 
      (2*(7*f79 - f94)*fvu2u1u8)/9 - (2*(6*f79 - f94)*tci11^2)/
       27 - (f82*tci12)/9 + ((7*f79 - f94)*fvu1u1u8*tci12)/
       9 + fvu1u1u7*((-6*f9 + f50 - 6*f53)/9 + (2*f94*fvu1u1u8)/
         9 - (2*f94*tci12)/9)) + 
    fvu1u1u5*((19*f3 - 2*f4 + 16*f11 - 2*f14 - 8*f15 + f17 + 
        18*f19 + 16*f21 - 2*f22 - 2*f24 - 16*f25 + 2*f28 + 
        8*f31 - f32 + 16*f54 - 8*f55 - 2*f56 - 2*f58 - 7*f59 + 
        8*f62 - 8*f63 - 8*f64 - f65 + f66 + 8*f68 + 8*f70 + 
        8*f71 - 8*f72 - f73 + f74 + 8*f77 - f80 - f104 + 
        16*f105 - 2*f106 - 8*f117 + f118 - 24*f121 + 3*f122 + 
        8*f123 + 2*f124 - f125 - 16*f127 + 2*f128 - 8*f129 - 
        8*f130 + f131 + f132 - 8*f148 + f149 + 4*f157 - 
        8*f160 + f161 - 8*f165 + f166 + f176 - 8*f177 + f178 - 
        16*f180 + 2*f181)/9 + ((-f79 + f94)*fvu1u1u6^2)/6 + 
      (f94*fvu1u1u7^2)/3 + ((-f79 + 3*f94)*fvu1u1u8^2)/18 + 
      (f94*fvu1u1u9^2)/6 - (2*(f79 - 3*f94)*fvu2u1u4)/9 + 
      ((f79 + 2*f94)*fvu2u1u5)/3 + (2*(f79 - 3*f94)*fvu2u1u7)/9 + 
      ((-13*f79 + 54*f94)*tci11^2)/108 + 
      ((-6*f84 + f86 + 6*f90)*tci12)/9 + 
      ((f79 - 3*f94)*fvu1u1u8*tci12)/9 + 
      (f94*fvu1u1u9*tci12)/3 + ((-f79 - 2*f94)*fvu1u2u7*
        tci12)/3 + fvu1u1u7*(-f97/9 - (2*f79*fvu1u1u8)/9 + 
        (2*f79*tci12)/9) + fvu1u1u6*(f87/9 + 
        ((f79 - f94)*fvu1u1u8)/3 + ((-f79 + f94)*tci12)/3)) + 
    fvu1u1u2*(f57/9 + ((-f79 - f94)*fvu1u1u3^2)/6 + 
      ((f79 - 4*f94)*fvu1u1u4^2)/18 + ((f79 - 6*f94)*fvu1u1u5^2)/
       18 + (f79*fvu1u1u6^2)/6 - (f94*fvu1u1u7^2)/6 - 
      (2*f94*fvu1u1u9^2)/9 - (2*(f79 - f94)*fvu2u1u3)/9 - 
      (2*(f79 - 3*f94)*fvu2u1u4)/9 + (2*f94*fvu2u1u11)/9 + 
      ((f79 - 2*f94)*tci11^2)/9 - (f82*tci12)/9 - 
      (2*f94*fvu1u1u9*tci12)/9 - (2*(f79 - f94)*fvu1u2u6*
        tci12)/9 + fvu1u1u5*((-6*f84 + f86 + 6*f90)/9 - 
        (f79*tci12)/9) + fvu1u1u6*(-f82/9 + (f79*tci12)/3) + 
      fvu1u1u7*(f97/9 - (f94*tci12)/3) + 
      fvu1u1u3*((f94*fvu1u1u7)/3 + ((f79 + f94)*tci12)/3) + 
      fvu1u1u4*((-6*f84 + f86 + 6*f90)/9 + (f79*fvu1u1u5)/9 + 
        (2*f94*fvu1u1u9)/9 + ((f79 + 2*f94)*tci12)/9)) + 
    fvu1u1u1*((f79*fvu1u1u2^2)/6 + ((-3*f79 - 2*f94)*fvu1u1u3^2)/
       6 + ((-5*f79 - 4*f94)*fvu1u1u4^2)/18 + 
      ((f79 + 2*f94)*fvu1u1u5^2)/6 - (5*f79*fvu1u1u6^2)/18 + 
      (2*f79*fvu1u1u7^2)/9 - (2*(2*f79 + f94)*fvu1u1u8^2)/9 + 
      (2*(f79 + f94)*fvu2u1u1)/3 - (2*(f79 - f94)*fvu2u1u2)/9 - 
      (4*f79*fvu2u1u8)/9 + (2*(2*f79 + f94)*fvu2u1u9)/9 + 
      ((-11*f79 - 16*f94)*tci11^2)/54 + 
      (4*(2*f79 + f94)*fvu1u1u8*tci12)/9 + 
      fvu1u1u2*(-(f79*fvu1u1u6)/3 - (2*f79*tci12)/3) + 
      fvu1u1u6*((2*f79*fvu1u1u7)/9 + (2*f79*fvu1u1u8)/9 - 
        (5*f79*tci12)/9) + fvu1u1u7*((-2*f79*fvu1u1u8)/9 + 
        (2*f79*tci12)/9) + fvu1u1u3*((f79*fvu1u1u4)/3 + 
        ((3*f79 + 2*f94)*tci12)/3) + 
      fvu1u1u4*((2*f79*fvu1u1u6)/9 - (2*f79*fvu1u1u7)/9 + 
        (2*(2*f79 + f94)*fvu1u1u8)/9 + ((5*f79 + 4*f94)*tci12)/
         9)) + ((7*f79 + 8*f94)*tci12*tcr11^2)/18 + 
    ((-9*f79 - 4*f94)*tcr11^3)/18 + 
    ((3*f53 - 16*f79 - 3*f90 - 4*f94)*tci11^2*tcr12)/18 + 
    (10*(f79 + 2*f94)*tci12*tcr21)/9 + 
    tcr11*(((-f79 + 16*f94)*tci11^2)/54 + 
      (2*f79*tci12*tcr12)/3 - (2*(4*f79 + 5*f94)*tcr21)/
       9) + ((9*f53 + 26*f79 - 12*f85 - 9*f90 - 12*f94)*tcr33)/
     36;
L ieu1ueu7 = (-11*f3 + f4 + 10*f19 - f22 + 9*f58 + 
      10*f59 + f60 - f104 + f120 - f124 - f157)/9 + 
    ((f79 - 2*f90 - f94)*fvu1u1u2^2)/6 + 
    ((-f53 - f79)*fvu1u1u4^2)/6 + ((-f85 + f94)*fvu1u1u5^2)/6 + 
    ((f53 - f85)*fvu1u1u6^2)/6 + 
    fvu1u1u4*(-f68/3 - (f90*fvu1u1u5)/3 + (f79*fvu1u1u6)/3 + 
      (f53*fvu1u1u7)/3) + ((f79 - f90)*fvu2u1u3)/3 + 
    ((-f90 - f94)*fvu2u1u4)/3 + ((-f53 - f79)*fvu2u1u6)/3 + 
    ((-f85 + f94)*fvu2u1u7)/3 + ((f53 - f85)*fvu2u1u8)/3 + 
    ((-f53 - 4*f79 + f85 + f94)*tci11^2)/18 + 
    (f68*tci12)/3 + ((f53 + f79)*fvu1u2u3*tci12)/3 + 
    ((f79 - f90)*fvu1u2u6*tci12)/3 + 
    fvu1u1u7*(f63/3 - (f53*tci12)/3) + 
    fvu1u1u6*(-f71/3 - (f53*fvu1u1u7)/3 - (f79*tci12)/3) + 
    fvu1u1u2*(f55/3 + (f90*fvu1u1u4)/3 + (f90*fvu1u1u5)/3 - 
      (f79*fvu1u1u6)/3 + (f94*fvu1u1u7)/3 - (f79*tci12)/3) + 
    fvu1u1u5*((-f55 - f59 - f63 + f68 + f71)/3 + 
      (f85*fvu1u1u6)/3 - (f94*fvu1u1u7)/3 + (f90*tci12)/3);
L ieu0ueu7 = f59/3;
L ieum1ueu7 = 0;
L ieum2ueu7 = 0;
L ieu2uou7 = -(f76*fvu4u25) - f76*fvu4u28 - f76*fvu4u39 - 
    f76*fvu4u41 + (f76*fvu4u49)/3 - (f76*fvu4u51)/3 - 
    f76*fvu4u80 - f76*fvu4u83 - f76*fvu4u91 - f76*fvu4u93 + 
    (f76*fvu4u100)/3 - (f76*fvu4u102)/3 + (7*f76*fvu4u171)/4 + 
    (5*f76*fvu4u174)/4 - (3*f76*fvu4u182)/4 - (f76*fvu4u184)/4 - 
    (f76*fvu4u190)/3 - (f76*fvu4u192)/2 + (f76*fvu4u199)/4 + 
    (f76*fvu4u201)/2 + f76*fvu4u202 - 2*f76*fvu4u213 + 
    (2*f76*fvu4u225)/3 + 7*f76*fvu4u244 - (3*f76*fvu4u252)/2 - 
    (7*f76*fvu4u255)/6 + (f76*fvu4u271)/4 - (17*f76*fvu4u273)/4 + 
    (17*f76*fvu4u277)/4 + (f76*fvu4u279)/4 - (f76*fvu4u282)/3 - 
    (f76*fvu4u289)/4 - (f76*fvu4u290)/2 - f76*fvu4u291 - 
    2*f76*fvu4u293 - 2*f76*fvu4u309 + 2*f76*fvu4u325 - 
    2*f76*fvu4u330 + ((f76*fvu3u45)/2 - (f76*fvu3u70)/2 - 
      (f76*fvu3u81)/2)*fvu1u1u1 + fvu3u78*(-(f76*fvu1u1u4) - 
      f76*fvu1u1u5 + f76*fvu1u1u6) + 
    fvu3u25*((f76*fvu1u1u1)/2 - (f76*fvu1u1u3)/2 + 
      (f76*fvu1u1u6)/2 - (f76*fvu1u1u7)/2) + 
    fvu3u81*((f76*fvu1u1u3)/2 + (f76*fvu1u1u6)/2 - 
      (f76*fvu1u1u7)/2) + fvu3u23*((f76*fvu1u1u2)/2 + 
      (f76*fvu1u1u6)/2 - (f76*fvu1u1u7)/2 - (f76*fvu1u1u9)/2) + 
    fvu3u70*((f76*fvu1u1u2)/2 - (f76*fvu1u1u3)/2 + 
      f76*fvu1u1u4 + f76*fvu1u1u5 + f76*fvu1u1u7 - 
      (f76*fvu1u1u9)/2) + fvu3u45*(-(f76*fvu1u1u2)/2 - 
      (f76*fvu1u1u3)/2 + (f76*fvu1u1u9)/2) + 
    fvu3u43*(-(f76*fvu1u1u6)/2 + (f76*fvu1u1u7)/2 + 
      (f76*fvu1u1u9)/2) + (151*f76*tci11^3*tci12)/135 + 
    fvu3u71*(f76*fvu1u1u4 - f76*fvu1u1u5 - f76*fvu1u1u6 - 
      2*f76*tci12) + fvu3u82*(f76*fvu1u1u4 - f76*fvu1u1u5 - 
      f76*fvu1u1u6 - 2*f76*tci12) + 
    fvu3u80*((-5*f76*fvu1u1u6)/2 + (5*f76*fvu1u1u7)/2 - 
      (7*f76*fvu1u1u8)/2 + (5*f76*fvu1u1u9)/2 + 
      (7*f76*tci12)/2) + (2*f76*tci11^2*tci21)/9 + 
    fvu1u1u4*((4*f76*tci11^3)/27 - 4*f76*tci12*tci21) + 
    fvu1u1u5*((4*f76*tci11^3)/27 - 4*f76*tci12*tci21) + 
    fvu1u1u3*(f76*fvu3u71 + f76*fvu3u78 + f76*fvu3u82 - 
      (4*f76*tci11^3)/27 + 4*f76*tci12*tci21) + 
    fvu1u1u8*((-7*f76*tci11^3)/27 + 7*f76*tci12*tci21) + 
    tci12*((48*f76*tci31)/5 + 16*f76*tci32) + 
    (4*f76*tci41)/3 + (4*f76*tci11^3*tcr11)/27 - 
    (f76*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u7*((19*f76*tci11^3)/60 - (23*f76*tci12*tci21)/
       3 + (12*f76*tci31)/5 + f76*tci21*tcr11 - 
      (f76*tci11*tcr11^2)/20) + 
    fvu1u1u9*((91*f76*tci11^3)/540 - (11*f76*tci12*tci21)/
       3 + (12*f76*tci31)/5 + f76*tci21*tcr11 - 
      (f76*tci11*tcr11^2)/20) + 
    fvu1u1u2*(-(f76*fvu3u43)/2 + f76*fvu3u80 + 
      (49*f76*tci11^3)/540 - (10*f76*tci12*tci21)/3 - 
      (12*f76*tci31)/5 - f76*tci21*tcr11 + 
      (f76*tci11*tcr11^2)/20) + 
    fvu1u1u6*((-19*f76*tci11^3)/60 + (23*f76*tci12*tci21)/
       3 - (12*f76*tci31)/5 - f76*tci21*tcr11 + 
      (f76*tci11*tcr11^2)/20) - 
    (40*f76*tci12*tci21*tcr12)/3 - 4*f76*tci11*tci12*
     tcr12^2;
L ieu1uou7 = 2*f76*fvu3u70 + 
    (4*f76*tci11^3)/27 - 4*f76*tci12*tci21;
L ieu0uou7 = 0;
L ieum1uou7 = 0;
L ieum2uou7 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou7+w^2*ieu1uou7+w^3*ieu0uou7+w^4*ieum1uou7+w^5*ieum2uou7;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou7a = K[w^1];
L ieu1uou7a = K[w^2];
L ieu0uou7a = K[w^3];
L ieum1uou7a = K[w^4];
L ieum2uou7a = K[w^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_odd.c> "%O"
#write <e7_odd.c> "return Eps5o2<T>("
#write <e7_odd.c> "%E", ieu2uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieu0uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum1uou7a
#write <e7_odd.c> ", "
#write <e7_odd.c> "%E", ieum2uou7a
#write <e7_odd.c> ");\n}"
L H=+u^1*ieu2ueu7+u^2*ieu1ueu7+u^3*ieu0ueu7+u^4*ieum1ueu7+u^5*ieum2ueu7;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu7a = H[u^1];
L ieu1ueu7a = H[u^2];
L ieu0ueu7a = H[u^3];
L ieum1ueu7a = H[u^4];
L ieum2ueu7a = H[u^5];
.sort
#write <e7.tmp> "`optimmaxvar_'"
#write <e7_even.c> "%O"
#write <e7_even.c> "return Eps5o2<T>("
#write <e7_even.c> "%E", ieu2ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieu0ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum1ueu7a
#write <e7_even.c> ", "
#write <e7_even.c> "%E", ieum2ueu7a
#write <e7_even.c> ");\n}"
.end
