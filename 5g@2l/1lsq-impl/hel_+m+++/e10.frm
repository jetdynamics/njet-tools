#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f66;
S f180;
S f65;
S f64;
S f181;
S f186;
S f132;
S f62;
S f130;
S f185;
S f60;
S f69;
S f68;
S f124;
S f77;
S f127;
S f120;
S f121;
S f122;
S f128;
S f151;
S f150;
S f152;
S f155;
S f157;
S f156;
S f159;
S f158;
S f148;
S f149;
S f19;
S f3;
S f80;
S f17;
S f147;
S f14;
S f15;
S f11;
S f4;
S f28;
S f22;
S f172;
S f21;
S f171;
S f176;
S f25;
S f24;
S f39;
S f115;
S f31;
S f32;
S f162;
S f41;
S f164;
S f165;
S f166;
S f58;
S f59;
S f102;
S f56;
S f57;
S f54;
S f104;
S f55;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu10 = (-729*f3 + 74*f4 + 11*f19 + 641*f58 + 
      11*f59 + 63*f60 - 74*f104 + 63*f120 - 63*f124 - 74*f157 - 
      74*f176)/27 - (f156*fvu3u1)/3 + (f156*fvu3u2)/6 + 
    (2*f156*fvu3u3)/9 + (11*f156*fvu3u5)/18 - (f156*fvu3u6)/9 + 
    (f156*fvu3u7)/3 + (16*f156*fvu3u8)/9 - (f156*fvu3u10)/6 + 
    (2*f156*fvu3u11)/9 + (2*f156*fvu3u12)/9 - (f156*fvu3u13)/18 + 
    (f156*fvu3u14)/3 + (f156*fvu3u17)/6 + (f156*fvu3u18)/6 - 
    (f156*fvu3u19)/9 - (2*f156*fvu3u24)/9 + 
    ((-f156 + 4*f164)*fvu3u31)/9 + ((3*f162 + 14*f164)*fvu3u32)/
     18 + (f164*fvu3u33)/2 + (16*f164*fvu3u34)/9 + 
    (2*f164*fvu3u36)/9 + ((-f156 - 2*f164)*fvu3u37)/18 - 
    (f156*fvu3u38)/6 + (f164*fvu3u40)/3 + (f156*fvu3u42)/3 - 
    (2*f164*fvu3u44)/9 + (2*f164*fvu3u51)/9 + (f164*fvu3u52)/6 + 
    (f164*fvu3u53)/6 - (f164*fvu3u56)/18 - (f147*fvu3u57)/6 + 
    ((-f159 + f164)*fvu3u59)/6 + (f156*fvu1u1u1^3)/2 + 
    (11*f156*fvu1u1u2^3)/27 + ((f156 - 2*f164)*fvu1u1u3^3)/18 + 
    (f164*fvu1u1u4^3)/3 - (2*(f156 + f164)*fvu1u1u5^3)/9 - 
    (35*f164*fvu1u1u6^3)/27 + ((3*f156 - 4*f164)*fvu1u1u7^3)/27 + 
    ((4*f156 - 3*f164)*fvu1u1u9^3)/27 + 
    fvu1u1u8^2*((6*f102 - f115 + 6*f147 - 3*f151 + f158)/18 + 
      (f156*fvu1u1u9)/9) + ((6*f102 - f115 + 6*f147 + f158)*
      fvu2u1u12)/9 + ((6*f159 + 6*f164 - 6*f171 + f172 - 6*f185 + 
       f186)*fvu2u1u14)/9 + ((-6*f102 + f115 - 6*f147 + 6*f159 - 
       6*f185 + f186)*fvu2u1u15)/9 - (2*f164*fvu1u2u2*tci11^2)/
     9 + (2*f164*fvu1u2u3*tci11^2)/3 - 
    (2*f156*fvu1u2u6*tci11^2)/3 - (f156*fvu1u2u7*tci11^2)/3 + 
    ((-19*f3 + 2*f4 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 9*f19 - 
       8*f21 + f22 + f24 + 16*f25 - 2*f28 - 8*f31 + f32 - 
       16*f54 + 2*f56 + 11*f58 - f59 + f60 - 16*f62 + 2*f65 - 
       8*f68 + f69 - f104 + 2*f120 + 8*f121 - f122 - 2*f124 + 
       16*f127 - 2*f128 + 8*f130 - f132 - 8*f151 - 4*f157 + 
       8*f165 - f166 - f176 + 8*f180 - f181)*tci12)/9 + 
    ((-6*f102 + f115 - 6*f147 - f158)*fvu1u2u9*tci12)/9 - 
    (4*f156*fvu2u1u2*tci12)/9 - (2*f156*fvu2u1u3*tci12)/3 + 
    (f156*fvu2u1u5*tci12)/3 - (2*f164*fvu2u1u6*tci12)/3 - 
    (16*f156*fvu2u1u9*tci12)/9 - (4*f156*fvu2u1u11*tci12)/9 + 
    (2*f164*fvu2u1u13*tci12)/9 + fvu1u1u1^2*
     (-(f156*fvu1u1u2)/9 - (f156*fvu1u1u3)/6 - 
      (f156*fvu1u1u4)/9 + (f164*fvu1u1u6)/9 + 
      ((-3*f156 - f164)*fvu1u1u7)/9 + (f156*fvu1u1u8)/18 - 
      (f156*fvu1u1u9)/18 - (23*f156*tci12)/18) + 
    fvu1u1u2^2*((f156*fvu1u1u4)/9 - (f156*fvu1u1u5)/9 + 
      (f156*fvu1u1u8)/9 + (2*f156*fvu1u1u9)/9 - 
      (4*f156*tci12)/9) + fvu1u2u8*
     ((-2*(f156 + 4*f164)*tci11^2)/9 + 
      ((6*f39 - f41 - f158 + 6*f162)*tci12)/9) + 
    tci11^2*((-30*f3 + 3*f4 + 30*f58 + 3*f60 + 12*f102 - 
        3*f104 - 2*f115 + 3*f120 - 3*f124 + 12*f147 - 18*f151 - 
        3*f157 + 8*f158 - 12*f159 + 12*f164 - 12*f171 + 2*f172 - 
        3*f176 + 12*f185 - 2*f186)/108 + 
      ((3*f147 + 7*f156 - 3*f162 - 12*f164)*tci12)/108) + 
    fvu1u1u7^2*((6*f39 - f41 + 3*f148 - f158 + 6*f162)/18 + 
      ((f156 - 4*f164)*fvu1u1u8)/18 - (f156*fvu1u1u9)/6 - 
      (f164*fvu1u1u10)/6 + ((-2*f156 - 3*f164)*tci12)/18) + 
    fvu1u1u9^2*((-6*f102 + f115 - 6*f147 + 3*f152 + 6*f159 - 
        6*f185 + f186)/18 - (f164*fvu1u1u10)/9 + 
      ((-10*f156 - f164)*tci12)/18) + 
    fvu1u1u5^2*((f164*fvu1u1u6)/3 + ((f156 - 4*f164)*fvu1u1u7)/
       18 + ((f156 + 2*f164)*fvu1u1u8)/18 + (f156*fvu1u1u9)/6 + 
      ((-f156 - f164)*tci12)/9) + fvu1u1u6^2*
     ((-30*f3 + 3*f4 - 6*f39 + f41 + 30*f58 + 3*f60 - 3*f104 + 
        3*f120 - 3*f124 - 3*f148 - 3*f150 + 3*f151 - 3*f152 - 
        3*f157 + 6*f159 - 6*f162 + 12*f164 - 12*f171 + 2*f172 - 
        3*f176 - 6*f185 + f186)/18 + (4*f164*fvu1u1u7)/9 + 
      (f164*fvu1u1u8)/3 + (f164*fvu1u1u9)/6 - (f164*tci12)/2) + 
    fvu1u1u4^2*((f156*fvu1u1u5)/18 + (f164*fvu1u1u6)/9 - 
      (5*f164*fvu1u1u7)/18 + (2*f156*fvu1u1u8)/9 + 
      (7*f156*fvu1u1u9)/18 - (f164*fvu1u1u10)/6 - 
      (f164*tci12)/3) + fvu1u1u10^2*
     (f150/6 + (f164*tci12)/6) + 
    fvu2u1u8*((-6*f39 + f41 - 6*f162 + 6*f164 - 6*f171 + f172)/
       9 + (4*f164*tci12)/9) + fvu1u1u3^2*((f164*fvu1u1u6)/6 + 
      (f156*fvu1u1u7)/6 + (f156*fvu1u1u8)/6 + 
      (f164*fvu1u1u9)/18 - (f164*fvu1u1u10)/9 + 
      ((-9*f156 + 2*f164)*tci12)/18) + 
    fvu2u1u10*((6*f39 - f41 - f158 + 6*f162)/9 - 
      (2*(f156 + 4*f164)*tci12)/9) + 
    fvu1u1u10*((8*f11 - f14 - 8*f55 + f57 - 8*f64 + f66 + 
        8*f68 - f69 - 8*f77 + f80 - 8*f150)/9 - 
      (2*f164*fvu2u1u13)/9 - (2*f164*fvu2u1u14)/3 - 
      (2*f164*fvu2u1u15)/9 - (7*f164*tci11^2)/27 + 
      ((6*f102 - f115 + 6*f147)*tci12)/9 + 
      (2*f164*fvu1u2u2*tci12)/9) + 
    fvu1u1u8*((19*f3 - 2*f4 + 16*f11 - 2*f14 - 16*f15 + 2*f17 + 
        9*f19 + 8*f21 - f22 - f24 - 16*f25 + 2*f28 + 8*f31 - 
        f32 + 16*f54 - 2*f56 - 11*f58 + f59 - f60 + 16*f62 - 
        2*f65 + 8*f68 - f69 + f104 - 2*f120 - 8*f121 + f122 + 
        2*f124 - 16*f127 + 2*f128 - 8*f130 + f132 + 8*f151 + 
        4*f157 - 8*f165 + f166 + f176 - 8*f180 + f181)/9 + 
      (f156*fvu1u1u9^2)/9 + ((-6*f102 + f115 - 6*f147)*fvu1u1u10)/
       9 - (4*f164*fvu2u1u8)/9 + (8*f156*fvu2u1u9)/9 + 
      (2*(f156 + 4*f164)*fvu2u1u10)/9 + (4*f156*fvu2u1u11)/9 + 
      ((3*f156 - 4*f164)*tci11^2)/54 + (f151*tci12)/3 + 
      (2*(f156 + 4*f164)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*(-f158/9 - (2*f156*tci12)/9)) + 
    fvu1u1u2*((-4*f156*fvu1u1u4^2)/9 - (2*f156*fvu1u1u5^2)/9 - 
      (f156*fvu1u1u8^2)/9 - (5*f156*fvu1u1u9^2)/9 + 
      (2*f156*fvu2u1u3)/3 + (2*f156*fvu2u1u4)/9 + 
      (10*f156*fvu2u1u11)/9 - (2*f156*tci11^2)/27 - 
      (2*f156*fvu1u1u5*tci12)/9 + (4*f156*fvu1u1u9*tci12)/9 + 
      (2*f156*fvu1u2u6*tci12)/3 + fvu1u1u4*
       ((2*f156*fvu1u1u5)/9 - (2*f156*fvu1u1u8)/9 - 
        (4*f156*fvu1u1u9)/9 + (2*f156*tci12)/9) + 
      fvu1u1u8*((-2*f156*fvu1u1u9)/9 + (2*f156*tci12)/9)) + 
    fvu1u1u9*((-8*f11 + f14 + 8*f55 - f57 - 8*f152 + 8*f165 - 
        f166)/9 + (14*f156*fvu2u1u11)/9 - (2*f164*fvu2u1u15)/9 + 
      (2*(6*f156 + f164)*tci11^2)/27 + (f158*tci12)/9 + 
      fvu1u1u10*((6*f102 - f115 + 6*f147)/9 - (2*f164*tci12)/
         9)) + fvu1u1u3*((f156*fvu1u1u5^2)/6 + 
      (f164*fvu1u1u6^2)/6 - (f156*fvu1u1u4*fvu1u1u8)/3 + 
      (f156*fvu1u1u8^2)/6 + (f164*fvu1u1u9^2)/18 - 
      (f156*fvu2u1u5)/3 - (2*f164*fvu2u1u13)/9 + 
      ((12*f156 - f164)*tci11^2)/18 - (f156*fvu1u1u5*tci12)/
       3 - (f156*fvu1u1u7*tci12)/3 - (f156*fvu1u1u8*tci12)/
       3 + (2*f164*fvu1u2u2*tci12)/9 + (f156*fvu1u2u7*tci12)/
       3 + fvu1u1u6*(-(f164*fvu1u1u9)/3 - (f164*tci12)/3) + 
      fvu1u1u9*((2*f164*fvu1u1u10)/9 - (f164*tci12)/9)) + 
    fvu1u1u1*((-4*f156*fvu1u1u2^2)/9 - (f156*fvu1u1u3^2)/6 - 
      (2*f156*fvu1u1u4^2)/9 - (f156*fvu1u1u5^2)/6 + 
      (4*f164*fvu1u1u6^2)/9 + ((-3*f156 - 4*f164)*fvu1u1u7^2)/18 - 
      (7*f156*fvu1u1u8^2)/18 + (f156*fvu1u1u9^2)/18 + 
      (2*f156*fvu2u1u2)/9 + (4*f164*fvu2u1u8)/9 + 
      (8*f156*fvu2u1u9)/9 - (4*f156*fvu2u1u11)/9 + 
      (10*f156*tci11^2)/27 + (5*f156*fvu1u1u9*tci12)/9 + 
      fvu1u1u2*((2*f156*fvu1u1u4)/9 + (2*f156*fvu1u1u8)/9 + 
        (2*f156*fvu1u1u9)/9 - (2*f156*tci12)/9) + 
      fvu1u1u3*((f156*fvu1u1u4)/3 + (f156*tci12)/3) + 
      fvu1u1u4*((-2*f164*fvu1u1u6)/9 + (2*f164*fvu1u1u7)/9 - 
        (f156*fvu1u1u8)/9 - (2*f156*fvu1u1u9)/9 + 
        (4*f156*tci12)/9) + fvu1u1u8*((-2*f156*fvu1u1u9)/9 + 
        (7*f156*tci12)/9) + fvu1u1u7*((2*f164*fvu1u1u8)/9 + 
        (f156*fvu1u1u9)/3 + (2*(3*f156 - f164)*tci12)/9) + 
      fvu1u1u6*((-2*f164*fvu1u1u7)/9 - (2*f164*fvu1u1u8)/9 + 
        (2*f164*tci12)/9)) + fvu1u1u6*
     ((80*f3 - 8*f4 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 
        10*f19 - 8*f21 + f22 + f24 + 16*f25 - 2*f28 - 8*f31 + 
        f32 - 16*f54 + 2*f56 - 80*f58 - 2*f59 - 8*f60 - 
        16*f62 + 8*f64 + 2*f65 - f66 - 16*f68 + 2*f69 + 8*f77 - 
        f80 + 9*f104 - 7*f120 + 8*f121 - f122 + 7*f124 + 
        16*f127 - 2*f128 + 8*f130 - f132 + f149 + 8*f150 - 
        8*f151 + 8*f152 + 6*f157 + 9*f176 + 8*f180 - f181)/9 + 
      ((-6*f164 + 6*f171 - f172)*fvu1u1u7)/9 + 
      (8*f164*fvu1u1u7^2)/9 + (5*f164*fvu1u1u8^2)/9 + 
      (f164*fvu1u1u9^2)/6 + ((-6*f164 + 6*f171 - f172)*fvu1u1u10)/
       9 + (f164*fvu1u1u10^2)/3 + (2*f164*fvu2u1u6)/3 - 
      (20*f164*fvu2u1u8)/9 - (2*f164*fvu2u1u14)/3 + 
      f164*tci11^2 + ((-6*f39 + f41 - 6*f162)*tci12)/9 - 
      (2*f164*fvu1u2u3*tci12)/3 + fvu1u1u8*
       ((6*f39 - f41 + 6*f162)/9 - (10*f164*tci12)/9) + 
      fvu1u1u9*((-6*f159 + 6*f185 - f186)/9 + (f164*tci12)/
         3)) + fvu1u1u4*((f156*fvu1u1u5^2)/18 - 
      (4*f164*fvu1u1u6^2)/9 - (5*f164*fvu1u1u7^2)/18 + 
      (2*f156*fvu1u1u8^2)/9 + (7*f156*fvu1u1u9^2)/18 - 
      (f164*fvu1u1u10^2)/6 + (2*f156*fvu2u1u2)/9 + 
      (2*f156*fvu2u1u3)/3 + (2*f164*fvu2u1u6)/3 - 
      (4*f164*fvu2u1u8)/9 + (4*f156*fvu2u1u11)/9 + 
      ((-2*f156 + 9*f164)*tci11^2)/54 - 
      (7*f156*fvu1u1u9*tci12)/9 + (f164*fvu1u1u10*tci12)/3 - 
      (2*f164*fvu1u2u3*tci12)/3 + (2*f156*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*((2*f156*fvu1u1u9)/9 - (4*f156*tci12)/9) + 
      fvu1u1u5*(-(f156*fvu1u1u9)/3 - (f156*tci12)/9) + 
      fvu1u1u6*((2*f164*fvu1u1u7)/9 + (2*f164*fvu1u1u8)/9 + 
        (4*f164*tci12)/9) + fvu1u1u7*((-2*f164*fvu1u1u8)/9 + 
        (f164*fvu1u1u10)/3 + (5*f164*tci12)/9)) + 
    fvu1u1u5*((f164*fvu1u1u6^2)/3 + (f156*fvu1u1u7^2)/6 + 
      ((f156 + 2*f164)*fvu1u1u8^2)/18 + (f156*fvu1u1u9^2)/6 + 
      (2*f156*fvu2u1u4)/9 - (f156*fvu2u1u5)/3 - 
      (2*(f156 + 2*f164)*fvu2u1u7)/9 + 
      ((13*f156 + 16*f164)*tci11^2)/108 + 
      ((-f156 - 2*f164)*fvu1u1u8*tci12)/9 + 
      (f156*fvu1u1u9*tci12)/3 + (f156*fvu1u2u7*tci12)/3 + 
      fvu1u1u7*(((-f156 + 4*f164)*fvu1u1u8)/9 + 
        ((f156 - 4*f164)*tci12)/9) + 
      fvu1u1u6*((-2*f164*fvu1u1u8)/3 + (2*f164*tci12)/3)) + 
    fvu1u1u7*(-f149/9 + ((-f156 - 12*f164)*fvu1u1u8^2)/18 - 
      (f156*fvu1u1u9^2)/6 - (f164*fvu1u1u10^2)/6 - 
      (2*(f156 + 2*f164)*fvu2u1u7)/9 - (16*f164*fvu2u1u8)/9 + 
      (2*(f156 + 4*f164)*fvu2u1u10)/9 + 
      ((-3*f156 - 8*f164)*tci11^2)/27 + (f158*tci12)/9 + 
      (2*(f156 + 4*f164)*fvu1u2u8*tci12)/9 + 
      fvu1u1u9*(f158/9 - (f156*tci12)/3) + 
      fvu1u1u10*((6*f164 - 6*f171 + f172)/9 - (f164*tci12)/3) + 
      fvu1u1u8*((-6*f39 + f41 - 6*f162)/9 + 
        ((-f156 + 4*f164)*tci12)/9)) + 
    (29*f156*tci12*tcr11^2)/18 - (f156*tcr11^3)/2 + 
    ((-3*f147 + 16*f156 + 3*f162 + 8*f164)*tci11^2*tcr12)/
     18 + (26*f156*tci12*tcr21)/9 + 
    tcr11*((-4*f156*tci11^2)/27 - (2*f156*tci12*tcr12)/
       3 - (10*f156*tcr21)/9) + 
    ((-9*f147 - 26*f156 + 12*f159 + 9*f162 - 12*f164)*tcr33)/
     36;
L ieu1ueu10 = 
   (-99*f3 + 10*f4 + f19 + 91*f58 + f59 + 9*f60 - 10*f104 + 
      9*f120 - 9*f124 - 10*f157 - 10*f176)/9 + 
    ((f159 - f162 + 2*f164)*fvu1u1u6^2)/6 + 
    ((-f156 + f162)*fvu1u1u7^2)/6 + ((f147 + f156)*fvu1u1u8^2)/
     6 + ((-f147 + f159)*fvu1u1u9^2)/6 + 
    fvu1u1u8*(f151/3 - (f156*fvu1u1u9)/3 - (f147*fvu1u1u10)/3) + 
    ((-f162 + f164)*fvu2u1u8)/3 + ((-f156 + f162)*fvu2u1u10)/3 + 
    ((f147 + f156)*fvu2u1u12)/3 + ((f159 + f164)*fvu2u1u14)/3 + 
    ((-f147 + f159)*fvu2u1u15)/3 + 
    ((f147 + 4*f156 - f159 + f164)*tci11^2)/18 - 
    (f151*tci12)/3 + ((-f156 + f162)*fvu1u2u8*tci12)/3 + 
    ((-f147 - f156)*fvu1u2u9*tci12)/3 + 
    fvu1u1u10*(-f150/3 + (f147*tci12)/3) + 
    fvu1u1u9*(-f152/3 + (f147*fvu1u1u10)/3 + (f156*tci12)/3) + 
    fvu1u1u7*(-f148/3 - (f162*fvu1u1u8)/3 + (f156*fvu1u1u9)/3 + 
      (f164*fvu1u1u10)/3 + (f156*tci12)/3) + 
    fvu1u1u6*((10*f3 - f4 - 10*f58 - f60 + f104 - f120 + 
        f124 + f148 + f150 - f151 + f152 + f157 + f176)/3 - 
      (f164*fvu1u1u7)/3 + (f162*fvu1u1u8)/3 - (f159*fvu1u1u9)/3 - 
      (f164*fvu1u1u10)/3 - (f162*tci12)/3);
L ieu0ueu10 = (-10*f3 + f4 + 10*f58 + f60 - f104 + 
     f120 - f124 - f157 - f176)/3;
L ieum1ueu10 = 0;
L ieum2ueu10 = 0;
L ieu2uou10 = (-2*f155*fvu4u28)/3 - (2*f155*fvu4u39)/3 + 
    (2*f155*fvu4u49)/9 - (2*f155*fvu4u51)/9 + (f155*fvu4u80)/12 - 
    (17*f155*fvu4u83)/12 - (3*f155*fvu4u91)/4 + (f155*fvu4u93)/12 + 
    (4*f155*fvu4u100)/9 - (7*f155*fvu4u102)/6 + (f155*fvu4u111)/4 + 
    (f155*fvu4u113)/2 + f155*fvu4u114 - (2*f155*fvu4u129)/3 + 
    (13*f155*fvu4u132)/3 - (7*f155*fvu4u139)/2 - 
    (2*f155*fvu4u141)/3 + (4*f155*fvu4u146)/9 + (f155*fvu4u148)/2 + 
    (2*f155*fvu4u174)/3 + (2*f155*fvu4u182)/3 - 
    (2*f155*fvu4u190)/9 + (2*f155*fvu4u192)/9 - (f155*fvu4u213)/12 - 
    (43*f155*fvu4u215)/12 + (17*f155*fvu4u219)/4 - 
    (f155*fvu4u221)/12 - (4*f155*fvu4u225)/9 - (f155*fvu4u233)/4 - 
    (f155*fvu4u234)/2 - f155*fvu4u235 + (14*f155*fvu4u244)/3 + 
    (2*f155*fvu4u246)/3 - (4*f155*fvu4u252)/3 - 
    (10*f155*fvu4u255)/9 - 2*f155*fvu4u273 + 2*f155*fvu4u277 + 
    fvu3u25*(-(f155*fvu1u1u3)/3 + (f155*fvu1u1u6)/3 - 
      (f155*fvu1u1u7)/3) + fvu3u71*((f155*fvu1u1u3)/3 - 
      (f155*fvu1u1u6)/3 + (f155*fvu1u1u7)/3) + 
    fvu3u63*((5*f155*fvu1u1u3)/2 + (2*f155*fvu1u1u5)/3 + 
      (13*f155*fvu1u1u6)/6 + (f155*fvu1u1u7)/3 - 
      (f155*fvu1u1u8)/3 - (2*f155*fvu1u1u9)/3 - 
      (5*f155*fvu1u1u10)/2) + fvu3u45*(-(f155*fvu1u1u3)/3 - 
      (2*f155*fvu1u1u5)/3 - (f155*fvu1u1u6)/3 + f155*fvu1u1u7 + 
      (2*f155*fvu1u1u9)/3 - (2*f155*fvu1u1u10)/3) + 
    fvu3u23*((f155*fvu1u1u1)/3 + (f155*fvu1u1u6)/3 - 
      (f155*fvu1u1u8)/3 - (f155*fvu1u1u10)/3) + 
    fvu3u43*(-(f155*fvu1u1u2)/3 - (f155*fvu1u1u4)/3 - 
      (f155*fvu1u1u6)/3 + (f155*fvu1u1u7)/3 + 
      (2*f155*fvu1u1u9)/3 - (f155*fvu1u1u10)/3) + 
    fvu3u62*(-(f155*fvu1u1u2)/3 - (f155*fvu1u1u4)/3 + 
      (2*f155*fvu1u1u5)/3 - (f155*fvu1u1u6)/3 - 
      (f155*fvu1u1u7)/3 + (f155*fvu1u1u10)/3) + 
    fvu3u70*(-(f155*fvu1u1u6)/3 + (f155*fvu1u1u8)/3 + 
      (f155*fvu1u1u10)/3) + (151*f155*tci11^3*tci12)/135 + 
    fvu3u78*((f155*fvu1u1u2)/3 - (f155*fvu1u1u3)/3 + 
      (f155*fvu1u1u4)/3 - (f155*fvu1u1u6)/3 + (f155*fvu1u1u8)/3 - 
      (2*f155*fvu1u1u9)/3 - (2*f155*tci12)/3) + 
    fvu3u80*((f155*fvu1u1u2)/3 + (f155*fvu1u1u3)/3 + 
      (f155*fvu1u1u4)/3 - (2*f155*fvu1u1u5)/3 - 
      (4*f155*fvu1u1u6)/3 + 2*f155*fvu1u1u7 - 
      (4*f155*fvu1u1u8)/3 + 2*f155*fvu1u1u9 + f155*fvu1u1u10 + 
      (4*f155*tci12)/3) + (2*f155*tci11^2*tci21)/9 + 
    fvu1u1u8*((-8*f155*tci11^3)/81 + (8*f155*tci12*tci21)/
       3) + tci12*((48*f155*tci31)/5 + 16*f155*tci32) + 
    (4*f155*tci41)/3 + ((35*f155*tci11^3)/108 - 
      (f155*tci12*tci21)/3 + 12*f155*tci31)*tcr11 + 
    (-(f155*tci11*tci12)/5 + 5*f155*tci21)*tcr11^2 - 
    (f155*tci11*tcr11^3)/4 + 
    fvu1u1u3*((41*f155*tci11^3)/324 + f155*tci12*tci21 + 
      12*f155*tci31 + 5*f155*tci21*tcr11 - 
      (f155*tci11*tcr11^2)/4) + 
    fvu1u1u6*((f155*tci11^3)/36 + (11*f155*tci12*tci21)/3 + 
      12*f155*tci31 + 5*f155*tci21*tcr11 - 
      (f155*tci11*tcr11^2)/4) + 
    fvu1u1u5*(-(f155*tci11^3)/45 + (16*f155*tci12*tci21)/
       9 + (16*f155*tci31)/5 + (4*f155*tci21*tcr11)/3 - 
      (f155*tci11*tcr11^2)/15) + 
    fvu1u1u7*((89*f155*tci11^3)/810 - (32*f155*tci12*tci21)/
       9 - (8*f155*tci31)/5 - (2*f155*tci21*tcr11)/3 + 
      (f155*tci11*tcr11^2)/30) + 
    fvu1u1u9*((23*f155*tci11^3)/135 - (52*f155*tci12*tci21)/
       9 - (16*f155*tci31)/5 - (4*f155*tci21*tcr11)/3 + 
      (f155*tci11*tcr11^2)/15) + 
    fvu1u1u10*((17*f155*tci11^3)/1620 - 
      (37*f155*tci12*tci21)/9 - (52*f155*tci31)/5 - 
      (13*f155*tci21*tcr11)/3 + (13*f155*tci11*tcr11^2)/
       60) + fvu1u1u1*((f155*fvu3u25)/3 + (f155*fvu3u43)/3 + 
      (f155*fvu3u45)/3 + (f155*fvu3u62)/3 - (13*f155*fvu3u63)/6 - 
      (f155*fvu3u70)/3 - (f155*fvu3u71)/3 + (f155*fvu3u78)/3 - 
      (2*f155*fvu3u80)/3 - (19*f155*tci11^3)/108 + 
      (f155*tci12*tci21)/3 - 12*f155*tci31 - 
      5*f155*tci21*tcr11 + (f155*tci11*tcr11^2)/4) - 
    (40*f155*tci12*tci21*tcr12)/3 - 
    4*f155*tci11*tci12*tcr12^2;
L ieu1uou10 = 
   2*f155*fvu3u80 + (4*f155*tci11^3)/27 - 
    4*f155*tci12*tci21;
L ieu0uou10 = 0;
L ieum1uou10 = 0;
L ieum2uou10 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou10+w^2*ieu1uou10+w^3*ieu0uou10+w^4*ieum1uou10+w^5*ieum2uou10;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou10a = K[w^1];
L ieu1uou10a = K[w^2];
L ieu0uou10a = K[w^3];
L ieum1uou10a = K[w^4];
L ieum2uou10a = K[w^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_odd.c> "%O"
#write <e10_odd.c> "return Eps5o2<T>("
#write <e10_odd.c> "%E", ieu2uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu0uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum2uou10a
#write <e10_odd.c> ");\n}"
L H=+u^1*ieu2ueu10+u^2*ieu1ueu10+u^3*ieu0ueu10+u^4*ieum1ueu10+u^5*ieum2ueu10;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu10a = H[u^1];
L ieu1ueu10a = H[u^2];
L ieu0ueu10a = H[u^3];
L ieum1ueu10a = H[u^4];
L ieum2ueu10a = H[u^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_even.c> "%O"
#write <e10_even.c> "return Eps5o2<T>("
#write <e10_even.c> "%E", ieu2ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu0ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum2ueu10a
#write <e10_even.c> ");\n}"
.end
