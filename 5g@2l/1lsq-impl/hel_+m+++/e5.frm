#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f180;
S f65;
S f181;
S f62;
S f132;
S f131;
S f130;
S f69;
S f68;
S f124;
S f127;
S f77;
S f120;
S f121;
S f122;
S f128;
S f129;
S f157;
S f8;
S f148;
S f18;
S f149;
S f19;
S f146;
S f80;
S f17;
S f14;
S f15;
S f145;
S f7;
S f12;
S f11;
S f28;
S f22;
S f21;
S f20;
S f176;
S f25;
S f24;
S f115;
S f114;
S f111;
S f110;
S f113;
S f112;
S f108;
S f58;
S f109;
S f59;
S f102;
S f103;
S f51;
S f56;
S f106;
S f107;
S f54;
S f104;
S f105;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (99*f19 - 11*f22 - 11*f58 + 63*f104 + 
      11*f120 + 11*f124 + 11*f176)/27 - (f114*fvu3u3)/9 + 
    (f114*fvu3u4)/12 - (11*f114*fvu3u5)/36 + (f114*fvu3u6)/9 - 
    (f114*fvu3u7)/6 - (8*f114*fvu3u8)/9 + (f110*fvu3u10)/6 - 
    (f114*fvu3u11)/9 + (f114*fvu3u12)/36 + (f114*fvu3u13)/36 - 
    (f114*fvu3u17)/12 + ((-2*f113 + f114)*fvu3u18)/12 + 
    (f114*fvu3u24)/9 + (f114*fvu3u51)/9 + (f114*fvu3u52)/18 - 
    (2*f114*fvu3u55)/9 + ((3*f111 - 2*f114)*fvu3u56)/18 + 
    ((-f102 - f114)*fvu3u57)/6 + (f114*fvu3u59)/6 + 
    (f114*fvu3u61)/3 + (f114*fvu1u1u1^3)/18 - 
    (11*f114*fvu1u1u2^3)/54 - (f114*fvu1u1u6^3)/9 + 
    (f114*fvu1u1u8^3)/18 - (8*f114*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(-(f114*fvu1u1u5)/36 + (f114*fvu1u1u8)/36 - 
      (7*f114*fvu1u1u9)/36) + fvu1u1u3^2*
     ((-3*f103 + 3*f104 - 3*f105 - 3*f107 + 3*f108 + f112 - 
        6*f114 + 6*f145 - f146)/18 + (f114*fvu1u1u4)/12 + 
      (f114*fvu1u1u6)/18 + (f114*fvu1u1u8)/12 + 
      (f114*fvu1u1u9)/9 - (f114*fvu1u1u10)/6) + 
    ((f112 - 6*f114 + 6*f145 - f146)*fvu2u1u13)/9 + 
    ((f112 - f115)*fvu2u1u15)/9 + (f114*fvu1u2u6*tci11^2)/3 + 
    ((-8*f15 + f17 + 9*f19 - 8*f21 - f22 + f24 - f58 + 
       9*f104 - 8*f105 + 8*f108 + f120 + 8*f121 - f122 + f124 + 
       8*f130 - f132 + f176)*tci12)/9 + 
    ((-f112 + 6*f114 - 6*f145 + f146)*fvu1u2u2*tci12)/9 - 
    (2*f114*fvu2u1u2*tci12)/9 + (f114*fvu2u1u3*tci12)/3 + 
    (2*f114*fvu2u1u11*tci12)/9 + 
    fvu2u1u1*((6*f18 - f20 - 6*f113 + 6*f114 - 6*f145 + f146)/
       9 - (2*f114*tci12)/3) + fvu1u1u8^2*
     ((-6*f12 + f51 + 3*f107 + 6*f110 + f115)/18 - 
      (f114*fvu1u1u9)/18 + (f114*fvu1u1u10)/6 - 
      (f114*tci12)/2) + fvu2u1u9*
     ((6*f12 + 6*f18 - f20 - f51 - 6*f110 - 6*f113)/9 - 
      (2*f114*tci12)/9) + fvu1u1u1^2*
     ((6*f12 + 12*f18 - 2*f20 - f51 + 3*f103 - 6*f110 - 
        12*f113 + 6*f114 - 6*f145 + f146)/18 + 
      (f114*fvu1u1u2)/18 - (f114*fvu1u1u4)/18 - 
      (f114*fvu1u1u8)/18 - (f114*fvu1u1u9)/18 - 
      (f114*fvu1u1u10)/6 - (f114*tci12)/18) + 
    fvu1u1u6^2*((f114*fvu1u1u9)/6 - (f114*fvu1u1u10)/9 - 
      (f114*tci12)/18) + fvu1u1u5^2*(-(f114*fvu1u1u9)/12 + 
      (f114*tci12)/36) + fvu1u1u9^2*
     ((-3*f108 + f112 - f115)/18 - (f114*fvu1u1u10)/18 + 
      (f114*tci12)/12) + fvu1u1u10^2*
     (f105/6 + (f114*tci12)/6) + fvu1u1u2^2*
     (-(f114*fvu1u1u4)/18 + (f114*fvu1u1u5)/18 - 
      (f114*fvu1u1u8)/18 - (f114*fvu1u1u9)/9 + 
      (2*f114*tci12)/9) + fvu2u1u12*
     ((-6*f12 + f51 + 6*f110 + f115)/9 + (f114*tci12)/3) + 
    tci11^2*((12*f18 - 2*f20 + 21*f104 - 18*f105 + 18*f108 + 
        2*f112 - 12*f113 + 2*f115)/108 + 
      ((f102 - 3*f110 + f111 - 2*f113 + 5*f114)*tci12)/36) + 
    fvu1u2u9*(-(f114*tci11^2)/3 + 
      ((6*f12 - f51 - 6*f110 - f115)*tci12)/9) + 
    fvu1u1u10*(-f106/9 - (2*f114*fvu2u1u14)/9 - 
      (4*f114*fvu2u1u15)/9 - (f114*tci11^2)/9 + 
      (f115*tci12)/9) + fvu1u1u5*(-(f114*fvu1u1u9^2)/12 - 
      (f114*fvu2u1u4)/9 - (f114*tci11^2)/27 - 
      (f114*fvu1u1u9*tci12)/6) + 
    fvu1u1u6*((f114*fvu1u1u9^2)/6 - (2*f114*fvu2u1u14)/9 + 
      (2*f114*tci11^2)/27 + (f114*fvu1u1u9*tci12)/3 - 
      (2*f114*fvu1u1u10*tci12)/9) + 
    fvu1u1u8*((8*f7 - f8 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 
        19*f19 + 2*f22 + 16*f25 - 2*f28 - 16*f54 + 2*f56 + 
        f58 - 2*f59 - 16*f62 + 2*f65 - 16*f68 + 2*f69 + 8*f77 - 
        f80 - 8*f107 - 2*f124 + 16*f127 - 2*f128 + 8*f129 - 
        f131 - 2*f157 + 16*f180 - 2*f181)/9 + 
      (f114*fvu1u1u9^2)/9 + (f114*fvu2u1u9)/9 - 
      (2*f114*fvu2u1u11)/9 - (f114*fvu2u1u12)/3 + 
      (73*f114*tci11^2)/108 - (f107*tci12)/3 + 
      (f114*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (-f115/9 - (f114*tci12)/3) + fvu1u1u9*
       ((6*f12 - f51 - 6*f110)/9 - (2*f114*tci12)/9)) + 
    fvu1u1u9*((-8*f15 + f17 - 8*f21 + f24 - 8*f105 + f106 + 
        8*f108 + 8*f121 - f122 + 8*f130 - f132)/9 + 
      (f114*fvu1u1u10^2)/6 - (7*f114*fvu2u1u11)/9 - 
      (f114*fvu2u1u12)/3 - (4*f114*fvu2u1u15)/9 - 
      (7*f114*tci11^2)/54 + (f112*tci12)/9 + 
      (f114*fvu1u2u9*tci12)/3 + fvu1u1u10*
       (f115/9 - (f114*tci12)/9)) + 
    fvu1u1u2*((2*f114*fvu1u1u4^2)/9 + (f114*fvu1u1u5^2)/9 + 
      (f114*fvu1u1u8^2)/18 + (5*f114*fvu1u1u9^2)/18 - 
      (f114*fvu2u1u3)/3 - (f114*fvu2u1u4)/9 - 
      (5*f114*fvu2u1u11)/9 + (f114*tci11^2)/27 + 
      (f114*fvu1u1u5*tci12)/9 - (2*f114*fvu1u1u9*tci12)/9 - 
      (f114*fvu1u2u6*tci12)/3 + fvu1u1u8*((f114*fvu1u1u9)/9 - 
        (f114*tci12)/9) + fvu1u1u4*(-(f114*fvu1u1u5)/9 + 
        (f114*fvu1u1u8)/9 + (2*f114*fvu1u1u9)/9 - 
        (f114*tci12)/9)) + fvu1u1u4*(-(f114*fvu1u1u5^2)/36 + 
      (f114*fvu1u1u8^2)/36 - (7*f114*fvu1u1u9^2)/36 + 
      (f114*fvu2u1u2)/9 - (f114*fvu2u1u3)/3 - 
      (2*f114*fvu2u1u11)/9 + (f114*tci11^2)/27 + 
      (7*f114*fvu1u1u9*tci12)/18 - (f114*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f114*fvu1u1u9)/9 - (f114*tci12)/18) + 
      fvu1u1u5*((f114*fvu1u1u9)/6 + (f114*tci12)/18)) + 
    fvu1u1u1*((-8*f103 - 8*f129 + f131 - 8*f148 + f149 - 
        8*f180 + f181)/9 + (2*f114*fvu1u1u2^2)/9 - 
      (f114*fvu1u1u3^2)/6 - (f114*fvu1u1u4^2)/9 - 
      (f114*fvu1u1u8^2)/9 - (f114*fvu1u1u9^2)/9 + 
      (f114*fvu2u1u1)/3 + (f114*fvu2u1u2)/9 + (f114*fvu2u1u9)/9 + 
      (2*f114*fvu2u1u11)/9 - (23*f114*tci11^2)/54 + 
      ((-6*f12 + f51 - 3*f103 + 6*f110 - 6*f114 + 6*f145 - 
         f146)*tci12)/9 + fvu1u1u9*((-6*f12 + f51 + 6*f110)/9 - 
        (f114*tci12)/9) + fvu1u1u2*(-(f114*fvu1u1u4)/9 - 
        (f114*fvu1u1u8)/9 - (f114*fvu1u1u9)/9 + 
        (f114*tci12)/9) + fvu1u1u8*((-6*f18 + f20 + 6*f113)/9 + 
        (f114*fvu1u1u9)/9 + (2*f114*tci12)/9) + 
      fvu1u1u4*((f114*fvu1u1u8)/9 + (f114*fvu1u1u9)/9 + 
        (2*f114*tci12)/9) + fvu1u1u3*((-6*f18 + f20 + 6*f113)/
         9 + (f114*tci12)/3) + fvu1u1u10*
       ((-6*f114 + 6*f145 - f146)/9 + (f114*tci12)/3)) + 
    fvu1u1u3*((-8*f7 + f8 + 16*f11 - 2*f14 - 8*f15 + f17 + 
        10*f19 + 8*f21 - f22 - f24 - 16*f25 + 2*f28 + 16*f54 - 
        2*f56 + 2*f59 + 16*f62 - 2*f65 + 16*f68 - 2*f69 - 
        8*f77 + f80 + 8*f103 - 9*f104 + 8*f105 + 8*f107 - 
        8*f108 - f120 - 8*f121 + f122 + f124 - 16*f127 + 
        2*f128 - 8*f130 + f132 + 8*f148 - f149 + 2*f157 - 
        f176 - 8*f180 + f181)/9 + (f114*fvu1u1u4^2)/12 + 
      (f114*fvu1u1u6^2)/18 + (f114*fvu1u1u8^2)/12 + 
      (f114*fvu1u1u9^2)/9 - (f114*fvu1u1u10^2)/6 + 
      (f114*fvu2u1u1)/3 + (f114*tci11^2)/36 + 
      ((f103 - f104 + f105 + f107 - f108)*tci12)/3 + 
      fvu1u1u9*(-f112/9 + (f114*fvu1u1u10)/9 - (2*f114*tci12)/
         9) + fvu1u1u8*((6*f18 - f20 - 6*f113)/9 - 
        (f114*tci12)/6) + fvu1u1u4*(-(f114*fvu1u1u8)/6 - 
        (f114*tci12)/6) + fvu1u1u6*(-(f114*fvu1u1u9)/3 + 
        (2*f114*fvu1u1u10)/9 - (f114*tci12)/9) + 
      fvu1u1u10*((6*f114 - 6*f145 + f146)/9 + (f114*tci12)/
         3)) + ((-9*f110 - 6*f113 + 10*f114)*tci12*tcr11^2)/
     18 + ((f110 + 4*f113 - 2*f114)*tcr11^3)/18 + 
    ((-6*f102 - 7*f110 - 6*f111 + 2*f113 + 7*f114)*tci11^2*
      tcr12)/36 + ((f110 - 2*f113 - f114)*tcr12^3)/9 - 
    (2*(3*f110 + 3*f113 - 8*f114)*tci12*tcr21)/9 + 
    tcr11*(((21*f110 + 25*f114)*tci11^2)/108 + 
      ((f110 - f114)*tci12*tcr12)/3 + 
      ((6*f113 - 5*f114)*tcr21)/9) + 
    ((-f110 + 2*f113 + f114)*tcr31)/3 + 
    ((-f110 + 2*f113 + f114)*tcr32)/12 + 
    ((-18*f102 + 11*f110 - 18*f111 - 74*f113 - 11*f114)*tcr33)/
     72;
L ieu1ueu5 = (9*f19 - f22 - f58 + 9*f104 + f120 + 
      f124 + f176)/9 + ((-f110 - 2*f113 + f114)*fvu1u1u1^2)/6 + 
    ((f111 - f114)*fvu1u1u3^2)/6 + ((f102 + f110)*fvu1u1u8^2)/6 + 
    ((-f102 + f111)*fvu1u1u9^2)/6 + 
    fvu1u1u8*(-f107/3 - (f110*fvu1u1u9)/3 - (f102*fvu1u1u10)/3) + 
    fvu1u1u3*((f103 - f104 + f105 + f107 - f108)/3 - 
      (f113*fvu1u1u8)/3 - (f111*fvu1u1u9)/3 + 
      (f114*fvu1u1u10)/3) + ((-f113 + f114)*fvu2u1u1)/3 + 
    ((-f110 - f113)*fvu2u1u9)/3 + ((f102 + f110)*fvu2u1u12)/3 + 
    ((f111 - f114)*fvu2u1u13)/3 + ((-f102 + f111)*fvu2u1u15)/3 + 
    ((f102 + f111 - f113)*tci11^2)/18 + 
    ((f104 - f105 + f108)*tci12)/3 + 
    ((-f111 + f114)*fvu1u2u2*tci12)/3 + 
    ((-f102 - f110)*fvu1u2u9*tci12)/3 + 
    fvu1u1u10*(-f105/3 + (f102*tci12)/3) + 
    fvu1u1u9*(f108/3 + (f102*fvu1u1u10)/3 + (f111*tci12)/3) + 
    fvu1u1u1*(-f103/3 + (f113*fvu1u1u3)/3 + (f113*fvu1u1u8)/3 + 
      (f110*fvu1u1u9)/3 - (f114*fvu1u1u10)/3 + 
      ((f110 - f114)*tci12)/3);
L ieu0ueu5 = f104/3;
L ieum1ueu5 = 0;
L ieum2ueu5 = 0;
L ieu2uou5 = (-4*f109*fvu4u28)/3 - (4*f109*fvu4u39)/3 - 
    (2*f109*fvu4u49)/9 - (4*f109*fvu4u51)/9 - (f109*fvu4u80)/12 - 
    (7*f109*fvu4u83)/12 + (3*f109*fvu4u91)/4 - (f109*fvu4u93)/12 - 
    (4*f109*fvu4u100)/9 + (f109*fvu4u102)/2 - (f109*fvu4u111)/4 - 
    (f109*fvu4u113)/2 - f109*fvu4u114 + (2*f109*fvu4u129)/3 - 
    (13*f109*fvu4u132)/3 + (7*f109*fvu4u139)/2 - 
    (4*f109*fvu4u141)/3 - (4*f109*fvu4u146)/9 - 
    (7*f109*fvu4u148)/6 + (4*f109*fvu4u174)/3 + 
    (4*f109*fvu4u182)/3 + (2*f109*fvu4u190)/9 + 
    (4*f109*fvu4u192)/9 + (f109*fvu4u213)/12 + 
    (67*f109*fvu4u215)/12 - (9*f109*fvu4u219)/4 + 
    (f109*fvu4u221)/12 + (4*f109*fvu4u225)/9 + (f109*fvu4u233)/4 + 
    (f109*fvu4u234)/2 + f109*fvu4u235 + (10*f109*fvu4u244)/3 - 
    (2*f109*fvu4u246)/3 - 2*f109*fvu4u250 - (2*f109*fvu4u252)/3 - 
    (2*f109*fvu4u255)/9 - 2*f109*fvu4u273 + 2*f109*fvu4u277 + 
    fvu3u25*((-2*f109*fvu1u1u3)/3 + (2*f109*fvu1u1u6)/3 - 
      (2*f109*fvu1u1u7)/3) + fvu3u71*((2*f109*fvu1u1u3)/3 - 
      (2*f109*fvu1u1u6)/3 + (2*f109*fvu1u1u7)/3) + 
    fvu3u70*((f109*fvu1u1u6)/3 - (f109*fvu1u1u8)/3 - 
      (f109*fvu1u1u10)/3) + fvu3u45*(-(f109*fvu1u1u2) + 
      (f109*fvu1u1u3)/3 + (2*f109*fvu1u1u5)/3 + 
      (f109*fvu1u1u6)/3 - f109*fvu1u1u8 + (f109*fvu1u1u9)/3 - 
      (f109*fvu1u1u10)/3) + fvu3u23*(-(f109*fvu1u1u1)/3 - 
      (f109*fvu1u1u6)/3 + (f109*fvu1u1u8)/3 + 
      (f109*fvu1u1u10)/3) + fvu3u43*((f109*fvu1u1u2)/3 + 
      (f109*fvu1u1u4)/3 + (f109*fvu1u1u6)/3 - (f109*fvu1u1u7)/3 - 
      (2*f109*fvu1u1u9)/3 + (f109*fvu1u1u10)/3) + 
    fvu3u63*(f109*fvu1u1u2 - (3*f109*fvu1u1u3)/2 - 
      (2*f109*fvu1u1u5)/3 - (19*f109*fvu1u1u6)/6 - 
      (f109*fvu1u1u7)/3 - (2*f109*fvu1u1u8)/3 - 
      (f109*fvu1u1u9)/3 + (7*f109*fvu1u1u10)/2) + 
    (13*f109*tci11^3*tci12)/15 + 
    fvu3u62*((-2*f109*fvu1u1u2)/3 + f109*fvu1u1u3 + 
      (f109*fvu1u1u4)/3 - (2*f109*fvu1u1u5)/3 - 
      (2*f109*fvu1u1u6)/3 + (f109*fvu1u1u7)/3 + f109*fvu1u1u8 + 
      f109*fvu1u1u9 + (2*f109*fvu1u1u10)/3 - 2*f109*tci12) + 
    fvu3u78*(-(f109*fvu1u1u2)/3 + (f109*fvu1u1u3)/3 - 
      (f109*fvu1u1u4)/3 + (f109*fvu1u1u6)/3 - (f109*fvu1u1u8)/3 + 
      (2*f109*fvu1u1u9)/3 + (2*f109*tci12)/3) + 
    fvu3u80*((2*f109*fvu1u1u2)/3 - (f109*fvu1u1u3)/3 - 
      (f109*fvu1u1u4)/3 + (2*f109*fvu1u1u5)/3 - 
      (2*f109*fvu1u1u6)/3 + f109*fvu1u1u7 - (5*f109*fvu1u1u8)/3 + 
      f109*fvu1u1u9 + (8*f109*tci12)/3) - 
    (10*f109*tci11^2*tci21)/27 + 
    tci12*((16*f109*tci31)/5 + 16*f109*tci32) - 
    (2072*f109*tci41)/135 - (64*f109*tci42)/5 - 
    (16*f109*tci43)/3 + ((53*f109*tci11^3)/108 + 
      (5*f109*tci12*tci21)/3 - (12*f109*tci31)/5 + 
      16*f109*tci32)*tcr11 + (61*f109*tci11*tcr11^3)/
     180 + fvu1u1u10*((169*f109*tci11^3)/1620 + 
      (25*f109*tci12*tci21)/9 + (76*f109*tci31)/5 + 
      (19*f109*tci21*tcr11)/3 - (19*f109*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f109*tci11^3)/270 + 
      (72*f109*tci31)/5 + 6*f109*tci21*tcr11 - 
      (3*f109*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f109*fvu3u25)/3 - (f109*fvu3u43)/3 + 
      (2*f109*fvu3u45)/3 - (f109*fvu3u62)/3 + (13*f109*fvu3u63)/6 + 
      (f109*fvu3u70)/3 - (2*f109*fvu3u71)/3 - (f109*fvu3u78)/3 - 
      (f109*fvu3u80)/3 + (19*f109*tci11^3)/108 - 
      (f109*tci12*tci21)/3 + 12*f109*tci31 + 
      5*f109*tci21*tcr11 - (f109*tci11*tcr11^2)/4) + 
    fvu1u1u5*((f109*tci11^3)/45 - (16*f109*tci12*tci21)/9 - 
      (16*f109*tci31)/5 - (4*f109*tci21*tcr11)/3 + 
      (f109*tci11*tcr11^2)/15) + 
    fvu1u1u8*((-133*f109*tci11^3)/810 + 
      (8*f109*tci12*tci21)/3 - (24*f109*tci31)/5 - 
      2*f109*tci21*tcr11 + (f109*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f109*tci11^3)/180 - f109*tci12*tci21 - 
      (36*f109*tci31)/5 - 3*f109*tci21*tcr11 + 
      (3*f109*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f109*tci11^3)/162 - (16*f109*tci12*tci21)/
       9 - 8*f109*tci31 - (10*f109*tci21*tcr11)/3 + 
      (f109*tci11*tcr11^2)/6) + 
    fvu1u1u9*((-19*f109*tci11^3)/270 - 
      (20*f109*tci12*tci21)/9 - (56*f109*tci31)/5 - 
      (14*f109*tci21*tcr11)/3 + (7*f109*tci11*tcr11^2)/
       30) + fvu1u1u3*((-391*f109*tci11^3)/1620 + 
      (f109*tci12*tci21)/3 - (84*f109*tci31)/5 - 
      7*f109*tci21*tcr11 + (7*f109*tci11*tcr11^2)/20) + 
    ((-218*f109*tci11^3)/243 - (40*f109*tci12*tci21)/3)*
     tcr12 + (-4*f109*tci11*tci12 - (40*f109*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f109*tci11*tci12)/15 - 
      3*f109*tci21 - 2*f109*tci11*tcr12) + 
    (130*f109*tci11*tcr33)/27;
L ieu1uou5 = 
   2*f109*fvu3u62 - (11*f109*tci11^3)/135 - 
    (4*f109*tci12*tci21)/3 - (48*f109*tci31)/5 - 
    4*f109*tci21*tcr11 + (f109*tci11*tcr11^2)/5;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
