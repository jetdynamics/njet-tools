#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f137;
S f65;
S f135;
S f133;
S f132;
S f62;
S f187;
S f131;
S f130;
S f60;
S f188;
S f69;
S f68;
S f124;
S f125;
S f126;
S f77;
S f70;
S f120;
S f121;
S f122;
S f123;
S f73;
S f129;
S f153;
S f148;
S f149;
S f19;
S f80;
S f17;
S f14;
S f15;
S f142;
S f140;
S f11;
S f28;
S f173;
S f176;
S f25;
S f98;
S f99;
S f118;
S f160;
S f161;
S f117;
S f116;
S f111;
S f165;
S f166;
S f112;
S f58;
S f52;
S f100;
S f106;
S f104;
S f105;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu12 = (-11*f19 + 99*f58 + 11*f60 - 11*f104 - 
      63*f120 - 11*f124 - 11*f176)/27 - (2*f135*fvu3u1)/3 + 
    (f135*fvu3u2)/6 + ((f135 - 4*f142)*fvu3u3)/9 - 
    (5*f135*fvu3u4)/12 + ((11*f135 - 28*f142)*fvu3u5)/36 - 
    (5*f135*fvu3u6)/9 + ((f135 - 3*f142)*fvu3u7)/6 + 
    (8*(f135 - 2*f142)*fvu3u8)/9 + ((f135 - 2*f142)*fvu3u11)/9 - 
    (5*f135*fvu3u12)/36 + ((-f135 + 4*f142)*fvu3u13)/36 - 
    (f116*fvu3u15)/6 + ((f135 - 2*f137 - 4*f142)*fvu3u17)/12 - 
    (5*f135*fvu3u18)/12 - (2*f135*fvu3u19)/9 + 
    ((-f135 + 2*f142)*fvu3u24)/9 + (f135*fvu3u51)/9 - 
    (f142*fvu3u52)/18 + (2*f142*fvu3u55)/9 + 
    ((f135 + 3*f140 + 3*f142)*fvu3u56)/18 + 
    ((f135 + f142)*fvu3u57)/6 - (f142*fvu3u59)/6 - 
    (f135*fvu3u61)/3 - (5*f135*fvu1u1u1^3)/6 + 
    ((11*f135 - 52*f142)*fvu1u1u2^3)/54 + 
    ((f135 + f142)*fvu1u1u3^3)/9 - (2*f135*fvu1u1u5^3)/9 + 
    (f142*fvu1u1u6^3)/9 + ((-f135 - 6*f142)*fvu1u1u8^3)/18 + 
    (5*(f135 + f142)*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*(((f135 - 4*f142)*fvu1u1u5)/36 - 
      (5*f135*fvu1u1u8)/36 + ((7*f135 - 20*f142)*fvu1u1u9)/36) + 
    ((6*f52 - f98 + 6*f99 - f100 + 6*f116 - 6*f137)*fvu2u1u4)/
     9 + ((6*f52 - f98 + 6*f116 - 6*f135 + 6*f153 - f173)*
      fvu2u1u5)/9 + ((6*f111 - f112 + 6*f140 - 6*f142 + 6*f187 - 
       f188)*fvu2u1u15)/9 + ((-3*f135 + 8*f142)*fvu1u2u6*
      tci11^2)/9 + ((f135 - 2*f142)*fvu1u2u9*tci11^2)/3 + 
    ((16*f11 - 2*f14 - 16*f15 + 2*f17 - 16*f25 + 2*f28 + 
       16*f62 - 2*f65 + 24*f68 - 3*f69 - 8*f70 + f73 - 16*f77 + 
       2*f80 - 16*f105 + 2*f106 - 3*f120 + 16*f121 - 2*f122 - 
       16*f126 + 16*f129 - 8*f130 - 2*f131 + f132 + 16*f148 - 
       2*f149 + 16*f160 - 2*f161 + 8*f165 - f166 - 3*f176)*
      tci12)/18 + ((-6*f52 + f98 - 6*f116 + 6*f135 - 6*f153 + 
       f173)*fvu1u2u7*tci12)/9 + (10*f135*fvu2u1u1*tci12)/3 + 
    (10*f135*fvu2u1u2*tci12)/9 + 
    ((-3*f135 + 8*f142)*fvu2u1u3*tci12)/9 + 
    (10*f135*fvu2u1u9*tci12)/9 + 
    ((-f135 + 2*f142)*fvu2u1u12*tci12)/3 + 
    fvu1u1u1^2*(((-f135 + 2*f142)*fvu1u1u2)/18 + 
      (5*f135*fvu1u1u4)/18 + (f135*fvu1u1u5)/6 + 
      (5*f135*fvu1u1u8)/18 + ((f135 - 2*f142)*fvu1u1u9)/18 + 
      (f135*fvu1u1u10)/3 + (35*f135*tci12)/18) + 
    fvu2u1u11*((6*f99 - f100 - 6*f137 + 6*f142 - 6*f187 + f188)/
       9 - (2*(f135 - 2*f142)*tci12)/9) + 
    fvu1u1u3^2*((6*f52 - f98 + 6*f111 - f112 + 6*f116 - 
        3*f126 - 12*f135 + 6*f140 + 12*f153 - 2*f173)/18 - 
      (5*f135*fvu1u1u4)/12 - (f142*fvu1u1u6)/18 - 
      (5*f135*fvu1u1u8)/12 + ((-f135 - 3*f142)*fvu1u1u9)/18 + 
      ((-f135 + 2*f142)*fvu1u1u10)/18 + ((-f135 - f142)*tci12)/
       9) + fvu1u1u10^2*(-f117/6 - (f142*tci12)/6) + 
    fvu1u1u6^2*(-(f142*fvu1u1u9)/6 + (f142*fvu1u1u10)/9 + 
      (f142*tci12)/18) + fvu2u1u13*
     ((6*f111 - f112 - 6*f135 + 6*f140 + 6*f153 - f173)/9 - 
      (2*(f135 + f142)*tci12)/9) + 
    fvu1u1u8^2*(((f135 - 2*f142)*fvu1u1u9)/18 + 
      ((-f135 + f142)*fvu1u1u10)/6 + ((3*f135 + 2*f142)*tci12)/
       6) + fvu1u1u5^2*(f129/6 + ((f135 - 4*f142)*fvu1u1u9)/12 + 
      (f135*fvu1u1u10)/6 + ((5*f135 + 4*f142)*tci12)/36) + 
    fvu1u1u2^2*((6*f52 - f98 + 12*f99 - 2*f100 + 6*f116 + 
        3*f123 - 12*f137 + 6*f142 - 6*f187 + f188)/18 + 
      (f135*fvu1u1u4)/18 + ((-f135 + 4*f142)*fvu1u1u5)/18 + 
      ((f135 + f142)*fvu1u1u8)/18 + ((f135 - 2*f142)*fvu1u1u9)/
       9 + (f142*fvu1u1u10)/6 + ((-4*f135 + 7*f142)*tci12)/18) + 
    tci11^2*((12*f52 - 2*f98 + 12*f99 - 2*f100 + 12*f111 - 
        2*f112 + 12*f116 - 3*f120 - 18*f126 - 60*f135 - 12*f137 + 
        12*f140 - 12*f142 + 60*f153 - 10*f173 + 12*f187 - 2*f188)/
       108 + ((3*f116 - 20*f135 + 3*f140 + 12*f142)*tci12)/108) + 
    fvu1u1u9^2*((6*f111 - f112 + 3*f117 - 3*f120 - 3*f123 + 
        3*f126 - 3*f129 + 6*f140 - 6*f142 + 6*f187 - f188)/18 - 
      (f135*fvu1u1u10)/18 + ((-5*f135 + 26*f142)*tci12)/36) + 
    fvu1u2u2*((2*(f135 + f142)*tci11^2)/9 + 
      ((-6*f111 + f112 + 6*f135 - 6*f140 - 6*f153 + f173)*
        tci12)/9) + fvu1u1u6*(-(f142*fvu1u1u9^2)/6 + 
      (2*f142*fvu2u1u14)/9 - (2*f142*tci11^2)/27 - 
      (f142*fvu1u1u9*tci12)/3 + (2*f142*fvu1u1u10*tci12)/9) + 
    fvu1u1u10*(f118/9 + (2*(f135 + f142)*fvu2u1u13)/9 + 
      (2*f142*fvu2u1u14)/9 + (2*(f135 + 3*f142)*fvu2u1u15)/9 + 
      ((4*f135 + 7*f142)*tci11^2)/27 + 
      ((-6*f135 + 6*f153 - f173)*tci12)/9 - 
      (2*(f135 + f142)*fvu1u2u2*tci12)/9) + 
    fvu1u1u8*(((-f135 + 2*f142)*fvu1u1u9^2)/9 + 
      (f142*fvu1u1u10^2)/6 - (5*f135*fvu2u1u9)/9 + 
      (2*(f135 - 2*f142)*fvu2u1u11)/9 + 
      ((f135 - 2*f142)*fvu2u1u12)/3 + 
      ((-77*f135 - 18*f142)*tci11^2)/108 + 
      (2*(f135 - 2*f142)*fvu1u1u9*tci12)/9 + 
      ((f135 - f142)*fvu1u1u10*tci12)/3 + 
      ((-f135 + 2*f142)*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((16*f11 - 2*f14 - 16*f15 + 2*f17 + 2*f19 - 
        16*f25 + 2*f28 - 18*f58 - 2*f60 + 16*f62 - 2*f65 + 
        24*f68 - 3*f69 - 8*f70 + f73 - 16*f77 + 2*f80 + 
        2*f104 - 16*f105 + 2*f106 - 2*f118 + 15*f120 + 16*f121 - 
        2*f122 + 2*f124 + 2*f125 - 16*f126 + 16*f129 - 8*f130 + 
        f132 + 16*f148 - 2*f149 + 16*f160 - 2*f161 + 8*f165 - 
        f166 - f176)/18 + ((-f135 - 2*f142)*fvu1u1u10^2)/6 + 
      ((7*f135 - 20*f142)*fvu2u1u11)/9 + 
      ((f135 - 2*f142)*fvu2u1u12)/3 + 
      (2*(f135 + 3*f142)*fvu2u1u15)/9 + 
      ((11*f135 - 54*f142)*tci11^2)/54 + 
      ((6*f111 - f112 + 6*f140)*tci12)/9 + 
      ((-f135 + 2*f142)*fvu1u2u9*tci12)/3 + 
      fvu1u1u10*((6*f142 - 6*f187 + f188)/9 - (f135*tci12)/
         9)) + fvu1u1u5*(-f131/9 + ((f135 - 4*f142)*fvu1u1u9^2)/
       12 + (f135*fvu1u1u10^2)/6 + ((f135 - 4*f142)*fvu2u1u4)/9 + 
      ((5*f135 - 8*f142)*tci11^2)/54 + 
      ((-6*f135 + 6*f153 - f173)*tci12)/9 + 
      fvu1u1u10*((-6*f135 + 6*f153 - f173)/9 + 
        (f135*tci12)/3) + fvu1u1u9*((6*f99 - f100 - 6*f137)/9 + 
        ((f135 - 4*f142)*tci12)/6)) + 
    fvu1u1u1*((-2*(f135 - 2*f142)*fvu1u1u2^2)/9 + 
      (5*f135*fvu1u1u3^2)/6 + (5*f135*fvu1u1u4^2)/9 - 
      (f135*fvu1u1u5^2)/6 + (5*f135*fvu1u1u8^2)/9 + 
      ((f135 - 2*f142)*fvu1u1u9^2)/9 + (f135*fvu1u1u10^2)/6 - 
      (5*f135*fvu2u1u1)/3 - (5*f135*fvu2u1u2)/9 - 
      (5*f135*fvu2u1u9)/9 - (2*(f135 - 2*f142)*fvu2u1u11)/9 + 
      (2*f135*tci11^2)/27 - (5*f135*fvu1u1u3*tci12)/3 + 
      ((f135 - 2*f142)*fvu1u1u9*tci12)/9 - 
      (2*f135*fvu1u1u10*tci12)/3 + fvu1u1u8*
       (((-f135 + 2*f142)*fvu1u1u9)/9 - (10*f135*tci12)/9) + 
      fvu1u1u4*((-5*f135*fvu1u1u8)/9 + 
        ((-f135 + 2*f142)*fvu1u1u9)/9 - (10*f135*tci12)/9) + 
      fvu1u1u5*(-(f135*fvu1u1u10)/3 - (f135*tci12)/3) + 
      fvu1u1u2*(((f135 - 2*f142)*fvu1u1u4)/9 + 
        ((f135 - 2*f142)*fvu1u1u8)/9 + ((f135 - 2*f142)*fvu1u1u9)/
         9 + ((-f135 + 2*f142)*tci12)/9)) + 
    fvu1u1u3*((-16*f11 + 2*f14 + 16*f15 - 2*f17 + 16*f25 - 
        2*f28 - 16*f62 + 2*f65 - 24*f68 + 3*f69 + 8*f70 - f73 + 
        16*f77 - 2*f80 + 16*f105 - 2*f106 + 3*f120 - 16*f121 + 
        2*f122 + 16*f126 - 16*f129 + 8*f130 + 2*f131 - f132 - 
        16*f148 + 2*f149 - 16*f160 + 2*f161 - 8*f165 + f166 + 
        3*f176)/18 - (5*f135*fvu1u1u4^2)/12 + 
      ((6*f135 - 6*f153 + f173)*fvu1u1u5)/9 - 
      (f142*fvu1u1u6^2)/18 - (5*f135*fvu1u1u8^2)/12 + 
      ((-f135 - 3*f142)*fvu1u1u9^2)/18 - (f135*fvu1u1u10^2)/6 - 
      (5*f135*fvu2u1u1)/3 + (2*(f135 + f142)*fvu2u1u13)/9 + 
      ((-3*f135 + 2*f142)*tci11^2)/36 + (f126*tci12)/3 + 
      (5*f135*fvu1u1u8*tci12)/6 - (2*(f135 + f142)*fvu1u2u2*
        tci12)/9 + fvu1u1u10*((6*f135 - 6*f153 + f173)/9 + 
        (f135*tci12)/3) + fvu1u1u4*((5*f135*fvu1u1u8)/6 + 
        (5*f135*tci12)/6) + fvu1u1u6*((f142*fvu1u1u9)/3 - 
        (2*f142*fvu1u1u10)/9 + (f142*tci12)/9) + 
      fvu1u1u9*((-6*f111 + f112 - 6*f140)/9 + (f135*fvu1u1u10)/
         9 + ((f135 + 3*f142)*tci12)/9)) + 
    fvu1u1u4*(((f135 - 4*f142)*fvu1u1u5^2)/36 - 
      (5*f135*fvu1u1u8^2)/36 + ((7*f135 - 20*f142)*fvu1u1u9^2)/36 - 
      (5*f135*fvu2u1u2)/9 + ((3*f135 - 8*f142)*fvu2u1u3)/9 + 
      (2*(f135 - 2*f142)*fvu2u1u11)/9 - 
      (2*(f135 - f142)*tci11^2)/27 + 
      ((-7*f135 + 20*f142)*fvu1u1u9*tci12)/18 + 
      ((3*f135 - 8*f142)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(((f135 - 2*f142)*fvu1u1u9)/9 + (5*f135*tci12)/
         18) + fvu1u1u5*(((-f135 + 4*f142)*fvu1u1u9)/6 + 
        ((-f135 + 4*f142)*tci12)/18)) + 
    fvu1u1u2*(-f125/9 + ((-6*f52 + f98 - 6*f116)*fvu1u1u3)/9 - 
      (2*(f135 - 3*f142)*fvu1u1u4^2)/9 + 
      ((-f135 + 4*f142)*fvu1u1u5^2)/9 + 
      ((-f135 + 5*f142)*fvu1u1u8^2)/18 + 
      ((-5*f135 + 16*f142)*fvu1u1u9^2)/18 + (f142*fvu1u1u10^2)/6 + 
      ((3*f135 - 8*f142)*fvu2u1u3)/9 + ((f135 - 4*f142)*fvu2u1u4)/
       9 + ((5*f135 - 16*f142)*fvu2u1u11)/9 + 
      ((-f135 + 8*f142)*tci11^2)/27 + 
      ((6*f52 - f98 + 6*f116)*tci12)/9 + 
      ((3*f135 - 8*f142)*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(((-f135 + 2*f142)*fvu1u1u9)/9 - 
        (f142*fvu1u1u10)/3 + ((f135 - 5*f142)*tci12)/9) + 
      fvu1u1u4*(((f135 - 4*f142)*fvu1u1u5)/9 + 
        ((-f135 + 2*f142)*fvu1u1u8)/9 - 
        (2*(f135 - 2*f142)*fvu1u1u9)/9 + 
        ((f135 - 4*f142)*tci12)/9) + 
      fvu1u1u9*((-6*f99 + f100 + 6*f137)/9 + 
        (2*(f135 - 2*f142)*tci12)/9) + 
      fvu1u1u10*((-6*f142 + 6*f187 - f188)/9 + 
        (f142*tci12)/3) + fvu1u1u5*((-6*f99 + f100 + 6*f137)/
         9 + ((-f135 + 4*f142)*tci12)/9)) - 
    (41*f135*tci12*tcr11^2)/18 + (5*f135*tcr11^3)/6 + 
    ((-3*f116 + 2*f135 - 3*f140 - 8*f142)*tci11^2*tcr12)/18 - 
    (56*f135*tci12*tcr21)/9 + 
    tcr11*((-8*f135*tci11^2)/27 + (2*f135*tci12*tcr12)/
       3 + (25*f135*tcr21)/9) + 
    ((-9*f116 + 26*f135 + 12*f137 - 9*f140 + 12*f142)*tcr33)/
     36;
L ieu1ueu12 = 
   (-f19 + 9*f58 + f60 - f104 - 9*f120 - f124 - f176)/9 + 
    ((f116 - 2*f137 + f142)*fvu1u1u2^2)/6 + 
    ((f116 - 2*f135 + f140)*fvu1u1u3^2)/6 + 
    ((f140 - f142)*fvu1u1u9^2)/6 + 
    fvu1u1u3*(f126/3 + (f135*fvu1u1u5)/3 - (f140*fvu1u1u9)/3 + 
      (f135*fvu1u1u10)/3) + ((f116 - f137)*fvu2u1u4)/3 + 
    ((f116 - f135)*fvu2u1u5)/3 + ((-f137 + f142)*fvu2u1u11)/3 + 
    ((-f135 + f140)*fvu2u1u13)/3 + ((f140 - f142)*fvu2u1u15)/3 + 
    ((f116 - 5*f135 - f137 + f140 - f142)*tci11^2)/18 - 
    (f126*tci12)/3 + ((f135 - f140)*fvu1u2u2*tci12)/3 + 
    ((-f116 + f135)*fvu1u2u7*tci12)/3 + 
    fvu1u1u2*(-f123/3 - (f116*fvu1u1u3)/3 + (f137*fvu1u1u5)/3 + 
      (f137*fvu1u1u9)/3 - (f142*fvu1u1u10)/3 + 
      (f116*tci12)/3) + fvu1u1u10*(f117/3 - (f135*tci12)/3) + 
    fvu1u1u5*(-f129/3 - (f137*fvu1u1u9)/3 - (f135*fvu1u1u10)/3 - 
      (f135*tci12)/3) + fvu1u1u9*
     ((-f117 + f120 + f123 - f126 + f129)/3 + 
      (f142*fvu1u1u10)/3 + (f140*tci12)/3);
L ieu0ueu12 = -f120/3;
L ieum1ueu12 = 0;
L ieum2ueu12 = 0;
L ieu2uou12 = (-2*f133*fvu4u25)/3 - (8*f133*fvu4u28)/3 - 
    (8*f133*fvu4u39)/3 - (2*f133*fvu4u41)/3 - (10*f133*fvu4u51)/9 - 
    (23*f133*fvu4u80)/12 - (41*f133*fvu4u83)/12 - 
    (19*f133*fvu4u91)/4 - (23*f133*fvu4u93)/12 + 
    (4*f133*fvu4u100)/3 - (25*f133*fvu4u102)/18 + 
    (f133*fvu4u111)/4 + (f133*fvu4u113)/2 + f133*fvu4u114 + 
    (2*f133*fvu4u129)/3 + (17*f133*fvu4u132)/3 - 
    (5*f133*fvu4u139)/6 + (2*f133*fvu4u141)/3 + 
    (2*f133*fvu4u146)/9 + (5*f133*fvu4u148)/18 + 
    (f133*fvu4u171)/12 + (31*f133*fvu4u174)/12 + 
    (31*f133*fvu4u182)/12 + (f133*fvu4u184)/12 + 
    (2*f133*fvu4u190)/9 + (7*f133*fvu4u192)/18 + (f133*fvu4u199)/4 + 
    (f133*fvu4u201)/2 + f133*fvu4u202 - (25*f133*fvu4u213)/12 - 
    (9*f133*fvu4u215)/4 + (17*f133*fvu4u219)/4 - 
    (25*f133*fvu4u221)/12 + (4*f133*fvu4u225)/9 - 
    (f133*fvu4u233)/4 - (f133*fvu4u234)/2 - f133*fvu4u235 + 
    (49*f133*fvu4u244)/3 - (4*f133*fvu4u246)/3 - 
    (3*f133*fvu4u252)/2 - (49*f133*fvu4u255)/18 + 
    (23*f133*fvu4u271)/12 - (107*f133*fvu4u273)/12 + 
    (107*f133*fvu4u277)/12 + (23*f133*fvu4u279)/12 - 
    (4*f133*fvu4u282)/3 - (f133*fvu4u289)/4 - (f133*fvu4u290)/2 - 
    f133*fvu4u291 - (4*f133*fvu4u293)/3 + (8*f133*fvu4u295)/3 - 
    2*f133*fvu4u330 + fvu3u71*((f133*fvu1u1u2)/3 + 
      (5*f133*fvu1u1u3)/3 - (f133*fvu1u1u5)/3 - 
      (5*f133*fvu1u1u6)/3 + (5*f133*fvu1u1u7)/3 - 
      (f133*fvu1u1u8)/3) + fvu3u70*((f133*fvu1u1u2)/3 - 
      (f133*fvu1u1u3)/3 - (f133*fvu1u1u9)/3) + 
    fvu3u78*((-2*f133*fvu1u1u2)/3 + (2*f133*fvu1u1u3)/3 + 
      (2*f133*fvu1u1u9)/3) + fvu3u45*(-2*f133*fvu1u1u2 - 
      (4*f133*fvu1u1u3)/3 + (2*f133*fvu1u1u6)/3 - 
      (2*f133*fvu1u1u7)/3 + 2*f133*fvu1u1u9) + 
    fvu3u63*((4*f133*fvu1u1u2)/3 + (5*f133*fvu1u1u3)/2 + 
      (f133*fvu1u1u5)/3 + (3*f133*fvu1u1u6)/2 - 
      (2*f133*fvu1u1u7)/3 + (f133*fvu1u1u8)/3 - 
      (5*f133*fvu1u1u9)/3 - (5*f133*fvu1u1u10)/6) + 
    fvu3u43*((-4*f133*fvu1u1u2)/3 - (2*f133*fvu1u1u4)/3 - 
      (4*f133*fvu1u1u6)/3 + (4*f133*fvu1u1u7)/3 + 
      2*f133*fvu1u1u9 - (2*f133*fvu1u1u10)/3) + 
    fvu3u25*((f133*fvu1u1u2)/3 - (4*f133*fvu1u1u3)/3 - 
      (f133*fvu1u1u5)/3 + (5*f133*fvu1u1u6)/3 - 
      (4*f133*fvu1u1u7)/3 - (f133*fvu1u1u10)/3) + 
    fvu3u62*((f133*fvu1u1u2)/3 - (2*f133*fvu1u1u4)/3 + 
      (f133*fvu1u1u5)/3 + (f133*fvu1u1u6)/3 - 
      (2*f133*fvu1u1u7)/3 - (f133*fvu1u1u10)/3) + 
    fvu3u23*(-(f133*fvu1u1u1)/3 + (f133*fvu1u1u2)/3 - 
      (f133*fvu1u1u7)/3 + (f133*fvu1u1u8)/3 - (f133*fvu1u1u9)/3 + 
      (f133*fvu1u1u10)/3) + fvu3u81*((4*f133*fvu1u1u3)/3 + 
      (2*f133*fvu1u1u4)/3 + (4*f133*fvu1u1u6)/3 - 
      (4*f133*fvu1u1u7)/3 - (2*f133*fvu1u1u9)/3 + 
      (2*f133*fvu1u1u10)/3) - (151*f133*tci11^3*tci12)/135 + 
    fvu3u82*(-(f133*fvu1u1u2) - (f133*fvu1u1u3)/3 + 
      (2*f133*fvu1u1u4)/3 - 2*f133*fvu1u1u5 - f133*fvu1u1u6 + 
      (4*f133*fvu1u1u7)/3 - (f133*fvu1u1u8)/3 + 
      (f133*fvu1u1u9)/3 - f133*fvu1u1u10 - (4*f133*tci12)/3) + 
    fvu3u80*((-29*f133*fvu1u1u6)/6 + (29*f133*fvu1u1u7)/6 - 
      (49*f133*fvu1u1u8)/6 + (29*f133*fvu1u1u9)/6 + 
      (49*f133*tci12)/6) - (2*f133*tci11^2*tci21)/9 + 
    fvu1u1u8*((-49*f133*tci11^3)/81 + (49*f133*tci12*tci21)/
       3) + tci12*((-48*f133*tci31)/5 - 16*f133*tci32) - 
    (4*f133*tci41)/3 + ((-151*f133*tci11^3)/324 + 
      13*f133*tci12*tci21 + 12*f133*tci31)*tcr11 + 
    ((f133*tci11*tci12)/5 + 5*f133*tci21)*tcr11^2 - 
    (f133*tci11*tcr11^3)/4 + 
    fvu1u1u3*((-71*f133*tci11^3)/324 + 
      (31*f133*tci12*tci21)/3 + 12*f133*tci31 + 
      5*f133*tci21*tcr11 - (f133*tci11*tcr11^2)/4) + 
    fvu1u1u2*((10*f133*fvu3u80)/3 + (47*f133*tci11^3)/162 - 
      (44*f133*tci12*tci21)/9 + 8*f133*tci31 + 
      (10*f133*tci21*tcr11)/3 - (f133*tci11*tcr11^2)/6) + 
    fvu1u1u7*((383*f133*tci11^3)/810 - 11*f133*tci12*
       tci21 + (24*f133*tci31)/5 + 2*f133*tci21*tcr11 - 
      (f133*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-121*f133*tci11^3)/324 + 
      (104*f133*tci12*tci21)/9 + 4*f133*tci31 + 
      (5*f133*tci21*tcr11)/3 - (f133*tci11*tcr11^2)/12) + 
    fvu1u1u5*((-37*f133*tci11^3)/270 + 
      (28*f133*tci12*tci21)/9 - (8*f133*tci31)/5 - 
      (2*f133*tci21*tcr11)/3 + (f133*tci11*tcr11^2)/30) + 
    fvu1u1u9*((41*f133*tci11^3)/135 - (95*f133*tci12*tci21)/
       9 - (32*f133*tci31)/5 - (8*f133*tci21*tcr11)/3 + 
      (2*f133*tci11*tcr11^2)/15) + 
    fvu1u1u10*((-107*f133*tci11^3)/540 + 
      (19*f133*tci12*tci21)/9 - (44*f133*tci31)/5 - 
      (11*f133*tci21*tcr11)/3 + (11*f133*tci11*tcr11^2)/
       60) + fvu1u1u1*((4*f133*fvu3u25)/3 + (2*f133*fvu3u43)/3 + 
      (4*f133*fvu3u45)/3 + (2*f133*fvu3u62)/3 - 
      (17*f133*fvu3u63)/6 + (f133*fvu3u70)/3 - (4*f133*fvu3u71)/3 - 
      (2*f133*fvu3u78)/3 - 2*f133*fvu3u81 + (4*f133*fvu3u82)/3 + 
      (103*f133*tci11^3)/324 - 13*f133*tci12*tci21 - 
      12*f133*tci31 - 5*f133*tci21*tcr11 + 
      (f133*tci11*tcr11^2)/4) + 
    (40*f133*tci12*tci21*tcr12)/3 + 
    4*f133*tci11*tci12*tcr12^2;
L ieu1uou12 = 
   -2*f133*fvu3u82 - (4*f133*tci11^3)/27 + 
    4*f133*tci12*tci21;
L ieu0uou12 = 0;
L ieum1uou12 = 0;
L ieum2uou12 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou12+w^2*ieu1uou12+w^3*ieu0uou12+w^4*ieum1uou12+w^5*ieum2uou12;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou12a = K[w^1];
L ieu1uou12a = K[w^2];
L ieu0uou12a = K[w^3];
L ieum1uou12a = K[w^4];
L ieum2uou12a = K[w^5];
.sort
#write <e12.tmp> "`optimmaxvar_'"
#write <e12_odd.c> "%O"
#write <e12_odd.c> "return Eps5o2<T>("
#write <e12_odd.c> "%E", ieu2uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieu1uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieu0uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieum1uou12a
#write <e12_odd.c> ", "
#write <e12_odd.c> "%E", ieum2uou12a
#write <e12_odd.c> ");\n}"
L H=+u^1*ieu2ueu12+u^2*ieu1ueu12+u^3*ieu0ueu12+u^4*ieum1ueu12+u^5*ieum2ueu12;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu12a = H[u^1];
L ieu1ueu12a = H[u^2];
L ieu0ueu12a = H[u^3];
L ieum1ueu12a = H[u^4];
L ieum2ueu12a = H[u^5];
.sort
#write <e12.tmp> "`optimmaxvar_'"
#write <e12_even.c> "%O"
#write <e12_even.c> "return Eps5o2<T>("
#write <e12_even.c> "%E", ieu2ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieu1ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieu0ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieum1ueu12a
#write <e12_even.c> ", "
#write <e12_even.c> "%E", ieum2ueu12a
#write <e12_even.c> ");\n}"
.end
