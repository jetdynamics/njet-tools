#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f67;
S f180;
S f65;
S f181;
S f132;
S f62;
S f131;
S f130;
S f60;
S f69;
S f68;
S f124;
S f75;
S f127;
S f120;
S f70;
S f121;
S f122;
S f73;
S f128;
S f78;
S f129;
S f157;
S f89;
S f8;
S f148;
S f149;
S f19;
S f81;
S f17;
S f14;
S f15;
S f7;
S f84;
S f11;
S f86;
S f96;
S f28;
S f92;
S f93;
S f22;
S f176;
S f25;
S f98;
S f118;
S f160;
S f161;
S f117;
S f165;
S f166;
S f58;
S f59;
S f52;
S f106;
S f56;
S f104;
S f54;
S f105;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu4u132;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u22;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu4u91;
S fvu4u93;
S fvu4u113;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u330;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu1 = (-36*f58 - 11*f60)/27 + (f93*fvu3u1)/3 + 
    ((-f78 - f93)*fvu3u2)/6 - (f93*fvu3u3)/9 + 
    ((3*f89 + 2*f93)*fvu3u4)/18 - (f93*fvu3u5)/18 + 
    (2*f93*fvu3u6)/9 + (2*f93*fvu3u11)/9 + (f93*fvu3u12)/18 + 
    ((-3*f84 + 2*f93)*fvu3u13)/18 + (f93*fvu3u14)/9 + 
    ((-f52 + f93)*fvu3u15)/6 - (f93*fvu3u17)/6 + (f93*fvu3u18)/6 + 
    (f93*fvu3u19)/9 - (f93*fvu3u22)/3 + (7*f93*fvu1u1u1^3)/18 - 
    (f93*fvu1u1u2^3)/3 - (f93*fvu1u1u3^3)/18 + 
    (f93*fvu1u1u5^3)/9 + fvu1u1u4^2*
     ((f54 + f58 + f62 - f67 + f70)/6 - (f93*fvu1u1u5)/9 + 
      (f93*fvu1u1u8)/18 - (f93*fvu1u1u9)/18) + 
    ((f92 - f96)*fvu2u1u2)/9 + ((f86 - f96)*fvu2u1u3)/9 + 
    ((f86 + f98)*fvu2u1u4)/9 + fvu1u1u9*((-2*f93*fvu2u1u11)/9 - 
      (2*f93*tci11^2)/27) + fvu1u1u8*((2*f93*fvu2u1u9)/9 + 
      (f93*tci11^2)/54) + ((-f60 + f65 + f73)*tci12)/9 + 
    (f93*fvu1u1u9^2*tci12)/18 + 
    ((f86 - f96)*fvu1u2u6*tci12)/9 - (4*f93*fvu2u1u9*tci12)/
     9 + tci11^2*((21*f58 + 18*f62 + 18*f70 + 2*f92 - 2*f96 + 
        2*f98)/108 + ((f52 - 3*f78 - f84 + 2*f89 - 4*f93)*
        tci12)/36) + fvu2u1u1*((-f81 + f92)/9 - 
      (8*f93*tci12)/9) + fvu1u1u1^2*
     ((-3*f54 - f81 + 2*f92 - f96)/18 + (f93*fvu1u1u2)/6 - 
      (f93*fvu1u1u3)/18 - (f93*fvu1u1u4)/6 - (f93*fvu1u1u8)/9 - 
      (5*f93*tci12)/6) + fvu2u1u5*((f81 + f98)/9 - 
      (f93*tci12)/3) + fvu1u1u2^2*
     ((-3*f62 + 2*f86 - f96 + f98)/18 + (f93*fvu1u1u4)/6 + 
      (f93*fvu1u1u5)/18 + (f93*fvu1u1u9)/9 - (f93*tci12)/6) + 
    fvu1u1u5^2*(-f70/6 - (f93*fvu1u1u9)/6 + (f93*tci12)/9) + 
    fvu1u1u3^2*((3*f67 + f81 + f98)/18 + (f93*fvu1u1u4)/9 + 
      (f93*fvu1u1u8)/6 + (f93*tci12)/2) + 
    fvu1u2u7*((f93*tci11^2)/3 + ((-f81 - f98)*tci12)/9) + 
    fvu1u1u5*(f73/9 - (f93*fvu1u1u9^2)/6 - (4*f93*fvu2u1u4)/9 + 
      (f93*fvu2u1u5)/3 - (13*f93*tci11^2)/108 - 
      (f86*tci12)/9 - (f93*fvu1u1u9*tci12)/3 - 
      (f93*fvu1u2u7*tci12)/3) + 
    fvu1u1u4*((-16*f7 + 2*f8 + 16*f11 - 2*f14 - 16*f15 + 
        2*f17 + 36*f19 - 4*f22 - 16*f25 + 2*f28 + 16*f54 - 
        4*f56 + 4*f59 + 2*f60 + 16*f62 - 4*f65 + 16*f67 + 
        8*f68 - f69 - 8*f70 - f73 - 2*f104 + 16*f105 - 2*f106 - 
        16*f117 + 2*f118 + f120 - 16*f121 + 2*f122 + 2*f124 - 
        32*f127 + 4*f128 - 16*f129 - 8*f130 + 2*f131 + f132 - 
        16*f148 + 2*f149 + 4*f157 - 16*f160 + 2*f161 - 8*f165 + 
        f166 + f176 - 32*f180 + 4*f181)/18 - (f93*fvu1u1u5^2)/9 + 
      (f93*fvu1u1u8^2)/18 - (f93*fvu1u1u9^2)/18 + 
      ((-f54 - f58 - f62 + f67 - f70)*tci12)/3 - 
      (f93*fvu1u1u8*tci12)/9 + (f93*fvu1u1u9*tci12)/9 + 
      fvu1u1u5*(f86/9 + (f93*fvu1u1u9)/3 + (2*f93*tci12)/9)) + 
    fvu1u1u3*((16*f7 - 2*f8 - 16*f11 + 2*f14 + 16*f15 - 2*f17 - 
        36*f19 + 4*f22 + 16*f25 - 2*f28 - 16*f54 + 2*f56 - 
        4*f59 - 16*f62 + 2*f65 - 16*f67 - 8*f68 + f69 + 8*f70 - 
        f73 + 2*f104 - 16*f105 + 2*f106 + 16*f117 - 2*f118 - 
        f120 + 16*f121 - 2*f122 - 2*f124 + 32*f127 - 4*f128 + 
        16*f129 + 8*f130 - 2*f131 - f132 + 16*f148 - 2*f149 - 
        4*f157 + 16*f160 - 2*f161 + 8*f165 - f166 - f176 + 
        32*f180 - 4*f181)/18 + (f93*fvu1u1u4^2)/9 - 
      (f93*fvu1u1u5^2)/6 + (f93*fvu1u1u8^2)/6 + 
      (4*f93*fvu2u1u1)/9 + (f93*fvu2u1u5)/3 - 
      (17*f93*tci11^2)/27 - (f67*tci12)/3 - 
      (f93*fvu1u1u8*tci12)/3 - (f93*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(f92/9 - (f93*fvu1u1u8)/3 - (2*f93*tci12)/9) + 
      fvu1u1u5*(-f81/9 + (f93*tci12)/3)) + 
    fvu1u1u2*(f65/9 - (f93*fvu1u1u3^2)/6 + (f93*fvu1u1u4^2)/6 + 
      (5*f93*fvu1u1u5^2)/18 + (2*f93*fvu1u1u9^2)/9 - 
      (4*f93*fvu2u1u4)/9 - (2*f93*fvu2u1u11)/9 + 
      (f93*tci11^2)/9 + (f98*tci12)/9 + 
      (2*f93*fvu1u1u9*tci12)/9 + fvu1u1u4*
       (-f86/9 - (f93*fvu1u1u5)/9 - (2*f93*fvu1u1u9)/9 - 
        (f93*tci12)/3) + fvu1u1u5*(-f86/9 + (f93*tci12)/9) + 
      fvu1u1u3*(-f98/9 + (f93*tci12)/3)) + 
    fvu1u1u1*(f56/9 - (5*f93*fvu1u1u3^2)/18 - 
      (f93*fvu1u1u4^2)/6 + (f81*fvu1u1u5)/9 + 
      (f93*fvu1u1u5^2)/6 - (2*f93*fvu1u1u8^2)/9 + 
      (4*f93*fvu2u1u1)/9 + (2*f93*fvu2u1u9)/9 + 
      (f93*tci11^2)/3 + ((3*f54 + f81 + f96)*tci12)/9 + 
      (4*f93*fvu1u1u8*tci12)/9 + fvu1u1u2*
       (f96/9 - (f93*tci12)/3) + fvu1u1u4*
       (-f92/9 + (2*f93*fvu1u1u8)/9 + (f93*tci12)/3) + 
      fvu1u1u3*(-f92/9 + (f93*fvu1u1u4)/9 + (5*f93*tci12)/9)) + 
    ((-3*f78 + 2*f89 + 2*f93)*tci12*tcr11^2)/6 + 
    ((f78 - 4*f89 - 6*f93)*tcr11^3)/18 + 
    ((-6*f52 - 7*f78 + 6*f84 - 2*f89 - 19*f93)*tci11^2*
      tcr12)/36 + ((f78 + 2*f89 + f93)*tcr12^3)/9 - 
    (2*(f78 - f89 - f93)*tci12*tcr21)/3 + 
    tcr11*(((7*f78 - 5*f93)*tci11^2)/36 + 
      ((f78 + f93)*tci12*tcr12)/3 - (2*(f89 + f93)*tcr21)/
       3) + ((-f78 - 2*f89 - f93)*tcr31)/3 + 
    ((-f78 - 2*f89 - f93)*tcr32)/12 + 
    ((-18*f52 + 11*f78 + 18*f84 + 74*f89 + 11*f93)*tcr33)/72;
L ieu1ueu1 = -f60/9 + ((-f78 + 2*f89 - f93)*fvu1u1u1^2)/
     6 + ((f52 + 2*f84 - f93)*fvu1u1u2^2)/6 + 
    ((f52 + f78)*fvu1u1u3^2)/6 + 
    fvu1u1u3*(-f67/3 + (f89*fvu1u1u4)/3 - (f78*fvu1u1u5)/3) + 
    fvu1u1u4*((-f54 - f58 - f62 + f67 - f70)/3 + 
      (f84*fvu1u1u5)/3) + ((-f78 + f89)*fvu2u1u1)/3 + 
    ((f89 - f93)*fvu2u1u2)/3 + ((f84 - f93)*fvu2u1u3)/3 + 
    ((f52 + f84)*fvu2u1u4)/3 + ((f52 + f78)*fvu2u1u5)/3 + 
    ((f52 + f89 - f93)*tci11^2)/18 + 
    ((f58 + f62 + f70)*tci12)/3 + 
    ((f84 - f93)*fvu1u2u6*tci12)/3 + 
    ((-f52 - f78)*fvu1u2u7*tci12)/3 + 
    fvu1u1u2*(f62/3 - (f52*fvu1u1u3)/3 - (f84*fvu1u1u4)/3 - 
      (f84*fvu1u1u5)/3 + (f52*tci12)/3) + 
    fvu1u1u5*(f70/3 - (f84*tci12)/3) + 
    fvu1u1u1*(f54/3 + (f93*fvu1u1u2)/3 - (f89*fvu1u1u3)/3 - 
      (f89*fvu1u1u4)/3 + (f78*fvu1u1u5)/3 + 
      ((f78 + f93)*tci12)/3);
L ieu0ueu1 = f58/3;
L ieum1ueu1 = 0;
L ieum2ueu1 = 0;
L ieu2uou1 = (5*f75*fvu4u25)/3 - (f75*fvu4u28)/3 - 
    (7*f75*fvu4u39)/3 - (f75*fvu4u41)/3 - (f75*fvu4u49)/3 - 
    (5*f75*fvu4u51)/9 + (5*f75*fvu4u80)/12 - (37*f75*fvu4u83)/12 - 
    (7*f75*fvu4u91)/4 + (5*f75*fvu4u93)/12 + (f75*fvu4u100)/3 - 
    (23*f75*fvu4u102)/18 + (f75*fvu4u111)/4 + (f75*fvu4u113)/2 + 
    f75*fvu4u114 - (8*f75*fvu4u129)/3 + (7*f75*fvu4u132)/3 - 
    (f75*fvu4u139)/6 - (8*f75*fvu4u141)/3 + (4*f75*fvu4u146)/9 + 
    (f75*fvu4u148)/18 + (5*f75*fvu4u171)/12 - (f75*fvu4u174)/12 - 
    (f75*fvu4u182)/12 + (5*f75*fvu4u184)/12 + (f75*fvu4u190)/9 - 
    (f75*fvu4u192)/18 + (f75*fvu4u199)/4 + (f75*fvu4u201)/2 + 
    f75*fvu4u202 + (7*f75*fvu4u213)/12 - (f75*fvu4u215)/4 + 
    (9*f75*fvu4u219)/4 + (7*f75*fvu4u221)/12 - (4*f75*fvu4u225)/9 - 
    (f75*fvu4u233)/4 - (f75*fvu4u234)/2 - f75*fvu4u235 + 
    (17*f75*fvu4u244)/3 + (4*f75*fvu4u246)/3 - (7*f75*fvu4u252)/2 - 
    (17*f75*fvu4u255)/18 - (5*f75*fvu4u271)/12 - 
    (43*f75*fvu4u273)/12 + (67*f75*fvu4u277)/12 - 
    (5*f75*fvu4u279)/12 - (f75*fvu4u282)/3 - (f75*fvu4u289)/4 - 
    (f75*fvu4u290)/2 - f75*fvu4u291 + (4*f75*fvu4u293)/3 - 
    (2*f75*fvu4u295)/3 + 2*f75*fvu4u313 + 2*f75*fvu4u330 + 
    fvu3u71*(-(f75*fvu1u1u2)/3 + (f75*fvu1u1u3)/3 + 
      (f75*fvu1u1u5)/3 - (f75*fvu1u1u6)/3 + (f75*fvu1u1u7)/3 + 
      (f75*fvu1u1u8)/3) + fvu3u78*((2*f75*fvu1u1u2)/3 - 
      (2*f75*fvu1u1u3)/3 - (2*f75*fvu1u1u9)/3) + 
    fvu3u70*((f75*fvu1u1u2)/6 - (f75*fvu1u1u3)/6 - 
      (f75*fvu1u1u9)/6) + fvu3u45*(-(f75*fvu1u1u2)/2 - 
      (7*f75*fvu1u1u3)/6 - (2*f75*fvu1u1u6)/3 + 
      (2*f75*fvu1u1u7)/3 + (f75*fvu1u1u9)/2) + 
    fvu3u43*(-(f75*fvu1u1u2)/6 - (f75*fvu1u1u4)/3 - 
      (f75*fvu1u1u6)/6 + (f75*fvu1u1u7)/6 + (f75*fvu1u1u9)/2 - 
      (f75*fvu1u1u10)/3) + fvu3u63*((2*f75*fvu1u1u2)/3 + 
      (3*f75*fvu1u1u3)/2 - (f75*fvu1u1u5)/3 + (f75*fvu1u1u6)/2 - 
      (f75*fvu1u1u7)/3 - (f75*fvu1u1u8)/3 - (f75*fvu1u1u9)/3 - 
      (f75*fvu1u1u10)/6) + fvu3u25*(-(f75*fvu1u1u2)/3 - 
      (7*f75*fvu1u1u3)/6 + f75*fvu1u1u4 - (2*f75*fvu1u1u5)/3 + 
      (5*f75*fvu1u1u6)/6 - (f75*fvu1u1u7)/6 + 
      (f75*fvu1u1u10)/3) + fvu3u81*((f75*fvu1u1u3)/6 + 
      (f75*fvu1u1u4)/3 + (f75*fvu1u1u6)/6 - (f75*fvu1u1u7)/6 - 
      (f75*fvu1u1u9)/3 + (f75*fvu1u1u10)/3) + 
    fvu3u62*((-4*f75*fvu1u1u2)/3 + (2*f75*fvu1u1u4)/3 + 
      (2*f75*fvu1u1u5)/3 - (4*f75*fvu1u1u6)/3 + 
      (2*f75*fvu1u1u7)/3 + (4*f75*fvu1u1u10)/3) + 
    (13*f75*tci11^3*tci12)/15 + 
    fvu3u23*((f75*fvu1u1u1)/3 + (f75*fvu1u1u2)/6 + 
      f75*fvu1u1u3 + f75*fvu1u1u4 + f75*fvu1u1u5 - 
      (f75*fvu1u1u6)/2 - (f75*fvu1u1u7)/6 - (f75*fvu1u1u8)/3 - 
      (f75*fvu1u1u9)/6 - (f75*fvu1u1u10)/3 - 2*f75*tci12) + 
    fvu3u82*((-2*f75*fvu1u1u3)/3 - (2*f75*fvu1u1u4)/3 + 
      f75*fvu1u1u5 + f75*fvu1u1u6 - (f75*fvu1u1u7)/3 + 
      (f75*fvu1u1u8)/3 - (f75*fvu1u1u9)/3 + (4*f75*tci12)/3) + 
    fvu3u80*((2*f75*fvu1u1u2)/3 + f75*fvu1u1u3 - 
      (19*f75*fvu1u1u6)/6 + (19*f75*fvu1u1u7)/6 - 
      (17*f75*fvu1u1u8)/6 + (13*f75*fvu1u1u9)/6 + 
      (17*f75*tci12)/6) - (10*f75*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f75*tci11^3)/81 + (17*f75*tci12*tci21)/
       3) + tci12*((16*f75*tci31)/5 + 16*f75*tci32) - 
    (2072*f75*tci41)/135 - (64*f75*tci42)/5 - 
    (16*f75*tci43)/3 + ((1259*f75*tci11^3)/1620 + 
      f75*tci12*tci21 + (84*f75*tci31)/5 + 
      16*f75*tci32)*tcr11 - (11*f75*tci11*tcr11^3)/180 + 
    fvu1u1u6*((f75*tci11^3)/810 + (58*f75*tci12*tci21)/9 + 
      (88*f75*tci31)/5 + (22*f75*tci21*tcr11)/3 - 
      (11*f75*tci11*tcr11^2)/30) + 
    fvu1u1u2*((181*f75*tci11^3)/1620 + (2*f75*tci12*tci21)/
       9 + (44*f75*tci31)/5 + (11*f75*tci21*tcr11)/3 - 
      (11*f75*tci11*tcr11^2)/60) + 
    fvu1u1u3*((73*f75*tci11^3)/1620 - (f75*tci12*tci21)/3 + 
      (12*f75*tci31)/5 + f75*tci21*tcr11 - 
      (f75*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f75*tci11^3)/540 - (49*f75*tci12*tci21)/
       9 - (4*f75*tci31)/5 - (f75*tci21*tcr11)/3 + 
      (f75*tci11*tcr11^2)/60) + 
    fvu1u1u4*((-31*f75*tci11^3)/270 + (4*f75*tci12*tci21)/
       3 - (24*f75*tci31)/5 - 2*f75*tci21*tcr11 + 
      (f75*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f75*tci11^3)/180 - (f75*tci12*tci21)/
       9 - (28*f75*tci31)/5 - (7*f75*tci21*tcr11)/3 + 
      (7*f75*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f75*tci11^3)/1620 - (19*f75*tci12*tci21)/
       3 - (36*f75*tci31)/5 - 3*f75*tci21*tcr11 + 
      (3*f75*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f75*fvu3u25)/6 + (f75*fvu3u43)/3 + 
      (7*f75*fvu3u45)/6 - (2*f75*fvu3u62)/3 - (7*f75*fvu3u63)/6 + 
      (f75*fvu3u70)/6 - (2*f75*fvu3u71)/3 + (2*f75*fvu3u78)/3 - 
      f75*fvu3u80 - (f75*fvu3u81)/2 - (f75*fvu3u82)/3 - 
      (179*f75*tci11^3)/1620 + (f75*tci12*tci21)/3 - 
      (36*f75*tci31)/5 - 3*f75*tci21*tcr11 + 
      (3*f75*tci11*tcr11^2)/20) + 
    fvu1u1u5*((-8*f75*tci11^3)/135 - (28*f75*tci12*tci21)/
       9 - (64*f75*tci31)/5 - (16*f75*tci21*tcr11)/3 + 
      (4*f75*tci11*tcr11^2)/15) + 
    ((-218*f75*tci11^3)/243 - (40*f75*tci12*tci21)/3)*
     tcr12 + (-4*f75*tci11*tci12 - (40*f75*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f75*tci11*tci12)/15 + 
      5*f75*tci21 - 2*f75*tci11*tcr12) + 
    (130*f75*tci11*tcr33)/27;
L ieu1uou1 = 
   2*f75*fvu3u23 - (11*f75*tci11^3)/135 - 
    (4*f75*tci12*tci21)/3 - (48*f75*tci31)/5 - 
    4*f75*tci21*tcr11 + (f75*tci11*tcr11^2)/5;
L ieu0uou1 = 0;
L ieum1uou1 = 0;
L ieum2uou1 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou1+w^2*ieu1uou1+w^3*ieu0uou1+w^4*ieum1uou1+w^5*ieum2uou1;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou1a = K[w^1];
L ieu1uou1a = K[w^2];
L ieu0uou1a = K[w^3];
L ieum1uou1a = K[w^4];
L ieum2uou1a = K[w^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_odd.c> "%O"
#write <e1_odd.c> "return Eps5o2<T>("
#write <e1_odd.c> "%E", ieu2uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieu0uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum1uou1a
#write <e1_odd.c> ", "
#write <e1_odd.c> "%E", ieum2uou1a
#write <e1_odd.c> ");\n}"
L H=+u^1*ieu2ueu1+u^2*ieu1ueu1+u^3*ieu0ueu1+u^4*ieum1ueu1+u^5*ieum2ueu1;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu1a = H[u^1];
L ieu1ueu1a = H[u^2];
L ieu0ueu1a = H[u^3];
L ieum1ueu1a = H[u^4];
L ieum2ueu1a = H[u^5];
.sort
#write <e1.tmp> "`optimmaxvar_'"
#write <e1_even.c> "%O"
#write <e1_even.c> "return Eps5o2<T>("
#write <e1_even.c> "%E", ieu2ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieu0ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum1ueu1a
#write <e1_even.c> ", "
#write <e1_even.c> "%E", ieum2ueu1a
#write <e1_even.c> ");\n}"
.end
