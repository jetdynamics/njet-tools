
Z[1]=tci21*fvu1u1u6;
Z[2]=tci12*tci21;
Z[3]=4*Z[2];
Z[4]=pow(tci11,3);
Z[5]=2*tci21;
Z[6]=fvu1u1u10*Z[5];
Z[6]=Z[6] + Z[3] - 2./9.*Z[4] - Z[1];
Z[7]=tcr11*tci11;
Z[8]= - fvu1u1u10 + 1./2.*fvu1u1u6 + tci12;
Z[8]=Z[8]*Z[7];
Z[6]=2*Z[6] + 1./5.*Z[8];
Z[6]=tcr11*Z[6];
Z[8]=fvu3u82 - fvu3u81;
Z[9]=Z[8] - fvu3u78;
Z[10]= - fvu3u71 - 8*Z[2] + 8./27.*Z[4] + Z[9];
Z[10]=fvu1u1u1*Z[10];
Z[9]=5*Z[2] - 5./27.*Z[4] - Z[9];
Z[9]=fvu1u1u8*Z[9];
Z[5]= - Z[5] + 1./10.*Z[7];
Z[5]=Z[5]*tcr11;
Z[7]=Z[5] + 1./3.*Z[2] - 7./90.*Z[4];
Z[11]=fvu3u43 + fvu3u82;
Z[12]=Z[11] + Z[7];
Z[12]=fvu1u1u9*Z[12];
Z[8]=Z[3] - 4./27.*Z[4] - fvu3u78 - Z[8];
Z[8]=fvu1u1u5*Z[8];
Z[11]=Z[11] + fvu3u71;
Z[7]= - 1./2.*fvu3u80 - fvu3u25 - fvu3u63 + Z[11] + Z[7];
Z[7]=fvu1u1u7*Z[7];
Z[5]= - fvu3u45 + fvu3u80 - 4./3.*Z[2] - fvu3u43 + 31./270.*Z[4] + 
fvu3u81 - fvu3u78 - Z[5] + fvu3u63;
Z[5]=fvu1u1u2*Z[5];
Z[13]=tcr12*tci12*tci11;
Z[13]=10./3.*Z[2] + Z[13];
Z[13]=tcr12*Z[13];
Z[13]=Z[13] - fvu4u213;
Z[1]= - 16*tci32 - 5./3.*Z[1] - 2*fvu3u78 - 151./135.*Z[4];
Z[1]=tci12*Z[1];
Z[11]= - 1./270.*Z[4] - Z[11];
Z[11]=fvu1u1u6*Z[11];
Z[3]=11./45.*Z[4] + Z[3];
Z[3]=fvu1u1u10*Z[3];
Z[2]= - 2*Z[2] + 2./27.*Z[4];
Z[4]=fvu3u78 - Z[2];
Z[4]=fvu3u80 + 2*Z[4] - fvu3u25 + fvu3u71;
Z[4]=fvu1u1u3*Z[4];
Z[14]=fvu4u80 + fvu4u330 - fvu4u325 - fvu4u215 + fvu4u221 - fvu4u139 + 
fvu4u28 + fvu4u91 - fvu4u295 + fvu4u83 + fvu4u39 + fvu4u93;
Z[15]=fvu4u100 - fvu4u282 - 2*tci41 - fvu4u51 - fvu4u148 + fvu4u225 - 
fvu4u102;
Z[16]= - fvu4u279 + fvu4u184 + fvu4u171;
Z[17]=fvu4u199 + fvu4u277 - fvu4u289;
Z[18]=fvu4u271 + fvu4u174 + fvu4u182;
Z[19]=fvu4u201 - fvu4u290;
Z[20]=tci21*pow(tci11,2);
Z[21]= - fvu1u1u6 - fvu1u1u9 + 2*fvu1u1u10;
Z[22]=fvu3u63*Z[21];
Z[23]=fvu1u1u6 + fvu1u1u1;
Z[23]=fvu3u25*Z[23];
Z[24]=fvu1u1u9 + tci12;
Z[24]=fvu1u1u8 + fvu1u1u6 - 3*Z[24];
Z[24]=1./2.*Z[24] - fvu1u1u5;
Z[24]=fvu3u80*Z[24];
Z[25]= - fvu1u1u3 + fvu1u1u1 + fvu1u1u9;
Z[25]=fvu3u45*Z[25];
Z[21]=fvu1u1u2 - fvu1u1u7 + Z[21] - 2*tci12;
Z[21]=tci31*Z[21];
Z[1]=24./5.*Z[21] + Z[5] + Z[7] + 9./4.*fvu4u273 + Z[25] + Z[4] + Z[24]
 + 7./6.*fvu4u192 + Z[23] + Z[8] + Z[22] + Z[12] + Z[6] + 1./3.*Z[3] + 
fvu4u291 + Z[9] + Z[10] - fvu4u244 - fvu4u202 - 2./9.*Z[20] + 3./2.*
fvu4u252 - 1./6.*fvu4u255 + Z[11] + Z[1] + 4*Z[13] - 1./2.*Z[19] + 11./
4.*Z[18] - 1./4.*Z[17] - 3./4.*Z[16] + 2./3.*Z[15] - 2*Z[14];
Z[1]=f88*Z[1];
Z[2]=fvu3u81 - Z[2];
Z[2]=2*f88*Z[2];

return Eps5o2<T>(
Z[1]
, 
Z[2]
, 
 0
, 
 0
, 
 0
);
}
