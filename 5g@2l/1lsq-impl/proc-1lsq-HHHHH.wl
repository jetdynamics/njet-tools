(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1lsq-impl/hel_HHHHH"];


data=Get["P1_HHHHH.m"];


epss[o_,p_]:=inveps[3-#,If[p==1,"e","o"],o]->data[[1,o,p,6-#]]&/@Range[5];
epsPe[o_]:=epss[o,1];
epsPo[o_]:=epss[o,2];


epsE=epsPe/@Range[12];


epsO=epsPo/@Range[12];


Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];


es=Transpose[{epsE,epsO}];


Export["e"<>ToString[#]<>".m",es[[#]]]&/@Range[12];
