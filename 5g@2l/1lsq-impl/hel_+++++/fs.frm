#-
#: WorkSpace 40M
#: Threads 60
S u,x1,x2,x3,x4,x5;
S y91;
S y90;
S y93;
S y92;
S y95;
S y94;
S y97;
S y79;
S y96;
S y78;
S y99;
S y77;
S y98;
S y76;
S y75;
S y74;
S y73;
S y72;
S y71;
S y70;
S y68;
S y69;
S y64;
S y65;
S y66;
S y67;
S y60;
S y61;
S y62;
S y63;
S y19;
S y2;
S y18;
S y3;
S y1;
S y6;
S y7;
S y88;
S y4;
S y89;
S y5;
S y86;
S y11;
S y87;
S y10;
S y84;
S y13;
S y8;
S y85;
S y9;
S y12;
S y82;
S y15;
S y83;
S y14;
S y80;
S y17;
S y81;
S y16;
S y109;
S y108;
S y103;
S y102;
S y101;
S y100;
S y107;
S y106;
S y105;
S y104;
S y33;
S y32;
S y31;
S y30;
S y37;
S y36;
S y35;
S y34;
S y110;
S y111;
S y112;
S y39;
S y113;
S y38;
S y114;
S y115;
S y116;
S y117;
S y55;
S y20;
S y54;
S y21;
S y57;
S y22;
S y56;
S y23;
S y51;
S y24;
S y50;
S y25;
S y53;
S y26;
S y52;
S y27;
S y28;
S y29;
S y59;
S y58;
S y42;
S y43;
S y40;
S y41;
S y46;
S y47;
S y44;
S y45;
S y48;
S y49;
ExtraSymbols,array,Z;

L f1 = (y1^5*y2^3*y3^2*y4^2*y6*y8)/(y5*y7^3);
L f2 = (y1^5*y2^2*y3^2*y11^2*y12)/(y7*y9*y10);
L f3 = y1^5*y2*y3*y13;
L f4 = (y1^5*y2^3*y3^2*y15)/
   (y7*y10*y14);
L f5 = (y1^5*y2^2*y3*y4^2*y16)/(y7*y9);
L f6 = (y1^5*y2^2*y3^2*y6^2*y17)/(y7*y14);
L f7 = (y1^5*y2^4*y3^3*y4^2*y6^2*y11^2*y18^2)/y7^4;
L f8 = (y1^5*y2^2*y3^3*y4*y6^2*y11^2*y20)/(y7^3*y19);
L f9 = (y1^5*y2^2*y3^3*y4*y6^2*y11^2*y21)/(y7^3*y19);
L f10 = (y1^5*y2^3*y3^2*y6^2*y18^2*y23)/(y7^3*y14^2*y22);
L f11 = (y1^5*y2^3*y3^2*y6^2*y18^2*y24)/(y7^3*y14^2*y22);
L f12 = (y1^5*y2^2*y3*y4^2*y11*y18^2*y26)/
   (y7^3*y9^2*y25);
L f13 = (y1^5*y2^2*y3*y4^2*y11*y18^2*y27)/
   (y7^3*y9^2*y25);
L f14 = (y1^5*y2^4*y3^3*y11^2*y18*y29)/
   (y7^3*y10^2*y28);
L f15 = (y1^5*y2^4*y3^3*y11^2*y18*y30)/(y7^3*y10^2*y28);
L f16 = (y1^5*y2^3*y3^2*y4^2*y6*y31)/(y5*y7^3);
L f17 = (y1^5*y2^2*y3^3*y11^2*y18*y32)/(y7^3*y10^2*y28);
L f18 = (y1^5*y3*y25^2*y33)/(y7*y14);
L f19 = (y1^5*y2^2*y3^2*y34)/(y7*y10*y14);
L f20 = (y1^5*y2*y3^2*y11^2*y35)/(y7*y9*y10);
L f21 = (y1^5*y3*y18^2*y36)/(y7*y10);
L f22 = (y1^5*y2*y3^3*y9*y11^2*y14*y18^2*y22^2*y25^2)/
   y7^4;
L f23 = (y1^5*y3*y9*y11*y18^2*y25^2*y37)/
   (y2*y4*y7^3);
L f24 = (y1^5*y3*y9*y11*y18^2*y25^2*
    y38)/(y2*y4*y7^3);
L f25 = (y1^5*y3^2*y14*y18^2*y22^2*y39)/(y6*y7^3);
L f26 = (y1^5*y3^2*y14*y18^2*y22^2*y40)/(y6*y7^3);
L f27 = (y1^5*y2*y3^2*y9*y22*y25^2*y41)/(y7^3*y14*y19);
L f28 = (y1^5*y2*y3^3*y11^2*y14*y22^2*y25*y42)/
   (y5*y7^3*y9);
L f29 = (y1^5*y2^2*y3*y5^2*y43)/
   (y7*y9*y10);
L f30 = (y1^5*y2^4*y3^2*y19^2*y22*y44)/
   (y7^3*y9^2*y14*y25);
L f31 = (y1^5*y2*y3^3*y11^2*y14*y22^2*y25*y45)/
   (y5*y7^3*y9);
L f32 = (y1^5*y2^2*y3*y4^2*y46)/
   (y7*y10);
L f33 = (y1^5*y2*y3^2*y9*y22*y25^2*y47)/
   (y7^3*y14*y19);
L f34 = (y1^5*y2^2*y3^2*y48)/(y7*y14);
L f35 = (y1^5*y2*y3^2*y49)/y14;
L f36 = (y1^5*y2*y3*y25^2*y50)/(y7*y10*y14);
L f37 = (y1^5*y2^3*y3^2*y51)/(y7*y9*y14);
L f38 = (y1^5*y2*y3*y22^2*y52)/(y7*y9);
L f39 = (y1^5*y2^2*y3*y19^2*y53)/(y7*y9*y10*y14);
L f40 = (y1^5*y2^3*y3^2*y5^2*y6^2*y9*y14*y22^2*y25^2)/
   y7^4;
L f41 = (y1^5*y2*y3^2*y22^2*y54)/(y7*y9*y10);
L f42 = (y1^5*y2*y5^2*y14*y22^2*y25*y26)/(y7^3*y9*y11);
L f43 = (y1^5*y2^4*y3^2*y4^2*y6^2*y14*y19^2*y22^2)/y7^4;
L f44 = (y1^5*y2*y5^2*y14*y22^2*y25*y27)/(y7^3*y9*y11);
L f45 = (y1^5*y2^2*y3^2*y4^2*y14*y19*y22^2*y55)/
   (y7^3*y10^2*y28);
L f46 = (y1^5*y2^3*y3^2*y6^2*y14*y22^2*y56)/(y7^3*y18);
L f47 = (y1^5*y2^2*y3^2*y4^2*y14*y19*y22^2*y57)/
   (y7^3*y10^2*y28);
L f48 = (y1^5*y2^3*y3^2*y6^2*y14*y22^2*y58)/(y7^3*y18);
L f49 = (y1^5*y2^3*y3^2*y5*y6^2*y9*y25^2*y59)/
   (y7^3*y10^2*y28);
L f50 = (y1^5*y2^3*y3^2*y6^2*y14*y22^2*y60)/(y7^3*y18);
L f51 = (y1^5*y2^3*y3^2*y5*y6^2*y9*y25^2*y61)/
   (y7^3*y10^2*y28);
L f52 = (y1^5*y2^2*y4*y6^2*y19^2*y37)/
   (y7^3*y11);
L f53 = (y1^5*y2^3*y3^2*y5^2*y6*y62)/
   (y4*y7^3);
L f54 = (y1^5*y2^2*y4*y6^2*y19^2*y38)/
   (y7^3*y11);
L f55 = (y1^5*y2^3*y3^2*y5^2*y6*y63)/
   (y4*y7^3);
L f56 = (y1^5*y2^3*y3^2*y4^2*y6*y64)/
   (y5*y7^3);
L f57 = (y1^5*y2*y3^2*y9*y22*y25^2*y65)/
   (y7^3*y14*y19);
L f58 = (y1^5*y2^4*y3^2*y19^2*y22*y66)/
   (y7^3*y9^2*y14*y25);
L f59 = (y1^5*y2^3*y3^2*y4^2*y5^2*y6*y39)/y7^3;
L f60 = (y1^5*y2^2*y3^2*y11^2*y67)/(y7*y9*y14);
L f61 = (y1^5*y2*y3*y68)/y9;
L f62 = (y1^5*y2^3*y3^2*y5^2*y69)/(y7*y9*y10*y14);
L f63 = (y1^5*y2^2*y3*y4^2*y70)/y7;
L f64 = (y1^5*y2^2*y3^3*y4*y11^2*y19^2*y62)/(y6*y7^3);
L f65 = (y1^5*y2^2*y3^2*y6^2*y71)/(y7*y10);
L f66 = (y1^5*y2*y3*y25^2*y72)/(y7*y14);
L f67 = (y1^5*y2^4*y3^3*y4^2*y5^2*y6^2*y9*y11^2*y25^2)/
   y7^4;
L f68 = (y1^5*y2*y3*y73)/(y9*y14);
L f69 = (y1^5*y2^2*y3^3*y4*y6^2*y11^2*y74)/(y7^3*y19);
L f70 = (y1^5*y2^2*y3^2*y19^2*y75)/(y7*y10*y14);
L f71 = (y1^5*y2^2*y3^2*y11^2*y76)/(y7*y9);
L f72 = (y1^5*y2^3*y3^2*y5*y6^2*y9*y25^2*y77)/
   (y7^3*y10^2*y28);
L f73 = (y1^5*y2^2*y3*y4^2*y78)/
   (y7*y10);
L f74 = (y1^5*y2^2*y3^3*y4^2*y9*y11^2*y14*
    y19^2*y22^2*y25^2)/y7^4;
L f75 = (y1^5*y2^2*y3*y4^2*y9*y11*y25^2*y79)/
   (y7^3*y18);
L f76 = (y1^5*y2^2*y3*y4^2*y9*y11*y25^2*
    y80)/(y7^3*y18);
L f77 = (y1^5*y2^2*y3*y4^2*y9*y11*y25^2*y81)/
   (y7^3*y18);
L f78 = (y1^5*y2^4*y3^3*y5^2*y11^2*y25*y44)/
   (y7^3*y9*y14^2*y22);
L f79 = (y1^5*y2^2*y3^2*y4^2*y14*y19*y22^2*y82)/
   (y7^3*y10^2*y28);
L f80 = (y1^5*y2^4*y3^3*y5^2*y11^2*y25*y66)/
   (y7^3*y9*y14^2*y22);
L f81 = (y1^5*y2^3*y3^2*y4^2*y5^2*y6*y40)/y7^3;
L f82 = (y1^5*y2*y3^3*y11^2*y14*y22^2*y25*y83)/
   (y5*y7^3*y9);
L f83 = (y1^5*y2*y3^2*y9*y19^2*y22*y23*
    y25^2)/(y7^3*y14);
L f84 = (y1^5*y2^2*y3^3*y4*y11^2*y19^2*y63)/(y6*y7^3);
L f85 = (y1^5*y2*y10*y18*y28^2*y80)/(y7^3*y11);
L f86 = (y1^5*y2*y3^2*y19^2*y84)/(y7*y9*y10*y14);
L f87 = (y1^5*y2^2*y3*y85)/(y7*y9*y14);
L f88 = (y1^5*y3*y28^2*y86)/y7;
L f89 = (y1^5*y2*y3*y18^2*y87)/(y7*y10);
L f90 = (y1^5*y2*y3^2*y10*y14*y18^2*y19^2*y22^2*y28^2)/
   y7^4;
L f91 = (y1^5*y2*y3^2*y18^2*y19^2*y28*y59)/
   (y5*y7^3*y10);
L f92 = (y1^5*y2*y3^2*y18^2*y19^2*y28*
    y61)/(y5*y7^3*y10);
L f93 = (y1^5*y3^2*y14*y18^2*y22^2*y88)/(y6*y7^3);
L f94 = (y1^5*y3^2*y10*y14*y19*y20*y22^2*y28^2)/
   (y2*y4*y7^3);
L f95 = (y1^5*y3^2*y10*y14*y19*y21*
    y22^2*y28^2)/(y2*y4*y7^3);
L f96 = (y1^5*y2^2*y3^2*y19^2*y22*y89)/
   (y7^3*y9^2*y14*y25);
L f97 = (y1^5*y2^2*y3^3*y4*y11^2*y19^2*y90)/(y6*y7^3);
L f98 = (y1^5*y2*y3*y18^2*y91)/(y7*y9*y10);
L f99 = (y1^5*y2*y3*y92)/y10;
L f100 = (y1^5*y2^3*y3^2*y5^2*y6*y90)/(y4*y7^3);
L f101 = (y1^5*y2^2*y3^2*y19^2*y93)/(y7*y10*y14);
L f102 = (y1^5*y2*y3*y28^2*y94)/(y7*y9);
L f103 = (y1^5*y2^2*y3^2*y11^2*y95)/(y7*y9);
L f104 = (y1^5*y2^2*y3*y4^2*y96)/(y7*y9*y14);
L f105 = (y1^5*y2^2*y3*y97)/(y7*y14);
L f106 = (y1^5*y2^2*y3^3*y4^2*y10*y11^2*y18^2*y19^2*
    y28^2)/y7^4;
L f107 = (y1^5*y2^2*y3^2*y5^2*y98)/
   (y7*y9*y10);
L f108 = (y1^5*y2^2*y3*y4^2*y11*y18^2*
    y99)/(y7^3*y9^2*y25);
L f109 = (y1^5*y2^3*y3^2*y6^2*y100)/(y7*y9*y14);
L f110 = (y1^5*y2^3*y3^2*y5^2*y6^2*y10*y18^2*y28^2)/y7^4;
L f111 = (y1^5*y2^2*y3^2*y4^2*y10*y19*y28^2*y47)/
   (y7^3*y14^2*y22);
L f112 = (y1^5*y2^3*y3^2*y5*y6^2*y10*y28^2*y42)/
   (y7^3*y9^2*y25);
L f113 = (y1^5*y2^3*y3^2*y5*y6^2*y10*y28^2*y45)/
   (y7^3*y9^2*y25);
L f114 = (y1^5*y2*y3^3*y10*y11^2*y18*y28^2*y58)/y7^3;
L f115 = (y1^5*y2^3*y3^2*y6^2*y18^2*y101)/
   (y7^3*y14^2*y22);
L f116 = (y1^5*y2*y3^2*y18^2*y19^2*y28*y77)/
   (y5*y7^3*y10);
L f117 = (y1^5*y2*y3^2*y5^2*y18^2*y28*
    y55)/(y7^3*y10*y19);
L f118 = (y1^5*y2*y3^2*y5^2*y18^2*y28*y57)/
   (y7^3*y10*y19);
L f119 = (y1^5*y2*y10*y18*y28^2*y79)/
   (y7^3*y11);
L f120 = (y1^5*y2*y10*y18*y28^2*y81)/
   (y7^3*y11);
L f121 = (y1^5*y2^2*y3^2*y5^2*y19^2*y28*
    y32)/(y7^3*y10*y18);
L f122 = (y1^5*y3*y22^2*y102)/
   (y7*y9);
L f123 = (y1^5*y2^2*y3*y5^2*y103)/
   (y7*y9*y10);
L f124 = (y1^5*y2*y3^2*y19^2*y104)/
   (y7*y10*y14);
L f125 = (y1^5*y3^2*y28^2*y105)/y7;
L f126 = (y1^5*y2*y3^2*y5^2*y9*y10*y14*y19^2*y22^2*
    y25^2*y28^2)/y7^4;
L f127 = (y1^5*y3^2*y10*y14*y19*y22^2*y28^2*y74)/
   (y2*y4*y7^3);
L f128 = (y1^5*y3^2*y5*y9*y10*y25^2*
    y28^2*y64)/(y6*y7^3);
L f129 = (y1^5*y2*y3^2*y5^2*y18^2*y28*y82)/
   (y7^3*y10*y19);
L f130 = (y1^5*y2*y3^2*y11^2*y106)/
   (y7*y9*y14);
L f131 = (y1^5*y2*y3^2*y9*y19^2*y22*
    y25^2*y101)/(y7^3*y14);
L f132 = (y1^5*y2*y3^2*y9*y19^2*y22*y25^2*y107)/
   (y7^3*y14);
L f133 = (y1^5*y2^2*y3^2*y5^2*y108)/
   (y7*y9*y10*y14);
L f134 = (y1^5*y2*y5^2*y14*y22^2*y25*y99)/
   (y7^3*y9*y11);
L f135 = (y1^5*y3*y18^2*y109)/(y7*y10);
L f136 = (y1^5*y2*y3^2*y28^2*y110)/y7;
L f137 = (y1^5*y2*y3^3*y5^2*y9*y10*y11^2*y18^2*y25^2*
    y28^2)/y7^4;
L f138 = (y1^5*y2*y3^3*y10*y11^2*y18*
    y28^2*y56)/y7^3;
L f139 = (y1^5*y2*y3^3*y10*y11^2*y18*y28^2*y111)/y7^3;
L f140 = (y1^5*y3^2*y5*y8*y9*y10*y25^2*y28^2)/
   (y6*y7^3);
L f141 = (y1^5*y3^2*y5*y9*y10*y25^2*y28^2*
    y31)/(y6*y7^3);
L f142 = (y1^5*y3*y9*y11*y18^2*y25^2*y112)/
   (y2*y4*y7^3);
L f143 = (y1^5*y2^2*y3^3*y5^2*y11^2*y25*
    y89)/(y7^3*y9*y14^2*y22);
L f144 = (y1^5*y2^4*y3^2*y5^2*y19^2*y28*y29)/
   (y7^3*y10*y18);
L f145 = (y1^5*y2^2*y3*y4^2*y113)/
   (y7*y14);
L f146 = (y1^5*y2*y3^2*y114)/y10;
L f147 = (y1^5*y2^3*y3^2*y5^2*y115)/(y7*y9*y10);
L f148 = (y1^5*y2^2*y3*y19^2*y116)/(y7*y10*y14);
L f149 = (y1^5*y2*y3^2*y28^2*y117)/(y7*y9*y14);
L f150 = (y1^5*y2^4*y3^2*y4^2*y5^2*y6^2*y10*y19^2*y28^2)/
   y7^4;
L f151 = (y1^5*y2^2*y3^2*y4^2*y10*y19*y28^2*y41)/
   (y7^3*y14^2*y22);
L f152 = (y1^5*y2^2*y3^2*y4^2*y10*y19*y28^2*y65)/
   (y7^3*y14^2*y22);
L f153 = (y1^5*y2^3*y3^2*y5*y6^2*y10*y28^2*y83)/
   (y7^3*y9^2*y25);
L f154 = (y1^5*y2^2*y4*y6^2*y19^2*y112)/(y7^3*y11);
L f155 = (y1^5*y2^3*y3^2*y4^2*y5^2*y6*y88)/y7^3;
L f156 = (y1^5*y2^4*y3^2*y5^2*y19^2*y28*y30)/
   (y7^3*y10*y18);


.sort
Format O4;
Format C;
L H=+u^1*f1+u^2*f2+u^3*f3+u^4*f4+u^5*f5+u^6*f6+u^7*f7+u^8*f8+u^9*f9+u^10*f10+u^11*f11+u^12*f12+u^13*f13+u^14*f14+u^15*f15+u^16*f16+u^17*f17+u^18*f18+u^19*f19+u^20*f20+u^21*f21+u^22*f22+u^23*f23+u^24*f24+u^25*f25+u^26*f26+u^27*f27+u^28*f28+u^29*f29+u^30*f30+u^31*f31+u^32*f32+u^33*f33+u^34*f34+u^35*f35+u^36*f36+u^37*f37+u^38*f38+u^39*f39+u^40*f40+u^41*f41+u^42*f42+u^43*f43+u^44*f44+u^45*f45+u^46*f46+u^47*f47+u^48*f48+u^49*f49+u^50*f50+u^51*f51+u^52*f52+u^53*f53+u^54*f54+u^55*f55+u^56*f56+u^57*f57+u^58*f58+u^59*f59+u^60*f60+u^61*f61+u^62*f62+u^63*f63+u^64*f64+u^65*f65+u^66*f66+u^67*f67+u^68*f68+u^69*f69+u^70*f70+u^71*f71+u^72*f72+u^73*f73+u^74*f74+u^75*f75+u^76*f76+u^77*f77+u^78*f78+u^79*f79+u^80*f80+u^81*f81+u^82*f82+u^83*f83+u^84*f84+u^85*f85+u^86*f86+u^87*f87+u^88*f88+u^89*f89+u^90*f90+u^91*f91+u^92*f92+u^93*f93+u^94*f94+u^95*f95+u^96*f96+u^97*f97+u^98*f98+u^99*f99+u^100*f100+u^101*f101+u^102*f102+u^103*f103+u^104*f104+u^105*f105+u^106*f106+u^107*f107+u^108*f108+u^109*f109+u^110*f110+u^111*f111+u^112*f112+u^113*f113+u^114*f114+u^115*f115+u^116*f116+u^117*f117+u^118*f118+u^119*f119+u^120*f120+u^121*f121+u^122*f122+u^123*f123+u^124*f124+u^125*f125+u^126*f126+u^127*f127+u^128*f128+u^129*f129+u^130*f130+u^131*f131+u^132*f132+u^133*f133+u^134*f134+u^135*f135+u^136*f136+u^137*f137+u^138*f138+u^139*f139+u^140*f140+u^141*f141+u^142*f142+u^143*f143+u^144*f144+u^145*f145+u^146*f146+u^147*f147+u^148*f148+u^149*f149+u^150*f150+u^151*f151+u^152*f152+u^153*f153+u^154*f154+u^155*f155+u^156*f156;
B u;
.sort
#optimize H
B u;
.sort
L f1a = H[u^1];
L f2a = H[u^2];
L f3a = H[u^3];
L f4a = H[u^4];
L f5a = H[u^5];
L f6a = H[u^6];
L f7a = H[u^7];
L f8a = H[u^8];
L f9a = H[u^9];
L f10a = H[u^10];
L f11a = H[u^11];
L f12a = H[u^12];
L f13a = H[u^13];
L f14a = H[u^14];
L f15a = H[u^15];
L f16a = H[u^16];
L f17a = H[u^17];
L f18a = H[u^18];
L f19a = H[u^19];
L f20a = H[u^20];
L f21a = H[u^21];
L f22a = H[u^22];
L f23a = H[u^23];
L f24a = H[u^24];
L f25a = H[u^25];
L f26a = H[u^26];
L f27a = H[u^27];
L f28a = H[u^28];
L f29a = H[u^29];
L f30a = H[u^30];
L f31a = H[u^31];
L f32a = H[u^32];
L f33a = H[u^33];
L f34a = H[u^34];
L f35a = H[u^35];
L f36a = H[u^36];
L f37a = H[u^37];
L f38a = H[u^38];
L f39a = H[u^39];
L f40a = H[u^40];
L f41a = H[u^41];
L f42a = H[u^42];
L f43a = H[u^43];
L f44a = H[u^44];
L f45a = H[u^45];
L f46a = H[u^46];
L f47a = H[u^47];
L f48a = H[u^48];
L f49a = H[u^49];
L f50a = H[u^50];
L f51a = H[u^51];
L f52a = H[u^52];
L f53a = H[u^53];
L f54a = H[u^54];
L f55a = H[u^55];
L f56a = H[u^56];
L f57a = H[u^57];
L f58a = H[u^58];
L f59a = H[u^59];
L f60a = H[u^60];
L f61a = H[u^61];
L f62a = H[u^62];
L f63a = H[u^63];
L f64a = H[u^64];
L f65a = H[u^65];
L f66a = H[u^66];
L f67a = H[u^67];
L f68a = H[u^68];
L f69a = H[u^69];
L f70a = H[u^70];
L f71a = H[u^71];
L f72a = H[u^72];
L f73a = H[u^73];
L f74a = H[u^74];
L f75a = H[u^75];
L f76a = H[u^76];
L f77a = H[u^77];
L f78a = H[u^78];
L f79a = H[u^79];
L f80a = H[u^80];
L f81a = H[u^81];
L f82a = H[u^82];
L f83a = H[u^83];
L f84a = H[u^84];
L f85a = H[u^85];
L f86a = H[u^86];
L f87a = H[u^87];
L f88a = H[u^88];
L f89a = H[u^89];
L f90a = H[u^90];
L f91a = H[u^91];
L f92a = H[u^92];
L f93a = H[u^93];
L f94a = H[u^94];
L f95a = H[u^95];
L f96a = H[u^96];
L f97a = H[u^97];
L f98a = H[u^98];
L f99a = H[u^99];
L f100a = H[u^100];
L f101a = H[u^101];
L f102a = H[u^102];
L f103a = H[u^103];
L f104a = H[u^104];
L f105a = H[u^105];
L f106a = H[u^106];
L f107a = H[u^107];
L f108a = H[u^108];
L f109a = H[u^109];
L f110a = H[u^110];
L f111a = H[u^111];
L f112a = H[u^112];
L f113a = H[u^113];
L f114a = H[u^114];
L f115a = H[u^115];
L f116a = H[u^116];
L f117a = H[u^117];
L f118a = H[u^118];
L f119a = H[u^119];
L f120a = H[u^120];
L f121a = H[u^121];
L f122a = H[u^122];
L f123a = H[u^123];
L f124a = H[u^124];
L f125a = H[u^125];
L f126a = H[u^126];
L f127a = H[u^127];
L f128a = H[u^128];
L f129a = H[u^129];
L f130a = H[u^130];
L f131a = H[u^131];
L f132a = H[u^132];
L f133a = H[u^133];
L f134a = H[u^134];
L f135a = H[u^135];
L f136a = H[u^136];
L f137a = H[u^137];
L f138a = H[u^138];
L f139a = H[u^139];
L f140a = H[u^140];
L f141a = H[u^141];
L f142a = H[u^142];
L f143a = H[u^143];
L f144a = H[u^144];
L f145a = H[u^145];
L f146a = H[u^146];
L f147a = H[u^147];
L f148a = H[u^148];
L f149a = H[u^149];
L f150a = H[u^150];
L f151a = H[u^151];
L f152a = H[u^152];
L f153a = H[u^153];
L f154a = H[u^154];
L f155a = H[u^155];
L f156a = H[u^156];
.sort
#write <fs.tmp> "`optimmaxvar_'"
#write <fs.c> "%O"
#write <fs.c> "f[1] = %E;", f1a
#write <fs.c> "f[2] = %E;", f2a
#write <fs.c> "f[3] = %E;", f3a
#write <fs.c> "f[4] = %E;", f4a
#write <fs.c> "f[5] = %E;", f5a
#write <fs.c> "f[6] = %E;", f6a
#write <fs.c> "f[7] = %E;", f7a
#write <fs.c> "f[8] = %E;", f8a
#write <fs.c> "f[9] = %E;", f9a
#write <fs.c> "f[10] = %E;", f10a
#write <fs.c> "f[11] = %E;", f11a
#write <fs.c> "f[12] = %E;", f12a
#write <fs.c> "f[13] = %E;", f13a
#write <fs.c> "f[14] = %E;", f14a
#write <fs.c> "f[15] = %E;", f15a
#write <fs.c> "f[16] = %E;", f16a
#write <fs.c> "f[17] = %E;", f17a
#write <fs.c> "f[18] = %E;", f18a
#write <fs.c> "f[19] = %E;", f19a
#write <fs.c> "f[20] = %E;", f20a
#write <fs.c> "f[21] = %E;", f21a
#write <fs.c> "f[22] = %E;", f22a
#write <fs.c> "f[23] = %E;", f23a
#write <fs.c> "f[24] = %E;", f24a
#write <fs.c> "f[25] = %E;", f25a
#write <fs.c> "f[26] = %E;", f26a
#write <fs.c> "f[27] = %E;", f27a
#write <fs.c> "f[28] = %E;", f28a
#write <fs.c> "f[29] = %E;", f29a
#write <fs.c> "f[30] = %E;", f30a
#write <fs.c> "f[31] = %E;", f31a
#write <fs.c> "f[32] = %E;", f32a
#write <fs.c> "f[33] = %E;", f33a
#write <fs.c> "f[34] = %E;", f34a
#write <fs.c> "f[35] = %E;", f35a
#write <fs.c> "f[36] = %E;", f36a
#write <fs.c> "f[37] = %E;", f37a
#write <fs.c> "f[38] = %E;", f38a
#write <fs.c> "f[39] = %E;", f39a
#write <fs.c> "f[40] = %E;", f40a
#write <fs.c> "f[41] = %E;", f41a
#write <fs.c> "f[42] = %E;", f42a
#write <fs.c> "f[43] = %E;", f43a
#write <fs.c> "f[44] = %E;", f44a
#write <fs.c> "f[45] = %E;", f45a
#write <fs.c> "f[46] = %E;", f46a
#write <fs.c> "f[47] = %E;", f47a
#write <fs.c> "f[48] = %E;", f48a
#write <fs.c> "f[49] = %E;", f49a
#write <fs.c> "f[50] = %E;", f50a
#write <fs.c> "f[51] = %E;", f51a
#write <fs.c> "f[52] = %E;", f52a
#write <fs.c> "f[53] = %E;", f53a
#write <fs.c> "f[54] = %E;", f54a
#write <fs.c> "f[55] = %E;", f55a
#write <fs.c> "f[56] = %E;", f56a
#write <fs.c> "f[57] = %E;", f57a
#write <fs.c> "f[58] = %E;", f58a
#write <fs.c> "f[59] = %E;", f59a
#write <fs.c> "f[60] = %E;", f60a
#write <fs.c> "f[61] = %E;", f61a
#write <fs.c> "f[62] = %E;", f62a
#write <fs.c> "f[63] = %E;", f63a
#write <fs.c> "f[64] = %E;", f64a
#write <fs.c> "f[65] = %E;", f65a
#write <fs.c> "f[66] = %E;", f66a
#write <fs.c> "f[67] = %E;", f67a
#write <fs.c> "f[68] = %E;", f68a
#write <fs.c> "f[69] = %E;", f69a
#write <fs.c> "f[70] = %E;", f70a
#write <fs.c> "f[71] = %E;", f71a
#write <fs.c> "f[72] = %E;", f72a
#write <fs.c> "f[73] = %E;", f73a
#write <fs.c> "f[74] = %E;", f74a
#write <fs.c> "f[75] = %E;", f75a
#write <fs.c> "f[76] = %E;", f76a
#write <fs.c> "f[77] = %E;", f77a
#write <fs.c> "f[78] = %E;", f78a
#write <fs.c> "f[79] = %E;", f79a
#write <fs.c> "f[80] = %E;", f80a
#write <fs.c> "f[81] = %E;", f81a
#write <fs.c> "f[82] = %E;", f82a
#write <fs.c> "f[83] = %E;", f83a
#write <fs.c> "f[84] = %E;", f84a
#write <fs.c> "f[85] = %E;", f85a
#write <fs.c> "f[86] = %E;", f86a
#write <fs.c> "f[87] = %E;", f87a
#write <fs.c> "f[88] = %E;", f88a
#write <fs.c> "f[89] = %E;", f89a
#write <fs.c> "f[90] = %E;", f90a
#write <fs.c> "f[91] = %E;", f91a
#write <fs.c> "f[92] = %E;", f92a
#write <fs.c> "f[93] = %E;", f93a
#write <fs.c> "f[94] = %E;", f94a
#write <fs.c> "f[95] = %E;", f95a
#write <fs.c> "f[96] = %E;", f96a
#write <fs.c> "f[97] = %E;", f97a
#write <fs.c> "f[98] = %E;", f98a
#write <fs.c> "f[99] = %E;", f99a
#write <fs.c> "f[100] = %E;", f100a
#write <fs.c> "f[101] = %E;", f101a
#write <fs.c> "f[102] = %E;", f102a
#write <fs.c> "f[103] = %E;", f103a
#write <fs.c> "f[104] = %E;", f104a
#write <fs.c> "f[105] = %E;", f105a
#write <fs.c> "f[106] = %E;", f106a
#write <fs.c> "f[107] = %E;", f107a
#write <fs.c> "f[108] = %E;", f108a
#write <fs.c> "f[109] = %E;", f109a
#write <fs.c> "f[110] = %E;", f110a
#write <fs.c> "f[111] = %E;", f111a
#write <fs.c> "f[112] = %E;", f112a
#write <fs.c> "f[113] = %E;", f113a
#write <fs.c> "f[114] = %E;", f114a
#write <fs.c> "f[115] = %E;", f115a
#write <fs.c> "f[116] = %E;", f116a
#write <fs.c> "f[117] = %E;", f117a
#write <fs.c> "f[118] = %E;", f118a
#write <fs.c> "f[119] = %E;", f119a
#write <fs.c> "f[120] = %E;", f120a
#write <fs.c> "f[121] = %E;", f121a
#write <fs.c> "f[122] = %E;", f122a
#write <fs.c> "f[123] = %E;", f123a
#write <fs.c> "f[124] = %E;", f124a
#write <fs.c> "f[125] = %E;", f125a
#write <fs.c> "f[126] = %E;", f126a
#write <fs.c> "f[127] = %E;", f127a
#write <fs.c> "f[128] = %E;", f128a
#write <fs.c> "f[129] = %E;", f129a
#write <fs.c> "f[130] = %E;", f130a
#write <fs.c> "f[131] = %E;", f131a
#write <fs.c> "f[132] = %E;", f132a
#write <fs.c> "f[133] = %E;", f133a
#write <fs.c> "f[134] = %E;", f134a
#write <fs.c> "f[135] = %E;", f135a
#write <fs.c> "f[136] = %E;", f136a
#write <fs.c> "f[137] = %E;", f137a
#write <fs.c> "f[138] = %E;", f138a
#write <fs.c> "f[139] = %E;", f139a
#write <fs.c> "f[140] = %E;", f140a
#write <fs.c> "f[141] = %E;", f141a
#write <fs.c> "f[142] = %E;", f142a
#write <fs.c> "f[143] = %E;", f143a
#write <fs.c> "f[144] = %E;", f144a
#write <fs.c> "f[145] = %E;", f145a
#write <fs.c> "f[146] = %E;", f146a
#write <fs.c> "f[147] = %E;", f147a
#write <fs.c> "f[148] = %E;", f148a
#write <fs.c> "f[149] = %E;", f149a
#write <fs.c> "f[150] = %E;", f150a
#write <fs.c> "f[151] = %E;", f151a
#write <fs.c> "f[152] = %E;", f152a
#write <fs.c> "f[153] = %E;", f153a
#write <fs.c> "f[154] = %E;", f154a
#write <fs.c> "f[155] = %E;", f155a
#write <fs.c> "f[156] = %E;", f156a
.end
