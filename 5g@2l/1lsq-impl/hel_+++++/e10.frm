#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f134;
S f132;
S f61;
S f131;
S f68;
S f124;
S f125;
S f126;
S f127;
S f121;
S f122;
S f123;
S f128;
S f156;
S f146;
S f3;
S f144;
S f140;
S f141;
S f94;
S f95;
S f99;
S f44;
S f35;
S f42;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu4u233;
S fvu4u141;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u14;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu3u34;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu2u1u8;
S fvu3u7;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u93;
S fvu3u52;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u44;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu10 = -f122/3 - (2*f128*fvu3u1)/3 - 
    (2*f128*fvu3u2)/9 - (2*f128*fvu3u3)/9 + (2*f128*fvu3u5)/3 - 
    (2*(4*f128 - f131)*fvu3u6)/9 - (2*f128*fvu3u7)/3 + 
    (4*f128*fvu3u8)/9 + ((f128 + f134)*fvu3u10)/3 + 
    ((-6*f42 + f44 + 6*f128 + 6*f134 - 6*f140 + f141)*fvu3u11)/
     9 - (2*(4*f128 - f131)*fvu3u12)/9 + (2*f128*fvu3u13)/9 + 
    ((-f121 + f128)*fvu3u14)/3 + 
    ((-6*f121 + 6*f128 - 6*f140 + f141 - 6*f144 + f156)*fvu3u17)/
     9 + (f131*fvu3u18)/3 - (f131*fvu3u19)/6 - (2*f131*fvu3u24)/9 + 
    (f127*fvu3u31)/6 + (f127*fvu3u32)/3 + 
    ((-6*f94 + f95 + 6*f127)*fvu3u33)/9 - (f127*fvu3u34)/3 + 
    ((6*f94 - f95 - 6*f127)*fvu3u36)/9 - (f127*fvu3u37)/3 - 
    (11*f131*fvu3u38)/18 + ((6*f94 - f95 - 6*f127)*fvu3u40)/9 + 
    (f131*fvu3u42)/9 + ((f127 + f131)*fvu3u44)/3 + 
    ((-6*f94 + f95 + 6*f127 + f132)*fvu3u51)/9 + 
    ((-f127 - f131)*fvu3u52)/3 + ((6*f94 - f95 - 6*f127 - f132)*
      fvu3u53)/9 + ((-f121 + f127)*fvu3u56)/3 - 
    (29*f131*fvu3u57)/18 + ((-6*f94 + f95 - 6*f121 + 6*f127 - 
       6*f144 + f156)*fvu3u59)/9 - (f131*fvu1u1u1^3)/3 + 
    (2*f131*fvu1u1u2^3)/9 - (f128*fvu1u1u3^3)/18 + 
    ((-2*f128 - f131)*fvu1u1u4^3)/18 - (4*f128*fvu1u1u5^3)/9 + 
    (f123*fvu1u1u6^3)/3 + ((3*f3 - 3*f35 + 5*f61 - f68 - 
       3*f99 - 4*f122 - 4*f123 + 4*f124 + 4*f125 - f146)*
      fvu1u1u7^3)/12 - (f124*fvu1u1u9^3)/3 + 
    fvu1u1u8^2*((-12*f121 + 9*f127 - 12*f128 + 26*f131 - 9*f134)/
       36 - (f131*fvu1u1u9)/6) + (f131*fvu2u1u12)/3 + 
    (2*f128*fvu2u1u14)/9 + (4*f131*fvu2u1u15)/27 - 
    (16*f128*fvu1u2u2*tci11^2)/9 + 
    (2*(4*f128 - f131)*fvu1u2u3*tci11^2)/9 - 
    (f132*fvu1u2u6*tci11^2)/9 - (f131*fvu1u2u7*tci11^2)/3 + 
    f128*tci12 + (2*f131*fvu1u2u9*tci12)/9 + 
    (f131*fvu2u1u2*tci12)/3 + (f132*fvu2u1u3*tci12)/9 - 
    (8*f131*fvu2u1u5*tci12)/9 - (4*f128*fvu2u1u6*tci12)/9 - 
    (4*f131*fvu2u1u9*tci12)/9 - (f132*fvu2u1u11*tci12)/9 - 
    (2*f128*fvu2u1u13*tci12)/9 + fvu1u1u10^2*
     ((-f127 - f131)/6 - (f128*tci12)/3) + 
    fvu1u1u5^2*((2*f128*fvu1u1u6)/9 + (2*f128*fvu1u1u7)/9 + 
      (4*f128*fvu1u1u8)/9 - (7*f131*fvu1u1u9)/18 - 
      (5*f128*tci12)/18) + fvu2u1u10*(-f131/3 - 
      (2*f128*tci12)/9) + fvu1u1u3^2*
     (((f121 + f128)*fvu1u1u6)/6 - (2*f131*fvu1u1u7)/9 + 
      (2*f131*fvu1u1u8)/9 + (f128*fvu1u1u9)/9 + 
      ((-f128 + 3*f131)*fvu1u1u10)/9 - (2*f128*tci12)/9) + 
    fvu1u1u6^2*((-3*f128 - 4*f131)/27 - 
      (2*(f128 - f131)*fvu1u1u7)/9 + (f128*fvu1u1u8)/3 + 
      ((-4*f128 - f131)*fvu1u1u9)/18 + ((2*f128 - f131)*tci12)/
       18) + fvu2u1u8*((-f128 + 10*f131)/18 + 
      (2*(4*f128 - f131)*tci12)/9) + 
    fvu1u1u1^2*((-16*f131*fvu1u1u2)/9 + (f131*fvu1u1u3)/6 - 
      (2*f131*fvu1u1u4)/9 + ((3*f127 + 8*f128 - 16*f131 - 3*f134)*
        fvu1u1u6)/18 + ((4*f128 + f131)*fvu1u1u7)/9 - 
      (2*f131*fvu1u1u8)/9 + (f131*fvu1u1u9)/18 - 
      (f131*tci12)/3) + fvu1u2u8*((2*(4*f128 - f131)*tci11^2)/
       9 - (f131*tci12)/9) + fvu1u1u2^2*((-4*f131*fvu1u1u4)/9 + 
      (f131*fvu1u1u5)/6 - (f131*fvu1u1u8)/3 + 
      (7*f131*fvu1u1u9)/18 + (2*f131*tci12)/9) + 
    fvu1u1u9^2*((-4*f128 - 3*f131)/27 + 
      ((-3*f128 + 2*f131)*fvu1u1u10)/18 + 
      ((-12*f128 + f131)*tci12)/18) + 
    fvu1u1u4^2*((-2*f131*fvu1u1u5)/9 + (f128*fvu1u1u6)/6 + 
      (f128*fvu1u1u7)/18 - (10*f131*fvu1u1u8)/9 + 
      (2*f131*fvu1u1u9)/27 - (f128*fvu1u1u10)/9 + 
      ((2*f128 + 9*f131)*tci12)/18) + 
    tci11^2*((8*f122)/9 + ((-f121 + 2*f128 + f134)*tci12)/
       6) + fvu1u1u7^2*(f125/6 + (8*f128*fvu1u1u8)/9 - 
      (7*f131*fvu1u1u9)/18 - (f128*fvu1u1u10)/3 + 
      ((-6*f128 + 6*f140 - f141)*tci12)/9) + 
    fvu1u1u10*(f122/3 + ((6*f128 - 6*f140 + f141)*fvu2u1u13)/9 + 
      (2*(4*f128 - f131)*fvu2u1u14)/9 - 
      (2*(2*f128 - f131)*fvu2u1u15)/9 + 
      ((6*f94 - f95 - 3*f122 - 6*f127 - f132)*tci11^2)/18 + 
      ((6*f42 - f44 - 6*f134)*tci12)/9 + 
      (f128*fvu1u2u2*tci12)/3) + 
    fvu1u1u5*((-2*f128*fvu1u1u6^2)/9 - (f131*fvu1u1u7^2)/18 - 
      (2*f128*fvu1u1u8^2)/3 + (f131*fvu1u1u9^2)/3 - 
      (2*f131*fvu2u1u4)/9 + (4*f131*fvu2u1u5)/9 - 
      (4*f128*fvu2u1u7)/9 + ((9*f128 + 2*f131)*tci11^2)/54 + 
      (2*f128*fvu1u1u8*tci12)/3 + (f131*fvu1u1u9*tci12)/9 - 
      (2*f131*fvu1u2u7*tci12)/9 + fvu1u1u7*
       (-(f128*fvu1u1u8)/6 + (f128*tci12)/3) + 
      fvu1u1u6*((f128*fvu1u1u8)/3 + (5*f128*tci12)/9)) + 
    fvu1u1u7*((-8*f125)/9 + (5*f128*fvu1u1u8^2)/9 + 
      (7*f131*fvu1u1u9^2)/9 + (f128*fvu1u1u10^2)/6 - 
      (2*f128*fvu2u1u7)/3 + (2*f128*fvu2u1u8)/3 - 
      (20*f128*fvu2u1u10)/9 - (7*f128*tci11^2)/27 - 
      (f131*tci12)/6 + ((-6*f128 + 6*f140 - f141)*fvu1u2u8*
        tci12)/9 + fvu1u1u8*((6*f121 + 6*f144 - f156)/9 - 
        (10*f128*tci12)/9) + fvu1u1u10*(-f128/3 + 
        (f128*tci12)/3) + fvu1u1u9*((-2*f131)/3 - 
        (2*f131*tci12)/3)) + fvu1u1u8*((-35*f128)/27 - 
      (f131*fvu1u1u9^2)/6 - (26*f131*fvu1u1u10)/9 + 
      ((-4*f128 - f131)*fvu2u1u8)/18 + (f131*fvu2u1u9)/3 - 
      (f128*fvu2u1u10)/6 + (f131*fvu2u1u11)/6 + 
      ((-3*f3 + 3*f35 - 5*f61 + f68 + 3*f99 + f146)*tci11^2)/
       12 - (2*(3*f3 - 3*f35 + 5*f61 - f68 - 3*f99 - f146)*
        tci12)/9 - (2*f128*fvu1u2u8*tci12)/3 + 
      fvu1u1u9*((-2*f131)/9 - (f131*tci12)/3)) + 
    fvu1u1u2*((-7*f131*fvu1u1u4^2)/9 + (4*f131*fvu1u1u5^2)/9 - 
      (11*f131*fvu1u1u8^2)/27 - (f131*fvu1u1u9^2)/9 + 
      (4*f131*fvu2u1u3)/9 - (2*f131*fvu2u1u4)/9 + 
      (2*f131*fvu2u1u11)/9 + (4*f131*tci11^2)/9 - 
      (10*f131*fvu1u1u5*tci12)/27 - (2*f131*fvu1u1u9*tci12)/
       9 + (4*f131*fvu1u2u6*tci12)/9 + 
      fvu1u1u4*(-(f131*fvu1u1u5)/18 - (5*f131*fvu1u1u8)/9 - 
        (2*f131*fvu1u1u9)/9 - (8*f131*tci12)/9) + 
      fvu1u1u8*(-(f131*fvu1u1u9)/9 + (f131*tci12)/9)) + 
    fvu1u1u1*(-(f131*fvu1u1u2^2)/6 - (f131*fvu1u1u3^2)/3 + 
      (f131*fvu1u1u4^2)/6 + (23*f131*fvu1u1u5^2)/18 + 
      (16*f128*fvu1u1u6^2)/9 - (2*f128*fvu1u1u7^2)/9 - 
      (2*f131*fvu1u1u8^2)/9 + (2*f131*fvu1u1u9^2)/9 - 
      (f131*fvu2u1u2)/3 + (f128*fvu2u1u8)/6 - (f131*fvu2u1u9)/3 + 
      (2*f131*fvu2u1u11)/9 + (f131*tci11^2)/9 + 
      (f131*fvu1u1u9*tci12)/6 + fvu1u1u7*((2*f128*fvu1u1u8)/9 + 
        (4*f131*fvu1u1u9)/9 + (f128*tci12)/6) + 
      fvu1u1u6*((2*f128*fvu1u1u7)/9 + ((-2*f128 + f131)*fvu1u1u8)/
         18 + (f128*tci12)/3) + fvu1u1u8*((-2*f131*fvu1u1u9)/9 - 
        (2*f131*tci12)/9) + fvu1u1u4*
       (((14*f128 - 3*f134)*fvu1u1u6)/18 + (f128*fvu1u1u7)/2 + 
        (f131*fvu1u1u8)/9 - (f131*fvu1u1u9)/18 + 
        (f131*tci12)/18) + fvu1u1u3*(-(f131*fvu1u1u4)/2 + 
        (f131*tci12)/9) + fvu1u1u2*(-(f131*fvu1u1u4)/6 + 
        (f131*fvu1u1u8)/9 + (2*f131*fvu1u1u9)/9 + 
        (f131*tci12)/6)) + fvu1u1u4*(-(f131*fvu1u1u5^2)/6 + 
      (f128*fvu1u1u6^2)/6 + (2*f128*fvu1u1u7^2)/9 - 
      (f131*fvu1u1u8^2)/6 - (f131*fvu1u1u9^2)/6 + 
      ((-f128 - 12*f131)*fvu1u1u10^2)/18 + (f131*fvu2u1u2)/3 - 
      (f131*fvu2u1u3)/18 - (5*f128*fvu2u1u6)/18 - 
      (f128*fvu2u1u8)/6 - (2*f131*fvu2u1u11)/9 - 
      (f128*tci11^2)/3 + (f131*fvu1u1u9*tci12)/3 + 
      (f128*fvu1u1u10*tci12)/3 + (f128*fvu1u2u3*tci12)/9 - 
      (f131*fvu1u2u6*tci12)/3 + fvu1u1u7*(-(f128*fvu1u1u8)/9 + 
        (2*f128*fvu1u1u10)/9 - (2*f128*tci12)/9) + 
      fvu1u1u6*(-(f128*fvu1u1u7)/3 - (f128*fvu1u1u8)/3 + 
        (f128*tci12)/18) + fvu1u1u5*(-(f131*fvu1u1u9)/6 + 
        (f131*tci12)/3) + fvu1u1u8*((f131*fvu1u1u9)/3 + 
        (f131*tci12)/3)) + fvu1u1u9*((-8*f128 + 3*f131)/27 + 
      (f131*fvu2u1u11)/6 - (f128*fvu2u1u15)/6 + 
      ((f131 - f134)*tci11^2)/6 - (f131*tci12)/3 + 
      fvu1u1u10*((10*f131)/9 + ((4*f128 + f131)*tci12)/9)) + 
    fvu1u1u6*((2*(f128 - 6*f131))/27 - (2*f128*fvu1u1u7)/3 + 
      ((-f128 + f131)*fvu1u1u7^2)/9 + (2*f128*fvu1u1u8^2)/3 + 
      ((-4*f128 - f131)*fvu1u1u9^2)/9 + 
      ((16*f128 - 13*f131)*fvu1u1u10)/108 + 
      ((-2*f128 + f131)*fvu1u1u10^2)/9 + (f128*fvu2u1u6)/3 + 
      (f128*fvu2u1u8)/6 - (f128*fvu2u1u14)/2 + 
      ((-f121 + f127)*tci11^2)/6 + ((-6*f42 + f44 + 6*f134)*
        tci12)/9 + (4*f128*fvu1u2u3*tci12)/9 + 
      fvu1u1u9*((6*f42 - f44 + f132 - 6*f134)/9 + 
        ((2*f128 - f131)*tci12)/18) + 
      fvu1u1u8*((-6*f42 + f44 + 6*f134)/9 + 
        ((4*f128 + f131)*tci12)/9)) + 
    fvu1u1u3*((f131*fvu1u1u5^2)/9 + (2*f128*fvu1u1u6^2)/9 + 
      (2*f131*fvu1u1u4*fvu1u1u8)/9 + (5*f131*fvu1u1u8^2)/9 - 
      (2*f128*fvu1u1u9^2)/9 - (2*f131*fvu2u1u5)/3 - 
      (2*(f128 + 3*f131)*fvu2u1u13)/9 + (4*f128*tci11^2)/9 + 
      (2*f131*fvu1u1u5*tci12)/9 - (2*f131*fvu1u1u7*tci12)/9 - 
      (4*f131*fvu1u1u8*tci12)/9 + (2*f128*fvu1u2u2*tci12)/9 - 
      (2*f131*fvu1u2u7*tci12)/3 + fvu1u1u6*
       ((4*f128*fvu1u1u9)/9 - (2*f128*tci12)/9) + 
      fvu1u1u9*((2*f128*fvu1u1u10)/9 + ((-4*f128 + 3*f131)*
          tci12)/18)) + ((6*f42 - f44 + f132 - 6*f134)*tci12*
      tcr11^2)/9 - (f131*tcr11^3)/3 - 
    (2*f128*tci11^2*tcr12)/9 + (2*f131*tci12*tcr21)/3 + 
    tcr11*(((f131 - f134)*tci11^2)/3 + 
      (4*f131*tci12*tcr12)/9 + (16*f131*tcr21)/9) + 
    ((-3*f127 - 12*f128 - 7*f131 + 3*f134)*tcr33)/108;
L ieu1ueu10 = (-9*f3 + 9*f35 - 15*f61 + 3*f68 + 48*f94 - 
      8*f95 + 9*f99 + 48*f121 - 72*f122 - 48*f127 + 48*f128 - 
      32*f132 - 48*f140 + 8*f141 + 48*f144 + 3*f146 - 8*f156)/
     432 + (8*f123*fvu1u1u6^2)/9 - 
    (2*(-3*f3 + 3*f35 - 5*f61 + f68 + 3*f99 + 4*f122 + 4*f123 - 
       4*f124 - 4*f125 + f146)*fvu1u1u7^2)/9 - 
    (8*f122*fvu1u1u8^2)/9 - (8*f124*fvu1u1u9^2)/9 + 
    fvu1u1u8*((-13*(3*f3 - 3*f35 + 5*f61 - f68 - 3*f99 - f146))/
       27 - (f131*fvu1u1u9)/3 + (2*f131*fvu1u1u10)/3) - 
    (f128*fvu2u1u8)/9 - (14*f131*fvu2u1u10)/9 + 
    (2*f131*fvu2u1u12)/3 + (f128*fvu2u1u14)/6 + 
    (4*f131*fvu2u1u15)/9 + (f122*tci11^2)/3 + 
    ((-6*f42 + f44 - 6*f121 - 3*f123 + 12*f128 + 6*f134 - 
       12*f140 + 2*f141 - 6*f144 + f156)*tci12)/18 - 
    (f132*fvu1u2u8*tci12)/9 - (f131*fvu1u2u9*tci12)/9 + 
    fvu1u1u7*(-f125/3 + (f121*fvu1u1u8)/3 - (2*f131*fvu1u1u9)/9 + 
      (f128*fvu1u1u10)/3 - (4*f131*tci12)/9) + 
    fvu1u1u9*((-9*f3 + 9*f35 + 24*f42 - 4*f44 - 15*f61 + 
        3*f68 + 9*f99 + 12*f122 + 12*f123 - 12*f124 - 12*f125 + 
        4*f132 - 24*f134 + 3*f146)/72 + (f131*fvu1u1u10)/2 + 
      (f131*tci12)/3) + fvu1u1u10*((-4*f128 - 3*f131)/54 - 
      (f134*tci12)/3) + fvu1u1u6*
     ((-6*f94 + f95 - 6*f121 + 3*f124 + 6*f127 - 6*f144 + f156)/
       18 + (f128*fvu1u1u7)/3 + (f134*fvu1u1u8)/3 + 
      ((f131 - f134)*fvu1u1u9)/3 - (2*(2*f128 - f131)*fvu1u1u10)/
       9 + (f134*tci12)/3);
L ieu0ueu10 = 
   (f121 - f127 + f128 - 4*f131)/18;
L ieum1ueu10 = 0;
L ieum2ueu10 = 0;
L ieu2uou10 = (2*f126*fvu4u28)/3 + (2*f126*fvu4u39)/3 - 
    (2*f126*fvu4u49)/9 + (2*f126*fvu4u51)/9 - (f126*fvu4u80)/12 + 
    (17*f126*fvu4u83)/12 + (3*f126*fvu4u91)/4 - (f126*fvu4u93)/12 - 
    (4*f126*fvu4u100)/9 + (7*f126*fvu4u102)/6 - (f126*fvu4u111)/4 - 
    (f126*fvu4u113)/2 - f126*fvu4u114 + (2*f126*fvu4u129)/3 - 
    (13*f126*fvu4u132)/3 + (7*f126*fvu4u139)/2 + 
    (2*f126*fvu4u141)/3 - (4*f126*fvu4u146)/9 - (f126*fvu4u148)/2 - 
    (2*f126*fvu4u174)/3 - (2*f126*fvu4u182)/3 + 
    (2*f126*fvu4u190)/9 - (2*f126*fvu4u192)/9 + (f126*fvu4u213)/12 + 
    (43*f126*fvu4u215)/12 - (17*f126*fvu4u219)/4 + 
    (f126*fvu4u221)/12 + (4*f126*fvu4u225)/9 + (f126*fvu4u233)/4 + 
    (f126*fvu4u234)/2 + f126*fvu4u235 - (14*f126*fvu4u244)/3 - 
    (2*f126*fvu4u246)/3 + (4*f126*fvu4u252)/3 + 
    (10*f126*fvu4u255)/9 + 2*f126*fvu4u273 - 2*f126*fvu4u277 + 
    fvu3u71*(-(f126*fvu1u1u3)/3 + (f126*fvu1u1u6)/3 - 
      (f126*fvu1u1u7)/3) + fvu3u25*((f126*fvu1u1u3)/3 - 
      (f126*fvu1u1u6)/3 + (f126*fvu1u1u7)/3) + 
    fvu3u62*((f126*fvu1u1u2)/3 + (f126*fvu1u1u4)/3 - 
      (2*f126*fvu1u1u5)/3 + (f126*fvu1u1u6)/3 + 
      (f126*fvu1u1u7)/3 - (f126*fvu1u1u10)/3) + 
    fvu3u70*((f126*fvu1u1u6)/3 - (f126*fvu1u1u8)/3 - 
      (f126*fvu1u1u10)/3) + fvu3u23*(-(f126*fvu1u1u1)/3 - 
      (f126*fvu1u1u6)/3 + (f126*fvu1u1u8)/3 + 
      (f126*fvu1u1u10)/3) + fvu3u43*((f126*fvu1u1u2)/3 + 
      (f126*fvu1u1u4)/3 + (f126*fvu1u1u6)/3 - (f126*fvu1u1u7)/3 - 
      (2*f126*fvu1u1u9)/3 + (f126*fvu1u1u10)/3) + 
    fvu3u45*((f126*fvu1u1u3)/3 + (2*f126*fvu1u1u5)/3 + 
      (f126*fvu1u1u6)/3 - f126*fvu1u1u7 - (2*f126*fvu1u1u9)/3 + 
      (2*f126*fvu1u1u10)/3) + fvu3u63*((-5*f126*fvu1u1u3)/2 - 
      (2*f126*fvu1u1u5)/3 - (13*f126*fvu1u1u6)/6 - 
      (f126*fvu1u1u7)/3 + (f126*fvu1u1u8)/3 + 
      (2*f126*fvu1u1u9)/3 + (5*f126*fvu1u1u10)/2) - 
    (151*f126*tci11^3*tci12)/135 + 
    fvu3u80*(-(f126*fvu1u1u2)/3 - (f126*fvu1u1u3)/3 - 
      (f126*fvu1u1u4)/3 + (2*f126*fvu1u1u5)/3 + 
      (4*f126*fvu1u1u6)/3 - 2*f126*fvu1u1u7 + 
      (4*f126*fvu1u1u8)/3 - 2*f126*fvu1u1u9 - f126*fvu1u1u10 - 
      (4*f126*tci12)/3) + fvu3u78*(-(f126*fvu1u1u2)/3 + 
      (f126*fvu1u1u3)/3 - (f126*fvu1u1u4)/3 + (f126*fvu1u1u6)/3 - 
      (f126*fvu1u1u8)/3 + (2*f126*fvu1u1u9)/3 + 
      (2*f126*tci12)/3) - (2*f126*tci11^2*tci21)/9 + 
    fvu1u1u8*((8*f126*tci11^3)/81 - (8*f126*tci12*tci21)/
       3) + tci12*((-48*f126*tci31)/5 - 16*f126*tci32) - 
    (4*f126*tci41)/3 + ((-35*f126*tci11^3)/108 + 
      (f126*tci12*tci21)/3 - 12*f126*tci31)*tcr11 + 
    ((f126*tci11*tci12)/5 - 5*f126*tci21)*tcr11^2 + 
    (f126*tci11*tcr11^3)/4 + fvu1u1u1*(-(f126*fvu3u25)/3 - 
      (f126*fvu3u43)/3 - (f126*fvu3u45)/3 - (f126*fvu3u62)/3 + 
      (13*f126*fvu3u63)/6 + (f126*fvu3u70)/3 + (f126*fvu3u71)/3 - 
      (f126*fvu3u78)/3 + (2*f126*fvu3u80)/3 + (19*f126*tci11^3)/
       108 - (f126*tci12*tci21)/3 + 12*f126*tci31 + 
      5*f126*tci21*tcr11 - (f126*tci11*tcr11^2)/4) + 
    fvu1u1u10*((-17*f126*tci11^3)/1620 + 
      (37*f126*tci12*tci21)/9 + (52*f126*tci31)/5 + 
      (13*f126*tci21*tcr11)/3 - (13*f126*tci11*tcr11^2)/
       60) + fvu1u1u9*((-23*f126*tci11^3)/135 + 
      (52*f126*tci12*tci21)/9 + (16*f126*tci31)/5 + 
      (4*f126*tci21*tcr11)/3 - (f126*tci11*tcr11^2)/15) + 
    fvu1u1u7*((-89*f126*tci11^3)/810 + 
      (32*f126*tci12*tci21)/9 + (8*f126*tci31)/5 + 
      (2*f126*tci21*tcr11)/3 - (f126*tci11*tcr11^2)/30) + 
    fvu1u1u5*((f126*tci11^3)/45 - (16*f126*tci12*tci21)/9 - 
      (16*f126*tci31)/5 - (4*f126*tci21*tcr11)/3 + 
      (f126*tci11*tcr11^2)/15) + 
    fvu1u1u6*(-(f126*tci11^3)/36 - (11*f126*tci12*tci21)/
       3 - 12*f126*tci31 - 5*f126*tci21*tcr11 + 
      (f126*tci11*tcr11^2)/4) + 
    fvu1u1u3*((-41*f126*tci11^3)/324 - f126*tci12*tci21 - 
      12*f126*tci31 - 5*f126*tci21*tcr11 + 
      (f126*tci11*tcr11^2)/4) + 
    (40*f126*tci12*tci21*tcr12)/3 + 
    4*f126*tci11*tci12*tcr12^2;
L ieu1uou10 = 
   -2*f126*fvu3u80 - (4*f126*tci11^3)/27 + 
    4*f126*tci12*tci21;
L ieu0uou10 = 0;
L ieum1uou10 = 0;
L ieum2uou10 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou10+w^2*ieu1uou10+w^3*ieu0uou10+w^4*ieum1uou10+w^5*ieum2uou10;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou10a = K[w^1];
L ieu1uou10a = K[w^2];
L ieu0uou10a = K[w^3];
L ieum1uou10a = K[w^4];
L ieum2uou10a = K[w^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_odd.c> "%O"
#write <e10_odd.c> "return Eps5o2<T>("
#write <e10_odd.c> "%E", ieu2uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieu0uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum1uou10a
#write <e10_odd.c> ", "
#write <e10_odd.c> "%E", ieum2uou10a
#write <e10_odd.c> ");\n}"
L H=+u^1*ieu2ueu10+u^2*ieu1ueu10+u^3*ieu0ueu10+u^4*ieum1ueu10+u^5*ieum2ueu10;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu10a = H[u^1];
L ieu1ueu10a = H[u^2];
L ieu0ueu10a = H[u^3];
L ieum1ueu10a = H[u^4];
L ieum2ueu10a = H[u^5];
.sort
#write <e10.tmp> "`optimmaxvar_'"
#write <e10_even.c> "%O"
#write <e10_even.c> "return Eps5o2<T>("
#write <e10_even.c> "%E", ieu2ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieu0ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum1ueu10a
#write <e10_even.c> ", "
#write <e10_even.c> "%E", ieum2ueu10a
#write <e10_even.c> ");\n}"
.end
