#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f61;
S f68;
S f120;
S f89;
S f88;
S f146;
S f3;
S f85;
S f87;
S f86;
S f96;
S f94;
S f95;
S f92;
S f93;
S f90;
S f91;
S f26;
S f25;
S f99;
S f119;
S f30;
S f35;
S f58;
S fvu1u2u9;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u11;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u139;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu3u51;
S fvu4u91;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u102;
S fvu4u244;
S fvu4u49;
S fvu4u182;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu5 = (-5*f3 + f35 - 3*f61 + 3*f68 + f99 + 
      3*f146)/12 + ((-2*f85 + 3*f91)*fvu3u3)/18 - (f91*fvu3u4)/3 - 
    (f92*fvu3u5)/9 + (f91*fvu3u6)/3 + (f92*fvu3u7)/9 + 
    ((f85 - f91)*fvu3u8)/3 + (2*f85*fvu3u10)/27 + 
    ((6*f85 - f92 + 6*f119 - f120)*fvu3u11)/9 + 
    ((-f85 + f91)*fvu3u12)/3 + ((-6*f85 + f92 - 6*f119 + f120)*
      fvu3u13)/9 + ((f91 - f94)*fvu3u17)/3 + 
    ((f92 - f95)*fvu3u18)/9 + 
    ((7*f85 - 6*f91 - 2*f93 - 6*f94 - 7*f96)*fvu3u24)/36 + 
    ((f85 + 2*f93)*fvu3u51)/12 - (f93*fvu3u52)/3 + 
    ((6*f25 - f26 - 6*f93)*fvu3u55)/9 - (f93*fvu3u56)/3 + 
    ((6*f25 - f26 - 6*f93)*fvu3u57)/9 + (f93*fvu3u59)/3 + 
    ((-6*f25 + f26 + 6*f93)*fvu3u61)/9 + 
    ((5*f3 - f35 + 3*f61 - 3*f68 + 4*f86 - 4*f87 + 4*f88 + 
       4*f89 - f99 - 3*f146)*fvu1u1u1^3)/12 + 
    ((-f85 + f96)*fvu1u1u2^3)/3 + (f85*fvu1u1u6^3)/27 - 
    (2*(5*f3 - f35 + 3*f61 - 3*f68 - f99 - 3*f146)*fvu1u1u8^3)/
     9 - (8*f88*fvu1u1u9^3)/9 + fvu1u1u4^2*((-11*f85*fvu1u1u5)/54 - 
      (f85*fvu1u1u8)/18 + (f85*fvu1u1u9)/18) + 
    fvu1u1u3^2*((-5*f3 + f35 - 3*f61 + 3*f68 - 4*f86 - 4*f88 + 
        f99 + 3*f146)/12 - (f85*fvu1u1u4)/9 - (f85*fvu1u1u6)/9 - 
      (f85*fvu1u1u8)/9 + (f85*fvu1u1u9)/9 - (f85*fvu1u1u10)/6) - 
    (f85*fvu2u1u13)/6 - (f85*fvu2u1u15)/3 - 
    (f85*fvu1u2u6*tci11^2)/18 + (8*f87*tci12)/9 - 
    (f85*fvu1u2u2*tci12)/9 - (f85*fvu2u1u2*tci12)/3 - 
    (2*f85*fvu2u1u3*tci12)/9 - (f85*fvu2u1u11*tci12)/12 + 
    fvu1u1u10*((-7*f85)/54 + (f85*fvu2u1u14)/6 + 
      (f85*fvu2u1u15)/18 + ((f91 - f94)*tci11^2)/6 - 
      (f85*tci12)/3) + fvu1u1u5^2*((5*f85*fvu1u1u9)/18 - 
      (2*f85*tci12)/9) + fvu1u1u2^2*(-(f85*fvu1u1u4)/9 + 
      (f85*fvu1u1u5)/12 - (11*f85*fvu1u1u8)/36 + 
      (f85*fvu1u1u9)/9 - (f85*tci12)/6) + 
    fvu1u1u10^2*((-8*f85)/27 - (f85*tci12)/36) + 
    fvu2u1u12*(-f85/18 + (f85*tci12)/36) + 
    fvu2u1u9*((-2*f85)/9 + (f85*tci12)/27) + 
    fvu1u1u6^2*((f85*fvu1u1u9)/12 + (f85*fvu1u1u10)/18 + 
      (f85*tci12)/12) + fvu1u1u9^2*(-f85/2 - 
      (f85*fvu1u1u10)/6 + (f85*tci12)/3) + 
    fvu1u2u9*((-7*f85*tci11^2)/36 + (f85*tci12)/3) + 
    fvu2u1u1*(f85/9 + (7*f85*tci12)/18) + 
    tci11^2*((-8*f86)/9 + ((6*f30 - f58 - 6*f85 + 3*f87 + 
         6*f96 - 6*f119 + f120)*tci12)/9) + 
    fvu1u1u8^2*(-f85/18 + (f85*fvu1u1u9)/18 - (f85*fvu1u1u10)/3 - 
      (13*(5*f3 - f35 + 3*f61 - 3*f68 - f99 - 3*f146)*tci12)/
       27) + fvu1u1u1^2*((-f85 + f91)/6 + ((f85 + f93)*fvu1u1u2)/
       3 + ((-6*f25 + f26 + 6*f85 + 6*f93 + 6*f119 - f120)*
        fvu1u1u4)/9 + ((f93 - f96)*fvu1u1u8)/3 + 
      ((-6*f25 + f26 - 6*f30 + f58 + 6*f93 - 6*f96)*fvu1u1u9)/
       9 + ((10*f85 + 6*f93 - 9*f96)*fvu1u1u10)/18 + 
      ((5*f3 - f35 + 3*f61 - 3*f68 + 4*f86 - 4*f87 + 4*f88 + 
         4*f89 - f99 - 3*f146)*tci12)/12) + 
    fvu1u1u5*(-(f85*fvu1u1u9^2)/3 - (f85*fvu2u1u4)/9 - 
      (5*f85*tci11^2)/9 - (f85*fvu1u1u9*tci12)/3) + 
    fvu1u1u6*((f85*fvu1u1u9^2)/9 - (f85*fvu2u1u14)/6 - 
      (f85*tci11^2)/6 - (f85*fvu1u1u9*tci12)/6 + 
      (f85*fvu1u1u10*tci12)/12) + 
    fvu1u1u8*((-f85 + f96)/3 + (2*f85*fvu1u1u9^2)/9 + 
      (f85*fvu2u1u9)/9 + (f85*fvu2u1u11)/9 - 
      (2*f85*fvu2u1u12)/9 + ((f85 + 2*f93 - f96)*tci11^2)/6 + 
      ((-12*f25 + 2*f26 - 6*f30 + f58 + 6*f85 - 3*f87 + 
         12*f93 - 6*f96 + 6*f119 - f120)*tci12)/18 - 
      (f85*fvu1u2u9*tci12)/6 + fvu1u1u9*((-2*f85)/9 - 
        (f85*tci12)/9) + fvu1u1u10*(f85/6 + (f85*tci12)/
         12)) + fvu1u1u2*((-8*f85*fvu1u1u4^2)/9 + 
      (f85*fvu1u1u5^2)/9 + (f85*fvu1u1u8^2)/18 + 
      (f85*fvu1u1u9^2)/3 - (f85*fvu2u1u3)/18 - 
      (f85*fvu2u1u4)/18 - (f85*fvu2u1u11)/6 + 
      (2*f85*tci11^2)/9 + (f85*fvu1u1u5*tci12)/9 + 
      (f85*fvu1u1u9*tci12)/18 - (f85*fvu1u2u6*tci12)/18 + 
      fvu1u1u4*(-(f85*fvu1u1u5)/9 + (f85*fvu1u1u8)/36 + 
        (f85*fvu1u1u9)/36 - (f85*tci12)/12) + 
      fvu1u1u8*((-2*f85*fvu1u1u9)/9 + (f85*tci12)/6)) + 
    fvu1u1u4*(-(f85*fvu1u1u5^2)/18 + (2*f85*fvu1u1u8^2)/9 + 
      (2*f85*fvu1u1u9^2)/9 + (f85*fvu2u1u2)/9 + 
      (f85*fvu2u1u3)/18 + (f85*fvu2u1u11)/9 - (f85*tci11^2)/9 - 
      (f85*fvu1u1u9*tci12)/9 + (f85*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(-(f85*fvu1u1u9)/9 + (f85*tci12)/9) + 
      fvu1u1u5*(-(f85*fvu1u1u9)/9 + (2*f85*tci12)/9)) + 
    fvu1u1u3*(-f88/3 + (f85*fvu1u1u4^2)/3 + (f85*fvu1u1u6^2)/9 + 
      (2*f85*fvu1u1u8^2)/9 - (f85*fvu1u1u9^2)/9 + 
      ((-6*f85 - 6*f119 + f120)*fvu1u1u10^2)/9 + 
      (2*f85*fvu2u1u1)/9 - (2*(5*f3 - f35 + 3*f61 - 3*f68 + 
         4*f86 + 4*f88 - f99 - 3*f146)*tci11^2)/9 + 
      (f88*tci12)/6 + fvu1u1u9*(f85/6 + (f85*fvu1u1u10)/3 - 
        (f85*tci12)/3) + fvu1u1u8*(f85/3 - (f85*tci12)/9) + 
      fvu1u1u4*(-(f85*fvu1u1u8)/9 + (f85*tci12)/9) + 
      fvu1u1u6*((2*f85*fvu1u1u9)/9 - (f85*fvu1u1u10)/9 + 
        (f85*tci12)/9) + fvu1u1u10*(f85/9 + (f85*tci12)/3)) + 
    fvu1u1u1*((-8*f89)/9 + (2*(8*f85 + 3*f93 - 3*f96)*fvu1u1u2^2)/
       9 + ((f85 - 2*f93 - f96)*fvu1u1u3^2)/12 - 
      (f94*fvu1u1u4^2)/3 + (f94*fvu1u1u8^2)/3 + 
      ((-6*f30 + f58 - f95 - 6*f96)*fvu1u1u9^2)/9 + 
      ((6*f30 - f58 + 6*f96)*fvu2u1u1)/9 - (f96*fvu2u1u2)/3 + 
      ((-6*f30 + f58 - 6*f96)*fvu2u1u9)/9 + 
      ((25*f85 + 21*f96)*fvu2u1u11)/108 + 
      ((-6*f85 + 3*f89 + f92 - 6*f119 + f120)*tci11^2)/18 - 
      (f89*tci12)/3 + fvu1u1u3*((-2*f85)/9 + 
        ((-f85 - f94)*tci12)/6) + fvu1u1u4*(-(f95*fvu1u1u8)/9 + 
        (f94*fvu1u1u9)/3 + (f95*tci12)/9) + 
      fvu1u1u2*(((-2*f85 - 4*f93 + f96)*fvu1u1u4)/18 + 
        ((-5*f85 - 6*f93)*fvu1u1u8)/9 + ((-f85 + 2*f93 + f96)*
          fvu1u1u9)/9 + ((f85 - 2*f93 - f96)*tci12)/3) + 
      fvu1u1u8*((-2*f85)/9 + (f95*fvu1u1u9)/9 + 
        ((-f94 - f96)*tci12)/3) + fvu1u1u9*
       (f85/6 + ((f94 + f96)*tci12)/3) + 
      fvu1u1u10*(f96/3 + ((6*f30 - f58 + f95 + 6*f96)*tci12)/
         9)) + fvu1u1u9*((73*f85)/108 + (f85*fvu1u1u10^2)/3 - 
      (f85*fvu2u1u11)/36 + (f85*fvu2u1u12)/36 - 
      (7*f85*fvu2u1u15)/36 + ((f94 + f96)*tci11^2)/6 + 
      (f85*tci12)/3 + (f85*fvu1u2u9*tci12)/3 + 
      fvu1u1u10*((-4*f85)/9 + ((6*f85 + 6*f119 - f120)*tci12)/
         9)) - (f85*tci12*tcr11^2)/9 - (f85*tcr11^3)/18 - 
    (f85*tci11^2*tcr12)/27 + (f85*tcr12^3)/3 - 
    (f85*tci12*tcr21)/9 + tcr11*(-(f85*tci11^2)/9 + 
      (f85*tci12*tcr12)/6 + (f85*tcr21)/6) - 
    (2*f85*tcr31)/9 - (2*f85*tcr32)/9 + 
    ((f91 + f93 + f94)*tcr33)/18;
L ieu1ueu5 = 
   (-11*f85 - 18*f91 + 74*f93 - 18*f94 + 11*f96)/72 + 
    (2*(5*f3 - f35 + 3*f61 - 3*f68 + 4*f86 - 4*f87 + 4*f88 + 
       4*f89 - f99 - 3*f146)*fvu1u1u1^2)/9 + 
    ((5*f85 + f91 + 2*f93 + f94 - 3*f96)*fvu1u1u3^2)/36 + 
    (f85*fvu1u1u8^2)/18 + (f85*fvu1u1u9^2)/18 + 
    fvu1u1u8*((-23*f85)/54 + (f85*fvu1u1u9)/9 - 
      (4*f85*fvu1u1u10)/9) + fvu1u1u3*
     (-f85/9 + (2*f85*fvu1u1u8)/9 + (f85*fvu1u1u9)/12 + 
      (f85*fvu1u1u10)/9) - (f85*fvu2u1u1)/3 + (f85*fvu2u1u9)/9 - 
    (f85*fvu2u1u12)/3 - (f85*fvu2u1u13)/12 - 
    (7*f85*fvu2u1u15)/9 - (f86*tci11^2)/3 + (f87*tci12)/3 + 
    (f85*fvu1u2u2*tci12)/36 - (f85*fvu1u2u9*tci12)/3 + 
    fvu1u1u9*((-15*f3 + 24*f30 + 3*f35 - 4*f58 - 9*f61 + 
        9*f68 - 12*f86 + 12*f87 - 12*f88 - 12*f89 + 4*f95 + 
        24*f96 + 3*f99 + 9*f146)/72 - (2*f85*fvu1u1u10)/9 - 
      (f85*tci12)/9) + fvu1u1u1*(-f89/3 - (2*f85*fvu1u1u3)/3 + 
      (f85*fvu1u1u8)/3 - (f85*fvu1u1u9)/18 + (f96*fvu1u1u10)/6 + 
      (f85*tci12)/36) + fvu1u1u10*((3*f86 + f92 - f95)/18 + 
      (f85*tci12)/3);
L ieu0ueu5 = 
   (-105*f3 - 48*f25 + 8*f26 + 21*f35 - 63*f61 + 63*f68 - 
     72*f86 - 72*f88 + 8*f92 + 48*f93 + 8*f95 + 21*f99 + 
     63*f146)/432;
L ieum1ueu5 = 0;
L ieum2ueu5 = 0;
L ieu2uou5 = (-4*f90*fvu4u28)/3 - (4*f90*fvu4u39)/3 - 
    (2*f90*fvu4u49)/9 - (4*f90*fvu4u51)/9 - (f90*fvu4u80)/12 - 
    (7*f90*fvu4u83)/12 + (3*f90*fvu4u91)/4 - (f90*fvu4u93)/12 - 
    (4*f90*fvu4u100)/9 + (f90*fvu4u102)/2 - (f90*fvu4u111)/4 - 
    (f90*fvu4u113)/2 - f90*fvu4u114 + (2*f90*fvu4u129)/3 - 
    (13*f90*fvu4u132)/3 + (7*f90*fvu4u139)/2 - (4*f90*fvu4u141)/3 - 
    (4*f90*fvu4u146)/9 - (7*f90*fvu4u148)/6 + (4*f90*fvu4u174)/3 + 
    (4*f90*fvu4u182)/3 + (2*f90*fvu4u190)/9 + (4*f90*fvu4u192)/9 + 
    (f90*fvu4u213)/12 + (67*f90*fvu4u215)/12 - (9*f90*fvu4u219)/4 + 
    (f90*fvu4u221)/12 + (4*f90*fvu4u225)/9 + (f90*fvu4u233)/4 + 
    (f90*fvu4u234)/2 + f90*fvu4u235 + (10*f90*fvu4u244)/3 - 
    (2*f90*fvu4u246)/3 - 2*f90*fvu4u250 - (2*f90*fvu4u252)/3 - 
    (2*f90*fvu4u255)/9 - 2*f90*fvu4u273 + 2*f90*fvu4u277 + 
    fvu3u25*((-2*f90*fvu1u1u3)/3 + (2*f90*fvu1u1u6)/3 - 
      (2*f90*fvu1u1u7)/3) + fvu3u71*((2*f90*fvu1u1u3)/3 - 
      (2*f90*fvu1u1u6)/3 + (2*f90*fvu1u1u7)/3) + 
    fvu3u70*((f90*fvu1u1u6)/3 - (f90*fvu1u1u8)/3 - 
      (f90*fvu1u1u10)/3) + fvu3u45*(-(f90*fvu1u1u2) + 
      (f90*fvu1u1u3)/3 + (2*f90*fvu1u1u5)/3 + (f90*fvu1u1u6)/3 - 
      f90*fvu1u1u8 + (f90*fvu1u1u9)/3 - (f90*fvu1u1u10)/3) + 
    fvu3u23*(-(f90*fvu1u1u1)/3 - (f90*fvu1u1u6)/3 + 
      (f90*fvu1u1u8)/3 + (f90*fvu1u1u10)/3) + 
    fvu3u43*((f90*fvu1u1u2)/3 + (f90*fvu1u1u4)/3 + 
      (f90*fvu1u1u6)/3 - (f90*fvu1u1u7)/3 - (2*f90*fvu1u1u9)/3 + 
      (f90*fvu1u1u10)/3) + fvu3u63*(f90*fvu1u1u2 - 
      (3*f90*fvu1u1u3)/2 - (2*f90*fvu1u1u5)/3 - 
      (19*f90*fvu1u1u6)/6 - (f90*fvu1u1u7)/3 - 
      (2*f90*fvu1u1u8)/3 - (f90*fvu1u1u9)/3 + 
      (7*f90*fvu1u1u10)/2) + (13*f90*tci11^3*tci12)/15 + 
    fvu3u62*((-2*f90*fvu1u1u2)/3 + f90*fvu1u1u3 + 
      (f90*fvu1u1u4)/3 - (2*f90*fvu1u1u5)/3 - 
      (2*f90*fvu1u1u6)/3 + (f90*fvu1u1u7)/3 + f90*fvu1u1u8 + 
      f90*fvu1u1u9 + (2*f90*fvu1u1u10)/3 - 2*f90*tci12) + 
    fvu3u78*(-(f90*fvu1u1u2)/3 + (f90*fvu1u1u3)/3 - 
      (f90*fvu1u1u4)/3 + (f90*fvu1u1u6)/3 - (f90*fvu1u1u8)/3 + 
      (2*f90*fvu1u1u9)/3 + (2*f90*tci12)/3) + 
    fvu3u80*((2*f90*fvu1u1u2)/3 - (f90*fvu1u1u3)/3 - 
      (f90*fvu1u1u4)/3 + (2*f90*fvu1u1u5)/3 - 
      (2*f90*fvu1u1u6)/3 + f90*fvu1u1u7 - (5*f90*fvu1u1u8)/3 + 
      f90*fvu1u1u9 + (8*f90*tci12)/3) - 
    (10*f90*tci11^2*tci21)/27 + 
    tci12*((16*f90*tci31)/5 + 16*f90*tci32) - 
    (2072*f90*tci41)/135 - (64*f90*tci42)/5 - 
    (16*f90*tci43)/3 + ((53*f90*tci11^3)/108 + 
      (5*f90*tci12*tci21)/3 - (12*f90*tci31)/5 + 
      16*f90*tci32)*tcr11 + (61*f90*tci11*tcr11^3)/180 + 
    fvu1u1u10*((169*f90*tci11^3)/1620 + 
      (25*f90*tci12*tci21)/9 + (76*f90*tci31)/5 + 
      (19*f90*tci21*tcr11)/3 - (19*f90*tci11*tcr11^2)/
       60) + fvu1u1u2*((53*f90*tci11^3)/270 + 
      (72*f90*tci31)/5 + 6*f90*tci21*tcr11 - 
      (3*f90*tci11*tcr11^2)/10) + 
    fvu1u1u1*((2*f90*fvu3u25)/3 - (f90*fvu3u43)/3 + 
      (2*f90*fvu3u45)/3 - (f90*fvu3u62)/3 + (13*f90*fvu3u63)/6 + 
      (f90*fvu3u70)/3 - (2*f90*fvu3u71)/3 - (f90*fvu3u78)/3 - 
      (f90*fvu3u80)/3 + (19*f90*tci11^3)/108 - 
      (f90*tci12*tci21)/3 + 12*f90*tci31 + 
      5*f90*tci21*tcr11 - (f90*tci11*tcr11^2)/4) + 
    fvu1u1u5*((f90*tci11^3)/45 - (16*f90*tci12*tci21)/9 - 
      (16*f90*tci31)/5 - (4*f90*tci21*tcr11)/3 + 
      (f90*tci11*tcr11^2)/15) + 
    fvu1u1u8*((-133*f90*tci11^3)/810 + (8*f90*tci12*tci21)/
       3 - (24*f90*tci31)/5 - 2*f90*tci21*tcr11 + 
      (f90*tci11*tcr11^2)/10) + 
    fvu1u1u6*((-11*f90*tci11^3)/180 - f90*tci12*tci21 - 
      (36*f90*tci31)/5 - 3*f90*tci21*tcr11 + 
      (3*f90*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-7*f90*tci11^3)/162 - (16*f90*tci12*tci21)/
       9 - 8*f90*tci31 - (10*f90*tci21*tcr11)/3 + 
      (f90*tci11*tcr11^2)/6) + 
    fvu1u1u9*((-19*f90*tci11^3)/270 - (20*f90*tci12*tci21)/
       9 - (56*f90*tci31)/5 - (14*f90*tci21*tcr11)/3 + 
      (7*f90*tci11*tcr11^2)/30) + 
    fvu1u1u3*((-391*f90*tci11^3)/1620 + (f90*tci12*tci21)/
       3 - (84*f90*tci31)/5 - 7*f90*tci21*tcr11 + 
      (7*f90*tci11*tcr11^2)/20) + 
    ((-218*f90*tci11^3)/243 - (40*f90*tci12*tci21)/3)*
     tcr12 + (-4*f90*tci11*tci12 - (40*f90*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f90*tci11*tci12)/15 - 
      3*f90*tci21 - 2*f90*tci11*tcr12) + 
    (130*f90*tci11*tcr33)/27;
L ieu1uou5 = 
   2*f90*fvu3u62 - (11*f90*tci11^3)/135 - 
    (4*f90*tci12*tci21)/3 - (48*f90*tci31)/5 - 
    4*f90*tci21*tcr11 + (f90*tci11*tcr11^2)/5;
L ieu0uou5 = 0;
L ieum1uou5 = 0;
L ieum2uou5 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou5+w^2*ieu1uou5+w^3*ieu0uou5+w^4*ieum1uou5+w^5*ieum2uou5;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou5a = K[w^1];
L ieu1uou5a = K[w^2];
L ieu0uou5a = K[w^3];
L ieum1uou5a = K[w^4];
L ieum2uou5a = K[w^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_odd.c> "%O"
#write <e5_odd.c> "return Eps5o2<T>("
#write <e5_odd.c> "%E", ieu2uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieu0uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum1uou5a
#write <e5_odd.c> ", "
#write <e5_odd.c> "%E", ieum2uou5a
#write <e5_odd.c> ");\n}"
L H=+u^1*ieu2ueu5+u^2*ieu1ueu5+u^3*ieu0ueu5+u^4*ieum1ueu5+u^5*ieum2ueu5;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu5a = H[u^1];
L ieu1ueu5a = H[u^2];
L ieu0ueu5a = H[u^3];
L ieum1ueu5a = H[u^4];
L ieum2ueu5a = H[u^5];
.sort
#write <e5.tmp> "`optimmaxvar_'"
#write <e5_even.c> "%O"
#write <e5_even.c> "return Eps5o2<T>("
#write <e5_even.c> "%E", ieu2ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieu0ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum1ueu5a
#write <e5_even.c> ", "
#write <e5_even.c> "%E", ieum2ueu5a
#write <e5_even.c> ");\n}"
.end
