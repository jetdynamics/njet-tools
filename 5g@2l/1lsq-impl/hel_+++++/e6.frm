#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f61;
S f68;
S f120;
S f146;
S f3;
S f10;
S f11;
S f99;
S f119;
S f118;
S f115;
S f117;
S f110;
S f35;
S f113;
S f112;
S f109;
S f102;
S f53;
S f100;
S f107;
S f105;
S f55;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu6 = (23*f119)/54 + 
    ((f100 - 2*f115 - f119)*fvu3u3)/12 + ((3*f117 - f119)*fvu3u4)/
     18 + (f117*fvu3u5)/3 + (f118*fvu3u6)/9 - (f117*fvu3u7)/3 - 
    (f118*fvu3u8)/9 + ((f117 - f119)*fvu3u11)/3 + 
    ((f118 - f120)*fvu3u12)/9 + ((-f117 + f119)*fvu3u13)/3 + 
    ((-f118 + f120)*fvu3u17)/9 + (f119*fvu3u18)/9 + 
    (11*f119*fvu3u24)/36 + ((-f113 - f118)*fvu3u35)/9 - 
    (f119*fvu3u51)/9 + (f112*fvu3u52)/6 + 
    ((-21*f100 - 25*f119)*fvu3u53)/108 + (f119*fvu3u55)/6 + 
    (8*f119*fvu3u56)/9 + (f119*fvu3u57)/9 - (f119*fvu3u59)/36 - 
    (f119*fvu3u61)/36 - (2*f119*fvu1u1u1^3)/27 + 
    (f119*fvu1u1u2^3)/9 + ((-f100 + f112)*fvu1u1u6^3)/6 + 
    (f119*fvu1u1u8^3)/9 + (f119*fvu1u1u9^3)/2 + 
    fvu1u1u4^2*((6*f53 - f55 - 6*f100 + 3*f105 + f120)/9 + 
      (f119*fvu1u1u5)/6 - (f119*fvu1u1u8)/9 - 
      (f119*fvu1u1u9)/9) + fvu1u1u3^2*
     (f119/9 - (f119*fvu1u1u4)/9 + (f119*fvu1u1u6)/9 - 
      (5*f119*fvu1u1u8)/18 + (2*f119*fvu1u1u9)/9 + 
      (f119*fvu1u1u10)/3) + ((-6*f10 + f11 + 6*f115 - f120)*
      fvu2u1u6)/9 + ((-6*f53 + f55 + 6*f100 - f113)*fvu2u1u13)/
     9 + ((6*f53 - f55 - 6*f100 + f113)*fvu2u1u14)/9 + 
    (f119*fvu1u2u6*tci11^2)/9 - (f119*fvu1u2u9*tci11^2)/3 - 
    (f119*tci12)/18 + (f113*fvu1u2u2*tci12)/9 + 
    ((6*f10 - f11 - 6*f115)*fvu1u2u3*tci12)/9 - 
    (f119*fvu2u1u3*tci12)/6 + (2*f119*fvu2u1u9*tci12)/9 + 
    (4*f119*fvu2u1u11*tci12)/9 - (f119*fvu2u1u12*tci12)/3 + 
    fvu1u1u10*((-3*f107 - f113 - f118)/18 - (f119*fvu2u1u14)/12 - 
      (f119*fvu2u1u15)/6 + (f119*tci11^2)/9 + 
      ((-6*f53 + f55 + 6*f100)*tci12)/9) + 
    fvu1u1u1^2*((-f117 + f119)/6 + (f119*fvu1u1u2)/12 - 
      (f119*fvu1u1u4)/12 - (f119*fvu1u1u8)/9 - 
      (f119*fvu1u1u9)/9 + (2*f119*fvu1u1u10)/9 + 
      (f107*tci12)/3) + fvu1u1u2^2*((f119*fvu1u1u4)/9 - 
      (f119*fvu1u1u5)/3 + (f119*fvu1u1u8)/3 + (f120*fvu1u1u9)/9 - 
      (f119*tci12)/3) + fvu1u1u10^2*
     ((2*(f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + 4*f102 + 
         4*f107 + f146))/9 + (f119*tci12)/18) + 
    fvu1u1u5^2*(-(f119*fvu1u1u9)/36 + (f119*tci12)/9) + 
    fvu1u1u8^2*((f119*fvu1u1u9)/27 - (f119*fvu1u1u10)/6 + 
      (f119*tci12)/9) + fvu2u1u2*((-6*f10 + f11 + 6*f115)/9 + 
      (7*f119*tci12)/54) + fvu1u1u9^2*(-(f119*fvu1u1u10)/9 + 
      (2*f119*tci12)/9) + fvu1u1u6^2*(-f119/27 - 
      (f119*fvu1u1u9)/9 + (f119*fvu1u1u10)/3 + 
      (2*f119*tci12)/9) + fvu2u1u1*((4*f119)/9 + 
      (7*f119*tci12)/9) + tci11^2*
     ((2*(f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + f146))/9 + 
      ((-11*f100 - 18*f112 + 74*f115 + 18*f117 + 11*f119)*
        tci12)/72) + fvu1u1u5*((f119*fvu1u1u9^2)/18 - 
      (7*f119*fvu2u1u4)/18 + (f119*tci11^2)/3 + 
      (7*f119*fvu1u1u9*tci12)/36) + 
    fvu1u1u9*((f119*fvu1u1u10^2)/3 + (2*f119*fvu2u1u11)/9 + 
      (f119*fvu2u1u12)/3 - (73*f119*fvu2u1u15)/108 + 
      (8*f119*tci11^2)/27 - (f119*fvu1u1u10*tci12)/3 - 
      (f119*fvu1u2u9*tci12)/9) + 
    fvu1u1u8*((f119*fvu1u1u9^2)/18 + (2*f119*fvu2u1u9)/9 - 
      (f119*fvu2u1u11)/18 + (f119*fvu2u1u12)/18 - 
      (f119*tci11^2)/6 - (f119*fvu1u1u9*tci12)/6 - 
      (f119*fvu1u1u10*tci12)/3 + (2*f119*fvu1u2u9*tci12)/9) + 
    fvu1u1u4*((f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + 4*f102 + 
        4*f107 + f146)/12 + (2*f119*fvu1u1u5^2)/9 + 
      ((6*f10 - f11 - 6*f115)*fvu1u1u6)/9 - (f119*fvu1u1u8^2)/3 + 
      (f119*fvu1u1u9^2)/36 - (f100*fvu1u1u10)/6 + 
      (f119*fvu2u1u2)/36 - (f119*fvu2u1u3)/6 - 
      (f119*fvu2u1u11)/18 + (f105*tci11^2)/3 + 
      (8*f105*tci12)/9 - (f119*fvu1u1u9*tci12)/36 + 
      (7*f119*fvu1u2u6*tci12)/36 + fvu1u1u5*
       ((f119*fvu1u1u9)/6 - (f119*tci12)/3) + 
      fvu1u1u8*(-(f120*fvu1u1u9)/9 - (f119*tci12)/3)) + 
    fvu1u1u2*(-(f119*fvu1u1u4^2)/9 - (f119*fvu1u1u5^2)/18 + 
      (f119*fvu1u1u8^2)/9 + (f119*fvu1u1u9^2)/9 + 
      (f119*fvu2u1u3)/9 - (f119*fvu2u1u4)/9 - 
      (f119*fvu2u1u11)/9 - (f119*tci11^2)/18 + 
      (f119*fvu1u1u5*tci12)/18 - (f119*fvu1u1u9*tci12)/9 - 
      (2*f119*fvu1u2u6*tci12)/9 + fvu1u1u8*
       ((-2*f119*fvu1u1u9)/9 - (2*f119*tci12)/9) + 
      fvu1u1u4*(-(f119*fvu1u1u5)/9 - (2*f119*fvu1u1u8)/9 + 
        (11*f119*fvu1u1u9)/54 + (f119*tci12)/18)) + 
    fvu1u1u6*((-8*f109)/9 + (f119*fvu1u1u9^2)/12 + 
      (f119*fvu2u1u14)/6 - (f109*tci11^2)/3 + 
      (f113*tci12)/9 - (f119*fvu1u1u9*tci12)/36 + 
      fvu1u1u10*((6*f53 - f55 - 6*f100)/9 + (f119*tci12)/12)) + 
    fvu1u1u1*((-2*(f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + 
         4*f102 + 4*f105 + 4*f107 - 4*f109 + f146))/9 + 
      (f119*fvu1u1u2^2)/9 + (f119*fvu1u1u3^2)/18 + 
      (f119*fvu1u1u4^2)/18 + ((2*f115 - f119)*fvu1u1u6)/12 + 
      (f119*fvu1u1u8^2)/9 + (f119*fvu1u1u9^2)/6 - 
      (2*f119*fvu2u1u1)/9 + (f119*fvu2u1u2)/9 - 
      (f119*fvu2u1u9)/9 - (2*f119*fvu2u1u11)/9 + 
      ((3*f3 + 9*f35 + 9*f61 - 9*f68 - 15*f99 + 12*f102 + 
         12*f105 + 12*f107 - 12*f109 - 4*f118 + 4*f120 + 3*f146)*
        tci11^2)/72 + ((-f3 - 3*f35 - 3*f61 + 3*f68 + 5*f99 - 
         4*f102 - 4*f105 - 4*f107 + 4*f109 - f146)*tci12)/12 - 
      (f119*fvu1u1u9*tci12)/3 + fvu1u1u8*((f119*fvu1u1u9)/9 - 
        (f119*tci12)/9) + fvu1u1u2*((f119*fvu1u1u4)/6 - 
        (f119*fvu1u1u8)/6 - (f119*fvu1u1u9)/3 - 
        (f119*tci12)/18) + fvu1u1u3*((2*f119)/9 + 
        (f119*tci12)/18) + fvu1u1u10*(-f119/9 + 
        (f119*tci12)/9) + fvu1u1u4*((2*f119)/9 + 
        (f119*fvu1u1u8)/6 - (2*f119*fvu1u1u9)/9 + 
        (f119*tci12)/9)) + fvu1u1u3*
     ((21*f3 - 48*f10 + 8*f11 + 63*f35 + 63*f61 - 63*f68 - 
        105*f99 + 72*f102 + 72*f107 + 8*f113 + 48*f115 - 8*f118 + 
        21*f146)/432 + (f119*fvu1u1u4^2)/3 - (f119*fvu1u1u6^2)/27 - 
      (f119*fvu1u1u8^2)/9 - (f119*fvu1u1u9^2)/12 - 
      (f119*fvu1u1u10^2)/18 - (f119*fvu2u1u1)/12 + 
      (f102*tci11^2)/3 + (8*f102*tci12)/9 + 
      (f119*fvu1u1u8*tci12)/6 + fvu1u1u6*
       (-f113/9 - (f119*fvu1u1u9)/12 - (f119*fvu1u1u10)/18 - 
        (f119*tci12)/12) + fvu1u1u9*((f119*fvu1u1u10)/6 + 
        (f119*tci12)/6) + fvu1u1u10*(f119/9 + 
        (f119*tci12)/3) + fvu1u1u4*(-f119/3 + 
        (f119*fvu1u1u8)/9 + (5*f119*tci12)/9)) + 
    ((-6*f10 + f11 - 6*f53 + f55 + 6*f100 + 6*f115)*tci12*
      tcr11^2)/9 + ((-f100 - 4*f115 + 2*f119)*tcr11^3)/18 + 
    ((-f112 - f117)*tci11^2*tcr12)/3 + 
    ((-6*f115 + 5*f119)*tcr12^3)/9 + 
    (2*(3*f100 + 3*f115 - 8*f119)*tci12*tcr21)/9 + 
    tcr11*(((f100 + f115)*tci11^2)/3 + 
      ((9*f100 + 6*f115 - 10*f119)*tci12*tcr12)/18 + 
      (f119*tcr21)/3) + ((-f100 + 2*f115 + f119)*tcr31)/9 + 
    ((f100 - 2*f115 - f119)*tcr32)/3 + (f119*tcr33)/18;
L ieu1ueu6 = (-12*f10 + 2*f11 - 6*f53 + f55 + 6*f100 - 
      3*f105 + 12*f115 - f120)/18 + (8*f107*fvu1u1u1^2)/9 - 
    (f102*fvu1u1u3^2)/6 + ((-f100 + f119)*fvu1u1u4^2)/3 + 
    ((6*f53 - f55 - 6*f100 + 3*f109 + f113)*fvu1u1u6^2)/18 + 
    fvu1u1u3*((f112 + f115 - f117)/18 - (2*f119*fvu1u1u4)/9 - 
      (f112*fvu1u1u6)/3 - (2*f119*fvu1u1u10)/9) + 
    fvu1u1u4*((3*f100 + f112 + 2*f115 - f117 - 5*f119)/36 - 
      (f115*fvu1u1u6)/3 + ((-f100 + f119)*fvu1u1u10)/3) + 
    (f119*fvu2u1u1)/3 + (f115*fvu2u1u2)/3 + 
    ((f115 - f119)*fvu2u1u6)/3 + ((f100 - f112)*fvu2u1u13)/3 + 
    ((-f100 + f112)*fvu2u1u14)/3 + 
    ((f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + f146)*tci11^2)/
     12 + (13*(f3 + 3*f35 + 3*f61 - 3*f68 - 5*f99 + f146)*
      tci12)/27 + (f112*fvu1u2u2*tci12)/3 - 
    (f115*fvu1u2u3*tci12)/3 + fvu1u1u10*((-f112 - f117)/6 + 
      (f100*tci12)/3) + fvu1u1u6*(-f109/3 - 
      (f100*fvu1u1u10)/3 + (f112*tci12)/3) + 
    fvu1u1u1*((-f3 - 3*f35 - 3*f61 + 3*f68 + 5*f99 - 4*f102 - 
        4*f105 - 4*f107 + 4*f109 - f146)/12 + 
      (2*f119*fvu1u1u3)/3 - (f119*fvu1u1u4)/3 + 
      ((7*f100 - 6*f112 - 2*f115 + 6*f117 - 7*f119)*fvu1u1u6)/
       36 - (f119*fvu1u1u10)/9 - (f119*tci12)/36);
L ieu0ueu6 = (f100 + 2*f115 - f119)/6;
L ieum1ueu6 = 0;
L ieum2ueu6 = 0;
L ieu2uou6 = (f110*fvu4u25)/3 + (f110*fvu4u28)/3 + 
    (7*f110*fvu4u39)/3 + (f110*fvu4u41)/3 - (f110*fvu4u49)/3 + 
    (5*f110*fvu4u51)/9 - (5*f110*fvu4u80)/12 + 
    (37*f110*fvu4u83)/12 + (7*f110*fvu4u91)/4 - 
    (5*f110*fvu4u93)/12 - (f110*fvu4u100)/3 + 
    (23*f110*fvu4u102)/18 - (f110*fvu4u111)/4 - (f110*fvu4u113)/2 - 
    f110*fvu4u114 + (8*f110*fvu4u129)/3 - (f110*fvu4u132)/3 + 
    (f110*fvu4u139)/6 + (8*f110*fvu4u141)/3 - (4*f110*fvu4u146)/9 + 
    (11*f110*fvu4u148)/18 - (5*f110*fvu4u171)/12 - 
    (23*f110*fvu4u174)/12 + (f110*fvu4u182)/12 - 
    (5*f110*fvu4u184)/12 - (f110*fvu4u190)/9 + (f110*fvu4u192)/18 - 
    (f110*fvu4u199)/4 - (f110*fvu4u201)/2 - f110*fvu4u202 - 
    (7*f110*fvu4u213)/12 + (f110*fvu4u215)/4 - (9*f110*fvu4u219)/4 - 
    (7*f110*fvu4u221)/12 + (4*f110*fvu4u225)/9 + (f110*fvu4u233)/4 + 
    (f110*fvu4u234)/2 + f110*fvu4u235 - (17*f110*fvu4u244)/3 - 
    (4*f110*fvu4u246)/3 + (7*f110*fvu4u252)/2 + 
    (17*f110*fvu4u255)/18 + (5*f110*fvu4u271)/12 + 
    (43*f110*fvu4u273)/12 - (67*f110*fvu4u277)/12 + 
    (5*f110*fvu4u279)/12 + (f110*fvu4u282)/3 + (f110*fvu4u289)/4 + 
    (f110*fvu4u290)/2 + f110*fvu4u291 + (2*f110*fvu4u293)/3 + 
    (2*f110*fvu4u295)/3 + 2*f110*fvu4u309 - 2*f110*fvu4u313 + 
    fvu3u45*((f110*fvu1u1u2)/2 + (7*f110*fvu1u1u3)/6 + 
      (2*f110*fvu1u1u6)/3 - (2*f110*fvu1u1u7)/3 - 
      (f110*fvu1u1u9)/2) + fvu3u70*(-(f110*fvu1u1u2)/6 + 
      (f110*fvu1u1u3)/6 + (f110*fvu1u1u9)/6) + 
    fvu3u78*((-2*f110*fvu1u1u2)/3 + (2*f110*fvu1u1u3)/3 + 
      (2*f110*fvu1u1u9)/3) + fvu3u62*((4*f110*fvu1u1u2)/3 - 
      (2*f110*fvu1u1u4)/3 - (2*f110*fvu1u1u5)/3 + 
      (4*f110*fvu1u1u6)/3 - (2*f110*fvu1u1u7)/3 - 
      (4*f110*fvu1u1u10)/3) + fvu3u25*((f110*fvu1u1u2)/3 + 
      (7*f110*fvu1u1u3)/6 - f110*fvu1u1u4 + (2*f110*fvu1u1u5)/3 - 
      (5*f110*fvu1u1u6)/6 + (f110*fvu1u1u7)/6 - 
      (f110*fvu1u1u10)/3) + fvu3u81*(-(f110*fvu1u1u3)/6 - 
      (f110*fvu1u1u4)/3 - (f110*fvu1u1u6)/6 + (f110*fvu1u1u7)/6 + 
      (f110*fvu1u1u9)/3 - (f110*fvu1u1u10)/3) + 
    fvu3u43*((f110*fvu1u1u2)/6 + (f110*fvu1u1u4)/3 + 
      (f110*fvu1u1u6)/6 - (f110*fvu1u1u7)/6 - (f110*fvu1u1u9)/2 + 
      (f110*fvu1u1u10)/3) + fvu3u23*(-(f110*fvu1u1u1)/3 - 
      (f110*fvu1u1u2)/6 - (f110*fvu1u1u6)/2 + (f110*fvu1u1u7)/6 + 
      (f110*fvu1u1u8)/3 + (f110*fvu1u1u9)/6 + 
      (f110*fvu1u1u10)/3) - (13*f110*tci11^3*tci12)/15 + 
    fvu3u80*((-2*f110*fvu1u1u2)/3 - f110*fvu1u1u3 + 
      (19*f110*fvu1u1u6)/6 - (19*f110*fvu1u1u7)/6 + 
      (17*f110*fvu1u1u8)/6 - (13*f110*fvu1u1u9)/6 - 
      (17*f110*tci12)/6) + fvu3u63*((-2*f110*fvu1u1u2)/3 - 
      (f110*fvu1u1u3)/2 + f110*fvu1u1u4 - (2*f110*fvu1u1u5)/3 + 
      (f110*fvu1u1u6)/2 + (f110*fvu1u1u7)/3 + (f110*fvu1u1u8)/3 + 
      (f110*fvu1u1u9)/3 + (f110*fvu1u1u10)/6 - 2*f110*tci12) + 
    fvu3u82*(-(f110*fvu1u1u3)/3 - (f110*fvu1u1u4)/3 + 
      (f110*fvu1u1u7)/3 - (f110*fvu1u1u8)/3 + (f110*fvu1u1u9)/3 + 
      (2*f110*tci12)/3) + fvu3u71*((f110*fvu1u1u2)/3 - 
      (4*f110*fvu1u1u3)/3 - f110*fvu1u1u4 + (2*f110*fvu1u1u5)/3 + 
      (4*f110*fvu1u1u6)/3 - (f110*fvu1u1u7)/3 - 
      (f110*fvu1u1u8)/3 + 2*f110*tci12) + 
    (10*f110*tci11^2*tci21)/27 + 
    fvu1u1u8*((17*f110*tci11^3)/81 - (17*f110*tci12*tci21)/
       3) + tci12*((-16*f110*tci31)/5 - 16*f110*tci32) + 
    (2072*f110*tci41)/135 + (64*f110*tci42)/5 + 
    (16*f110*tci43)/3 + ((-1259*f110*tci11^3)/1620 - 
      f110*tci12*tci21 - (84*f110*tci31)/5 - 
      16*f110*tci32)*tcr11 + (11*f110*tci11*tcr11^3)/
     180 + fvu1u1u1*(-(f110*fvu3u25)/6 - (f110*fvu3u43)/3 - 
      (7*f110*fvu3u45)/6 + (2*f110*fvu3u62)/3 + (7*f110*fvu3u63)/6 - 
      (f110*fvu3u70)/6 + (2*f110*fvu3u71)/3 - (2*f110*fvu3u78)/3 + 
      f110*fvu3u80 + (f110*fvu3u81)/2 + (f110*fvu3u82)/3 + 
      (179*f110*tci11^3)/1620 - (f110*tci12*tci21)/3 + 
      (36*f110*tci31)/5 + 3*f110*tci21*tcr11 - 
      (3*f110*tci11*tcr11^2)/20) + 
    fvu1u1u7*((-221*f110*tci11^3)/1620 + 
      (19*f110*tci12*tci21)/3 + (36*f110*tci31)/5 + 
      3*f110*tci21*tcr11 - (3*f110*tci11*tcr11^2)/20) + 
    fvu1u1u10*((13*f110*tci11^3)/180 + (f110*tci12*tci21)/
       9 + (28*f110*tci31)/5 + (7*f110*tci21*tcr11)/3 - 
      (7*f110*tci11*tcr11^2)/60) + 
    fvu1u1u4*((31*f110*tci11^3)/270 - (4*f110*tci12*tci21)/
       3 + (24*f110*tci31)/5 + 2*f110*tci21*tcr11 - 
      (f110*tci11*tcr11^2)/10) + 
    fvu1u1u5*(-(f110*tci11^3)/45 + (16*f110*tci12*tci21)/
       9 + (16*f110*tci31)/5 + (4*f110*tci21*tcr11)/3 - 
      (f110*tci11*tcr11^2)/15) + 
    fvu1u1u9*((-103*f110*tci11^3)/540 + 
      (49*f110*tci12*tci21)/9 + (4*f110*tci31)/5 + 
      (f110*tci21*tcr11)/3 - (f110*tci11*tcr11^2)/60) + 
    fvu1u1u3*((-73*f110*tci11^3)/1620 + (f110*tci12*tci21)/
       3 - (12*f110*tci31)/5 - f110*tci21*tcr11 + 
      (f110*tci11*tcr11^2)/20) + 
    fvu1u1u6*((13*f110*tci11^3)/162 - (46*f110*tci12*tci21)/
       9 - 8*f110*tci31 - (10*f110*tci21*tcr11)/3 + 
      (f110*tci11*tcr11^2)/6) + 
    fvu1u1u2*((-181*f110*tci11^3)/1620 - 
      (2*f110*tci12*tci21)/9 - (44*f110*tci31)/5 - 
      (11*f110*tci21*tcr11)/3 + (11*f110*tci11*tcr11^2)/
       60) + ((218*f110*tci11^3)/243 + (40*f110*tci12*tci21)/
       3)*tcr12 + (4*f110*tci11*tci12 + 
      (40*f110*tci21)/3)*tcr12^2 + 
    tcr11^2*((f110*tci11*tci12)/15 - 5*f110*tci21 + 
      2*f110*tci11*tcr12) - (130*f110*tci11*tcr33)/27;
L ieu1uou6 = 2*f110*fvu3u63 + (11*f110*tci11^3)/135 + 
    (4*f110*tci12*tci21)/3 + (48*f110*tci31)/5 + 
    4*f110*tci21*tcr11 - (f110*tci11*tcr11^2)/5;
L ieu0uou6 = 0;
L ieum1uou6 = 0;
L ieum2uou6 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou6+w^2*ieu1uou6+w^3*ieu0uou6+w^4*ieum1uou6+w^5*ieum2uou6;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou6a = K[w^1];
L ieu1uou6a = K[w^2];
L ieu0uou6a = K[w^3];
L ieum1uou6a = K[w^4];
L ieum2uou6a = K[w^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_odd.c> "%O"
#write <e6_odd.c> "return Eps5o2<T>("
#write <e6_odd.c> "%E", ieu2uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu0uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum2uou6a
#write <e6_odd.c> ");\n}"
L H=+u^1*ieu2ueu6+u^2*ieu1ueu6+u^3*ieu0ueu6+u^4*ieum1ueu6+u^5*ieum2ueu6;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu6a = H[u^1];
L ieu1ueu6a = H[u^2];
L ieu0ueu6a = H[u^3];
L ieum1ueu6a = H[u^4];
L ieum2ueu6a = H[u^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_even.c> "%O"
#write <e6_even.c> "return Eps5o2<T>("
#write <e6_even.c> "%E", ieu2ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu0ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum2ueu6a
#write <e6_even.c> ");\n}"
.end
