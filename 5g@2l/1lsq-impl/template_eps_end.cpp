
#ifdef USE_SD
template class Amp0q5g_a2l<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a2l<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a2l<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a2l<Vc::double_v>;
#endif
