(* ::Package:: *)

SetDirectory["/scratch/rmoodie/njet-tools/5g@2l/1lsq-impl/hel_HHHHH"];


ys=Get["P1_HHHHH.m"][[3]];


ysSimp=ParallelMap[Simplify, ys];


Export["ys.m",ysSimp];
